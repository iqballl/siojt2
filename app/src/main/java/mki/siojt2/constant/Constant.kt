package mki.siojt2.constant

/**
 * Created by iqbal on 22/04/19.
 */

object Constant {

    //AdvancedNetworkImage
    const val APP_NAME = "SIOJT"
    const val NETWORK_ERROR = "Please check your network and try again"

    const val PREF_NAME = "siojt_pref"

    //mki main
    const val BASE_URL = "http://api-sipt.mkitech.co.id/ws/api/"
    const val APP_CODE = "1:925823727896:android:1fdb984ce"

    /*NOTIFICATIONS */
    // broadcast receiver intent filters
    const val REGISTRATION_COMPLETE = "registrationComplete"
    const val PUSH_NOTIFICATION = "pushNotification"

    // id to handle the notification in the notification tray
    const val NOTIFICATION_ID = 100
    const val NOTIFICATION_ID_BIG_IMAGE = 101

    const val SHARED_PREF = "ah_firebase"
    const val TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss"

    const val STATUS_SUCCESS = 1
    const val STATUS_ERROR = 0
    const val STATUS_TOKEN_EXPIRED = 2

    const val MENUOBJEK1 = "Tanah"
    const val MENUOBJEK2 = "Bangunan"
    const val MENUOBJEK3 = "Tanaman"
    const val MENUOBJEK4 = "Mesin / Perlatan"

    const val MENUTANAH1 = "Lokasi Tanah"
    const val MENUTANAH2 = "Lingkungan"
    const val MENUTANAH3 = "Legalitas"

    const val MENUBANGUNAN1 = "Dasar"
    const val MENUBANGUNAN2 = "Spesifikasi"
    const val MENUBANGUNAN3 = "Fasilitas"
    const val MENUBANGUNAN4 = "Sarana Pelengkap"

    // file size
    const val LANDSCAPE_SIZE = 1080
    const val PORTRAIT_SIZE = 720
}
