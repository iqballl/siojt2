package mki.siojt2.constant

/**
 * Created by iqbal on 09/10/17.
 */

object ApiUri {

    const val GET_ACCESSTOKEN = "user/token"
    //GETTER
    const val GET_AUTH = "login"
    const val GET_LIST_PROJECT = "GetListProjects"
    const val GET_SYNC_PROJECT = "ProjectSynchronizing"

    /* mengambil daftar dan detail pemilik terkena dampak */
    const val ADD_PARTY = "AddParty"
    const val GET_LIST_PARTIES1 = "GetListFirstParty"
    const val GET_LIST_PARTIES2 = "GetListParties"
    const val GET_DETAIL_PARTIES = "GetDetailParty"

    /* daftar tanah */
    const val LAND_ADD = "AddLand"
    const val GET_LAND_LIST = "GetListLands"
    const val GET_LAND_DETAIL = "GetDetailLand"

    /* OBJEK TANAH */
    /* Bangunan */
    const val ADD_BUILDING = "AddBuilding"
    const val GET_DAFTAR_OBJEK_GEDUNG = "GetListBuildings"
    const val GET_BUILDING_DETAIL = "GetDetailBuilding"

    /* Tanaman */
    const val GET_DAFTAR_OBJEK_TANAMAN = "GetListPlants"
    const val ADD_PLANT = "AddPlant"

    /* Objek lain */
    const val GET_DAFTAR_OBJEK_LAIN = "GetListOtherFacilities"
    const val ADD_OTHER_FACILITY = "AddOtherFacility"
    const val GET_BUILDING_OTHER_FACILITY = "GetDetailOtherFacility"


    /* MASTER DATA */
    /* mengambil data wilayah */
    const val GET_DAFTAR_PROVINSI = "GetAreaProvinces"
    const val GET_DAFTAR_KOTAKABUPATEN = "GetAreaRegencies"
    const val GET_DAFTAR_KECAMATAN = "GetAreaDistricts"
    const val GET_DAFTAR_KELURAHAN = "GetAreaVillages"


    /* subjek atau pemilik tanah */
    const val GET_DAFTAR_MATA_PENCAHARIAN = "GetPartyLivelihoodOpts"
    const val GET_DAFTAR_PEKJERJAAN = "GetPartyOccupations"
    const val GET_DAFTAR_SUMBER_IDENTITAS = "GetPartyIdentitySources"

    const val GET_DAFTAR_PENGUASAAN_TANAH = "GetProjectPartyTypes"

    /* tanah */
    const val GET_LAND_INFO_ADD = "GetLandAddInfos"
    const val GET_LAND_SHAPE_LIST = "GetLandShapes"
    const val GET_LAND_TYPE_LIST = "GetLandTypes"
    const val GET_LAND_TOPOGRAFI = "GetLandTypographies"
    /* penggunaan tanah */
    const val GET_LAND_UTILIZATION = "GetLandUtilizations"

    /* kesesuain tanah peruntukan rt/rw */
    const val GET_LAND_ZONING_SPATIAL_PLAN = "GetLandZoningSpatialPlans"

    /* posisi tanah terhadap jalan */
    const val GET_LAND_TO_ROAD = "GetLandPositionToRoads"

    /* kepemilikan tanah*/
    const val GET_LAND_OWNERSHIPS = "GetLandOwnerships"

    //bangunan
    const val GET_TYPE_BANGUNAN = "GetBuildingTypes"
    const val GET_PONDASI_BUILDING = "GetBuildingFoundationTypes"
    const val GET_STRUCTURE_BUILDING = "GetBuildingStructureTypes"
    const val GET_ROOF_FRAME_BUILDING = "GetBuildingRoofFrameTypes"
    const val GET_ROOF_COVERING_BUILDING = "GetBuildingRoofCoveringTypes"

    const val GET_ROOF_PLAFON_BUILDING = "GetBuildingCeilingTypes"
    const val GET_ROOF_WALL_BUILDING = "GetBuildingWallTypes"
    const val GET_ROOF_DOOR_WINDOW_BUILDING = "GetBuildingDoorWindowTypes"
    const val GET_ROOF_FLOOR_BUILDING = "GetBuildingFloorTypes"

    //tanaman
    const val GET_TYPE_PLANTS = "GetPlants"

    //sarana pelengkap
    const val GET_FASILITY_TYPE = "GetFacilityTypes"
    const val GET_FASILITY_TYPE_CLEAN_WATER= "GetCleanWaterSources"
    const val GET_FASILITY_TYPE_CIRCUM= "GetCircumferentialFences"


}
