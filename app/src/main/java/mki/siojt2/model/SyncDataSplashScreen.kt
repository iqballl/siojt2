package mki.siojt2.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class SyncDataSplashScreen(

        @field:SerializedName("AppSyncStatus")
        var appSyncStatus: Int? = null,

        @field:SerializedName("AppSyncDate")
        var appSyncDate: Date? = null
)