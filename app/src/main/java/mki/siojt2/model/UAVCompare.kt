package mki.siojt2.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject

open class UAVCompare(
        var id: String? = "",
        var group: String? = "",
        var landAreaSize: AreaSize? = AreaSize("", ""),
        var buildingAreaSize: AreaSize? = AreaSize("", "")
)

open class AreaSize(
        var survey: String? = "",
        var uav: String? = ""
)