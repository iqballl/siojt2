package mki.siojt2.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class SyncDataDinamisSplashScreen(
        @field:SerializedName("AppSyncStatus")
        var appDinamisSyncStatus: Int? = null,

        @field:SerializedName("AppSyncDate")
        var appDinamisSyncDate: Date? = null
)