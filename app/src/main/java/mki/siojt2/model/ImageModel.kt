package mki.siojt2.model

import android.os.Parcel
import android.os.Parcelable

open class ImageModel : Parcelable {

    var name:String? = null
    var url:String? = null

    constructor()
    protected constructor(`in`:Parcel) {
        name = `in`.readString()
        url = `in`.readString()
    }
    override fun describeContents():Int {
        return 0
    }
    override fun writeToParcel(dest:Parcel, flags:Int) {
        dest.writeString(name)
        dest.writeString(url)
    }

    companion object CREATOR : Parcelable.Creator<ImageModel> {
        override fun createFromParcel(parcel: Parcel): ImageModel {
            return ImageModel(parcel)
        }

        override fun newArray(size: Int): Array<ImageModel?> {
            return arrayOfNulls(size)
        }
    }

/*    companion object {
        var CREATOR: Parcelable.Creator<ImageModel> = object: Parcelable.Creator<ImageModel> {
            override fun createFromParcel(`in`:Parcel):ImageModel {
                return ImageModel(`in`)
            }
            override fun newArray(size:Int): Array<ImageModel?> {
                return arrayOfNulls<ImageModel>(size)
            }
        }
    }*/

    /*internal var name: String? = null
    private var url:String? = null


    private fun ImageModel(`in`: Parcel) {
        name = `in`.readString()
        url = `in`.readString()
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getUrl(): String? {
        return url
    }

    fun setUrl(url: String) {
        this.url = url
    }

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(name)
        dest.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ImageModel> {
        override fun createFromParcel(`in`: Parcel): ImageModel {
            return ImageModel(`in`)
        }

        override fun newArray(size: Int): Array<ImageModel?> {
            return arrayOfNulls(size)
        }
    }*/

}