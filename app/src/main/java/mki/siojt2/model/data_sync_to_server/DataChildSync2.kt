package mki.siojt2.model.data_sync_to_server

import com.google.gson.annotations.SerializedName
import mki.siojt2.model.localsave.PartyAdd
import mki.siojt2.model.localsave.ProjectLand

data class DataChildSync2(
        @field:SerializedName("PartyAdd")
        var partyAdd: List<PartyAdd>? = null,

        @field:SerializedName("ProjectLand")
        var projectLand: List<ProjectLand>? = null
)