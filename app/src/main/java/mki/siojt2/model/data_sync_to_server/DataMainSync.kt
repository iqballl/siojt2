package mki.siojt2.model.data_sync_to_server

import com.google.gson.annotations.SerializedName

data class DataMainSync(

        @field:SerializedName("DataProject")
        var dataProject: DataChildSync? = null
)