package mki.siojt2.model.data_sync_to_server

import com.google.gson.annotations.SerializedName

data class DataChildSync(

        @field:SerializedName("ProjectId")
        var projectId: Int? = null,

        @field:SerializedName("DataChildSync2")
        var dataChildSync2: DataChildSync2? = null

)