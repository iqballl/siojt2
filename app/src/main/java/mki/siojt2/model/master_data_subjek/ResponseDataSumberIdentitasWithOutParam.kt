package mki.siojt2.model.master_data_subjek

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataSumberIdentitasWithOutParam : RealmObject() {

    @SerializedName("IdentitySourceId")
    @Expose
    private var identitySourceId: Int? = null

    @SerializedName("IdentitySourceName")
    @Expose
    private var identitySourceName: String? = ""

    @SerializedName("IdentitySourceStatus")
    @Expose
    private var identitySourceStatus: Int? = null

    fun getidentitySourceId(): Int? {
        return identitySourceId
    }

    fun setidentitySourceId(identitySourceId: Int?) {
        this.identitySourceId = identitySourceId
    }

    fun getidentitySourceName(): String? {
        return identitySourceName
    }

    fun setidentitySourceName(identitySourceName: String) {
        this.identitySourceName = identitySourceName
    }

    fun geidentitySourceStatus(): Int? {
        return identitySourceStatus
    }

    fun setidentitySourceStatus(identitySourceStatus: Int?) {
        this.identitySourceStatus = identitySourceStatus
    }

    override fun toString(): String {
        return identitySourceName.toString()
    }
}