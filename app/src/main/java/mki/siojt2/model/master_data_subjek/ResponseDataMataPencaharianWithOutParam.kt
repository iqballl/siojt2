package mki.siojt2.model.master_data_subjek

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataMataPencaharianWithOutParam : RealmObject() {

    @SerializedName("LivelihoodId")
    @Expose
    private var liveliHoodId: Int? = null

    @SerializedName("LivelihoodName")
    @Expose
    private var liveliHoodName: String? = ""

    @SerializedName("LivelihoodStatus")
    @Expose
    private var liveliHoodStatus: Int? = null

    fun getliveliHoodId(): Int? {
        return liveliHoodId
    }

    fun setliveliHoodId(liveliHoodId: Int?) {
        this.liveliHoodId = liveliHoodId
    }

    fun getliveliHoodName(): String? {
        return liveliHoodName
    }

    fun setliveliHoodName(liveliHoodName: String) {
        this.liveliHoodName = liveliHoodName
    }

    fun geliveliHoodStatus(): Int? {
        return liveliHoodStatus
    }

    fun setliveliHoodStatus(liveliHoodStatus: Int?) {
        this.liveliHoodStatus = liveliHoodStatus
    }

    override fun toString(): String {
        return liveliHoodName.toString()
    }
}
