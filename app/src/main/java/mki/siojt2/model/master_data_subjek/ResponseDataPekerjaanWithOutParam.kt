package mki.siojt2.model.master_data_subjek

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataPekerjaanWithOutParam : RealmObject(){

    @SerializedName("OccupationId")
    @Expose
    private var occupationId: Int? = null

    @SerializedName("OccupationName")
    @Expose
    private var occupationName: String? = ""

    @SerializedName("OccupationStatus")
    @Expose
    private var occupationStatus: Int? = null

    fun getliveliHoodId(): Int? {
        return occupationId
    }

    fun setliveliHoodId(occupationId: Int?) {
        this.occupationId = occupationId
    }

    fun getliveliHoodName(): String? {
        return occupationName
    }

    fun setliveliHoodName(occupationName: String) {
        this.occupationName = occupationName
    }

    fun geliveliHoodStatus(): Int? {
        return occupationStatus
    }

    fun setliveliHoodStatus(occupationStatus: Int?) {
        this.occupationStatus = occupationStatus
    }

    override fun toString():String {
        return occupationName.toString()
    }


}