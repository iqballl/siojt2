package mki.siojt2.model.master_data_subjek

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataMataPencaharian(

	@field:SerializedName("LivelihoodCreateBy")
	var livelihoodCreateBy: Int? = null,

	@field:SerializedName("LivelihoodLogId")
	var livelihoodLogId: Any? = null,

	@field:SerializedName("LivelihoodId")
	var livelihoodId: Int? = null,

	@field:SerializedName("LivelihoodStatus")
	var livelihoodStatus: Int? = null,

	@field:SerializedName("LivelihoodCreateDate")
	var livelihoodCreateDate: String? = null,

	@field:SerializedName("LivelihoodName")
	var livelihoodName: String? = null,

	@field:SerializedName("LivelihoodDescrtiption")
	var livelihoodDescrtiption: Any? = null
){
	override fun toString(): String {
		return livelihoodName.toString()
	}
}