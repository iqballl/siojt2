package mki.siojt2.model.master_data_subjek

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataSumberIdentitas(

	@field:SerializedName("IdentitySourceCreateDate")
	var identitySourceCreateDate: String? = null,

	@field:SerializedName("IdentitySourceDescription")
	var identitySourceDescription: Any? = null,

	@field:SerializedName("IdentitySourceCreateBy")
	var identitySourceCreateBy: Int? = null,

	@field:SerializedName("IdentitySourceStatus")
	var identitySourceStatus: Int? = null,

	@field:SerializedName("IdentitySourceName")
	var identitySourceName: String? = null,

	@field:SerializedName("IdentitySourceLogId")
	var identitySourceLogId: Any? = null,

	@field:SerializedName("IdentitySourceId")
	var identitySourceId: Int? = null
)