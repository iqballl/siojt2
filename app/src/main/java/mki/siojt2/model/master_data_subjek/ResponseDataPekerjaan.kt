package mki.siojt2.model.master_data_subjek

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataPekerjaan(

	@field:SerializedName("OccupationCreateDate")
	var occupationCreateDate: String? = null,

	@field:SerializedName("OccupationCreateBy")
	var occupationCreateBy: Int? = null,

	@field:SerializedName("OccupationLogId")
	var occupationLogId: Any? = null,

	@field:SerializedName("OccupationDesctiption")
	var occupationDesctiption: Any? = null,

	@field:SerializedName("OccupationId")
	var occupationId: Int? = null,

	@field:SerializedName("OccupationStatus")
	var occupationStatus: Int? = null,

	@field:SerializedName("OccupationName")
	var occupationName: String? = null
)