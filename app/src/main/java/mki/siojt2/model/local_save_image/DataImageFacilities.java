package mki.siojt2.model.local_save_image;

import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;
import mki.siojt2.model.localsave.SaranaLain;

public class DataImageFacilities extends RealmObject {

    public Integer imageId;
    public String imagepathLand;
    public String imagenameLand;

    @LinkingObjects("dataImageOtherFacilities")
    private final RealmResults<SaranaLain> imageSaranaPelengkap = null;

    public String getImagepathLand() {
        return imagepathLand;
    }

    public void setImagepathLand(String imagepathLand) {
        this.imagepathLand = imagepathLand;
    }

    public String getImagenameLand() {
        return imagenameLand;
    }

    public void setImagenameLand(String imagenameLand) {
        this.imagenameLand = imagenameLand;
    }

    public Integer getImageId() {
        return this.imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public RealmResults<SaranaLain> getImageSaranaPelengkap() {
        return this.imageSaranaPelengkap;
    }
}
