package mki.siojt2.model.local_save_image;

import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;
import io.realm.annotations.PrimaryKey;
import mki.siojt2.model.localsave.Bangunan;

public class DataImageBuilding extends RealmObject {

    public Integer imageId;
    public String imagepathLand;
    public String imagenameLand;

    @LinkingObjects("dataImageBuilding")
    private final RealmResults<Bangunan> buildings = null;

    public String getImagepathLand() {
        return this.imagepathLand;
    }

    public void setImagepathLand(String imagepathLand) {
        this.imagepathLand = imagepathLand;
    }

    public String getImagenameLand() {
        return this.imagenameLand;
    }

    public void setImagenameLand(String imagenameLand) {
        this.imagenameLand = imagenameLand;
    }

    public Integer getImageId() {
        return this.imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public RealmResults<Bangunan> getBuildings() {
        return buildings;
    }
}
