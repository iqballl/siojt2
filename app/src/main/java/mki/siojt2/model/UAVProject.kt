package mki.siojt2.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject

open class UAVProject: RealmObject() {
    @SerializedName("geom")
    @Expose
    var geom: RealmList<UAVGeomResponse>? = null
}