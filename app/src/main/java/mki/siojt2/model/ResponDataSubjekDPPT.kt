package mki.siojt2.model

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponDataSubjekDPPT(

	@field:SerializedName("id_jenis")
	var idJenis: String? = null,

	@field:SerializedName("kode_jenis")
	var kodeJenis: String? = null,

	@field:SerializedName("nama_jenis")
	var namaJenis: String? = null,

	@field:SerializedName("keterangan")
    var keterangan: String? = null
)