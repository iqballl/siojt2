package mki.siojt2.model.data_form_tanah

import com.google.gson.annotations.SerializedName

data class DataFormTanah3(

        @field:SerializedName("ProjectLandOwnershipId")
        var projectLandOwnershipId: Int? = 0,

        @field:SerializedName("ProjectLandOwnershipName")
        var projectLandOwnershipName: String? = "",

        @field:SerializedName("ProjectLandOwnershipNameOther")
        var projectLandOwnershipNameOther: String? = "",

        @field:SerializedName("ProjectLandOwnershipNameTypeSHM")
        var projectLandOwnershipNameTypeSHM: String? = "",

        @field:SerializedName("ProjectLandDateOfIssue")
        var projectLandDateOfIssue: String? = "",

        @field:SerializedName("ProjectLandRightsExpirationDate")
        var projectLandRightsExpirationDate: String? = "",

        @field:SerializedName("ProjectLandNIB")
        var projectLandNIB: String? = "",

        @field:SerializedName("ProjectLandCertificateNumber")
        var projectLandCertificateNumber: String? = "",

        @field:SerializedName("ProjectLandUtilizationLandUtilizationId")
        var projectLandUtilizationLandUtilizationId: ArrayList<String>? = null,

        @field:SerializedName("ProjectLandUtilizationLandUtilizationName")
        var projectLandUtilizationLandUtilizationName: ArrayList<String>? = null,

        @field:SerializedName("projectLandUtilizationLandUtilizationNameOther")
        var projectLandUtilizationLandUtilizationNameOther: ArrayList<String>? = null,

        @field:SerializedName("ProjectLandZoningSpatialPlanId")
        var projectLandZoningSpatialPlanId: Int? = 0,

        @field:SerializedName("ProjectLandZoningSpatialPlanName")
        var projectLandZoningSpatialPlanName: String? = "",

        @field:SerializedName("ProjectLandOrientationWest")
        var projectLandOrientationWest: String? = "",

        @field:SerializedName("ProjectLandOrientationEast")
        var projectLandOrientationEast: String? = "",

        @field:SerializedName("projectLandOrientationNorth")
        var projectLandOrientationNorth: String? = "",

        @field:SerializedName("ProjectLandOrientationSouth")
        var projectLandOrientationSouth: String? = "",

        @field:SerializedName("ProjectLandOrientationNorthwest")
        var projectLandOrientationNorthwest: String? = "",

        @field:SerializedName("ProjectLandOrientationNortheast")
        var projectLandOrientationNortheast: String? = "",

        @field:SerializedName("ProjectLandOrientationSoutheast")
        var projectLandOrientationSoutheast: String? = "",

        @field:SerializedName("ProjectLandOrientationSouthwest")
        var projectLandOrientationSouthwest: String? = "",

        @field:SerializedName("ProjectLandAddInfoAddInfoId")
        var projectLandAddInfoAddInfoId: ArrayList<String>? = null,

        @field:SerializedName("ProjectLandAddInfoAddInfoName")
        var projectLandAddInfoAddInfoName: ArrayList<String>? = null,

        var sendIdAlasHak: String?,

        var dampak: String? = "",

        var landAlasHakTanah: String? = ""
)