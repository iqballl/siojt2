package mki.siojt2.model.data_form_tanah

import com.google.android.gms.maps.model.LatLng

data class DataKoordinatMaps(
        var coordinat: List<LatLng>? = null
)