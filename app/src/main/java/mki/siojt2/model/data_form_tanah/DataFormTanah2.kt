package mki.siojt2.model.data_form_tanah

import com.google.gson.annotations.SerializedName

data class DataFormTanah2(

        @field:SerializedName("ProjectLandPositionToRoadId")
        var projectLandPositionToRoadId: Int? = 0,

        @field:SerializedName("ProjectLandPositionToRoadName")
        var projectLandPositionToRoadName: String = "",

        @field:SerializedName("ProjectLandWideFrontRoad")
        var projectLandWideFrontRoad: Float? = 0F,

        @field:SerializedName("ProjectLandFrontage")
        var projectLandFrontage: Float? = 0F,

        @field:SerializedName("ProjectLandLength")
        var projectLandLength: Float? = 0F,

        @field:SerializedName("ProjectLandShapeId")
        var projectLandShapeId: Int? = 0,

        @field:SerializedName("ProjectLandShapeName")
        var projectLandShapeName: String = "",

        @field:SerializedName("ProjectLandShapeNameOther")
        var projectLandShapeNameOther: String = "",

        @field:SerializedName("ProjectLandAreaCertificate")
        var projectLandAreaCertificate: String? = "",

        @field:SerializedName("ProjectLandAreaAffected")
        var projectLandAreaAffected: String? = "",

        @field:SerializedName("ProjectLandTypeLandTypeId")
        var projectLandTypeLandTypeId: ArrayList<String>? = null,

        @field:SerializedName("ProjectLandTypeLandTypeName")
        var projectLandTypeLandTypeName: ArrayList<String>? = null,

        @field:SerializedName("ProjectLandTypeLandTypeNameOther")
        var projectLandTypeLandTypeNameOther: ArrayList<String>? = null,

        @field:SerializedName("ProjectLandTopographyId")
        var projectLandTopographyId: Int? = 0,

        @field:SerializedName("ProjectLandTopographyName")
        var projectLandTopographyName: String? = "",

        @field:SerializedName("ProjectLandHeightToRoad")
        var projectLandHeightToRoad: String? = "",

        @field:SerializedName("TanahSisa")
        var tanahSisa: String? = "",

        @field:SerializedName("PartyLivelihoodLivelihoodId")
        var partyLivelihoodLivelihoodId: ArrayList<String>? = null,

        @field:SerializedName("PartyLivelihoodLivelihoodOther")
        var partyLivelihoodLivelihoodOther: ArrayList<String>? = null,

        @field:SerializedName("PartyLivelihoodIncome")
        var partyLivelihoodIncome: ArrayList<String>? = null,

        @field:SerializedName("PartyLivelihoodName")
        var partyLivelihoodName: ArrayList<String>? = null

)