package mki.siojt2.model.data_form_tanah

import com.google.gson.annotations.SerializedName

data class DataJenisTanah(

        @field:SerializedName("ProjectLandTypeId")
        var projectLandTypeId: String? = "",

        @field:SerializedName("ProjectLandTypeName")
        var projectLandTypeName: String? = "",

        @field:SerializedName("ProjectLandTypeNameOther")
        var projectLandTypeNameOther: String? = ""
)