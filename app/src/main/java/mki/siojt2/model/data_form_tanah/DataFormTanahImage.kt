package mki.siojt2.model.data_form_tanah

import com.google.gson.annotations.SerializedName
import mki.siojt2.model.local_save_image.DataImageLand

data class DataFormTanahImage(
        @field:SerializedName("DataImagePhoto")
        var dataImagePhoto: List<DataImageLand>? = null,
        var notes: String = "",
        var hasOtherObject: String? = ""
)