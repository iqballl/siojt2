package mki.siojt2.model.data_form_tanah

import com.mapbox.mapboxsdk.geometry.LatLng


data class DataKoordinatMapBox(
        var coordinat: List<LatLng>? = arrayListOf()
)