package mki.siojt2.model.data_form_tanah

import com.google.gson.annotations.SerializedName
import com.mapbox.mapboxsdk.geometry.LatLng

data class DataFormTanah1(
        @field:SerializedName("projectLandNumberList")
        var projectLandNumberList: String? = "",

        @field:SerializedName("LandProvinceCode")
        var landProvinceCode: String? = "",

        @field:SerializedName("LandProvinceName")
        var landProvinceName: String? = "",

        @field:SerializedName("LandRegencyCode")
        var landRegencyCode: String? = "",

        @field:SerializedName("LandRegencyName")
        var landRegencyName: String? = "",

        @field:SerializedName("LandDistrictCode")
        var landDistrictCode: String? = "",

        @field:SerializedName("LandDistrictName")
        var landDistrictName: String? = "",

        @field:SerializedName("LandVillageId")
        var landVillageId: String? = "",

        @field:SerializedName("LandVillageName")
        var landVillageName: String? = "",

        @field:SerializedName("LandBlock")
        var landBlock: String? = "",

        @field:SerializedName("LandNumber")
        var landNumber: String? = "",

        @field:SerializedName("LandComplexName")
        var landComplexName: String? = "",

        @field:SerializedName("LandStreetName")
        var landStreetName: String? = "",

        @field:SerializedName("LandRT")
        var landRT: String? = "",

        @field:SerializedName("LandRW")
        var landRW: String? = "",

        @field:SerializedName("LandPostalCode")
        var landPostalCode: String? = "",

        @field:SerializedName("ProjectLandLatitude")
        var projectLandLatitude: String? = "",

        @field:SerializedName("projectLandLangitude")
        var rojectLandLangitude: String? = "",

        @field:SerializedName("projectimageCanvas")
        var imageCanvas: ByteArray? = null,

        var polygonPoint: List<LatLng>? = arrayListOf(),

        var subjectId: Int?,
        var subjectName: String?

) {
        override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as DataFormTanah1

                if (imageCanvas != null) {
                        if (other.imageCanvas == null) return false
                        if (!imageCanvas!!.contentEquals(other.imageCanvas!!)) return false
                } else if (other.imageCanvas != null) return false

                return true
        }

        override fun hashCode(): Int {
                return imageCanvas?.contentHashCode() ?: 0
        }
}