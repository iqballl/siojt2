package mki.siojt2.model.data_form_tanah

import com.google.gson.annotations.SerializedName

data class DataPenggunaanTanah(

        @field:SerializedName("ProjectLandUtilizationId")
        var projectLandUtilizationId: String? = "",

        @field:SerializedName("ProjectLandUtilizationName")
        var projectLandUtilizationName: String? = "",

        @field:SerializedName("ProjectLandUtilizationNameOther")
        var projectLandUtilizationNameOther: String? = ""
)