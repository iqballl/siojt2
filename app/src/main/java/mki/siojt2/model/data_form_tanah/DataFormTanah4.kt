package mki.siojt2.model.data_form_tanah

import com.google.gson.annotations.SerializedName

data class DataFormTanah4(

        @field:SerializedName("ProjectLandEaseToPropertyId")
        var projectLandEaseToPropertyId: Int? = 0,

        @field:SerializedName("ProjectLandEaseToPropertyName")
        var projectLandEaseToPropertyName: String? = "",

        @field:SerializedName("ProjectLandEaseToShopingCenter")
        var projectLandEaseToShopingCenter: Int? = 0,

        @field:SerializedName("ProjectLandEaseToShopingCenterName")
        var projectLandEaseToShopingCenterName: String? = "",

        @field:SerializedName("ProjectLandEaseToEducationFacilitiesId")
        var projectLandEaseToEducationFacilitiesId: Int? = 0,

        @field:SerializedName("ProjectLandEaseToEducationFacilitiesName")
        var projectLandEaseToEducationFacilitiesName: String? = "",

        @field:SerializedName("ProjectLandEaseToTouristSitesId")
        var projectLandEaseToTouristSitesId: Int? = 0,

        @field:SerializedName("ProjectLandEaseToTouristSitesName")
        var projectLandEaseToTouristSitesName: String? = "",

        @field:SerializedName("ProjectLandEaseOfTransportationId")
        var projectLandEaseOfTransportationId: Int? = 0,

        @field:SerializedName("ProjectLandEaseOfTransportationName")
        var projectLandEaseOfTransportationName: String? = "",

        @field:SerializedName("ProjectLandSecurityAgainstCrimeId")
        var projectLandSecurityAgainstCrimeId: Int? = 0,

        @field:SerializedName("ProjectLandSecurityAgainstCrimeName")
        var projectLandSecurityAgainstCrimeName: String? = "",

        @field:SerializedName("ProjectLandSafetyAgainstFireHazardsId")
        var projectLandSafetyAgainstFireHazardsId: Int? = 0,

        @field:SerializedName("ProjectLandSafetyAgainstFireHazardsName")
        var projectLandSafetyAgainstFireHazardsName: String? = "",

        @field:SerializedName("ProjectLandSecurityAgainstNaturalDisastersId")
        var projectLandSecurityAgainstNaturalDisastersId: Int? = 0,

        @field:SerializedName("ProjectLandSecurityAgainstNaturalDisastersName")
        var projectLandSecurityAgainstNaturalDisastersName: String? = "",

        @field:SerializedName("ProjectLandAvailableElectricalGridId")
        var projectLandAvailableElectricalGridId: Int? = 0,

        @field:SerializedName("ProjectLandAvailableElectricalGridName")
        var projectLandAvailableElectricalGridName: String? = "",

        @field:SerializedName("projectLandAvailableCleanWaterSystemId")
        var projectLandAvailableCleanWaterSystemId: Int? = 0,

        @field:SerializedName("projectLandAvailableCleanWaterSystemName")
        var projectLandAvailableCleanWaterSystemName: String? = "",

        @field:SerializedName("ProjectLandAvailableTelephoneNetworkId")
        var projectLandAvailableTelephoneNetworkId: Int? = 0,

        @field:SerializedName("ProjectLandAvailableTelephoneNetworkName")
        var projectLandAvailableTelephoneNetworkName: String? = "",

        @field:SerializedName("ProjectLandAvailableGasPipelineNetworkId")
        var projectLandAvailableGasPipelineNetworkId: Int? = 0,

        @field:SerializedName("ProjectLandAvailableGasPipelineNetworkName")
        var projectLandAvailableGasPipelineNetworkName: String? = "",

        @field:SerializedName("ProjectLandSUTETPresenceId")
        var projectLandSUTETPresenceId: Int? = 0,

        @field:SerializedName("ProjectLandSUTETPresenceName")
        var projectLandSUTETPresenceName: String? = "",

        @field:SerializedName("ProjectLandSUTETDistance")
        var projectLandSUTETDistance: Float? = 000000F,

        @field:SerializedName("ProjectLandCemeteryPresenceId")
        var projectLandCemeteryPresenceId: Int? = 0,

        @field:SerializedName("ProjectLandCemeteryPresenceName")
        var projectLandCemeteryPresenceName: String? = "",

        @field:SerializedName("ProjectLandCemeteryDistance")
        var projectLandCemeteryDistance: Float? = 000000F,

        @field:SerializedName("ProjectLandSkewerPresenceId")
        var projectLandSkewerPresenceId: Int? = 0,
        @field:SerializedName("ProjectLandSkewerPresenceName")
        var projectLandSkewerPresenceName: String? = ""
)