package mki.siojt2.model

import io.realm.RealmObject
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Endra on 2019-11-28.
 */
open class ResponseDataListProjectLocal : RealmObject(){
    @SerializedName("ProjectAssignCreateBy")
    @Expose
    private var projectAssignCreateBy: Int? = null

    @SerializedName("ProjectAssignId")
    @Expose
    private var projectAssignId: Int? = null

    @SerializedName("ProjectAssignMobileUserId")
    @Expose
    private var projectAssignMobileUserId: Int? = null

    @SerializedName("ProjectAssignProjectId")
    @Expose
    private var projectAssignProjectId: Int? = null

    @SerializedName("ProjectAssignStatus")
    @Expose
    private var projectAssignStatus: Int? = null
    @SerializedName("ProjectAssignSyncStatus")
    @Expose
    private var projectAssignSyncStatus: Int? = null
    @SerializedName("ProjectCreateBy")
    @Expose
    private var projectCreateBy: Int? = null
    @SerializedName("ProjectCreateDate")
    @Expose
    private var projectCreateDate: String? = null
    @SerializedName("ProjectDescription")
    @Expose
    private var projectDescription: String? = null
    @SerializedName("ProjectJabatanPemohon")
    @Expose
    private var projectJabatanPemohon: String? = null
    @SerializedName("ProjectJangkaWaktu")
    @Expose
    private var projectJangkaWaktu: Int? = null
    @SerializedName("ProjectJumlahBidang")
    @Expose
    private var projectJumlahBidang: Int? = null
    @SerializedName("ProjectLuas")
    @Expose
    private var projectLuas: Int? = null
    @SerializedName("ProjectNamaPemohon")
    @Expose
    private var projectNamaPemohon: String? = null
    @SerializedName("ProjectName")
    @Expose
    private var projectName: String? = null
    @SerializedName("ProjectNilaiAnggaran")
    @Expose
    private var projectNilaiAnggaran: Double? = null
    @SerializedName("ProjectPanjang")
    @Expose
    private var projectPanjang: Int? = null
    @SerializedName("ProjectStatus")
    @Expose
    private var projectStatus: Int? = null
    @SerializedName("ProjectSumberAnggaran")
    @Expose
    private var projectSumberAnggaran: String? = null
    @SerializedName("ProjectTanggalPermohonan")
    @Expose
    private var projectTanggalPermohonan: String? = null
    @SerializedName("uavdata")
    @Expose
    private var UAVDataResponse: UAVDataResponse? = null

    fun getUAVDataResponse(): UAVDataResponse? {
        return UAVDataResponse
    }

    fun getProjectAssignCreateBy(): Int? {
        return projectAssignCreateBy
    }

    fun setProjectAssignCreateBy(projectAssignCreateBy: Int?) {
        this.projectAssignCreateBy = projectAssignCreateBy
    }

    fun getProjectAssignId(): Int? {
        return projectAssignId
    }

    fun setProjectAssignId(projectAssignId: Int?) {
        this.projectAssignId = projectAssignId
    }

    fun getProjectAssignMobileUserId(): Int? {
        return projectAssignMobileUserId
    }

    fun setProjectAssignMobileUserId(projectAssignMobileUserId: Int?) {
        this.projectAssignMobileUserId = projectAssignMobileUserId
    }

    fun getProjectAssignProjectId(): Int? {
        return projectAssignProjectId
    }

    fun setProjectAssignProjectId(projectAssignProjectId: Int?) {
        this.projectAssignProjectId = projectAssignProjectId
    }

    fun getProjectAssignStatus(): Int? {
        return projectAssignStatus
    }

    fun setProjectAssignStatus(projectAssignStatus: Int?) {
        this.projectAssignStatus = projectAssignStatus
    }

    fun getProjectAssignSyncStatus(): Int? {
        return projectAssignSyncStatus
    }

    fun setProjectAssignSyncStatus(projectAssignSyncStatus: Int?) {
        this.projectAssignSyncStatus = projectAssignSyncStatus
    }

    fun getProjectCreateBy(): Int? {
        return projectCreateBy
    }

    fun setProjectCreateBy(projectCreateBy: Int?) {
        this.projectCreateBy = projectCreateBy
    }

    fun getProjectCreateDate(): String? {
        return projectCreateDate
    }

    fun setProjectCreateDate(projectCreateDate: String) {
        this.projectCreateDate = projectCreateDate
    }

    fun getProjectDescription(): String? {
        return projectDescription
    }

    fun setProjectDescription(projectDescription: String) {
        this.projectDescription = projectDescription
    }

    fun getProjectJabatanPemohon(): String? {
        return projectJabatanPemohon
    }

    fun setProjectJabatanPemohon(projectJabatanPemohon: String) {
        this.projectJabatanPemohon = projectJabatanPemohon
    }

    fun getProjectJangkaWaktu(): Int? {
        return projectJangkaWaktu
    }

    fun setProjectJangkaWaktu(projectJangkaWaktu: Int?) {
        this.projectJangkaWaktu = projectJangkaWaktu
    }

    fun getProjectJumlahBidang(): Int? {
        return projectJumlahBidang
    }

    fun setProjectJumlahBidang(projectJumlahBidang: Int?) {
        this.projectJumlahBidang = projectJumlahBidang
    }

    fun getProjectLuas(): Int? {
        return projectLuas
    }

    fun setProjectLuas(projectLuas: Int?) {
        this.projectLuas = projectLuas
    }

    fun getProjectNamaPemohon(): String? {
        return projectNamaPemohon
    }

    fun setProjectNamaPemohon(projectNamaPemohon: String) {
        this.projectNamaPemohon = projectNamaPemohon
    }

    fun getProjectName(): String? {
        return projectName
    }

    fun setProjectName(projectName: String) {
        this.projectName = projectName
    }

    fun getProjectNilaiAnggaran(): Double? {
        return projectNilaiAnggaran
    }

    fun setProjectNilaiAnggaran(projectNilaiAnggaran: Double?) {
        this.projectNilaiAnggaran = projectNilaiAnggaran
    }

    fun getProjectPanjang(): Int? {
        return projectPanjang
    }

    fun setProjectPanjang(projectPanjang: Int?) {
        this.projectPanjang = projectPanjang
    }

    fun getProjectStatus(): Int? {
        return projectStatus
    }

    fun setProjectStatus(projectStatus: Int?) {
        this.projectStatus = projectStatus
    }

    fun getProjectSumberAnggaran(): String? {
        return projectSumberAnggaran
    }

    fun setProjectSumberAnggaran(projectSumberAnggaran: String) {
        this.projectSumberAnggaran = projectSumberAnggaran
    }

    fun getProjectTanggalPermohonan(): String? {
        return projectTanggalPermohonan
    }

    fun setProjectTanggalPermohonan(projectTanggalPermohonan: String) {
        this.projectTanggalPermohonan = projectTanggalPermohonan
    }
}