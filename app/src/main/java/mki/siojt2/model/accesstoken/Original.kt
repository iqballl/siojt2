package mki.siojt2.model.accesstoken

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Original(

	@field:SerializedName("access_token")
	var accessToken: String? = null,

	@field:SerializedName("token_type")
	var tokenType: String? = null,

	@field:SerializedName("expires_in")
	var expiresIn: Int? = null
)