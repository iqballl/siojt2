package mki.siojt2.model.accesstoken

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseAccessToken(

	@field:SerializedName("exception")
	var exception: Any? = null,

	@field:SerializedName("headers")
	var headers: Headers? = null,

	@field:SerializedName("original")
	var original: Original? = null
)