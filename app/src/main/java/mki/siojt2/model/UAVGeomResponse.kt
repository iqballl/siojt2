package mki.siojt2.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class UAVGeomResponse: RealmObject() {
    @SerializedName("lng")
    var lng = 0.0

    @SerializedName("lat")
    var lat = 0.0
}