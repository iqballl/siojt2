package mki.siojt2.model.master_data_form_tanah

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListInformasiTambahanTanah(

	@field:SerializedName("AddInfoName")
	var addInfoName: String? = null,

	@field:SerializedName("AddInfoDescription")
	var addInfoDescription: Any? = null,

	@field:SerializedName("AddInfoStatus")
	var addInfoStatus: Int? = null,

	@field:SerializedName("AddInfoCreateDate")
	var addInfoCreateDate: String? = null,

	@field:SerializedName("AddInfoCreateBy")
	var addInfoCreateBy: Int? = null,

	@field:SerializedName("AddInfoId")
	var addInfoId: Int? = null,

	@field:SerializedName("AddInfoLogId")
	var addInfoLogId: Any? = null
){
	override fun toString(): String {
		return addInfoName.toString()
	}
}