package mki.siojt2.model.master_data_form_tanah

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataListPosisiTanahWithOutParam : RealmObject(){

	@SerializedName("PositionToRoadId")
	@Expose
	private var positionToRoadId: Int? = null

	@SerializedName("PositionToRoadName")
	@Expose
	private var positionToRoadName: String? = ""

	@SerializedName("PositionToRoadStatus")
	@Expose
	private var positionToRoadStatus: Int? = null

	fun getpositionToRoadId(): Int? {
		return positionToRoadId
	}

	fun setpositionToRoadId(positionToRoadId: Int?) {
		this.positionToRoadId = positionToRoadId
	}

	fun getpositionToRoadName(): String? {
		return positionToRoadName
	}

	fun setpositionToRoadName(positionToRoadName: String) {
		this.positionToRoadName = positionToRoadName
	}

	fun getpositionToRoadStatus(): Int? {
		return positionToRoadStatus
	}

	fun setpositionToRoadStatus(positionToRoadStatus: Int?) {
		this.positionToRoadStatus = positionToRoadStatus
	}


	override fun toString(): String {
		return positionToRoadName.toString()
	}
}