package mki.siojt2.model.master_data_form_tanah

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListPenguasaanTanah(

	@field:SerializedName("PartyTypeDescription")
	var partyTypeDescription: Any? = null,

	@field:SerializedName("PartyTypeStatus")
	var partyTypeStatus: Int? = null,

	@field:SerializedName("PartyTypeName")
	var partyTypeName: String? = null,

	@field:SerializedName("PartyTypeId")
	var partyTypeId: Int? = null,

	@field:SerializedName("PartyTypeCreateBy")
	var partyTypeCreateBy: Int? = null,

	@field:SerializedName("PartyTypeCreateDate")
	var partyTypeCreateDate: String? = null,

	@field:SerializedName("PartyTypeLogId")
	var partyTypeLogId: Any? = null
)