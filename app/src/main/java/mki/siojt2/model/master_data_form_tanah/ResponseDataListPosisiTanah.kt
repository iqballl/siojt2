package mki.siojt2.model.master_data_form_tanah

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListPosisiTanah(

	@field:SerializedName("PositionToRoadCreateDate")
	var positionToRoadCreateDate: String? = null,

	@field:SerializedName("PositionToRoadLogId")
	var positionToRoadLogId: Any? = null,

	@field:SerializedName("PositionToRoadDescription")
	var positionToRoadDescription: Any? = null,

	@field:SerializedName("PositionToRoadStatus")
	var positionToRoadStatus: Int? = null,

	@field:SerializedName("PositionToRoadCreateBy")
	var positionToRoadCreateBy: Int? = null,

	@field:SerializedName("PositionToRoadId")
	var positionToRoadId: Int? = null,

	@field:SerializedName("PositionToRoadName")
	var positionToRoadName: String? = null
)