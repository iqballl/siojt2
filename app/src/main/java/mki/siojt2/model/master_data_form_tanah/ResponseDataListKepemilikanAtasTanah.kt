package mki.siojt2.model.master_data_form_tanah

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListKepemilikanAtasTanah(

	@field:SerializedName("OwnershipDescription")
	var ownershipDescription: Any? = null,

	@field:SerializedName("OwnershipLogId")
	var ownershipLogId: Any? = null,

	@field:SerializedName("OwnershipCreateDate")
	var ownershipCreateDate: String? = null,

	@field:SerializedName("OwnershipId")
	var ownershipId: Int? = null,

	@field:SerializedName("OwnershipName")
	var ownershipName: String? = null,

	@field:SerializedName("OwnershipStatus")
	var ownershipStatus: Int? = null,

	@field:SerializedName("OwnershipCreateBy")
	var ownershipCreateBy: Int? = null
)