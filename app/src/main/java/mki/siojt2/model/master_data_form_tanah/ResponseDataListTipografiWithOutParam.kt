package mki.siojt2.model.master_data_form_tanah

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataListTipografiWithOutParam : RealmObject() {

    @SerializedName("TypographyId")
    @Expose
    private var typographyId: Int? = null

    @SerializedName("TypographyName")
    @Expose
    private var typographyName: String? = ""

    @SerializedName("TypographyStatus")
    @Expose
    private var typographyStatus: Int? = null

    fun getTypoGraphyId(): Int? {
        return typographyId
    }

    fun setTypoGraphyId(typographyId: Int?) {
        this.typographyId = typographyId
    }

    fun getTypoGraphyName(): String? {
        return typographyName
    }

    fun setTypoGraphyName(typographyName: String) {
        this.typographyName = typographyName
    }

    fun getTypoGraphyStatus(): Int? {
        return typographyStatus
    }

    fun setTypoGraphyStatus(typographyStatus: Int?) {
        this.typographyStatus = typographyStatus
    }

    override fun toString(): String {
        return typographyName.toString()
    }
}
