package mki.siojt2.model.master_data_form_tanah

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject


open class ResponseDataListPenguasaanTanahWithOutParam: RealmObject() {

    @SerializedName("PartyTypeId")
    @Expose
    private var partyTypeId: String? = null

    @SerializedName("PartyTypeName")
    @Expose
    private var partyTypeName: String? = ""

    @SerializedName("PartyTypeStatus")
    @Expose
    private var partyTypeStatus: Int? = null

    fun getpartyTypeId(): String? {
        return partyTypeId
    }

    fun setpartyTypeId(partyTypeId: String?) {
        this.partyTypeId = partyTypeId
    }

    fun getpartyTypeName(): String? {
        return partyTypeName
    }

    fun setpartyTypeName(partyTypeName: String) {
        this.partyTypeName = partyTypeName
    }

    fun getpartyTypeStatus(): Int? {
        return partyTypeStatus
    }

    fun setpartyTypeStatus(partyTypeStatus: Int?) {
        this.partyTypeStatus = partyTypeStatus
    }

    override fun toString(): String {
        return partyTypeName.toString()
    }
}
