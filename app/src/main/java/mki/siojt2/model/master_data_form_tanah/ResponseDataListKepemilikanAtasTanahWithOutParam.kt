package mki.siojt2.model.master_data_form_tanah

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataListKepemilikanAtasTanahWithOutParam : RealmObject() {

    @SerializedName("OwnershipId")
    @Expose
    private var ownerShipId: Int? = null

    @SerializedName("OwnershipName")
    @Expose
    private var ownerShipName: String? = ""

    @SerializedName("OwnershipStatus")
    @Expose
    private var ownerShipStatus: Int? = null


    fun getownerShipId(): Int? {
        return ownerShipId
    }

    fun setownerShipId(ownerShipId: Int?) {
        this.ownerShipId = ownerShipId
    }

    fun getownerShipName(): String? {
        return ownerShipName
    }

    fun setownerShipName(ownerShipName: String) {
        this.ownerShipName = ownerShipName
    }

    fun getownerShipStatus(): Int? {
        return ownerShipStatus
    }

    fun setshapeStatus(ownerShipStatus: Int?) {
        this.ownerShipStatus = ownerShipStatus
    }

    override fun toString(): String {
        return ownerShipName.toString()
    }
}