package mki.siojt2.model.master_data_form_tanah

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListBentukTanah(

	@field:SerializedName("ShapeId")
	var shapeId: Int? = null,

	@field:SerializedName("ShapeDescription")
	var shapeDescription: Any? = null,

	@field:SerializedName("ShapeName")
	var shapeName: String? = null,

	@field:SerializedName("ShapeStatus")
	var shapeStatus: Int? = null,

	@field:SerializedName("ShapeCreateDate")
	var shapeCreateDate: String? = null,

	@field:SerializedName("ShapeLogId")
	var shapeLogId: Any? = null,

	@field:SerializedName("ShapeCreateBy")
	var shapeCreateBy: Int? = null
)