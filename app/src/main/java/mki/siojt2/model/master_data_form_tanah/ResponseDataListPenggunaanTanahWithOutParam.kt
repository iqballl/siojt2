package mki.siojt2.model.master_data_form_tanah

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataListPenggunaanTanahWithOutParam : RealmObject() {

    @SerializedName("UtilizationId")
    @Expose
    private var utilizationId: Int? = null

    @SerializedName("UtilizationName")
    @Expose
    private var utilizationName: String? = ""

    @SerializedName("UtilizationStatus")
    @Expose
    private var utilizationStatus: Int? = null

    fun getutilizationId(): Int? {
        return utilizationId
    }

    fun setutilizationId(utilizationId: Int?) {
        this.utilizationId = utilizationId
    }

    fun getutilizationName(): String? {
        return utilizationName
    }

    fun setutilizationName(utilizationName: String) {
        this.utilizationName = utilizationName
    }

    fun getutilizationStatus(): Int? {
        return utilizationStatus
    }

    fun setutilizationStatus(utilizationStatus: Int?) {
        this.utilizationStatus = utilizationStatus
    }

    override fun toString(): String {
        return utilizationName.toString()
    }
}
