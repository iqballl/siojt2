package mki.siojt2.model.master_data_form_tanah

import com.google.gson.annotations.SerializedName

data class ResponseDataListYesNo (

    @field:SerializedName("YesNoId")
    var yesNoId: Int? = 0,

    @field:SerializedName("YesNoName")
    var yesNoName: String? = null

    ){
    override fun toString():String {
        return yesNoName.toString()
    }
}