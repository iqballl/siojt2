package mki.siojt2.model.master_data_form_tanah

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListPeruntukanTanah(

	@field:SerializedName("ZoningSpatialPlanCreateDate")
	var zoningSpatialPlanCreateDate: String? = null,

	@field:SerializedName("ZoningSpatialPlanDescription")
	var zoningSpatialPlanDescription: Any? = null,

	@field:SerializedName("ZoningSpatialPlanCreateBy")
	var zoningSpatialPlanCreateBy: Int? = null,

	@field:SerializedName("ZoningSpatialPlanStatus")
	var zoningSpatialPlanStatus: Int? = null,

	@field:SerializedName("ZoningSpatialPlanId")
	var zoningSpatialPlanId: Int? = null,

	@field:SerializedName("ZoningSpatialPlanLogId")
	var zoningSpatialPlanLogId: Any? = null,

	@field:SerializedName("ZoningSpatialPlanName")
	var zoningSpatialPlanName: String? = null
)