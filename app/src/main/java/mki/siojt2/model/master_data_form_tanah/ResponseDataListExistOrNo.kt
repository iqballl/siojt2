package mki.siojt2.model.master_data_form_tanah

import com.google.gson.annotations.SerializedName

data class ResponseDataListExistOrNo (

    @field:SerializedName("ExistOrNoId")
    var existOrNoId: Int? = 0,

    @field:SerializedName("ExistOrNoName")
    var existOrNoName: String? = null

    ){
    override fun toString():String {
        return existOrNoName.toString()
    }
}