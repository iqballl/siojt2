package mki.siojt2.model.master_data_form_tanah

import com.google.gson.annotations.SerializedName

data class ResponseDataListEaseOfReach (

    @field:SerializedName("EaseOfReachId")
    var easeOfReachId: Int? = 0,

    @field:SerializedName("EaseOfReachName")
    var easeOfReachName: String? = null

    ){
    override fun toString():String {
        return easeOfReachName.toString()
    }
}