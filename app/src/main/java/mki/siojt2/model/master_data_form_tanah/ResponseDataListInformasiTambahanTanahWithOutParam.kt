package mki.siojt2.model.master_data_form_tanah

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataListInformasiTambahanTanahWithOutParam : RealmObject(){

	@SerializedName("AddInfoId")
	@Expose
	private var addInfoId: Int? = null

	@SerializedName("AddInfoName")
	@Expose
	private var addInfoName: String? = ""

	@SerializedName("AddInfoStatus")
	@Expose
	private var addInfoStatus: Int? = null


	fun getaddInfoId(): Int? {
		return addInfoId
	}

	fun setaddInfoId(addInfoId: Int?) {
		this.addInfoId = addInfoId
	}

	fun getaddInfoName(): String? {
		return addInfoName
	}

	fun setshapeName(addInfoName: String) {
		this.addInfoName = addInfoName
	}

	fun getaddInfoStatus(): Int? {
		return addInfoStatus
	}

	fun setshapeStatus(addInfoStatus: Int?) {
		this.addInfoStatus = addInfoStatus
	}

	override fun toString(): String {
		return addInfoName.toString()
	}
}

