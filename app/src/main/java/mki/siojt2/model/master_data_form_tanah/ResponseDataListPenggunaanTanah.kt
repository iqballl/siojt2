package mki.siojt2.model.master_data_form_tanah

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListPenggunaanTanah(

	@field:SerializedName("UtilizationCreateDate")
	var utilizationCreateDate: String? = null,

	@field:SerializedName("UtilizationCreateBy")
	var utilizationCreateBy: Int? = null,

	@field:SerializedName("UtilizationDesctiption")
	var utilizationDesctiption: Any? = null,

	@field:SerializedName("UtilizationId")
	var utilizationId: Int? = null,

	@field:SerializedName("UtilizationName")
	var utilizationName: String? = null,

	@field:SerializedName("UtilizationStatus")
	var utilizationStatus: Int? = null,

	@field:SerializedName("UtilizationLogId")
	var utilizationLogId: Any? = null
){
	override fun toString(): String {
		return utilizationName.toString()
	}
}