package mki.siojt2.model.master_data_form_tanah

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class ResponseDataListBentukTanahWithOutParam : RealmObject() {

    @PrimaryKey
    @SerializedName("ShapeId")
    @Expose
    private var shapeId: Int? = null

    @SerializedName("ShapeName")
    @Expose
    private var shapeName: String? = ""

    @SerializedName("ShapeStatus")
    @Expose
    private var shapeStatus: Int? = null

    fun getshapeId(): Int? {
        return shapeId
    }

    fun setshapeId(shapeId: Int?) {
        this.shapeId = shapeId
    }

    fun getshapeName(): String? {
        return shapeName
    }

    fun setshapeName(shapeName: String) {
        this.shapeName = shapeName
    }

    fun geshapetStatus(): Int? {
        return shapeStatus
    }

    fun setshapeStatus(shapeStatus: Int?) {
        this.shapeStatus = shapeStatus
    }

    override fun toString(): String {
        return shapeName.toString()
    }
}
