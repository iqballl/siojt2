package mki.siojt2.model.master_data_form_tanah

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListTipografi(

	@field:SerializedName("TypographyName")
	var typographyName: String? = null,

    @field:SerializedName("TypographyCreateDate")
    var typographyCreateDate: String? = null,

	@field:SerializedName("TypographyDescription")
    var typographyDescription: Any? = null,

	@field:SerializedName("TypographyId")
    var typographyId: Int? = null,

	@field:SerializedName("TypographyCreateBy")
    var typographyCreateBy: Int? = null,

	@field:SerializedName("TypographyLogId")
    var typographyLogId: Any? = null,

	@field:SerializedName("TypographyStatus")
    var typographyStatus: Int? = null
)