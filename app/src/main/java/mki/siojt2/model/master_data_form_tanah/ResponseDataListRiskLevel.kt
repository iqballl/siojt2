package mki.siojt2.model.master_data_form_tanah

import com.google.gson.annotations.SerializedName

data class ResponseDataListRiskLevel (

    @field:SerializedName("RiskLevelId")
    var riskLevelId: Int? = 0,

    @field:SerializedName("RiskLevelName")
    var riskLevelName: String? = null

    ){
    override fun toString():String {
        return riskLevelName.toString()
    }
}