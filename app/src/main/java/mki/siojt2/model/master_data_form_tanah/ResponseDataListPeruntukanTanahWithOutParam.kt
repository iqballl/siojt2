package mki.siojt2.model.master_data_form_tanah

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataListPeruntukanTanahWithOutParam : RealmObject() {

    @SerializedName("ZoningSpatialPlanId")
    @Expose
    private var zoningSpatialPlanId: Int? = null

    @SerializedName("ZoningSpatialPlanName")
    @Expose
    private var zoningSpatialPlanName: String? = ""

    @SerializedName("ZoningSpatialPlanStatus")
    @Expose
    private var zoningSpatialPlanStatus: Int? = null

    fun getzoningSpatialPlanId(): Int? {
        return zoningSpatialPlanId
    }

    fun setzoningSpatialPlanId(zoningSpatialPlanId: Int?) {
        this.zoningSpatialPlanId = zoningSpatialPlanId
    }

    fun getzoningSpatialPlanName(): String? {
        return zoningSpatialPlanName
    }

    fun setzoningSpatialPlannName(zoningSpatialPlanName: String) {
        this.zoningSpatialPlanName = zoningSpatialPlanName
    }

    fun getzoningSpatialPlanStatus(): Int? {
        return zoningSpatialPlanStatus
    }

    fun setzoningSpatialPlanStatus(zoningSpatialPlanStatus: Int?) {
        this.zoningSpatialPlanStatus = zoningSpatialPlanStatus
    }

    override fun toString(): String {
        return zoningSpatialPlanName.toString()
    }
}
