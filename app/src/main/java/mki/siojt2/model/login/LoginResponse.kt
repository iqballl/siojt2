package mki.siojt2.model.login

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class LoginResponse(

	@field:SerializedName("DataUser")
	var dataUser: DataUser? = null
)