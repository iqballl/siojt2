package mki.siojt2.model.login

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class DataUser(

        @field:SerializedName("KantahDistrictCode")
        var kantahDistrictCode: String? = null,

        @field:SerializedName("MobileUserId")
        var mobileUserId: Int? = null,

        @field:SerializedName("MobileUserPassword")
        var mobileUserPassword: String? = null,

        @field:SerializedName("KantahLogId")
        var kantahLogId: Any? = null,

        @field:SerializedName("MobileUserLastLogin")
        var mobileUserLastLogin: Any? = null,

        @field:SerializedName("MobileUserAccessToken")
        var mobileUserAccessToken: String? = null,

        @field:SerializedName("MobileUserKantahId")
        var mobileUserKantahId: Int? = null,

        @field:SerializedName("ProvinceName")
        var provinceName: String? = null,

        @field:SerializedName("MobileUserCreateDate")
        var mobileUserCreateDate: String? = null,

        @field:SerializedName("MobileUserLogId")
        var mobileUserLogId: Any? = null,

        @field:SerializedName("MobileUserIsOnline")
        var mobileUserIsOnline: Any? = null,

        @field:SerializedName("KantahDesctiption")
        var kantahDesctiption: String? = null,

        @field:SerializedName("MobileUserName")
        var mobileUserName: String? = null,

        @field:SerializedName("KantahProvinceCode")
        var kantahProvinceCode: String? = null,

        @field:SerializedName("MobileUserIpAddress")
        var mobileUserIpAddress: Any? = null,

        @field:SerializedName("KantahName")
        var kantahName: String? = null,

        @field:SerializedName("KantahCreateBy")
        var kantahCreateBy: Int? = null,

        @field:SerializedName("MobileUserUpdateDate")
        var mobileUserUpdateDate: String? = null,

        @field:SerializedName("KantahStatus")
        var kantahStatus: Int? = null,

        @field:SerializedName("MobileUserCreateBy")
        var mobileUserCreateBy: Int? = null,

        @field:SerializedName("KantahId")
        var kantahId: Int? = null,

        @field:SerializedName("KantahCreateDate")
        var kantahCreateDate: String? = null,

        @field:SerializedName("MobileUserEmployeeName")
        var mobileUserEmployeeName: String? = null,

        @field:SerializedName("DistrictName")
        var districtName: String? = null,

        @field:SerializedName("VillageName")
        var villageName: String? = null,

        @field:SerializedName("MobileUserLastOnline")
        var mobileUserLastOnline: Any? = null,

        @field:SerializedName("MobileUserUpdateBy")
        var mobileUserUpdateBy: Int? = null,

        @field:SerializedName("KantahVillageCode")
        var kantahVillageCode: String? = null,

        @field:SerializedName("MobileUserEmployeeNIP")
        var mobileUserEmployeeNIP: String? = null,

        @field:SerializedName("MobileUserStatus")
        var mobileUserStatus: Int? = null,

        @field:SerializedName("RegencyName")
        var regencyName: String? = null,

        @field:SerializedName("KantahRegencyCode")
        var kantahRegencyCode: String? = null
)