package mki.siojt2.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class DataExpiredDialogSync(

        @field:SerializedName("DataExpiredDialogSyncStatus")
        var dtaExpiredDialogSyncStatus: Int? = null,

        @field:SerializedName("DataExpiredDialogSyncDate")
        var dataExpiredDialogSyncDate: Date? = null
)