package mki.siojt2.model

import android.graphics.Path


class FingerPath(color: Int, emboss: Boolean, blur: Boolean, strokeWidth: Int, path: Path) {
    var mColor: Int = 0
    var mEmboss: Boolean = false
    var mBlur: Boolean = false
    var mStrokeWidth: Int = 0
    var mPath: Path

    init {
        this.mColor = color
        this.mEmboss = emboss
        this.mBlur = blur
        this.mStrokeWidth = strokeWidth
        this.mPath = path
    }
}