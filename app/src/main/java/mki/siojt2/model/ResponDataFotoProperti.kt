package mki.siojt2.model

data class ResponDataFotoProperti(
    var imageName:String? = null,
    var imageUrl:String? = null
)