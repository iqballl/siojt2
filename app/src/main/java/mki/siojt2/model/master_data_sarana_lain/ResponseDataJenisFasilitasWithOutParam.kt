package mki.siojt2.model.master_data_sarana_lain

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataJenisFasilitasWithOutParam : RealmObject(){

	@SerializedName("FacilityTypeId")
	@Expose
	private var facilityTypeId: Int? = null

	@SerializedName("FacilityTypeName")
	@Expose
	private var facilityTypeName: String? = ""

	@SerializedName("FacilityTypeStatus")
	@Expose
	private var facilityTypeStatus: Int? = null

	fun getfacilityTypeId(): Int? {
		return facilityTypeId
	}

	fun setfacilityTypeId(facilityTypeId: Int?) {
		this.facilityTypeId = facilityTypeId
	}

	fun getfacilityTypeName(): String? {
		return facilityTypeName
	}

	fun setfacilityTypeName(facilityTypeName: String) {
		this.facilityTypeName = facilityTypeName
	}

	fun gefacilityTypeStatus(): Int? {
		return facilityTypeStatus
	}

	fun setfacilityTypeStatus(facilityTypeStatus: Int?) {
		this.facilityTypeStatus = facilityTypeStatus
	}

	override fun toString(): String {
		return facilityTypeName.toString()
	}
}