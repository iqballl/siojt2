package mki.siojt2.model.master_data_sarana_lain

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataJenisFasilitas(

	@field:SerializedName("FacilityTypeStatus")
	var facilityTypeStatus: Int? = null,

	@field:SerializedName("FacilityTypeCreateDate")
	var facilityTypeCreateDate: String? = null,

	@field:SerializedName("FacilityTypeName")
	var facilityTypeName: String? = null,

	@field:SerializedName("FacilityTypeCreateBy")
	var facilityTypeCreateBy: Int? = null,

	@field:SerializedName("FacilityTypeId")
	var facilityTypeId: Int? = null,

	@field:SerializedName("FacilityTypeDescription")
	var facilityTypeDescription: Any? = null,

	@field:SerializedName("FacilityTypeLogId")
	var facilityTypeLogId: Any? = null
){
	override fun toString(): String {
		return facilityTypeName.toString()
	}
}