package mki.siojt2.model.master_data_sarana_lain

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataJenisPagarKelilingWithOutParam: RealmObject(){

	@SerializedName("CircumferentialFenceId")
	@Expose
	private var circumFerentialFenceId: Int? = null

	@SerializedName("CircumferentialFenceName")
	@Expose
	private var circumFerentialFenceName: String? = ""

	@SerializedName("CircumferentialFenceStatus")
	@Expose
	private var circumFerentialFenceStatus: Int? = null

	fun getcircumFerentialFenceId(): Int? {
		return circumFerentialFenceId
	}

	fun setcircumFerentialFenceId(circumFerentialFenceId: Int?) {
		this.circumFerentialFenceId = circumFerentialFenceId
	}

	fun getcircumFerentialFenceName(): String? {
		return circumFerentialFenceName
	}

	fun setcircumFerentialFenceName(circumFerentialFenceName: String) {
		this.circumFerentialFenceName = circumFerentialFenceName
	}

	fun gecircumFerentialFenceStatus(): Int? {
		return circumFerentialFenceStatus
	}

	fun setcircumFerentialFenceStatus(circumFerentialFenceStatus: Int?) {
		this.circumFerentialFenceStatus = circumFerentialFenceStatus
	}

	override fun toString(): String {
		return circumFerentialFenceName.toString()
	}
}
