package mki.siojt2.model.master_data_sarana_lain

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataJenisPagarKeliling(

	@field:SerializedName("CircumferentialFenceName")
	var circumferentialFenceName: String? = null,

	@field:SerializedName("CircumferentialFenceCreateDate")
	var circumferentialFenceCreateDate: String? = null,

	@field:SerializedName("CircumferentialFenceLogId")
	var circumferentialFenceLogId: Any? = null,

	@field:SerializedName("CircumferentialFenceStatus")
	var circumferentialFenceStatus: Int? = null,

	@field:SerializedName("CircumferentialFenceId")
	var circumferentialFenceId: Int? = null,

	@field:SerializedName("CircumferentialFenceDescription")
	var circumferentialFenceDescription: Any? = null,

	@field:SerializedName("CircumferentialFenceCreateBy")
	var circumferentialFenceCreateBy: Int? = null
){
	override fun toString(): String {
		return circumferentialFenceName.toString()
	}
}