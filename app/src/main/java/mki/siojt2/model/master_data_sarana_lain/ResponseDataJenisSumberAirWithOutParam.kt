package mki.siojt2.model.master_data_sarana_lain

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataJenisSumberAirWithOutParam: RealmObject() {

    @SerializedName("CleanWaterSourceId")
    @Expose
    private var cleanWaterId: Int? = null

    @SerializedName("CleanWaterSourceName")
    @Expose
    private var cleanWaterName: String? = ""

    @SerializedName("CleanWaterSourceStatus")
    @Expose
    private var cleanWaterStatus: Int? = null

    fun getcleanWaterId(): Int? {
        return cleanWaterId
    }

    fun setcleanWaterId(cleanWaterId: Int?) {
        this.cleanWaterId = cleanWaterId
    }

    fun getcleanWaterName(): String? {
        return cleanWaterName
    }

    fun setcleanWaterName(cleanWaterName: String) {
        this.cleanWaterName = cleanWaterName
    }

    fun gecleanWaterStatus(): Int? {
        return cleanWaterStatus
    }

    fun setcleanWaterStatus(cleanWaterStatus: Int?) {
        this.cleanWaterStatus = cleanWaterStatus
    }

    override fun toString(): String {
        return cleanWaterName.toString()
    }
}