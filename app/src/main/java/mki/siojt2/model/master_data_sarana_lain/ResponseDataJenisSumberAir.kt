package mki.siojt2.model.master_data_sarana_lain

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataJenisSumberAir(

	@field:SerializedName("CleanWaterSourceId")
	var cleanWaterSourceId: Int? = null,

	@field:SerializedName("CleanWaterSourceDescription")
	var cleanWaterSourceDescription: Any? = null,

	@field:SerializedName("CleanWaterSourceStatus")
	var cleanWaterSourceStatus: Int? = null,

	@field:SerializedName("CleanWaterSourceLogId")
	var cleanWaterSourceLogId: Any? = null,

	@field:SerializedName("CleanWaterSourceCreateBy")
	var cleanWaterSourceCreateBy: Int? = null,

	@field:SerializedName("CleanWaterSourceCreateDate")
	var cleanWaterSourceCreateDate: String? = null,

	@field:SerializedName("CleanWaterSourceName")
	var cleanWaterSourceName: String? = null
){
	override fun toString(): String {
		return cleanWaterSourceName.toString()
	}
}