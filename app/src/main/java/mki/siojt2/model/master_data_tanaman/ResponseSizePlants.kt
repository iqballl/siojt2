package mki.siojt2.model.master_data_tanaman

import com.google.gson.annotations.SerializedName

data class ResponseSizePlants (
        @field:SerializedName("PlantSizeId")
        var plantSizeId: Int? = null,

        @field:SerializedName("PlantSizeName")
        var plantSizeName: String? = null
){
    override fun toString(): String {
        return plantSizeName.toString()
    }
}