package mki.siojt2.model.master_data_tanaman

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListKategoriTanaman(

	@field:SerializedName("PlantCreateDate")
	var plantCreateDate: String? = null,

	@field:SerializedName("PlantLogId")
	var plantLogId: Any? = null,

	@field:SerializedName("PlantStatus")
	var plantStatus: Int? = null,

	@field:SerializedName("PlantCreateBy")
	var plantCreateBy: Int? = null,

	@field:SerializedName("PlantType")
	var plantType: Int? = null,

	@field:SerializedName("PlantDescription")
	var plantDescription: String? = null,

	@field:SerializedName("PlantName")
	var plantName: String? = null,

	@field:SerializedName("PlantId")
	var plantId: Int? = null
){
	override fun toString(): String {
		return plantName.toString()
	}
}