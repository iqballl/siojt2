package mki.siojt2.model.master_data_tanaman

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataListKategoriTanamanWithOutParam: RealmObject() {

    @SerializedName("PlantId")
    @Expose
    private var plantId: Int? = null

    @SerializedName("PlantName")
    @Expose
    private var plantName: String? = ""

    @SerializedName("PlantDescription")
    @Expose
    private var plantDescription: String? = ""

    @SerializedName("PlantStatus")
    @Expose
    private var plantStatus: Int? = null

    fun getplantId(): Int? {
        return plantId
    }

    fun setplantId(plantId: Int?) {
        this.plantId = plantId
    }

    fun getplantName(): String? {
        return plantName
    }

    fun setplantName(plantName: String) {
        this.plantName = plantName
    }

    fun getplantDescription(): String? {
        return plantDescription
    }

    fun setplantDescription(plantDescription: String) {
        this.plantDescription = plantDescription
    }

    fun getplantStatus(): Int? {
        return plantStatus
    }

    fun setplantStatus(plantStatus: Int?) {
        this.plantStatus = plantStatus
    }

    override fun toString(): String {
        return plantName.toString()
    }

}
