package mki.siojt2.model.master_data_tanaman

import com.google.gson.annotations.SerializedName

data class ResponseJenisTanamanWithOutParam (
        @field:SerializedName("PlantTypeId")
        var plantTypeId: Int? = null,

        @field:SerializedName("PlantTypeName")
        var plantTypeName: String? = null
){
    override fun toString(): String {
        return plantTypeName.toString()
    }
}