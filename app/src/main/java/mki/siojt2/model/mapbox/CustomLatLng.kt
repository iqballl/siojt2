package mki.siojt2.model.mapbox

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

@Generated("com.robohorse.robopojogenerator")
open class CustomLatLng(
	var lat: Double? = 0.0,
	var lon: Double? = 0.0
): RealmObject()