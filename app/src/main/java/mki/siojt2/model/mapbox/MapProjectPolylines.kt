package mki.siojt2.model.mapbox

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import com.mapbox.geojson.Feature
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import io.realm.RealmObject
import mki.siojt2.model.UAVObjectsItem
import mki.siojt2.model.UAVProject
import mki.siojt2.model.localsave.*

@Generated("com.robohorse.robopojogenerator")
open class MapProjectPolylines(
		var id: Int? = 0,
		var fillLayerPointList: MutableList<Point> = arrayListOf(),
		var lineLayerPointList: MutableList<Point> = arrayListOf(),
		var circleLayerFeatureList: MutableList<Feature> = arrayListOf(),
		var listOfList: MutableList<MutableList<Point>> = arrayListOf(),
		var circleSource: GeoJsonSource? = null,
		var fillSource: GeoJsonSource? = null,
		var lineSource: GeoJsonSource? = null,
		var CIRCLE_SOURCE_ID: String = "",
		var FILL_SOURCE_ID: String  = "",
		var LINE_SOURCE_ID: String  = "",
		var CIRCLE_LAYER_ID: String  = "",
		var FILL_LAYER_ID: String  = "",
		var LINE_LAYER_ID: String  = "",
		var coordinat: List<List<Point>> = arrayListOf(),
		var firstPointOfPolygon: Point? = null,
		var type: String? = null,
		var uavProject: UAVProject? = null,
		var uavObject: UAVObjectsItem? = null,
		var landData: ProjectLand? = null,
		var buildingData: Bangunan? = null,
		var objectType: Int? = null, //1 tanah 2 bangunan
		var owner: PartyAdd? = null,
		var infrastructure: SaranaLain? = null,
		var plant: Tanaman? = null
)