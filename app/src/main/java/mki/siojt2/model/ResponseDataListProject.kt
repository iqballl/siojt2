package mki.siojt2.model

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListProject(

	@field:SerializedName("ProjectInstansiName")
	var projectInstansiName: String? = null,

	@field:SerializedName("ProjectNilaiAnggaran")
	var projectNilaiAnggaran: Double? = null,

	@field:SerializedName("ProjectName")
	var projectName: String? = null,

	@field:SerializedName("ProjectPanjang")
	var projectPanjang: Int? = null,

	@field:SerializedName("ProjectAssignId")
	var projectAssignId: Int? = null,

	@field:SerializedName("ProjectNamaPemohon")
	var projectNamaPemohon: String? = null,

	@field:SerializedName("ProjectSumberAnggaran")
	var projectSumberAnggaran: String? = null,

	@field:SerializedName("ProjectJangkaWaktu")
	var projectJangkaWaktu: Int? = null,

	@field:SerializedName("ProjectJumlahBidang")
	var projectJumlahBidang: Int? = null,

	@field:SerializedName("ProjectCreateDate")
	var projectCreateDate: String? = null,

	@field:SerializedName("ProjectAssignCreateBy")
	var projectAssignCreateBy: Int? = null,

	@field:SerializedName("ProjectAssignStatus")
	var projectAssignStatus: Int? = null,

	@field:SerializedName("ProjectAssignCreateDate")
	var projectAssignCreateDate: String? = null,

	@field:SerializedName("ProjectAssignSyncStatus")
	var projectAssignSyncStatus: Int? = null,

	@field:SerializedName("ProjectAssignMobileUserId")
	var projectAssignMobileUserId: Int? = null,

	@field:SerializedName("ProjectStatus")
	var projectStatus: Int? = null,

	@field:SerializedName("ProjectTanggalPermohonan")
	var projectTanggalPermohonan: String? = null,

	@field:SerializedName("ProjectLuas")
	var projectLuas: Int? = null,

	@field:SerializedName("ProjectAssignLogId")
	var projectAssignLogId: Any? = null,

	@field:SerializedName("ProjectAssignProjectId")
	var projectAssignProjectId: Int? = null,

	@field:SerializedName("ProjectDescription")
	var projectDescription: String? = null,

	@field:SerializedName("ProjectCreateBy")
	var projectCreateBy: Int? = null,

	@field:SerializedName("ProjectJabatanPemohon")
	var projectJabatanPemohon: String? = null
)