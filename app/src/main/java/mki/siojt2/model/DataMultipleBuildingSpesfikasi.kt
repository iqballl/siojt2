package mki.siojt2.model

import com.google.gson.annotations.SerializedName

data class DataMultipleBuildingSpesfikasi(

        @field:SerializedName("BuildingSpesfikasiId")
        var buildingSpesfikasiId: String? = "",

        @field:SerializedName("buildingSpesfikasiValue")
        var buildingSpesfikasiValue: String? = ""


)