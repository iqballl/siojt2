package mki.siojt2.model

import com.google.gson.annotations.SerializedName

data class DataMultiplePagerKeliling(

        @field:SerializedName("POtherFacilitiyName")
        var pOtherFacilitiyName: String? = "",

        @field:SerializedName("POtherFacilitiyValue")
        var pOtherFacilitiyValue: String? = ""


)