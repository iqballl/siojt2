package mki.siojt2.model.data_form_objek_bangunan

import com.google.gson.annotations.SerializedName

data class DataFormObjekBangunan2(

        @field:SerializedName("FoundationTypeId")
        var foundationTypeId: ArrayList<String>? = null,

        @field:SerializedName("FoundationTypeName")
        var foundationTypeName: ArrayList<String>? = null,

        @field:SerializedName("FoundationTypeNameOther")
        var foundationTypeNameOther: ArrayList<String>? = null,

        @field:SerializedName("PBFoundationTypeAreaTotal")
        var pbFoundationTypeAreaTotal: ArrayList<Float>? = null,

        @field:SerializedName("StructureTypeId")
        var structureTypeId: ArrayList<String>? = null,

        @field:SerializedName("StructureTypeName")
        var structureTypeName: ArrayList<String>? = null,

        @field:SerializedName("StructureTypeNameOther")
        var structureTypeNameOther: ArrayList<String>? = null,

        @field:SerializedName("PBStructureTypeAreaTotal")
        var pbStructureTypeAreaTotal:  ArrayList<Float>? = null,

        @field:SerializedName("RoofFrameTypeId")
        var roofFrameTypeId: ArrayList<String>? = null,

        @field:SerializedName("RoofFrameTypeName")
        var roofFrameTypeName: ArrayList<String>? = null,

        @field:SerializedName("RoofFrameTypeNameOther")
        var roofFrameTypeNameOther: ArrayList<String>? = null,

        @field:SerializedName("PBRoofFrameTypeAreaTotal")
        var pbRoofFrameTypeAreaTotal:  ArrayList<Float>? = null,

        @field:SerializedName("RoofCoveringTypeId")
        var roofCoveringTypeId: ArrayList<String>? = null,

        @field:SerializedName("RoofCoveringTypeName")
        var roofCoveringTypeName: ArrayList<String>? = null,

        @field:SerializedName("RoofCoveringTypeNameOther")
        var roofCoveringTypeNameOther: ArrayList<String>? = null,

        @field:SerializedName("PBRoofCoveringTypeAreaTotal")
        var pbRoofCoveringTypeAreaTotal:  ArrayList<Float>? = null,

        @field:SerializedName("CeilingTypeId")
        var ceilingTypeId: ArrayList<String>? = null,

        @field:SerializedName("CeilingTypeName")
        var ceilingTypeName: ArrayList<String>? = null,

        @field:SerializedName("CeilingTypeNameOther")
        var ceilingTypeNameOther: ArrayList<String>? = null,

        @field:SerializedName("PBCeilingTypeAreaTotal")
        var pbCeilingTypeAreaTotal:  ArrayList<Float>? = null,

        @field:SerializedName("DoorWindowTypeld")
        var doorWindowTypeld: ArrayList<String>? = null,

        @field:SerializedName("DoorWindowTypeName")
        var doorWindowTypeName: ArrayList<String>? = null,

        @field:SerializedName("DoorWindowTypeNameOther")
        var doorWindowTypeNameOther: ArrayList<String>? = null,

        @field:SerializedName("PBDoorWindowTypeAreaTotal")
        var pbDoorWindowTypeAreaTotal:  ArrayList<Float>? = null,

        @field:SerializedName("FloorTypeId")
        var floorTypeId: ArrayList<String>? = null,

        @field:SerializedName("FloorTypeName")
        var floorTypeName: ArrayList<String>? = null,

        @field:SerializedName("FloorTypeNameOther")
        var floorTypeNameOther: ArrayList<String>? = null,

        @field:SerializedName("PBFloorTypeAreaTotal")
        var pbFloorTypeAreaTotal:  ArrayList<Float>? = null,

        @field:SerializedName("WallTypeId")
        var wallTypeId: ArrayList<String>? = null,

        @field:SerializedName("WallTypeName")
        var wallTypeName: ArrayList<String>? = null,

        @field:SerializedName("WallTypeNameOther")
        var wallTypeNameOther: ArrayList<String>? = null,

        @field:SerializedName("PBFWallTypeAreaTotal")
        var pbFWallTypeAreaTotal:  ArrayList<Float>? = null

)