package mki.siojt2.model.data_form_objek_bangunan

import com.google.gson.annotations.SerializedName

data class DataStruktur(

        @field:SerializedName("StructureTypeId")
        var structureTypeId: String? = "",

        @field:SerializedName("StructureTypeName")
        var structureTypeName: String? = "",

        @field:SerializedName("StructureTypeNameOther")
        var structureTypeNameOther: String? = "",

        @field:SerializedName("PBStructureTypeAreaTotal")
        var pbStructureTypeAreaTotal: Float? = 0F


)