package mki.siojt2.model.data_form_objek_bangunan

import com.google.gson.annotations.SerializedName

data class DataLantai(

        @field:SerializedName("FloorTypeId")
        var floorTypeId: String? = "",

        @field:SerializedName("FloorTypeName")
        var floorTypeName: String? = "",

        @field:SerializedName("FloorTypeNameOther")
        var floorTypeNameOther: String? = "",

        @field:SerializedName("PBFloorTypeAreaTotal")
        var pbFloorTypeAreaTotal: Float? = 0F


)