package mki.siojt2.model.data_form_objek_bangunan

import com.google.gson.annotations.SerializedName

data class DataPondasi(

        @field:SerializedName("FoundationTypeId")
        var foundationTypeId: String? = "",

        @field:SerializedName("FoundationTypeName")
        var foundationTypeName: String? = "",

        @field:SerializedName("FoundationTypeNameOther")
        var foundationTypeNameOther: String? = "",

        @field:SerializedName("PBFoundationTypeAreaTotal")
        var pbFoundationTypeAreaTotal: Float? = 0F


)