package mki.siojt2.model.data_form_objek_bangunan

import com.google.gson.annotations.SerializedName

data class DataPintuJendela(

        @field:SerializedName("DoorWindowTypeld")
        var doorWindowTypeld: String? = "",

        @field:SerializedName("DoorWindowTypeName")
        var doorWindowTypeName: String? = "",

        @field:SerializedName("DoorWindowTypeNameOther")
        var doorWindowTypeNameOther: String? = "",

        @field:SerializedName("PBDoorWindowTypeAreaTotal")
        var pbDoorWindowTypeAreaTotal: Float? = 0F


)