package mki.siojt2.model.data_form_objek_bangunan

import com.google.gson.annotations.SerializedName
import mki.siojt2.model.local_save_image.DataImageBuilding
import mki.siojt2.model.local_save_image.DataImageLand

data class DataFormBangunanImage(
        @field:SerializedName("DataImagePhoto")
        var dataImagePhoto: List<DataImageBuilding>? = null,
        var notes: String?
)