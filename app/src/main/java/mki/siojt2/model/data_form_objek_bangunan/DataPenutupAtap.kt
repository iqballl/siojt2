package mki.siojt2.model.data_form_objek_bangunan

import com.google.gson.annotations.SerializedName

data class DataPenutupAtap(

        @field:SerializedName("RoofCoveringTypeId")
        var roofCoveringTypeId: String? = "",

        @field:SerializedName("RoofCoveringTypeName")
        var roofCoveringTypeName: String? = "",

        @field:SerializedName("RoofCoveringTypeNameOther")
        var roofCoveringTypeNameOther: String? = "",

        @field:SerializedName("PBRoofCoveringTypeAreaTotal")
        var pbRoofCoveringTypeAreaTotal: Float? = 0F


)