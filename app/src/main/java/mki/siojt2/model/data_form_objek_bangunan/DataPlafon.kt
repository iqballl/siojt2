package mki.siojt2.model.data_form_objek_bangunan

import com.google.gson.annotations.SerializedName

data class DataPlafon(

        @field:SerializedName("CeilingTypeId")
        var ceilingTypeId: String? = "",

        @field:SerializedName("CeilingTypeName")
        var ceilingTypeName: String? = "",

        @field:SerializedName("CeilingTypeNameOther")
        var ceilingTypeNameOther: String? = "",

        @field:SerializedName("PBCeilingTypeAreaTotal")
        var pbCeilingTypeAreaTotal: Float? = 0F


)