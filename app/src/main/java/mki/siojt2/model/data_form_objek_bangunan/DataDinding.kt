package mki.siojt2.model.data_form_objek_bangunan

import com.google.gson.annotations.SerializedName

data class DataDinding(

        @field:SerializedName("WallTypeId")
        var wallTypeId: String? = "",

        @field:SerializedName("WallTypeTypeName")
        var wallTypeTypeName: String? = "",

        @field:SerializedName("WallTypeNameOther")
        var wallTypeNameOther: String? = "",

        @field:SerializedName("PBFWallTypeAreaTotal")
        var pbFWallTypeAreaTotal: Float? = 0F


)