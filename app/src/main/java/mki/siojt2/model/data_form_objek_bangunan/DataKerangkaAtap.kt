package mki.siojt2.model.data_form_objek_bangunan

import com.google.gson.annotations.SerializedName

data class DataKerangkaAtap(

        @field:SerializedName("RoofFrameTypeId")
        var roofFrameTypeId: String? = "",

        @field:SerializedName("RoofFrameTypeName")
        var roofFrameTypeName: String? = "",

        @field:SerializedName("RoofFrameTypeNameOther")
        var roofFrameTypeNameOther: String? = "",

        @field:SerializedName("PBRoofFrameTypeAreaTotal")
        var pbRoofFrameTypeAreaTotal: Float? = 0F


)