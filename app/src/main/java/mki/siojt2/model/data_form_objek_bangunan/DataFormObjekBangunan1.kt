package mki.siojt2.model.data_form_objek_bangunan

import com.google.gson.annotations.SerializedName

data class DataFormObjekBangunan1(

        @field:SerializedName("ProjectPartyId")
        var projectPartyId: String? = "",

        @field:SerializedName("ProjectPartyName")
        var projectPartyName: String? = "",

        @field:SerializedName("ProjectBuildingTypeId")
        var projectBuildingTypeId: Int? = 0,

        @field:SerializedName("ProjectBuildingTypeName")
        var projectBuildingTypeName: String? = "",

        @field:SerializedName("ProjectBuildingYearOfBuilt")
        var projectBuildingYearOfBuilt: String? = "",

        @field:SerializedName("ProjectBuildingTypeNameOther")
        var projectBuildingTypeNameOther: String? = "",

        @field:SerializedName("ProjectBuildingFloorTotal")
        var projectBuildingFloorTotal: Int? = 0,

        @field:SerializedName("ProjectBuildingAreaUpperRoom")
        var projectBuildingAreaUpperRoom: String? = "",

        @field:SerializedName("ProjectBuildingAreaLowerRoom")
        var projectBuildingAreaLowerRoom: String? = ""

)