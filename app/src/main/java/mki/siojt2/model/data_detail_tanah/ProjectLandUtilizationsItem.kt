package mki.siojt2.model.data_detail_tanah

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ProjectLandUtilizationsItem(

	@field:SerializedName("ProjectLandUtilizationStatus")
	var projectLandUtilizationStatus: Int? = null,

	@field:SerializedName("ProjectLandUtilizationCreateBy")
	var projectLandUtilizationCreateBy: Int? = null,

	@field:SerializedName("UtilizationCreateDate")
	var utilizationCreateDate: String? = null,

	@field:SerializedName("UtilizationDesctiption")
	var utilizationDesctiption: Any? = null,

	@field:SerializedName("UtilizationId")
	var utilizationId: Int? = null,

	@field:SerializedName("ProjectLandUtilizationCreateDate")
	var projectLandUtilizationCreateDate: String? = null,

	@field:SerializedName("UtilizationStatus")
	var utilizationStatus: Int? = null,

	@field:SerializedName("UtilizationLogId")
	var utilizationLogId: Any? = null,

	@field:SerializedName("UtilizationCreateBy")
	var utilizationCreateBy: Int? = null,

	@field:SerializedName("ProjectLandUtilizationId")
	var projectLandUtilizationId: Int? = null,

	@field:SerializedName("UtilizationName")
	var utilizationName: String? = null,

	@field:SerializedName("ProjectLandUtilizationLandUtilizationId")
	var projectLandUtilizationLandUtilizationId: Int? = null,

	@field:SerializedName("ProjectLandUtilizationLandId")
	var projectLandUtilizationLandId: Int? = null,

	@field:SerializedName("ProjectLandUtilizationLogId")
	var projectLandUtilizationLogId: Any? = null
)