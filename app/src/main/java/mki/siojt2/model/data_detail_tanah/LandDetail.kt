package mki.siojt2.model.data_detail_tanah

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class LandDetail(

	@field:SerializedName("ProjectLandCemeteryPresenceStatus")
	var projectLandCemeteryPresenceStatus: String? = null,

	@field:SerializedName("ProjectPartyCreateBy")
	var projectPartyCreateBy: Int? = null,

	@field:SerializedName("ProjectLandRT")
	var projectLandRT: String? = null,

	@field:SerializedName("ProjectLandDateOfIssue")
	var projectLandDateOfIssue: String? = null,

	@field:SerializedName("ProjectLandRW")
	var projectLandRW: String? = null,

	@field:SerializedName("TypographyName")
	var typographyName: String? = null,

	@field:SerializedName("ShapeName")
	var shapeName: String? = null,

	@field:SerializedName("ProjectLandNIB")
	var projectLandNIB: String? = null,

	@field:SerializedName("ProjectLandCreateBy")
	var projectLandCreateBy: Int? = null,

	@field:SerializedName("ProjectLandEaseToEducationFacilities")
	var projectLandEaseToEducationFacilities: Int? = null,

	@field:SerializedName("ZoningSpatialPlanName")
	var zoningSpatialPlanName: String? = null,

	@field:SerializedName("ProjectLandEaseToProperty")
	var projectLandEaseToProperty: Int? = null,

	@field:SerializedName("ProjectLandEaseToShopingCenter")
	var projectLandEaseToShopingCenter: Int? = null,

	@field:SerializedName("OwnershipName")
	var ownershipName: String? = null,

	@field:SerializedName("ProjectLandSecurityAgainstNaturalDisasters")
	var projectLandSecurityAgainstNaturalDisasters: Int? = null,

	@field:SerializedName("ProjectLandPositionToRoadId")
	var projectLandPositionToRoadId: Int? = null,

	@field:SerializedName("ProjectLandOrientationSoutheast")
	var projectLandOrientationSoutheast: String? = null,

	@field:SerializedName("ProjectPartyProjectId")
	var projectPartyProjectId: Int? = null,

	@field:SerializedName("ProjectLandStreetName")
	var projectLandStreetName: String? = null,

	@field:SerializedName("ProjectLandAvailableGasPipelineNetworkkStatus")
	var projectLandAvailableGasPipelineNetworkkStatus: String? = null,

	@field:SerializedName("ProjectLandPostalCode")
	var projectLandPostalCode: String? = null,

	@field:SerializedName("ProjectLandSUTETPresence")
	var projectLandSUTETPresence: Int? = null,

	@field:SerializedName("ProjectPartyId")
	var projectPartyId: Int? = null,

	@field:SerializedName("ProjectLandWideFrontRoad")
	var projectLandWideFrontRoad: Double? = null,

	@field:SerializedName("ProjectLandSkewerPresenceStatus")
	var projectLandSkewerPresenceStatus: String? = null,

	@field:SerializedName("ProjectLandEaseToTouristSites")
	var projectLandEaseToTouristSites: Int? = null,

	@field:SerializedName("ProjectLandDistrictCode")
	var projectLandDistrictCode: String? = null,

	@field:SerializedName("ProjectLandStatus")
	var projectLandStatus: Int? = null,

	@field:SerializedName("ProjectLandLangitude")
	var projectLandLangitude: String? = null,

	@field:SerializedName("ProjectLandCemeteryDistance")
	var projectLandCemeteryDistance: Double? = null,

	@field:SerializedName("ProjectLandOrientationNorthwest")
	var projectLandOrientationNorthwest: String? = null,

	@field:SerializedName("ProjectLandFrontage")
	var projectLandFrontage: Double? = null,

	@field:SerializedName("ProjectLandOrientationNorth")
	var projectLandOrientationNorth: String? = null,

	@field:SerializedName("ProjectLandAvailableGasPipelineNetwork")
	var projectLandAvailableGasPipelineNetwork: Int? = null,

	@field:SerializedName("RegencyName")
	var regencyName: String? = null,

	@field:SerializedName("ProjectLandAvailableTelephoneNetworkStatus")
	var projectLandAvailableTelephoneNetworkStatus: String? = null,

	@field:SerializedName("ProjectLandLatitude")
	var projectLandLatitude: String? = null,

	@field:SerializedName("ProjectLandAreaAffected")
	var projectLandAreaAffected: String? = null,

	@field:SerializedName("PositionToRoadName")
	var positionToRoadName: String? = null,

	@field:SerializedName("ProjectLandOwnershipId")
	var projectLandOwnershipId: Int? = null,

	@field:SerializedName("ProjectLandZoningSpatialPlanId")
	var projectLandZoningSpatialPlanId: Int? = null,

	@field:SerializedName("ProjectLandOrientationEast")
	var projectLandOrientationEast: String? = null,

	@field:SerializedName("ProjectPartyType")
	var projectPartyType: Int? = null,

	@field:SerializedName("PartyFullName")
	var partyFullName: String? = null,

	@field:SerializedName("ProjectLandShapeId")
	var projectLandShapeId: Int? = null,

	@field:SerializedName("ProjectLandOrientationSouth")
	var projectLandOrientationSouth: String? = null,

	@field:SerializedName("ProjectLandProjectPartyId")
	var projectLandProjectPartyId: Int? = null,

	@field:SerializedName("ProjectLandLogId")
	var projectLandLogId: Any? = null,

	@field:SerializedName("ProjectPartyPartyId")
	var projectPartyPartyId: Int? = null,

	@field:SerializedName("ProjectLandRegencyCode")
	var projectLandRegencyCode: String? = null,

	@field:SerializedName("ProjectLandHeightToRoad")
	var projectLandHeightToRoad: String? = null,

	@field:SerializedName("ProjectPartyLogId")
	var projectPartyLogId: Any? = null,

	@field:SerializedName("ProvinceName")
	var provinceName: String? = null,

	@field:SerializedName("ProjectLandSUTETDistance")
	var projectLandSUTETDistance: Double? = null,

	@field:SerializedName("ProjectLandSkewerPresence")
	var projectLandSkewerPresence: Int? = null,

	@field:SerializedName("ProjectLandOrientationSouthwest")
	var projectLandOrientationSouthwest: String? = null,

	@field:SerializedName("ProjectLandAvailableElectricalGridStatus")
	var projectLandAvailableElectricalGridStatus: String? = null,

	@field:SerializedName("ProjectLandAvailableTelephoneNetwork")
	var projectLandAvailableTelephoneNetwork: Int? = null,

	@field:SerializedName("ProjectLandSafetyAgainstFireHazards")
	var projectLandSafetyAgainstFireHazards: Int? = null,

	@field:SerializedName("ProjectLandComplexName")
	var projectLandComplexName: String? = null,

	@field:SerializedName("ProjectLandAvailableCleanWaterSystem")
	var projectLandAvailableCleanWaterSystem: Int? = null,

	@field:SerializedName("ProjectLandNumber")
	var projectLandNumber: Int? = null,

	@field:SerializedName("ProjectLandAreaCertificate")
	var projectLandAreaCertificate: String? = null,

	@field:SerializedName("ProjectLandCreateDate")
	var projectLandCreateDate: String? = null,

	@field:SerializedName("ProjectLandOrientationNortheast")
	var projectLandOrientationNortheast: String? = null,

	@field:SerializedName("ProjectLandCemeteryPresence")
	var projectLandCemeteryPresence: Int? = null,

	@field:SerializedName("ProjectLandBlock")
	var projectLandBlock: String? = null,

	@field:SerializedName("ProjectLandVillageName")
	var projectLandVillageName: String? = null,

	@field:SerializedName("ProjectLandTopographyId")
	var projectLandTopographyId: Int? = null,

	@field:SerializedName("ProjectPartyLandId")
	var projectPartyLandId: Int? = null,

	@field:SerializedName("ProjectLandSecurityAgainstCrime")
	var projectLandSecurityAgainstCrime: Int? = null,

	@field:SerializedName("ProjectLandLength")
	var projectLandLength: Double? = null,

	@field:SerializedName("ProjectLandCertificateNumber")
	var projectLandCertificateNumber: String? = null,

	@field:SerializedName("ProjectLandRightsExpirationDate")
	var projectLandRightsExpirationDate: String? = null,

	@field:SerializedName("ProjectPartyStatus")
	var projectPartyStatus: Int? = null,

	@field:SerializedName("ProjectLandSUTETPresenceStatus")
	var projectLandSUTETPresenceStatus: String? = null,

	@field:SerializedName("ProjectLandOrientationWest")
	var projectLandOrientationWest: String? = null,

	@field:SerializedName("ProjectLandAvailableCleanWaterSystemStatus")
	var projectLandAvailableCleanWaterSystemStatus: String? = null,

	@field:SerializedName("ProjectLandProvinceCode")
	var projectLandProvinceCode: String? = null,

	@field:SerializedName("DistrictName")
	var districtName: String? = null,

	@field:SerializedName("ProjectLandAvailableElectricalGrid")
	var projectLandAvailableElectricalGrid: Int? = null,

	@field:SerializedName("ProjectLandId")
	var projectLandId: Int? = null,

	@field:SerializedName("ProjectPartyCreateDate")
	var projectPartyCreateDate: String? = null,

	@field:SerializedName("ProjectLandEaseOfTransportation")
	var projectLandEaseOfTransportation: Int? = null
)