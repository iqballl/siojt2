package mki.siojt2.model.data_detail_tanah

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ProjectLandAddInfosItem(

	@field:SerializedName("ProjectLandAddInfoAddInfoId")
	var projectLandAddInfoAddInfoId: Int? = null,

	@field:SerializedName("AddInfoName")
	var addInfoName: String? = null,

	@field:SerializedName("ProjectLandAddInfoId")
	var projectLandAddInfoId: Int? = null,

	@field:SerializedName("ProjectLandAddInfoCreateDate")
	var projectLandAddInfoCreateDate: String? = null,

	@field:SerializedName("ProjectLandAddInfoLandId")
	var projectLandAddInfoLandId: Int? = null,

	@field:SerializedName("ProjectLandAddInfoCreateBy")
	var projectLandAddInfoCreateBy: Int? = null,

	@field:SerializedName("ProjectLandAddInfoLogId")
	var projectLandAddInfoLogId: Any? = null,

	@field:SerializedName("ProjectLandAddInfoStatus")
	var projectLandAddInfoStatus: Int? = null
)