package mki.siojt2.model.data_detail_tanah

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ProjectLandTypesItem(

	@field:SerializedName("ProjectLandTypeId")
	var projectLandTypeId: Int? = null,

	@field:SerializedName("ProjectLandTypeCreateDate")
	var projectLandTypeCreateDate: String? = null,

	@field:SerializedName("ProjectLandTypeLogId")
	var projectLandTypeLogId: Any? = null,

	@field:SerializedName("ProjectLandTypeStatus")
	var projectLandTypeStatus: Int? = null,

	@field:SerializedName("LandTypeName")
	var landTypeName: String? = null,

	@field:SerializedName("LandTypeDescription")
	var landTypeDescription: Any? = null,

	@field:SerializedName("ProjectLandTypeCreateBy")
	var projectLandTypeCreateBy: Int? = null,

	@field:SerializedName("ProjectLandTypeLandId")
	var projectLandTypeLandId: Int? = null,

	@field:SerializedName("ProjectLandTypeLandTypeId")
	var projectLandTypeLandTypeId: Int? = null
)