package mki.siojt2.model.data_detail_tanah

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataDetailTanah2(

	@field:SerializedName("ProjectLandTypes")
	var projectLandTypes: List<ProjectLandTypesItem?>? = null,

	@field:SerializedName("ProjectLandUtilizations")
	var projectLandUtilizations: List<ProjectLandUtilizationsItem?>? = null,

	@field:SerializedName("ProjectLandAddInfos")
	var projectLandAddInfos: List<ProjectLandAddInfosItem?>? = null,

	@field:SerializedName("LandDetail")
	var landDetail: LandDetail? = null
)