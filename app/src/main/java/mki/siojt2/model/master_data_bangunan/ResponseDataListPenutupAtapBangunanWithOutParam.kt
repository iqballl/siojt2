package mki.siojt2.model.master_data_bangunan

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataListPenutupAtapBangunanWithOutParam : RealmObject() {

    @SerializedName("RoofCoveringTypeId")
    @Expose
    private var roofCoveringTypeId: Int? = null

    @SerializedName("RoofCoveringTypeName")
    @Expose
    private var roofCoveringTypeName: String? = ""

    @SerializedName("RoofCoveringTypeStatus")
    @Expose
    private var roofCoveringTypeStatus: Int? = null

    fun getroofCoveringTypeId(): Int? {
        return roofCoveringTypeId
    }

    fun setroofCoveringTypeId(roofCoveringTypeId: Int?) {
        this.roofCoveringTypeId = roofCoveringTypeId
    }

    fun getroofCoveringTypeName(): String? {
        return roofCoveringTypeName
    }

    fun setroofCoveringTypeName(roofCoveringTypeName: String) {
        this.roofCoveringTypeName = roofCoveringTypeName
    }

    fun getroofCoveringTypeStatus(): Int? {
        return roofCoveringTypeStatus
    }

    fun setroofCoveringTypeStatus(roofCoveringTypeStatus: Int?) {
        this.roofCoveringTypeStatus = roofCoveringTypeStatus
    }

    override fun toString(): String {
        return roofCoveringTypeName.toString()
    }
}
