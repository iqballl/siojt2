package mki.siojt2.model.master_data_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListTipeBangunan(

	@field:SerializedName("BuildingTypeCreateBy")
	var buildingTypeCreateBy: Int? = null,

	@field:SerializedName("BuildingTypeId")
	var buildingTypeId: Int? = null,

	@field:SerializedName("BuildingTypeCreateDate")
	var buildingTypeCreateDate: String? = null,

	@field:SerializedName("BuildingTypeDescription")
	var buildingTypeDescription: Any? = null,

	@field:SerializedName("BuildingTypeStatus")
	var buildingTypeStatus: Int? = null,

	@field:SerializedName("BuildingTypeName")
	var buildingTypeName: String? = null,

	@field:SerializedName("BuildingTypeLogId")
	var buildingTypeLogId: Any? = null
){
	override fun toString(): String {
		return buildingTypeName.toString()
	}
}