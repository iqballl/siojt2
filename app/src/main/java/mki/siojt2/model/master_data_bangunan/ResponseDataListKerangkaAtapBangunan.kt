package mki.siojt2.model.master_data_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListKerangkaAtapBangunan(

	@field:SerializedName("RoofFrameTypeCreateBy")
	var roofFrameTypeCreateBy: Int? = null,

	@field:SerializedName("RoofFrameTypeId")
	var roofFrameTypeId: Int? = null,

	@field:SerializedName("RoofFrameTypeLogId")
	var roofFrameTypeLogId: Any? = null,

	@field:SerializedName("RoofFrameTypeName")
	var roofFrameTypeName: String? = null,

	@field:SerializedName("RoofFrameTypeDescription")
	var roofFrameTypeDescription: Any? = null,

	@field:SerializedName("RoofFrameTypeStatus")
	var roofFrameTypeStatus: Int? = null,

	@field:SerializedName("RoofFrameTypeCreateDate")
	var roofFrameTypeCreateDate: String? = null
){
	override fun toString(): String {
		return roofFrameTypeName.toString()
	}
}