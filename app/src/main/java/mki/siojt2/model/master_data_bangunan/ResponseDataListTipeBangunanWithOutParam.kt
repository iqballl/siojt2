package mki.siojt2.model.master_data_bangunan

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
//bb
open class ResponseDataListTipeBangunanWithOutParam : RealmObject() {
    @SerializedName("BuildingTypeId")
    @Expose
    private var buildingTypeId: Int? = null

    @SerializedName("BuildingTypeName")
    @Expose
    private var buildingTypeName: String? = ""

    @SerializedName("BuildingTypeStatus")
    @Expose
    private var buildingTypeStatus: Int? = null

    fun getbuildingTypeId(): Int? {
        return buildingTypeId
    }

    fun setbuildingTypeId(buildingTypeId: Int?) {
        this.buildingTypeId = buildingTypeId
    }

    fun getbuildingTypeName(): String? {
        return buildingTypeName
    }

    fun setbuildingTypeName(buildingTypeName: String) {
        this.buildingTypeName = buildingTypeName
    }

    fun getbuildingTypeStatus(): Int? {
        return buildingTypeStatus
    }

    fun setbuildingTypeStatus(BuildingTypeStatus: Int?) {
        this.buildingTypeStatus = BuildingTypeStatus
    }

    override fun toString(): String {
        return buildingTypeName.toString()
    }
}