package mki.siojt2.model.master_data_bangunan

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataListPlafonBangunanWithOutParam : RealmObject(){
    @SerializedName("CeilingTypeId")
    @Expose
    private var ceilingTypeId: Int? = null

    @SerializedName("CeilingTypeName")
    @Expose
    private var ceilingTypeName: String? = ""

    @SerializedName("CeilingTypeStatus")
    @Expose
    private var ceilingTypeStatus: Int? = null

    fun getceilingTypeId(): Int? {
        return ceilingTypeId
    }

    fun setceilingTypeId(ceilingTypeId: Int?) {
        this.ceilingTypeId = ceilingTypeId
    }

    fun getceilingTypeName(): String? {
        return ceilingTypeName
    }

    fun setceilingTypeName(ceilingTypeName: String) {
        this.ceilingTypeName = ceilingTypeName
    }

    fun getceilingTypeStatus(): Int? {
        return ceilingTypeStatus
    }

    fun setceilingTypeStatus(ceilingTypeStatus: Int?) {
        this.ceilingTypeStatus = ceilingTypeStatus
    }

    override fun toString(): String {
        return ceilingTypeName.toString()
    }
}