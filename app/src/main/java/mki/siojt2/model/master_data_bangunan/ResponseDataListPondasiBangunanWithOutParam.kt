package mki.siojt2.model.master_data_bangunan

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataListPondasiBangunanWithOutParam : RealmObject(){

	@SerializedName("FoundationTypeId")
	@Expose
	private var foundationTypeId: Int? = null

	@SerializedName("FoundationTypeName")
	@Expose
	private var foundationTypeName: String? = ""

	@SerializedName("FoundationTypeStatus")
	@Expose
	private var foundationTypeStatus: Int? = null

	fun getfoundationTypeId(): Int? {
		return foundationTypeId
	}

	fun setfoundationTypeId(foundationTypeId: Int?) {
		this.foundationTypeId = foundationTypeId
	}

	fun getfoundationTypeName(): String? {
		return foundationTypeName
	}

	fun setfoundationTypeName(foundationTypeName: String) {
		this.foundationTypeName = foundationTypeName
	}

	fun getfoundationTypeStatus(): Int? {
		return foundationTypeStatus
	}

	fun setfoundationTypeStatus(foundationTypeStatus: Int?) {
		this.foundationTypeStatus = foundationTypeStatus
	}

	override fun toString(): String {
		return foundationTypeName.toString()
	}
}