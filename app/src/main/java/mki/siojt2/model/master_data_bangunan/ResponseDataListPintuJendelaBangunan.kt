package mki.siojt2.model.master_data_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListPintuJendelaBangunan(

	@field:SerializedName("DoorWindowTypeName")
	var doorWindowTypeName: String? = null,

	@field:SerializedName("DoorWindowTypeCreateBy")
	var doorWindowTypeCreateBy: Int? = null,

	@field:SerializedName("DoorWindowTypeStatus")
	var doorWindowTypeStatus: Int? = null,

	@field:SerializedName("DoorWindowTypeLogId")
	var doorWindowTypeLogId: Any? = null,

	@field:SerializedName("DoorWindowTypeId")
	var doorWindowTypeId: Int? = null,

	@field:SerializedName("DoorWindowTypeDescription")
	var doorWindowTypeDescription: Any? = null,

	@field:SerializedName("DoorWindowTypeCreateDate")
	var doorWindowTypeCreateDate: String? = null
){
	override fun toString(): String {
		return doorWindowTypeName.toString()
	}
}