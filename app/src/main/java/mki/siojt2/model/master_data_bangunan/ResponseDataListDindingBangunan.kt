package mki.siojt2.model.master_data_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListDindingBangunan(

	@field:SerializedName("WallTypeDescription")
	var wallTypeDescription: Any? = null,

	@field:SerializedName("WallTypeName")
	var wallTypeName: String? = null,

	@field:SerializedName("WallTypeId")
	var wallTypeId: Int? = null,

	@field:SerializedName("WallTypeCreateDate")
	var wallTypeCreateDate: String? = null,

	@field:SerializedName("WallTypeStatus")
	var wallTypeStatus: Int? = null,

	@field:SerializedName("WallTypeCreateBy")
	var wallTypeCreateBy: Int? = null,

	@field:SerializedName("WallTypeLogId")
	var wallTypeLogId: Any? = null
){
	override fun toString(): String {
		return wallTypeName.toString()
	}
}