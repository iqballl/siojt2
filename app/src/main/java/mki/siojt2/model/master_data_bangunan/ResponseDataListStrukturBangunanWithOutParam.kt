package mki.siojt2.model.master_data_bangunan

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataListStrukturBangunanWithOutParam : RealmObject() {


    @SerializedName("StructureTypeId")
    @Expose
    private var structureTypeId: Int? = null

    @SerializedName("StructureTypeName")
    @Expose
    private var structureTypeName: String? = ""

    @SerializedName("StructureTypeStatus")
    @Expose
    private var structureTypeStatus: Int? = null

    fun getstructureTypeId(): Int? {
        return structureTypeId
    }

    fun setstructureTypeId(structureTypeId: Int?) {
        this.structureTypeId = structureTypeId
    }

    fun getstructureTypeName(): String? {
        return structureTypeName
    }

    fun setstructureTypeName(structureTypeName: String) {
        this.structureTypeName = structureTypeName
    }

    fun getstructureTypeStatus(): Int? {
        return structureTypeStatus
    }

    fun setstructureTypeStatus(structureTypeStatus: Int?) {
        this.structureTypeStatus = structureTypeStatus
    }

    override fun toString(): String {
        return structureTypeName.toString()
    }

}