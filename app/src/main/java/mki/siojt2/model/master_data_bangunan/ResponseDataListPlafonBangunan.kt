package mki.siojt2.model.master_data_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListPlafonBangunan(

        @field:SerializedName("CeilingTypeId")
        var ceilingTypeId: Int? = null,

        @field:SerializedName("CeilingTypeCreateBy")
        var ceilingTypeCreateBy: Int? = null,

        @field:SerializedName("CeilingTypeName")
        var ceilingTypeName: String? = null,

        @field:SerializedName("CeilingTypeLogId")
        var ceilingTypeLogId: Any? = null,

        @field:SerializedName("CeilingTypeDescription")
        var ceilingTypeDescription: Any? = null,

        @field:SerializedName("CeilingTypeStatus")
        var ceilingTypeStatus: Int? = null,

        @field:SerializedName("CeilingTypeCreateDate")
        var ceilingTypeCreateDate: String? = null
) {
    override fun toString(): String {
        return ceilingTypeName.toString()
    }
}