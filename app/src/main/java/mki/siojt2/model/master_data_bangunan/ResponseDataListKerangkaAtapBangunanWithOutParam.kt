package mki.siojt2.model.master_data_bangunan

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataListKerangkaAtapBangunanWithOutParam : RealmObject() {

    @SerializedName("RoofFrameTypeId")
    @Expose
    private var roofFrameTypeId: Int? = null

    @SerializedName("RoofFrameTypeName")
    @Expose
    private var roofFrameTypeName: String? = ""

    @SerializedName("RoofFrameTypeStatus")
    @Expose
    private var roofFrameTypeStatus: Int? = null

    fun getroofFrameTypeId(): Int? {
        return roofFrameTypeId
    }

    fun setroofFrameTypeId(roofFrameTypeId: Int?) {
        this.roofFrameTypeId = roofFrameTypeId
    }

    fun getroofFrameTypeName(): String? {
        return roofFrameTypeName
    }

    fun setroofFrameTypeName(roofFrameTypeName: String) {
        this.roofFrameTypeName = roofFrameTypeName
    }

    fun getroofFrameTypeStatus(): Int? {
        return roofFrameTypeStatus
    }

    fun setroofFrameStatus(roofFrameTypeStatus: Int?) {
        this.roofFrameTypeStatus = roofFrameTypeStatus
    }

    override fun toString(): String {
        return roofFrameTypeName.toString()
    }
}

