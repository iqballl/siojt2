package mki.siojt2.model.master_data_bangunan

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataListPintuJendelaBangunanWithOutParam : RealmObject() {

    @SerializedName("DoorWindowTypeId")
    @Expose
    private var doorWindowTypeId: Int? = null

    @SerializedName("DoorWindowTypeName")
    @Expose
    private var doorWindowTypeName: String? = ""

    @SerializedName("DoorWindowTypeStatus")
    @Expose
    private var doorWindowTypeStatus: Int? = null

    fun getdoorWindowTypeId(): Int? {
        return doorWindowTypeId
    }

    fun setdoorWindowTypeId(doorWindowTypeId: Int?) {
        this.doorWindowTypeId = doorWindowTypeId
    }

    fun getdoorWindowTypeName(): String? {
        return doorWindowTypeName
    }

    fun setdoorWindowTypeName(doorWindowTypeName: String) {
        this.doorWindowTypeName = doorWindowTypeName
    }

    fun getdoorWindowTypeStatus(): Int? {
        return doorWindowTypeStatus
    }

    fun setdoorWindowTypeStatus(doorWindowTypeStatus: Int?) {
        this.doorWindowTypeStatus = doorWindowTypeStatus
    }

    override fun toString(): String {
        return doorWindowTypeName.toString()
    }
}