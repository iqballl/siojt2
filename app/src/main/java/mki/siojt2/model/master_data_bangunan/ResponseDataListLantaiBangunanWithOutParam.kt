package mki.siojt2.model.master_data_bangunan

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataListLantaiBangunanWithOutParam : RealmObject() {

    @SerializedName("FloorTypeId")
    @Expose
    private var floorTypeId: Int? = null

    @SerializedName("FloorTypeName")
    @Expose
    private var floorTypeName: String? = ""

    @SerializedName("FloorTypeStatus")
    @Expose
    private var floorTypeStatus: Int? = null

    fun getFloorTypeId(): Int? {
        return floorTypeId
    }

    fun setFloorTypeId(floorTypeId: Int?) {
        this.floorTypeId = floorTypeId
    }

    fun getFloorTypeName(): String? {
        return floorTypeName
    }

    fun setFloorTypeName(floorTypeName: String) {
        this.floorTypeName = floorTypeName
    }

    fun getFloorTypeStatus(): Int? {
        return floorTypeStatus
    }

    fun setFloorTypeStatus(roofFrameTypeStatus: Int?) {
        this.floorTypeStatus = roofFrameTypeStatus
    }

    override fun toString(): String {
        return floorTypeName.toString()
    }
}
