package mki.siojt2.model.master_data_bangunan

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataListDindingBangunanWithOutParam : RealmObject(){
	@SerializedName("WallTypeId")
	@Expose
	private var wallTypeId: Int? = null

	@SerializedName("WallTypeName")
	@Expose
	private var wallTypeName: String? = ""

	@SerializedName("WallTypeStatus")
	@Expose
	private var wallTypeStatus: Int? = null

	fun getwallTypeId(): Int? {
		return wallTypeId
	}

	fun setwallTypeId(wallTypeId: Int?) {
		this.wallTypeId = wallTypeId
	}

	fun getwallTypeName(): String? {
		return wallTypeName
	}

	fun setwallTypeName(wallTypeName: String) {
		this.wallTypeName = wallTypeName
	}

	fun getwallTypeStatus(): Int? {
		return wallTypeStatus
	}

	fun setwallTypeStatus(wallTypeStatus: Int?) {
		this.wallTypeStatus = wallTypeStatus
	}

	override fun toString(): String {
		return wallTypeName.toString()
	}
}
