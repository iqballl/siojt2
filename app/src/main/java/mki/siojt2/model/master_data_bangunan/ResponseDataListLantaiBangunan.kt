package mki.siojt2.model.master_data_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListLantaiBangunan(

	@field:SerializedName("FloorTypeLogId")
	var floorTypeLogId: Any? = null,

	@field:SerializedName("FloorTypeStatus")
	var floorTypeStatus: Int? = null,

	@field:SerializedName("FloorTypeName")
	var floorTypeName: String? = null,

	@field:SerializedName("FloorTypeCreateBy")
	var floorTypeCreateBy: Int? = null,

	@field:SerializedName("FloorTypeDescription")
	var floorTypeDescription: Any? = null,

	@field:SerializedName("FloorTypeCreateDate")
	var floorTypeCreateDate: String? = null,

	@field:SerializedName("FloorTypeId")
	var floorTypeId: Int? = null
){
	override fun toString(): String {
		return floorTypeName.toString()
	}
}