package mki.siojt2.model.master_data_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListPondasiBangunan(

	@field:SerializedName("FoundationTypeDescription")
	var foundationTypeDescription: Any? = null,

	@field:SerializedName("FoundationTypeId")
	var foundationTypeId: Int? = null,

	@field:SerializedName("FoundationTypeName")
	var foundationTypeName: String? = null,

	@field:SerializedName("FoundationTypeStatus")
	var foundationTypeStatus: Int? = null,

	@field:SerializedName("FoundationTypeLogId")
	var foundationTypeLogId: Any? = null,

	@field:SerializedName("FoundationTypeCreateBy")
	var foundationTypeCreateBy: Int? = null,

	@field:SerializedName("FoundationTypeCreateDate")
	var foundationTypeCreateDate: String? = null
){
	override fun toString(): String {
		return foundationTypeName.toString()
	}
}