package mki.siojt2.model.master_data_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListPenutupAtapBangunan(

	@field:SerializedName("RoofCoveringTypeDescription")
	var roofCoveringTypeDescription: Any? = null,

	@field:SerializedName("RoofCoveringTypeCreateDate")
	var roofCoveringTypeCreateDate: String? = null,

	@field:SerializedName("RoofCoveringTypeLogId")
	var roofCoveringTypeLogId: Any? = null,

	@field:SerializedName("RoofCoveringTypeId")
	var roofCoveringTypeId: Int? = null,

	@field:SerializedName("RoofCoveringTypeCreateBy")
	var roofCoveringTypeCreateBy: Any? = null,

	@field:SerializedName("RoofCoveringTypeStatus")
	var roofCoveringTypeStatus: Int? = null,

	@field:SerializedName("RoofCoveringTypeName")
	var roofCoveringTypeName: String? = null
){
	override fun toString(): String {
		return roofCoveringTypeName.toString()
	}
}