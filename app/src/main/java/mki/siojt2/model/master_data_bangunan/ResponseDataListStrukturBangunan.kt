package mki.siojt2.model.master_data_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListStrukturBangunan(

	@field:SerializedName("StructureTypeName")
	var structureTypeName: String? = null,

	@field:SerializedName("StructureTypeCreateDate")
	var structureTypeCreateDate: String? = null,

	@field:SerializedName("StructureTypeLogId")
	var structureTypeLogId: Any? = null,

	@field:SerializedName("StructureTypeId")
	var structureTypeId: Int? = null,

	@field:SerializedName("StructureTypeStatus")
	var structureTypeStatus: Int? = null,

	@field:SerializedName("StructureTypeDescription")
	var structureTypeDescription: Any? = null,

	@field:SerializedName("StructureTypeCreateBy")
	var structureTypeCreateBy: Int? = null
){
	override fun toString(): String {
		return structureTypeName.toString()
	}
}