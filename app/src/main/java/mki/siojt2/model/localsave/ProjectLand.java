package mki.siojt2.model.localsave;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import mki.siojt2.model.mapbox.CustomLatLng;
import mki.siojt2.model.local_save_image.DataImageLand;

/**
 * Created by Endra on 2019-10-28.
 */
public class ProjectLand extends RealmObject {

    @PrimaryKey
    private Integer LandIdTemp;

    private Integer LandId;
    private Integer PartyIdTemp;
    private Integer PartyId;
    private Integer ProjectId;
    private Integer areaId;
    private Integer subjectId;
    private String subjectName;
    private Boolean isSimpleMode;

    //Form Objek Tanah 1
    private String LandNumberList;
    private String LandProvinceCode;
    private String LandProvinceName;
    private String LandRegencyCode;
    private String LandRegencyName;
    private String LandDistrictCode;
    private String LandDistrictName;
    private String LandVillageId;
    private String LandVillageName;
    private String LandBlok;
    private String LandNumber;
    private String LandComplekName;
    private String LandStreet;
    private String LandRT;
    private String LandRW;
    private String LandPostalCode;
    private String LandLatitude;
    private String LandLangitude;
    private RealmList<CustomLatLng> LandPolygonPoint;

    //Form Objek Tanah 2
    private Integer LandPositionToRoadId;
    private String LandPositionToRoadName;
    private Float LandWideFrontRoad;
    private Float LandFrontage;
    private Float LandLength;
    private Integer LandShapeId;
    private String LandShapeName;
    private String LandShapeNameOther;
    private String LandAreaCertificate;
    private String LandAreaAffected;
    private String LandTypeLandTypeId;
    private String LandTypeLandTypeValue;
    private String LandTypeLandTypeNameOther;
    private String LandTypographyId;
    private String LandTypographyName;
    private String LandHeightToRoad;
    private String tanahSisa;
    private String LivelihoodLiveId;
    private String LivelihoodLiveName;
    private String  LivelihoodOther;
    private String  LivelihoodIncome;

    //Form Objek Tanah 3
    private Integer LandOwnershipId;
    private String LandOwnershipName;
    private String LandOwnershipNameOther;
    private String LandOwnershipTypeSHM;
    private String LandDateOfIssue;
    private String LandRightsExpirationDate;
    private String LandNIB;
    private String LandCertificateNumber;
    private String LandUtilizationLandUtilizationId;
    private String LandUtilizationLandUtilizationName;
    private String LandUtilizationLandUtilizationNameOther;
    private Integer LandZoningSpatialPlanId;
    private String LandZoningSpatialPlanName;
    private String LandOrientationWest;
    private String LandOrientationEast;
    private String LandOrientationNorth;
    private String LandOrientationSouth;
    private String LandOrientationNorthwest;
    private String LandOrientationNortheast;
    private String LandOrientationSoutheast;
    private String LandOrientationSouthwest;
    private String dampak;

    //Form Objek Tanah 4
    private Integer LandEaseToPropertyId;
    private String LandEaseToPropertyName;
    private Integer LandEaseToShopingCenterId;
    private String LandEaseToShopingCenterName;
    private Integer LandEaseToEducationFacilitiesId;
    private String LandEaseToEducationFacilitiesName;
    private Integer LandEaseToTouristSitesId;
    private String LandEaseToTouristSitesName;
    private Integer LandEaseOfTransportationId;
    private String LandEaseOfTransportationName;
    private Integer LandSecurityAgainstCrimeId;
    private String LandSecurityAgainstCrimeName;
    private Integer LandSafetyAgainstFireHazardsId;
    private String LandSafetyAgainstFireHazardsName;
    private Integer LandSecurityAgainstNaturalDisastersId;
    private String LandSecurityAgainstNaturalDisastersName;
    private Integer LandAvailableElectricalGridId;
    private String LandAvailableElectricalGridName;
    private Integer LandAvailableCleanWaterSystemId;
    private String LandAvailableCleanWaterSystemName;
    private Integer LandAvailableTelephoneNetworkId;
    private String LandAvailableTelephoneNetworkName;
    private Integer LandAvailableGasPipelineNetworkId;
    private String LandAvailableGasPipelineNetworkName;
    private Integer LandSUTETPresenceId;
    private String LandSUTETPresenceName;
    private Float ProjectLandSUTETDistance;
    private Integer LandCemeteryPresenceId;
    private String LandCemeteryPresenceName;
    private Float LandCemeteryDistance;
    private Integer LandSkewerPresenceId;
    private String LandSkewerPresenceName;
    private String LandAddInfoAddInfoId;
    private String LandAddInfoAddInfoName;

    private String note;
    private String hasOtherObject;

    public String getLandAlasHakTanah() {
        return LandAlasHakTanah;
    }

    public void setLandAlasHakTanah(String landAlasHakTanah) {
        LandAlasHakTanah = landAlasHakTanah;
    }

    private String LandAlasHakTanah;

    private RealmList<DataImageLand> dataImageLands;

    private Integer PartyCreateBy;

    private Integer isCreate;

    private Integer selectedLinkedObjectUAVId;

    public Boolean getSimpleMode() {
        return isSimpleMode;
    }

    public void setSimpleMode(Boolean simpleMode) {
        isSimpleMode = simpleMode;
    }

    public String getTanahSisa() {
        return tanahSisa;
    }

    public void setTanahSisa(String tanahSisa) {
        this.tanahSisa = tanahSisa;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getSelectedLinkedObjectUAVId() {
        return selectedLinkedObjectUAVId;
    }

    public void setSelectedLinkedObjectUAVId(Integer selectedLinkedObjectUAVId) {
        this.selectedLinkedObjectUAVId = selectedLinkedObjectUAVId;
    }

    public List<CustomLatLng> getLandPolygonPoint() {
        return LandPolygonPoint;
    }

    public void setLandPolygonPoint(RealmList<CustomLatLng> landPolygon) {
        LandPolygonPoint = landPolygon;
    }

    public String getLandUtilizationLandUtilizationName() {
        return LandUtilizationLandUtilizationName;
    }

    public void setLandUtilizationLandUtilizationName(String landUtilizationLandUtilizationName) {
        LandUtilizationLandUtilizationName = landUtilizationLandUtilizationName;
    }

    public String getLandAddInfoAddInfoName() {
        return LandAddInfoAddInfoName;
    }

    public void setLandAddInfoAddInfoName(String landAddInfoAddInfoName) {
        LandAddInfoAddInfoName = landAddInfoAddInfoName;
    }

    public String getLandTypeLandTypeValue() {
        return LandTypeLandTypeValue;
    }

    public void setLandTypeLandTypeValue(String landTypeLandTypeValue) {
        LandTypeLandTypeValue = landTypeLandTypeValue;
    }

    public String getLandTypographyName() {
        return LandTypographyName;
    }

    public void setLandTypographyName(String landTypographyName) {
        LandTypographyName = landTypographyName;
    }

    public Integer getIsCreate() {
        return isCreate;
    }

    public void setIsCreate(Integer isCreate) {
        this.isCreate = isCreate;
    }

    public Integer getLandIdTemp() {
        return LandIdTemp;
    }

    public void setLandIdTemp(Integer landIdTemp) {
        LandIdTemp = landIdTemp;
    }

    public Integer getLandId() {
        return LandId;
    }

    public void setLandId(Integer landId) {
        LandId = landId;
    }

    public Integer getPartyIdTemp() {
        return PartyIdTemp;
    }

    public void setPartyIdTemp(Integer partyIdTemp) {
        PartyIdTemp = partyIdTemp;
    }

    public Integer getPartyId() {
        return PartyId;
    }

    public void setPartyId(Integer partyId) {
        PartyId = partyId;
    }

    public Integer getProjectId() {
        return ProjectId;
    }

    public void setProjectId(Integer projectId) {
        ProjectId = projectId;
    }

    public String getLandNumberList() {
        return LandNumberList;
    }

    public void setLandNumberList(String landNumberList) {
        LandNumberList = landNumberList;
    }

    public String getLandProvinceCode() {
        return LandProvinceCode;
    }

    public void setLandProvinceCode(String landProvinceCode) {
        LandProvinceCode = landProvinceCode;
    }

    public String getLandProvinceName() {
        return LandProvinceName;
    }

    public void setLandProvinceName(String landProvinceName) {
        LandProvinceName = landProvinceName;
    }

    public String getLandRegencyCode() {
        return LandRegencyCode;
    }

    public void setLandRegencyCode(String landRegencyCode) {
        LandRegencyCode = landRegencyCode;
    }

    public String getLandRegencyName() {
        return LandRegencyName;
    }

    public void setLandRegencyName(String landRegencyName) {
        LandRegencyName = landRegencyName;
    }

    public String getLandDistrictCode() {
        return LandDistrictCode;
    }

    public void setLandDistrictCode(String landDistrictCode) {
        LandDistrictCode = landDistrictCode;
    }

    public String getLandDistrictName() {
        return LandDistrictName;
    }

    public void setLandDistrictName(String landDistrictName) {
        LandDistrictName = landDistrictName;
    }

    public String getLandVillageId() {
        return LandVillageId;
    }

    public void setLandVillageId(String landVillageId) {
        LandVillageId = landVillageId;
    }

    public String getLandVillageName() {
        return LandVillageName;
    }

    public void setLandVillageName(String landVillageName) {
        LandVillageName = landVillageName;
    }

    public String getLandBlok() {
        return LandBlok;
    }

    public void setLandBlok(String landBlok) {
        LandBlok = landBlok;
    }

    public String getLandNumber() {
        return LandNumber;
    }

    public void setLandNumber(String landNumber) {
        LandNumber = landNumber;
    }

    public String getLandComplekName() {
        return LandComplekName;
    }

    public void setLandComplekName(String landComplekName) {
        LandComplekName = landComplekName;
    }

    public String getLandStreet() {
        return LandStreet;
    }

    public void setLandStreet(String landStreet) {
        LandStreet = landStreet;
    }

    public String getLandRT() {
        return LandRT;
    }

    public void setLandRT(String landRT) {
        LandRT = landRT;
    }

    public String getLandRW() {
        return LandRW;
    }

    public void setLandRW(String landRW) {
        LandRW = landRW;
    }

    public String getLandPostalCode() {
        return LandPostalCode;
    }

    public void setLandPostalCode(String landPostalCode) {
        LandPostalCode = landPostalCode;
    }

    public String getLandLatitude() {
        return LandLatitude;
    }

    public void setLandLatitude(String landLatitude) {
        LandLatitude = landLatitude;
    }

    public String getLandLangitude() {
        return LandLangitude;
    }

    public void setLandLangitude(String landLangitude) {
        LandLangitude = landLangitude;
    }

    public Integer getLandPositionToRoadId() {
        return LandPositionToRoadId;
    }

    public void setLandPositionToRoadId(Integer landPositionToRoadId) {
        LandPositionToRoadId = landPositionToRoadId;
    }

    public String getLandPositionToRoadName() {
        return LandPositionToRoadName;
    }

    public void setLandPositionToRoadName(String landPositionToRoadName) {
        LandPositionToRoadName = landPositionToRoadName;
    }

    public Float getLandWideFrontRoad() {
        return LandWideFrontRoad;
    }

    public void setLandWideFrontRoad(Float landWideFrontRoad) {
        LandWideFrontRoad = landWideFrontRoad;
    }

    public Float getLandFrontage() {
        return LandFrontage;
    }

    public void setLandFrontage(Float landFrontage) {
        LandFrontage = landFrontage;
    }

    public Float getLandLength() {
        return LandLength;
    }

    public void setLandLength(Float landLength) {
        LandLength = landLength;
    }

    public Integer getLandShapeId() {
        return LandShapeId;
    }

    public void setLandShapeId(Integer landShapeId) {
        LandShapeId = landShapeId;
    }

    public String getLandShapeName() {
        return LandShapeName;
    }

    public void setLandShapeName(String landShapeName) {
        LandShapeName = landShapeName;
    }

    public String getLandAreaCertificate() {
        return LandAreaCertificate;
    }

    public void setLandAreaCertificate(String landAreaCertificate) {
        LandAreaCertificate = landAreaCertificate;
    }

    public String getLandAreaAffected() {
        return LandAreaAffected;
    }

    public void setLandAreaAffected(String landAreaAffected) {
        LandAreaAffected = landAreaAffected;
    }

    public String getLandTypeLandTypeId() {
        return LandTypeLandTypeId;
    }

    public void setLandTypeLandTypeId(String landTypeLandTypeId) {
        LandTypeLandTypeId = landTypeLandTypeId;
    }

    public String getLandTypographyId() {
        return LandTypographyId;
    }

    public void setLandTypographyId(String landTypographyId) {
        LandTypographyId = landTypographyId;
    }

    public String getLandHeightToRoad() {
        return LandHeightToRoad;
    }

    public void setLandHeightToRoad(String landHeightToRoad) {
        LandHeightToRoad = landHeightToRoad;
    }

    public Integer getLandOwnershipId() {
        return LandOwnershipId;
    }

    public void setLandOwnershipId(Integer landOwnershipId) {
        LandOwnershipId = landOwnershipId;
    }

    public String getLandOwnershipName() {
        return LandOwnershipName;
    }

    public void setLandOwnershipName(String landOwnershipName) {
        LandOwnershipName = landOwnershipName;
    }

    public String getLandDateOfIssue() {
        return LandDateOfIssue;
    }

    public void setLandDateOfIssue(String landDateOfIssue) {
        LandDateOfIssue = landDateOfIssue;
    }

    public String getLandRightsExpirationDate() {
        return LandRightsExpirationDate;
    }

    public void setLandRightsExpirationDate(String landRightsExpirationDate) {
        LandRightsExpirationDate = landRightsExpirationDate;
    }

    public String getLandNIB() {
        return LandNIB;
    }

    public void setLandNIB(String landNIB) {
        LandNIB = landNIB;
    }

    public String getLandCertificateNumber() {
        return LandCertificateNumber;
    }

    public void setLandCertificateNumber(String landCertificateNumber) {
        LandCertificateNumber = landCertificateNumber;
    }

    public String getLandUtilizationLandUtilizationId() {
        return LandUtilizationLandUtilizationId;
    }

    public void setLandUtilizationLandUtilizationId(String landUtilizationLandUtilizationId) {
        LandUtilizationLandUtilizationId = landUtilizationLandUtilizationId;
    }

    public Integer getLandZoningSpatialPlanId() {
        return LandZoningSpatialPlanId;
    }

    public void setLandZoningSpatialPlanId(Integer landZoningSpatialPlanId) {
        LandZoningSpatialPlanId = landZoningSpatialPlanId;
    }

    public String getLandZoningSpatialPlanName() {
        return LandZoningSpatialPlanName;
    }

    public void setLandZoningSpatialPlanName(String landZoningSpatialPlanName) {
        LandZoningSpatialPlanName = landZoningSpatialPlanName;
    }

    public String getLandOrientationWest() {
        return LandOrientationWest;
    }

    public void setLandOrientationWest(String landOrientationWest) {
        LandOrientationWest = landOrientationWest;
    }

    public String getLandOrientationEast() {
        return LandOrientationEast;
    }

    public void setLandOrientationEast(String landOrientationEast) {
        LandOrientationEast = landOrientationEast;
    }

    public String getLandOrientationNorth() {
        return LandOrientationNorth;
    }

    public void setLandOrientationNorth(String landOrientationNorth) {
        LandOrientationNorth = landOrientationNorth;
    }

    public String getLandOrientationSouth() {
        return LandOrientationSouth;
    }

    public void setLandOrientationSouth(String landOrientationSouth) {
        LandOrientationSouth = landOrientationSouth;
    }

    public String getLandOrientationNorthwest() {
        return LandOrientationNorthwest;
    }

    public void setLandOrientationNorthwest(String landOrientationNorthwest) {
        LandOrientationNorthwest = landOrientationNorthwest;
    }

    public String getLandOrientationNortheast() {
        return LandOrientationNortheast;
    }

    public void setLandOrientationNortheast(String landOrientationNortheast) {
        LandOrientationNortheast = landOrientationNortheast;
    }

    public String getLandOrientationSoutheast() {
        return LandOrientationSoutheast;
    }

    public void setLandOrientationSoutheast(String landOrientationSoutheast) {
        LandOrientationSoutheast = landOrientationSoutheast;
    }

    public String getLandOrientationSouthwest() {
        return LandOrientationSouthwest;
    }

    public void setLandOrientationSouthwest(String landOrientationSouthwest) {
        LandOrientationSouthwest = landOrientationSouthwest;
    }

    public Integer getLandEaseToPropertyId() {
        return LandEaseToPropertyId;
    }

    public void setLandEaseToPropertyId(Integer landEaseToPropertyId) {
        LandEaseToPropertyId = landEaseToPropertyId;
    }

    public String getLandEaseToPropertyName() {
        return LandEaseToPropertyName;
    }

    public void setLandEaseToPropertyName(String landEaseToPropertyName) {
        LandEaseToPropertyName = landEaseToPropertyName;
    }

    public Integer getLandEaseToShopingCenterId() {
        return LandEaseToShopingCenterId;
    }

    public void setLandEaseToShopingCenterId(Integer landEaseToShopingCenterId) {
        LandEaseToShopingCenterId = landEaseToShopingCenterId;
    }

    public String getLandEaseToShopingCenterName() {
        return LandEaseToShopingCenterName;
    }

    public void setLandEaseToShopingCenterName(String landEaseToShopingCenterName) {
        LandEaseToShopingCenterName = landEaseToShopingCenterName;
    }

    public Integer getLandEaseToEducationFacilitiesId() {
        return LandEaseToEducationFacilitiesId;
    }

    public void setLandEaseToEducationFacilitiesId(Integer landEaseToEducationFacilitiesId) {
        LandEaseToEducationFacilitiesId = landEaseToEducationFacilitiesId;
    }

    public String getLandEaseToEducationFacilitiesName() {
        return LandEaseToEducationFacilitiesName;
    }

    public void setLandEaseToEducationFacilitiesName(String landEaseToEducationFacilitiesName) {
        LandEaseToEducationFacilitiesName = landEaseToEducationFacilitiesName;
    }

    public Integer getLandEaseToTouristSitesId() {
        return LandEaseToTouristSitesId;
    }

    public void setLandEaseToTouristSitesId(Integer landEaseToTouristSitesId) {
        LandEaseToTouristSitesId = landEaseToTouristSitesId;
    }

    public String getLandEaseToTouristSitesName() {
        return LandEaseToTouristSitesName;
    }

    public void setLandEaseToTouristSitesName(String landEaseToTouristSitesName) {
        LandEaseToTouristSitesName = landEaseToTouristSitesName;
    }

    public Integer getLandEaseOfTransportationId() {
        return LandEaseOfTransportationId;
    }

    public void setLandEaseOfTransportationId(Integer landEaseOfTransportationId) {
        LandEaseOfTransportationId = landEaseOfTransportationId;
    }

    public String getLandEaseOfTransportationName() {
        return LandEaseOfTransportationName;
    }

    public void setLandEaseOfTransportationName(String landEaseOfTransportationName) {
        LandEaseOfTransportationName = landEaseOfTransportationName;
    }

    public Integer getLandSecurityAgainstCrimeId() {
        return LandSecurityAgainstCrimeId;
    }

    public void setLandSecurityAgainstCrimeId(Integer landSecurityAgainstCrimeId) {
        LandSecurityAgainstCrimeId = landSecurityAgainstCrimeId;
    }

    public String getLandSecurityAgainstCrimeName() {
        return LandSecurityAgainstCrimeName;
    }

    public void setLandSecurityAgainstCrimeName(String landSecurityAgainstCrimeName) {
        LandSecurityAgainstCrimeName = landSecurityAgainstCrimeName;
    }

    public Integer getLandSafetyAgainstFireHazardsId() {
        return LandSafetyAgainstFireHazardsId;
    }

    public void setLandSafetyAgainstFireHazardsId(Integer landSafetyAgainstFireHazardsId) {
        LandSafetyAgainstFireHazardsId = landSafetyAgainstFireHazardsId;
    }

    public String getLandSafetyAgainstFireHazardsName() {
        return LandSafetyAgainstFireHazardsName;
    }

    public void setLandSafetyAgainstFireHazardsName(String landSafetyAgainstFireHazardsName) {
        LandSafetyAgainstFireHazardsName = landSafetyAgainstFireHazardsName;
    }

    public Integer getLandSecurityAgainstNaturalDisastersId() {
        return LandSecurityAgainstNaturalDisastersId;
    }

    public void setLandSecurityAgainstNaturalDisastersId(Integer landSecurityAgainstNaturalDisastersId) {
        LandSecurityAgainstNaturalDisastersId = landSecurityAgainstNaturalDisastersId;
    }

    public String getLandSecurityAgainstNaturalDisastersName() {
        return LandSecurityAgainstNaturalDisastersName;
    }

    public void setLandSecurityAgainstNaturalDisastersName(String landSecurityAgainstNaturalDisastersName) {
        LandSecurityAgainstNaturalDisastersName = landSecurityAgainstNaturalDisastersName;
    }

    public Integer getLandAvailableElectricalGridId() {
        return LandAvailableElectricalGridId;
    }

    public void setLandAvailableElectricalGridId(Integer landAvailableElectricalGridId) {
        LandAvailableElectricalGridId = landAvailableElectricalGridId;
    }

    public String getLandAvailableElectricalGridName() {
        return LandAvailableElectricalGridName;
    }

    public void setLandAvailableElectricalGridName(String landAvailableElectricalGridName) {
        LandAvailableElectricalGridName = landAvailableElectricalGridName;
    }

    public Integer getLandAvailableCleanWaterSystemId() {
        return LandAvailableCleanWaterSystemId;
    }

    public void setLandAvailableCleanWaterSystemId(Integer landAvailableCleanWaterSystemId) {
        LandAvailableCleanWaterSystemId = landAvailableCleanWaterSystemId;
    }

    public String getLandAvailableCleanWaterSystemName() {
        return LandAvailableCleanWaterSystemName;
    }

    public void setLandAvailableCleanWaterSystemName(String landAvailableCleanWaterSystemName) {
        LandAvailableCleanWaterSystemName = landAvailableCleanWaterSystemName;
    }

    public Integer getLandAvailableTelephoneNetworkId() {
        return LandAvailableTelephoneNetworkId;
    }

    public void setLandAvailableTelephoneNetworkId(Integer landAvailableTelephoneNetworkId) {
        LandAvailableTelephoneNetworkId = landAvailableTelephoneNetworkId;
    }

    public String getLandAvailableTelephoneNetworkName() {
        return LandAvailableTelephoneNetworkName;
    }

    public void setLandAvailableTelephoneNetworkName(String landAvailableTelephoneNetworkName) {
        LandAvailableTelephoneNetworkName = landAvailableTelephoneNetworkName;
    }

    public Integer getLandAvailableGasPipelineNetworkId() {
        return LandAvailableGasPipelineNetworkId;
    }

    public void setLandAvailableGasPipelineNetworkId(Integer landAvailableGasPipelineNetworkId) {
        LandAvailableGasPipelineNetworkId = landAvailableGasPipelineNetworkId;
    }

    public String getLandAvailableGasPipelineNetworkName() {
        return LandAvailableGasPipelineNetworkName;
    }

    public void setLandAvailableGasPipelineNetworkName(String landAvailableGasPipelineNetworkName) {
        LandAvailableGasPipelineNetworkName = landAvailableGasPipelineNetworkName;
    }

    public Integer getLandSUTETPresenceId() {
        return LandSUTETPresenceId;
    }

    public void setLandSUTETPresenceId(Integer landSUTETPresenceId) {
        LandSUTETPresenceId = landSUTETPresenceId;
    }

    public String getLandSUTETPresenceName() {
        return LandSUTETPresenceName;
    }

    public void setLandSUTETPresenceName(String landSUTETPresenceName) {
        LandSUTETPresenceName = landSUTETPresenceName;
    }

    public Float getProjectLandSUTETDistance() {
        return ProjectLandSUTETDistance;
    }

    public void setProjectLandSUTETDistance(Float projectLandSUTETDistance) {
        ProjectLandSUTETDistance = projectLandSUTETDistance;
    }

    public Integer getLandCemeteryPresenceId() {
        return LandCemeteryPresenceId;
    }

    public void setLandCemeteryPresenceId(Integer landCemeteryPresenceId) {
        LandCemeteryPresenceId = landCemeteryPresenceId;
    }

    public String getLandCemeteryPresenceName() {
        return LandCemeteryPresenceName;
    }

    public void setLandCemeteryPresenceName(String landCemeteryPresenceName) {
        LandCemeteryPresenceName = landCemeteryPresenceName;
    }

    public Float getLandCemeteryDistance() {
        return LandCemeteryDistance;
    }

    public void setLandCemeteryDistance(Float landCemeteryDistance) {
        LandCemeteryDistance = landCemeteryDistance;
    }

    public Integer getLandSkewerPresenceId() {
        return LandSkewerPresenceId;
    }

    public void setLandSkewerPresenceId(Integer landSkewerPresenceId) {
        LandSkewerPresenceId = landSkewerPresenceId;
    }

    public String getLandSkewerPresenceName() {
        return LandSkewerPresenceName;
    }

    public void setLandSkewerPresenceName(String landSkewerPresenceName) {
        LandSkewerPresenceName = landSkewerPresenceName;
    }

    public String getLandAddInfoAddInfoId() {
        return LandAddInfoAddInfoId;
    }

    public void setLandAddInfoAddInfoId(String landAddInfoAddInfoId) {
        LandAddInfoAddInfoId = landAddInfoAddInfoId;
    }

    public Integer getPartyCreateBy() {
        return PartyCreateBy;
    }

    public void setPartyCreateBy(Integer partyCreateBy) {
        PartyCreateBy = partyCreateBy;
    }

    public RealmList<DataImageLand> getDataImageLands() {
        return dataImageLands;
    }

    public void setDataImageLands(RealmList<DataImageLand> dataImageLands) {
        this.dataImageLands = dataImageLands;
    }

    public String getLandShapeNameOther() {
        return LandShapeNameOther;
    }

    public void setLandShapeNameOther(String landShapeNameOther) {
        LandShapeNameOther = landShapeNameOther;
    }

    public String getLandTypeLandTypeNameOther() {
        return LandTypeLandTypeNameOther;
    }

    public void setLandTypeLandTypeNameOther(String landTypeLandTypeNameOther) {
        LandTypeLandTypeNameOther = landTypeLandTypeNameOther;
    }

    public String getLandUtilizationLandUtilizationNameOther() {
        return LandUtilizationLandUtilizationNameOther;
    }

    public void setLandUtilizationLandUtilizationNameOther(String landUtilizationLandUtilizationNameOther) {
        LandUtilizationLandUtilizationNameOther = landUtilizationLandUtilizationNameOther;
    }

    public String getLandOwnershipNameOther() {
        return LandOwnershipNameOther;
    }

    public void setLandOwnershipNameOther(String landOwnershipNameOther) {
        LandOwnershipNameOther = landOwnershipNameOther;
    }

    public String getLandOwnershipTypeSHM() {
        return LandOwnershipTypeSHM;
    }

    public void setLandOwnershipTypeSHM(String landOwnershipTypeSHM) {
        LandOwnershipTypeSHM = landOwnershipTypeSHM;
    }

    public String getLivelihoodLiveId() {
        return LivelihoodLiveId;
    }

    public void setLivelihoodLiveId(String livelihoodLiveId) {
        LivelihoodLiveId = livelihoodLiveId;
    }

    public String getLivelihoodLiveName() {
        return LivelihoodLiveName;
    }

    public void setLivelihoodLiveName(String livelihoodLiveName) {
        LivelihoodLiveName = livelihoodLiveName;
    }

    public String getLivelihoodOther() {
        return LivelihoodOther;
    }

    public void setLivelihoodOther(String livelihoodOther) {
        LivelihoodOther = livelihoodOther;
    }

    public String getLivelihoodIncome() {
        return LivelihoodIncome;
    }

    public void setLivelihoodIncome(String livelihoodIncome) {
        LivelihoodIncome = livelihoodIncome;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getHasOtherObject() {
        return hasOtherObject;
    }

    public void setHasOtherObject(String hasOtherObject) {
        this.hasOtherObject = hasOtherObject;
    }

    public String getDampak() {
        return dampak;
    }

    public void setDampak(String dampak) {
        this.dampak = dampak;
    }
}
