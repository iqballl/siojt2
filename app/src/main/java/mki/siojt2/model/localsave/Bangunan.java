package mki.siojt2.model.localsave;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import mki.siojt2.model.local_save_image.DataImageBuilding;
import mki.siojt2.model.local_save_image.DataImageLand;

/**
 * Created by Endra on 2019-11-03.
 */

public class Bangunan extends RealmObject {
    @PrimaryKey
    private Integer BangunanIdTemp;

    private Integer BangunanId;
    private Integer ProjectId;
    private Integer LandIdTemp;
    private Integer LandId;

    private String ProjectPartyId;
    private String ProjectPartyName;
    private Integer ProjectBuildingTypeId;
    private String ProjectBuildingTypeName;
    private String ProjectBuildingTypeNameOther;
    private Integer ProjectBuildingFloorTotal;
    private String ProjectBuildingAreaUpperRoom;
    private String ProjectBuildingAreaLowerRoom;
    private String ProjectBuildingYearOfBuilt;
    private String FoundationTypeId;
    private String FoundationTypeName;
    private String FoundationTypeNameOther;
    private String PBFoundationTypeAreaTotal;
    private String StructureTypeId;
    private String StructureTypeName;
    private String StructureTypeNameOther;
    private String PBStructureTypeAreaTotal;
    private String RoofFrameTypeId;
    private String RoofFrameTypeName;
    private String RoofFrameTypeNameOther;
    private String PBRoofFrameTypeAreaTotal;
    private String RoofCoveringTypeId;
    private String RoofCoveringTypeName;
    private String RoofCoveringTypeNameOther;
    private String PBRoofCoveringTypeAreaTotal;
    private String CeilingTypeId;
    private String CeilingTypeName;
    private String CeilingTypeNameOther;
    private String PBCeilingTypeAreaTotal;
    private String DoorWindowTypeld;
    private String DoorWindowTypeName;
    private String DoorWindowTypeNameOther;
    private String PBDoorWindowTypeAreaTotal;
    private String FloorTypeId;
    private String FloorTypeName;
    private String FloorTypeNameOther;
    private String PBFloorTypeAreaTotal;
    private String WallTypeId;
    private String WallTypeName;
    private String WallTypeNameOther;
    private String PBFWallTypeAreaTotal;
    private String notes;

    private Integer selectedLinkedObjectUAVId;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getSelectedLinkedObjectUAVId() {
        return selectedLinkedObjectUAVId;
    }

    public void setSelectedLinkedObjectUAVId(Integer selectedLinkedObjectUAVId) {
        this.selectedLinkedObjectUAVId = selectedLinkedObjectUAVId;
    }

    private RealmList<DataImageBuilding> dataImageBuilding;

    private Integer CreatedBy;
    private Integer isCreate;

    public Integer getIsCreate() {
        return isCreate;
    }

    public void setIsCreate(Integer isCreate) {
        this.isCreate = isCreate;
    }

    public Integer getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(Integer createdBy) {
        CreatedBy = createdBy;
    }

    public Integer getBangunanIdTemp() {
        return BangunanIdTemp;
    }

    public void setBangunanIdTemp(Integer bangunanIdTemp) {
        BangunanIdTemp = bangunanIdTemp;
    }

    public Integer getBangunanId() {
        return BangunanId;
    }

    public void setBangunanId(Integer bangunanId) {
        BangunanId = bangunanId;
    }

    public Integer getProjectId() {
        return ProjectId;
    }

    public void setProjectId(Integer projectId) {
        ProjectId = projectId;
    }

    public Integer getLandIdTemp() {
        return LandIdTemp;
    }

    public void setLandIdTemp(Integer landIdTemp) {
        LandIdTemp = landIdTemp;
    }

    public Integer getLandId() {
        return LandId;
    }

    public void setLandId(Integer landId) {
        LandId = landId;
    }

    public String getProjectPartyId() {
        return ProjectPartyId;
    }

    public void setProjectPartyId(String projectPartyId) {
        ProjectPartyId = projectPartyId;
    }

    public String getProjectPartyName() {
        return ProjectPartyName;
    }

    public void setProjectPartyName(String projectPartyName) {
        ProjectPartyName = projectPartyName;
    }

    public Integer getProjectBuildingTypeId() {
        return ProjectBuildingTypeId;
    }

    public void setProjectBuildingTypeId(Integer projectBuildingTypeId) {
        ProjectBuildingTypeId = projectBuildingTypeId;
    }

    public String getProjectBuildingTypeName() {
        return ProjectBuildingTypeName;
    }

    public void setProjectBuildingTypeName(String projectBuildingTypeName) {
        ProjectBuildingTypeName = projectBuildingTypeName;
    }

    public Integer getProjectBuildingFloorTotal() {
        return ProjectBuildingFloorTotal;
    }

    public void setProjectBuildingFloorTotal(Integer projectBuildingFloorTotal) {
        ProjectBuildingFloorTotal = projectBuildingFloorTotal;
    }

    public String getProjectBuildingAreaUpperRoom() {
        return ProjectBuildingAreaUpperRoom;
    }

    public void setProjectBuildingAreaUpperRoom(String projectBuildingAreaUpperRoom) {
        ProjectBuildingAreaUpperRoom = projectBuildingAreaUpperRoom;
    }

    public String getProjectBuildingAreaLowerRoom() {
        return ProjectBuildingAreaLowerRoom;
    }

    public void setProjectBuildingAreaLowerRoom(String projectBuildingAreaLowerRoom) {
        ProjectBuildingAreaLowerRoom = projectBuildingAreaLowerRoom;
    }

    public String getFoundationTypeId() {
        return FoundationTypeId;
    }

    public void setFoundationTypeId(String foundationTypeId) {
        FoundationTypeId = foundationTypeId;
    }

    public String getFoundationTypeName() {
        return FoundationTypeName;
    }

    public void setFoundationTypeName(String foundationTypeName) {
        FoundationTypeName = foundationTypeName;
    }

    public String getPBFoundationTypeAreaTotal() {
        return PBFoundationTypeAreaTotal;
    }

    public void setPBFoundationTypeAreaTotal(String PBFoundationTypeAreaTotal) {
        this.PBFoundationTypeAreaTotal = PBFoundationTypeAreaTotal;
    }

    public String getStructureTypeId() {
        return StructureTypeId;
    }

    public void setStructureTypeId(String structureTypeId) {
        StructureTypeId = structureTypeId;
    }

    public String getStructureTypeName() {
        return StructureTypeName;
    }

    public void setStructureTypeName(String structureTypeName) {
        StructureTypeName = structureTypeName;
    }

    public String getPBStructureTypeAreaTotal() {
        return PBStructureTypeAreaTotal;
    }

    public void setPBStructureTypeAreaTotal(String PBStructureTypeAreaTotal) {
        this.PBStructureTypeAreaTotal = PBStructureTypeAreaTotal;
    }

    public String getRoofFrameTypeId() {
        return RoofFrameTypeId;
    }

    public void setRoofFrameTypeId(String roofFrameTypeId) {
        RoofFrameTypeId = roofFrameTypeId;
    }

    public String getRoofFrameTypeName() {
        return RoofFrameTypeName;
    }

    public void setRoofFrameTypeName(String roofFrameTypeName) {
        RoofFrameTypeName = roofFrameTypeName;
    }

    public String getPBRoofFrameTypeAreaTotal() {
        return PBRoofFrameTypeAreaTotal;
    }

    public void setPBRoofFrameTypeAreaTotal(String PBRoofFrameTypeAreaTotal) {
        this.PBRoofFrameTypeAreaTotal = PBRoofFrameTypeAreaTotal;
    }

    public String getRoofCoveringTypeId() {
        return RoofCoveringTypeId;
    }

    public void setRoofCoveringTypeId(String roofCoveringTypeId) {
        RoofCoveringTypeId = roofCoveringTypeId;
    }

    public String getRoofCoveringTypeName() {
        return RoofCoveringTypeName;
    }

    public void setRoofCoveringTypeName(String roofCoveringTypeName) {
        RoofCoveringTypeName = roofCoveringTypeName;
    }

    public String getPBRoofCoveringTypeAreaTotal() {
        return PBRoofCoveringTypeAreaTotal;
    }

    public void setPBRoofCoveringTypeAreaTotal(String PBRoofCoveringTypeAreaTotal) {
        this.PBRoofCoveringTypeAreaTotal = PBRoofCoveringTypeAreaTotal;
    }

    public String getCeilingTypeId() {
        return CeilingTypeId;
    }

    public void setCeilingTypeId(String ceilingTypeId) {
        CeilingTypeId = ceilingTypeId;
    }

    public String getCeilingTypeName() {
        return CeilingTypeName;
    }

    public void setCeilingTypeName(String ceilingTypeName) {
        CeilingTypeName = ceilingTypeName;
    }

    public String getPBCeilingTypeAreaTotal() {
        return PBCeilingTypeAreaTotal;
    }

    public void setPBCeilingTypeAreaTotal(String PBCeilingTypeAreaTotal) {
        this.PBCeilingTypeAreaTotal = PBCeilingTypeAreaTotal;
    }

    public String getDoorWindowTypeld() {
        return DoorWindowTypeld;
    }

    public void setDoorWindowTypeld(String doorWindowTypeld) {
        DoorWindowTypeld = doorWindowTypeld;
    }

    public String getDoorWindowTypeName() {
        return DoorWindowTypeName;
    }

    public void setDoorWindowTypeName(String doorWindowTypeName) {
        DoorWindowTypeName = doorWindowTypeName;
    }

    public String getPBDoorWindowTypeAreaTotal() {
        return PBDoorWindowTypeAreaTotal;
    }

    public void setPBDoorWindowTypeAreaTotal(String PBDoorWindowTypeAreaTotal) {
        this.PBDoorWindowTypeAreaTotal = PBDoorWindowTypeAreaTotal;
    }

    public String getFloorTypeId() {
        return FloorTypeId;
    }

    public void setFloorTypeId(String floorTypeId) {
        FloorTypeId = floorTypeId;
    }

    public String getFloorTypeName() {
        return FloorTypeName;
    }

    public void setFloorTypeName(String floorTypeName) {
        FloorTypeName = floorTypeName;
    }

    public String getPBFloorTypeAreaTotal() {
        return PBFloorTypeAreaTotal;
    }

    public void setPBFloorTypeAreaTotal(String PBFloorTypeAreaTotal) {
        this.PBFloorTypeAreaTotal = PBFloorTypeAreaTotal;
    }

    public String getWallTypeId() {
        return WallTypeId;
    }

    public void setWallTypeId(String wallTypeId) {
        WallTypeId = wallTypeId;
    }

    public String getWallTypeName() {
        return WallTypeName;
    }

    public void setWallTypeName(String wallTypeName) {
        WallTypeName = wallTypeName;
    }

    public String getPBFWallTypeAreaTotal() {
        return PBFWallTypeAreaTotal;
    }

    public void setPBFWallTypeAreaTotal(String PBFWallTypeAreaTotal) {
        this.PBFWallTypeAreaTotal = PBFWallTypeAreaTotal;
    }

    public RealmList<DataImageBuilding> getDataImageBuilding() {
        return dataImageBuilding;
    }

    public void setDataImageBuilding(RealmList<DataImageBuilding> dataImagePlants) {
        this.dataImageBuilding = dataImagePlants;
    }

    public String getFoundationTypeNameOther() {
        return FoundationTypeNameOther;
    }

    public void setFoundationTypeNameOther(String foundationTypeNameOther) {
        FoundationTypeNameOther = foundationTypeNameOther;
    }

    public String getStructureTypeNameOther() {
        return StructureTypeNameOther;
    }

    public void setStructureTypeNameOther(String structureTypeNameOther) {
        StructureTypeNameOther = structureTypeNameOther;
    }

    public String getRoofFrameTypeNameOther() {
        return RoofFrameTypeNameOther;
    }

    public void setRoofFrameTypeNameOther(String roofFrameTypeNameOther) {
        RoofFrameTypeNameOther = roofFrameTypeNameOther;
    }

    public String getRoofCoveringTypeNameOther() {
        return RoofCoveringTypeNameOther;
    }

    public void setRoofCoveringTypeNameOther(String roofCoveringTypeNameOther) {
        RoofCoveringTypeNameOther = roofCoveringTypeNameOther;
    }

    public String getCeilingTypeNameOther() {
        return CeilingTypeNameOther;
    }

    public void setCeilingTypeNameOther(String ceilingTypeNameOther) {
        CeilingTypeNameOther = ceilingTypeNameOther;
    }

    public String getDoorWindowTypeNameOther() {
        return DoorWindowTypeNameOther;
    }

    public void setDoorWindowTypeNameOther(String doorWindowTypeNameOther) {
        DoorWindowTypeNameOther = doorWindowTypeNameOther;
    }

    public String getFloorTypeNameOther() {
        return FloorTypeNameOther;
    }

    public void setFloorTypeNameOther(String floorTypeNameOther) {
        FloorTypeNameOther = floorTypeNameOther;
    }

    public String getWallTypeNameOther() {
        return WallTypeNameOther;
    }

    public void setWallTypeNameOther(String wallTypeNameOther) {
        WallTypeNameOther = wallTypeNameOther;
    }

    public String getProjectBuildingTypeNameOther() {
        return ProjectBuildingTypeNameOther;
    }

    public void setProjectBuildingTypeNameOther(String projectBuildingTypeNameOther) {
        ProjectBuildingTypeNameOther = projectBuildingTypeNameOther;
    }

    /* 2nd development */
    public String getProjectBuildingYearOfBuilt() {
        return ProjectBuildingYearOfBuilt;
    }

    public void setProjectBuildingYearOfBuilt(String projectBuildingYearOfBuilt) {
        ProjectBuildingYearOfBuilt = projectBuildingYearOfBuilt;
    }
}
