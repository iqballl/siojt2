package mki.siojt2.model.localsave;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Endra on 2019-11-14.
 */
public class Session {

    private static final String EXTRA_MARIPEDULI = "extra_editsavedelete";

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public Session(Context context) {
        sharedPreferences = context.getSharedPreferences(EXTRA_MARIPEDULI, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void setIdAndStatus(String Id, String statusPost, String from) {
        editor.putString("extra_id", Id);
        editor.putString("extra_status", statusPost);
        editor.putString("extra_from", from);
        editor.commit();
    }


    public String getId() {
        return sharedPreferences.getString("extra_id", "");
    }

    public String getStatus() {
        return sharedPreferences.getString("extra_status", "");
    }

    public String getFrom() {
        return sharedPreferences.getString("extra_from", "");
    }
}
