package mki.siojt2.model.localsave;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import mki.siojt2.model.local_save_image.DataImageFacilities;
import mki.siojt2.model.local_save_image.DataImageLand;
import mki.siojt2.model.local_save_image.DataImagePlants;
import mki.siojt2.model.second_development.OtherFacilityOfAirConditioner;
import mki.siojt2.model.second_development.OtherFacilityOfCleanWaterProcessingInstallation;
import mki.siojt2.model.second_development.OtherFacilityOfElevator;
import mki.siojt2.model.second_development.OtherFacilityOfPavement;
import mki.siojt2.model.second_development.OtherFacilityOfSwimmingPool;
import mki.siojt2.model.second_development.OtherFacilityOfTelephoneLine;
import mki.siojt2.model.second_development.OtherFacilityOfWWTP;

/**
 * Created by Endra on 2019-11-02.
 */
public class SaranaLain extends RealmObject {

    @PrimaryKey
    private Integer SaranaIdTemp;

    private Integer SaranaId;
    private Integer ProjectId;
    private Integer LandIdTemp;
    private Integer LandId;

    private String PerlengkapanId;
    private String PerlengkapanName;

    private String JenisFasilitasId;
    private String JenisFasilitasName;
    private String JenisFasilitasNameOther;

    private String Kapasitas;
    private String KapasitasValue;

    private String SatuanKapasitasValue;

    private RealmList<DataImageFacilities> dataImageOtherFacilities;

    /**
     * 2nd development
     * */

    private OtherFacilityOfTelephoneLine otherFacilityOfTelephoneLine;

    private OtherFacilityOfSwimmingPool otherFacilityOfSwimmingPool;

    private OtherFacilityOfAirConditioner otherFacilityOfAirConditioner;

    private OtherFacilityOfElevator otherFacilityOfElevator;

    private OtherFacilityOfCleanWaterProcessingInstallation otherFacilityOfCleanWaterProcessingInstallation;

    private OtherFacilityOfWWTP otherFacilityOfWWTP;

    private OtherFacilityOfPavement otherFacilityOfPavement;

    private Integer isCreate;

//    3rd development

    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getIsCreate() {
        return isCreate;
    }

    public void setIsCreate(Integer isCreate) {
        this.isCreate = isCreate;
    }

    public Integer getSaranaIdTemp() {
        return SaranaIdTemp;
    }

    public void setSaranaIdTemp(Integer saranaIdTemp) {
        SaranaIdTemp = saranaIdTemp;
    }

    public Integer getSaranaId() {
        return SaranaId;
    }

    public void setSaranaId(Integer saranaId) {
        SaranaId = saranaId;
    }

    public Integer getProjectId() {
        return ProjectId;
    }

    public void setProjectId(Integer projectId) {
        ProjectId = projectId;
    }

    public Integer getLandIdTemp() {
        return LandIdTemp;
    }

    public void setLandIdTemp(Integer landIdTemp) {
        LandIdTemp = landIdTemp;
    }

    public Integer getLandId() {
        return LandId;
    }

    public void setLandId(Integer landId) {
        LandId = landId;
    }

    public String getPerlengkapanId() {
        return PerlengkapanId;
    }

    public void setPerlengkapanId(String perlengkapanId) {
        PerlengkapanId = perlengkapanId;
    }

    public String getPerlengkapanName() {
        return PerlengkapanName;
    }

    public void setPerlengkapanName(String perlengkapanName) {
        PerlengkapanName = perlengkapanName;
    }

    public String getJenisFasilitasId() {
        return JenisFasilitasId;
    }

    public void setJenisFasilitasId(String jenisFasilitasId) {
        JenisFasilitasId = jenisFasilitasId;
    }

    public String getJenisFasilitasName() {
        return JenisFasilitasName;
    }

    public void setJenisFasilitasName(String jenisFasilitasName) {
        JenisFasilitasName = jenisFasilitasName;
    }

    public String getKapasitas() {
        return Kapasitas;
    }

    public void setKapasitas(String kapasitas) {
        Kapasitas = kapasitas;
    }

    public String getKapasitasValue() {
        return KapasitasValue;
    }

    public void setKapasitasValue(String kapasitasValue) {
        KapasitasValue = kapasitasValue;
    }

    public String getSatuanKapasitasValue() {
        return SatuanKapasitasValue;
    }

    public void setSatuanKapasitasValue(String satuanKapasitasValue) {
        SatuanKapasitasValue = satuanKapasitasValue;
    }

    public RealmList<DataImageFacilities> getDataImageOtherFacilities() {
        return dataImageOtherFacilities;
    }

    public void setDataImageOtherFacilities(RealmList<DataImageFacilities> dataImageOtherFacilities) {
        this.dataImageOtherFacilities = dataImageOtherFacilities;
    }

    public String getJenisFasilitasNameOther() {
        return JenisFasilitasNameOther;
    }

    public void setJenisFasilitasNameOther(String jenisFasilitasNameOther) {
        JenisFasilitasNameOther = jenisFasilitasNameOther;
    }


    /**
     * 2nd development
     * */
    public OtherFacilityOfSwimmingPool getOtherFacilityOfSwimmingPool() {
        return otherFacilityOfSwimmingPool;
    }

    public void setOtherFacilityOfSwimmingPool(OtherFacilityOfSwimmingPool otherFacilityOfSwimmingPool) {
        this.otherFacilityOfSwimmingPool = otherFacilityOfSwimmingPool;
    }

    public OtherFacilityOfAirConditioner getOtherFacilityOfAirConditioner() {
        return otherFacilityOfAirConditioner;
    }

    public void setOtherFacilityOfAirConditioner(OtherFacilityOfAirConditioner otherFacilityOfAirConditioner) {
        this.otherFacilityOfAirConditioner = otherFacilityOfAirConditioner;
    }

    public OtherFacilityOfElevator getOtherFacilityOfElevator() {
        return otherFacilityOfElevator;
    }

    public void setOtherFacilityOfElevator(OtherFacilityOfElevator otherFacilityOfElevator) {
        this.otherFacilityOfElevator = otherFacilityOfElevator;
    }

    public OtherFacilityOfCleanWaterProcessingInstallation getOtherFacilityOfCleanWaterProcessingInstallation() {
        return otherFacilityOfCleanWaterProcessingInstallation;
    }

    public void setOtherFacilityOfCleanWaterProcessingInstallation(OtherFacilityOfCleanWaterProcessingInstallation otherFacilityOfCleanWaterProcessingInstallation) {
        this.otherFacilityOfCleanWaterProcessingInstallation = otherFacilityOfCleanWaterProcessingInstallation;
    }

    public OtherFacilityOfWWTP getOtherFacilityOfWWTP() {
        return otherFacilityOfWWTP;
    }

    public void setOtherFacilityOfWWTP(OtherFacilityOfWWTP otherFacilityOfWWTP) {
        this.otherFacilityOfWWTP = otherFacilityOfWWTP;
    }

    public OtherFacilityOfPavement getOtherFacilityOfPavement() {
        return otherFacilityOfPavement;
    }

    public void setOtherFacilityOfPavement(OtherFacilityOfPavement otherFacilityOfPavement) {
        this.otherFacilityOfPavement = otherFacilityOfPavement;
    }

    public OtherFacilityOfTelephoneLine getOtherFacilityOfTelephoneLine() {
        return otherFacilityOfTelephoneLine;
    }

    public void setOtherFacilityOfTelephoneLine(OtherFacilityOfTelephoneLine otherFacilityOfTelephoneLine) {
        this.otherFacilityOfTelephoneLine = otherFacilityOfTelephoneLine;
    }
}
