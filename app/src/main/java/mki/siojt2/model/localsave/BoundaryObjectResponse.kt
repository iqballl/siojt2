package mki.siojt2.model.localsave

import com.google.gson.annotations.SerializedName

data class BoundaryObjectResponse(

	@field:SerializedName("area")
	val area: Double? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("geom")
	val geom: List<GeomItems?>? = null
)

data class GeomItems(

	@field:SerializedName("lat")
	val lat: Double? = null,

	@field:SerializedName("long")
	val jsonMemberLong: Double? = null
)
