package mki.siojt2.model.localsave;

import io.realm.RealmObject;

/**
 * Created by Endra on 2019-12-03.
 */
public class TempPemilikObjek extends RealmObject {

    private String pemilikId;
    private String pemilikName;

    public String getPemilikId() {
        return pemilikId;
    }

    public void setPemilikId(String pemilikId) {
        this.pemilikId = pemilikId;
    }

    public String getPemilikName() {
        return pemilikName;
    }

    public void setPemilikName(String pemilikName) {
        this.pemilikName = pemilikName;
    }

    @Override
    public String toString(){
        return pemilikName;
    }

}
