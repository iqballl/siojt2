package mki.siojt2.model.localsave

import com.google.gson.annotations.SerializedName

data class BoundaryProjectResponse(

	@field:SerializedName("geom")
	val geom: List<GeomItem?>? = null
)

data class GeomItem(

	@field:SerializedName("lat")
	val lat: Double? = null,

	@field:SerializedName("long")
	val jsonMemberLong: Double? = null
)
