package mki.siojt2.model.localsave

import com.google.gson.annotations.SerializedName

data class OverlayImagesResponse(
		@field:SerializedName("position")
	val position: Position? = null,

		@field:SerializedName("url")
	val url: String? = null
)

data class TopRight(
	@field:SerializedName("lat")
	val lat: Double? = null,

	@field:SerializedName("long")
	val jsonMemberLong: Double? = null
)

data class TopLeft(
	@field:SerializedName("lat")
	val lat: Double? = null,

	@field:SerializedName("long")
	val jsonMemberLong: Double? = null
)

data class Position(
		@field:SerializedName("bottomLeft")
	val bottomLeft: BottomLeft? = null,

		@field:SerializedName("bottomRight")
	val bottomRight: BottomRight? = null,

		@field:SerializedName("topLeft")
	val topLeft: TopLeft? = null,

		@field:SerializedName("topRight")
	val topRight: TopRight? = null
)

data class BottomRight(
	@field:SerializedName("lat")
	val lat: Double? = null,

	@field:SerializedName("long")
	val jsonMemberLong: Double? = null
)
data class BottomLeft(

	@field:SerializedName("lat")
	val lat: Double? = null,

	@field:SerializedName("long")
	val jsonMemberLong: Double? = null
)
