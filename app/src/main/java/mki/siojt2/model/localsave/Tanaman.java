package mki.siojt2.model.localsave;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import mki.siojt2.model.local_save_image.DataImageLand;
import mki.siojt2.model.local_save_image.DataImagePlants;

/**
 * Created by Endra on 2019-11-02.
 */
/* @Parcel(implementations = {io.realm.mki_siojt2_model_localsave_TanamanRealmProxy.class},
        value = Parcel.Serialization.BEAN,
        analyze = {Tanaman.class})*/
public class Tanaman extends RealmObject {

    @PrimaryKey
    private Integer TanamanIdTemp;

    private Integer TanamanId;
    private Integer ProjectId;
    private Integer LandIdTemp;
    private Integer LandId;

    private String PemilikTanamanId;
    private String PemilikTanaman;
    private String JenisTanamanId;
    private String JenisTanamanNama;
    private String NamaTanamanId;
    private String NamaTanaman;
    private String NamaTanamanOther;
    private String UkuranTanamanId;
    private String UkuranTanaman;
    private String JumlahTanaman;
    private RealmList<DataImagePlants> dataImagePlants;

    private Integer isCreate;

    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getIsCreate() {
        return isCreate;
    }

    public void setIsCreate(Integer isCreate) {
        this.isCreate = isCreate;
    }

    public Integer getTanamanIdTemp() {
        return TanamanIdTemp;
    }

    public void setTanamanIdTemp(Integer tanamanIdTemp) {
        TanamanIdTemp = tanamanIdTemp;
    }

    public Integer getTanamanId() {
        return TanamanId;
    }

    public void setTanamanId(Integer tanamanId) {
        TanamanId = tanamanId;
    }

    public Integer getProjectId() {
        return ProjectId;
    }

    public void setProjectId(Integer projectId) {
        ProjectId = projectId;
    }

    public Integer getLandIdTemp() {
        return LandIdTemp;
    }

    public void setLandIdTemp(Integer landIdTemp) {
        LandIdTemp = landIdTemp;
    }

    public Integer getLandId() {
        return LandId;
    }

    public void setLandId(Integer landId) {
        LandId = landId;
    }

    public String getPemilikTanamanId() {
        return PemilikTanamanId;
    }

    public void setPemilikTanamanId(String pemilikTanamanId) {
        PemilikTanamanId = pemilikTanamanId;
    }

    public String getPemilikTanaman() {
        return PemilikTanaman;
    }

    public void setPemilikTanaman(String pemilikTanaman) {
        PemilikTanaman = pemilikTanaman;
    }

    public String getJenisTanamanId() {
        return JenisTanamanId;
    }

    public void setJenisTanamanId(String jenisTanamanId) {
        JenisTanamanId = jenisTanamanId;
    }

    public String getJenisTanamanNama() {
        return JenisTanamanNama;
    }

    public void setJenisTanamanNama(String jenisTanamanNama) {
        JenisTanamanNama = jenisTanamanNama;
    }

    public String getNamaTanamanId() {
        return NamaTanamanId;
    }

    public void setNamaTanamanId(String namaTanamanId) {
        NamaTanamanId = namaTanamanId;
    }

    public String getNamaTanaman() {
        return NamaTanaman;
    }

    public void setNamaTanaman(String namaTanaman) {
        NamaTanaman = namaTanaman;
    }

    public String getUkuranTanamanId() {
        return UkuranTanamanId;
    }

    public void setUkuranTanamanId(String ukuranTanamanId) {
        UkuranTanamanId = ukuranTanamanId;
    }

    public String getUkuranTanaman() {
        return UkuranTanaman;
    }

    public void setUkuranTanaman(String ukuranTanaman) {
        UkuranTanaman = ukuranTanaman;
    }

    public String getJumlahTanaman() {
        return JumlahTanaman;
    }

    public void setJumlahTanaman(String jumlahTanaman) {
        JumlahTanaman = jumlahTanaman;
    }

    public RealmList<DataImagePlants> getDataImagePlants() {
        return dataImagePlants;
    }

    public void setDataImagePlants(RealmList<DataImagePlants> dataImagePlants) {
        this.dataImagePlants = dataImagePlants;
    }

    public String getNamaTanamanOther() {
        return NamaTanamanOther;
    }

    public void setNamaTanamanOther(String namaTanamanOther) {
        NamaTanamanOther = namaTanamanOther;
    }
}
