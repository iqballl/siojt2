package mki.siojt2.model.localsave

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import mki.siojt2.model.local_save_image.DataImageRoom

open class Room: RealmObject() {

    @PrimaryKey
    @SerializedName("roomId")
    @Expose
    var roomId: Int? = null

    var ProjectId: Int? = null
    var LandIdTemp: Int? = null

    //Form Ruang Tanah 1
    var landNumberList: String? = null
    var ownerDataId: String? = null
    var ownerDataName: String? = null
    var year: String? = null
    var upperRoom: Int? = null
    var lowerRoom: Int? = null

    //Form Ruang Tanah 2
    var notes: String? = null
    var dataImageRoom: RealmList<DataImageRoom>? = null
}