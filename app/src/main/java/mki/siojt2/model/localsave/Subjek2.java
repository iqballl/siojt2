package mki.siojt2.model.localsave;

import io.realm.RealmObject;

/**
 * Created by Endra on 2019-10-31.
 */
public class Subjek2 extends RealmObject {
    private Integer SubjekIdTemp;
    private Integer SubjekId;
    private Integer ProjectId;
    private Integer LandIdTemp;
    private Integer LandId;
    private String ProjectPartyType;

//    private Integer Subjek2IdentitySourceId;
//    private String Subjek2IdentitySourceName;
//    private Integer Subjek2OwnerType;
//    private String Subjek2IdentitySourceOther;
    private String Subjek2FullName;
    private String Subjek2BirthPlaceCode;
    private String Subjek2BirthPlaceName;
    private String Subjek2BirthPlaceOther;
    private String Subjek2BirthDate;
    private String Subjek2OccupationId;
    private String Subjek2OccupationName;
    private String Subjek2OccupationOther;
    private String Subjek2ProvinceCode;
    private String Subjek2ProvinceName;
    private String Subjek2RegencyCode;
    private String Subjek2RegencyName;
    private String Subjek2DistrictCode;
    private String Subjek2DistrictName;
    private String Subjek2VillageId;
    private String Subjek2VillageName;
    private String Subjek2RT;
    private String Subjek2RW;
    private String Subjek2Number;
    private String Subjek2Block;
    private String Subjek2StreetName;
    private String Subjek2ComplexName;
    private String Subjek2PostalCode;
    private String Subjek2NPWP;
    private Integer Subjek2PenguasaanTanahId;
    private String Subjek2PenguasaanTanahName;
    private String Subjek2YearStayBegin;
    private String Subjek2IdentityNumber;
//    private String LivelihoodLiveId;
//    private String LivelihoodLiveName;
//    private String  LivelihoodOther;
//    private String  LivelihoodIncome;
    private Integer Subjek2Status;
    private Integer Subjek2CreateBy;

    private Integer isCreate;

    public Integer getSubjekIdTemp() {
        return SubjekIdTemp;
    }

    public void setSubjekIdTemp(Integer subjekIdTemp) {
        SubjekIdTemp = subjekIdTemp;
    }

    public Integer getSubjekId() {
        return SubjekId;
    }

    public void setSubjekId(Integer subjekId) {
        SubjekId = subjekId;
    }

    public Integer getProjectId() {
        return ProjectId;
    }

    public void setProjectId(Integer projectId) {
        ProjectId = projectId;
    }

    public Integer getLandIdTemp() {
        return LandIdTemp;
    }

    public void setLandIdTemp(Integer landIdTemp) {
        LandIdTemp = landIdTemp;
    }

    public Integer getLandId() {
        return LandId;
    }

    public void setLandId(Integer landId) {
        LandId = landId;
    }

    public String getProjectPartyType() {
        return ProjectPartyType;
    }

    public void setProjectPartyType(String projectPartyType) {
        ProjectPartyType = projectPartyType;
    }

//    public Integer getSubjek2IdentitySourceId() {
//        return Subjek2IdentitySourceId;
//    }
//
//    public void setSubjek2IdentitySourceId(Integer subjek2IdentitySourceId) {
//        Subjek2IdentitySourceId = subjek2IdentitySourceId;
//    }
//
//    public String getSubjek2IdentitySourceName() {
//        return Subjek2IdentitySourceName;
//    }
//
//    public void setSubjek2IdentitySourceName(String subjek2IdentitySourceName) {
//        Subjek2IdentitySourceName = subjek2IdentitySourceName;
//    }
//
//    public Integer getSubjek2OwnerType() {
//        return Subjek2OwnerType;
//    }
//
//    public void setSubjek2OwnerType(Integer subjek2OwnerType) {
//        Subjek2OwnerType = subjek2OwnerType;
//    }
//
//    public String getSubjek2IdentitySourceOther() {
//        return Subjek2IdentitySourceOther;
//    }
//
//    public void setSubjek2IdentitySourceOther(String subjek2IdentitySourceOther) {
//        Subjek2IdentitySourceOther = subjek2IdentitySourceOther;
//    }

    public String getSubjek2FullName() {
        return Subjek2FullName;
    }

    public void setSubjek2FullName(String subjek2FullName) {
        Subjek2FullName = subjek2FullName;
    }

    public String getSubjek2BirthPlaceCode() {
        return Subjek2BirthPlaceCode;
    }

    public void setSubjek2BirthPlaceCode(String subjek2BirthPlaceCode) {
        Subjek2BirthPlaceCode = subjek2BirthPlaceCode;
    }

    public String getSubjek2BirthPlaceName() {
        return Subjek2BirthPlaceName;
    }

    public void setSubjek2BirthPlaceName(String subjek2BirthPlaceName) {
        Subjek2BirthPlaceName = subjek2BirthPlaceName;
    }

    public String getSubjek2BirthPlaceOther() {
        return Subjek2BirthPlaceOther;
    }

    public void setSubjek2BirthPlaceOther(String subjek2BirthPlaceOther) {
        Subjek2BirthPlaceOther = subjek2BirthPlaceOther;
    }

    public String getSubjek2BirthDate() {
        return Subjek2BirthDate;
    }

    public void setSubjek2BirthDate(String subjek2BirthDate) {
        Subjek2BirthDate = subjek2BirthDate;
    }

    public String getSubjek2OccupationId() {
        return Subjek2OccupationId;
    }

    public void setSubjek2OccupationId(String subjek2OccupationId) {
        Subjek2OccupationId = subjek2OccupationId;
    }

    public String getSubjek2OccupationName() {
        return Subjek2OccupationName;
    }

    public void setSubjek2OccupationName(String subjek2OccupationName) {
        Subjek2OccupationName = subjek2OccupationName;
    }

    public String getSubjek2OccupationOther() {
        return Subjek2OccupationOther;
    }

    public void setSubjek2OccupationOther(String subjek2OccupationOther) {
        Subjek2OccupationOther = subjek2OccupationOther;
    }

    public String getSubjek2ProvinceCode() {
        return Subjek2ProvinceCode;
    }

    public void setSubjek2ProvinceCode(String subjek2ProvinceCode) {
        Subjek2ProvinceCode = subjek2ProvinceCode;
    }

    public String getSubjek2ProvinceName() {
        return Subjek2ProvinceName;
    }

    public void setSubjek2ProvinceName(String subjek2ProvinceName) {
        Subjek2ProvinceName = subjek2ProvinceName;
    }

    public String getSubjek2RegencyCode() {
        return Subjek2RegencyCode;
    }

    public void setSubjek2RegencyCode(String subjek2RegencyCode) {
        Subjek2RegencyCode = subjek2RegencyCode;
    }

    public String getSubjek2RegencyName() {
        return Subjek2RegencyName;
    }

    public void setSubjek2RegencyName(String subjek2RegencyName) {
        Subjek2RegencyName = subjek2RegencyName;
    }

    public String getSubjek2DistrictCode() {
        return Subjek2DistrictCode;
    }

    public void setSubjek2DistrictCode(String subjek2DistrictCode) {
        Subjek2DistrictCode = subjek2DistrictCode;
    }

    public String getSubjek2DistrictName() {
        return Subjek2DistrictName;
    }

    public void setSubjek2DistrictName(String subjek2DistrictName) {
        Subjek2DistrictName = subjek2DistrictName;
    }

    public String getSubjek2VillageId() {
        return Subjek2VillageId;
    }

    public void setSubjek2VillageId(String subjek2VillageId) {
        Subjek2VillageId = subjek2VillageId;
    }

    public String getSubjek2VillageName() {
        return Subjek2VillageName;
    }

    public void setSubjek2VillageName(String subjek2VillageName) {
        Subjek2VillageName = subjek2VillageName;
    }

    public String getSubjek2RT() {
        return Subjek2RT;
    }

    public void setSubjek2RT(String subjek2RT) {
        Subjek2RT = subjek2RT;
    }

    public String getSubjek2RW() {
        return Subjek2RW;
    }

    public void setSubjek2RW(String subjek2RW) {
        Subjek2RW = subjek2RW;
    }

    public String getSubjek2Number() {
        return Subjek2Number;
    }

    public void setSubjek2Number(String subjek2Number) {
        Subjek2Number = subjek2Number;
    }

    public String getSubjek2Block() {
        return Subjek2Block;
    }

    public void setSubjek2Block(String subjek2Block) {
        Subjek2Block = subjek2Block;
    }

    public String getSubjek2StreetName() {
        return Subjek2StreetName;
    }

    public void setSubjek2StreetName(String subjek2StreetName) {
        Subjek2StreetName = subjek2StreetName;
    }

    public String getSubjek2ComplexName() {
        return Subjek2ComplexName;
    }

    public void setSubjek2ComplexName(String subjek2ComplexName) {
        Subjek2ComplexName = subjek2ComplexName;
    }

    public String getSubjek2PostalCode() {
        return Subjek2PostalCode;
    }

    public void setSubjek2PostalCode(String subjek2PostalCode) {
        Subjek2PostalCode = subjek2PostalCode;
    }

    public String getSubjek2NPWP() {
        return Subjek2NPWP;
    }

    public void setSubjek2NPWP(String subjek2NPWP) {
        Subjek2NPWP = subjek2NPWP;
    }

    public Integer getSubjek2PenguasaanTanahId() {
        return Subjek2PenguasaanTanahId;
    }

    public void setSubjek2PenguasaanTanahId(Integer subjek2PenguasaanTanahId) {
        Subjek2PenguasaanTanahId = subjek2PenguasaanTanahId;
    }

    public String getSubjek2PenguasaanTanahName() {
        return Subjek2PenguasaanTanahName;
    }

    public void setSubjek2PenguasaanTanahName(String subjek2PenguasaanTanahName) {
        Subjek2PenguasaanTanahName = subjek2PenguasaanTanahName;
    }

    public String getSubjek2YearStayBegin() {
        return Subjek2YearStayBegin;
    }

    public void setSubjek2YearStayBegin(String subjek2YearStayBegin) {
        Subjek2YearStayBegin = subjek2YearStayBegin;
    }

    public String getSubjek2IdentityNumber() {
        return Subjek2IdentityNumber;
    }

    public void setSubjek2IdentityNumber(String subjek2IdentityNumber) {
        Subjek2IdentityNumber = subjek2IdentityNumber;
    }

//    public String getLivelihoodLiveId() {
//        return LivelihoodLiveId;
//    }
//
//    public void setLivelihoodLiveId(String livelihoodLiveId) {
//        LivelihoodLiveId = livelihoodLiveId;
//    }
//
//    public String getLivelihoodLiveName() {
//        return LivelihoodLiveName;
//    }
//
//    public void setLivelihoodLiveName(String livelihoodLiveName) {
//        LivelihoodLiveName = livelihoodLiveName;
//    }
//
//    public String getLivelihoodOther() {
//        return LivelihoodOther;
//    }
//
//    public void setLivelihoodOther(String livelihoodOther) {
//        LivelihoodOther = livelihoodOther;
//    }
//
//    public String getLivelihoodIncome() {
//        return LivelihoodIncome;
//    }
//
//    public void setLivelihoodIncome(String livelihoodIncome) {
//        LivelihoodIncome = livelihoodIncome;
//    }

    public Integer getSubjek2Status() {
        return Subjek2Status;
    }

    public void setSubjek2Status(Integer subjek2Status) {
        Subjek2Status = subjek2Status;
    }

    public Integer getSubjek2CreateBy() {
        return Subjek2CreateBy;
    }

    public void setSubjek2CreateBy(Integer subjek2CreateBy) {
        Subjek2CreateBy = subjek2CreateBy;
    }

    public Integer getIsCreate() {
        return isCreate;
    }

    public void setIsCreate(Integer isCreate) {
        this.isCreate = isCreate;
    }
}
