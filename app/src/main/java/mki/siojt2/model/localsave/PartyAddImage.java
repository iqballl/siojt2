package mki.siojt2.model.localsave;

import io.realm.RealmObject;

public class PartyAddImage extends RealmObject {

    public String imageId;
    public String imagepathLand;
    public String imagenameLand;

    public String getImagepathLand() {
        return this.imagepathLand;
    }

    public void setImagepathLand(String imagepathLand) {
        this.imagepathLand = imagepathLand;
    }

    public String getImagenameLand() {
        return this.imagenameLand;
    }

    public void setImagenameLand(String imagenameLand) {
        this.imagenameLand = imagenameLand;
    }

    public String getImageId() {
        return this.imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }
}