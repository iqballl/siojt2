package mki.siojt2.model.localsave;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Endra on 2019-10-21.
 */
public class PartyAdd extends RealmObject {

    //bayu
    private Integer areaId;

    /* total 41 */
    private Integer TempId;
    private Integer PartyId;

    private Integer ProjectId;
//    private Integer PartyIdentitySourceId;
//    private String partyIdentitySourceName;
//    private Integer PartyOwnerType;
    private String PartyType;

//    private String PartyIdentitySourceOther;
    private String PartyFullName;
    private String PartyBirthPlaceCode;
    private String PartyBirthPlaceName;
    private String PartyBirthPlaceOther;
    private String PartyBirthDate;
    private String PartyOccupationId;
    private String PartyOccupationName;
    private String PartyOccupationOther;
    private String PartyProvinceCode;
    private String PartyProvinceName;
    private String PartyRegencyCode;
    private String PartyRegencyName;
    private String PartyDistrictCode;
    private String PartyDistrictName;
    private String PartyVillageId;
    private String PartyVillageName;
    private String PartyRT;
    private String PartyRW;
    private String PartyNumber;
    private String PartyBlock;
    private String PartyStreetName;
    private String PartyComplexName;
    private String PartyPostalCode;

    private String PartyNPWP;
    private Integer PartyYearStayBegin;
    private String PartyIdentityNumber;
//    private String LivelihoodLiveId;
//    private String LivelihoodLiveName;
//    private String LivelihoodOther;
//    private String LivelihoodIncome;
    private Integer PartyStatus;
    private Integer PartyCreateBy;

    private Integer isCreate;

    private String PartyPhone;
    private String PartyEmail;
    private String PartyOwnershipType;
    private String PartyOwnershipLandData;
    private String PartyNotes;
    private RealmList<String> filePaths;
    private RealmList<PartyAddImage> partyAddImage;
    private RealmList<String> fileName;

    public RealmList<PartyAddImage> getPartyAddImage() {
        return partyAddImage;
    }

    public void setPartyAddImage(RealmList<PartyAddImage> partyAddImage) {
        this.partyAddImage = partyAddImage;
    }

    public RealmList<String> getFilePaths() {
        return filePaths;
    }

    public void setFilePaths(RealmList<String> filePaths) {
        this.filePaths = filePaths;
    }

    public RealmList<String> getFileName() {
        return fileName;
    }

    public void setFileName(RealmList<String> fileName) {
        this.fileName = fileName;
    }

    public String getPartyNotes() {
        return PartyNotes;
    }

    public void setPartyNotes(String partyNotes) {
        PartyNotes = partyNotes;
    }

    public String getPartyOwnershipType() {
        return PartyOwnershipType;
    }

    public void setPartyOwnershipType(String partyOwnershipType) {
        PartyOwnershipType = partyOwnershipType;
    }

    public String getPartyOwnershipLandData() {
        return PartyOwnershipLandData;
    }

    public void setPartyOwnershipLandData(String partyOwnershipLandData) {
        PartyOwnershipLandData = partyOwnershipLandData;
    }

    public String getPartyPhone() {
        return PartyPhone;
    }

    public void setPartyPhone(String partyPhone) {
        PartyPhone = partyPhone;
    }

    public String getPartyEmail() {
        return PartyEmail;
    }

    public void setPartyEmail(String partyEmail) {
        PartyEmail = partyEmail;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getIsCreate() {
        return isCreate;
    }

    public void setIsCreate(Integer isCreate) {
        this.isCreate = isCreate;
    }

    public Integer getTempId() {
        return TempId;
    }

    public void setTempId(Integer tempId) {
        TempId = tempId;
    }

    public Integer getPartyId() {
        return PartyId;
    }

    public void setPartyId(Integer partyId) {
        PartyId = partyId;
    }

    public Integer getProjectId() {
        return ProjectId;
    }

    public void setProjectId(Integer projectId) {
        ProjectId = projectId;
    }

//    public Integer getPartyIdentitySourceId() {
//        return PartyIdentitySourceId;
//    }
//
//    public void setPartyIdentitySourceId(Integer partyIdentitySourceId) {
//        PartyIdentitySourceId = partyIdentitySourceId;
//    }
//
//    public String getPartyIdentitySourceName() {
//        return partyIdentitySourceName;
//    }
//
//    public void setPartyIdentitySourceName(String partyIdentitySourceName) {
//        this.partyIdentitySourceName = partyIdentitySourceName;
//    }
//
//    public Integer getPartyOwnerType() {
//        return PartyOwnerType;
//    }
//
//    public void setPartyOwnerType(Integer partyOwnerType) {
//        PartyOwnerType = partyOwnerType;
//    }

    public String getPartyType() {
        return PartyType;
    }

    public void setPartyType(String partyType) {
        PartyType = partyType;
    }

//    public String getPartyIdentitySourceOther() {
//        return PartyIdentitySourceOther;
//    }
//
//    public void setPartyIdentitySourceOther(String partyIdentitySourceOther) {
//        PartyIdentitySourceOther = partyIdentitySourceOther;
//    }

    public String getPartyFullName() {
        return PartyFullName;
    }

    public void setPartyFullName(String partyFullName) {
        PartyFullName = partyFullName;
    }

    public String getPartyBirthPlaceCode() {
        return PartyBirthPlaceCode;
    }

    public void setPartyBirthPlaceCode(String partyBirthPlaceCode) {
        PartyBirthPlaceCode = partyBirthPlaceCode;
    }

    public String getPartyBirthPlaceName() {
        return PartyBirthPlaceName;
    }

    public void setPartyBirthPlaceName(String partyBirthPlaceName) {
        PartyBirthPlaceName = partyBirthPlaceName;
    }

    public String getPartyBirthPlaceOther() {
        return PartyBirthPlaceOther;
    }

    public void setPartyBirthPlaceOther(String partyBirthPlaceOther) {
        PartyBirthPlaceOther = partyBirthPlaceOther;
    }

    public String getPartyBirthDate() {
        return PartyBirthDate;
    }

    public void setPartyBirthDate(String partyBirthDate) {
        PartyBirthDate = partyBirthDate;
    }

    public String getPartyOccupationId() {
        return PartyOccupationId;
    }

    public void setPartyOccupationId(String partyOccupationId) {
        PartyOccupationId = partyOccupationId;
    }

    public String getPartyOccupationName() {
        return PartyOccupationName;
    }

    public void setPartyOccupationName(String partyOccupationName) {
        PartyOccupationName = partyOccupationName;
    }

    public String getPartyOccupationOther() {
        return PartyOccupationOther;
    }

    public void setPartyOccupationOther(String partyOccupationOther) {
        PartyOccupationOther = partyOccupationOther;
    }

    public String getPartyProvinceCode() {
        return PartyProvinceCode;
    }

    public void setPartyProvinceCode(String partyProvinceCode) {
        PartyProvinceCode = partyProvinceCode;
    }

    public String getPartyProvinceName() {
        return PartyProvinceName;
    }

    public void setPartyProvinceName(String partyProvinceName) {
        PartyProvinceName = partyProvinceName;
    }

    public String getPartyRegencyCode() {
        return PartyRegencyCode;
    }

    public void setPartyRegencyCode(String partyRegencyCode) {
        PartyRegencyCode = partyRegencyCode;
    }

    public String getPartyRegencyName() {
        return PartyRegencyName;
    }

    public void setPartyRegencyName(String partyRegencyName) {
        PartyRegencyName = partyRegencyName;
    }

    public String getPartyDistrictCode() {
        return PartyDistrictCode;
    }

    public void setPartyDistrictCode(String partyDistrictCode) {
        PartyDistrictCode = partyDistrictCode;
    }

    public String getPartyDistrictName() {
        return PartyDistrictName;
    }

    public void setPartyDistrictName(String partyDistrictName) {
        PartyDistrictName = partyDistrictName;
    }

    public String getPartyVillageId() {
        return PartyVillageId;
    }

    public void setPartyVillageId(String partyVillageId) {
        PartyVillageId = partyVillageId;
    }

    public String getPartyVillageName() {
        return PartyVillageName;
    }

    public void setPartyVillageName(String partyVillageName) {
        PartyVillageName = partyVillageName;
    }

    public String getPartyRT() {
        return PartyRT;
    }

    public void setPartyRT(String partyRT) {
        PartyRT = partyRT;
    }

    public String getPartyRW() {
        return PartyRW;
    }

    public void setPartyRW(String partyRW) {
        PartyRW = partyRW;
    }

    public String getPartyNumber() {
        return PartyNumber;
    }

    public void setPartyNumber(String partyNumber) {
        PartyNumber = partyNumber;
    }

    public String getPartyBlock() {
        return PartyBlock;
    }

    public void setPartyBlock(String partyBlock) {
        PartyBlock = partyBlock;
    }

    public String getPartyStreetName() {
        return PartyStreetName;
    }

    public void setPartyStreetName(String partyStreetName) {
        PartyStreetName = partyStreetName;
    }

    public String getPartyComplexName() {
        return PartyComplexName;
    }

    public void setPartyComplexName(String partyComplexName) {
        PartyComplexName = partyComplexName;
    }

    public String getPartyPostalCode() {
        return PartyPostalCode;
    }

    public void setPartyPostalCode(String partyPostalCode) {
        PartyPostalCode = partyPostalCode;
    }

    public String getPartyNPWP() {
        return PartyNPWP;
    }

    public void setPartyNPWP(String partyNPWP) {
        PartyNPWP = partyNPWP;
    }

    public Integer getPartyYearStayBegin() {
        return PartyYearStayBegin;
    }

    public void setPartyYearStayBegin(Integer partyYearStayBegin) {
        PartyYearStayBegin = partyYearStayBegin;
    }

    public String getPartyIdentityNumber() {
        return PartyIdentityNumber;
    }

    public void setPartyIdentityNumber(String partyIdentityNumber) {
        PartyIdentityNumber = partyIdentityNumber;
    }

//    public String getLivelihoodLiveId() {
//        return LivelihoodLiveId;
//    }
//
//    public void setLivelihoodLiveId(String livelihoodLiveId) {
//        LivelihoodLiveId = livelihoodLiveId;
//    }
//
//    public String getLivelihoodLiveName() {
//        return LivelihoodLiveName;
//    }
//
//    public void setLivelihoodLiveName(String livelihoodLiveName) {
//        LivelihoodLiveName = livelihoodLiveName;
//    }
//
//    public String getLivelihoodOther() {
//        return LivelihoodOther;
//    }
//
//    public void setLivelihoodOther(String livelihoodOther) {
//        LivelihoodOther = livelihoodOther;
//    }
//
//    public String getLivelihoodIncome() {
//        return LivelihoodIncome;
//    }
//
//    public void setLivelihoodIncome(String livelihoodIncome) {
//        LivelihoodIncome = livelihoodIncome;
//    }

    public Integer getPartyStatus() {
        return PartyStatus;
    }

    public void setPartyStatus(Integer partyStatus) {
        PartyStatus = partyStatus;
    }

    public Integer getPartyCreateBy() {
        return PartyCreateBy;
    }

    public void setPartyCreateBy(Integer partyCreateBy) {
        PartyCreateBy = partyCreateBy;
    }
}
