package mki.siojt2.model.response_data_objek

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListDetailFasilitasLain(

	@field:SerializedName("OtherFacilityDetail")
	var otherFacilityDetail: List<OtherFacilityDetailItem?>? = null
)