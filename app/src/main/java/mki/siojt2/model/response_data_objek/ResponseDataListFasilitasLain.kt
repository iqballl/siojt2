package mki.siojt2.model.response_data_objek

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListFasilitasLain(

        @field:SerializedName("POtherFacilityProjectLandId")
        var pOtherFacilityProjectLandId: Int? = null,

        @field:SerializedName("POtherFacilityFacilityTypeId")
        var pOtherFacilityFacilityTypeId: Int? = null,

        @field:SerializedName("PartyFullName")
        var partyFullName: String? = null,

        @field:SerializedName("FacilityTypeName")
        var facilityTypeName: String? = null,

        @field:SerializedName("POtherFacilityName")
        var pOtherFacilityName: String? = null,

        @field:SerializedName("PartyIdentityNumber")
        var partyIdentityNumber: String? = null,

        @field:SerializedName("POtherFacilityProjectPartyId")
        var pOtherFacilityProjectPartyId: Int? = null
)