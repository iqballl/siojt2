package mki.siojt2.model.response_data_objek

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListTanaman(

        @field:SerializedName("PPlantProjectPartyId")
        var pPlantProjectPartyId: Int? = null,

        @field:SerializedName("ProjectPartyCreateBy")
        var projectPartyCreateBy: Int? = null,

        @field:SerializedName("ProjectPartyType")
        var projectPartyType: Int? = null,

        @field:SerializedName("PartyFullName")
        var partyFullName: String? = null,

        @field:SerializedName("ProjectPartyPartyId")
        var projectPartyPartyId: Int? = null,

        @field:SerializedName("PPlantCreateBy")
        var pPlantCreateBy: Int? = null,

        @field:SerializedName("ProjectPartyLogId")
        var projectPartyLogId: Any? = null,

        @field:SerializedName("PPlantId")
        var pPlantId: Int? = null,

        @field:SerializedName("PPlantNumber")
        var pPlantNumber: Int? = null,

        @field:SerializedName("PPlantPlantId")
        var pPlantPlantId: Int? = null,

        @field:SerializedName("PPlantLogId")
        var pPlantLogId: Any? = null,

        @field:SerializedName("PPlantCreateDate")
        var pPlantCreateDate: String? = null,

        @field:SerializedName("PPlantStatus")
        var pPlantStatus: Int? = null,

        @field:SerializedName("PPlantPlantSizeText")
        var pPlantPlantSizeText: String? = null,

        @field:SerializedName("ProjectPartyProjectId")
        var projectPartyProjectId: Int? = null,

        @field:SerializedName("PPlantPlantSizeId")
        var pPlantPlantSizeId: Int? = null,

        @field:SerializedName("PPlantPlantTypeText")
        var pPlantPlantTypeText: String? = null,

        @field:SerializedName("ProjectPartyId")
        var projectPartyId: Int? = null,

        @field:SerializedName("PPlantProjectLandId")
        var pPlantProjectLandId: Int? = null,

        @field:SerializedName("ProjectPartyLandId")
        var projectPartyLandId: Int? = null,

        @field:SerializedName("PPlantPlantType")
        var pPlantPlantType: Int? = null,

        @field:SerializedName("PlantDescription")
        var plantDescription: String? = null,

        @field:SerializedName("PlantName")
        var plantName: String? = null,

        @field:SerializedName("ProjectPartyStatus")
        var projectPartyStatus: Int? = null,

        @field:SerializedName("PartyIdentityNumber")
        var partyIdentityNumber: String? = null,

        @field:SerializedName("ProjectPartyCreateDate")
        var projectPartyCreateDate: String? = null
){
        override fun toString(): String {
                return plantName.toString()
        }
}