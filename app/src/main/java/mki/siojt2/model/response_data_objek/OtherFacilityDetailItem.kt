package mki.siojt2.model.response_data_objek

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class OtherFacilityDetailItem(

	@field:SerializedName("ProjectPartyProjectId")
	var projectPartyProjectId: Int? = null,

	@field:SerializedName("ProjectPartyCreateBy")
	var projectPartyCreateBy: Int? = null,

	@field:SerializedName("ProjectPartyType")
	var projectPartyType: Int? = null,

	@field:SerializedName("PartyFullName")
	var partyFullName: String? = null,

	@field:SerializedName("ProjectPartyId")
	var projectPartyId: Int? = null,

	@field:SerializedName("FacilityTypeName")
	var facilityTypeName: String? = null,

	@field:SerializedName("ProjectPartyPartyId")
	var projectPartyPartyId: Int? = null,

	@field:SerializedName("ProjectPartyLandId")
	var projectPartyLandId: Int? = null,

	@field:SerializedName("ProjectPartyLogId")
	var projectPartyLogId: Any? = null,

	@field:SerializedName("POtherFacilityLogId")
	var pOtherFacilityLogId: Any? = null,

	@field:SerializedName("ProjectPartyStatus")
	var projectPartyStatus: Int? = null,

	@field:SerializedName("POtherFacilityProjectLandId")
	var pOtherFacilityProjectLandId: Int? = null,

	@field:SerializedName("POtherFacilityFacilityTypeId")
	var pOtherFacilityFacilityTypeId: Int? = null,

	@field:SerializedName("POtherFacilityCreateBy")
	var pOtherFacilityCreateBy: Int? = null,

	@field:SerializedName("POtherFacilityCreateDate")
	var pOtherFacilityCreateDate: String? = null,

	@field:SerializedName("POtherFacilityName")
	var pOtherFacilityName: String? = null,

	@field:SerializedName("POtherFacilityValue")
	var pOtherFacilityValue: String? = null,

	@field:SerializedName("POtherFacilityId")
	var pOtherFacilityId: Int? = null,

	@field:SerializedName("PartyIdentityNumber")
	var partyIdentityNumber: String? = null,

	@field:SerializedName("POtherFacilityProjectPartyId")
	var pOtherFacilityProjectPartyId: Int? = null,

	@field:SerializedName("ProjectPartyCreateDate")
	var projectPartyCreateDate: String? = null,

	@field:SerializedName("FacilityTypeDescription")
	var facilityTypeDescription: Any? = null,

	@field:SerializedName("POtherFacilityStatus")
	var pOtherFacilityStatus: Int? = null
)