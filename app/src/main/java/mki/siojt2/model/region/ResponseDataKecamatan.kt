package mki.siojt2.model.region

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataKecamatan(

	@field:SerializedName("DistrictLogId")
	var districtLogId: Any? = null,

	@field:SerializedName("DistrictName")
	var districtName: String? = null,

	@field:SerializedName("DistrictCreateDate")
	var districtCreateDate: Any? = null,

	@field:SerializedName("DistrictId")
	var districtId: Int? = null,

	@field:SerializedName("DistrictRegencyCode")
	var districtRegencyCode: String? = null,

	@field:SerializedName("DistrictStatus")
	var districtStatus: Int? = null,

	@field:SerializedName("DistrictCreateBy")
	var districtCreateBy: Any? = null,

	@field:SerializedName("DistrictCode")
	var districtCode: String? = null
)