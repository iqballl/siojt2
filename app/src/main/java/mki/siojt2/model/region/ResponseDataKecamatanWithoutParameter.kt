package mki.siojt2.model.region

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class ResponseDataKecamatanWithoutParameter : RealmObject(){

    @PrimaryKey
    @SerializedName("DistrictId")
    @Expose
    private var districtId: Int? = null

    @SerializedName("DistrictCode")
    @Expose
    private var districtCode: String? = ""

    @field:SerializedName("DistrictRegencyCode")
    @Expose
    private var districtRegencyCode: String? = ""

    @SerializedName("DistrictName")
    @Expose
    private var districtName: String? = ""

    @SerializedName("DistrictStatus")
    @Expose
    private var districtStatus: Int? = null


    fun getdistrictId(): Int? {
        return districtId
    }

    fun setdistrictId(districtId: Int?) {
        this.districtId = districtId
    }

    fun getdistrictCode(): String? {
        return districtCode
    }

    fun setdistrictCode(districtCode: String) {
        this.districtCode = districtCode
    }

    fun getdistrictRegencyCode(): String? {
        return districtRegencyCode
    }

    fun setdistrictRegencyCode(districtRegencyCode: String) {
        this.districtRegencyCode = districtRegencyCode
    }


    fun getdistrictName(): String? {
        return districtName
    }

    fun setdistrictName(districtName: String) {
        this.districtName = districtName
    }

    fun getdistrictStatus(): Int? {
        return districtStatus
    }

    fun setdistrictStatus(districtStatus: Int?) {
        this.districtStatus = districtStatus
    }

    override fun toString(): String {
        return districtName.toString()
    }

}
