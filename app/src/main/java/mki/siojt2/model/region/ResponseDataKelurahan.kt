package mki.siojt2.model.region

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataKelurahan(

	@field:SerializedName("VillageCreateDate")
	var villageCreateDate: Any? = null,

	@field:SerializedName("VillageName")
	var villageName: String? = null,

	@field:SerializedName("VillageLogId")
	var villageLogId: Any? = null,

	@field:SerializedName("VillageCode")
	var villageCode: String? = null,

	@field:SerializedName("VillageStatus")
	var villageStatus: Int? = null,

	@field:SerializedName("VillageCreateBy")
	var villageCreateBy: Any? = null,

	@field:SerializedName("VillageId")
	var villageId: Int? = null,

	@field:SerializedName("VillageDistrictCode")
	var villageDistrictCode: String? = null
)