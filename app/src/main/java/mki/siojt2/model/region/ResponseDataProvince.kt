package mki.siojt2.model.region

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataProvince(

	@field:SerializedName("ProvinceName")
	var provinceName: String? = null,

	@field:SerializedName("ProvinceId")
	var provinceId: Int? = null,

	@field:SerializedName("ProvinceCountryCode")
	var provinceCountryCode: String? = null,

	@field:SerializedName("ProvinceLogId")
	var provinceLogId: Any? = null,

	@field:SerializedName("ProvinceCode")
	var provinceCode: String? = null,

	@field:SerializedName("ProvinceCreateDate")
	var provinceCreateDate: Any? = null,

	@field:SerializedName("ProvinceCreateBy")
	var provinceCreateBy: Int? = null,

	@field:SerializedName("ProvinceStatus")
	var provinceStatus: Int? = null
)