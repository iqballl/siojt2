package mki.siojt2.model.region

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ResponseDataProvinceWithoutParameter : RealmObject() {

    @SerializedName("ProvinceId")
    @Expose
    private var provinceId: Int? = null

    @SerializedName("ProvinceCode")
    @Expose
    private var provinceCode: String? = ""

    @SerializedName("ProvinceName")
    @Expose
    private var provinceName: String? = ""

    @SerializedName("ProvinceStatus")
    @Expose
    private var provinceStatus: Int? = null


    fun getPropinceId(): Int? {
        return provinceId
    }

    fun setPropinceId(provinceId: Int?) {
        this.provinceId = provinceId
    }

    fun getProvinceCode(): String? {
        return provinceCode
    }

    fun setProvinceCode(provinceCode: String) {
        this.provinceCode = provinceCode
    }


    fun getProvinceName(): String? {
        return provinceName
    }

    fun setProvinceName(provinceName: String) {
        this.provinceName = provinceName
    }

    fun getProvinceStatus(): Int? {
        return provinceStatus
    }

    fun setProvineStatus(provinceStatus: Int?) {
        this.provinceStatus = provinceStatus
    }

    override fun toString(): String {
        return provinceName.toString()
    }

}
