package mki.siojt2.model.region

import com.google.gson.annotations.Expose
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class ResponseDataKelurahanWithoutParameter : RealmObject() {

    @PrimaryKey
    @SerializedName("VillageId")
    @Expose
    private var villageId: Int? = null

    @SerializedName("VillageCode")
    @Expose
    private var villageCode: String? = ""

    @field:SerializedName("VillageDistrictCode")
    @Expose
    private var villageDistrictCode: String? = ""

    @SerializedName("VillageName")
    @Expose
    private var villageName: String? = ""

    @SerializedName("VillageStatus")
    @Expose
    private var villageStatus: Int? = null


    fun gevillagetId(): Int? {
        return villageId
    }

    fun setvillageId(villageId: Int?) {
        this.villageId = villageId
    }

    fun getvillageCode(): String? {
        return villageCode
    }

    fun setvillageCode(villageCode: String) {
        this.villageCode = villageCode
    }

    /* -- */
    fun getvillageDistrictCode(): String? {
        return villageDistrictCode
    }

    fun setvillageDistrictCode(villageDistrictCode: String) {
        this.villageDistrictCode = villageDistrictCode
    }

    fun getvillageName(): String? {
        return villageName
    }

    fun setvillageName(villageName: String) {
        this.villageName = villageName
    }

    fun getvillageStatus(): Int? {
        return villageStatus
    }

    fun sevillageStatus(villageStatus: Int?) {
        this.villageStatus = villageStatus
    }

    override fun toString(): String {
        return villageName.toString()
    }

}
