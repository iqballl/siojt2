package mki.siojt2.model.region

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

/**
 * Created by Endra on 2019-10-16.
 */
open class ResponseDataCityWithoutParameter : RealmObject(){
    @SerializedName("RegencyId")
    @Expose
    private var regencyId: Int? = null
    @SerializedName("RegencyCode")
    @Expose
    private var regencyCode: String? = ""
    @SerializedName("RegencyProvinceCode")
    @Expose
    private var regencyProvinceCode: String? = ""
    @SerializedName("RegencyName")
    @Expose
    private var regencyName: String? = ""
    @SerializedName("RegencyStatus")
    @Expose
    private var regencyStatus: Int? = null

    fun getRegencyId(): Int? {
        return regencyId
    }

    fun setRegencyId(regencyId: Int?) {
        this.regencyId = regencyId
    }

    fun getRegencyCode(): String? {
        return regencyCode
    }

    fun setRegencyCode(regencyCode: String) {
        this.regencyCode = regencyCode
    }

    fun getRegencyProvinceCode(): String? {
        return regencyProvinceCode
    }

    fun setRegencyProvinceCode(regencyProvinceCode: String) {
        this.regencyProvinceCode = regencyProvinceCode
    }

    fun getRegencyName(): String? {
        return regencyName
    }

    fun setRegencyName(regencyName: String) {
        this.regencyName = regencyName
    }


    fun getRegencyStatus(): Int? {
        return regencyStatus
    }

    fun setRegencyStatus(regencyStatus: Int?) {
        this.regencyStatus = regencyStatus
    }

    override fun toString(): String {
        return regencyName.toString()
    }

}