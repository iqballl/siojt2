package mki.siojt2.model.region

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataCity(

	@field:SerializedName("RegencyCode")
	var regencyCode: String? = null,

	@field:SerializedName("RegencyCreateBy")
	var regencyCreateBy: Any? = null,

	@field:SerializedName("RegencyCreateDate")
	var regencyCreateDate: Any? = null,

	@field:SerializedName("RegencyId")
	var regencyId: Int? = null,

	@field:SerializedName("RegencyProvinceCode")
	var regencyProvinceCode: String? = null,

	@field:SerializedName("RegencyStatus")
	var regencyStatus: Int? = null,

	@field:SerializedName("RegencyLogId")
	var regencyLogId: Any? = null,

	@field:SerializedName("RegencyName")
	var regencyName: String? = null
){
	override fun toString(): String {
		return regencyName.toString()
	}
}