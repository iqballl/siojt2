package mki.siojt2.model.data_form_subjek

import android.net.Uri
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import mki.siojt2.model.localsave.PartyAddImage

data class DataFormSubjek2(
        @field:SerializedName("ProjectPartyType")
        var projectPartyType: String? = "",

        @field:SerializedName("ProjectPartyTypeName")
        var projectPartyTypeName: String? = "",

//        @field:SerializedName("PartyIdentitySourceId")
//        var partyIdentitySourceId: String? = "",
//
//        @field:SerializedName("PartyIdentitySourceName")
//        var partyIdentitySourceName: String? = "",
//
//        @field:SerializedName("PartyIdentitySourceOther")
//        var partyIdentitySourceOther: String? = "",
//
//        @field:SerializedName("PartyIdentityNumber")
//        var partyIdentityNumber: String? = "",

        @field:SerializedName("PartyNPWP")
        var partyNPWP: String? = "",

        @field:SerializedName("PartyYearStayBegin")
        var partyYearStayBegin: String? = "",

        var ownershipType: String?,
        var ownershipLand: String?,
        var filePaths: RealmList<String?>,
        var fileName: RealmList<String?>,
        var image: MutableList<PartyAddImage?>
)