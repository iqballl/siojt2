package mki.siojt2.model.data_form_subjek

import com.google.gson.annotations.SerializedName

data class DataMataPencaharian(

        @field:SerializedName("LivelihoodLiveId")
        var livelihoodLiveId: String? = "",

        @field:SerializedName("LivelihoodLiveName")
        var livelihoodLiveName: String? = "",

        @field:SerializedName("LivelihoodOther")
        var livelihoodOther: String? = "",

        @field:SerializedName("LivelihoodIncome")
        var livelihoodIncome: String? = ""


)