package mki.siojt2.model.data_form_subjek

import com.google.gson.annotations.SerializedName

data class DataFormSubjek1(

//        @field:SerializedName("PartyOwnerType")
//        var partyOwnerType: String? = "",

        @field:SerializedName("partyFullName")
        var partyFullName: String? = "",

        @field:SerializedName("partyBirthPlaceCode")
        var partyBirthPlaceCode: String? = "",

        @field:SerializedName("partyBirthPlaceName")
        var partyBirthPlaceName: String? = "",

        @field:SerializedName("partyBirthPlaceOther")
        var partyBirthPlaceOther: String? = "",

        @field:SerializedName("partyBirthDate")
        var partyBirthDate: String? = "",

        @field:SerializedName("PartyOccupationId")
        var partyOccupationId: String? = "",

        @field:SerializedName("PartyOccupationName")
        var partyOccupationName: String? = "",

        @field:SerializedName("PartyOccupationOther")
        var partyOccupationOther: String? = "",

        @field:SerializedName("PartyProvinceCode")
        var partyProvinceCode: String? = "",

        @field:SerializedName("PartyProvinceName")
        var partyProvinceName: String? = "",

        @field:SerializedName("PartyRegencyCode")
        var partyRegencyCode: String? = "",

        @field:SerializedName("PartyRegencyName")
        var partyRegencyName: String? = "",

        @field:SerializedName("PartyDistrictCode")
        var partyDistrictCode: String? = "",

        @field:SerializedName("PartyDistrictName")
        var partyDistrictName: String? = "",

        @field:SerializedName("PartyVillageName")
        var partyVillageName: String? = "",

        @field:SerializedName("PartyVillage")
        var partyVillage: String? = "",

        @field:SerializedName("PartyBlock")
        var partyBlock: String? = "",

        @field:SerializedName("PartyNumber")
        var partyNumber: String? = "",

        @field:SerializedName("PartyComplexName")
        var partyComplexName: String? = "",

        @field:SerializedName("PartyStreetName")
        var partyStreetName: String? = "",

        @field:SerializedName("partyRT")
        var partyRT: String? = "",

        @field:SerializedName("PartyRW")
        var partyRW: String? = "",

        @field:SerializedName("PartyPostalCode")
        var partyPostalCode: String? = "",

        var temporaryFieldNumber: String?,
        var nik: String?,
        var phone: String?,
        var email: String?

)