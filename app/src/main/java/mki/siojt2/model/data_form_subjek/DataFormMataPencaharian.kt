package mki.siojt2.model.data_form_subjek

import com.google.gson.annotations.SerializedName

data class DataFormMataPencaharian(

        @field:SerializedName("DataMataPencaharian")
        var dataMataPencaharian: List<DataMataPencaharian>? = null

)