package mki.siojt2.model.merge_result

import mki.siojt2.model.master_data_sarana_lain.ResponseDataJenisFasilitasWithOutParam
import mki.siojt2.model.master_data_subjek.ResponseDataMataPencaharianWithOutParam
import mki.siojt2.model.master_data_subjek.ResponseDataPekerjaanWithOutParam
import mki.siojt2.model.master_data_tanaman.ResponseDataListKategoriRumpunWithOutParam
import mki.siojt2.model.master_data_tanaman.ResponseDataListKategoriTanamanWithOutParam

data class MergedResponseDataReferenceDinamis(val listOfKategoriTanaman: MutableList<ResponseDataListKategoriTanamanWithOutParam>,
                                              val listOfKategoriRumpun: MutableList<ResponseDataListKategoriRumpunWithOutParam>,
                                              val listOfMataPencaharian: MutableList<ResponseDataMataPencaharianWithOutParam>,
                                              val listOfPekerjaan: MutableList<ResponseDataPekerjaanWithOutParam>,
                                              val listOfJenisFasilitas: MutableList<ResponseDataJenisFasilitasWithOutParam>) {
    // this is just a POJO to store all the responses in one object
    val mlistOfKategoriTanaman: MutableList<ResponseDataListKategoriTanamanWithOutParam> = listOfKategoriTanaman
    val mlistOfKategoriRumpun: MutableList<ResponseDataListKategoriRumpunWithOutParam> = listOfKategoriRumpun
    val mlistOfMataPencaharian: MutableList<ResponseDataMataPencaharianWithOutParam> = listOfMataPencaharian
    val mlistOfPekerjaan: MutableList<ResponseDataPekerjaanWithOutParam> = listOfPekerjaan
    val mlistOfJenisFasilitas: MutableList<ResponseDataJenisFasilitasWithOutParam> = listOfJenisFasilitas

}

