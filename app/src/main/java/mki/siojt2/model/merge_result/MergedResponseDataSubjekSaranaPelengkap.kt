package mki.siojt2.model.merge_result

import mki.siojt2.model.master_data_sarana_lain.ResponseDataJenisPagarKelilingWithOutParam
import mki.siojt2.model.master_data_sarana_lain.ResponseDataJenisSumberAirWithOutParam
import mki.siojt2.model.master_data_subjek.ResponseDataSumberIdentitasWithOutParam
import mki.siojt2.model.region.ResponseDataCityWithoutParameter
import mki.siojt2.model.region.ResponseDataProvinceWithoutParameter

data class MergedResponseDataSubjekSaranaPelengkap(
        val listOfSumberIdentitas: MutableList<ResponseDataSumberIdentitasWithOutParam>,
        val listOfSumberAir: MutableList<ResponseDataJenisSumberAirWithOutParam>,
        val listOfPagarKeliling: MutableList<ResponseDataJenisPagarKelilingWithOutParam>,
        val listOfProvince: MutableList<ResponseDataProvinceWithoutParameter>,
        val listOfCity: MutableList<ResponseDataCityWithoutParameter>) {
    // this is just a POJO to store all the responses in one object
    val mlistOfSumberIdentitas: MutableList<ResponseDataSumberIdentitasWithOutParam> = listOfSumberIdentitas
    val mlistOfSumberAir: MutableList<ResponseDataJenisSumberAirWithOutParam> = listOfSumberAir
    val mlistOfPagarKeliling: MutableList<ResponseDataJenisPagarKelilingWithOutParam> = listOfPagarKeliling
    var mlistOfProvince: MutableList<ResponseDataProvinceWithoutParameter> = listOfProvince
    var mlistOfCity: MutableList<ResponseDataCityWithoutParameter> = listOfCity

}