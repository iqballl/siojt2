package mki.siojt2.model.merge_result

import mki.siojt2.model.region.ResponseDataCityWithoutParameter
import mki.siojt2.model.region.ResponseDataKecamatanWithoutParameter
import mki.siojt2.model.region.ResponseDataProvinceWithoutParameter

data class MergedResponseRegion(val listOfProvince: MutableList<ResponseDataProvinceWithoutParameter>,
                                val listOfCity: MutableList<ResponseDataCityWithoutParameter>,
                                val listOfKecamtan: MutableList<ResponseDataKecamatanWithoutParameter>) {
    // this is just a POJO to store all the responses in one object
    var mlistOfProvince: MutableList<ResponseDataProvinceWithoutParameter> = listOfProvince
    var mlistOfCity: MutableList<ResponseDataCityWithoutParameter> = listOfCity
    var mlistOfKecamatan: MutableList<ResponseDataKecamatanWithoutParameter> = listOfKecamtan
    //var mlistOfKelurahan: MutableList<ResponseDataKelurahanWithoutParameter> = listOfKelurahan

}