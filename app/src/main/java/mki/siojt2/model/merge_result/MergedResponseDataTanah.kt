package mki.siojt2.model.merge_result

import mki.siojt2.model.master_data_form_tanah.*

data class MergedResponseDataTanah(val listOfPenguasaanAtasTanah: MutableList<ResponseDataListPenguasaanTanahWithOutParam>,
                                   val listOfBentukTanah: MutableList<ResponseDataListBentukTanahWithOutParam>,
                                   val listOfTipographiTanah: MutableList<ResponseDataListTipografiWithOutParam>,
                                   val listOfPosisiTanah: MutableList<ResponseDataListPosisiTanahWithOutParam>,

                                   val listOfKepemilikanTanah: MutableList<ResponseDataListKepemilikanAtasTanahWithOutParam>,
                                   val listOfPenggunaanTanah: MutableList<ResponseDataListPenggunaanTanahWithOutParam>,
                                   val listOfInformasiTambahanTanah: MutableList<ResponseDataListInformasiTambahanTanahWithOutParam>,
                                   val listOfPeruntukanTanah: MutableList<ResponseDataListPeruntukanTanahWithOutParam>) {
    // this is just a POJO to store all the responses in one object
    val mlistOfPenguasaanAtasTanah: MutableList<ResponseDataListPenguasaanTanahWithOutParam> = listOfPenguasaanAtasTanah
    val mlistOfBentukTanah: MutableList<ResponseDataListBentukTanahWithOutParam> = listOfBentukTanah
    val mlistOfTipographiTanah: MutableList<ResponseDataListTipografiWithOutParam> = listOfTipographiTanah
    val mlistOfPosisiTanah: MutableList<ResponseDataListPosisiTanahWithOutParam> = listOfPosisiTanah

    val mlistOfKepemilikanTanah: MutableList<ResponseDataListKepemilikanAtasTanahWithOutParam> = listOfKepemilikanTanah
    val mlistOfPenggunaanTanah: MutableList<ResponseDataListPenggunaanTanahWithOutParam> = listOfPenggunaanTanah
    val mlistOfInformasiTambahanTanah: MutableList<ResponseDataListInformasiTambahanTanahWithOutParam> = listOfInformasiTambahanTanah
    val mlistOfPeruntukanTanah: MutableList<ResponseDataListPeruntukanTanahWithOutParam> = listOfPeruntukanTanah

}