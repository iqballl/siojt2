package mki.siojt2.model.merge_result

import mki.siojt2.model.master_data_bangunan.*

data class MergedResponseDataBangunan(val listOfDindingBangunan: MutableList<ResponseDataListDindingBangunanWithOutParam>,
                                      val listOfKerangkaAtapBangunan: MutableList<ResponseDataListKerangkaAtapBangunanWithOutParam>,
                                      val listOfPenutupAtapBangunan: MutableList<ResponseDataListPenutupAtapBangunanWithOutParam>,
                                      val listOfLantaiBangunan: MutableList<ResponseDataListLantaiBangunanWithOutParam>,
                                      val listOfPintuJendelaBangunan: MutableList<ResponseDataListPintuJendelaBangunanWithOutParam>,

                                      val listOfPlafonBangunan: MutableList<ResponseDataListPlafonBangunanWithOutParam>,
                                      val listOfPondasiBangunan: MutableList<ResponseDataListPondasiBangunanWithOutParam>,
                                      val listOfStukturBangunan: MutableList<ResponseDataListStrukturBangunanWithOutParam>,
                                      val listOfTipeBangunan: MutableList<ResponseDataListTipeBangunanWithOutParam>) {

    // this is just a POJO to store all the responses in one object
    val mlistOfDindingBangunan: MutableList<ResponseDataListDindingBangunanWithOutParam> = listOfDindingBangunan
    val mlistOfKerangkaAtapBangunan: MutableList<ResponseDataListKerangkaAtapBangunanWithOutParam> = listOfKerangkaAtapBangunan
    val mlistOfPenutupAtapBangunan: MutableList<ResponseDataListPenutupAtapBangunanWithOutParam> = listOfPenutupAtapBangunan
    val mlistOfLantaiBangunan: MutableList<ResponseDataListLantaiBangunanWithOutParam> = listOfLantaiBangunan
    val mlistOfPintuJendelaBangunan: MutableList<ResponseDataListPintuJendelaBangunanWithOutParam> = listOfPintuJendelaBangunan

    val mlistOfPlafonBangunan: MutableList<ResponseDataListPlafonBangunanWithOutParam> = listOfPlafonBangunan
    val mlistOfPondasiBangunan: MutableList<ResponseDataListPondasiBangunanWithOutParam> = listOfPondasiBangunan
    val mlistOfStukturBangunan: MutableList<ResponseDataListStrukturBangunanWithOutParam> = listOfStukturBangunan
    val mlistOfTipeBangunan: MutableList<ResponseDataListTipeBangunanWithOutParam> = listOfTipeBangunan

}