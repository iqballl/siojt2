package mki.siojt2.model

class NavigationData {
    var isSelected: Boolean = false
    var name: String? = null
    var drawableId: Int = 0
}