package mki.siojt2.model.data_from_objek_tanaman

import com.google.gson.annotations.SerializedName

data class DataTanaman(

        @field:SerializedName("TanamanId")
        var tanamanId: String? = "",

        @field:SerializedName("TanamanName")
        var tanamanName: String? = "",

        @field:SerializedName("KategoriTanamanId")
        var kategoriTanamanId: String? = "",

        @field:SerializedName("KategoriTanamanName")
        var kategoriTanamanName: String? = "",

        @field:SerializedName("KategoriTanamanNameOther")
        var kategoriTanamanNameOther: String? = "",

        @field:SerializedName("UkuranTanamanId")
        var ukuranTanamanId: String? = "",

        @field:SerializedName("UkuranTanamanName")
        var ukuranTanamanName: String? = "",

        @field:SerializedName("JumlahTanaman")
        var jumlahTanaman: String? = ""




)