package mki.siojt2.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class UAVImagesItem: RealmObject() {
    @SerializedName("position")
    @Expose
    var UAVPosition: UAVPosition? = null

    @SerializedName("url")
    @Expose
    var url: String? = null

    var imgPath: String? = null
}