package mki.siojt2.model.second_development

import io.realm.RealmObject
import io.realm.RealmResults
import io.realm.annotations.LinkingObjects
import io.realm.annotations.PrimaryKey
import mki.siojt2.model.localsave.SaranaLain

/**
 * Created by Iqbal on 15/10/2020.
 */

open class OtherFacilityOfAirConditioner : RealmObject(){

    @PrimaryKey
    var airConditionerId: Int? = null

    var airConditionerFacilityId: Int? = null

    var airConditionerBrand: String? = null

    var airConditionerType: String? = null

    var airConditionerMadeIn: String? = null

    var airConditionerCapacity: Int? = null

    var airConditionerQuantity: Int? = null

    var airConditionerYearProduction: Int? = null

    @LinkingObjects("otherFacilityOfAirConditioner")
    private val msaranaLain: RealmResults<SaranaLain>? = null
}