package mki.siojt2.model.second_development

import io.realm.RealmObject
import io.realm.RealmResults
import io.realm.annotations.LinkingObjects
import io.realm.annotations.PrimaryKey
import mki.siojt2.model.localsave.SaranaLain

/**
 * Created by Iqbal on 15/10/2020.
 */

open class OtherFacilityOfWWTP : RealmObject(){

    @PrimaryKey
    var wwtpId: Int? = null

    var wwtpFacilityId: Int? = null

    var wwtpCapacity: Int? = null

    var wwtpYearBuilt: Int? = null

    @LinkingObjects("otherFacilityOfWWTP")
    val msaranaLain: RealmResults<SaranaLain>? = null
}