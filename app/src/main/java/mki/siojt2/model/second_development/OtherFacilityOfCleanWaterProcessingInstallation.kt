package mki.siojt2.model.second_development

import io.realm.RealmObject
import io.realm.RealmResults
import io.realm.annotations.LinkingObjects
import io.realm.annotations.PrimaryKey
import mki.siojt2.model.localsave.SaranaLain

/**
 * Created by Iqbal on 15/10/2020.
 */

open class OtherFacilityOfCleanWaterProcessingInstallation : RealmObject(){

    @PrimaryKey
    var cleanWaterProcessingInstallationId: Int? = null

    var cleanWaterProcessingInstallationFacilityId: Int? = null

    var cleanWaterProcessingInstallationCapacity: Int? = null

    var cleanWaterProcessingYearProduction: Int? = null

    @LinkingObjects("otherFacilityOfCleanWaterProcessingInstallation")
    val msaranaLain: RealmResults<SaranaLain>? = null
}