package mki.siojt2.model.second_development

import io.realm.RealmObject
import io.realm.RealmResults
import io.realm.annotations.LinkingObjects
import io.realm.annotations.PrimaryKey
import mki.siojt2.model.localsave.SaranaLain

/**
 * Created by Iqbal on 15/10/2020.
 */

open class OtherFacilityOfSwimmingPool : RealmObject(){

    @PrimaryKey
    var swimmingPoolId: Int? = null

    var swimmingPoolFacilityId: Int? = null

    var swimmingPoolLength: Double? = null

    var swimmingPoolWidth: Double? = null

    var swimmingPoolDept: Double? = null

    var swimmingPoolYear: Double? = null

    @LinkingObjects("otherFacilityOfSwimmingPool")
    val msaranaLain: RealmResults<SaranaLain>? = null


}