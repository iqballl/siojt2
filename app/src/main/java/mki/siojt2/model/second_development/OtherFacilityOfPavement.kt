package mki.siojt2.model.second_development

import io.realm.RealmObject
import io.realm.RealmResults
import io.realm.annotations.LinkingObjects
import io.realm.annotations.PrimaryKey
import mki.siojt2.model.localsave.SaranaLain

/**
 * Created by Iqbal on 15/10/2020.
 */

open class OtherFacilityOfPavement : RealmObject(){

    @PrimaryKey
    var pavementId: Int? = null

    var pavementFacilityId: Int? = null

    var pavementCapacity: Int? = null

    @LinkingObjects("otherFacilityOfPavement")
    val msaranaLain: RealmResults<SaranaLain>? = null
}