package mki.siojt2.model.second_development

import io.realm.RealmObject
import io.realm.RealmResults
import io.realm.annotations.LinkingObjects
import io.realm.annotations.PrimaryKey
import mki.siojt2.model.localsave.SaranaLain

/**
 * Created by Iqbal on 15/10/2020.
 */

open class OtherFacilityOfTelephoneLine : RealmObject(){

    @PrimaryKey
    var telephoneLineId: Int? = null

    var telephoneLineFacilityId: Int? = null

    var telephoneLineYesNO: Int? = null

    var telephoneLineQuantity: Int? = null

    @LinkingObjects("otherFacilityOfTelephoneLine")
    val msaranaLain: RealmResults<SaranaLain>? = null
}