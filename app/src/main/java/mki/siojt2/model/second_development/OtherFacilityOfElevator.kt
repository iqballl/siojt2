package mki.siojt2.model.second_development

import io.realm.RealmObject
import io.realm.RealmResults
import io.realm.annotations.LinkingObjects
import io.realm.annotations.PrimaryKey
import mki.siojt2.model.localsave.SaranaLain

/**
 * Created by Iqbal on 15/10/2020.
 */

open class OtherFacilityOfElevator : RealmObject(){

    @PrimaryKey
    var elevatorId: Int? = null

    var elevatorFacilityId: Int? = null

    var elevatorBrand: String? = null

    var elevatorMadeIn: String? = null

    var elevatorMaximumPersonQuantity: Int? = null

    var elevatorMaximumWeight: Int? = null

    var elevatorLength: Int? = null

    var elevatorWidth: Int? = null

    var elevatorHeight: Int? = null

    var elevatorQuantity: Int? = null

    var elevatorQYearProduction: Int? = null

    @LinkingObjects("otherFacilityOfElevator")
    val msaranaLain: RealmResults<SaranaLain>? = null
}