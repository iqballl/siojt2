package mki.siojt2.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject

open class UAVDataResponse: RealmObject() {
    @SerializedName("images")
    @Expose
    var images: RealmList<UAVImagesItem>? = null

    @SerializedName("objects")
    @Expose
    var objects: RealmList<UAVObjectsItem>? = null

    @SerializedName("project")
    @Expose
    var uAVProject: UAVProject? = null
}