package mki.siojt2.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class UAVObjectsItem: RealmObject() {
    @SerializedName("area")
    @Expose
    var area: String? = null

    @PrimaryKey
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("type")
    @Expose
    var type: Int? = null  //1 tanah 2 bangunan

    @SerializedName("geom")
    @Expose
    var geom: RealmList<UAVGeomResponse>? = null

    var selectedLandId: Int? = null

    var selectedBuildingId: Int? = null
}