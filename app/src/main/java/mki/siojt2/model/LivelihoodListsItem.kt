package mki.siojt2.model

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class LivelihoodListsItem(

	@field:SerializedName("PartyLivelihoodId")
	var partyLivelihoodId: Int? = null,

	@field:SerializedName("PartyLivelihoodCreateDate")
	var partyLivelihoodCreateDate: String? = null,

	@field:SerializedName("PartyLivelihoodLivelihoodOther")
	var partyLivelihoodLivelihoodOther: Any? = null,

	@field:SerializedName("PartyLivelihoodIncome")
	var partyLivelihoodIncome: String? = null,

	@field:SerializedName("PartyLivelihoodLogId")
	var partyLivelihoodLogId: Any? = null,

	@field:SerializedName("PartyLivelihoodLivelihoodId")
	var partyLivelihoodLivelihoodId: Int? = null,

	@field:SerializedName("PartyLivelihoodStatus")
	var partyLivelihoodStatus: Int? = null,

	@field:SerializedName("LivelihoodName")
	var livelihoodName: String? = null,

	@field:SerializedName("PartyLivelihoodPartyId")
	var partyLivelihoodPartyId: Int? = null,

	@field:SerializedName("LivelihoodDescrtiption")
	var livelihoodDescrtiption: Any? = null,

	@field:SerializedName("PartyLivelihoodCreateBy")
	var partyLivelihoodCreateBy: Int? = null
)