package mki.siojt2.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject

open class UAVPosition: RealmObject() {
    @SerializedName("bottomLeft")
    @Expose
    var bottomLeft: UAVGeomResponse? = null

    @SerializedName("bottomRight")
    @Expose
    var bottomRight: UAVGeomResponse? = null

    @SerializedName("topLeft")
    @Expose
    var topLeft: UAVGeomResponse? = null

    @SerializedName("topRight")
    @Expose
    var topRight: UAVGeomResponse? = null

}