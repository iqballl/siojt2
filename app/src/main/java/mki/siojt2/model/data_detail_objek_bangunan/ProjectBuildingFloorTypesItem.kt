package mki.siojt2.model.data_detail_objek_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ProjectBuildingFloorTypesItem(

	@field:SerializedName("PBFloorTypeId")
	var pBFloorTypeId: Int? = null,

	@field:SerializedName("PBFloorTypeAreaTotal")
	var pBFloorTypeAreaTotal: Double? = null,

	@field:SerializedName("PBFloorTypeProjectBuildingId")
	var pBFloorTypeProjectBuildingId: Int? = null,

	@field:SerializedName("FloorTypeName")
	var floorTypeName: String? = null,

	@field:SerializedName("PBFloorTypeStatus")
	var pBFloorTypeStatus: Int? = null,

	@field:SerializedName("PBFloorTypeCreateBy")
	var pBFloorTypeCreateBy: Int? = null,

	@field:SerializedName("PBFloorTypeLogId")
	var pBFloorTypeLogId: Any? = null,

	@field:SerializedName("FloorTypeDescription")
	var floorTypeDescription: Any? = null,

	@field:SerializedName("PBFloorTypeCreateDate")
	var pBFloorTypeCreateDate: String? = null,

	@field:SerializedName("PBFloorTypeFloorTypeId")
	var pBFloorTypeFloorTypeId: Int? = null
)