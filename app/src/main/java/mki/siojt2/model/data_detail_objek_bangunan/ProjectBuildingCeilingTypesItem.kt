package mki.siojt2.model.data_detail_objek_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ProjectBuildingCeilingTypesItem(

	@field:SerializedName("PBCeilingTypeId")
	var pBCeilingTypeId: Int? = null,

	@field:SerializedName("PBCeilingTypeProjectBuildingId")
	var pBCeilingTypeProjectBuildingId: Int? = null,

	@field:SerializedName("PBCeilingTypeAreaTotal")
	var pBCeilingTypeAreaTotal: Double? = null,

	@field:SerializedName("PBCeilingTypeStatus")
	var pBCeilingTypeStatus: Int? = null,

	@field:SerializedName("PBCeilingTypeCreateDate")
	var pBCeilingTypeCreateDate: String? = null,

	@field:SerializedName("CeilingTypeName")
	var ceilingTypeName: String? = null,

	@field:SerializedName("CeilingTypeDescription")
	var ceilingTypeDescription: Any? = null,

	@field:SerializedName("PBCeilingTypeCreateBy")
	var pBCeilingTypeCreateBy: Int? = null,

	@field:SerializedName("PBCeilingTypeCeilingTypeId")
	var pBCeilingTypeCeilingTypeId: Int? = null,

	@field:SerializedName("PBCeilingTypeLogId")
	var pBCeilingTypeLogId: Any? = null
)