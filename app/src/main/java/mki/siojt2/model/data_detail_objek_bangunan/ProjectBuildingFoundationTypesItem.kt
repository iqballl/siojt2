package mki.siojt2.model.data_detail_objek_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ProjectBuildingFoundationTypesItem(

	@field:SerializedName("PBFoundationTypeFoundationTypeId")
	var pBFoundationTypeFoundationTypeId: Int? = null,

	@field:SerializedName("FoundationTypeDescription")
	var foundationTypeDescription: Any? = null,

	@field:SerializedName("PBFoundationTypeId")
	var pBFoundationTypeId: Int? = null,

	@field:SerializedName("PBFoundationTypeProjectBuildingId")
	var pBFoundationTypeProjectBuildingId: Int? = null,

	@field:SerializedName("FoundationTypeName")
	var foundationTypeName: String? = null,

	@field:SerializedName("PBFoundationTypeAreaTotal")
	var pBFoundationTypeAreaTotal: Double? = null,

	@field:SerializedName("PBFoundationTypeStatus")
	var pBFoundationTypeStatus: Int? = null,

	@field:SerializedName("PBFoundationTypeLogId")
	var pBFoundationTypeLogId: Any? = null,

	@field:SerializedName("PBFoundationTypeCreateBy")
	var pBFoundationTypeCreateBy: Int? = null,

	@field:SerializedName("PBFoundationTypeCreateDate")
	var pBFoundationTypeCreateDate: String? = null
)