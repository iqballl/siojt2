package mki.siojt2.model.data_detail_objek_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ProjectBuildingWallTypesItem(

	@field:SerializedName("PBWallTypeId")
	var pBWallTypeId: Int? = null,

	@field:SerializedName("WallTypeDescription")
	var wallTypeDescription: Any? = null,

	@field:SerializedName("PBWallTypeProjectBuildingId")
	var pBWallTypeProjectBuildingId: Int? = null,

	@field:SerializedName("WallTypeName")
	var wallTypeName: String? = null,

	@field:SerializedName("PBWallTypeCreateBy")
	var pBWallTypeCreateBy: Int? = null,

	@field:SerializedName("PBWallTypeCreateDate")
	var pBWallTypeCreateDate: String? = null,

	@field:SerializedName("PBWallTypeStatus")
	var pBWallTypeStatus: Int? = null,

	@field:SerializedName("PBWallTypeWallTypeId")
	var pBWallTypeWallTypeId: Int? = null,

	@field:SerializedName("PBWallTypeAreaTotal")
	var pBWallTypeAreaTotal: Double? = null,

	@field:SerializedName("PBWallTypeLogId")
	var pBWallTypeLogId: Any? = null
)