package mki.siojt2.model.data_detail_objek_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class BuildingDetail(

	@field:SerializedName("ProjectBuildingProjectLandId")
	var projectBuildingProjectLandId: Int? = null,

	@field:SerializedName("ProjectPartyCreateBy")
	var projectPartyCreateBy: Int? = null,

	@field:SerializedName("ProjectPartyType")
	var projectPartyType: Int? = null,

	@field:SerializedName("ProjectPartyPartyId")
	var projectPartyPartyId: Int? = null,

	@field:SerializedName("ProjectPartyLogId")
	var projectPartyLogId: Any? = null,

	@field:SerializedName("ProjectBuildingOnPermitArea")
	var projectBuildingOnPermitArea: Any? = null,

	@field:SerializedName("ProjectBuildingId")
	var projectBuildingId: Int? = null,

	@field:SerializedName("ProjectBuildingFloorAreaUpperRoom")
	var projectBuildingFloorAreaUpperRoom: String? = null,

	@field:SerializedName("ProjectBuildingProjectPartyId")
	var projectBuildingProjectPartyId: Int? = null,

	@field:SerializedName("ProjectBuildingStatus")
	var projectBuildingStatus: Int? = null,

	@field:SerializedName("ProjectBuildingTypeId")
	var projectBuildingTypeId: Int? = null,

	@field:SerializedName("ProjectBuildingCreateBy")
	var projectBuildingCreateBy: Int? = null,

	@field:SerializedName("ProjectPartyProjectId")
	var projectPartyProjectId: Int? = null,

	@field:SerializedName("ProjectBuildingCreateDate")
	var projectBuildingCreateDate: String? = null,

	@field:SerializedName("ProjectPartyId")
	var projectPartyId: Int? = null,

	@field:SerializedName("ProjectBuildingLogId")
	var projectBuildingLogId: Any? = null,

	@field:SerializedName("BuildingTypeName")
	var buildingTypeName: String? = null,

	@field:SerializedName("ProjectPartyStatus")
	var projectPartyStatus: Int? = null,

	@field:SerializedName("ProjectBuildingRenovYear")
	var projectBuildingRenovYear: Any? = null,

	@field:SerializedName("ProjectBuildingOnPermitDate")
	var projectBuildingOnPermitDate: Any? = null,

	@field:SerializedName("ProjectBuildingFloorTotal")
	var projectBuildingFloorTotal: Int? = null,

	@field:SerializedName("BuildingTypeDescription")
	var buildingTypeDescription: Any? = null,

	@field:SerializedName("ProjectBuildingFloorAreaLowerRoom")
	var projectBuildingFloorAreaLowerRoom: String? = null,

	@field:SerializedName("ProjectBuildingBuiltYear")
	var projectBuildingBuiltYear: Any? = null,

	@field:SerializedName("ProjectBuildingPermitNo")
	var projectBuildingPermitNo: Any? = null,

	@field:SerializedName("ProjectPartyCreateDate")
	var projectPartyCreateDate: String? = null
)