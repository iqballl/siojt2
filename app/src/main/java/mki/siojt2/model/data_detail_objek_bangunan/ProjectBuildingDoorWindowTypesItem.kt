package mki.siojt2.model.data_detail_objek_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ProjectBuildingDoorWindowTypesItem(

	@field:SerializedName("DoorWindowTypeName")
	var doorWindowTypeName: String? = null,

	@field:SerializedName("PBDoorWindowTypeStatus")
	var pBDoorWindowTypeStatus: Int? = null,

	@field:SerializedName("PBDoorWindowTypeLogId")
	var pBDoorWindowTypeLogId: Any? = null,

	@field:SerializedName("PBDoorWindowTypeAreaTotal")
	var pBDoorWindowTypeAreaTotal: Double? = null,

	@field:SerializedName("PBDoorWindowTypeCreateBy")
	var pBDoorWindowTypeCreateBy: Int? = null,

	@field:SerializedName("DoorWindowTypeDescription")
	var doorWindowTypeDescription: Any? = null,

	@field:SerializedName("PBDoorWindowTypeProjectBuildingId")
	var pBDoorWindowTypeProjectBuildingId: Int? = null,

	@field:SerializedName("PBDoorWindowTypeDoorWindowTypeId")
	var pBDoorWindowTypeDoorWindowTypeId: Int? = null,

	@field:SerializedName("PBDoorWindowTypeCreateDate")
	var pBDoorWindowTypeCreateDate: String? = null,

	@field:SerializedName("PBDoorWindowTypeId")
	var pBDoorWindowTypeId: Int? = null
)