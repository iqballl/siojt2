package mki.siojt2.model.data_detail_objek_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ProjectBuildingRoofFrameTypesItem(

	@field:SerializedName("PBRoofFrameTypeStatus")
	var pBRoofFrameTypeStatus: Int? = null,

	@field:SerializedName("RoofFrameTypeName")
	var roofFrameTypeName: String? = null,

	@field:SerializedName("PBRoofFrameTypeLogId")
	var pBRoofFrameTypeLogId: Any? = null,

	@field:SerializedName("PBRoofFrameTypeCreateBy")
	var pBRoofFrameTypeCreateBy: Int? = null,

	@field:SerializedName("PBRoofFrameTypeProjectBuildingId")
	var pBRoofFrameTypeProjectBuildingId: Int? = null,

	@field:SerializedName("PBRoofFrameTypeRoofFrameTypeId")
	var pBRoofFrameTypeRoofFrameTypeId: Int? = null,

	@field:SerializedName("RoofFrameTypeDescription")
	var roofFrameTypeDescription: Any? = null,

	@field:SerializedName("PBRoofFrameTypeCreateDate")
	var pBRoofFrameTypeCreateDate: String? = null,

	@field:SerializedName("PBRoofFrameTypeId")
	var pBRoofFrameTypeId: Int? = null,

	@field:SerializedName("PBRoofFrameTypeAreaTotal")
	var pBRoofFrameTypeAreaTotal: Double? = null
)