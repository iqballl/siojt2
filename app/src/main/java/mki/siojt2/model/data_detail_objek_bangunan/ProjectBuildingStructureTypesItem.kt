package mki.siojt2.model.data_detail_objek_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ProjectBuildingStructureTypesItem(

	@field:SerializedName("PBStructureTypeCreateDate")
	var pBStructureTypeCreateDate: String? = null,

	@field:SerializedName("StructureTypeName")
	var structureTypeName: String? = null,

	@field:SerializedName("PBStructureTypeStatus")
	var pBStructureTypeStatus: Int? = null,

	@field:SerializedName("PBStructureTypeLogId")
	var pBStructureTypeLogId: Any? = null,

	@field:SerializedName("PBStructureTypeProjectBuildingId")
	var pBStructureTypeProjectBuildingId: Int? = null,

	@field:SerializedName("PBStructureTypeCreateBy")
	var pBStructureTypeCreateBy: Int? = null,

	@field:SerializedName("PBStructureTypeStructureTypeId")
	var pBStructureTypeStructureTypeId: Int? = null,

	@field:SerializedName("PBStructureTypeAreaTotal")
	var pBStructureTypeAreaTotal: Double? = null,

	@field:SerializedName("StructureTypeDescription")
	var structureTypeDescription: Any? = null,

	@field:SerializedName("PBStructureTypeId")
	var pBStructureTypeId: Int? = null
)