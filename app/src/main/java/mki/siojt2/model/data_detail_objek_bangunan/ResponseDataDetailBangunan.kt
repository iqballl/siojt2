package mki.siojt2.model.data_detail_objek_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataDetailBangunan(

	@field:SerializedName("ProjectBuildingRoofCoveringTypes")
	var projectBuildingRoofCoveringTypes: List<ProjectBuildingRoofCoveringTypesItem?>? = null,

	@field:SerializedName("ProjectBuildingRoofFrameTypes")
	var projectBuildingRoofFrameTypes: List<ProjectBuildingRoofFrameTypesItem?>? = null,

	@field:SerializedName("ProjectBuildingCeilingTypes")
	var projectBuildingCeilingTypes: List<ProjectBuildingCeilingTypesItem?>? = null,

	@field:SerializedName("ProjectBuildingWallTypes")
	var projectBuildingWallTypes: List<ProjectBuildingWallTypesItem?>? = null,

	@field:SerializedName("ProjectBuildingStructureTypes")
	var projectBuildingStructureTypes: List<ProjectBuildingStructureTypesItem?>? = null,

	@field:SerializedName("ProjectBuildingDoorWindowTypes")
	var projectBuildingDoorWindowTypes: List<ProjectBuildingDoorWindowTypesItem?>? = null,

	@field:SerializedName("BuildingDetail")
	var buildingDetail: BuildingDetail? = null,

	@field:SerializedName("ProjectBuildingFoundationTypes")
	var projectBuildingFoundationTypes: List<ProjectBuildingFoundationTypesItem?>? = null,

	@field:SerializedName("ProjectBuildingFloorTypes")
	var projectBuildingFloorTypes: List<ProjectBuildingFloorTypesItem?>? = null
)