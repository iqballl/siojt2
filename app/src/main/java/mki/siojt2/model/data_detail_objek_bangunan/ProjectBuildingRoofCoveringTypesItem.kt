package mki.siojt2.model.data_detail_objek_bangunan

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ProjectBuildingRoofCoveringTypesItem(

	@field:SerializedName("RoofCoveringTypeDescription")
	var roofCoveringTypeDescription: Any? = null,

	@field:SerializedName("PBRoofCoveringTypeRoofCoveringTypeId")
	var pBRoofCoveringTypeRoofCoveringTypeId: Int? = null,

	@field:SerializedName("PBRoofCoveringTypeId")
	var pBRoofCoveringTypeId: Int? = null,

	@field:SerializedName("PBRoofCoveringTypeCreateDate")
	var pBRoofCoveringTypeCreateDate: String? = null,

	@field:SerializedName("PBRoofCoveringTypeProjectBuildingId")
	var pBRoofCoveringTypeProjectBuildingId: Int? = null,

	@field:SerializedName("PBRoofCoveringTypeLogId")
	var pBRoofCoveringTypeLogId: Any? = null,

	@field:SerializedName("PBRoofCoveringTypeAreaTotal")
	var pBRoofCoveringTypeAreaTotal: Double? = null,

	@field:SerializedName("PBRoofCoveringTypeStatus")
	var pBRoofCoveringTypeStatus: Int? = null,

	@field:SerializedName("PBRoofCoveringTypeCreateBy")
	var pBRoofCoveringTypeCreateBy: Int? = null,

	@field:SerializedName("RoofCoveringTypeName")
	var roofCoveringTypeName: String? = null
)