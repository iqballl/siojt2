package mki.siojt2.model

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataDetailPemilik(

	@field:SerializedName("PartyDetail")
	var partyDetail: PartyDetail? = null,

	@field:SerializedName("LivelihoodLists")
	var livelihoodLists: List<LivelihoodListsItem?>? = null
)