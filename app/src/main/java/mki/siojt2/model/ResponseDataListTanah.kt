package mki.siojt2.model

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListTanah(

	@field:SerializedName("ProjectPartyCreateBy")
	var projectPartyCreateBy: Int? = null,

	@field:SerializedName("ProjectPartyType")
	var projectPartyType: Int? = null,

	@field:SerializedName("ProjectLandProjectPartyId")
	var projectLandProjectPartyId: Int? = null,

	@field:SerializedName("ProjectLandRT")
	var projectLandRT: String? = null,

	@field:SerializedName("ProjectLandLogId")
	var projectLandLogId: Any? = null,

	@field:SerializedName("ProjectLandRW")
	var projectLandRW: String? = null,

	@field:SerializedName("ProjectPartyPartyId")
	var projectPartyPartyId: Int? = null,

	@field:SerializedName("ProjectLandRegencyCode")
	var projectLandRegencyCode: String? = null,

	@field:SerializedName("ProjectPartyLogId")
	var projectPartyLogId: Any? = null,

	@field:SerializedName("ProvinceName")
	var provinceName: String? = null,

	@field:SerializedName("ProjectLandCreateBy")
	var projectLandCreateBy: Int? = null,

	@field:SerializedName("ProjectLandComplexName")
	var projectLandComplexName: String? = null,

	@field:SerializedName("ProjectLandNumber")
	var projectLandNumber: String? = null,

	@field:SerializedName("ProjectLandCreateDate")
	var projectLandCreateDate: String? = null,

	@field:SerializedName("ProjectPartyProjectId")
	var projectPartyProjectId: Int? = null,

	@field:SerializedName("ProjectLandStreetName")
	var projectLandStreetName: String? = null,

	@field:SerializedName("ProjectLandPostalCode")
	var projectLandPostalCode: String? = null,

	@field:SerializedName("ProjectPartyId")
	var projectPartyId: Int? = null,

	@field:SerializedName("ProjectLandBlock")
	var projectLandBlock: Any? = null,

	@field:SerializedName("ProjectLandVillageName")
	var projectLandVillageName: String? = null,

	@field:SerializedName("ProjectPartyStatus")
	var projectPartyStatus: Int? = null,

	@field:SerializedName("ProjectLandProvinceCode")
	var projectLandProvinceCode: String? = null,

	@field:SerializedName("ProjectLandDistrictCode")
	var projectLandDistrictCode: String? = null,

	@field:SerializedName("DistrictName")
	var districtName: String? = null,

	@field:SerializedName("ProjectLandStatus")
	var projectLandStatus: Int? = null,

	@field:SerializedName("RegencyName")
	var regencyName: String? = null,

	@field:SerializedName("ProjectLandId")
	var projectLandId: Int? = null,

	@field:SerializedName("ProjectPartyCreateDate")
	var projectPartyCreateDate: String? = null
)