package mki.siojt2.model

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class PartyDetail(

	@field:SerializedName("PartyProvinceCode")
	var partyProvinceCode: String? = null,

	@field:SerializedName("PartyIdentitySourceId")
	var partyIdentitySourceId: Int? = null,

	@field:SerializedName("PartyCreateDate")
	var partyCreateDate: String? = null,

	@field:SerializedName("PartyPostalCode")
	var partyPostalCode: String? = null,

	@field:SerializedName("PartyYearStayBegin")
	var partyYearStayBegin: Int? = null,

	@field:SerializedName("ProjectPartyCreateBy")
	var projectPartyCreateBy: Int? = null,

	@field:SerializedName("ProjectPartyType")
	var projectPartyType: Int? = null,

	@field:SerializedName("PartyFullName")
	var partyFullName: String? = null,

	@field:SerializedName("PartyBirthPlaceCode")
	var partyBirthPlaceCode: Any? = null,

	@field:SerializedName("ProjectPartyPartyId")
	var projectPartyPartyId: Int? = null,

	@field:SerializedName("PartyRegencyCode")
	var partyRegencyCode: String? = null,

	@field:SerializedName("PartyStatus")
	var partyStatus: Int? = null,

	@field:SerializedName("PartyStreetName")
	var partyStreetName: String? = null,

	@field:SerializedName("PartyCreateBy")
	var partyCreateBy: Int? = null,

	@field:SerializedName("ProjectPartyLogId")
	var projectPartyLogId: Any? = null,

	@field:SerializedName("PartyLogId")
	var partyLogId: Any? = null,

	@field:SerializedName("ProvinceName")
	var provinceName: String? = null,

	@field:SerializedName("PartyComplexName")
	var partyComplexName: Any? = null,

	@field:SerializedName("PartyNPWP")
	var partyNPWP: String? = null,

	@field:SerializedName("PartyBirthDate")
	var partyBirthDate: String? = null,

	@field:SerializedName("PartyBirthPlaceOther")
	var partyBirthPlaceOther: String? = null,

	@field:SerializedName("PartyDistrictCode")
	var partyDistrictCode: String? = null,

	@field:SerializedName("PartyBirthPlaceName")
	var partyBirthPlaceName: Any? = null,

	@field:SerializedName("ProjectPartyProjectId")
	var projectPartyProjectId: Int? = null,

	@field:SerializedName("PartyVillageName")
	var partyVillageName: String? = null,

	@field:SerializedName("ProjectPartyTypeText")
	var projectPartyTypeText: String? = null,

	@field:SerializedName("ProjectPartyId")
	var projectPartyId: Int? = null,

//	@field:SerializedName("IdentitySourceName")
//	var identitySourceName: String? = null,

	@field:SerializedName("PartyIdentitySourceOther")
	var partyIdentitySourceOther: Any? = null,

	@field:SerializedName("PartyOccupationOther")
	var partyOccupationOther: Any? = null,

	@field:SerializedName("OccupationName")
	var occupationName: String? = null,

	@field:SerializedName("PartyBlock")
	var partyBlock: Any? = null,

	@field:SerializedName("ProjectPartyStatus")
	var projectPartyStatus: Int? = null,

	@field:SerializedName("DistrictName")
	var districtName: String? = null,

	@field:SerializedName("PartyNumber")
	var partyNumber: String? = null,

	@field:SerializedName("PartyId")
	var partyId: Int? = null,

	@field:SerializedName("PartyOccupationId")
	var partyOccupationId: Int? = null,

	@field:SerializedName("RegencyName")
	var regencyName: String? = null,

	@field:SerializedName("PartyRT")
	var partyRT: String? = null,

	@field:SerializedName("PartyIdentityNumber")
	var partyIdentityNumber: String? = null,

	@field:SerializedName("PartyRW")
	var partyRW: String? = null,

	@field:SerializedName("ProjectPartyCreateDate")
	var projectPartyCreateDate: String? = null
)