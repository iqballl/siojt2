package mki.siojt2.model

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseDataListPemilik(
        @field:SerializedName("PartyProvinceCode")
        var partyProvinceCode: String? = null,

        @field:SerializedName("PartyDistrictCode")
        var partyDistrictCode: String? = null,

        @field:SerializedName("PartyCreateDate")
        var partyCreateDate: String? = null,

        @field:SerializedName("PartyPostalCode")
        var partyPostalCode: String? = null,

        @field:SerializedName("ProjectPartyProjectId")
        var projectPartyProjectId: Int? = null,

        @field:SerializedName("PartyFullName")
        var partyFullName: String? = null,

        @field:SerializedName("PartyVillageName")
        var partyVillageName: String? = null,

        @field:SerializedName("ProjectPartyPartyId")
        var projectPartyPartyId: Int? = null,

        @field:SerializedName("PartyRegencyCode")
        var partyRegencyCode: String? = null,

        @field:SerializedName("PartyStatus")
        var partyStatus: Int? = null,

        @field:SerializedName("PartyStreetName")
        var partyStreetName: String? = null,

        @field:SerializedName("PartyCreateBy")
        var partyCreateBy: Int? = null,

        @field:SerializedName("PartyBlock")
        var partyBlock: String? = null,

        @field:SerializedName("PartyLogId")
        var partyLogId: Any? = null,

        @field:SerializedName("ProvinceName")
        var provinceName: String? = null,

        @field:SerializedName("DistrictName")
        var districtName: String? = null,

        @field:SerializedName("PartyNumber")
        var partyNumber: String? = null,

        @field:SerializedName("PartyComplexName")
        var partyComplexName: String? = null,

        @field:SerializedName("PartyId")
        var partyId: Int? = null,

        @field:SerializedName("RegencyName")
        var regencyName: String? = null,

        @field:SerializedName("PartyRT")
        var partyRT: String? = null,

        @field:SerializedName("PartyRW")
        var partyRW: String? = null
) {
    override fun toString(): String {
        return partyFullName.toString()
    }
}