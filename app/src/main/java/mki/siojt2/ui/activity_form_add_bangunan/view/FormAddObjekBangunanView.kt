package mki.siojt2.ui.activity_form_add_bangunan.view

import mki.siojt2.base.view.MvpView

interface FormAddObjekBangunanView: MvpView{
    fun onsuccesspostBuildingObjek()
}