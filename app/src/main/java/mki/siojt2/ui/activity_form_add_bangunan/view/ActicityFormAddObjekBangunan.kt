package mki.siojt2.ui.activity_form_add_bangunan.view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.realm.Realm
import io.realm.RealmList
import kotlinx.android.synthetic.main.activity_form_add_objek.*
import kotlinx.android.synthetic.main.dialog_confirm_delete.view.*
import kotlinx.android.synthetic.main.toolbar_form.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.fragment.form_objek_bangunan.*
import mki.siojt2.model.UAVObjectsItem
import mki.siojt2.model.data_form_objek_bangunan.DataFormBangunanImage
import mki.siojt2.model.data_form_objek_bangunan.DataFormObjekBangunan1
import mki.siojt2.model.data_form_objek_bangunan.DataFormObjekBangunan2
import mki.siojt2.model.local_save_image.DataImageBuilding
import mki.siojt2.model.localsave.Bangunan
import mki.siojt2.model.localsave.Session
import mki.siojt2.persistence.Preference
import mki.siojt2.ui.activity_form_add_bangunan.presenter.FormAddObjekBangunanPresenter
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.Utils

class ActicityFormAddObjekBangunan : BaseActivity(), FormAddObjekBangunanView, FormObjekBangunan1.OnStepOneListener,
        FormObjekBangunan2.OnStepTwoListener, FormObjekBangunan3.OnStepThreeListener, FormObjekBangunan4.OnStepFourListener, FormObjekBangunanPhoto.OnStepFiveListener {

    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * [FragmentPagerAdapter] derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v13.app.FragmentStatePagerAdapter].
     */

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    lateinit var formAddObjekBangunanPresenter: FormAddObjekBangunanPresenter

    private var projectLandId: Int? = 0
    private var partyTypdeId: Int? = 0

    private var LandIdTemp: Int? = 0
    var ProjectId: Int? = 0

    private lateinit var realm: Realm
    private lateinit var session: Session

    private lateinit var mdialogConfirmDelete: AlertDialog
    private lateinit var mDialogView: View
    private var sessionFlagsCRUD: mki.siojt2.model.Session? = null
    var selectedLinkedObjectUAVId: Int? = null
    var nextId: Int = 0

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActicityFormAddObjekBangunan::class.java)
        }
    }

    @SuppressLint("SetTextI18n", "InflateParams")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_add_objek)

        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        session = Session(this)

        val historyData = intent?.extras?.getString("data_current_tanah")
        val toJson = Gson().fromJson(historyData, Array<String>::class.java).toList()

        val sessionData = intent?.extras?.getString("data_current_session")
        //val toJson = Gson().fromJson(historyData , Array<String>::class.java).toList()
        //val turnsType = object : TypeToken<ResponseDataListGedung>() {}.type
        //val dataconvert = Gson().fromJson<ResponseDataListGedung>(historyData, turnsType)

        if (sessionData != null) {
            val turnsType = object : TypeToken<mki.siojt2.model.Session>() {}.type
            sessionFlagsCRUD = Gson().fromJson<mki.siojt2.model.Session>(sessionData, turnsType)
            //Log.d("session", sessionFlagsCRUD!!.statusPost)

            if (sessionFlagsCRUD!!.statusPost == "2") {
                tvFormToolbarTitle.text = "Edit data objek - Bangunan"
            }
        } else {
            tvFormToolbarTitle.text = "Tambah data objek - Bangunan"
        }

        //projectLandId = dataconvert.projectLandId
        //partyTypdeId = dataconvert.projectPartyId

        LandIdTemp = toJson[0].toInt()
        ProjectId = toJson[1].toInt()

        /* set dialog confirm delete data */
        mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_confirm_delete, null)
        val mBuilder = AlertDialog.Builder(this)
        mdialogConfirmDelete = mBuilder.create()

        mdialogConfirmDelete.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        mdialogConfirmDelete.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mdialogConfirmDelete.setCancelable(false)
        val window = mdialogConfirmDelete.window
        if (window != null) {
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            mdialogConfirmDelete.window!!.setLayout(width, height)
        }
        mdialogConfirmDelete.setView(mDialogView)

        /* set toolbar*/
        if (toolbarCustom != null) {
            setSupportActionBar(toolbarCustom)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarCustom.setNavigationOnClickListener {
            dialogToHandleExitFromCurrentPage()
        }

        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager, 20, "")
        mSectionsPagerAdapter!!.addFrag(FormObjekBangunan1.newInstance(LandIdTemp!!, projectLandId!!))
        mSectionsPagerAdapter!!.addFrag(FormObjekBangunan2.newInstance("", ""))
        mSectionsPagerAdapter!!.addFrag(FormObjekBangunanPhoto.newInstance("", ""))

        // Set up the ViewPager with the sections adapter.
        viewpagerObjekTanah.adapter = mSectionsPagerAdapter
        stepperIndicatorObjekTanah.showLabels(true)
        stepperIndicatorObjekTanah.setViewPager(viewpagerObjekTanah)

        // or keep last page as "end page"
        stepperIndicatorObjekTanah.setViewPager(viewpagerObjekTanah, viewpagerObjekTanah.adapter!!.count - 1) //}

        viewpagerObjekTanah.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                mSectionsPagerAdapter!!.notifyDataSetChanged()
            }
        })

        showDialog()
    }

    private fun showDialog() {
        if (sessionFlagsCRUD != null) {
            if (sessionFlagsCRUD!!.id != null && sessionFlagsCRUD!!.statusPost == "3") {
                mDialogView.btnYesDelete.setOnClickListener {
                    realm.executeTransactionAsync({ inRealm ->
                        val resultRealmBanguanan = inRealm.where(Bangunan::class.java).findAll()
                        val dataParty = resultRealmBanguanan.where()
                                .equalTo("BangunanIdTemp", sessionFlagsCRUD!!.id!!)
                                .and()
                                .equalTo("LandIdTemp", sessionFlagsCRUD!!.id2!!)
                                .findFirst()
                        val resultdataImageBuilding = inRealm.where(DataImageBuilding::class.java).findAll()
                        val dataimageBuilding = resultdataImageBuilding.where().equalTo("imageId", sessionFlagsCRUD!!.id!!).findAll()
                        dataimageBuilding?.deleteAllFromRealm()
                        dataParty?.deleteFromRealm()
                    }, {
                        Log.d("delete", "onSuccess : delete single object")
                        mdialogConfirmDelete.dismiss()
                        finish()
                    }, {
                        Log.d("delete", "onFailed : ${it.localizedMessage}")
                        Log.d("delete", "onFailed : delete single object")
                    })
                }

                mDialogView.btnNoDelete.setOnClickListener {
                    mdialogConfirmDelete.dismiss()
                    finish()
                }
                mdialogConfirmDelete.show()
                /*val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
                    when (which) {
                        DialogInterface.BUTTON_POSITIVE -> {
                            val bangunan = realm.where(Bangunan::class.java).equalTo("BangunanIdTemp", session.id.toInt()).findFirst()
                            realm.beginTransaction()
                            bangunan!!.deleteFromRealm()
                            realm.commitTransaction()
                            dialog.dismiss()
                            finish()
                        }

                        DialogInterface.BUTTON_NEGATIVE -> {
                            dialog.dismiss()
                            finish()
                        }
                    }
                }

                val builder = android.support.v7.app.AlertDialog.Builder(this)
                builder.setTitle("SIOJT")
                        .setMessage("Yakin akan dihapus?")
                        .setPositiveButton("Ya", dialogClickListener)
                        .setNegativeButton("Tidak", dialogClickListener)
                        .setIcon(ContextCompat.getDrawable(this@ActicityFormAddObjekBangunan, R.drawable.ic_logo_splash)!!)
                        .show()*/
            }
        }

    }

    private fun getPresenter(): FormAddObjekBangunanPresenter? {
        formAddObjekBangunanPresenter = FormAddObjekBangunanPresenter()
        formAddObjekBangunanPresenter.onAttach(this)
        return formAddObjekBangunanPresenter
    }

    private fun dialogsuccessTransaksi(title_dialog: String) {
        Utils.deletedataform1(this@ActicityFormAddObjekBangunan)
        Utils.deletedataform3(this@ActicityFormAddObjekBangunan)
        Utils.deletedataform3(this@ActicityFormAddObjekBangunan)
        Utils.deletedataformImage(this@ActicityFormAddObjekBangunan)
        val dialogPopup = DialogSuccessAddBangunan.newInstance(title_dialog)
        dialogPopup.show(supportFragmentManager, "Dalog_popup")
    }

    private fun dialogToHandleExitFromCurrentPage() {
        val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> {
                    dialog.dismiss()
                    super.onBackPressed()
                    session.setIdAndStatus("", "", "")
                    finish()
                }

                DialogInterface.BUTTON_NEGATIVE -> {
                    dialog.dismiss()
                }
            }
        }

        MaterialAlertDialogBuilder(this, R.style.CustomAlertDialogMaterialTheme)
                .setTitle(getString(R.string.app_name))
                .setIcon(R.mipmap.ic_launcher)
                .setMessage("Data yang telah dibuat akan hilang, yakin keluar?")
                .setCancelable(false)
                .setPositiveButton("KELUAR", dialogClickListener)
                .setNegativeButton("BATAL", dialogClickListener)
                .show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 333) {
                selectedLinkedObjectUAVId = data?.getIntExtra("objectId", 0)
            }
        }
    }

    override fun onsuccesspostBuildingObjek() {
        Utils.deletedatabangunan1(this@ActicityFormAddObjekBangunan)
        Utils.deletedatabangunan2(this@ActicityFormAddObjekBangunan)
        Utils.deletedataformImage(this@ActicityFormAddObjekBangunan)
        val dialogPopup = DialogSuccessAddBangunan()
        dialogPopup.show(supportFragmentManager, "Dalog_popup")
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    class SectionsPagerAdapter(fm: androidx.fragment.app.FragmentManager, id: Int, landId: String) : androidx.fragment.app.FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        private var getId: Int? = id
        private var getLandId: String = landId
        private val mFragmentList = arrayListOf<Fragment>()

        fun addFrag(fragment: Fragment) {
            mFragmentList.add(fragment)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            /*when (position) {
                0 -> return FormObjekBangunan1.newInstance(getId!!, getLandId)
                1 -> return FormObjekBangunan2.newInstance("", "")
                2 -> return FormObjekBangunanPhoto.newInstance("", "")
            }*/
            return mFragmentList[position]
        }

        override fun getPageTitle(position: Int): CharSequence? {
            when (position) {
                0 -> return "Dasar"
                1 -> return "Spesifikasi"
                2 -> return "Foto properti"
            }
            return null
        }
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    override fun onBackPressed(fragment: Fragment) {
        mSectionsPagerAdapter!!.notifyDataSetChanged()
        when (fragment) {
            is FormObjekBangunan2 -> viewpagerObjekTanah.setCurrentItem(0, true)
        }
    }

    override fun onNextPressed(fragment: Fragment) {
        mSectionsPagerAdapter!!.notifyDataSetChanged()
        when (fragment) {
            is FormObjekBangunan1 -> viewpagerObjekTanah.setCurrentItem(1, true)
            is FormObjekBangunan2 -> {
                viewpagerObjekTanah.setCurrentItem(2, true)
            }
            is FormObjekBangunanPhoto -> {
                val turnsType = object : TypeToken<MutableList<DataFormObjekBangunan1>>() {}.type
                val dataconvert = Gson().fromJson<MutableList<DataFormObjekBangunan1>>(Utils.getdatabangunan1(this), turnsType)

                val turnsType2 = object : TypeToken<DataFormObjekBangunan2>() {}.type
                val dataconvert2 = Gson().fromJson<DataFormObjekBangunan2>(Utils.getdatabangunan2(this), turnsType2)

                val turnsType3 = object : TypeToken<DataFormBangunanImage>() {}.type
                val dataPhoto = Gson().fromJson<DataFormBangunanImage>(Utils.getdataformTanahImage(this), turnsType3)

                if (sessionFlagsCRUD != null) {

                    if (sessionFlagsCRUD!!.statusPost == "2") {
                        onShowLoading()
                        realm.executeTransactionAsync({ inrealm ->
                            val bangunan = inrealm.where(Bangunan::class.java)
                                    .equalTo("BangunanIdTemp", sessionFlagsCRUD!!.id!!)
                                    .and()
                                    .equalTo("LandIdTemp", sessionFlagsCRUD!!.id2!!)
                                    .findFirst()
                            nextId = bangunan?.bangunanIdTemp!!
                            bangunan.projectPartyId = dataconvert[0].projectPartyId
                            bangunan.projectPartyName = dataconvert[0].projectPartyName
                            bangunan.projectBuildingTypeId = dataconvert[0].projectBuildingTypeId
                            bangunan.projectBuildingTypeName = dataconvert[0].projectBuildingTypeName
                            bangunan.projectBuildingTypeNameOther = dataconvert[0].projectBuildingTypeNameOther
                            bangunan.projectBuildingFloorTotal = dataconvert[0].projectBuildingFloorTotal
                            bangunan.projectBuildingAreaUpperRoom = dataconvert[0].projectBuildingAreaUpperRoom
                            bangunan.projectBuildingAreaLowerRoom = dataconvert[0].projectBuildingAreaLowerRoom
                            bangunan.projectBuildingYearOfBuilt = dataconvert[0].projectBuildingYearOfBuilt
                            bangunan.foundationTypeId = dataconvert2.foundationTypeId.toString()
                            bangunan.foundationTypeName = dataconvert2.foundationTypeName.toString()
                            bangunan.foundationTypeNameOther = dataconvert2.foundationTypeNameOther.toString()
                            bangunan.pbFoundationTypeAreaTotal = dataconvert2.pbFoundationTypeAreaTotal.toString()
                            bangunan.structureTypeId = dataconvert2.structureTypeId.toString()
                            bangunan.structureTypeName = dataconvert2.structureTypeName.toString()
                            bangunan.structureTypeNameOther = dataconvert2.structureTypeNameOther.toString()
                            bangunan.pbStructureTypeAreaTotal = dataconvert2.pbStructureTypeAreaTotal.toString()
                            bangunan.roofFrameTypeId = dataconvert2.roofFrameTypeId.toString()
                            bangunan.roofFrameTypeName = dataconvert2.roofFrameTypeName.toString()
                            bangunan.roofFrameTypeNameOther = dataconvert2.roofFrameTypeNameOther.toString()
                            bangunan.pbRoofFrameTypeAreaTotal = dataconvert2.pbRoofFrameTypeAreaTotal.toString()
                            bangunan.roofCoveringTypeId = dataconvert2.roofCoveringTypeId.toString()
                            bangunan.roofCoveringTypeName = dataconvert2.roofCoveringTypeName.toString()
                            bangunan.roofCoveringTypeNameOther = dataconvert2.roofCoveringTypeNameOther.toString()
                            bangunan.pbRoofCoveringTypeAreaTotal = dataconvert2.pbRoofCoveringTypeAreaTotal.toString()
                            bangunan.ceilingTypeId = dataconvert2.ceilingTypeId.toString()
                            bangunan.ceilingTypeName = dataconvert2.ceilingTypeName.toString()
                            bangunan.ceilingTypeNameOther = dataconvert2.ceilingTypeNameOther.toString()
                            bangunan.pbCeilingTypeAreaTotal = dataconvert2.pbCeilingTypeAreaTotal.toString()
                            bangunan.doorWindowTypeld = dataconvert2.doorWindowTypeld.toString()
                            bangunan.doorWindowTypeName = dataconvert2.doorWindowTypeName.toString()
                            bangunan.doorWindowTypeNameOther = dataconvert2.doorWindowTypeNameOther.toString()
                            bangunan.pbDoorWindowTypeAreaTotal = dataconvert2.pbDoorWindowTypeAreaTotal.toString()
                            bangunan.floorTypeId = dataconvert2.floorTypeId.toString()
                            bangunan.floorTypeName = dataconvert2.floorTypeName.toString()
                            bangunan.floorTypeNameOther = dataconvert2.floorTypeNameOther.toString()
                            bangunan.pbFloorTypeAreaTotal = dataconvert2.pbFloorTypeAreaTotal.toString()
                            bangunan.wallTypeId = dataconvert2.wallTypeId.toString()
                            bangunan.wallTypeName = dataconvert2.wallTypeName.toString()
                            bangunan.wallTypeNameOther = dataconvert2.wallTypeNameOther.toString()
                            bangunan.pbfWallTypeAreaTotal = dataconvert2.pbFWallTypeAreaTotal.toString()
                            bangunan.selectedLinkedObjectUAVId = selectedLinkedObjectUAVId

                            val resultdataImageBuilding = inrealm.where(DataImageBuilding::class.java).findAll()
                            val dataimageBuilding = resultdataImageBuilding.where().equalTo("imageId", sessionFlagsCRUD!!.id!!).findAll()
                            dataimageBuilding?.deleteAllFromRealm()

                            val dataImagePlusId: MutableList<DataImageBuilding> = ArrayList()
                            for (element in dataPhoto.dataImagePhoto!!.indices) {
                                if (dataPhoto.dataImagePhoto!![element].imagepathLand.isNotEmpty() && dataPhoto.dataImagePhoto!![element].imagenameLand.isNotEmpty()) {
                                    val dataImage = DataImageBuilding()
                                    dataImage.imageId = sessionFlagsCRUD!!.id!!
                                    dataImage.imagepathLand = dataPhoto.dataImagePhoto!![element].imagepathLand
                                    dataImage.imagenameLand = dataPhoto.dataImagePhoto!![element].imagenameLand
                                    dataImagePlusId.add(dataImage)
                                }
                                //Log.d("hasil", dataPhoto.dataImagePhoto!![element].imageId.toString())
                                //Log.d("hasil", dataPhoto.dataImagePhoto!![element].imagenameLand.toString())
                            }
                            if (dataImagePlusId.isNotEmpty()) {
                                val dataPhotoBangunan = inrealm.copyToRealmOrUpdate(dataImagePlusId)
                                val dataPhotoBangunan2: RealmList<DataImageBuilding> = RealmList()
                                for (element in dataPhotoBangunan) {
                                    dataPhotoBangunan2.add(element)
                                }
                                bangunan.dataImageBuilding = dataPhotoBangunan2
                            }

                            bangunan.createdBy = Preference.auth.dataUser?.mobileUserId!!
                            bangunan.isCreate = 2

                        }, {

                            //bayu
                            val results = realm.where(UAVObjectsItem::class.java)
                                    .equalTo("id", selectedLinkedObjectUAVId)
                                    .findFirst()

                            Realm.getDefaultInstance().executeTransaction { realm ->
                                results?.selectedBuildingId = nextId
                                if (results != null) {
                                    realm.insertOrUpdate(results)
                                }
                                Log.d("building", "success update building objects")
                                runOnUiThread {
                                    onDismissLoading()
                                    dialogsuccessTransaksi("""
                                Data
                                berhasil
                                diperbaharui
                            """.trimIndent())
                                }
                            }
                        }, {
                            Log.d("building", "failed update building")
                            Log.d("building", it.localizedMessage!!)
                            Log.d("building", it.message!!)
                            runOnUiThread {
                                onDismissLoading()
                            }
                        })
                    }
                } else {
                    // Begin Saving
                    onShowLoading()
                    realm.executeTransactionAsync({ inrealm ->
                        //Log.d("hasil", dataconvert2.toString())
                        val currentIdNum = inrealm.where(Bangunan::class.java).max("BangunanIdTemp")
                        nextId = if (currentIdNum == null) {
                            1
                        } else {
                            currentIdNum.toInt() + 1
                        }
                        val bangunan = inrealm.createObject(Bangunan::class.java, nextId)
//                        //bangunan.bangunanIdTemp = nextId
                        bangunan.bangunanId = null
                        bangunan.projectId = ProjectId
                        bangunan.landIdTemp = LandIdTemp
                        bangunan.landId = null

                        bangunan.projectPartyId = dataconvert[0].projectPartyId
                        bangunan.projectPartyName = dataconvert[0].projectPartyName
                        bangunan.projectBuildingTypeId = dataconvert[0].projectBuildingTypeId
                        bangunan.projectBuildingTypeName = dataconvert[0].projectBuildingTypeName
                        bangunan.projectBuildingTypeNameOther = dataconvert[0].projectBuildingTypeNameOther
                        bangunan.projectBuildingFloorTotal = dataconvert[0].projectBuildingFloorTotal
                        bangunan.projectBuildingAreaUpperRoom = dataconvert[0].projectBuildingAreaUpperRoom
                        bangunan.projectBuildingAreaLowerRoom = dataconvert[0].projectBuildingAreaLowerRoom
                        bangunan.projectBuildingYearOfBuilt = dataconvert[0].projectBuildingYearOfBuilt
                        bangunan.foundationTypeId = dataconvert2.foundationTypeId.toString()
                        bangunan.foundationTypeName = dataconvert2.foundationTypeName.toString()
                        bangunan.foundationTypeNameOther = dataconvert2.foundationTypeNameOther.toString()
                        bangunan.pbFoundationTypeAreaTotal = dataconvert2.pbFoundationTypeAreaTotal.toString()
                        bangunan.structureTypeId = dataconvert2.structureTypeId.toString()
                        bangunan.structureTypeName = dataconvert2.structureTypeName.toString()
                        bangunan.structureTypeNameOther = dataconvert2.structureTypeNameOther.toString()
                        bangunan.pbStructureTypeAreaTotal = dataconvert2.pbStructureTypeAreaTotal.toString()
                        bangunan.roofFrameTypeId = dataconvert2.roofFrameTypeId.toString()
                        bangunan.roofFrameTypeName = dataconvert2.roofFrameTypeName.toString()
                        bangunan.roofFrameTypeNameOther = dataconvert2.roofFrameTypeNameOther.toString()
                        bangunan.pbRoofFrameTypeAreaTotal = dataconvert2.pbRoofFrameTypeAreaTotal.toString()
                        bangunan.roofCoveringTypeId = dataconvert2.roofCoveringTypeId.toString()
                        bangunan.roofCoveringTypeName = dataconvert2.roofCoveringTypeName.toString()
                        bangunan.roofCoveringTypeNameOther = dataconvert2.roofCoveringTypeNameOther.toString()
                        bangunan.pbRoofCoveringTypeAreaTotal = dataconvert2.pbRoofCoveringTypeAreaTotal.toString()
                        bangunan.ceilingTypeId = dataconvert2.ceilingTypeId.toString()
                        bangunan.ceilingTypeName = dataconvert2.ceilingTypeName.toString()
                        bangunan.ceilingTypeNameOther = dataconvert2.ceilingTypeNameOther.toString()
                        bangunan.pbCeilingTypeAreaTotal = dataconvert2.pbCeilingTypeAreaTotal.toString()
                        bangunan.doorWindowTypeld = dataconvert2.doorWindowTypeld.toString()
                        bangunan.doorWindowTypeName = dataconvert2.doorWindowTypeName.toString()
                        bangunan.doorWindowTypeNameOther = dataconvert2.doorWindowTypeNameOther.toString()
                        bangunan.pbDoorWindowTypeAreaTotal = dataconvert2.pbDoorWindowTypeAreaTotal.toString()
                        bangunan.floorTypeId = dataconvert2.floorTypeId.toString()
                        bangunan.floorTypeName = dataconvert2.floorTypeName.toString()
                        bangunan.floorTypeNameOther = dataconvert2.floorTypeNameOther.toString()
                        bangunan.pbFloorTypeAreaTotal = dataconvert2.pbFloorTypeAreaTotal.toString()
                        bangunan.wallTypeId = dataconvert2.wallTypeId.toString()
                        bangunan.wallTypeName = dataconvert2.wallTypeName.toString()
                        bangunan.wallTypeNameOther = dataconvert2.wallTypeNameOther.toString()
                        bangunan.pbfWallTypeAreaTotal = dataconvert2.pbFWallTypeAreaTotal.toString()
                        bangunan.selectedLinkedObjectUAVId = selectedLinkedObjectUAVId
                        bangunan.notes = dataPhoto.notes

                        val dataImagePlusId: MutableList<DataImageBuilding> = ArrayList()
                        for (element in dataPhoto.dataImagePhoto!!.indices) {
                            if (dataPhoto.dataImagePhoto!![element].imagepathLand.isNotEmpty() && dataPhoto.dataImagePhoto!![element].imagenameLand.isNotEmpty()) {
                                val dataImage = DataImageBuilding()
                                dataImage.imageId = nextId
                                dataImage.imagepathLand = dataPhoto.dataImagePhoto!![element].imagepathLand
                                dataImage.imagenameLand = dataPhoto.dataImagePhoto!![element].imagenameLand
                                dataImagePlusId.add(dataImage)
                            }
                            //Log.d("hasil", dataPhoto.dataImagePhoto!![element].imagenameLand.toString())
                        }

                        if (dataImagePlusId.isNotEmpty()) {
                            val dataPhotoBangunan = inrealm.copyToRealmOrUpdate(dataImagePlusId)
                            val dataPhotoBangunan2: RealmList<DataImageBuilding> = RealmList()
                            for (element in dataPhotoBangunan) {
                                dataPhotoBangunan2.add(element)
                            }

                            bangunan.dataImageBuilding = dataPhotoBangunan2
                        }

                        bangunan.createdBy = Preference.auth.dataUser?.mobileUserId!!
                        bangunan.isCreate = 1

                        /*
                        val dataPhotoBangunan = inrealm.copyToRealmOrUpdate(dataPhoto.dataImagePhoto!!)
                        val dataPhotoBangunan2: RealmList<DataImageBuilding> = RealmList()
                        for(element in dataPhotoBangunan){
                            dataPhotoBangunan2.add(element)
                        }*/
                    }, {

                        //bayu
                        val results = realm.where(UAVObjectsItem::class.java)
                                .equalTo("id", selectedLinkedObjectUAVId)
                                .findFirst()

                        Realm.getDefaultInstance().executeTransaction { realm ->
                            results?.selectedBuildingId = nextId
                            if (results != null) {
                                realm.insertOrUpdate(results)
                            }
                            Log.d("building", "success adds building objects")
                            runOnUiThread {
                                onDismissLoading()
                                onsuccesspostBuildingObjek()
                            }
                        }
                    }, {
                        Log.d("building", "failed adds building")
                        Log.d("building", it.localizedMessage!!)
                        Log.d("building", it.message!!)
                    })

                }
                /*End Saving*/

//                getPresenter()?.postBuildingObjek(Preference.auth.dataUser?.mobileUserId!!,
//                        projectLandId!!, dataconvert[0].projectPartyId!!,
//                        dataconvert[0].projectBuildingTypeId!!, dataconvert[0].projectBuildingFloorTotal!!,
//                        dataconvert[0].projectBuildingAreaUpperRoom!!, dataconvert[0].projectBuildingAreaLowerRoom!!,
//                        dataconvert2[0].ceilingTypeId!!, dataconvert2[0].pbCeilingTypeAreaTotal!!,
//                        dataconvert2[0].doorWindowTypeld!!, dataconvert2[0].pbDoorWindowTypeAreaTotal!!,
//                        dataconvert2[0].floorTypeId!!, dataconvert2[0].pbFloorTypeAreaTotal!!,
//                        dataconvert2[0].foundationTypeId!!, dataconvert2[0].pbFoundationTypeAreaTotal!!,
//                        dataconvert2[0].roofCoveringTypeId!!, dataconvert2[0].pbRoofCoveringTypeAreaTotal!!,
//                        dataconvert2[0].roofFrameTypeId!!, dataconvert2[0].pbRoofFrameTypeAreaTotal!!,
//                        dataconvert2[0].structureTypeId!!, dataconvert2[0].pbStructureTypeAreaTotal!!,
//                        dataconvert2[0].wallTypeId!!, dataconvert2[0].pbFWallTypeAreaTotal!!,
//                        Preference.accessToken
//                )
            }

        }
    }

    override fun onBackPressed() {
        dialogToHandleExitFromCurrentPage()
    }

    override fun onDestroy() {
        super.onDestroy()
        session.setIdAndStatus("", "", "")
    }
}