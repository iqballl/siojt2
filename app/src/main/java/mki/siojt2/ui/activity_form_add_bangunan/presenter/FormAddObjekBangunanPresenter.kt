package mki.siojt2.ui.activity_form_add_bangunan.presenter

import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.ui.activity_form_add_bangunan.view.FormAddObjekBangunanView
import mki.siojt2.utils.ErrorHandler
import mki.siojt2.utils.rxJava.RxUtils

class FormAddObjekBangunanPresenter : BasePresenter(), FormAddObjekBangunanMVPPresenter {


    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun postBuildingObjek(userId: Int, projectLandId: Int, projectPartyId: Int, projectBuildingTypeId: Int, projectBuildingFloorTotal: Int, projectBuildingAreaUpperRoom: String, projectBuildingAreaLowerRoom: String,
                                   ceilingTypeId: ArrayList<String>, pbCeilingTypeAreaTotal: ArrayList<Float>,
                                   doorWindowTypeld: ArrayList<String>, pbDoorWindowTypeAreaTotal: ArrayList<Float>,
                                   floorTypeId: ArrayList<String>, pbFloorTypeAreaTotal: ArrayList<Float>,
                                   foundationTypeId: ArrayList<String>, pbFoundationTypeAreaTotal: ArrayList<Float>,
                                   roofCoveringTypeId: ArrayList<String>, pbRoofCoveringTypeAreaTotal: ArrayList<Float>,
                                   roofFrameTypeId: ArrayList<String>, pbRoofFrameTypeAreaTotal: ArrayList<Float>,
                                   structureTypeId: ArrayList<String>, pbStructureTypeAreaTotal: ArrayList<Float>,
                                   wallTypeId: ArrayList<String>, pbWallTypeAreaTotal: ArrayList<Float>,
                                   accessToken: String) {
        view().onShowLoading()
        /* projectBuildingBuiltYear,
         ProjectBuildingRenovYear, projectBuildingOnPermiteDate, projectBuildingOnPermiteArea*/
        compositeDisposable.add(
                dataManager.postBuilding(userId, projectLandId, projectPartyId, projectBuildingTypeId, projectBuildingFloorTotal,
                        projectBuildingAreaUpperRoom, projectBuildingAreaLowerRoom,
                        ceilingTypeId, pbCeilingTypeAreaTotal,
                        doorWindowTypeld, pbDoorWindowTypeAreaTotal,
                        floorTypeId, pbFloorTypeAreaTotal,
                        foundationTypeId, pbFoundationTypeAreaTotal,
                        roofCoveringTypeId, pbRoofCoveringTypeAreaTotal,
                        roofFrameTypeId, pbRoofFrameTypeAreaTotal,
                        structureTypeId, pbStructureTypeAreaTotal,
                        wallTypeId, pbWallTypeAreaTotal,
                        accessToken)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccesspostBuildingObjek()
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }


    private fun view(): FormAddObjekBangunanView {
        return getView() as FormAddObjekBangunanView
    }

}

