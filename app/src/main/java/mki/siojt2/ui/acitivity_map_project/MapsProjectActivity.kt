package mki.siojt2.ui.acitivity_map_project

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.PointF
import android.graphics.RectF
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import android.view.ViewAnimationUtils
import android.view.Window
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.LinearLayout
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.mapbox.android.core.location.*
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.geojson.*
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationComponentConstants
import com.mapbox.mapboxsdk.location.LocationComponentOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.style.layers.FillLayer
import com.mapbox.mapboxsdk.style.layers.LineLayer
import com.mapbox.mapboxsdk.style.layers.Property.NONE
import com.mapbox.mapboxsdk.style.layers.Property.VISIBLE
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.*
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import io.realm.Realm
import io.realm.Sort
import kotlinx.android.synthetic.main.activity_map_project.*
import kotlinx.android.synthetic.main.bottomsheet_map_project.*
import kotlinx.android.synthetic.main.dialog_info_map_project.*
import kotlinx.android.synthetic.main.dialog_object_uav_detail.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.fragment.form_object_room.AdapterDaftarObjekRoom
import mki.siojt2.model.ResponDataSubjekDPPT
import mki.siojt2.model.ResponseDataListProjectLocal
import mki.siojt2.model.UAVObjectsItem
import mki.siojt2.model.UAVProject
import mki.siojt2.model.localsave.*
import mki.siojt2.model.mapbox.MapProjectPolylines
import mki.siojt2.ui.activity_compare_uav.CompareUAVActivity
import mki.siojt2.ui.activity_daftar_kepemilikan_tanah_pertama.recyleview_adapter.AdapterDaftarKepemilikanTanahPertama
import mki.siojt2.ui.activity_daftar_objek.fasilitas_lain.adapter_recycleview.AdapterDaftarObjekLain
import mki.siojt2.ui.activity_daftar_objek.gedung.adapter_recycleview.AdapterDaftarObjekGedung
import mki.siojt2.ui.activity_daftar_objek.tanaman.adapter_recylceview.AdapterDaftarObjekTanaman
import mki.siojt2.ui.activity_daftar_tanah.recycleview.AdapterDaftarTanah
import mki.siojt2.ui.activity_form_add_bangunan.view.ActicityFormAddObjekBangunan
import mki.siojt2.ui.activity_form_add_objek_lain.view.ActicityFormAddObjekLain
import mki.siojt2.ui.activity_form_add_objek_room.view.ActicityFormAddObjekRoom
import mki.siojt2.ui.activity_form_add_objek_tanah.view.ActicityFormAddObjekTanah
import mki.siojt2.ui.activity_form_add_objek_tanaman.ActivityFormAddObjekTanaman
import mki.siojt2.ui.activity_form_subjek.view.ActivityFormSubjek
import mki.siojt2.utils.MapsProjectIntentSetting
import mki.siojt2.utils.Utils
import mki.siojt2.utils.extension.MapBoxUtils
import mki.siojt2.utils.realm.RealmController
import java.lang.ref.WeakReference
import kotlin.math.*


class MapsProjectActivity : BaseActivity(), PermissionsListener, MapboxMap.OnMapClickListener {

    lateinit var realm: Realm
    private var permissionsManager: PermissionsManager? = null
    private var mapboxMap: MapboxMap? = null

    private val DEFAULT_INTERVAL_IN_MILLISECONDS = 10000L
    private val DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5
    private var locationEngine: LocationEngine? = null
    private val callback: LocationChangeListeningActivityLocationCallback =
        LocationChangeListeningActivityLocationCallback(this)

    private val MARKER_ID = "MARKER_ID"
    private val MARKER_SOURCE_ID = "MARKER_SOURCE_ID"
    private val MARKER_LAYER_ID = "MARKER_LAYER_ID"
    private val IMAGE_OVERLAY_SOURCE_IDs = arrayListOf<String>()

    private val polygonLayerList: MutableList<MapProjectPolylines> = arrayListOf()
    private var polygonProject: MapProjectPolylines? = null

    lateinit var intentSetting: MapsProjectIntentSetting
    lateinit var selectedStyle: Style
    var selectedLayerId = ""
    var selectedObjectId: Int? = 0
    var projectData: ResponseDataListProjectLocal? = null

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token))
        setContentView(R.layout.activity_map_project)

        intentSetting = intent.getSerializableExtra("setting") as MapsProjectIntentSetting
        selectedObjectId = intentSetting.defaultSelectedObjectUAVId

        /* set toolbar*/
        if (toolbarMaps != null) {
            setSupportActionBar(toolbarMaps)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        projectData = realm.where(ResponseDataListProjectLocal::class.java)
            .equalTo("projectAssignProjectId", intent.getIntExtra("projectId", 0))
            .findFirst()

        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync { mapboxMap ->
            this.mapboxMap = mapboxMap
            mapboxMap.addOnMapClickListener(this)
            initMapInteraction()
            mapboxMap.setStyle(
                Style.Builder().fromUri(Style.LIGHT)
                    .withImage(
                        MARKER_ID, BitmapFactory.decodeResource(
                            this.resources, R.drawable.mapbox_marker_icon_default
                        )
                    )
                    .withSource(
                        GeoJsonSource(
                            MARKER_SOURCE_ID,
                            FeatureCollection.fromFeatures(arrayListOf())
                        )
                    )
                    .withLayer(
                        SymbolLayer(MARKER_LAYER_ID, MARKER_SOURCE_ID)
                            .withProperties(
                                iconImage(MARKER_ID),
                                iconAllowOverlap(true),
                                iconIgnorePlacement(true)
                            )
                    )
            ) { style ->
                selectedStyle = style
                setUAVData(style)
                setPolygonLocal(style)
                enableLocationComponent(style)
            }
        }

        toolbarMaps.setNavigationOnClickListener {
            finish()
        }

        if (intentSetting.showCompareData) {
            btnCompare.visibility = View.VISIBLE

            btnCompare.setOnClickListener {
                startActivity(Intent(this, CompareUAVActivity::class.java))
            }
        } else {
            btnCompare.visibility = View.GONE
        }

        btnMenu.setOnClickListener {
            if (map_type_selection.visibility == View.VISIBLE) {
                map_type_selection.visibility = View.GONE
                cvCurrentLocation.visibility = View.VISIBLE
            } else {
                map_type_selection.visibility = View.VISIBLE
                cvCurrentLocation.visibility = View.GONE
            }
        }

        btnInfo.setOnClickListener {

            val dialog = Dialog(this)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCanceledOnTouchOutside(true)
            dialog.setContentView(R.layout.dialog_info_map_project)

            dialog.btnClose.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }
    }

    private fun setUAVData(style: Style) {
        if (projectData != null) {
            if (!projectData!!.getUAVDataResponse()?.images.isNullOrEmpty()) {
                for ((index, i) in projectData!!.getUAVDataResponse()?.images!!.withIndex()) {
                    if (!i.imgPath.isNullOrEmpty()) {
                        val overlaySourceId = "IMAGE_OVERLAY_SOURCE_ID_$index"
                        MapBoxUtils().addImageOverlay(
                            style,
                            LatLng(i.UAVPosition?.topLeft?.lat!!, i.UAVPosition?.topLeft?.lng!!),
                            LatLng(i.UAVPosition?.topRight?.lat!!, i.UAVPosition?.topRight?.lng!!),
                            LatLng(
                                i.UAVPosition?.bottomRight?.lat!!,
                                i.UAVPosition?.bottomRight?.lng!!
                            ),
                            LatLng(
                                i.UAVPosition?.bottomLeft?.lat!!,
                                i.UAVPosition?.bottomLeft?.lng!!
                            ),
                            overlaySourceId,
                            overlaySourceId,
                            i.imgPath!!,
                            LocationComponentConstants.SHADOW_LAYER
                        )
                        IMAGE_OVERLAY_SOURCE_IDs.add(overlaySourceId)
                    }
                }
            }
            uavProjectPolygon(style, projectData!!)
            uavObjectPolygon(style, projectData!!)
        }
    }

    private fun setPolygonLocal(style: Style) {
        val results = realm.where(ProjectLand::class.java)
            .equalTo("ProjectId", intent.getIntExtra("projectId", 0))
            .findAll()

        if (!results.isNullOrEmpty()) {
            for (i in results) {
                val polylines = MapProjectPolylines()
                polylines.CIRCLE_SOURCE_ID = "circle-source-id-${i.landIdTemp}"
                polylines.FILL_SOURCE_ID = "fill-source-id-${i.landIdTemp}"
                polylines.LINE_SOURCE_ID = "line-source-id-${i.landIdTemp}"
                polylines.CIRCLE_LAYER_ID = "circle-layer-id-${i.landIdTemp}"
                polylines.FILL_LAYER_ID = "fill-layer-id-${i.landIdTemp}"
                polylines.LINE_LAYER_ID = "line-layer-id-${i.landIdTemp}"
                polylines.type = "real"
                polylines.landData = i

                val l: MutableList<List<Point>> = arrayListOf()
                val coordinate: MutableList<Point> = arrayListOf()
                for (x in i.landPolygonPoint) {
                    coordinate.add(Point.fromLngLat(x.lon!!, x.lat!!))
                }
                l.add(coordinate)
                polylines.coordinat = l

                style.addSource(
                    GeoJsonSource(
                        polylines.FILL_SOURCE_ID,
                        Polygon.fromLngLats(polylines.coordinat)
                    )
                )
                style.addLayerBelow(
                    FillLayer(polylines.FILL_LAYER_ID, polylines.FILL_SOURCE_ID).withProperties(
                        fillColor(Color.parseColor("#00e9ff"))
                    ), LocationComponentConstants.SHADOW_LAYER
                )

                style.addSource(
                    GeoJsonSource(
                        polylines.LINE_SOURCE_ID,
                        LineString.fromLngLats(polylines.coordinat[0])
                    )
                )
                style.addLayerBelow(
                    LineLayer(polylines.LINE_LAYER_ID, polylines.LINE_SOURCE_ID).withProperties(
                        lineColor(Color.WHITE),
                        lineWidth(1f)
                    ), LocationComponentConstants.SHADOW_LAYER
                )

                polygonLayerList.add(polylines)
            }
            Log.d("results", results.toString())
        } else {
            Log.e("data", "kosong")
        }
    }

    private fun uavObjectPolygon(style: Style, results: ResponseDataListProjectLocal) {
        if (!results.getUAVDataResponse()?.objects.isNullOrEmpty()) {
            for ((index, value) in results.getUAVDataResponse()?.objects!!.withIndex()) {
                val polylines = MapProjectPolylines()
                polylines.id = value.id
                polylines.CIRCLE_SOURCE_ID = "circle-source-id-uav-object-$index"
                polylines.FILL_SOURCE_ID = "fill-source-id-uav-object-$index"
                polylines.LINE_SOURCE_ID = "line-source-id-uav-object-$index"
                polylines.CIRCLE_LAYER_ID = "circle-layer-id-uav-object-$index"
                polylines.FILL_LAYER_ID = "fill-layer-id-uav-object-$index"
                polylines.LINE_LAYER_ID = "line-layer-id-uav-object-$index"
                polylines.uavObject = value
//                polylines.circleSource = initCircleSource(style, polylines.CIRCLE_SOURCE_ID)
//                polylines.fillSource = initFillSource(style, polylines.FILL_SOURCE_ID)
//                polylines.lineSource = initFillSource(style, polylines.LINE_SOURCE_ID)
                polylines.type = "uav-object"

                polylines.objectType = intentSetting.objectType

                val resultsDataTanah = realm.where(ProjectLand::class.java)
                    .equalTo("selectedLinkedObjectUAVId", value?.id).findFirst()

                Realm.getDefaultInstance().executeTransaction { realm ->
                    resultsDataTanah?.areaId = value.id
                    if(resultsDataTanah != null){
                        realm.insertOrUpdate(resultsDataTanah)
                    }
                }

                polylines.landData = resultsDataTanah

                val resultsOwner =
                    realm.where(PartyAdd::class.java).equalTo("areaId", value?.id).findFirst()
                polylines.owner = resultsOwner

                val resultsInfrastructure =
                    realm.where(SaranaLain::class.java).equalTo("LandIdTemp", value?.id).findFirst()
                polylines.infrastructure = resultsInfrastructure

                val resultsPlant =
                    realm.where(Tanaman::class.java).equalTo("LandIdTemp", value?.id).findFirst()
                polylines.plant = resultsPlant

                val resultsDataBuilding = realm.where(Bangunan::class.java)
                    .equalTo("LandIdTemp", value?.id).findFirst()
                polylines.buildingData = resultsDataBuilding

                if (!value.geom.isNullOrEmpty()) {
                    val l: MutableList<List<Point>> = arrayListOf()
                    val coordinate: MutableList<Point> = arrayListOf()
                    for (i in value.geom!!) {
                        coordinate.add(Point.fromLngLat(i.lng, i.lat))
                    }
                    l.add(coordinate)
                    polylines.coordinat = l
                }

                if (selectedObjectId != null && polylines.id == selectedObjectId) {
                    selectedLayerId = polylines.FILL_LAYER_ID
                }

                style.addSource(
                    GeoJsonSource(
                        polylines.FILL_SOURCE_ID,
                        Polygon.fromLngLats(polylines.coordinat)
                    )
                )
                style.addLayerBelow(
                    FillLayer(polylines.FILL_LAYER_ID, polylines.FILL_SOURCE_ID).withProperties(
                        when {
                            polylines.owner == null && polylines.landData == null && polylines.buildingData == null &&
                                    polylines.infrastructure == null && polylines.plant == null -> {
                                fillColor(Color.parseColor("#CCCCCC"))//abu
                            }
                            polylines.owner != null && polylines.landData != null && polylines.buildingData != null &&
                                    polylines.infrastructure != null && polylines.plant != null -> {
                                fillColor(Color.parseColor("#66C99C"))//hijau
                            }
                            else -> {
                                fillColor(Color.parseColor("#F7CE46"))//kuning
                            }
                        },
                        fillOpacity(1f)
                    ), LocationComponentConstants.SHADOW_LAYER
                )

                style.addSource(
                    GeoJsonSource(
                        polylines.LINE_SOURCE_ID,
                        LineString.fromLngLats(polylines.coordinat[0])
                    )
                )
                style.addLayerBelow(
                    LineLayer(polylines.LINE_LAYER_ID, polylines.LINE_SOURCE_ID).withProperties(
                        lineColor(Color.WHITE),
                        lineWidth(1f)
                    ), LocationComponentConstants.SHADOW_LAYER
                )

                polygonLayerList.add(polylines)
            }
        }
    }

    private fun uavProjectPolygon(style: Style, results: ResponseDataListProjectLocal) {
        var centerProjectLat = 0.0
        var centerProjectLng = 0.0

        if (!results.getUAVDataResponse()?.uAVProject?.geom.isNullOrEmpty()) {
            polygonProject = MapProjectPolylines()
            polygonProject?.CIRCLE_SOURCE_ID = "circle-source-id-uav-project"
            polygonProject?.FILL_SOURCE_ID = "fill-source-id-uav-project"
            polygonProject?.LINE_SOURCE_ID = "line-source-id-uav-project"
            polygonProject?.CIRCLE_LAYER_ID = "circle-layer-id-uav-project"
            polygonProject?.FILL_LAYER_ID = "fill-layer-id-uav-project"
            polygonProject?.LINE_LAYER_ID = "line-layer-id-uav-project"
            polygonProject?.type = "uav-project"
            polygonProject?.uavProject = results.getUAVDataResponse()?.uAVProject

            if (!results.getUAVDataResponse()?.uAVProject?.geom.isNullOrEmpty()) {
                var x1 =
                    results.getUAVDataResponse()?.uAVProject?.geom!![0]?.lat!! // lowest x (lat)
                var x2 =
                    results.getUAVDataResponse()?.uAVProject?.geom!![0]?.lat!! // highest x (lat)
                var y1 =
                    results.getUAVDataResponse()?.uAVProject?.geom!![0]?.lng!! // lowest y (lng)
                var y2 =
                    results.getUAVDataResponse()?.uAVProject?.geom!![0]?.lng!! //highest y (lng)

                val l: MutableList<List<Point>> = arrayListOf()
                val coordinate: MutableList<Point> = arrayListOf()
                for (i in results.getUAVDataResponse()?.uAVProject?.geom!!) {
                    coordinate.add(Point.fromLngLat(i.lng, i.lat))
                    if (x1 > i.lat) {
                        x1 = i.lat
                    }

                    if (x2 < i.lat) {
                        x2 = i.lat
                    }

                    if (y1 > i.lng) {
                        y1 = i.lng
                    }

                    if (y2 < i.lng) {
                        y2 = i.lng
                    }
                }

                centerProjectLat = x1 + ((x2 - x1) / 2)
                centerProjectLng = y1 + ((y2 - y1) / 2)

                l.add(coordinate)
                polygonProject?.coordinat = l
            }

            val polygon = Polygon.fromLngLats(polygonProject!!.coordinat)

            style.addSource(GeoJsonSource(polygonProject?.FILL_SOURCE_ID, polygon))
            style.addLayerBelow(
                FillLayer(
                    polygonProject?.FILL_LAYER_ID,
                    polygonProject?.FILL_SOURCE_ID
                ).withProperties(
                    fillColor(Color.parseColor("#ff8400"))
                ), LocationComponentConstants.SHADOW_LAYER
            )

            style.addSource(
                GeoJsonSource(
                    polygonProject?.LINE_SOURCE_ID,
                    LineString.fromLngLats(polygonProject!!.coordinat[0])
                )
            )
            style.addLayerBelow(
                LineLayer(
                    polygonProject?.LINE_LAYER_ID,
                    polygonProject?.LINE_SOURCE_ID
                ).withProperties(
                    lineColor(Color.WHITE),
                    lineWidth(1f)
                ), LocationComponentConstants.SHADOW_LAYER
            )

            btnProjectLocation.setOnClickListener {
                mapboxMap?.animateCamera(
                    CameraUpdateFactory.newLatLng(
                        LatLng(centerProjectLat, centerProjectLng)
                    )
                )
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun enableLocationComponent(loadedMapStyle: Style) {
        selectedStyle = loadedMapStyle

        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            val locationComponentOptions: LocationComponentOptions =
                LocationComponentOptions.builder(this)
//                    .layerAbove(loadedMapStyle.layers.last().id)
                    .build()

            val locationComponent = mapboxMap!!.locationComponent
            locationComponent.activateLocationComponent(
                LocationComponentActivationOptions.builder(this, loadedMapStyle)
                    .locationComponentOptions(locationComponentOptions).build()
            )
            locationComponent.isLocationComponentEnabled = true
            locationComponent.cameraMode = CameraMode.TRACKING
            locationComponent.renderMode = RenderMode.COMPASS

            initClick(loadedMapStyle)

            initLocationEngine()
//            initDataLayer(polygonLayerList)
//            initDataLayer(arrayListOf(polygonProject!!))
        } else {
            permissionsManager = PermissionsManager(this)
            permissionsManager?.requestLocationPermissions(this)
        }
    }

    /**
     * Set up the LocationEngine and the parameters for querying the device's location
     */
    @SuppressLint("MissingPermission")
    private fun initLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(this)
        val request: LocationEngineRequest =
            LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build()
        locationEngine?.requestLocationUpdates(request, callback, mainLooper)
        locationEngine?.getLastLocation(callback)
    }

    private class LocationChangeListeningActivityLocationCallback internal constructor(activity: MapsProjectActivity?) :
        LocationEngineCallback<LocationEngineResult?> {
        private val activityWeakReference: WeakReference<MapsProjectActivity?>?

        override fun onSuccess(result: LocationEngineResult?) {
            val activity: MapsProjectActivity? = activityWeakReference?.get()
            if (activity != null) {
                val location: Location = result?.lastLocation ?: return

                activity.tvLat.text = "Latitude : ${location.latitude}"
                activity.tvLng.text = "Longitude : ${location.longitude}"
                activity.tvTM3.text =
                    "TM 3 : ${activity.Deg2UTM(location.latitude, location.longitude)}"

                if (activity.mapboxMap != null && result.lastLocation != null) {
                    activity.mapboxMap?.locationComponent?.forceLocationUpdate(result.lastLocation)
                }
            }
        }

        override fun onFailure(exception: java.lang.Exception) {
            val activity: MapsProjectActivity? = activityWeakReference?.get()
            if (activity != null) {
                Toast.makeText(
                    activity, exception.localizedMessage,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        init {
            activityWeakReference = WeakReference(activity)
        }
    }

    @SuppressLint("NewApi")
    private fun initMapInteraction() {
        mapboxMap?.addOnMapClickListener {

            false
        }

    }

    @SuppressLint("NewApi")
    private fun initClick(loadedMapStyle: Style) {
        btnmyLocation.setOnClickListener {
            val lastKnownLocation = mapboxMap?.locationComponent?.lastKnownLocation

            mapboxMap?.animateCamera(
                CameraUpdateFactory.newLatLng(
                    LatLng(lastKnownLocation?.latitude!!, lastKnownLocation.longitude)
                )
            )
        }

        btnDone.setOnClickListener {
            val intent = intent
            intent.putExtra("objectId", selectedObjectId)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

        btnZoomIn.setOnClickListener {
            mapboxMap?.animateCamera(CameraUpdateFactory.zoomIn())
        }

        btnZoomOut.setOnClickListener {
            mapboxMap?.animateCamera(CameraUpdateFactory.zoomOut())
        }

        map_type_default.setOnClickListener {
            map_type_default_background.visibility = View.VISIBLE
            map_type_satellite_background.visibility = View.INVISIBLE
            map_type_terrain_background.visibility = View.INVISIBLE
            map_type_default_text.setTextColor(Color.BLUE)
            map_type_satellite_text.setTextColor(Color.parseColor("#808080"))
            map_type_terrain_text.setTextColor(Color.parseColor("#808080"))
//            mapboxMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
            mapboxMap?.setStyle(
                Style.Builder().fromUri(Style.LIGHT)
                    .withImage(
                        MARKER_ID, BitmapFactory.decodeResource(
                            this.resources, R.drawable.mapbox_marker_icon_default
                        )
                    )
                    .withSource(
                        GeoJsonSource(
                            MARKER_SOURCE_ID,
                            FeatureCollection.fromFeatures(arrayListOf())
                        )
                    )
                    .withLayer(
                        SymbolLayer(MARKER_LAYER_ID, MARKER_SOURCE_ID)
                            .withProperties(
                                iconImage(MARKER_ID),
                                iconAllowOverlap(true),
                                iconIgnorePlacement(true)
                            )
                    )
            ) { style ->
                selectedStyle = style
                setUAVData(style)
                setPolygonLocal(style)
                enableLocationComponent(style)
            }
            map_type_selection.visibility = View.GONE
            cvCurrentLocation.visibility = View.VISIBLE
        }

        // Handle selection of the Satellite map type
        map_type_satellite.setOnClickListener {
            map_type_default_background.visibility = View.INVISIBLE
            map_type_satellite_background.visibility = View.VISIBLE
            map_type_terrain_background.visibility = View.INVISIBLE
            map_type_default_text.setTextColor(Color.parseColor("#808080"))
            map_type_satellite_text.setTextColor(Color.BLUE)
            map_type_terrain_text.setTextColor(Color.parseColor("#808080"))
//            mMap!!.mapType = GoogleMap.MAP_TYPE_HYBRID
            mapboxMap?.setStyle(
                Style.Builder().fromUri(Style.SATELLITE)
                    .withImage(
                        MARKER_ID, BitmapFactory.decodeResource(
                            this.resources, R.drawable.mapbox_marker_icon_default
                        )
                    )
                    .withSource(
                        GeoJsonSource(
                            MARKER_SOURCE_ID,
                            FeatureCollection.fromFeatures(arrayListOf())
                        )
                    )
                    .withLayer(
                        SymbolLayer(MARKER_LAYER_ID, MARKER_SOURCE_ID)
                            .withProperties(
                                iconImage(MARKER_ID),
                                iconAllowOverlap(true),
                                iconIgnorePlacement(true)
                            )
                    )
            ) { style ->
                selectedStyle = style
                setUAVData(style)
                setPolygonLocal(style)
                enableLocationComponent(style)
            }
            map_type_selection.visibility = View.GONE
            cvCurrentLocation.visibility = View.VISIBLE
        }

        // Handle selection of the terrain map type
        map_type_terrain.setOnClickListener {
            map_type_default_background.visibility = View.INVISIBLE
            map_type_satellite_background.visibility = View.INVISIBLE
            map_type_terrain_background.visibility = View.VISIBLE
            map_type_default_text.setTextColor(Color.parseColor("#808080"))
            map_type_satellite_text.setTextColor(Color.parseColor("#808080"))
            map_type_terrain_text.setTextColor(Color.BLUE)
            mapboxMap?.setStyle(Style.MAPBOX_STREETS)
            map_type_selection.visibility = View.GONE
            cvCurrentLocation.visibility = View.VISIBLE
        }

//        swLandmark.setOnCheckedChangeListener { _, b ->
//            for (i in polygonLayerList) {
//                if (i.type == "real") {
//                    if (b) {
//                        loadedMapStyle.getLayer(i.LINE_LAYER_ID)?.setProperties(visibility(VISIBLE))
//                        loadedMapStyle.getLayer(i.CIRCLE_LAYER_ID)?.setProperties(visibility(VISIBLE))
//                        loadedMapStyle.getLayer(i.FILL_LAYER_ID)?.setProperties(visibility(VISIBLE))
//                    } else {
//                        loadedMapStyle.getLayer(i.LINE_LAYER_ID)?.setProperties(visibility(NONE))
//                        loadedMapStyle.getLayer(i.CIRCLE_LAYER_ID)?.setProperties(visibility(NONE))
//                        loadedMapStyle.getLayer(i.FILL_LAYER_ID)?.setProperties(visibility(NONE))
//                    }
//                }
//            }
//        }

        swImageOverlay.setOnCheckedChangeListener { _, b ->
            for (i in IMAGE_OVERLAY_SOURCE_IDs) {
                if (b) {
                    loadedMapStyle.getLayer(i)?.setProperties(visibility(VISIBLE))
                } else {
                    loadedMapStyle.getLayer(i)?.setProperties(visibility(NONE))
                }
            }
        }

        swObjectUAV.setOnCheckedChangeListener { _, b ->
            for (i in polygonLayerList) {
                if (i.type == "uav-object") {
                    if (b) {
                        loadedMapStyle.getLayer(i.CIRCLE_LAYER_ID)
                            ?.setProperties(visibility(VISIBLE))
                        loadedMapStyle.getLayer(i.FILL_LAYER_ID)?.setProperties(visibility(VISIBLE))
                        loadedMapStyle.getLayer(i.LINE_LAYER_ID)?.setProperties(visibility(VISIBLE))
                    } else {
                        loadedMapStyle.getLayer(i.CIRCLE_LAYER_ID)?.setProperties(visibility(NONE))
                        loadedMapStyle.getLayer(i.FILL_LAYER_ID)?.setProperties(visibility(NONE))
                        loadedMapStyle.getLayer(i.LINE_LAYER_ID)?.setProperties(visibility(NONE))
                    }
                }
            }
        }

        swProject.setOnCheckedChangeListener { _, b ->
            if (polygonProject != null) {
                if (b) {
                    loadedMapStyle.getLayer(polygonProject!!.CIRCLE_LAYER_ID)
                        ?.setProperties(visibility(VISIBLE))
                    loadedMapStyle.getLayer(polygonProject!!.FILL_LAYER_ID)
                        ?.setProperties(visibility(VISIBLE))
                    loadedMapStyle.getLayer(polygonProject!!.LINE_LAYER_ID)
                        ?.setProperties(visibility(VISIBLE))
                } else {
                    loadedMapStyle.getLayer(polygonProject!!.CIRCLE_LAYER_ID)
                        ?.setProperties(visibility(NONE))
                    loadedMapStyle.getLayer(polygonProject!!.FILL_LAYER_ID)
                        ?.setProperties(visibility(NONE))
                    loadedMapStyle.getLayer(polygonProject!!.LINE_LAYER_ID)
                        ?.setProperties(visibility(NONE))
                }
            }
        }

        swSurvey.setOnCheckedChangeListener { _, b ->
            for (i in polygonLayerList) {
                if (i.type == "real") {
                    if (b) {
                        loadedMapStyle.getLayer(i.CIRCLE_LAYER_ID)
                            ?.setProperties(visibility(VISIBLE))
                        loadedMapStyle.getLayer(i.FILL_LAYER_ID)?.setProperties(visibility(VISIBLE))
                        loadedMapStyle.getLayer(i.LINE_LAYER_ID)?.setProperties(visibility(VISIBLE))
                    } else {
                        loadedMapStyle.getLayer(i.CIRCLE_LAYER_ID)?.setProperties(visibility(NONE))
                        loadedMapStyle.getLayer(i.FILL_LAYER_ID)?.setProperties(visibility(NONE))
                        loadedMapStyle.getLayer(i.LINE_LAYER_ID)?.setProperties(visibility(NONE))
                    }
                }
            }
        }

        swImageOverlay.isChecked = intentSetting.showImageUAV
        swProject.isChecked = intentSetting.showProjectUAV
        swObjectUAV.isChecked = intentSetting.showObjectUAV
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        permissionsManager?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onExplanationNeeded(permissionsToExplain: List<String?>?) {
        Toast.makeText(
            this, "Harap aktifkan lokasi",
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            mapboxMap!!.getStyle { style -> enableLocationComponent(style) }
        } else {
            Toast.makeText(this, "Permission tidak diberi akses", Toast.LENGTH_LONG).show()
        }
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()

        if (selectedLayerId.isNotEmpty()) {

            val resultsDataTanah = realm.where(ProjectLand::class.java)
                .equalTo("selectedLinkedObjectUAVId", selectedObjectId).findFirst()

            val resultsOwner =
                realm.where(PartyAdd::class.java).equalTo("areaId", selectedObjectId).findFirst()

            val resultsInfrastructure =
                realm.where(SaranaLain::class.java).equalTo("LandIdTemp", selectedObjectId).findFirst()

            val resultsPlant =
                realm.where(Tanaman::class.java).equalTo("LandIdTemp", selectedObjectId).findFirst()

            val resultsDataBuilding = realm.where(Bangunan::class.java)
                .equalTo("LandIdTemp", selectedObjectId).findFirst()

            selectedStyle.getLayer(selectedLayerId)?.setProperties(
                when {
                    resultsOwner == null && resultsDataTanah == null && resultsDataBuilding == null &&
                            resultsInfrastructure == null && resultsPlant == null -> {
                        fillColor(Color.parseColor("#CCCCCC"))//abu
                    }
                    resultsOwner != null && resultsDataTanah != null && resultsDataBuilding != null &&
                            resultsInfrastructure != null && resultsPlant != null -> {
                        fillColor(Color.parseColor("#66C99C"))//hijau
                    }
                    else -> {
                        fillColor(Color.parseColor("#F7CE46"))//kuning
                    }
                },
                fillOpacity(1f)
            )
        }
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    /*navigation drawer*/
    @SuppressLint("NewApi")
    fun funreplaceFragment(position: Int) {
        when (position) {
            0 -> {
                /*Type map*/
                map_type_selection.visibility = View.GONE
                cvCurrentLocation.visibility = View.VISIBLE
                // Start animator to reveal the selection view, starting from the FAB itself
                val anim = ViewAnimationUtils.createCircularReveal(
                    map_type_selection,
                    map_type_selection.width - (map_type_selection.width / 2),
                    map_type_selection.height / 2,
                    map_type_selection.width / 2f,
                    map_type_selection.width.toFloat()
                )
                anim.duration = 200
                anim.interpolator = AccelerateDecelerateInterpolator()

                anim.addListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationStart(animation: Animator?) {
                        super.onAnimationEnd(animation)
                        map_type_selection.visibility = View.VISIBLE
                        cvCurrentLocation.visibility = View.GONE
                    }
                })

                anim.start()
            }

            1 -> {
                /*Filter map*/
                map_type_selection.visibility = View.GONE
                cvCurrentLocation.visibility = View.VISIBLE
                // Start animator to reveal the selection view, starting from the FAB itself
//                val anim = ViewAnimationUtils.createCircularReveal(
//                        cvFilter,
//                        cvFilter.width - (cvFilter.width / 2),
//                        cvFilter.height / 2,
//                        cvFilter.width / 2f,
//                        cvFilter.width.toFloat())
//                anim.duration = 200
//                anim.interpolator = AccelerateDecelerateInterpolator()
//
//                anim.addListener(object : AnimatorListenerAdapter() {
//                    override fun onAnimationStart(animation: Animator?) {
//                        super.onAnimationEnd(animation)
//                        cvFilter.visibility = View.VISIBLE
//                    }
//                })
//
//                anim.start()
            }
        }
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    companion object {
        internal const val TAG = "MapsActivityDrawing"
        private const val DEFAULT_ZOOM: Float = 18f
        fun getStartIntent(context: Context): Intent {
            return Intent(context, MapsProjectActivity::class.java)
        }
    }

    override fun onMapClick(point: LatLng): Boolean {
        map_type_selection.visibility = View.GONE
        cvCurrentLocation.visibility = View.VISIBLE

        if (intentSetting.canTouchObjectUAV) {
            val pointf: PointF = mapboxMap!!.projection.toScreenLocation(point)
            val rectF = RectF(pointf.x - 10, pointf.y - 10, pointf.x + 10, pointf.y + 10)

            val projectFeature: List<Feature> =
                mapboxMap!!.queryRenderedFeatures(rectF, polygonProject?.FILL_LAYER_ID)

            for (i in polygonLayerList) {
                val featureList: List<Feature> =
                    mapboxMap!!.queryRenderedFeatures(rectF, i.FILL_LAYER_ID)
                if (featureList.isNotEmpty()) {
                    for (feature in featureList) {
                        if (intentSetting.canSelectObjectUAV) {
                            if (selectedLayerId != i.FILL_LAYER_ID && i.landData == null && i.buildingData == null) {
                                btnDone.visibility = View.VISIBLE
                                selectedStyle.getLayer(i.FILL_LAYER_ID)?.setProperties(
                                    fillColor(Color.parseColor("#00a6ff"))
                                )
                                if (selectedLayerId.isNotEmpty()) {
                                    selectedStyle.getLayer(selectedLayerId)?.setProperties(
                                        fillColor(Color.parseColor("#ff006a"))
                                    )
                                }
                                selectedLayerId = i.FILL_LAYER_ID
                            }
                        } else {
                            selectedLayerId = i.FILL_LAYER_ID
                            showBottomsheet(i.id)
                        }
                        selectedObjectId = i.id
                    }
                    return true
                }
            }
        }
        return false
    }


    var bottomSheetDialog:BottomSheetDialog? = null

    private fun showBottomsheet(areaId: Int?) {
        val bsd: View = layoutInflater.inflate(R.layout.bottomsheet_map_project, null)

        if(bottomSheetDialog != null && bottomSheetDialog?.isShowing!!){
            return
        }
        else{
            bottomSheetDialog = BottomSheetDialog(this)
        }

        bottomSheetDialog!!.setContentView(bsd)

        val bottomSheet = bottomSheetDialog!!.findViewById<View>(R.id.llOuter) as LinearLayout
        val coordinatorLayout = bottomSheet.parent as CoordinatorLayout
        val bottomSheetBehavior = BottomSheetBehavior.from(coordinatorLayout.parent as View)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED



        bottomSheetDialog!!.tvProjectName?.text = projectData?.getProjectName()
        bottomSheetDialog!!.tvAreaName?.text = "Bidang ${projectData?.getUAVDataResponse()?.objects?.first { it.id == areaId }?.name}"

        bottomSheetDialog!!.rvOwner.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        bottomSheetDialog!!.rvLand.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        bottomSheetDialog!!.rvRoom.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        bottomSheetDialog!!.rvBuilding.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        bottomSheetDialog!!.rvInfrastructure.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        bottomSheetDialog!!.rvPlant.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        val resultsOwner = realm.where(PartyAdd::class.java)
            .equalTo("ProjectId", intent.getIntExtra("projectId", 0))
            .equalTo("areaId", areaId)
            .sort("TempId", Sort.DESCENDING)
            .findAll()

        if(resultsOwner.isNullOrEmpty()){
            Glide.with(this).load(R.drawable.ic_unchecked).into(bottomSheetDialog!!.imgStatusOwner)
        }
        else{
            Glide.with(this).load(R.drawable.ic_checked_green).into(bottomSheetDialog!!.imgStatusOwner)
        }

        val ownerAdapter =
            AdapterDaftarKepemilikanTanahPertama(this, intent.getIntExtra("projectId", 0))
        ownerAdapter.setCallback(object : AdapterDaftarKepemilikanTanahPertama.Callback{
            override fun onRepoEmptyViewRetryClick() {

            }

            override fun onDismissBottomsheet() {
                bottomSheetDialog?.dismiss()
            }
        })
        bottomSheetDialog!!.rvOwner.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        bottomSheetDialog!!.rvOwner.adapter = ownerAdapter
        ownerAdapter.addItems(resultsOwner)

        bottomSheetDialog!!.btnAddDataOwner.setOnClickListener {
            bottomSheetDialog?.dismiss()
            val param = "['${intent.getIntExtra("projectId", 0)}','$areaId', 1, '']"
            val intent = ActivityFormSubjek.getStartIntent(this)
            intent.putExtra("data_current_project2", param)
            startActivity(intent)
        }

        val resultsLand = realm.where(ProjectLand::class.java)
            .equalTo("ProjectId", intent.getIntExtra("projectId", 0))
            .equalTo("PartyIdTemp", areaId)
            .findAll()

        if(resultsLand.isNullOrEmpty()){
            Glide.with(this).load(R.drawable.ic_unchecked).into(bottomSheetDialog!!.imgStatusLand)
        }
        else{
            Glide.with(this).load(R.drawable.ic_checked_green).into(bottomSheetDialog!!.imgStatusLand)
        }

        val landAdapter = AdapterDaftarTanah(this, this)
        landAdapter.setCallback(object: AdapterDaftarTanah.Callback{
            override fun onRepoEmptyViewRetryClick() {

            }

            override fun onDismissBottomsheet() {
                bottomSheetDialog?.dismiss()
            }
        })
        bottomSheetDialog!!.rvLand.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        bottomSheetDialog!!.rvLand.adapter = landAdapter
        landAdapter.addItems(resultsLand)

        bottomSheetDialog!!.btnAddDataLand.setOnClickListener {
            if (MapBoxUtils().locationStatusCheck(this)) {
                bottomSheetDialog?.dismiss()
                Utils.deletedataformtanah1(this)
                Utils.deletedataformtanah2(this)
                Utils.deletedataformtanah3(this)
                Utils.deletedataformtanah4(this)

                val param = "['${areaId}','${intent.getIntExtra("projectId", 0)}']"
                val intent = ActicityFormAddObjekTanah.getStartIntent(this)
                intent.putExtra("param_to_form_tanah", param)
                startActivity(intent)
            }
        }

        //data ruangan
        val resultRoom = realm.where(Room::class.java)
            .equalTo("ProjectId", intent.getIntExtra("projectId", 0))
            .equalTo("LandIdTemp", areaId)
            .findAll()

        if(resultRoom.isNullOrEmpty()){
            Glide.with(this).load(R.drawable.ic_unchecked).into(bottomSheetDialog!!.imgStatusRoom)
        }
        else{
            Glide.with(this).load(R.drawable.ic_checked_green).into(bottomSheetDialog!!.imgStatusRoom)
        }

        val roomAdapter = AdapterDaftarObjekRoom(this, this)
        roomAdapter.setCallback(object: AdapterDaftarObjekRoom.Callback{
            override fun onRepoEmptyViewRetryClick() {

            }

            override fun onDismissBottomsheet() {
                bottomSheetDialog?.dismiss()
            }
        })

        bottomSheetDialog!!.rvRoom.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        bottomSheetDialog!!.rvRoom.adapter = roomAdapter
        roomAdapter.addItems(resultRoom)


        val historyDataRoom = "['$areaId','${intent.getIntExtra("projectId", 0)}']"
        bottomSheetDialog?.btnAddDataRoom?.setOnClickListener {
            bottomSheetDialog?.dismiss()
            val intent = ActicityFormAddObjekRoom.getStartIntent(this)
            intent.putExtra("data_current_tanah", historyDataRoom)
            startActivity(intent)
        }

        //bangunan

        val resultBuilding = realm.where(Bangunan::class.java)
            .equalTo("ProjectId", intent.getIntExtra("projectId", 0))
            .equalTo("LandIdTemp", areaId)
            .findAll()

        if(resultBuilding.isNullOrEmpty()){
            Glide.with(this).load(R.drawable.ic_unchecked).into(bottomSheetDialog!!.imgStatusBuilding)
        }
        else{
            Glide.with(this).load(R.drawable.ic_checked_green).into(bottomSheetDialog!!.imgStatusBuilding)
        }

        val buildingAdapter = AdapterDaftarObjekGedung(this, this)
        buildingAdapter.setCallback(object: AdapterDaftarObjekGedung.Callback{
            override fun onRepoEmptyViewRetryClick() {

            }

            override fun onDismissBottomsheet() {
                bottomSheetDialog?.dismiss()
            }
        })

        bottomSheetDialog!!.rvBuilding.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        bottomSheetDialog!!.rvBuilding.adapter = buildingAdapter
        buildingAdapter.addItems(resultBuilding)


        val historyData = "['$areaId','${intent.getIntExtra("projectId", 0)}']"
        bottomSheetDialog?.btnAddDataBuilding?.setOnClickListener {
            bottomSheetDialog?.dismiss()
            val intent = ActicityFormAddObjekBangunan.getStartIntent(this)
            intent.putExtra("data_current_tanah", historyData)
            startActivity(intent)
        }

        //sarana lain

        val resultInfrastructure = realm.where(SaranaLain::class.java)
            .equalTo("ProjectId", intent.getIntExtra("projectId", 0))
            .equalTo("LandIdTemp", areaId)
            .sort("SaranaIdTemp", Sort.DESCENDING)
            .findAll()

        if(resultInfrastructure.isNullOrEmpty()){
            Glide.with(this).load(R.drawable.ic_unchecked).into(bottomSheetDialog!!.imgStatusInfrastructure)
        }
        else{
            Glide.with(this).load(R.drawable.ic_checked_green).into(bottomSheetDialog!!.imgStatusInfrastructure)
        }

        val infrastructureAdapter = AdapterDaftarObjekLain(this, this)
        infrastructureAdapter.setCallback(object: AdapterDaftarObjekLain.Callback{
            override fun onDismissBottomsheet() {
                bottomSheetDialog?.dismiss()
            }

            override fun onRepoEmptyViewRetryClick() {

            }
        })
        bottomSheetDialog!!.rvInfrastructure.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        bottomSheetDialog!!.rvInfrastructure.adapter = infrastructureAdapter
        infrastructureAdapter.addItems(resultInfrastructure)

        bottomSheetDialog!!.btnAddDataInfrastructure.setOnClickListener {
            bottomSheetDialog?.dismiss()
            val param = "['$areaId','${intent.getIntExtra("projectId", 0)}']"
            val intent = ActicityFormAddObjekLain.getStartIntent(this)
            intent.putExtra("data_current_tanah", param)
            startActivity(intent)
        }

        //tanaman

        val resultPlant = realm.where(Tanaman::class.java)
            .equalTo("ProjectId", intent.getIntExtra("projectId", 0))
            .equalTo("LandIdTemp", areaId)
            .sort("TanamanIdTemp", Sort.DESCENDING)
            .findAll()

        if(resultPlant.isNullOrEmpty()){
            Glide.with(this).load(R.drawable.ic_unchecked).into(bottomSheetDialog!!.imgStatusPlant)
        }
        else{
            Glide.with(this).load(R.drawable.ic_checked_green).into(bottomSheetDialog!!.imgStatusPlant)
        }

        val plantAdapter = AdapterDaftarObjekTanaman(this, this)
        plantAdapter.setCallback(object: AdapterDaftarObjekTanaman.Callback{
            override fun onRepoEmptyViewRetryClick() {

            }

            override fun onDismissBottomsheet() {
                bottomSheetDialog?.dismiss()
            }
        })
        bottomSheetDialog!!.rvPlant.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        bottomSheetDialog!!.rvPlant.adapter = plantAdapter
        plantAdapter.addItems(resultPlant)

        bottomSheetDialog!!.btnAddDataPlant.setOnClickListener {
            bottomSheetDialog?.dismiss()
            val param = "['${selectedObjectId}','${intent.getIntExtra("projectId", 0)}']"
            val intent = ActivityFormAddObjekTanaman.getStartIntent(this)
            intent.putExtra("data_current_tanah", param)
            startActivity(intent)
        }

        bottomSheetDialog!!.cvOwner.setOnClickListener {
            bottomSheetDialog!!.expOwner.isExpanded = !bottomSheetDialog!!.expOwner.isExpanded
        }

        bottomSheetDialog!!.cvLand.setOnClickListener {
            bottomSheetDialog!!.expLand.isExpanded = !bottomSheetDialog!!.expLand.isExpanded
        }

        bottomSheetDialog!!.cvRoom.setOnClickListener {
            bottomSheetDialog!!.expRoom.isExpanded = !bottomSheetDialog!!.expRoom.isExpanded
        }

        bottomSheetDialog!!.cvBuilding.setOnClickListener {
            bottomSheetDialog!!.expBuilding.isExpanded = !bottomSheetDialog!!.expBuilding.isExpanded
        }

        bottomSheetDialog!!.cvInfrastructure.setOnClickListener {
            bottomSheetDialog!!.expInfrastructure.isExpanded =
                !bottomSheetDialog!!.expInfrastructure.isExpanded
        }

        bottomSheetDialog!!.cvPlant.setOnClickListener {
            bottomSheetDialog!!.expPlant.isExpanded = !bottomSheetDialog!!.expPlant.isExpanded
        }

        bottomSheetDialog!!.show()
    }

    private fun Deg2UTM(Lat: Double?, Lon: Double?): String {
        return if (Lat != null && Lon != null) {
            val zone: Int = floor(Lon / 6 + 31).toInt()
            val letter =
                if (Lat < -72) 'C' else if (Lat < -64) 'D' else if (Lat < -56) 'E' else if (Lat < -48) 'F' else if (Lat < -40) 'G' else if (Lat < -32) 'H' else if (Lat < -24) 'J' else if (Lat < -16) 'K' else if (Lat < -8) 'L' else if (Lat < 0) 'M' else if (Lat < 8) 'N' else if (Lat < 16) 'P' else if (Lat < 24) 'Q' else if (Lat < 32) 'R' else if (Lat < 40) 'S' else if (Lat < 48) 'T' else if (Lat < 56) 'U' else if (Lat < 64) 'V' else if (Lat < 72) 'W' else 'X'
            var easting = 0.5 * ln(
                (1 + cos(Lat * Math.PI / 180) * sin(Lon * Math.PI / 180 - (6 * zone - 183) * Math.PI / 180)) / (1 - cos(
                    Lat * Math.PI / 180
                ) * sin(Lon * Math.PI / 180 - (6 * zone - 183) * Math.PI / 180))
            ) * 0.9996 * 6399593.62 / (1 + 0.0820944379.pow(2.0) * cos(Lat * Math.PI / 180).pow(2.0)).pow(
                0.5
            ) * (1 + 0.0820944379.pow(2.0) / 2 * (0.5 * ln(
                (1 + cos(Lat * Math.PI / 180) * sin(Lon * Math.PI / 180 - (6 * zone - 183) * Math.PI / 180)) / (1 - cos(
                    Lat * Math.PI / 180
                ) * sin(Lon * Math.PI / 180 - (6 * zone - 183) * Math.PI / 180))
            )).pow(2.0) * Math.pow(Math.cos(Lat * Math.PI / 180), 2.0) / 3) + 500000
            easting = (easting * 100).roundToInt() * 0.01
            var northing =
                (atan(tan(Lat * Math.PI / 180) / cos(Lon * Math.PI / 180 - (6 * zone - 183) * Math.PI / 180)) - Lat * Math.PI / 180) * 0.9996 * 6399593.625 / sqrt(
                    1 + 0.006739496742 * cos(Lat * Math.PI / 180).pow(2.0)
                ) * (1 + 0.006739496742 / 2 * (0.5 * ln(
                    (1 + cos(Lat * Math.PI / 180) * sin(Lon * Math.PI / 180 - (6 * zone - 183) * Math.PI / 180)) / (1 - cos(
                        Lat * Math.PI / 180
                    ) * sin(Lon * Math.PI / 180 - (6 * zone - 183) * Math.PI / 180))
                )).pow(2.0) * Math.cos(Lat * Math.PI / 180)
                    .pow(2.0)) + 0.9996 * 6399593.625 * (Lat * Math.PI / 180 - 0.005054622556 * (Lat * Math.PI / 180 + sin(
                    2 * Lat * Math.PI / 180
                ) / 2) + 4.258201531e-05 * (3 * (Lat * Math.PI / 180 + sin(2 * Lat * Math.PI / 180) / 2) + sin(
                    2 * Lat * Math.PI / 180
                ) * cos(Lat * Math.PI / 180).pow(2.0)) / 4 - 1.674057895e-07 * (5 * (3 * (Lat * Math.PI / 180 + sin(
                    2 * Lat * Math.PI / 180
                ) / 2) + sin(2 * Lat * Math.PI / 180) * Math.cos(Lat * Math.PI / 180)
                    .pow(2.0)) / 4 + Math.sin(2 * Lat * Math.PI / 180) * Math.cos(Lat * Math.PI / 180)
                    .pow(2.0) * Math.cos(Lat * Math.PI / 180).pow(2.0)) / 3)
            if (letter < 'M') northing += 10000000
            northing = (northing * 100).roundToInt() * 0.01

            "E $easting, N $northing"
        } else {
            "Terjadi Kesalahan"
        }
    }
}
