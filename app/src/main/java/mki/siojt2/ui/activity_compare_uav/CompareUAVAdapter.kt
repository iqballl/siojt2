package mki.siojt2.ui.activity_compare_uav

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.item_compare_uav.view.*
import mki.siojt2.R
import mki.siojt2.base.BaseViewHolder
import mki.siojt2.model.UAVCompare
import mki.siojt2.utils.extension.SafeClickListener
import kotlin.math.abs


class CompareUAVAdapter(context: Context, dataset: MutableList<UAVCompare>?) : androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder>() {

    private val mContext: Context? = context
    var mDataset: MutableList<UAVCompare>? = ArrayList()
    var rowLayout = R.layout.item_compare_uav
    var emptyrowLayout = R.layout.item_empty_view
    var pos = 0

    private var mCallback: Callback? = null

    init {
        this.mDataset = dataset
    }

    fun setCallback(callback: Callback) {
        mCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            VIEW_TYPE_NORMAL -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(rowLayout, parent, false))
            VIEW_TYPE_EMPTY -> EmptyViewHolder(
                    LayoutInflater.from(parent.context).inflate(R.layout.item_empty_view, parent, false))
            else -> EmptyViewHolder(LayoutInflater.from(parent.context).inflate(emptyrowLayout, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (mDataset != null && !mDataset.isNullOrEmpty()) {
            VIEW_TYPE_NORMAL
        } else {
            VIEW_TYPE_EMPTY
        }
    }

    override fun getItemCount(): Int {
        return if (!mDataset!!.isNullOrEmpty()) {
            mDataset!!.size
        } else {
            1
        }
    }

    /*inner class RuangViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var r_no: TextView = itemView.findViewById(R.id.rowNo) as TextView
        var r_tvTitle: TextView = itemView.findViewById(R.id.tvTitle) as TextView
        var r_tvContent1: TextView = itemView.findViewById(R.id.tvContent1) as TextView
        var r_tvContent2: TextView = itemView.findViewById(R.id.tvContent2) as TextView

    }*/

    fun clear() {
        this.mDataset!!.clear()
    }

    fun addItems(data: List<UAVCompare>) {
        this.mDataset!!.addAll(data)
        notifyDataSetChanged()
    }

    interface Callback {
        fun onRepoEmptyViewRetryClick()
    }

    inner class ViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }

        @SuppressLint("SetTextI18n")
        override fun onBind(position: Int) {
            super.onBind(position)

            val dataList = mDataset!![position]
            itemView.tvId.text = dataList.id
            itemView.tvGroup.text = dataList.group
            itemView.tvLandSurvey.text = dataList.landAreaSize?.survey
            itemView.tvLandUAV.text = dataList.landAreaSize?.uav
            itemView.tvBuildingSurvey.text = dataList.buildingAreaSize?.survey
            itemView.tvBuildingUAV.text = dataList.buildingAreaSize?.uav
            itemView.tvStatus.text = if (!dataList.buildingAreaSize?.survey.isNullOrEmpty() &&
                    !dataList.buildingAreaSize?.uav.isNullOrEmpty() &&
                    (abs(dataList.buildingAreaSize?.survey!!.toDouble() - dataList.buildingAreaSize?.uav!!.toDouble()) <= 0.5)) {
                "OK"
            } else {
                ""
            }

            if (position < mDataset!!.size - 1) {

                if (dataList.id == "") {
                    itemView.tvId.setBackgroundColor(mContext?.resources?.getColor(R.color.transparent)!!)
                } else {
                    itemView.tvId.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_transparent_top)
                }

                if (dataList.group == "") {
                    itemView.tvLandSurvey.text = ""
                    itemView.tvLandUAV.text = ""
                    itemView.tvGroup.setBackgroundColor(mContext?.resources?.getColor(R.color.transparent)!!)
                    if (!dataList.landAreaSize?.survey.isNullOrEmpty() && !dataList.landAreaSize?.uav.isNullOrEmpty() && (abs(dataList.landAreaSize?.survey!!.toDouble() - dataList.landAreaSize?.uav!!.toDouble()) > 0.5)) {
                        itemView.tvLandSurvey.setBackgroundColor(mContext.resources?.getColor(R.color.colorRealRed)!!)
                        itemView.tvLandUAV.setBackgroundColor(mContext.resources?.getColor(R.color.colorRealRed)!!)
                    } else {
                        itemView.tvLandSurvey.setBackgroundColor(mContext.resources?.getColor(R.color.transparent)!!)
                        itemView.tvLandUAV.setBackgroundColor(mContext.resources?.getColor(R.color.transparent)!!)
                    }
                } else {
                    itemView.tvGroup.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_transparent_top)
                    if (!dataList.landAreaSize?.survey.isNullOrEmpty() && !dataList.landAreaSize?.uav.isNullOrEmpty() && (abs(dataList.landAreaSize?.survey!!.toDouble() - dataList.landAreaSize?.uav!!.toDouble()) > 0.5)) {
                        itemView.tvLandSurvey.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_red_top)
                        itemView.tvLandUAV.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_red_top)
                    } else {
                        itemView.tvLandSurvey.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_transparent_top)
                        itemView.tvLandUAV.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_transparent_top)
                    }
                }

                if (!dataList.buildingAreaSize?.survey.isNullOrEmpty() &&
                        !dataList.buildingAreaSize?.uav.isNullOrEmpty() &&
                        (abs(dataList.buildingAreaSize?.survey!!.toDouble() - dataList.buildingAreaSize?.uav!!.toDouble()) > 0.5)) {
                    itemView.tvBuildingSurvey.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_red_top)
                    itemView.tvBuildingUAV.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_red_top)
                } else {
                    itemView.tvBuildingSurvey.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_transparent_top)
                    itemView.tvBuildingUAV.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_transparent_top)
                }

                itemView.tvStatus.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_transparent_top)
            } else {

                if(dataList.id == ""){
                    itemView.tvId.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_transparent_bottom)
                }
                else{
                    itemView.tvId.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_transparent)
                }

                if (dataList.group == ""){
                    itemView.tvGroup.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_transparent_bottom)
                    if (!dataList.landAreaSize?.survey.isNullOrEmpty() && !dataList.landAreaSize?.uav.isNullOrEmpty() && (abs(dataList.landAreaSize?.survey!!.toDouble() - dataList.landAreaSize?.uav!!.toDouble()) <= 0.5)) {
                        itemView.tvLandSurvey.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_transparent_bottom)
                        itemView.tvLandUAV.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_transparent_bottom)
                    } else {
                        itemView.tvLandSurvey.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_red_bottom)
                        itemView.tvLandUAV.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_red_bottom)
                    }
                }
                else{
                    itemView.tvGroup.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_transparent)
                    if (!dataList.landAreaSize?.survey.isNullOrEmpty() && !dataList.landAreaSize?.uav.isNullOrEmpty() && (abs(dataList.landAreaSize?.survey!!.toDouble() - dataList.landAreaSize?.uav!!.toDouble()) <= 0.5)) {
                        itemView.tvLandSurvey.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_transparent)
                        itemView.tvLandUAV.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_transparent)
                    } else {
                        itemView.tvLandSurvey.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_red)
                        itemView.tvLandUAV.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_red)
                    }
                }

                if (!dataList.buildingAreaSize?.survey.isNullOrEmpty() &&
                        !dataList.buildingAreaSize?.uav.isNullOrEmpty() &&
                        (abs(dataList.buildingAreaSize?.survey!!.toDouble() - dataList.buildingAreaSize?.uav!!.toDouble()) > 0.5)) {
                    itemView.tvBuildingSurvey.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_red)
                    itemView.tvBuildingUAV.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_red)
                } else {
                    itemView.tvBuildingSurvey.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_transparent)
                    itemView.tvBuildingUAV.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_transparent)
                }

                itemView.tvStatus.background = mContext?.resources?.getDrawable(R.drawable.bg_underline_black_transparent)
            }
        }
    }

    inner class EmptyViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }


    }

    private fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
        val safeClickListener = SafeClickListener {
            onSafeClick(it)
        }
        setOnClickListener(safeClickListener)
    }

    companion object {
        const val VIEW_TYPE_EMPTY = 0
        const val VIEW_TYPE_NORMAL = 1
    }
}
