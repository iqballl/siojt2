package mki.siojt2.ui.activity_compare_uav

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_compare_uav.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.model.AreaSize
import mki.siojt2.model.ResponseDataListProjectLocal
import mki.siojt2.model.UAVCompare
import mki.siojt2.model.UAVObjectsItem
import mki.siojt2.model.localsave.Bangunan
import mki.siojt2.model.localsave.ProjectLand
import mki.siojt2.model.region.ResponseDataKecamatanWithoutParameter
import mki.siojt2.utils.realm.RealmController

class CompareUAVActivity : BaseActivity() {

    lateinit var realm: Realm

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, CompareUAVActivity::class.java)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_compare_uav)

        initData()
    }

    private fun initData() {
        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        val landData = realm.where(ProjectLand::class.java).findAll()
        val compareData: MutableList<UAVCompare> = arrayListOf()
        if (!landData.isNullOrEmpty()) {
            var group = ""
            for (i in landData) {
                val buildingData = realm.where(Bangunan::class.java).equalTo("LandIdTemp", i.landIdTemp).findAll()
                val uavDataLand = realm.where(UAVObjectsItem::class.java)
                        .equalTo("id", i.selectedLinkedObjectUAVId)
                        .findFirst()

                if (buildingData.isNullOrEmpty()) {
                    compareData.add(
                            UAVCompare(
                                    id = if (group != "${i.landDistrictName} ${i.projectId}") {
                                        i.landIdTemp.toString()
                                    } else {
                                        ""
                                    },
                                    group = if (group != "${i.landDistrictName} ${i.projectId}") {
                                        "${i.landDistrictName} ${i.projectId}"
                                    } else {
                                        ""
                                    },
                                    landAreaSize = AreaSize(survey = i.landAreaCertificate, uav = uavDataLand?.area),
                                    buildingAreaSize = AreaSize(
                                            survey = "",
                                            uav = "")))
                    group = "${i.landDistrictName} ${i.projectId}"
                } else {
                    for (b in buildingData) {
                        val uavDataBuilding = realm.where(UAVObjectsItem::class.java)
                                .equalTo("id", b.selectedLinkedObjectUAVId)
                                .findFirst()

                        compareData.add(
                                UAVCompare(
                                        id = if (group != "${i.landDistrictName} ${i.projectId}") {
                                            i.landIdTemp.toString()
                                        } else {
                                            ""
                                        },
                                        group = if (group != "${i.landDistrictName} ${i.projectId}") {
                                            "${i.landDistrictName} ${i.projectId}"
                                        } else {
                                            ""
                                        },
                                        landAreaSize = AreaSize(survey = i.landAreaCertificate, uav = uavDataLand?.area),
                                        buildingAreaSize = AreaSize(
                                                survey = b.projectBuildingAreaLowerRoom,
                                                uav = uavDataBuilding?.area)))
                        group = "${i.landDistrictName} ${i.projectId}"
                    }
                }
            }
        }


        val adapter = CompareUAVAdapter(this, compareData)
        rvCompare.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvCompare.adapter = adapter

    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }
}