package mki.siojt2.ui.activity_daftar_kepemilikan_tanah_pertama.presenter

import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.ui.activity_daftar_kepemilikan_tanah_pertama.view.DPPTView
import mki.siojt2.utils.ErrorHandler
import mki.siojt2.utils.rxJava.RxUtils

class DetailPengajuanDPPTPresenter : BasePresenter(), DetailPengajuanDPPTMVVPPresenter {

    private var disposables: CompositeDisposable = CompositeDisposable()


    override fun getlistdataPemilik(projectId: Int, accessToken: String) {
        view().onShowLoading()
        disposables.add(
                dataManager.getlistParties(projectId, accessToken)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetlistdataPemilik(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    private fun view(): DPPTView {
        return getView() as DPPTView
    }
}