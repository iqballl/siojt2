package mki.siojt2.ui.activity_daftar_kepemilikan_tanah_pertama.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.ResponseDataListPemilik


/**
 * Created by iqbal on 10/01/18.
 */
interface DPPTView : MvpView {

    fun onsuccessgetlistdataPemilik(responseDataListPemilik: MutableList<ResponseDataListPemilik>)
}