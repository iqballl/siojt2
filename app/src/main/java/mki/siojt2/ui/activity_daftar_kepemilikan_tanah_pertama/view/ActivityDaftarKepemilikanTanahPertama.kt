package mki.siojt2.ui.activity_daftar_kepemilikan_tanah_pertama.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.widget.LinearLayout
import com.google.gson.Gson
import io.realm.Realm
import io.realm.RealmChangeListener
import io.realm.RealmResults
import io.realm.Sort
import kotlinx.android.synthetic.main.activity_detail_pengajuan_dppt_2.*
import kotlinx.android.synthetic.main.toolbar_main.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.model.ResponDataSubjekDPPT
import mki.siojt2.model.ResponseDataListPemilik
import mki.siojt2.model.ResponseDataListProject
import mki.siojt2.model.localsave.PartyAdd
import mki.siojt2.ui.activity_daftar_kepemilikan_tanah_pertama.presenter.DetailPengajuanDPPTPresenter
import mki.siojt2.ui.activity_daftar_kepemilikan_tanah_pertama.recyleview_adapter.AdapterDaftarKepemilikanTanahPertama
import mki.siojt2.ui.activity_form_subjek.view.ActivityFormSubjek
import mki.siojt2.utils.realm.RealmController

class ActivityDaftarKepemilikanTanahPertama : BaseActivity(), DPPTView, AdapterDaftarKepemilikanTanahPertama.Callback {

    private lateinit var adapterlistSubjek: AdapterDaftarKepemilikanTanahPertama

    private lateinit var presenter: DetailPengajuanDPPTPresenter

    lateinit var mData: ResponseDataListProject

    private var mprojectAssignProjectId: Int? = 0

    lateinit var realm: Realm

    var listener: RealmChangeListener<RealmResults<PartyAdd>> = RealmChangeListener { results ->
        if (results.isLoaded) {
            // results is always up to date here
            // after a write to Realm from ANY thread!
            updateUi(results)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_pengajuan_dppt_2)

        /* init realm */
        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        //intent.getStringExtra("projectId")
        val historyData = intent?.extras?.getString("data_current_project")
        val toJson = Gson().fromJson(historyData , Array<String>::class.java).toList()

        mprojectAssignProjectId = toJson[0].toInt()

        //tvToolbarTitle.text = ""
        if (toolbarCustom != null) {
            setSupportActionBar(toolbarCustom)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarCustom.setNavigationOnClickListener {
            finish()
        }

        tvToolbarTitle.text = toJson[1]

        adapterlistSubjek = AdapterDaftarKepemilikanTanahPertama(this@ActivityDaftarKepemilikanTanahPertama, 0)
        adapterlistSubjek.setCallback(this)
        rvdaftarSubjek2.let {
            it.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@ActivityDaftarKepemilikanTanahPertama, LinearLayoutManager.VERTICAL, false)
            it.itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()
            it.adapter = adapterlistSubjek
        }

        /*btnadddataSubjek.setOnClickListener(object : OnSingleClickListener() {
            override fun onSingleClick(v: View) {
                val param = "['${toJson[0]}','']"
                val intent = ActivityFormSubjek.getStartIntent(this@ActivityDaftarKepemilikanTanahPertama)
                intent.putExtra("data_current_project2", param)
                startActivity(intent)
            }
        })*/
        btnadddataSubjek.setSafeOnClickListener{
            val param = "['${toJson[0]}','', 1, '']"
            val intent = ActivityFormSubjek.getStartIntent(this@ActivityDaftarKepemilikanTanahPertama)
            intent.putExtra("data_current_project2", param)
            startActivity(intent)
        }

        /* get data api */
        //getPresenter()?.getlistdataPemilik(aaaaa.projectAssignProjectId!!, Preference.accessToken)
        getDataKepemilikanTanah()
    }

    private fun getPresenter(): DetailPengajuanDPPTPresenter? {
        presenter = DetailPengajuanDPPTPresenter()
        presenter.onAttach(this)
        return presenter
    }

    override fun onsuccessgetlistdataPemilik(responseDataListPemilik: MutableList<ResponseDataListPemilik>) {
        //adapterlistSubjek.clear()
        //adapterlistSubjek.addItems(responseDataListPemilik)
    }

    override fun onRepoEmptyViewRetryClick() {

    }

    override fun onDismissBottomsheet() {

    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    private fun updateUi(data: List<PartyAdd>) {
        adapterlistSubjek.clear()
        adapterlistSubjek.addItems(data)
    }

    override fun onRestart() {
        super.onRestart()
        //getPresenter()?.getlistdataPemilik(aaaaa.projectAssignProjectId!!, Preference.accessToken)
        getDataKepemilikanTanah()
    }

    override fun onDestroy() {
        super.onDestroy()
        getPresenter()?.onDetach()
    }

    private fun getDataKepemilikanTanah() {
        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        val results = realm.where(PartyAdd::class.java)
                .equalTo("ProjectId", mprojectAssignProjectId)
                .sort("TempId", Sort.DESCENDING)
                .findAllAsync()
        results.addChangeListener(listener)

    }

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityDaftarKepemilikanTanahPertama::class.java)
        }
    }




}