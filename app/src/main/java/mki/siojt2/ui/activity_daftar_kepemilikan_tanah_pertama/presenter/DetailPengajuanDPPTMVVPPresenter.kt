package mki.siojt2.ui.activity_daftar_kepemilikan_tanah_pertama.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface DetailPengajuanDPPTMVVPPresenter : MVPPresenter {

    fun getlistdataPemilik(projectId : Int, accessToken: String)
}