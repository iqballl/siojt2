package mki.siojt2.ui.activity_daftar_kepemilikan_tanah_pertama.view

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.activity_detail_pengajuan_dppt.*
import kotlinx.android.synthetic.main.toolbar_main.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.fragment.detail_pengajuan.data_subjek.FragDataSubjek
import mki.siojt2.fragment.detail_pengajuan.summary.FragSummary

class ActivityDetailPengajuanDPPT : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_pengajuan_dppt)

        //tvToolbarTitle.text = ""
        if (toolbarCustom != null) {
            setSupportActionBar(toolbarCustom)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarCustom.setNavigationOnClickListener {
            finish()
        }

        setupViewPager(viewpagerdetailDPPT)
        tab_layoutdetailDPPT.setupWithViewPager(viewpagerdetailDPPT)

        tab_layoutdetailDPPT.setSelectedTabIndicatorColor(Color.parseColor("#229F53"))
        tab_layoutdetailDPPT.setTabTextColors(Color.parseColor("#97959B"), Color.parseColor("#229F53"))
        viewpagerdetailDPPT.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tab_layoutdetailDPPT))


        tab_layoutdetailDPPT.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewpagerdetailDPPT.setCurrentItem(tab.position, false)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }

    private fun setupViewPager(viewPager: androidx.viewpager.widget.ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(FragSummary.newInstance(), "Summary")
        adapter.addFragment(FragDataSubjek.newInstance(), "Data Subjek")
        //viewPager.setPageTransformer(true, FadePageTransformer())
        val limit = if (adapter.count > 1) adapter.count - 1 else 1

        viewPager.offscreenPageLimit = limit
        viewPager.currentItem = 0
        viewPager.adapter = adapter
    }

    internal inner class ViewPagerAdapter(manager: androidx.fragment.app.FragmentManager) : androidx.fragment.app.FragmentStatePagerAdapter(manager) {

        private val mFragmentList = arrayListOf<androidx.fragment.app.Fragment>()
        private val mFragmentTitleList = arrayListOf<String>()


        fun addFragment(fragment: androidx.fragment.app.Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)

        }

        override fun getCount(): Int {

            return mFragmentList.size
        }

        override fun getItem(position: Int): androidx.fragment.app.Fragment {
            return mFragmentList[position]
        }

        override fun getItemPosition(`object`: Any): Int {
            /*val index = mFragmentItems.indexOf(`object`)
            return if (index == -1)
                POSITION_NONE
            else
                index*/
            return androidx.viewpager.widget.PagerAdapter.POSITION_NONE
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityDetailPengajuanDPPT::class.java)
        }
    }

}