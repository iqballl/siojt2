package mki.siojt2.ui.activity_daftar_kepemilikan_tanah_pertama.recyleview_adapter

import android.annotation.SuppressLint
import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.gson.Gson
import io.realm.RealmResults
import kotlinx.android.synthetic.main.item_daftar_subjek_vertical.view.*
import mki.siojt2.R
import mki.siojt2.base.BaseViewHolder
import mki.siojt2.model.ResponseDataListPemilik
import mki.siojt2.model.localsave.PartyAdd
import mki.siojt2.ui.activity_detail_subjek.view.ActivityDetailSubjek
import mki.siojt2.utils.extension.OnSingleClickListener
import io.realm.RealmObject
import com.google.gson.FieldAttributes
import com.google.gson.ExclusionStrategy
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.item_empty_view.view.*
import mki.siojt2.ui.activity_form_subjek.view.ActivityFormSubjek
import mki.siojt2.utils.extension.SafeClickListener


class AdapterDaftarKepemilikanTanahPertama(context: Context, private val projectId: Int) : androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder>() {

    private val mContext: Context? = context
    val mDataset: MutableList<PartyAdd>? = ArrayList()
    var rowLayout = R.layout.item_daftar_subjek_vertical
    var emptyrowLayout = R.layout.item_empty_view
    var pos = 0

    private var mCallback: Callback? = null

    fun setCallback(callback: Callback) {
        mCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            VIEW_TYPE_NORMAL -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(rowLayout, parent, false))
            VIEW_TYPE_EMPTY -> EmptyViewHolder(
                    LayoutInflater.from(parent.context).inflate(R.layout.item_empty_view, parent, false))
            else -> EmptyViewHolder(LayoutInflater.from(parent.context).inflate(emptyrowLayout, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (mDataset != null && mDataset.isNotEmpty()) {
            VIEW_TYPE_NORMAL
        } else {
            VIEW_TYPE_EMPTY
        }
    }

    override fun getItemCount(): Int {
        return if (mDataset!!.isNotEmpty()) {
            mDataset.size
        } else {
            1
        }
    }

    fun clear() {
        this.mDataset!!.clear()
    }

    fun addItems(data: List<PartyAdd>) {
        this.mDataset!!.addAll(data)
        notifyDataSetChanged()
    }

    interface Callback {
        fun onRepoEmptyViewRetryClick()
        fun onDismissBottomsheet()
    }

    inner class ViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }

        @SuppressLint("SetTextI18n")
        override fun onBind(position: Int) {
            super.onBind(position)

            val dataList = mDataset!![position]

            val gson = GsonBuilder()
                    .setExclusionStrategies(object : ExclusionStrategy {
                        override fun shouldSkipField(f: FieldAttributes): Boolean {
                            return f.declaringClass == RealmObject::class.java
                        }

                        override fun shouldSkipClass(clazz: Class<*>): Boolean {
                            return false
                        }
                    })
                    .create()

            inflateData(dataList)

            val pos = pos + position + 1
            itemView.tvnamaSubjek.text = dataList.partyFullName
            itemView.tvalamat1Subjek.text = "Alamat : ${dataList.partyStreetName
                    ?: "-"} No.${dataList.partyNumber ?: "-"}"
            itemView.tvalamat2Subjek.text = "${dataList.partyDistrictName
                    ?: "-"} ${dataList.partyRegencyName ?: "-"} ${dataList.partyProvinceName
                    ?: "-"}"
            itemView.tvkodeposalamatSubjek.text = "Kode pos : ${dataList.partyPostalCode ?: "-"}"

            itemView.btnSelngkapnya.setSafeOnClickListener {
                val intent = ActivityDetailSubjek.getStartIntent(mContext!!)
                intent.putExtra("data_current_pemilik", dataList.tempId.toString())
                mContext.startActivity(intent)
                mCallback?.onDismissBottomsheet()
            }

//            !!important
//            itemView.btnDaftarObjek.setSafeOnClickListener {
//                val param = "['${dataList.tempId}','${dataList.projectId}','${dataList.partyFullName}']"
//                val intent = ActivityDaftarTanah.getStartIntent(mContext!!)
//                intent.putExtra("data_current_pemilik", param)
//                mContext.startActivity(intent)
//            }

        }

        private fun inflateData(dataList: PartyAdd) {
            //nama?.let { itemView.tvMatapelajaran.text = it }
//            itemView.tvContent1.text = dataList.namaJenis
//            itemView.tvContent2.text= dataList.keterangan
        }
    }

    inner class EmptyViewHolder(itemView: View) : BaseViewHolder(itemView) {
        override fun clear() {

        }
    }

    private fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
        val safeClickListener = SafeClickListener {
            onSafeClick(it)
        }
        setOnClickListener(safeClickListener)
    }

    companion object {
        const val VIEW_TYPE_EMPTY = 0
        const val VIEW_TYPE_NORMAL = 1
    }
}
