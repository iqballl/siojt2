package mki.siojt2.ui.activity_form_subjek_2

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import androidx.fragment.app.FragmentPagerAdapter
import android.util.Log
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_form_subjek.*
import kotlinx.android.synthetic.main.toolbar_form.*
import mki.siojt2.fragment.form_subjek.*
import mki.siojt2.model.data_form_subjek.DataFormSubjek1
import mki.siojt2.model.data_form_subjek.DataFormSubjek2
import mki.siojt2.model.data_form_subjek.DataFormSubjek3
import mki.siojt2.model.localsave.Session
import mki.siojt2.model.localsave.Subjek2
import mki.siojt2.persistence.Preference
import mki.siojt2.ui.activity_form_subjek.presenter.ActivityFormSubjekPresenter
import mki.siojt2.ui.activity_form_subjek.view.AcitivtyFormSubjekView
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.Utils

class ActivityFormSubjek2 : BaseActivity(), AcitivtyFormSubjekView, FormSubjek1.OnStepOneListener, FormSubjek5.OnStepFiveListener,
        FormSubjek3.OnStepThreeListener, FormSubjek4.OnStepFourListener {

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityFormSubjek2::class.java)
        }
    }

    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * [FragmentPagerAdapter] derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v13.app.FragmentStatePagerAdapter].
     */
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    private lateinit var activityformSubjekPresenter: ActivityFormSubjekPresenter

    var projectId: Int = 0
    var projectLandId: Int = 0

    private lateinit var realm: Realm
    private lateinit var session: Session

    /**
     * The [ViewPager] that will host the section contents.
     */
    //private var mViewPager: NonSwipeableViewPager? = null

    //private val stepperIndicator: StepperIndicator? = null

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_subjek)
        session = Session(this)

        val historyData = intent?.extras?.getString("data_current_tanah")
        val toJson = Gson().fromJson(historyData, Array<String>::class.java).toList()

        //val turnsType = object : TypeToken<ResponseDataListTanah>() {}.type
        //val dataconvert = Gson().fromJson<ResponseDataListTanah>(historyData, turnsType)

        projectId = toJson[1].toInt()
        projectLandId = toJson[0].toInt()


        /* set toolbar*/
        if (toolbarCustom != null) {
            setSupportActionBar(toolbarCustom)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarCustom.setNavigationOnClickListener {
            dialogToHandleExitFromCurrentPage()
        }

        if (session.status == "2") {
            tvFormToolbarTitle.text = "Edit data kepemilikan tanah ke-2"
        } else {
            tvFormToolbarTitle.text = "Tambah data kepemilikan tanah ke-2"
        }

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)
        mSectionsPagerAdapter!!.addFragment(FormSubjek1.newInstance("1", ""))
        mSectionsPagerAdapter!!.addFragment(FormSubjek5.newInstance("1", ""))
        mSectionsPagerAdapter!!.addFragment(FormSubjek3.newInstance("1", ""))

        // Set up the ViewPager with the sections adapter.
        viewpagerformSubjek.adapter = mSectionsPagerAdapter
        stepperIndicator.showLabels(true)
        stepperIndicator.setViewPager(viewpagerformSubjek)

        // or keep last page as "end page"
        stepperIndicator.setViewPager(viewpagerformSubjek, viewpagerformSubjek.adapter!!.count - 1) //}

        viewpagerformSubjek.setOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                mSectionsPagerAdapter!!.notifyDataSetChanged()
            }
        })

        realm = RealmController.with(this).realm
        realm.isAutoRefresh
    }


    private fun getPresenter(): ActivityFormSubjekPresenter? {
        activityformSubjekPresenter = ActivityFormSubjekPresenter()
        activityformSubjekPresenter.onAttach(this)
        return activityformSubjekPresenter
    }

    private fun dialogsuccessTransaksi(title_dialog: String) {
        Utils.deletedataform1(this@ActivityFormSubjek2)
        Utils.deletedataform3(this@ActivityFormSubjek2)
        Utils.deletedataform3(this@ActivityFormSubjek2)
        val dialogPopup = DialogSuccessAddSubjek.newInstance(title_dialog)
        dialogPopup.show(supportFragmentManager, "Dalog_popup")
    }

    private fun dialogToHandleExitFromCurrentPage() {
        val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> {
                    dialog.dismiss()
                    super.onBackPressed()
                    session.setIdAndStatus("", "", "")
                    finish()
                }

                DialogInterface.BUTTON_NEGATIVE -> {
                    dialog.dismiss()
                }
            }
        }

        MaterialAlertDialogBuilder(this, R.style.CustomAlertDialogMaterialTheme)
                .setTitle(getString(R.string.app_name))
                .setIcon(R.mipmap.ic_launcher)
                .setMessage("Data yang telah dibuat akan hilang, yakin keluar?")
                .setCancelable(false)
                .setPositiveButton("KELUAR", dialogClickListener)
                .setNegativeButton("BATAL", dialogClickListener)
                .show()
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    class SectionsPagerAdapter(fm: androidx.fragment.app.FragmentManager) : androidx.fragment.app.FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        private var mListFragment: ArrayList<Fragment> = ArrayList()

        fun addFragment(fragment: Fragment) {
            mListFragment.add(fragment)
        }

        override fun getCount(): Int {
            return mListFragment.size
        }

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            /*when (position) {
                0 -> return FormSubjek1.newInstance("1", "")
                1 -> return FormSubjek5.newInstance("", "")
                2 -> return FormSubjek3.newInstance("", "")
            }*/
            return mListFragment[position]
        }

        override fun getPageTitle(position: Int): CharSequence? {
            when (position) {
                0 -> return "Data Kepemilikan"
                1 -> return "Dokumen (kelengkapan)"
                2 -> return "Catatan"
            }
            return null
        }
    }

    override fun onsuccessaddPemilikTanah() {
        Utils.deletedataform1(this@ActivityFormSubjek2)
        Utils.deletedataform3(this@ActivityFormSubjek2)
        Utils.deletedataform3(this@ActivityFormSubjek2)
        val dialogPopup = DialogSuccessAddSubjek()
        dialogPopup.show(supportFragmentManager, "Dalog_popup")
    }

    override fun gotomainSuccess() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    override fun onBackPressed(fragment: Fragment) {
        mSectionsPagerAdapter!!.notifyDataSetChanged()
        when (fragment) {
            is FormSubjek5 -> viewpagerformSubjek.setCurrentItem(0, true)
            is FormSubjek3 -> viewpagerformSubjek.setCurrentItem(1, true)
        }
    }

    override fun onNextPressed(fragment: Fragment) {
        mSectionsPagerAdapter!!.notifyDataSetChanged()
        when (fragment) {
            is FormSubjek1 -> viewpagerformSubjek.setCurrentItem(1, true)
            is FormSubjek5 -> viewpagerformSubjek.setCurrentItem(2, true)
            is FormSubjek3 -> {
                val turnsType = object : TypeToken<MutableList<DataFormSubjek1>>() {}.type
                val dataconvert = Gson().fromJson<MutableList<DataFormSubjek1>>(Utils.getdataform1(this), turnsType)

                val turnsType2 = object : TypeToken<MutableList<DataFormSubjek2>>() {}.type
                val dataconvert2 = Gson().fromJson<MutableList<DataFormSubjek2>>(Utils.getdataform2(this), turnsType2)

                val turnsType3 = object : TypeToken<MutableList<DataFormSubjek3>>() {}.type
                val dataconvert3 = Gson().fromJson<MutableList<DataFormSubjek3>>(Utils.getdataform3(this), turnsType3)

                /*--------Edit Data Realm-------*/
                if (session.status == "2") {
                    onShowLoading()
                    realm.executeTransactionAsync({ inrealm ->
                        val subjek2 = inrealm.where(Subjek2::class.java).equalTo("SubjekIdTemp", session.id.toInt()).findFirst()
//                        subjek2!!.subjek2IdentitySourceId = dataconvert2[0].partyIdentitySourceId?.toInt()
//                        subjek2.subjek2IdentitySourceName = dataconvert2[0].partyIdentitySourceName.toString()
//                        if (dataconvert[0].partyOwnerType!!.isNotEmpty()) {
//                            subjek2.subjek2OwnerType = dataconvert[0].partyOwnerType!!.toInt()
//                        }
//                        subjek2.subjek2IdentitySourceOther = dataconvert2[0].partyIdentitySourceOther.toString()
                        subjek2!!.subjek2FullName = dataconvert[0].partyFullName.toString()
                        subjek2.subjek2BirthPlaceCode = dataconvert[0].partyBirthPlaceCode.toString()
                        subjek2.subjek2BirthPlaceName = dataconvert[0].partyBirthPlaceName.toString()
                        subjek2.subjek2BirthPlaceOther = dataconvert[0].partyBirthPlaceOther.toString()
                        subjek2.subjek2BirthDate = dataconvert[0].partyBirthDate.toString()
                        subjek2.subjek2OccupationId = dataconvert[0].partyOccupationId.toString()
                        subjek2.subjek2OccupationName = dataconvert[0].partyOccupationName.toString()
                        subjek2.subjek2OccupationOther = dataconvert[0].partyOccupationOther.toString()
                        subjek2.subjek2ProvinceCode = dataconvert[0].partyProvinceCode.toString()
                        subjek2.subjek2ProvinceName = dataconvert[0].partyProvinceName.toString()
                        subjek2.subjek2RegencyCode = dataconvert[0].partyRegencyCode.toString()
                        subjek2.subjek2RegencyName = dataconvert[0].partyRegencyName.toString()
                        subjek2.subjek2DistrictCode = dataconvert[0].partyDistrictCode.toString()
                        subjek2.subjek2DistrictName = dataconvert[0].partyDistrictName.toString()
                        subjek2.subjek2VillageId = dataconvert[0].partyVillageName.toString()
                        subjek2.subjek2VillageName = dataconvert[0].partyVillage.toString()
                        subjek2.subjek2RT = dataconvert[0].partyRT.toString()
                        subjek2.subjek2RW = dataconvert[0].partyRW.toString()
                        subjek2.subjek2Number = dataconvert[0].partyNumber.toString()
                        subjek2.subjek2Block = dataconvert[0].partyBlock.toString()
                        subjek2.subjek2StreetName = dataconvert[0].partyStreetName.toString()
                        subjek2.subjek2ComplexName = dataconvert[0].partyComplexName.toString()
                        subjek2.subjek2PostalCode = dataconvert[0].partyPostalCode.toString()
                        if (dataconvert2[0].projectPartyType!!.isNotEmpty()) {
                            subjek2.subjek2PenguasaanTanahId = dataconvert2[0].projectPartyType?.toInt()
                        }
                        if (dataconvert2[0].projectPartyTypeName!!.isNotEmpty()) {
                            subjek2.subjek2PenguasaanTanahName = dataconvert2[0].projectPartyTypeName
                        }
                        subjek2.subjek2NPWP = dataconvert2[0].partyNPWP.toString()
                        subjek2.subjek2YearStayBegin = dataconvert2[0].partyYearStayBegin.toString()
//                        subjek2.subjek2IdentityNumber = dataconvert2[0].partyIdentityNumber.toString()
//                        subjek2.livelihoodLiveId = dataconvert3[0].partyLivelihoodLivelihoodId.toString()
//                        subjek2.livelihoodLiveName = dataconvert3[0].partyLivelihoodName.toString()
//                        subjek2.livelihoodOther = dataconvert3[0].partyLivelihoodLivelihoodOther.toString()
//                        subjek2.livelihoodIncome = dataconvert3[0].partyLivelihoodIncome.toString()
                        subjek2.subjek2Status = 1
                        subjek2.subjek2CreateBy = Preference.auth.dataUser?.mobileUserId!!
                        subjek2.isCreate = 2
                    }, {
                        Log.d("pemilik2", "success update subjek2")
                        runOnUiThread {
                            onDismissLoading()
                            dialogsuccessTransaksi("""
                                Data
                                berhasil
                                diperbaharui
                            """.trimIndent())
                        }
                    }, {
                        Log.d("pemilik2", "failed update subjek2")
                    })
                } else {
                    /*-------- Save Into Realm -----*/
                    realm.executeTransactionAsync({ inrealm ->
                        val currentIdNum = inrealm.where(Subjek2::class.java).max("SubjekIdTemp")
                        val nextId: Int
                        nextId = if (currentIdNum == null) {
                            1
                        } else {
                            currentIdNum.toInt() + 1
                        }
                        val subjek2 = inrealm.createObject(Subjek2::class.java)
                        subjek2.subjekIdTemp = nextId
                        subjek2.subjekId = null
                        subjek2.projectId = projectId
                        subjek2.landIdTemp = projectLandId
                        subjek2.landId = null
//                        subjek2.subjek2IdentitySourceId = dataconvert2[0].partyIdentitySourceId?.toInt()
//                        subjek2.subjek2IdentitySourceName = dataconvert2[0].partyIdentitySourceName.toString()
//                        if (dataconvert[0].partyOwnerType!!.isNotEmpty()) {
//                            subjek2.subjek2OwnerType = dataconvert[0].partyOwnerType!!.toInt()
//                        }
//                        subjek2.subjek2IdentitySourceOther = dataconvert2[0].partyIdentitySourceOther.toString()
                        subjek2.subjek2FullName = dataconvert[0].partyFullName.toString()
                        subjek2.subjek2BirthPlaceCode = dataconvert[0].partyBirthPlaceCode.toString()
                        subjek2.subjek2BirthPlaceName = dataconvert[0].partyBirthPlaceName.toString()
                        subjek2.subjek2BirthPlaceOther = dataconvert[0].partyBirthPlaceOther.toString()
                        subjek2.subjek2BirthDate = dataconvert[0].partyBirthDate.toString()
                        subjek2.subjek2OccupationId = dataconvert[0].partyOccupationId.toString()
                        subjek2.subjek2OccupationName = dataconvert[0].partyOccupationName.toString()
                        subjek2.subjek2OccupationOther = dataconvert[0].partyOccupationOther.toString()
                        subjek2.subjek2ProvinceCode = dataconvert[0].partyProvinceCode.toString()
                        subjek2.subjek2ProvinceName = dataconvert[0].partyProvinceName.toString()
                        subjek2.subjek2RegencyCode = dataconvert[0].partyRegencyCode.toString()
                        subjek2.subjek2RegencyName = dataconvert[0].partyRegencyName.toString()
                        subjek2.subjek2DistrictCode = dataconvert[0].partyDistrictCode.toString()
                        subjek2.subjek2DistrictName = dataconvert[0].partyDistrictName.toString()
                        subjek2.subjek2VillageId = dataconvert[0].partyVillageName.toString()
                        subjek2.subjek2VillageName = dataconvert[0].partyVillage.toString()
                        subjek2.subjek2RT = dataconvert[0].partyRT.toString()
                        subjek2.subjek2RW = dataconvert[0].partyRW.toString()
                        subjek2.subjek2Number = dataconvert[0].partyNumber.toString()
                        subjek2.subjek2Block = dataconvert[0].partyBlock.toString()
                        subjek2.subjek2StreetName = dataconvert[0].partyStreetName.toString()
                        subjek2.subjek2ComplexName = dataconvert[0].partyComplexName.toString()
                        subjek2.subjek2PostalCode = dataconvert[0].partyPostalCode.toString()
                        if (dataconvert2[0].projectPartyType!!.isNotEmpty()) {
                            subjek2.subjek2PenguasaanTanahId = dataconvert2[0].projectPartyType?.toInt()
                        }
                        if (dataconvert2[0].projectPartyTypeName!!.isNotEmpty()) {
                            subjek2.subjek2PenguasaanTanahName = dataconvert2[0].projectPartyTypeName
                        }
                        subjek2.subjek2NPWP = dataconvert2[0].partyNPWP.toString()
                        subjek2.subjek2YearStayBegin = dataconvert2[0].partyYearStayBegin.toString()
//                        subjek2.subjek2IdentityNumber = dataconvert2[0].partyIdentityNumber.toString()
//                        subjek2.livelihoodLiveId = dataconvert3[0].partyLivelihoodLivelihoodId.toString()
//                        subjek2.livelihoodLiveName = dataconvert3[0].partyLivelihoodName.toString()
//                        subjek2.livelihoodOther = dataconvert3[0].partyLivelihoodLivelihoodOther.toString()
//                        subjek2.livelihoodIncome = dataconvert3[0].partyLivelihoodIncome.toString()
                        subjek2.subjek2Status = 1
                        subjek2.subjek2CreateBy = Preference.auth.dataUser?.mobileUserId!!
                    }, {
                        Log.d("pemilik1", "success add subjek1")
                        runOnUiThread {
                            onDismissLoading()
                            onsuccessaddPemilikTanah()
                        }
                    }, {
                        Log.d("pemilik1", "failed update subjek1")
                    })

                }
                /*------------------End of Saving--------------------*/
//                getPresenter()?.postaddPemilikTanah(
//                        Preference.auth.dataUser?.mobileUserId!!,
//                        projectId,
//                        projectLandId.toString(),
//                        "",
//                        dataconvert2[0].projectPartyType.toString(),
//                        dataconvert2[0].partyIdentitySourceId.toString(),
//                        dataconvert2[0].partyIdentitySourceOther.toString(),
//                        dataconvert[0].partyFullName.toString(), dataconvert[0].partyBirthPlaceCode.toString(),
//                        dataconvert[0].partyBirthPlaceOther.toString(), dataconvert[0].partyBirthDate.toString(),
//                        dataconvert[0].partyOccupationId.toString(), dataconvert[0].partyOccupationOther.toString(),
//                        dataconvert[0].partyProvinceCode.toString(), dataconvert[0].partyRegencyCode.toString(),
//                        dataconvert[0].partyDistrictCode.toString(), dataconvert[0].partyVillageName.toString(),
//                        dataconvert[0].partyRT.toString(), dataconvert[0].partyRW.toString(),
//                        dataconvert[0].partyNumber.toString(), dataconvert[0].partyBlock.toString(),
//                        dataconvert[0].partyStreetName.toString(), dataconvert[0].partyComplexName.toString(),
//                        dataconvert[0].partyPostalCode.toString(), dataconvert2[0].partyNPWP.toString(),
//                        dataconvert2[0].partyYearStayBegin.toString(), dataconvert2[0].partyIdentityNumber.toString(),
//                        dataconvert3[0].partyLivelihoodLivelihoodId, dataconvert3[0].partyLivelihoodLivelihoodOther,
//                        dataconvert3[0].partyLivelihoodIncome
//
//                )
            }
        }
    }

    override fun onBackPressed() {
        dialogToHandleExitFromCurrentPage()
    }

    override fun onDestroy() {
        session.setIdAndStatus("", "", "")
        super.onDestroy()
    }
}