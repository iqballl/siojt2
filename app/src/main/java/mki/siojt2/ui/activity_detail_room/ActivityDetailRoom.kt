package mki.siojt2.ui.activity_detail_room

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Html
import androidx.fragment.app.FragmentActivity
import android.util.Log
import com.google.gson.Gson
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_detail_room.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.model.localsave.Room
import mki.siojt2.utils.realm.RealmController

class ActivityDetailRoom : BaseActivity() {

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityDetailRoom::class.java)
        }
    }

    lateinit var realm: Realm


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_room)

        val historyData = intent?.extras?.getString("data_current_room")
        val toJson = Gson().fromJson(historyData, Array<String>::class.java).toList()

        /* set toolbar*/
        if (toolbarCustomDetailObjekBangunan != null) {
            setSupportActionBar(toolbarCustomDetailObjekBangunan)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarCustomDetailObjekBangunan.setNavigationOnClickListener {
            finish()
        }

        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        val results = realm.where(Room::class.java).equalTo("roomId",toJson[0].toInt()).findFirst()

        if(results != null){
            tvYear.text = results.year
            tvisiluasLantai1.text = Html.fromHtml("${results.upperRoom.toString()} (m<sup><small>2</small></sup>)")
            tvisiluasLantai2.text = Html.fromHtml("${results.lowerRoom.toString()} (m<sup><small>2</small></sup>)")
        }
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }
}