package mki.siojt2.ui.activity_form_add_objek_tanah.view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import android.util.Log
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.realm.Realm
import io.realm.RealmList
import kotlinx.android.synthetic.main.activity_form_add_objek.*
import kotlinx.android.synthetic.main.toolbar_form.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.fragment.form_objek_tanah.*
import mki.siojt2.fragment.form_subjek.DialogSuccessAddSubjek
import mki.siojt2.fragment.form_subjek.FormSubjek5
import mki.siojt2.model.UAVObjectsItem
import mki.siojt2.model.mapbox.CustomLatLng
import mki.siojt2.model.data_form_tanah.*
import mki.siojt2.model.local_save_image.DataImageLand
import mki.siojt2.model.localsave.ProjectLand
import mki.siojt2.model.localsave.Session
import mki.siojt2.persistence.Preference
import mki.siojt2.ui.activity_form_add_objek_tanah.presenter.ActicityFormAddObjekTanahPresenter
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.Utils

class ActicityFormAddObjekTanah : BaseActivity(),
        ActicityFormAddObjekTanahView,
//        FormObjekTanah1.OnStepOneListener,
        FormObjekTanah1MapBox.OnStepOneListener,
        FormObjekTanah2.OnStepTwoListener,
        FormObjekTanah3.OnStepThreeListener,
        FormObjekTanah4.OnStepFourListener,
        FormTanahPhoto.OnStepFiveListener,
        FormObjekTanah5.OnStepFiveListener {

    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * [FragmentPagerAdapter] derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v13.app.FragmentStatePagerAdapter].
     */
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    lateinit var acticityFormAddObjekTanahPresenter: ActicityFormAddObjekTanahPresenter
    private lateinit var realm: Realm
    private lateinit var session: Session
    private var mcurrentSession: mki.siojt2.model.Session? = null

    var historyData: String? = ""

    var getprojectID: Int = 0
    var getpartyID: Int = 0
    var nextId: Int = 0
    var selectedLinkedObjectUAVId: Int? = null
    open var isSimpleMode: Boolean = false

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActicityFormAddObjekTanah::class.java)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_add_objek)

        historyData = intent.getStringExtra("param_to_form_tanah")
        //val historyData = intent?.extras?.getString("param_to_form_tanah")

        //val turnsType = object : TypeToken<ResponseDataListPemilik>() {}.type
        //val dataconvert = Gson().fromJson<ResponseDataListPemilik>(historyData, turnsType)
        val toJson = Gson().fromJson(historyData, Array<String>::class.java).toList()
        //Log.e("toJson", toJson.toString())

        getprojectID = toJson[1].toInt()
        getpartyID = toJson[0].toInt()
        selectedLinkedObjectUAVId = getpartyID

        val mdataSession = intent?.extras?.getString("session_to_form_tanah")
        if (mdataSession != null) {
            val turnsType = object : TypeToken<mki.siojt2.model.Session>() {}.type
            mcurrentSession = Gson().fromJson<mki.siojt2.model.Session>(mdataSession, turnsType)
        }

        /* init realm */
        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        session = Session(this)
        Log.e("sessionload", session.id + " " + session.status)

        /*Log.d("mobil_2", historyData.toString())
        Log.d("mobil_2", dataconvert.toString())*/

        /* set toolbar*/
        if (toolbarCustom != null) {
            setSupportActionBar(toolbarCustom)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        if (mcurrentSession != null && mcurrentSession!!.id != null && mcurrentSession!!.statusPost == "2") {
            tvFormToolbarTitle.text = "Edit data tanah"

            val results = realm.where(ProjectLand::class.java).equalTo("LandIdTemp", session.id.toInt()).findFirst()
            if (results != null) {
                isSimpleMode = results.simpleMode
                Log.d("results", results.toString())
            } else {
                Log.e("data", "kosong")
            }
        } else {
            tvFormToolbarTitle.text = "Tambah data tanah"
        }

        toolbarCustom.setNavigationOnClickListener {
            dialogToHandleExitFromCurrentPage()
        }

        SetadapterformTanahWithAsncTask().execute()
    }

    private fun getPresenter(): ActicityFormAddObjekTanahPresenter? {
        acticityFormAddObjekTanahPresenter = ActicityFormAddObjekTanahPresenter()
        acticityFormAddObjekTanahPresenter.onAttach(this)
        return acticityFormAddObjekTanahPresenter
    }

    private fun dialogsuccessTransaksi(title_dialog: String) {
        Utils.deletedataformtanah1(this@ActicityFormAddObjekTanah)
        Utils.deletedataformtanah2(this@ActicityFormAddObjekTanah)
        Utils.deletedataformtanah3(this@ActicityFormAddObjekTanah)
        Utils.deletedataformtanah4(this@ActicityFormAddObjekTanah)
        Utils.deletedataformImage(this@ActicityFormAddObjekTanah)

        val dialogPopup = DialogSuccessAddSubjek.newInstance(title_dialog)
        dialogPopup.show(supportFragmentManager, "Dalog_popup")
    }

    override fun onsuccessPostLand() {
        /*if (session.status.isNotEmpty()) {
            session.setIdAndStatus("", "", "")
        }*/
        Utils.deletedataformtanah1(this@ActicityFormAddObjekTanah)
        Utils.deletedataformtanah2(this@ActicityFormAddObjekTanah)
        Utils.deletedataformtanah3(this@ActicityFormAddObjekTanah)
        Utils.deletedataformtanah4(this@ActicityFormAddObjekTanah)
        Utils.deletedataformImage(this@ActicityFormAddObjekTanah)

        val dialogPopup = DialogSuccessAddTanah()
        dialogPopup.show(supportFragmentManager, "Dalog_popup")
    }

    @SuppressLint("StaticFieldLeak")
    inner class SetadapterformTanahWithAsncTask : AsyncTask<Void, Void, Void>() {

        override fun onPreExecute() {
            super.onPreExecute()
            onShowLoading()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager, isSimpleMode)
            // Set up the ViewPager with the sections adapter.

//            mSectionsPagerAdapter!!.addFrag(FormObjekTanah1.newInstance("", ""))
            if(isSimpleMode){
                mSectionsPagerAdapter!!.addFrag(FormObjekTanah1MapBox.newInstance(isSimpleMode, "${getpartyID}", "$getprojectID"))
                mSectionsPagerAdapter!!.addFrag(FormObjekTanah2.newInstance("", ""))
                mSectionsPagerAdapter!!.addFrag(FormObjekTanah3.newInstance("", ""))
                mSectionsPagerAdapter!!.addFrag(FormObjekTanah5.newInstance("", ""))
                mSectionsPagerAdapter!!.addFrag(FormTanahPhoto.newInstance("", ""))
            }
            else{
                mSectionsPagerAdapter!!.addFrag(FormObjekTanah1MapBox.newInstance(isSimpleMode, "${getpartyID}", "$getprojectID"))
                mSectionsPagerAdapter!!.addFrag(FormObjekTanah2.newInstance("", ""))
                mSectionsPagerAdapter!!.addFrag(FormObjekTanah3.newInstance("", ""))
                mSectionsPagerAdapter!!.addFrag(FormObjekTanah4.newInstance("", ""))
                mSectionsPagerAdapter!!.addFrag(FormObjekTanah5.newInstance("", ""))
                mSectionsPagerAdapter!!.addFrag(FormTanahPhoto.newInstance("", ""))
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            viewpagerObjekTanah.adapter = mSectionsPagerAdapter
            stepperIndicatorObjekTanah.showLabels(true)
            stepperIndicatorObjekTanah.setViewPager(viewpagerObjekTanah)

            // or keep last page as "end page"
            stepperIndicatorObjekTanah.setViewPager(viewpagerObjekTanah, viewpagerObjekTanah.adapter!!.count - 1) //}
            stepperIndicatorObjekTanah.stepCount = viewpagerObjekTanah.adapter!!.count
            if(isSimpleMode){
                stepperIndicatorObjekTanah.setLabels(arrayOf("Lokasi tanah", "Bidang tanah", "Alah Hak Tanah", "Fiducia", "Catatan"))
            }
            else {
                stepperIndicatorObjekTanah.setLabels(arrayOf("Lokasi tanah", "Bidang tanah", "Alah Hak Tanah", "Lingkungan", "Fiducia", "Catatan"))
            }


            viewpagerObjekTanah.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
                override fun onPageSelected(position: Int) {
                    mSectionsPagerAdapter!!.notifyDataSetChanged()
                }
            })

            onDismissLoading()
        }

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    class SectionsPagerAdapter(fm: androidx.fragment.app.FragmentManager, val isSimpleMode: Boolean) : androidx.fragment.app.FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        private val mFragmentList = arrayListOf<Fragment>()

        private var mPrimaryItem: Fragment? = null

        fun addFrag(fragment: Fragment) {
            mFragmentList.add(fragment)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            /*when (position) {
                0 -> return FormObjekTanah1.newInstance("", "")
                1 -> return FormObjekTanah2.newInstance("", "")
                2 -> return FormObjekTanah3.newInstance("", "")
                3 -> return FormObjekTanah4.newInstance("", "")
                4 -> return FormTanahPhoto.newInstance("", "")
            }
            return null*/
            return mFragmentList[position]
        }

        override fun getPageTitle(position: Int): CharSequence? {
            Log.d("xxx3", "$position : $isSimpleMode")
            when (position) {
                0 -> return "Lokasi tanah"
                1 -> return "Bidang tanah"
                2 -> return "Alas Hak Tanah"
                3 -> return if(isSimpleMode) {
                    "Catatan"
                } else {
                    "Lingkungan"
                }
                4 -> return "Fiducia"
                5 -> return "Catatan"
            }
            return null
        }
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    override fun onBackPressed(fragment: Fragment) {
        mSectionsPagerAdapter!!.notifyDataSetChanged()
        when (fragment) {
            is FormObjekTanah2 -> viewpagerObjekTanah.setCurrentItem(0, true)
            is FormObjekTanah3 -> viewpagerObjekTanah.setCurrentItem(1, true)
            is FormObjekTanah4 -> viewpagerObjekTanah.setCurrentItem(2, true)
        }
    }

    override fun onNextPressed(fragment: Fragment) {
        mSectionsPagerAdapter!!.notifyDataSetChanged()
        when (fragment) {
//            is FormObjekTanah1 -> viewpagerObjekTanah.setCurrentItem(1, true)
            is FormObjekTanah1MapBox -> viewpagerObjekTanah.setCurrentItem(1, true)
            is FormObjekTanah2 -> viewpagerObjekTanah.setCurrentItem(2, true)
            is FormObjekTanah3 -> {
                viewpagerObjekTanah.setCurrentItem(3, true)
            }
            is FormObjekTanah4 -> {
                viewpagerObjekTanah.setCurrentItem(4, true)
            }
            is FormObjekTanah5 -> {
                Log.d("aiaia", "${if(isSimpleMode){4} else{5}}")
                viewpagerObjekTanah.setCurrentItem(if(isSimpleMode){4} else{5}, true)
            }
            is FormTanahPhoto -> {
                saveData()
            }
        }
    }

    private fun saveData(){
        Preference.deleteDataKoordinatMapBox()
        val turnsType = object : TypeToken<MutableList<DataFormTanah1>>() {}.type
        val dataconvert = Gson().fromJson<MutableList<DataFormTanah1>>(Utils.getdataformtanah1(this), turnsType)

        val turnsType2 = object : TypeToken<MutableList<DataFormTanah2>>() {}.type
        val dataconvert2 = Gson().fromJson<MutableList<DataFormTanah2>>(Utils.getdataformtanah2(this), turnsType2)

        val turnsType3 = object : TypeToken<MutableList<DataFormTanah3>>() {}.type
        val dataconvert3 = Gson().fromJson<MutableList<DataFormTanah3>>(Utils.getdataformtanah3(this), turnsType3)

        if (mcurrentSession != null && mcurrentSession!!.id != null && mcurrentSession!!.statusPost == "2") {
            realm.executeTransactionAsync({ inrealm ->
                val projectLand = inrealm.where(ProjectLand::class.java).equalTo("LandIdTemp", mcurrentSession!!.id).findFirst()
                nextId = projectLand?.landIdTemp!!
                projectLand.simpleMode = isSimpleMode
                //form1
                projectLand.landNumberList = dataconvert[0].projectLandNumberList!!
                projectLand.landProvinceCode = dataconvert[0].landProvinceCode!!
                projectLand.landProvinceName = dataconvert[0].landProvinceName!!
                projectLand.landRegencyCode = dataconvert[0].landRegencyCode!!
                projectLand.landRegencyName = dataconvert[0].landRegencyName!!
                projectLand.landDistrictCode = dataconvert[0].landDistrictCode!!
                projectLand.landDistrictName = dataconvert[0].landDistrictName!!
                projectLand.landVillageId = dataconvert[0].landVillageId!!
                projectLand.landVillageName = dataconvert[0].landVillageName!!
                projectLand.landBlok = dataconvert[0].landBlock!!
                projectLand.landNumber = dataconvert[0].landNumber!!
                projectLand.landComplekName = dataconvert[0].landComplexName!!
                projectLand.landStreet = dataconvert[0].landStreetName!!
                projectLand.landRT = dataconvert[0].landRT!!
                projectLand.landRW = dataconvert[0].landRW!!
                projectLand.landPostalCode = dataconvert[0].landPostalCode!!
                projectLand.landLatitude = dataconvert[0].projectLandLatitude!!
                projectLand.landLangitude = dataconvert[0].rojectLandLangitude!!
                projectLand.selectedLinkedObjectUAVId = selectedLinkedObjectUAVId
                projectLand.subjectId = dataconvert[0].subjectId
                projectLand.subjectName = dataconvert[0].subjectName

                val listPolygonPoint: MutableList<CustomLatLng> = arrayListOf()
                for (i in dataconvert[0].polygonPoint!!) {
                    listPolygonPoint.add(CustomLatLng(i.latitude, i.longitude))
                }

                val listPolygonPoint2 = inrealm.copyToRealmOrUpdate(listPolygonPoint)
                val listPolygonPoint3: RealmList<CustomLatLng> = RealmList()
                for (element in listPolygonPoint2) {
                    listPolygonPoint3.add(element)
                }
                projectLand.setLandPolygonPoint(listPolygonPoint3)

                //form2
                projectLand.landPositionToRoadId = dataconvert2[0].projectLandPositionToRoadId!!
                projectLand.landPositionToRoadName = dataconvert2[0].projectLandPositionToRoadName
                projectLand.landWideFrontRoad = dataconvert2[0].projectLandWideFrontRoad!!
                projectLand.landFrontage = dataconvert2[0].projectLandFrontage!!
                projectLand.landLength = dataconvert2[0].projectLandLength!!
                projectLand.landShapeId = dataconvert2[0].projectLandShapeId!!
                projectLand.landShapeName = dataconvert2[0].projectLandShapeName
                projectLand.landShapeNameOther = dataconvert2[0].projectLandShapeNameOther
                projectLand.landAreaCertificate = dataconvert2[0].projectLandAreaCertificate!!
                projectLand.landAreaAffected = dataconvert2[0].projectLandAreaAffected
                projectLand.landTypeLandTypeId = dataconvert2[0].projectLandTypeLandTypeId.toString()
                projectLand.landTypeLandTypeValue = dataconvert2[0].projectLandTypeLandTypeName.toString()
                projectLand.landTypeLandTypeNameOther = dataconvert2[0].projectLandTypeLandTypeNameOther.toString()
                projectLand.landTypographyId = dataconvert2[0].projectLandTopographyId.toString()
                projectLand.landTypographyName = dataconvert2[0].projectLandTopographyName.toString()
                projectLand.landHeightToRoad = dataconvert2[0].projectLandHeightToRoad!!
                projectLand.tanahSisa = dataconvert2[0].tanahSisa
                projectLand.livelihoodLiveId = dataconvert2[0].partyLivelihoodLivelihoodId.toString()
                projectLand.livelihoodLiveName = dataconvert2[0].partyLivelihoodName.toString()
                projectLand.livelihoodOther = dataconvert2[0].partyLivelihoodLivelihoodOther.toString()
                projectLand.livelihoodIncome = dataconvert2[0].partyLivelihoodIncome.toString()

                //form3
                projectLand.landOwnershipId = dataconvert3[0].projectLandOwnershipId!!
                projectLand.landOwnershipName = dataconvert3[0].projectLandOwnershipName!!
                projectLand.landOwnershipNameOther = dataconvert3[0].projectLandOwnershipNameOther
                projectLand.landOwnershipTypeSHM = dataconvert3[0].projectLandOwnershipNameTypeSHM
                projectLand.landDateOfIssue = dataconvert3[0].projectLandDateOfIssue!!
                projectLand.landRightsExpirationDate = dataconvert3[0].projectLandRightsExpirationDate!!
                projectLand.landNIB = dataconvert3[0].projectLandNIB!!
                projectLand.landCertificateNumber = dataconvert3[0].projectLandCertificateNumber!!
                projectLand.landUtilizationLandUtilizationId = dataconvert3[0].projectLandUtilizationLandUtilizationId.toString()
                projectLand.landUtilizationLandUtilizationName = dataconvert3[0].projectLandUtilizationLandUtilizationName.toString()
                projectLand.landUtilizationLandUtilizationNameOther = dataconvert3[0].projectLandUtilizationLandUtilizationNameOther.toString()
                projectLand.landZoningSpatialPlanId = dataconvert3[0].projectLandZoningSpatialPlanId!!
                projectLand.landZoningSpatialPlanName = dataconvert3[0].projectLandZoningSpatialPlanName!!
                projectLand.landOrientationWest = dataconvert3[0].projectLandOrientationWest!!
                projectLand.landOrientationEast = dataconvert3[0].projectLandOrientationEast!!
                projectLand.landOrientationNorth = dataconvert3[0].projectLandOrientationNorth!!
                projectLand.landOrientationSouth = dataconvert3[0].projectLandOrientationSouth!!
                projectLand.landOrientationNorthwest = dataconvert3[0].projectLandOrientationNorthwest!!
                projectLand.landOrientationNortheast = dataconvert3[0].projectLandOrientationNortheast!!
                projectLand.landOrientationSoutheast = dataconvert3[0].projectLandOrientationSoutheast!!
                projectLand.landOrientationSouthwest = dataconvert3[0].projectLandOrientationSouthwest!!
                projectLand.landAddInfoAddInfoId = dataconvert3[0].projectLandAddInfoAddInfoId.toString()
                projectLand.landAddInfoAddInfoName = dataconvert3[0].projectLandAddInfoAddInfoName.toString()
                projectLand.landAlasHakTanah = dataconvert3[0].landAlasHakTanah
                projectLand.dampak = dataconvert3[0].dampak


                if(!isSimpleMode){
                    val turnsType4 = object : TypeToken<MutableList<DataFormTanah4>>() {}.type
                    val dataconvert4 = Gson().fromJson<MutableList<DataFormTanah4>>(Utils.getdataformtanah4(this), turnsType4)

                    //form4
                    projectLand.landEaseToPropertyId = dataconvert4[0].projectLandEaseToPropertyId
                    projectLand.landEaseToPropertyName = dataconvert4[0].projectLandEaseToPropertyName
                    projectLand.landEaseToShopingCenterId = dataconvert4[0].projectLandEaseToShopingCenter
                    projectLand.landEaseToShopingCenterName = dataconvert4[0].projectLandEaseToShopingCenterName
                    projectLand.landEaseToEducationFacilitiesId = dataconvert4[0].projectLandEaseToEducationFacilitiesId
                    projectLand.landEaseToEducationFacilitiesName = dataconvert4[0].projectLandEaseToEducationFacilitiesName
                    projectLand.landEaseToTouristSitesId = dataconvert4[0].projectLandEaseToTouristSitesId
                    projectLand.landEaseToTouristSitesName = dataconvert4[0].projectLandEaseToTouristSitesName
                    projectLand.landEaseOfTransportationId = dataconvert4[0].projectLandEaseOfTransportationId
                    projectLand.landEaseOfTransportationName = dataconvert4[0].projectLandEaseOfTransportationName
                    projectLand.landSecurityAgainstCrimeId = dataconvert4[0].projectLandSecurityAgainstCrimeId
                    projectLand.landSecurityAgainstCrimeName = dataconvert4[0].projectLandSecurityAgainstCrimeName
                    projectLand.landSafetyAgainstFireHazardsId = dataconvert4[0].projectLandSafetyAgainstFireHazardsId
                    projectLand.landSafetyAgainstFireHazardsName = dataconvert4[0].projectLandSafetyAgainstFireHazardsName
                    projectLand.landSecurityAgainstNaturalDisastersId = dataconvert4[0].projectLandSecurityAgainstNaturalDisastersId
                    projectLand.landSecurityAgainstNaturalDisastersName = dataconvert4[0].projectLandSecurityAgainstNaturalDisastersName
                    projectLand.landAvailableElectricalGridId = dataconvert4[0].projectLandAvailableElectricalGridId
                    projectLand.landAvailableElectricalGridName = dataconvert4[0].projectLandAvailableElectricalGridName
                    projectLand.landAvailableCleanWaterSystemId = dataconvert4[0].projectLandAvailableCleanWaterSystemId
                    projectLand.landAvailableCleanWaterSystemName = dataconvert4[0].projectLandAvailableCleanWaterSystemName
                    projectLand.landAvailableTelephoneNetworkId = dataconvert4[0].projectLandAvailableTelephoneNetworkId
                    projectLand.landAvailableTelephoneNetworkName = dataconvert4[0].projectLandAvailableTelephoneNetworkName
                    projectLand.landAvailableGasPipelineNetworkId = dataconvert4[0].projectLandAvailableGasPipelineNetworkId
                    projectLand.landAvailableGasPipelineNetworkName = dataconvert4[0].projectLandAvailableGasPipelineNetworkName

                    projectLand.landSUTETPresenceId = dataconvert4[0].projectLandSUTETPresenceId
                    projectLand.landSUTETPresenceName = dataconvert4[0].projectLandSUTETPresenceName
                    projectLand.projectLandSUTETDistance = dataconvert4[0].projectLandSUTETDistance

                    projectLand.landCemeteryPresenceId = dataconvert4[0].projectLandCemeteryPresenceId
                    projectLand.landCemeteryPresenceName = dataconvert4[0].projectLandCemeteryPresenceName
                    projectLand.landCemeteryDistance = dataconvert4[0].projectLandCemeteryDistance

                    projectLand.landSkewerPresenceId = dataconvert4[0].projectLandSkewerPresenceId
                    projectLand.landSkewerPresenceName = dataconvert4[0].projectLandSkewerPresenceName
                }

                /* data images */
                val turnsType5 = object : TypeToken<DataFormTanahImage>() {}.type
                val dataImageLand = Gson().fromJson<DataFormTanahImage>(Utils.getdataformTanahImage(this), turnsType5)

                projectLand.note = dataImageLand.notes
                projectLand.hasOtherObject = dataImageLand.hasOtherObject ?: ""

                val resultdataImageLand = inrealm.where(DataImageLand::class.java).findAll()
                val dataimageLand = resultdataImageLand.where().equalTo("imageId", mcurrentSession!!.id).findAll()
                dataimageLand?.deleteAllFromRealm()

                val dataImagePlusId: MutableList<DataImageLand> = ArrayList()
                for (element in dataImageLand.dataImagePhoto!!.indices) {
                    if (dataImageLand.dataImagePhoto!![element].imagepathLand.isNotEmpty() && dataImageLand.dataImagePhoto!![element].imagenameLand.isNotEmpty()) {
                        val dataImage = DataImageLand()
                        dataImage.imageId = mcurrentSession!!.id
                        dataImage.imagepathLand = dataImageLand.dataImagePhoto!![element].imagepathLand
                        dataImage.imagenameLand = dataImageLand.dataImagePhoto!![element].imagenameLand
                        dataImagePlusId.add(dataImage)
                    }
                    //Log.d("hasil", dataPhoto.dataImagePhoto!![element].imagenameLand.toString())
                }
                if (dataImagePlusId.isNotEmpty()) {
                    val dataPhotoBangunan = inrealm.copyToRealmOrUpdate(dataImagePlusId)
                    val dataPhotoBangunan2: RealmList<DataImageLand> = RealmList()
                    for (element in dataPhotoBangunan) {
                        dataPhotoBangunan2.add(element)
                    }

                    projectLand.dataImageLands = dataPhotoBangunan2
                }
                projectLand.isCreate = 2

            }, {

                //bayu
                val results = realm.where(UAVObjectsItem::class.java)
                    .equalTo("id", selectedLinkedObjectUAVId)
                    .findFirst()

                Realm.getDefaultInstance().executeTransaction { realm ->
                    results?.selectedLandId = nextId
                    if(results != null){
                        realm.insertOrUpdate(results)
                    }
                    Log.d("land", "success update land objects")
                    runOnUiThread {
                        onDismissLoading()
                        runOnUiThread {
                            onDismissLoading()
                            dialogsuccessTransaksi("""
                                Data
                                berhasil
                                diperbaharui
                            """.trimIndent())
                        }
                    }
                }
            }, {
                Log.d("land", "failed update land")
                Log.d("land", it.localizedMessage)
                runOnUiThread {
                    onDismissLoading()
                }
            })
        } else {
            //----------- Save Into Realm -----------
            onShowLoading()
            realm.executeTransactionAsync({ inRealm ->
                val currentIdNum = inRealm.where(ProjectLand::class.java).max("LandIdTemp")
                nextId = if (currentIdNum == null) {
                    1
                } else {
                    currentIdNum.toInt() + 1
                }
                val projectLand = inRealm.createObject(ProjectLand::class.java, nextId)
                projectLand.partyCreateBy = Preference.auth.dataUser!!.mobileUserId
                projectLand.simpleMode = isSimpleMode
                //projectLand.landIdTemp = nextId
                projectLand.landId = null
                projectLand.partyIdTemp = getpartyID
                projectLand.areaId = getpartyID
                projectLand.partyId = null
                projectLand.projectId = getprojectID
                projectLand.selectedLinkedObjectUAVId = selectedLinkedObjectUAVId
                projectLand.subjectId = dataconvert[0].subjectId
                projectLand.subjectName = dataconvert[0].subjectName

                //form1
                projectLand.landNumberList = dataconvert[0].projectLandNumberList!!
                projectLand.landProvinceCode = dataconvert[0].landProvinceCode!!
                projectLand.landProvinceName = dataconvert[0].landProvinceName!!
                projectLand.landRegencyCode = dataconvert[0].landRegencyCode!!
                projectLand.landRegencyName = dataconvert[0].landRegencyName!!
                projectLand.landDistrictCode = dataconvert[0].landDistrictCode!!
                projectLand.landDistrictName = dataconvert[0].landDistrictName!!
                projectLand.landVillageId = dataconvert[0].landVillageId!!
                projectLand.landVillageName = dataconvert[0].landVillageName!!
                projectLand.landBlok = dataconvert[0].landBlock!!
                projectLand.landNumber = dataconvert[0].landNumber!!
                projectLand.landComplekName = dataconvert[0].landComplexName!!
                projectLand.landStreet = dataconvert[0].landStreetName!!
                projectLand.landRT = dataconvert[0].landRT!!
                projectLand.landRW = dataconvert[0].landRW!!
                projectLand.landPostalCode = dataconvert[0].landPostalCode!!
                projectLand.landLatitude = dataconvert[0].projectLandLatitude!!
                projectLand.landLangitude = dataconvert[0].rojectLandLangitude!!

                val listPolygonPoint: MutableList<CustomLatLng> = arrayListOf()
                for (i in dataconvert[0].polygonPoint!!) {
                    listPolygonPoint.add(CustomLatLng(i.latitude, i.longitude))
                }

                val listPolygonPoint2 = inRealm.copyToRealmOrUpdate(listPolygonPoint)
                val listPolygonPoint3: RealmList<CustomLatLng> = RealmList()
                for (element in listPolygonPoint2) {
                    listPolygonPoint3.add(element)
                }
                projectLand.setLandPolygonPoint(listPolygonPoint3)

                //form2
                projectLand.landPositionToRoadId = dataconvert2[0].projectLandPositionToRoadId!!
                projectLand.landPositionToRoadName = dataconvert2[0].projectLandPositionToRoadName
                projectLand.landWideFrontRoad = dataconvert2[0].projectLandWideFrontRoad!!
                projectLand.landFrontage = dataconvert2[0].projectLandFrontage!!
                projectLand.landLength = dataconvert2[0].projectLandLength!!
                projectLand.landShapeId = dataconvert2[0].projectLandShapeId!!
                projectLand.landShapeName = dataconvert2[0].projectLandShapeName
                projectLand.landShapeNameOther = dataconvert2[0].projectLandShapeNameOther
                projectLand.landAreaCertificate = dataconvert2[0].projectLandAreaCertificate!!
                projectLand.landAreaAffected = dataconvert2[0].projectLandAreaAffected
                projectLand.landTypeLandTypeId = dataconvert2[0].projectLandTypeLandTypeId.toString()
                projectLand.landTypeLandTypeValue = dataconvert2[0].projectLandTypeLandTypeName.toString()
                projectLand.landTypeLandTypeNameOther = dataconvert2[0].projectLandTypeLandTypeNameOther.toString()
                projectLand.landTypographyId = dataconvert2[0].projectLandTopographyId.toString()
                projectLand.landTypographyName = dataconvert2[0].projectLandTopographyName.toString()
                projectLand.landHeightToRoad = dataconvert2[0].projectLandHeightToRoad!!
                projectLand.livelihoodLiveId = dataconvert2[0].partyLivelihoodLivelihoodId.toString()
                projectLand.livelihoodLiveName = dataconvert2[0].partyLivelihoodName.toString()
                projectLand.livelihoodOther = dataconvert2[0].partyLivelihoodLivelihoodOther.toString()
                projectLand.livelihoodIncome = dataconvert2[0].partyLivelihoodIncome.toString()
                projectLand.tanahSisa = dataconvert2[0].tanahSisa

                //form3
                projectLand.landOwnershipId = dataconvert3[0].projectLandOwnershipId!!
                projectLand.landOwnershipName = dataconvert3[0].projectLandOwnershipName!!
                projectLand.landOwnershipNameOther = dataconvert3[0].projectLandOwnershipNameOther
                projectLand.landOwnershipTypeSHM = dataconvert3[0].projectLandOwnershipNameTypeSHM
                projectLand.landDateOfIssue = dataconvert3[0].projectLandDateOfIssue!!
                projectLand.landRightsExpirationDate = dataconvert3[0].projectLandRightsExpirationDate!!
                projectLand.landNIB = dataconvert3[0].projectLandNIB!!
                projectLand.landCertificateNumber = dataconvert3[0].projectLandCertificateNumber!!
                projectLand.landUtilizationLandUtilizationId = dataconvert3[0].projectLandUtilizationLandUtilizationId.toString()
                projectLand.landUtilizationLandUtilizationName = dataconvert3[0].projectLandUtilizationLandUtilizationName.toString()
                projectLand.landUtilizationLandUtilizationNameOther = dataconvert3[0].projectLandUtilizationLandUtilizationNameOther.toString()
                projectLand.landZoningSpatialPlanId = dataconvert3[0].projectLandZoningSpatialPlanId!!
                projectLand.landZoningSpatialPlanName = dataconvert3[0].projectLandZoningSpatialPlanName!!
                projectLand.landOrientationWest = dataconvert3[0].projectLandOrientationWest!!
                projectLand.landOrientationEast = dataconvert3[0].projectLandOrientationEast!!
                projectLand.landOrientationNorth = dataconvert3[0].projectLandOrientationNorth!!
                projectLand.landOrientationSouth = dataconvert3[0].projectLandOrientationSouth!!
                projectLand.landOrientationNorthwest = dataconvert3[0].projectLandOrientationNorthwest!!
                projectLand.landOrientationNortheast = dataconvert3[0].projectLandOrientationNortheast!!
                projectLand.landOrientationSoutheast = dataconvert3[0].projectLandOrientationSoutheast!!
                projectLand.landOrientationSouthwest = dataconvert3[0].projectLandOrientationSouthwest!!
                projectLand.landAddInfoAddInfoId = dataconvert3[0].projectLandAddInfoAddInfoId.toString()
                projectLand.landAddInfoAddInfoName = dataconvert3[0].projectLandAddInfoAddInfoName.toString()
                projectLand.landAlasHakTanah = dataconvert3[0].landAlasHakTanah
                projectLand.dampak = dataconvert3[0].dampak

                if(!isSimpleMode){
                    val turnsType4 = object : TypeToken<MutableList<DataFormTanah4>>() {}.type
                    val dataconvert4 = Gson().fromJson<MutableList<DataFormTanah4>>(Utils.getdataformtanah4(this), turnsType4)

                    /* data images */
                    val turnsType5 = object : TypeToken<DataFormTanahImage>() {}.type
                    val dataImageLand = Gson().fromJson<DataFormTanahImage>(Utils.getdataformTanahImage(this), turnsType5)

                    //form4
                    projectLand.landEaseToPropertyId = dataconvert4[0].projectLandEaseToPropertyId
                    projectLand.landEaseToPropertyName = dataconvert4[0].projectLandEaseToPropertyName
                    projectLand.landEaseToShopingCenterId = dataconvert4[0].projectLandEaseToShopingCenter
                    projectLand.landEaseToShopingCenterName = dataconvert4[0].projectLandEaseToShopingCenterName
                    projectLand.landEaseToEducationFacilitiesId = dataconvert4[0].projectLandEaseToEducationFacilitiesId
                    projectLand.landEaseToEducationFacilitiesName = dataconvert4[0].projectLandEaseToEducationFacilitiesName
                    projectLand.landEaseToTouristSitesId = dataconvert4[0].projectLandEaseToTouristSitesId
                    projectLand.landEaseToTouristSitesName = dataconvert4[0].projectLandEaseToTouristSitesName
                    projectLand.landEaseOfTransportationId = dataconvert4[0].projectLandEaseOfTransportationId
                    projectLand.landEaseOfTransportationName = dataconvert4[0].projectLandEaseOfTransportationName
                    projectLand.landSecurityAgainstCrimeId = dataconvert4[0].projectLandSecurityAgainstCrimeId
                    projectLand.landSecurityAgainstCrimeName = dataconvert4[0].projectLandSecurityAgainstCrimeName
                    projectLand.landSafetyAgainstFireHazardsId = dataconvert4[0].projectLandSafetyAgainstFireHazardsId
                    projectLand.landSafetyAgainstFireHazardsName = dataconvert4[0].projectLandSafetyAgainstFireHazardsName
                    projectLand.landSecurityAgainstNaturalDisastersId = dataconvert4[0].projectLandSecurityAgainstNaturalDisastersId
                    projectLand.landSecurityAgainstNaturalDisastersName = dataconvert4[0].projectLandSecurityAgainstNaturalDisastersName
                    projectLand.landAvailableElectricalGridId = dataconvert4[0].projectLandAvailableElectricalGridId
                    projectLand.landAvailableElectricalGridName = dataconvert4[0].projectLandAvailableElectricalGridName
                    projectLand.landAvailableCleanWaterSystemId = dataconvert4[0].projectLandAvailableCleanWaterSystemId
                    projectLand.landAvailableCleanWaterSystemName = dataconvert4[0].projectLandAvailableCleanWaterSystemName
                    projectLand.landAvailableTelephoneNetworkId = dataconvert4[0].projectLandAvailableTelephoneNetworkId
                    projectLand.landAvailableTelephoneNetworkName = dataconvert4[0].projectLandAvailableTelephoneNetworkName
                    projectLand.landAvailableGasPipelineNetworkId = dataconvert4[0].projectLandAvailableGasPipelineNetworkId
                    projectLand.landAvailableGasPipelineNetworkName = dataconvert4[0].projectLandAvailableGasPipelineNetworkName

                    projectLand.landSUTETPresenceId = dataconvert4[0].projectLandSUTETPresenceId
                    projectLand.landSUTETPresenceName = dataconvert4[0].projectLandSUTETPresenceName
                    projectLand.projectLandSUTETDistance = dataconvert4[0].projectLandSUTETDistance

                    projectLand.landCemeteryPresenceId = dataconvert4[0].projectLandCemeteryPresenceId
                    projectLand.landCemeteryPresenceName = dataconvert4[0].projectLandCemeteryPresenceName
                    projectLand.landCemeteryDistance = dataconvert4[0].projectLandCemeteryDistance

                    projectLand.landSkewerPresenceId = dataconvert4[0].projectLandSkewerPresenceId
                    projectLand.landSkewerPresenceName = dataconvert4[0].projectLandSkewerPresenceName

                    projectLand.note = dataImageLand.notes
                    projectLand.hasOtherObject = dataImageLand.hasOtherObject ?: ""

                    val dataImagePlusId: MutableList<DataImageLand> = ArrayList()
                    for (element in dataImageLand.dataImagePhoto!!.indices) {
                        if (dataImageLand.dataImagePhoto!![element].imagepathLand.isNotEmpty() && dataImageLand.dataImagePhoto!![element].imagenameLand.isNotEmpty()) {
                            val dataImage = DataImageLand()
                            dataImage.imageId = nextId
                            dataImage.imagepathLand = dataImageLand.dataImagePhoto!![element].imagepathLand
                            dataImage.imagenameLand = dataImageLand.dataImagePhoto!![element].imagenameLand
                            dataImagePlusId.add(dataImage)
                        }
                    }

                    if (dataImagePlusId.isNotEmpty()) {
                        val dataPhotoBangunan = inRealm.copyToRealmOrUpdate(dataImagePlusId)
                        val dataPhotoBangunan2: RealmList<DataImageLand> = RealmList()
                        for (element in dataPhotoBangunan) {
                            dataPhotoBangunan2.add(element)
                        }

                        projectLand.dataImageLands = dataPhotoBangunan2
                    }
                }
                projectLand.isCreate = 1
            }, {

                //bayu
                val results = realm.where(UAVObjectsItem::class.java)
                    .equalTo("id", selectedLinkedObjectUAVId)
                    .findFirst()

                Realm.getDefaultInstance().executeTransaction { realm ->
                    results?.selectedLandId = nextId
                    if(results != null){
                        realm.insertOrUpdate(results)
                    }
                    runOnUiThread {
                        onDismissLoading()
                        onsuccessPostLand()
                    }
                }
            }, null)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 333) {
                selectedLinkedObjectUAVId = data?.getIntExtra("objectId",0)
            }
        }
    }

    override fun onBackPressed() {
        dialogToHandleExitFromCurrentPage()
    }

    private fun dialogToHandleExitFromCurrentPage() {
        /* val builder = androidx.appcompat.app.AlertDialog.Builder(this)
            builder.setTitle("SIOJT")
             .setMessage("Data yang telah dibuat akan hilang, yakin keluar?")
             .setPositiveButton("KELUAR", dialogClickListener)
             .setNegativeButton("BATAL", dialogClickListener)
             .setIcon(ContextCompat.getDrawable(this@ActicityFormAddObjekTanah, R.drawable.ic_logo_splash)!!)
             .show()*/


        val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> {
                    dialog.dismiss()
                    super.onBackPressed()
                    Preference.deleteDataKoordinatMapBox()
                    Utils.deletedataImageCanvas(this)
                    session.setIdAndStatus("", "", "")
                    finish()
                }

                DialogInterface.BUTTON_NEGATIVE -> {
                    dialog.dismiss()
                }
            }
        }

        MaterialAlertDialogBuilder(this, R.style.CustomAlertDialogMaterialTheme)
                .setTitle(getString(R.string.app_name))
                .setIcon(R.mipmap.ic_launcher)
                .setMessage("Data yang telah dibuat akan hilang, yakin keluar?")
                .setCancelable(false)
                .setPositiveButton("KELUAR", dialogClickListener)
                .setNegativeButton("BATAL", dialogClickListener)
                .setIcon(ContextCompat.getDrawable(this@ActicityFormAddObjekTanah, R.drawable.ic_logo_splash)!!)
                .show()
    }

    open fun setMode(isSimpleMode: Boolean){
        this.isSimpleMode = isSimpleMode
        SetadapterformTanahWithAsncTask().execute()
    }
}