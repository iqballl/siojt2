package mki.siojt2.ui.activity_form_add_objek_tanah.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface ActicityFormAddObjekTanahMVPPresenter: MVPPresenter{
    fun postLand(userId: Int, projectId: Int, partyId: Int, projectLandNumberList: String,
                 projectLandProvinceCode: String, projectLandRegencyCode: String,
                 projectLandDistrictCode: String, projectLandVillageName: String,
                 projectLandRT: String, projectLandRW: String,
                 projectLandNumber: Int, projectLandBlock: String,
                 projectLandStreetName: String, projectLandComplexName: String,
                 projectLandPostalCode: String, projectLandPositionToRoadId: Int,
                 projectLandWideFrontRoad: Float, projectLandFrontage: Float,
                 projectLandLength: Float, projectLandShapeId: Int,

                 projectLandTopographyId: Int, projectLandHeightToRoad: String,
                 projectLandZoningSpatialPlanId: Int, projectLandAvailableElectricalGrid: Int,
                 projectLandAvailableCleanWaterSystem: Int, projectLandAvailableTelephoneNetwork: Int,
                 projectLandAvailableGasPipelineNetwork: Int, projectLandSUTETPresence: Int,
                 projectLandSUTETDistance: Float, projectLandSkewerPresence: Int,
                 projectLandCemeteryPresence: Int, projectLandCemeteryDistance: Float,

                 projectLandOrientationWest: String, projectLandOrientationEast: String,
                 projectLandOrientationNorth: String, projectLandOrientationSouth: String,
                 projectLandOrientationNorthwest: String, projectLandOrientationSoutheast: String,
                 projectLandOrientationNortheast: String, projectLandOrientationSouthwest: String,
                 projectLandEaseToProperty: Int, projectLandEaseToShopingCenter: Int,
                 projectLandEaseToEducationFacilities: Int, projectLandEaseToTouristSites: Int,
                 projectLandEaseOfTransportation: Int, projectLandSecurityAgainstCrime: Int,
                 projectLandSafetyAgainstFireHazards: Int, projectLandSecurityAgainstNaturalDisasters: Int,

                 projectLandLatitude: String, projectLandLangitude: String,
                 projectLandOwnershipId: Int, projectLandNIB: String,
                 projectLandCertificateNumber: String, projectLandAreaCertificate: String,
                 projectLandAreaAffected: String, projectLandDateOfIssue: String,
                 projectLandRightsExpirationDate: String, projectLandAddInfoAddInfoId: ArrayList<String>?,
                 projectLandTypeLandTypeId: ArrayList<String>?, projectLandUtilizationLandUtilizationId: ArrayList<String>?,
                 accessToken: String)
}