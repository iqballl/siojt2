package mki.siojt2.ui.activity_form_add_objek_tanah.presenter

import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.ui.activity_form_add_objek_tanah.view.ActicityFormAddObjekTanahView
import mki.siojt2.utils.ErrorHandler
import mki.siojt2.utils.rxJava.RxUtils

class ActicityFormAddObjekTanahPresenter : BasePresenter(), ActicityFormAddObjekTanahMVPPresenter {

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun postLand(userId: Int, projectId: Int, partyId: Int, projectLandNumberList: String, projectLandProvinceCode: String, projectLandRegencyCode: String, projectLandDistrictCode: String, projectLandVillageName: String, projectLandRT: String, projectLandRW: String, projectLandNumber: Int, projectLandBlock: String, projectLandStreetName: String, projectLandComplexName: String, projectLandPostalCode: String, projectLandPositionToRoadId: Int, projectLandWideFrontRoad: Float, projectLandFrontage: Float, projectLandLength: Float, projectLandShapeId: Int, projectLandTopographyId: Int, projectLandHeightToRoad: String, projectLandZoningSpatialPlanId: Int, projectLandAvailableElectricalGrid: Int, projectLandAvailableCleanWaterSystem: Int, projectLandAvailableTelephoneNetwork: Int, projectLandAvailableGasPipelineNetwork: Int, projectLandSUTETPresence: Int, projectLandSUTETDistance: Float, projectLandSkewerPresence: Int, projectLandCemeteryPresence: Int, projectLandCemeteryDistance: Float, projectLandOrientationWest: String, projectLandOrientationEast: String, projectLandOrientationNorth: String, projectLandOrientationSouth: String, projectLandOrientationNorthwest: String, projectLandOrientationSoutheast: String, projectLandOrientationNortheast: String, projectLandOrientationSouthwest: String, projectLandEaseToProperty: Int, projectLandEaseToShopingCenter: Int, projectLandEaseToEducationFacilities: Int, projectLandEaseToTouristSites: Int, projectLandEaseOfTransportation: Int, projectLandSecurityAgainstCrime: Int, projectLandSafetyAgainstFireHazards: Int, projectLandSecurityAgainstNaturalDisasters: Int, projectLandLatitude: String, projectLandLangitude: String, projectLandOwnershipId: Int, projectLandNIB: String, projectLandCertificateNumber: String, projectLandAreaCertificate: String, projectLandAreaAffected: String, projectLandDateOfIssue: String, projectLandRightsExpirationDate: String, projectLandAddInfoAddInfoId: ArrayList<String>?, projectLandTypeLandTypeId: ArrayList<String>?, projectLandUtilizationLandUtilizationId: ArrayList<String>?, accessToken: String) {
        view().onShowLoading()
        compositeDisposable.add(
                dataManager.postaddLand(userId, projectId, partyId, projectLandNumberList,
                        projectLandProvinceCode, projectLandRegencyCode,
                        projectLandDistrictCode, projectLandVillageName,
                        projectLandRT, projectLandRW,
                        projectLandNumber, projectLandBlock,
                        projectLandStreetName, projectLandComplexName,
                        projectLandPostalCode, projectLandPositionToRoadId,
                        projectLandWideFrontRoad, projectLandFrontage,
                        projectLandLength, projectLandShapeId,

                        projectLandTopographyId, projectLandHeightToRoad,
                        projectLandZoningSpatialPlanId, projectLandAvailableElectricalGrid,
                        projectLandAvailableCleanWaterSystem, projectLandAvailableTelephoneNetwork,
                        projectLandAvailableGasPipelineNetwork, projectLandSUTETPresence,
                        projectLandSUTETDistance, projectLandSkewerPresence,
                        projectLandCemeteryPresence, projectLandCemeteryDistance,

                        projectLandOrientationWest, projectLandOrientationEast,
                        projectLandOrientationNorth, projectLandOrientationSouth,
                        projectLandOrientationNorthwest, projectLandOrientationSoutheast,
                        projectLandOrientationNortheast, projectLandOrientationSouthwest,
                        projectLandEaseToProperty, projectLandEaseToShopingCenter,
                        projectLandEaseToEducationFacilities, projectLandEaseToTouristSites,
                        projectLandEaseOfTransportation, projectLandSecurityAgainstCrime,
                        projectLandSafetyAgainstFireHazards, projectLandSecurityAgainstNaturalDisasters,

                        projectLandLatitude, projectLandLangitude,
                        projectLandOwnershipId, projectLandNIB,
                        projectLandCertificateNumber, projectLandAreaCertificate,
                        projectLandAreaAffected, projectLandDateOfIssue,
                        projectLandRightsExpirationDate, projectLandAddInfoAddInfoId,
                        projectLandTypeLandTypeId, projectLandUtilizationLandUtilizationId,
                        accessToken)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessPostLand()
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }


    private fun view(): ActicityFormAddObjekTanahView {
        return getView() as ActicityFormAddObjekTanahView
    }
}