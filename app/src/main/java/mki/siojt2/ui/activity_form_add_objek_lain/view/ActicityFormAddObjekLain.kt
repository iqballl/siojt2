package mki.siojt2.ui.activity_form_add_objek_lain.view

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.*
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import cn.pedant.SweetAlert.SweetAlertDialog
import com.weiwangcn.betterspinner.library.BetterSpinner
import com.whiteelephant.monthpicker.MonthPickerDialog
import io.realm.Realm
import io.realm.RealmList
import kotlinx.android.synthetic.main.activity_form_add_sarana_lain.*
import kotlinx.android.synthetic.main.dialog_confirm_delete.view.*
import kotlinx.android.synthetic.main.toolbar_form.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.fragment.form_subjek.FormSubjek2
import mki.siojt2.model.DataMultiplePagerKeliling
import mki.siojt2.model.ResponseDataListPemilik
import mki.siojt2.model.local_save_image.DataImageFacilities
import mki.siojt2.model.localsave.SaranaLain
import mki.siojt2.model.localsave.Session
import mki.siojt2.model.localsave.TempPemilikObjek
import mki.siojt2.model.master_data_form_tanah.ResponseDataListExistOrNo
import mki.siojt2.model.master_data_sarana_lain.*
import mki.siojt2.model.second_development.*
import mki.siojt2.ui.activity_form_add_objek_lain.DialogSuccessAddMesin
import mki.siojt2.ui.activity_form_add_objek_lain.adapter_recyleview.AdapterPagerKeliling
import mki.siojt2.ui.activity_form_add_objek_lain.adapter_recyleview.AdapterSumberAir
import mki.siojt2.ui.activity_form_add_objek_lain.adapter_recyleview.AdapterTakePictureFacilities
import mki.siojt2.ui.activity_form_add_objek_lain.presenter.FormAddObjekLainPresenter
import mki.siojt2.ui.activity_image_picker.ImagePickerActivity
import mki.siojt2.utils.realm.RealmController
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class ActicityFormAddObjekLain : BaseActivity(), FormAddObjekLainView, AdapterTakePictureFacilities.OnItemImageClickListener {

    companion object {
        private val REQUEST_IMAGE = 100
        internal const val TAG = "ActicityAddObjekLain"
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActicityFormAddObjekLain::class.java)
        }
    }

    private var data: ArrayList<Int> = ArrayList()

    private var projectLandId: Int? = 0
    private var partyTypdeId: String? = ""

    lateinit var formAddObjekLainPresenter: FormAddObjekLainPresenter

    /* this */
    private var sendidJenisFasilitas: ArrayList<Int>? = ArrayList()
    private var sendJenisFasilitasName: String? = null
    private var sendKapasitasSaranaPelengkap: ArrayList<String>? = ArrayList()
    private var sendKapasitasValueSaranaPelengkap: ArrayList<String>? = ArrayList()

    private var sendKapasitasValueSatuanSaranaPelengkap: ArrayList<String>? = ArrayList()

    private var sendvalueSumberAir: String? = ""
    private var sendvalueFacility: String? = ""

    private var datajenisSumberAir: MutableList<ResponseDataJenisSumberAir> = ArrayList()
    lateinit var adapterSumberAir: AdapterSumberAir
    lateinit var adapterPagerKeliling: AdapterPagerKeliling

    private var list: ArrayList<String>? = ArrayList()
    private var listpagerKeliling: ArrayList<DataMultiplePagerKeliling>? = ArrayList()


    private var mStepListFilter: ArrayList<String>? = ArrayList()

    private var isidayaListrik = ""
    private var isipanjangPagerKeliling = ""
    private var isilebarPagerKeliling = ""
    private var isitebalPagerKeliling = ""

    private lateinit var realm: Realm
    private lateinit var session: Session

    private var LandId: Int? = 0
    private var ProjectId: Int? = 0

    private lateinit var mdialogConfirmDelete: AlertDialog
    private lateinit var mDialogView: View

    private var mpositionImageClick: Int? = null
    lateinit var adapterTakePictureOtherFacilities: AdapterTakePictureFacilities

    private var mcurrentSession: mki.siojt2.model.Session? = null
    private var isTelephoneLane = false

    val dataListYesNo = mutableListOf(
            ResponseDataListExistOrNo(1, "Ada"),
            ResponseDataListExistOrNo(2, "Tidak Ada")
    )


    @SuppressLint("SetTextI18n", "NewApi", "InflateParams")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_add_sarana_lain)
        toolbar_elevation.visibility = View.VISIBLE

        /* init realm */
        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        session = Session(this)
        val historyData = intent?.extras?.getString("data_current_tanah")

        if (historyData != null) {
            val toJson = Gson().fromJson(historyData, Array<String>::class.java).toList()
            LandId = toJson[0].toInt()
            ProjectId = toJson[1].toInt()
        }

        ednomorTanah.setText(LandId.toString())

        val mdataSession = intent?.extras?.getString("seesion_fasilitas")
        if (mdataSession != null) {
            val turnsType = object : TypeToken<mki.siojt2.model.Session>() {}.type
            mcurrentSession = Gson().fromJson<mki.siojt2.model.Session>(mdataSession, turnsType)
            if (mcurrentSession!!.id != null && mcurrentSession!!.statusPost == "2") {
                tvFormToolbarTitle.text = "Edit data objek - Benda lain yang berhubungan dengan tanah"
            }
        } else {
            tvFormToolbarTitle.text = "Tambah data objek - Benda lain yang berhubungan dengan tanah"
        }


        //val turnsType = object : TypeToken<ResponseDataListTanah>() {}.type
        //val dataconvert = Gson().fromJson<ResponseDataListTanah>(historyData, turnsType)

        //getPresenter()?.getlistdataPemilik(20, "")

        /* set dialog confirm delete data */
        mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_confirm_delete, null)
        val mBuilder = AlertDialog.Builder(this)
        mdialogConfirmDelete = mBuilder.create()

        mdialogConfirmDelete.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        mdialogConfirmDelete.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mdialogConfirmDelete.setCancelable(false)
        val window = mdialogConfirmDelete.window
        if (window != null) {
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            mdialogConfirmDelete.window!!.setLayout(width, height)
        }
        mdialogConfirmDelete.setView(mDialogView)

        /* set toolbar*/
        if (toolbarCustom != null) {
            setSupportActionBar(toolbarCustom)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarCustom.setNavigationOnClickListener {
            finish()
        }

        /* set adapter & recycleview foto sarana pelengkap */
        adapterTakePictureOtherFacilities = AdapterTakePictureFacilities(this, this@ActicityFormAddObjekLain)
        adapterTakePictureOtherFacilities.setListnerItemImageClick(this)

        val linearLayoutManagerphotoFasilitas = LinearLayoutManager(this@ActicityFormAddObjekLain, LinearLayoutManager.VERTICAL, false)

        rvlistphotoSaranaPelengkap.layoutManager = linearLayoutManagerphotoFasilitas
        rvlistphotoSaranaPelengkap.adapter = adapterTakePictureOtherFacilities

        spinnerpemilikSaranaPelengkap.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val selectedProject = adapterView.getItemAtPosition(position) as TempPemilikObjek
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            partyTypdeId = selectedProject.pemilikId

            //Toast.makeText(this@ActicityFormAddObjekLain, partyTypdeId.toString(), Toast.LENGTH_SHORT).show()
        }

        /*val dataimagefasilitas = DataImageFacilities()
        dataimagefasilitas.imagepathLand = "iqbal"
        dataimagefasilitas.imagenameLand = "babab"
        //adapterTakePictureOtherFacilities.addItems(dataimagefasilitas)
        val listDPPT: ArrayList<DataImageFacilities> = arrayListOf(
                dataimagefasilitas
        )*/

        /*spinnerjenisFasilitas.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataJenisFasilitas
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            sendidJenisFasilitas = selectedProject.facilityTypeId

            when (sendidJenisFasilitas) {
                1 -> {
                    Log.d("nanas", sendidJenisFasilitasHistory.toString())
                    when (sendidJenisFasilitasHistory) {
                        2 -> {
                            sendidJenisFasilitasHistory = 1
                            adapterSumberAir.clear()
                        }
                        3 -> sendidJenisFasilitasHistory = 1
                        else -> sendidJenisFasilitasHistory = 1
                    }

                    sendvalueSumberAir = ""
                    tvKategoriFasilitas.text = "Kapasitas Listrik Terpasang"
                    lldayaListrik.visibility = View.VISIBLE
                    rvsumberAir.visibility = View.GONE
                    rvpagarKeliling.visibility = View.GONE
                    tvinfo.visibility = View.GONE

                }
                2 -> {
                    Log.d("nanas", sendidJenisFasilitasHistory.toString())
                    when (sendidJenisFasilitasHistory) {
                        1 -> {
                            sendidJenisFasilitasHistory = 2
                            eddayalistrik.setText("")
                        }
                        3 -> {
                            sendidJenisFasilitasHistory = 2
                            adapterPagerKeliling.getStepList()
                        }
                        else -> sendidJenisFasilitasHistory = 2
                    }

                    sendvalueFacility = ""
                    tvKategoriFasilitas.text = "Masukkan jenis sumber air bersih"
                    lldayaListrik.visibility = View.GONE
                    rvsumberAir.visibility = View.VISIBLE
                    rvpagarKeliling.visibility = View.GONE
                    tvinfo.visibility = View.VISIBLE
                    getPresenter()?.getdaftaJenisSumberAir()
                }
                3 -> {
                    Log.d("nanas", sendidJenisFasilitasHistory.toString())
                    when (sendidJenisFasilitasHistory) {
                        1 -> {
                            sendidJenisFasilitasHistory = 3
                            eddayalistrik.setText("")
                        }
                        2 -> {
                            sendidJenisFasilitasHistory = 3
                            adapterSumberAir.clear()
                        }
                        else -> sendidJenisFasilitasHistory = 3
                    }
                    tvKategoriFasilitas.text = "Masukkan jenis pagar keliling"
                    lldayaListrik.visibility = View.GONE
                    rvsumberAir.visibility = View.VISIBLE
                    rvpagarKeliling.visibility = View.VISIBLE
                    tvinfo.visibility = View.VISIBLE

                    getPresenter()?.getdaftaJenisPagarKeliling()
                }
            }

            //Toast.makeText(this@ActicityFormAddObjekLain, sendidJenisFasilitas.toString(), Toast.LENGTH_SHORT).show()
        }*/

        /* last used */
        /*spinnerjenisFasilitas.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataJenisFasilitas
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            sendidJenisFasilitas?.clear()
            sendKapasitasSaranaPelengkap?.clear()
            sendKapasitasValueSaranaPelengkap?.clear()
            sendidJenisFasilitas?.add(selectedProject.facilityTypeId!!)

            when (sendidJenisFasilitas?.get(0)) {
                1 -> {
                    lldayaListrik.visibility = View.VISIBLE
                    lljenissumberAir.visibility = View.GONE
                    llpagerKeliling.visibility = View.GONE

                    spinnersumberAir.text = null
                    spinnerPagerKeliling.text = null
                    edpanjangPagarKeliling.text = null
                    edlebarPagarKeliling.text = null
                    edtebalPagarKeliling.text = null

                    tvKategoriFasilitas.text = "Kapasitas Listrik Terpasang"

                }
                2 -> {
                    lldayaListrik.visibility = View.GONE
                    lljenissumberAir.visibility = View.VISIBLE
                    llpagerKeliling.visibility = View.GONE

                    eddayalistrik.text = null
                    spinnerPagerKeliling.text = null
                    edpanjangPagarKeliling.text = null
                    edlebarPagarKeliling.text = null
                    edtebalPagarKeliling.text = null


                    tvKategoriFasilitas.text = "Kapasitas Listrik Terpasang"
                    getPresenter()?.getdaftaJenisSumberAir()
                }
                3 -> {
                    lldayaListrik.visibility = View.GONE
                    lljenissumberAir.visibility = View.GONE
                    llpagerKeliling.visibility = View.VISIBLE

                    spinnersumberAir.text = null
                    eddayalistrik.text = null
                    tvKategoriFasilitas.text = "Jenis Pagar Keliling"

                    getPresenter()?.getdaftaJenisPagarKeliling()
                }
            }

            //Toast.makeText(this@ActicityFormAddObjekLain, sendidJenisFasilitas.toString(), Toast.LENGTH_SHORT).show()
        }

        spinnersumberAir.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataJenisSumberAir
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            //val idsumberAir = selectedProject.cleanWaterSourceId
            sendKapasitasSaranaPelengkap?.clear()
            sendvalueSumberAir = selectedProject.cleanWaterSourceName
            sendKapasitasSaranaPelengkap!!.add(selectedProject.cleanWaterSourceName.toString())

            //Toast.makeText(this@ActicityFormAddObjekLain, sendvalueSumberAir.toString(), Toast.LENGTH_SHORT).show()
        }

        spinnerPagerKeliling.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataJenisPagarKeliling
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            //val idsumberAir = selectedProject.cleanWaterSourceId
            sendvalueSumberAir = selectedProject.circumferentialFenceName
            sendKapasitasSaranaPelengkap?.clear()
            sendKapasitasSaranaPelengkap!!.add(selectedProject.circumferentialFenceName.toString())

            //Toast.makeText(this@ActicityFormAddObjekLain, sendvalueSumberAir.toString(), Toast.LENGTH_SHORT).show()
        }*/

        /* data offline jenis fasilitas */
        val realmResultJenisFasilitas = realm.where(ResponseDataJenisFasilitasWithOutParam::class.java).findAllAsync()
        val dataResultJenisFasilitas: MutableList<ResponseDataJenisFasilitasWithOutParam> = realm.copyFromRealm(realmResultJenisFasilitas)

        val adapterJenisFasilitas = ArrayAdapter<ResponseDataJenisFasilitasWithOutParam>(this@ActicityFormAddObjekLain, R.layout.item_spinner, dataResultJenisFasilitas)
        spinnerjenisFasilitas.setAdapter(adapterJenisFasilitas)

        /* data offline sumber air bersih */
        val realmResultSumberAir = realm.where(ResponseDataJenisSumberAirWithOutParam::class.java).findAllAsync()
        val dataResultSumberAir: MutableList<ResponseDataJenisSumberAirWithOutParam> = realm.copyFromRealm(realmResultSumberAir)

        val adapterSumberAirBersih = ArrayAdapter<ResponseDataJenisSumberAirWithOutParam>(this@ActicityFormAddObjekLain,
                R.layout.item_spinner, dataResultSumberAir)
        spinnersumberAir.setAdapter(adapterSumberAirBersih)

        /* data offline pagar keliling */
        val realmPagarKeliling = realm.where(ResponseDataJenisPagarKelilingWithOutParam::class.java).findAllAsync()
        val dataPagarKeliling: MutableList<ResponseDataJenisPagarKelilingWithOutParam> = realm.copyFromRealm(realmPagarKeliling)

        val adapterPagarKeliling = ArrayAdapter<ResponseDataJenisPagarKelilingWithOutParam>(this@ActicityFormAddObjekLain,
                R.layout.item_spinner, dataPagarKeliling)
        spinnerPagerKeliling.setAdapter(adapterPagarKeliling)

        val adapterexistOrNo = ArrayAdapter<ResponseDataListExistOrNo>(this, R.layout.item_spinner, dataListYesNo)
        spinner_telephone_line.setAdapter(adapterexistOrNo)
        spinner_telephone_line.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            //val valueSpinner = adapterView.getItemAtPosition(position).toString()
            //val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListExistOrNo
            if (position == adapterView.firstVisiblePosition) {
                isTelephoneLane = true
                ll_quantity_telephone_line.visibility = View.VISIBLE
            } else {
                ll_quantity_telephone_line.visibility = View.GONE
                isTelephoneLane = false
                ed_quantity_telephone_line.text = null
            }
            //Toast.makeText(activity!!, sendidKeberadaaanSutet.toString(), Toast.LENGTH_SHORT).show()

        }

        /* use data offline */
        spinnerjenisFasilitas.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataJenisFasilitasWithOutParam
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            sendJenisFasilitasName = valueSpinner
            sendidJenisFasilitas?.clear()
            sendKapasitasSaranaPelengkap?.clear()
            sendKapasitasValueSaranaPelengkap?.clear()
            sendidJenisFasilitas?.add(selectedProject.getfacilityTypeId()!!)
            when {
                valueSpinner.equals("Saluran Tegangan Listrik", ignoreCase = true) -> {
                    lljenissumberAir.visibility = View.GONE
                    llpagerKeliling.visibility = View.GONE
                    ll_telephone_lane.visibility = View.GONE
                    ll_swiming_pool.visibility = View.GONE
                    ll_air_conditioner.visibility = View.GONE
                    ll_elevator.visibility = View.GONE
                    ll_wwtp.visibility = View.GONE
                    ll_clean_water_proccessing.visibility = View.GONE
                    ll_pavement.visibility = View.GONE
                    llsaranaPelengkapLain.visibility = View.GONE
                    lldayaListrik.visibility = View.VISIBLE

                    //Swimming pool
                    clearSwimmingPool()
                    clearAirConditioner()
                    clearElevatorLift()
                    clearWwtpFacility()
                    clearCleanWaterProcessingFacility()
                    clearPavementacility()

                    spinnersumberAir.text = null
                    spinnerPagerKeliling.text = null
                    edpanjangPagarKeliling.text = null
                    edlebarPagarKeliling.text = null
                    edtebalPagarKeliling.text = null
                    edsaranaPelenglapLain.text = null
                    isTelephoneLane = false

                    tvKategoriFasilitas.text = "Kapasitas listrik terpasang"
                }

                valueSpinner.equals("Sumber Air Bersih", ignoreCase = true) -> {
                    lldayaListrik.visibility = View.GONE
                    llpagerKeliling.visibility = View.GONE
                    ll_swiming_pool.visibility = View.GONE
                    ll_telephone_lane.visibility = View.GONE
                    ll_air_conditioner.visibility = View.GONE
                    ll_elevator.visibility = View.GONE
                    ll_wwtp.visibility = View.GONE
                    ll_clean_water_proccessing.visibility = View.GONE
                    ll_pavement.visibility = View.GONE
                    llsaranaPelengkapLain.visibility = View.GONE
                    lljenissumberAir.visibility = View.VISIBLE

                    //Swimming pool
                    clearSwimmingPool()
                    clearAirConditioner()
                    clearElevatorLift()
                    clearWwtpFacility()
                    clearCleanWaterProcessingFacility()
                    clearPavementacility()

                    eddayalistrik.text = null
                    spinnerPagerKeliling.text = null
                    edpanjangPagarKeliling.text = null
                    edlebarPagarKeliling.text = null
                    edtebalPagarKeliling.text = null
                    edsaranaPelenglapLain.text = null
                    isTelephoneLane = false

                    tvKategoriFasilitas.text = "Sumber air bersih"
                    //getPresenter()?.getdaftaJenisSumberAir()
                }

                valueSpinner.equals("Pagar Keliling", ignoreCase = true) -> {
                    lldayaListrik.visibility = View.GONE
                    lljenissumberAir.visibility = View.GONE
                    ll_swiming_pool.visibility = View.GONE
                    ll_telephone_lane.visibility = View.GONE
                    ll_air_conditioner.visibility = View.GONE
                    ll_elevator.visibility = View.GONE
                    ll_wwtp.visibility = View.GONE
                    ll_clean_water_proccessing.visibility = View.GONE
                    ll_pavement.visibility = View.GONE
                    llsaranaPelengkapLain.visibility = View.GONE
                    llpagerKeliling.visibility = View.VISIBLE

                    //Swimming pool
                    clearSwimmingPool()
                    clearAirConditioner()
                    clearElevatorLift()
                    clearWwtpFacility()
                    clearCleanWaterProcessingFacility()
                    clearPavementacility()

                    spinnersumberAir.text = null
                    eddayalistrik.text = null
                    edsaranaPelenglapLain.text = null
                    isTelephoneLane = false
                    tvKategoriFasilitas.text = "Pagar keliling"

                    // Start animator to reveal the selection view, starting from the FAB itself
                    val anim = ViewAnimationUtils.createCircularReveal(
                            llpagerKeliling,
                            llpagerKeliling.width - (spinnerjenisFasilitas.width / 2),
                            spinnerjenisFasilitas.height / 2,
                            spinnerjenisFasilitas.width / 2f,
                            llpagerKeliling.width.toFloat())
                    anim.duration = 700
                    anim.interpolator = AccelerateDecelerateInterpolator()

                    anim.addListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationStart(animation: Animator?) {
                            super.onAnimationEnd(animation)
                            llpagerKeliling.visibility = View.VISIBLE
                        }
                    })

                    anim.start()
                    //getPresenter()?.getdaftaJenisPagarKeliling()
                }

                valueSpinner.equals("Telephone Line", ignoreCase = true) -> {
                    lldayaListrik.visibility = View.GONE
                    lljenissumberAir.visibility = View.GONE
                    llpagerKeliling.visibility = View.GONE
                    ll_air_conditioner.visibility = View.GONE
                    ll_elevator.visibility = View.GONE
                    ll_wwtp.visibility = View.GONE
                    ll_clean_water_proccessing.visibility = View.GONE
                    ll_pavement.visibility = View.GONE
                    llsaranaPelengkapLain.visibility = View.GONE
                    ll_swiming_pool.visibility = View.GONE
                    ll_telephone_lane.visibility = View.VISIBLE

                    //Swimming pool
                    clearAirConditioner()
                    clearElevatorLift()
                    clearWwtpFacility()
                    clearCleanWaterProcessingFacility()
                    clearPavementacility()

                    spinnersumberAir.text = null
                    eddayalistrik.text = null
                    spinnerPagerKeliling.text = null
                    edpanjangPagarKeliling.text = null
                    edlebarPagarKeliling.text = null
                    edtebalPagarKeliling.text = null
                    edsaranaPelenglapLain.text = null
                    tvKategoriFasilitas.text = "Telephone Line"
                }

                valueSpinner.equals("Kolam Renang", ignoreCase = true) -> {
                    lldayaListrik.visibility = View.GONE
                    lljenissumberAir.visibility = View.GONE
                    llpagerKeliling.visibility = View.GONE
                    ll_telephone_lane.visibility = View.GONE
                    ll_air_conditioner.visibility = View.GONE
                    ll_elevator.visibility = View.GONE
                    ll_wwtp.visibility = View.GONE
                    ll_clean_water_proccessing.visibility = View.GONE
                    ll_pavement.visibility = View.GONE
                    llsaranaPelengkapLain.visibility = View.GONE
                    edsaranaPelenglapLain.text = null
                    ll_swiming_pool.visibility = View.VISIBLE

                    //Swimming pool
                    clearAirConditioner()
                    clearElevatorLift()
                    clearWwtpFacility()
                    clearCleanWaterProcessingFacility()
                    clearPavementacility()

                    spinnersumberAir.text = null
                    eddayalistrik.text = null
                    spinnerPagerKeliling.text = null
                    edpanjangPagarKeliling.text = null
                    edlebarPagarKeliling.text = null
                    edtebalPagarKeliling.text = null
                    edsaranaPelenglapLain.text = null
                    isTelephoneLane = false

                    tvKategoriFasilitas.text = "Kolam renang"
                }

                valueSpinner.equals("Air Conditioner (AC)", ignoreCase = true) -> {
                    lldayaListrik.visibility = View.GONE
                    lljenissumberAir.visibility = View.GONE
                    ll_telephone_lane.visibility = View.GONE
                    llpagerKeliling.visibility = View.GONE
                    ll_swiming_pool.visibility = View.GONE
                    ll_elevator.visibility = View.GONE
                    ll_wwtp.visibility = View.GONE
                    ll_clean_water_proccessing.visibility = View.GONE
                    ll_pavement.visibility = View.GONE
                    llsaranaPelengkapLain.visibility = View.GONE
                    edsaranaPelenglapLain.text = null
                    ll_air_conditioner.visibility = View.VISIBLE

                    clearSwimmingPool()
                    clearElevatorLift()
                    clearWwtpFacility()
                    clearCleanWaterProcessingFacility()
                    clearPavementacility()

                    spinnersumberAir.text = null
                    eddayalistrik.text = null
                    spinnerPagerKeliling.text = null
                    edpanjangPagarKeliling.text = null
                    edlebarPagarKeliling.text = null
                    edtebalPagarKeliling.text = null
                    isTelephoneLane = false

                    tvKategoriFasilitas.text = "Air Conditioner (AC)"
                }

                valueSpinner.equals("Elevator/Lift", ignoreCase = true) -> {
                    lldayaListrik.visibility = View.GONE
                    lljenissumberAir.visibility = View.GONE
                    llpagerKeliling.visibility = View.GONE
                    ll_telephone_lane.visibility = View.GONE
                    ll_swiming_pool.visibility = View.GONE
                    ll_air_conditioner.visibility = View.GONE
                    ll_wwtp.visibility = View.GONE
                    ll_clean_water_proccessing.visibility = View.GONE
                    ll_pavement.visibility = View.GONE
                    llsaranaPelengkapLain.visibility = View.GONE
                    edsaranaPelenglapLain.text = null
                    ll_elevator.visibility = View.VISIBLE

                    clearSwimmingPool()
                    clearAirConditioner()
                    clearWwtpFacility()
                    clearCleanWaterProcessingFacility()
                    clearPavementacility()

                    spinnersumberAir.text = null
                    eddayalistrik.text = null
                    spinnerPagerKeliling.text = null
                    edpanjangPagarKeliling.text = null
                    edlebarPagarKeliling.text = null
                    edtebalPagarKeliling.text = null
                    isTelephoneLane = false

                    tvKategoriFasilitas.text = "Elevator/Lift"
                }
                valueSpinner.equals("Wastewater Treatment Plant (WWTP)", ignoreCase = true) -> {
                    lldayaListrik.visibility = View.GONE
                    lljenissumberAir.visibility = View.GONE
                    llpagerKeliling.visibility = View.GONE
                    ll_telephone_lane.visibility = View.GONE
                    ll_swiming_pool.visibility = View.GONE
                    ll_air_conditioner.visibility = View.GONE
                    ll_clean_water_proccessing.visibility = View.GONE
                    ll_pavement.visibility = View.GONE
                    ll_elevator.visibility = View.GONE
                    llsaranaPelengkapLain.visibility = View.GONE
                    edsaranaPelenglapLain.text = null
                    ll_wwtp.visibility = View.VISIBLE

                    clearSwimmingPool()
                    clearAirConditioner()
                    clearElevatorLift()
                    clearCleanWaterProcessingFacility()
                    clearPavementacility()

                    spinnersumberAir.text = null
                    eddayalistrik.text = null
                    spinnerPagerKeliling.text = null
                    edpanjangPagarKeliling.text = null
                    edlebarPagarKeliling.text = null
                    edtebalPagarKeliling.text = null
                    isTelephoneLane = false

                    tvKategoriFasilitas.text = "Wastewater Treatment Plant (WWTP)"
                }

                valueSpinner.equals("Clean Water Processing", ignoreCase = true) -> {
                    lldayaListrik.visibility = View.GONE
                    lljenissumberAir.visibility = View.GONE
                    llpagerKeliling.visibility = View.GONE
                    ll_telephone_lane.visibility = View.GONE
                    ll_swiming_pool.visibility = View.GONE
                    ll_air_conditioner.visibility = View.GONE
                    ll_pavement.visibility = View.GONE
                    ll_elevator.visibility = View.GONE
                    ll_wwtp.visibility = View.GONE
                    llsaranaPelengkapLain.visibility = View.GONE
                    edsaranaPelenglapLain.text = null
                    ll_clean_water_proccessing.visibility = View.VISIBLE

                    clearSwimmingPool()
                    clearAirConditioner()
                    clearElevatorLift()
                    clearWwtpFacility()
                    clearPavementacility()

                    spinnersumberAir.text = null
                    eddayalistrik.text = null
                    spinnerPagerKeliling.text = null
                    edpanjangPagarKeliling.text = null
                    edlebarPagarKeliling.text = null
                    edtebalPagarKeliling.text = null
                    isTelephoneLane = false

                    tvKategoriFasilitas.text = "Clean Water Processing"
                }

                valueSpinner.equals("Pavement", ignoreCase = true) -> {
                    lldayaListrik.visibility = View.GONE
                    lljenissumberAir.visibility = View.GONE
                    llpagerKeliling.visibility = View.GONE
                    ll_telephone_lane.visibility = View.GONE
                    ll_swiming_pool.visibility = View.GONE
                    ll_air_conditioner.visibility = View.GONE
                    ll_elevator.visibility = View.GONE
                    ll_wwtp.visibility = View.GONE
                    ll_clean_water_proccessing.visibility = View.GONE
                    ll_pavement.visibility = View.VISIBLE
                    llsaranaPelengkapLain.visibility = View.GONE
                    edsaranaPelenglapLain.text = null

                    clearSwimmingPool()
                    clearAirConditioner()
                    clearElevatorLift()
                    clearWwtpFacility()
                    clearCleanWaterProcessingFacility()

                    spinnersumberAir.text = null
                    eddayalistrik.text = null
                    spinnerPagerKeliling.text = null
                    edpanjangPagarKeliling.text = null
                    edlebarPagarKeliling.text = null
                    edtebalPagarKeliling.text = null
                    isTelephoneLane = false

                    tvKategoriFasilitas.text = "Pavement"
                }
                else -> {
                    llsaranaPelengkapLain.visibility = View.VISIBLE
                }
            }


            /*when (valueSpinner.equals("Saluran Tegangan Listrik", ignoreCase = true)) {
                1 -> {
                    lldayaListrik.visibility = View.VISIBLE
                    lljenissumberAir.visibility = View.GONE
                    llpagerKeliling.visibility = View.GONE

                    spinnersumberAir.text = null
                    spinnerPagerKeliling.text = null
                    edpanjangPagarKeliling.text = null
                    edlebarPagarKeliling.text = null
                    edtebalPagarKeliling.text = null

                    tvKategoriFasilitas.text = "Kapasitas listrik terpasang"

                }
                2 -> {
                    lldayaListrik.visibility = View.GONE
                    lljenissumberAir.visibility = View.VISIBLE
                    llpagerKeliling.visibility = View.GONE

                    eddayalistrik.text = null
                    spinnerPagerKeliling.text = null
                    edpanjangPagarKeliling.text = null
                    edlebarPagarKeliling.text = null
                    edtebalPagarKeliling.text = null

                    tvKategoriFasilitas.text = "Jenis sumber air"
                    //getPresenter()?.getdaftaJenisSumberAir()
                }
                3 -> {
                    lldayaListrik.visibility = View.GONE
                    lljenissumberAir.visibility = View.GONE
                    llpagerKeliling.visibility = View.VISIBLE

                    spinnersumberAir.text = null
                    eddayalistrik.text = null
                    tvKategoriFasilitas.text = "Jenis pagar keliling"

                    // Start animator to reveal the selection view, starting from the FAB itself
                    val anim = ViewAnimationUtils.createCircularReveal(
                            llpagerKeliling,
                            llpagerKeliling.width - (spinnerjenisFasilitas.width / 2),
                            spinnerjenisFasilitas.height / 2,
                            spinnerjenisFasilitas.width / 2f,
                            llpagerKeliling.width.toFloat())
                    anim.duration = 700
                    anim.interpolator = AccelerateDecelerateInterpolator()

                    anim.addListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationStart(animation: Animator?) {
                            super.onAnimationEnd(animation)
                            llpagerKeliling.visibility = View.VISIBLE
                        }
                    })

                    anim.start()

                    //getPresenter()?.getdaftaJenisPagarKeliling()
                }
            }*/
            //Toast.makeText(this@ActicityFormAddObjekLain, sendidJenisFasilitas.toString(), Toast.LENGTH_SHORT).show()
        }

        spinnersumberAir.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataJenisSumberAirWithOutParam
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            //val idsumberAir = selectedProject.cleanWaterSourceId
            sendKapasitasSaranaPelengkap?.clear()
            sendvalueSumberAir = selectedProject.getcleanWaterName()
            sendKapasitasSaranaPelengkap!!.add(selectedProject.getcleanWaterName().toString())

            //Toast.makeText(this@ActicityFormAddObjekLain, sendvalueSumberAir.toString(), Toast.LENGTH_SHORT).show()
        }

        spinnerPagerKeliling.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataJenisPagarKelilingWithOutParam
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            //val idsumberAir = selectedProject.cleanWaterSourceId
            sendvalueSumberAir = selectedProject.getcircumFerentialFenceName()
            sendKapasitasSaranaPelengkap?.clear()
            sendKapasitasSaranaPelengkap!!.add(selectedProject.getcircumFerentialFenceName().toString())

            //Toast.makeText(this@ActicityFormAddObjekLain, sendvalueSumberAir.toString(), Toast.LENGTH_SHORT).show()
        }

        eddayalistrik.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                isidayaListrik = s.toString()
            }

            override fun afterTextChanged(s: Editable) {}
        })

        edpanjangPagarKeliling.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                isipanjangPagerKeliling = s.toString()
            }

            override fun afterTextChanged(s: Editable) {}
        })

        edlebarPagarKeliling.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                isilebarPagerKeliling = s.toString()
            }

            override fun afterTextChanged(s: Editable) {}
        })

        edtebalPagarKeliling.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                isitebalPagerKeliling = s.toString()
            }

            override fun afterTextChanged(s: Editable) {}
        })


        if (list == null || list!!.size == 0 || list!!.isEmpty()) {
            list = java.util.ArrayList()
            list!!.add("")
        }

        if (listpagerKeliling == null || listpagerKeliling!!.size == 0 || listpagerKeliling!!.isEmpty()) {
            listpagerKeliling = java.util.ArrayList()
            listpagerKeliling!!.add(DataMultiplePagerKeliling("", ""))
        }

        btnaddObjekLain.setSafeOnClickListener {
            if (validate()) {
                when {
                    sendJenisFasilitasName.equals("Saluran Tegangan Listrik", ignoreCase = true) -> {
                        sendKapasitasSaranaPelengkap?.clear()
                        sendKapasitasValueSaranaPelengkap?.clear()
                        sendKapasitasValueSatuanSaranaPelengkap?.clear()

                        //Menambahakan data kapasitas listrike ke column kapasitas
                        sendKapasitasValueSaranaPelengkap?.add(isidayaListrik)
                    }
                    sendJenisFasilitasName.equals("Pagar Keliling", ignoreCase = true) -> {
                        sendKapasitasValueSatuanSaranaPelengkap?.clear()
                        sendKapasitasValueSaranaPelengkap?.clear()

                        sendKapasitasValueSaranaPelengkap?.add(isipanjangPagerKeliling)
                        sendKapasitasValueSaranaPelengkap?.add(isilebarPagerKeliling)
                        sendKapasitasValueSaranaPelengkap?.add(isitebalPagerKeliling)

                        sendKapasitasValueSatuanSaranaPelengkap?.add("m")
                        sendKapasitasValueSatuanSaranaPelengkap?.add("m")
                        sendKapasitasValueSatuanSaranaPelengkap?.add("cm")
                    }
                    else -> sendKapasitasValueSatuanSaranaPelengkap?.clear()
                }

                val dataPhoto: List<DataImageFacilities> = adapterTakePictureOtherFacilities.returnDataPhoto()

                var isvalidPhoto = false
                for (i in dataPhoto.indices) {
                    if (dataPhoto[i].imagepathLand!!.toString().isNotEmpty() && dataPhoto[i].imagenameLand!!.toString().isEmpty() ||
                            dataPhoto[i].imagepathLand!!.toString().isEmpty() && dataPhoto[i].imagenameLand!!.toString().isNotEmpty()) {
                        val sweetAlretLoading = SweetAlertDialog(this@ActicityFormAddObjekLain, SweetAlertDialog.WARNING_TYPE)
                        sweetAlretLoading.titleText = "LENGKAPI DATA"
                        sweetAlretLoading.setCancelable(false)
                        sweetAlretLoading.confirmText = "OK"
                        sweetAlretLoading.setConfirmClickListener { sDialog ->
                            sDialog?.let { if (it.isShowing) it.dismiss() }
                        }
                        sweetAlretLoading.show()
                        isvalidPhoto = false
                        break
                    } else {
                        isvalidPhoto = true
                        //mimagePath.add(dataPhoto[i].imageEncode.toString())
                        //mimageTitle.add(dataPhoto[i].imageName.toString())
                    }
                }

                Handler().postDelayed({
                    if (validate()) {
                        if (isvalidPhoto) {
                            if (mcurrentSession != null && mcurrentSession!!.id != null && mcurrentSession!!.statusPost == "2") {
                                //setting up for edit
                                onShowLoading()
                                realm.executeTransactionAsync({ inrealm ->
                                    val sarana = inrealm.where(SaranaLain::class.java)
                                            .equalTo("SaranaIdTemp", mcurrentSession!!.id)
                                            .and()
                                            .equalTo("LandIdTemp", LandId)
                                            .findFirst()
                                    sarana!!.perlengkapanId = partyTypdeId
                                    sarana.perlengkapanName = spinnerpemilikSaranaPelengkap.text.toString()

                                    sarana.notes = edNotes.text.toString()

                                    sarana.jenisFasilitasId = sendidJenisFasilitas.toString()
                                    sarana.jenisFasilitasName = sendJenisFasilitasName.toString()
                                    sarana.jenisFasilitasNameOther = edsaranaPelenglapLain.text.toString()

                                    sarana.kapasitas = sendKapasitasSaranaPelengkap.toString()
                                    sarana.kapasitasValue = sendKapasitasValueSaranaPelengkap.toString()
                                    sarana.satuanKapasitasValue = sendKapasitasValueSatuanSaranaPelengkap.toString()

                                    val resultdataImageFacilities = inrealm.where(DataImageFacilities::class.java).findAll()
                                    val dataimageFacilities = resultdataImageFacilities.where().equalTo("imageId", mcurrentSession!!.id).findAll()
                                    dataimageFacilities?.deleteAllFromRealm()

                                    val dataImageFacilitiesPlusId: MutableList<DataImageFacilities> = ArrayList()
                                    for (element in dataPhoto.indices) {
                                        if (dataPhoto[element].imagepathLand.isNotEmpty() && dataPhoto[element].imagenameLand.isNotEmpty()) {
                                            val createdataImage = DataImageFacilities()
                                            createdataImage.imageId = mcurrentSession!!.id
                                            createdataImage.imagepathLand = dataPhoto[element].imagepathLand
                                            createdataImage.imagenameLand = dataPhoto[element].imagenameLand
                                            dataImageFacilitiesPlusId.add(createdataImage)
                                        }
                                        //Log.d("hasil", dataPhoto.dataImagePhoto!![element].imagenameLand.toString())
                                    }

                                    if (dataImageFacilitiesPlusId.isNotEmpty()) {
                                        val dataFacilitiesPlants = inrealm.copyToRealmOrUpdate(dataImageFacilitiesPlusId)
                                        val dataFacilitiesPlants2: RealmList<DataImageFacilities> = RealmList()
                                        for (element in dataFacilitiesPlants) {
                                            dataFacilitiesPlants2.add(element)
                                        }
                                        sarana.dataImageOtherFacilities = dataFacilitiesPlants2
                                    }

                                    when {
                                        sendJenisFasilitasName.equals("Kolam Renang", ignoreCase = true) -> {
                                            val datUpdateswimmingPool = inrealm
                                                    .where(OtherFacilityOfSwimmingPool::class.java)
                                                    .equalTo("swimmingPoolFacilityId", mcurrentSession!!.id)
                                                    .findFirst()

                                            datUpdateswimmingPool?.swimmingPoolYear = tv_pick_year_build_swimming_pool.text.toString().toDouble()
                                            datUpdateswimmingPool?.swimmingPoolLength = ed_length_of_swimming_pool.text.toString().toDouble()
                                            datUpdateswimmingPool?.swimmingPoolWidth = ed_width_of_swimming_pool.text.toString().toDouble()
                                            datUpdateswimmingPool?.swimmingPoolDept = ed_depth_of_swimming_pool.text.toString().toDouble()
                                            sarana.otherFacilityOfSwimmingPool = datUpdateswimmingPool
                                        }

                                        sendJenisFasilitasName.equals("Telephone Line", ignoreCase = true) -> {
                                            val datUpdateTelephoneLine = inrealm
                                                    .where(OtherFacilityOfTelephoneLine::class.java)
                                                    .equalTo("telephoneLineFacilityId", mcurrentSession!!.id)
                                                    .findFirst()

                                            if (isTelephoneLane) datUpdateTelephoneLine?.telephoneLineYesNO = 1 else datUpdateTelephoneLine?.telephoneLineYesNO = null
                                            sarana.otherFacilityOfTelephoneLine = datUpdateTelephoneLine
                                        }

                                        sendJenisFasilitasName.equals("Air Conditioner (AC)", ignoreCase = true) -> {
                                            val datUpdateAirConditioner = inrealm
                                                    .where(OtherFacilityOfAirConditioner::class.java)
                                                    .equalTo("airConditionerFacilityId", mcurrentSession!!.id)
                                                    .findFirst()

                                            datUpdateAirConditioner?.airConditionerBrand = ed_brand_of_ac.text.toString()
                                            datUpdateAirConditioner?.airConditionerType = ed_type_of_ac.text.toString()
                                            datUpdateAirConditioner?.airConditionerMadeIn = ed_made_in_of_ac.text.toString()
                                            datUpdateAirConditioner?.airConditionerCapacity = ed_capacity_of_ac.text.toString().toInt()
                                            datUpdateAirConditioner?.airConditionerQuantity = ed_quantity_of_ac.text.toString().toInt()
                                            datUpdateAirConditioner?.airConditionerYearProduction = tv_year_production_of_ac.text.toString().toInt()

                                            sarana.otherFacilityOfAirConditioner = datUpdateAirConditioner
                                        }

                                        sendJenisFasilitasName.equals("Elevator/Lift", ignoreCase = true) -> {

                                            val datUpdateElevator = inrealm
                                                    .where(OtherFacilityOfElevator::class.java)
                                                    .equalTo("elevatorFacilityId", mcurrentSession!!.id)
                                                    .findFirst()

                                            datUpdateElevator?.elevatorBrand = ed_brand_of_elevator.text.toString()
                                            datUpdateElevator?.elevatorMadeIn = ed_made_in_of_elevator.text.toString()
                                            datUpdateElevator?.elevatorMaximumPersonQuantity = ed_max_person_quantity_of_elevator.text.toString().toInt()
                                            datUpdateElevator?.elevatorMaximumWeight = ed_max_weight_of_elevator.text.toString().toInt()
                                            datUpdateElevator?.elevatorLength = ed_length_of_elevator.text.toString().toInt()
                                            datUpdateElevator?.elevatorWidth = ed_width_of_elevator.text.toString().toInt()
                                            datUpdateElevator?.elevatorHeight = ed_height_of_elevator.text.toString().toInt()
                                            datUpdateElevator?.elevatorQuantity = ed_quantity_of_elevator.text.toString().toInt()
                                            datUpdateElevator?.elevatorQYearProduction = tv_year_production_of_elevator.text.toString().toInt()

                                            sarana.otherFacilityOfElevator = datUpdateElevator
                                        }

                                        sendJenisFasilitasName.equals("Wastewater Treatment Plant (WWTP)", ignoreCase = true) -> {
                                            val datUpdateWwtp = inrealm
                                                    .where(OtherFacilityOfWWTP::class.java)
                                                    .equalTo("wwtpFacilityId", mcurrentSession!!.id)
                                                    .findFirst()

                                            datUpdateWwtp?.wwtpCapacity = ed_capacity_of_wwtp.text.toString().toInt()
                                            datUpdateWwtp?.wwtpYearBuilt = tv_year_production_of_wwtp.text.toString().toInt()
                                            sarana.otherFacilityOfWWTP = datUpdateWwtp
                                        }

                                        sendJenisFasilitasName.equals("Clean Water Processing", ignoreCase = true) -> {
                                            val datUpdatecleanWaterProcessing = inrealm
                                                    .where(OtherFacilityOfCleanWaterProcessingInstallation::class.java)
                                                    .equalTo("cleanWaterProcessingInstallationFacilityId", mcurrentSession!!.id)
                                                    .findFirst()

                                            datUpdatecleanWaterProcessing?.cleanWaterProcessingInstallationCapacity = ed_capacity_of_clean_water_proccessing.text.toString().toInt()
                                            datUpdatecleanWaterProcessing?.cleanWaterProcessingYearProduction = tv_year_production_of_clean_water_proccessing.text.toString().toInt()
                                            sarana.otherFacilityOfCleanWaterProcessingInstallation = datUpdatecleanWaterProcessing
                                        }

                                        sendJenisFasilitasName.equals("Pavement", ignoreCase = true) -> {
                                            val datUpdatPavement = inrealm
                                                    .where(OtherFacilityOfPavement::class.java)
                                                    .equalTo("pavementFacilityId", mcurrentSession!!.id)
                                                    .findFirst()

                                            datUpdatPavement?.pavementCapacity = ed_capacity_of_pavement.text.toString().toInt()
                                            sarana.otherFacilityOfPavement = datUpdatPavement
                                        }

                                        sendJenisFasilitasName.equals("Pagar Keliling", ignoreCase = true) -> {
                                            sendKapasitasValueSatuanSaranaPelengkap?.clear()
                                            sendKapasitasValueSaranaPelengkap?.clear()

                                            sendKapasitasValueSaranaPelengkap?.add(isipanjangPagerKeliling)
                                            sendKapasitasValueSaranaPelengkap?.add(isilebarPagerKeliling)
                                            sendKapasitasValueSaranaPelengkap?.add(isitebalPagerKeliling)

                                            sendKapasitasValueSatuanSaranaPelengkap?.add("m")
                                            sendKapasitasValueSatuanSaranaPelengkap?.add("m")
                                            sendKapasitasValueSatuanSaranaPelengkap?.add("cm")
                                        }

                                        else -> sendKapasitasValueSatuanSaranaPelengkap?.clear()
                                    }

                                    sarana.isCreate = 2
                                    inrealm.copyToRealmOrUpdate(sarana)

                                }, {
                                    Log.d("fasilitas", "success update facilities objects")
                                    runOnUiThread {
                                        onDismissLoading()
                                        dialogsuccessTransaksi("""
                                Data
                                berhasil
                                diperbaharui
                            """.trimIndent())
                                    }
                                }, {
                                    Log.d("fasilitas", "failed update facilities objects")
                                    Log.d("fasilitas", it.localizedMessage)
                                })

                            }
                            else {
                                onShowLoading()
                                realm.executeTransactionAsync({ inrealm ->
                                    val saranaPelengkapId = inrealm.where(SaranaLain::class.java).max("SaranaIdTemp")
                                    val nextsaranaPelengkapId: Int
                                    nextsaranaPelengkapId = if (saranaPelengkapId == null) {
                                        1
                                    } else {
                                        saranaPelengkapId.toInt() + 1
                                    }

                                    val sarana = inrealm.createObject(SaranaLain::class.java, nextsaranaPelengkapId)
                                    //sarana.saranaIdTemp = nextId
                                    sarana.saranaId = null
                                    sarana.projectId = ProjectId
                                    sarana.landIdTemp = LandId
                                    sarana.landId = null

                                    sarana.notes = edNotes.text.toString()

                                    sarana.perlengkapanId = partyTypdeId
                                    sarana.perlengkapanName = spinnerpemilikSaranaPelengkap.text.toString()

                                    sarana.jenisFasilitasId = sendidJenisFasilitas.toString()
                                    sarana.jenisFasilitasName = sendJenisFasilitasName.toString()
                                    sarana.jenisFasilitasNameOther = edsaranaPelenglapLain.text.toString()

                                    sarana.kapasitas = sendKapasitasSaranaPelengkap.toString()
                                    sarana.kapasitasValue = sendKapasitasValueSaranaPelengkap.toString()

                                    sarana.satuanKapasitasValue = sendKapasitasValueSatuanSaranaPelengkap.toString()

                                    val dataImageFacilitiesPlusId: MutableList<DataImageFacilities> = ArrayList()
                                    for (element in dataPhoto.indices) {
                                        if (dataPhoto[element].imagepathLand.isNotEmpty() && dataPhoto[element].imagenameLand.isNotEmpty()) {
                                            val createdataImage = DataImageFacilities()
                                            createdataImage.imageId = nextsaranaPelengkapId
                                            createdataImage.imagepathLand = dataPhoto[element].imagepathLand
                                            createdataImage.imagenameLand = dataPhoto[element].imagenameLand
                                            dataImageFacilitiesPlusId.add(createdataImage)
                                        }
                                        //Log.d("hasil", dataPhoto.dataImagePhoto!![element].imagenameLand.toString())
                                    }

                                    if (dataImageFacilitiesPlusId.isNotEmpty()) {
                                        val dataFacilitiesPlants = inrealm.copyToRealmOrUpdate(dataImageFacilitiesPlusId)
                                        val dataFacilitiesPlants2: RealmList<DataImageFacilities> = RealmList()
                                        for (element in dataFacilitiesPlants) {
                                            dataFacilitiesPlants2.add(element)
                                        }
                                        sarana.dataImageOtherFacilities = dataFacilitiesPlants2
                                    }

                                    sarana.isCreate = 1

                                    when {
                                        sendJenisFasilitasName.equals("Kolam Renang", ignoreCase = true) -> {
                                            val swimmingPoolId = inrealm.where(OtherFacilityOfSwimmingPool::class.java).max("swimmingPoolId")
                                            val newswimmingPoolId: Int
                                            newswimmingPoolId = if (swimmingPoolId == null) {
                                                1
                                            } else {
                                                swimmingPoolId.toInt() + 1
                                            }

                                            val swimmingPool = inrealm.createObject(OtherFacilityOfSwimmingPool::class.java, newswimmingPoolId)
                                            swimmingPool.swimmingPoolFacilityId = nextsaranaPelengkapId
                                            swimmingPool.swimmingPoolYear = tv_pick_year_build_swimming_pool.text.toString().toDouble()
                                            swimmingPool.swimmingPoolLength = ed_length_of_swimming_pool.text.toString().toDouble()
                                            swimmingPool.swimmingPoolWidth = ed_width_of_swimming_pool.text.toString().toDouble()
                                            swimmingPool.swimmingPoolDept = ed_depth_of_swimming_pool.text.toString().toDouble()
                                            sarana.otherFacilityOfSwimmingPool = swimmingPool
                                        }


                                        sendJenisFasilitasName.equals("Telephone Line", ignoreCase = true) -> {
                                            val telephoneLineId = inrealm.where(OtherFacilityOfTelephoneLine::class.java).max("telephoneLineId")
                                            val newtelephoneLineId: Int
                                            newtelephoneLineId = if (telephoneLineId == null) {
                                                1
                                            } else {
                                                telephoneLineId.toInt() + 1
                                            }

                                            val telephoneLine = inrealm.createObject(OtherFacilityOfTelephoneLine::class.java, newtelephoneLineId)
                                            telephoneLine.telephoneLineFacilityId = nextsaranaPelengkapId
                                            if (isTelephoneLane) telephoneLine.telephoneLineYesNO = 1
                                            telephoneLine.telephoneLineQuantity = ed_quantity_telephone_line.text.toString().toInt()
                                            sarana.otherFacilityOfTelephoneLine = telephoneLine
                                        }

                                        sendJenisFasilitasName.equals("Air Conditioner (AC)", ignoreCase = true) -> {
                                            val airConditionerId = inrealm.where(OtherFacilityOfAirConditioner::class.java).max("airConditionerId")
                                            val newairConditionerId: Int
                                            newairConditionerId = if (airConditionerId == null) {
                                                1
                                            } else {
                                                airConditionerId.toInt() + 1
                                            }

                                            val airConditioner = inrealm.createObject(OtherFacilityOfAirConditioner::class.java, newairConditionerId)
                                            airConditioner.airConditionerFacilityId = nextsaranaPelengkapId
                                            airConditioner.airConditionerBrand = ed_brand_of_ac.text.toString()
                                            airConditioner.airConditionerType = ed_type_of_ac.text.toString()
                                            airConditioner.airConditionerMadeIn = ed_made_in_of_ac.text.toString()
                                            airConditioner.airConditionerCapacity = ed_capacity_of_ac.text.toString().toInt()
                                            airConditioner.airConditionerQuantity = ed_quantity_of_ac.text.toString().toInt()
                                            airConditioner.airConditionerYearProduction = tv_year_production_of_ac.text.toString().toInt()

                                            sarana.otherFacilityOfAirConditioner = airConditioner
                                        }

                                        sendJenisFasilitasName.equals("Elevator/Lift", ignoreCase = true) -> {
                                            val elevatorId = inrealm.where(OtherFacilityOfElevator::class.java).max("elevatorId")
                                            val newelevatorId: Int
                                            newelevatorId = if (elevatorId == null) {
                                                1
                                            } else {
                                                elevatorId.toInt() + 1
                                            }

                                            val elevator = inrealm.createObject(OtherFacilityOfElevator::class.java, newelevatorId)
                                            elevator.elevatorFacilityId = nextsaranaPelengkapId
                                            elevator.elevatorBrand = ed_brand_of_elevator.text.toString()
                                            elevator.elevatorMadeIn = ed_made_in_of_elevator.text.toString()
                                            elevator.elevatorMaximumPersonQuantity = ed_max_person_quantity_of_elevator.text.toString().toInt()
                                            elevator.elevatorMaximumWeight = ed_max_weight_of_elevator.text.toString().toInt()
                                            elevator.elevatorLength = ed_length_of_elevator.text.toString().toInt()
                                            elevator.elevatorWidth = ed_width_of_elevator.text.toString().toInt()
                                            elevator.elevatorHeight = ed_height_of_elevator.text.toString().toInt()
                                            elevator.elevatorQuantity = ed_quantity_of_elevator.text.toString().toInt()
                                            elevator.elevatorQYearProduction = tv_year_production_of_elevator.text.toString().toInt()

                                            sarana.otherFacilityOfElevator = elevator
                                        }

                                        sendJenisFasilitasName.equals("Wastewater Treatment Plant (WWTP)", ignoreCase = true) -> {
                                            val wwtpId = inrealm.where(OtherFacilityOfWWTP::class.java).max("wwtpId")
                                            val newwwtpId: Int
                                            newwwtpId = if (wwtpId == null) {
                                                1
                                            } else {
                                                wwtpId.toInt() + 1
                                            }

                                            val wwtpFacility = inrealm.createObject(OtherFacilityOfWWTP::class.java, newwwtpId)
                                            wwtpFacility.wwtpFacilityId = nextsaranaPelengkapId
                                            wwtpFacility.wwtpCapacity = ed_capacity_of_wwtp.text.toString().toInt()
                                            wwtpFacility.wwtpYearBuilt = tv_year_production_of_wwtp.text.toString().toInt()
                                            sarana.otherFacilityOfWWTP = wwtpFacility
                                        }

                                        sendJenisFasilitasName.equals("Clean Water Processing", ignoreCase = true) -> {
                                            val cleanWaterProcessingId = inrealm.where(OtherFacilityOfCleanWaterProcessingInstallation::class.java).max("cleanWaterProcessingInstallationId")
                                            val newcleanWaterProcessingId: Int
                                            newcleanWaterProcessingId = if (cleanWaterProcessingId == null) {
                                                1
                                            } else {
                                                cleanWaterProcessingId.toInt() + 1
                                            }

                                            val cleanWaterProcessingFacility = inrealm.createObject(OtherFacilityOfCleanWaterProcessingInstallation::class.java, newcleanWaterProcessingId)
                                            cleanWaterProcessingFacility.cleanWaterProcessingInstallationFacilityId = nextsaranaPelengkapId
                                            cleanWaterProcessingFacility.cleanWaterProcessingInstallationCapacity = ed_capacity_of_clean_water_proccessing.text.toString().toInt()
                                            cleanWaterProcessingFacility.cleanWaterProcessingYearProduction = tv_year_production_of_clean_water_proccessing.text.toString().toInt()
                                            sarana.otherFacilityOfCleanWaterProcessingInstallation = cleanWaterProcessingFacility
                                        }

                                        sendJenisFasilitasName.equals("Pavement", ignoreCase = true) -> {
                                            val pavementId = inrealm.where(OtherFacilityOfPavement::class.java).max("pavementId")
                                            val newpavementId: Int
                                            newpavementId = if (pavementId == null) {
                                                1
                                            } else {
                                                pavementId.toInt() + 1
                                            }

                                            val pavementFacility = inrealm.createObject(OtherFacilityOfPavement::class.java, newpavementId)
                                            pavementFacility.pavementFacilityId = nextsaranaPelengkapId
                                            pavementFacility.pavementCapacity = ed_capacity_of_pavement.text.toString().toInt()
                                            sarana.otherFacilityOfPavement = pavementFacility
                                        }


                                        sendJenisFasilitasName.equals("Pagar Keliling", ignoreCase = true) -> {
                                            sendKapasitasValueSatuanSaranaPelengkap?.clear()
                                            sendKapasitasValueSaranaPelengkap?.clear()

                                            sendKapasitasValueSaranaPelengkap?.add(isipanjangPagerKeliling)
                                            sendKapasitasValueSaranaPelengkap?.add(isilebarPagerKeliling)
                                            sendKapasitasValueSaranaPelengkap?.add(isitebalPagerKeliling)

                                            sendKapasitasValueSatuanSaranaPelengkap?.add("m")
                                            sendKapasitasValueSatuanSaranaPelengkap?.add("m")
                                            sendKapasitasValueSatuanSaranaPelengkap?.add("cm")
                                        }

                                        else -> sendKapasitasValueSatuanSaranaPelengkap?.clear()
                                    }

                                }, {
                                    Log.d("fasilitas", "success adds facilities objects")
                                    runOnUiThread {
                                        onDismissLoading()
                                        onsuccesspostSaranaPelengkap()
                                    }
                                }, {
                                    Log.d("fasilitas", "failed adds facilities objects")
                                    Log.d("fasilitas", it.localizedMessage)
                                    runOnUiThread {
                                        onDismissLoading()
                                    }
                                })
                            }
                        }

                    }
                }, 500)
            }
        }

        ll_pick_year_build_swimming_pool.setSafeOnClickListener {
            showDialogYearOnly()
        }

        ll_pick_year_production_of_ac.setSafeOnClickListener {
            showDialogYearOnlyAC()
        }

        ll_pick_year_production_of_elevator.setSafeOnClickListener {
            showDialogYearOnlyElevator()
        }

        ll_pick_year_production_of_wwtp.setSafeOnClickListener {
            showDialogYearOnlyWWTP()
        }

        ll_pick_year_production_of_clean_water_proccessing.setSafeOnClickListener {
            showDialogYearOnlyCleanWaterInstallation()
        }

        getDataForEditAndDelete()
        getDataPemilik()

    }

    private fun showDialogYearOnly() {
        val today = Calendar.getInstance()
        val builder = MonthPickerDialog.Builder(this, MonthPickerDialog.OnDateSetListener { selectedMonth, selectedYear ->
            tv_pick_year_build_swimming_pool.text = selectedYear.toString()
        }, today.get(Calendar.YEAR), today.get(Calendar.MONTH))

        builder.setActivatedYear(today.get(Calendar.YEAR))
                .setTitle("Tahun dibangun kolam renang")
                .setMinYear(today.get(Calendar.YEAR) - 100)
                .setMaxYear(today.get(Calendar.YEAR))
                .setOnMonthChangedListener { selectedMonth ->
                    Log.d(FormSubjek2.TAG, "Selected month : $selectedMonth")
                    // Toast.makeText(MainActivity.this, " Selected month : " + selectedMonth, Toast.LENGTH_SHORT).show();
                }
                .setOnYearChangedListener { selectedYear ->
                    Log.d(FormSubjek2.TAG, "Selected year : $selectedYear")
                    // Toast.makeText(MainActivity.this, " Selected year : " + selectedYear, Toast.LENGTH_SHORT).show();
                }
                .showYearOnly()
                .build()
                .show()
    }

    private fun showDialogYearOnlyAC() {
        val today = Calendar.getInstance()
        val builder = MonthPickerDialog.Builder(this, MonthPickerDialog.OnDateSetListener { selectedMonth, selectedYear ->
            tv_year_production_of_ac.text = selectedYear.toString()
        }, today.get(Calendar.YEAR), today.get(Calendar.MONTH))

        builder.setActivatedYear(today.get(Calendar.YEAR))
                .setTitle("Tahun pembuatan AC")
                .setMinYear(today.get(Calendar.YEAR) - 100)
                .setMaxYear(today.get(Calendar.YEAR))
                .setOnMonthChangedListener { selectedMonth ->
                    Log.d(FormSubjek2.TAG, "Selected month : $selectedMonth")
                    // Toast.makeText(MainActivity.this, " Selected month : " + selectedMonth, Toast.LENGTH_SHORT).show();
                }
                .setOnYearChangedListener { selectedYear ->
                    Log.d(FormSubjek2.TAG, "Selected year : $selectedYear")
                    // Toast.makeText(MainActivity.this, " Selected year : " + selectedYear, Toast.LENGTH_SHORT).show();
                }
                .showYearOnly()
                .build()
                .show()
    }

    private fun showDialogYearOnlyWWTP() {
        val today = Calendar.getInstance()
        val builder = MonthPickerDialog.Builder(this, MonthPickerDialog.OnDateSetListener { selectedMonth, selectedYear ->
            tv_year_production_of_wwtp.text = selectedYear.toString()
        }, today.get(Calendar.YEAR), today.get(Calendar.MONTH))

        builder.setActivatedYear(today.get(Calendar.YEAR))
                .setTitle("Tahun pembuatan")
                .setMinYear(today.get(Calendar.YEAR) - 100)
                .setMaxYear(today.get(Calendar.YEAR))
                .setOnMonthChangedListener { selectedMonth ->
                    Log.d(FormSubjek2.TAG, "Selected month : $selectedMonth")
                    // Toast.makeText(MainActivity.this, " Selected month : " + selectedMonth, Toast.LENGTH_SHORT).show();
                }
                .setOnYearChangedListener { selectedYear ->
                    Log.d(FormSubjek2.TAG, "Selected year : $selectedYear")
                    // Toast.makeText(MainActivity.this, " Selected year : " + selectedYear, Toast.LENGTH_SHORT).show();
                }
                .showYearOnly()
                .build()
                .show()
    }

    private fun showDialogYearOnlyElevator() {
        val today = Calendar.getInstance()
        val builder = MonthPickerDialog.Builder(this, MonthPickerDialog.OnDateSetListener { selectedMonth, selectedYear ->
            tv_year_production_of_elevator.text = selectedYear.toString()
        }, today.get(Calendar.YEAR), today.get(Calendar.MONTH))

        builder.setActivatedYear(today.get(Calendar.YEAR))
                .setTitle("Tahun pembuatan")
                .setMinYear(today.get(Calendar.YEAR) - 100)
                .setMaxYear(today.get(Calendar.YEAR))
                .setOnMonthChangedListener { selectedMonth ->
                    Log.d(FormSubjek2.TAG, "Selected month : $selectedMonth")
                    // Toast.makeText(MainActivity.this, " Selected month : " + selectedMonth, Toast.LENGTH_SHORT).show();
                }
                .setOnYearChangedListener { selectedYear ->
                    Log.d(FormSubjek2.TAG, "Selected year : $selectedYear")
                    // Toast.makeText(MainActivity.this, " Selected year : " + selectedYear, Toast.LENGTH_SHORT).show();
                }
                .showYearOnly()
                .build()
                .show()
    }

    private fun showDialogYearOnlyCleanWaterInstallation() {
        val today = Calendar.getInstance()
        val builder = MonthPickerDialog.Builder(this, MonthPickerDialog.OnDateSetListener { selectedMonth, selectedYear ->
            tv_year_production_of_clean_water_proccessing.text = selectedYear.toString()
        }, today.get(Calendar.YEAR), today.get(Calendar.MONTH))

        builder.setActivatedYear(today.get(Calendar.YEAR))
                .setTitle("Tahun pembuatan")
                .setMinYear(today.get(Calendar.YEAR) - 100)
                .setMaxYear(today.get(Calendar.YEAR))
                .setOnMonthChangedListener { selectedMonth ->
                    Log.d(FormSubjek2.TAG, "Selected month : $selectedMonth")
                    // Toast.makeText(MainActivity.this, " Selected month : " + selectedMonth, Toast.LENGTH_SHORT).show();
                }
                .setOnYearChangedListener { selectedYear ->
                    Log.d(FormSubjek2.TAG, "Selected year : $selectedYear")
                    // Toast.makeText(MainActivity.this, " Selected year : " + selectedYear, Toast.LENGTH_SHORT).show();
                }
                .showYearOnly()
                .build()
                .show()
    }

    private fun getDataPemilik() {
        val results = realm.where(TempPemilikObjek::class.java).findAll()
        if (results != null) {
            val adapterpemiliksaranaPelengkap = ArrayAdapter<TempPemilikObjek>(this@ActicityFormAddObjekLain,
                    R.layout.item_spinner, results)
            adapterpemiliksaranaPelengkap.setNotifyOnChange(true)
            spinnerpemilikSaranaPelengkap.setAdapter(adapterpemiliksaranaPelengkap)
            adapterpemiliksaranaPelengkap.setNotifyOnChange(true)
        } else {
            Log.e("data", "kosong")
        }
    }

    private fun getDataForEditAndDelete() {
        if (mcurrentSession != null) {
            if (mcurrentSession!!.id != null && mcurrentSession!!.statusPost == "2") {
                val results = realm.where(SaranaLain::class.java).equalTo("SaranaIdTemp", session.id.toInt()).findFirst()
                if (results != null) {
                    tampilkanData(results)
                    // Log.d("results", results.toString())
                } else {
                    Log.e("data", "kosong")
                }
            } else if (mcurrentSession!!.id != null && mcurrentSession!!.statusPost.equals("3")) {
                val results = realm.where(SaranaLain::class.java).equalTo("SaranaIdTemp", session.id.toInt()).findFirst()
                if (results != null) {
                    tampilkanData(results)
                }
                mDialogView.btnYesDelete.setOnClickListener {
                    runOnUiThread {
                        realm.executeTransactionAsync({ inRealm ->
                            val resultDataOtherFacility = inRealm.where(SaranaLain::class.java).findAll()
                            val dataParty = resultDataOtherFacility.where().equalTo("SaranaIdTemp", session.id.toInt()).findFirst()
                            val resultdataImageFacilities = inRealm.where(DataImageFacilities::class.java).findAll()
                            val dataimageFacilities = resultdataImageFacilities.where().equalTo("imageId", session.id.toInt()).findAll()

                            when {
                                dataParty?.jenisFasilitasNameOther!!.isEmpty() && dataParty.jenisFasilitasName.equals("Kolam Renang", ignoreCase = true) -> {
                                    val dataSwimmingPool = inRealm
                                            .where(OtherFacilityOfSwimmingPool::class.java)
                                            .equalTo("msaranaLain.SaranaIdTemp", mcurrentSession!!.id)
                                            .and()
                                            .equalTo("msaranaLain.LandIdTemp", LandId)
                                            .findFirst()
                                    dataSwimmingPool?.deleteFromRealm()
                                }

                                dataParty.jenisFasilitasNameOther.isEmpty() && dataParty.jenisFasilitasName.equals("Air Conditioner (AC)", ignoreCase = true) -> {
                                    val dataAirConditioner = inRealm
                                            .where(OtherFacilityOfAirConditioner::class.java)
                                            .equalTo("msaranaLain.SaranaIdTemp", mcurrentSession!!.id)
                                            .and()
                                            .equalTo("msaranaLain.LandIdTemp", LandId)
                                            .findFirst()
                                    dataAirConditioner?.deleteFromRealm()
                                }

                                dataParty.jenisFasilitasNameOther.isEmpty() && dataParty.jenisFasilitasName.equals("Elevator/Lift", ignoreCase = true) -> {
                                    val dataElevator = inRealm
                                            .where(OtherFacilityOfElevator::class.java)
                                            .equalTo("msaranaLain.SaranaIdTemp", mcurrentSession!!.id)
                                            .and()
                                            .equalTo("msaranaLain.LandIdTemp", LandId)
                                            .findFirst()
                                    dataElevator?.deleteFromRealm()
                                }

                                dataParty.jenisFasilitasNameOther.isEmpty() && dataParty.jenisFasilitasName.equals("Wastewater Treatment Plant (WWTP)", ignoreCase = true) -> {
                                    val dataWwtp = inRealm
                                            .where(OtherFacilityOfWWTP::class.java)
                                            .equalTo("msaranaLain.SaranaIdTemp", mcurrentSession!!.id)
                                            .and()
                                            .equalTo("msaranaLain.LandIdTemp", LandId)
                                            .findFirst()
                                    dataWwtp?.deleteFromRealm()
                                }

                                dataParty.jenisFasilitasNameOther.isEmpty() && dataParty.jenisFasilitasName.equals("Clean Water Processing", ignoreCase = true) -> {
                                    val dataCleanWaterProcessing = inRealm
                                            .where(OtherFacilityOfCleanWaterProcessingInstallation::class.java)
                                            .equalTo("msaranaLain.SaranaIdTemp", mcurrentSession!!.id)
                                            .and()
                                            .equalTo("msaranaLain.LandIdTemp", LandId)
                                            .findFirst()
                                    dataCleanWaterProcessing?.deleteFromRealm()

                                }

                                dataParty.jenisFasilitasNameOther.isEmpty() && dataParty.jenisFasilitasName.equals("Pavement", ignoreCase = true) -> {
                                    val dataPavement = inRealm
                                            .where(OtherFacilityOfPavement::class.java)
                                            .equalTo("msaranaLain.SaranaIdTemp", mcurrentSession!!.id)
                                            .and()
                                            .equalTo("msaranaLain.LandIdTemp", LandId)
                                            .findFirst()
                                    dataPavement?.deleteFromRealm()
                                }
                            }

                            dataimageFacilities?.deleteAllFromRealm()
                            dataParty?.deleteFromRealm()

                        }, {
                            Log.d("delete", "onSuccess : delete single object")
                            mdialogConfirmDelete.dismiss()
                            finish()
                        }, {
                            Log.d("delete", "onFailed : ${it.localizedMessage}")
                            Log.d("delete", "onFailed : delete single object")
                        })
                    }
                }

                mDialogView.btnNoDelete.setOnClickListener {
                    mdialogConfirmDelete.dismiss()
                    finish()
                }
                mdialogConfirmDelete.show()
                /*val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
                    when (which) {
                        DialogInterface.BUTTON_POSITIVE -> {
                            val results = realm.where(SaranaLain::class.java).equalTo("SaranaIdTemp", session.id.toInt()).findFirst()
                            realm.beginTransaction()
                            results!!.deleteFromRealm()
                            realm.commitTransaction()
                            dialog.dismiss()
                            finish()
                        }

                        DialogInterface.BUTTON_NEGATIVE -> {
                            dialog.dismiss()
                            finish()
                        }
                    }
                }

                val builder = android.support.v7.app.AlertDialog.Builder(this)
                builder.setTitle("SIOJT")
                        .setMessage("Yakin akan dihapus?")
                        .setPositiveButton("Ya", dialogClickListener)
                        .setNegativeButton("Tidak", dialogClickListener)
                        .setIcon(ContextCompat.getDrawable(this@ActicityFormAddObjekLain, R.drawable.ic_logo_splash)!!)
                        .show()*/
            }
        } else {
            val dataimagefasilitas = DataImageFacilities()
            dataimagefasilitas.imageId = null
            dataimagefasilitas.imagepathLand = ""
            dataimagefasilitas.imagenameLand = ""
            adapterTakePictureOtherFacilities.addItems(dataimagefasilitas)
        }

    }

    @SuppressLint("NewApi")
    private fun tampilkanData(results: SaranaLain) {
        /* disable spiiner jenis fasilitas */
        spinnerjenisFasilitas.isEnabled = false
        spinnerjenisFasilitas.isClickable = false
        spinnerjenisFasilitas.setAdapter(null)

        spinnerpemilikSaranaPelengkap.setText(results.perlengkapanName)
        partyTypdeId = results.perlengkapanId
        spinnerjenisFasilitas.setText(results.jenisFasilitasName)
        sendJenisFasilitasName = results.jenisFasilitasName

        if (results.jenisFasilitasNameOther.isNotEmpty()) {
            llsaranaPelengkapLain.visibility = View.VISIBLE
            edsaranaPelenglapLain.setText(results.jenisFasilitasNameOther)
        } else {
            llsaranaPelengkapLain.visibility = View.GONE
        }

        val liveName = results.jenisFasilitasId
        val replace1 = liveName.replace("[", "")
        val replace2 = replace1.replace("]", "")
        sendidJenisFasilitas?.clear()
        sendidJenisFasilitas?.add(replace2.toInt())

        val mdatafotoFasilitas = realm.copyFromRealm(results.dataImageOtherFacilities)
        if (mdatafotoFasilitas.isNotEmpty()) {
            for (i in 0 until mdatafotoFasilitas.size) {
                adapterTakePictureOtherFacilities.addItems(mdatafotoFasilitas[i])
                adapterTakePictureOtherFacilities.notifyDataSetChanged()
            }
        } else {
            val dataimagefasilitas = DataImageFacilities()
            dataimagefasilitas.imageId = null
            dataimagefasilitas.imagepathLand = ""
            dataimagefasilitas.imagenameLand = ""
            adapterTakePictureOtherFacilities.addItems(dataimagefasilitas)
        }

        when {
            results.jenisFasilitasNameOther.isEmpty() && results.jenisFasilitasName.equals("Telephone Line", ignoreCase = true) -> {
                lldayaListrik.visibility = View.GONE
                lljenissumberAir.visibility = View.GONE
                llpagerKeliling.visibility = View.GONE
                ll_air_conditioner.visibility = View.GONE
                ll_elevator.visibility = View.GONE
                ll_wwtp.visibility = View.GONE
                ll_clean_water_proccessing.visibility = View.GONE
                ll_pavement.visibility = View.GONE
                ll_swiming_pool.visibility = View.GONE
                ll_telephone_lane.visibility = View.VISIBLE

                //Swimming pool
                clearAirConditioner()
                clearElevatorLift()
                clearWwtpFacility()
                clearCleanWaterProcessingFacility()
                clearPavementacility()

                spinnersumberAir.text = null
                spinnerPagerKeliling.text = null
                edpanjangPagarKeliling.text = null
                edlebarPagarKeliling.text = null
                edtebalPagarKeliling.text = null
                tvKategoriFasilitas.text = "Telephone Line"

                val dataUpdateTelephoneLine = realm
                        .where(OtherFacilityOfTelephoneLine::class.java)
                        .equalTo("telephoneLineFacilityId", mcurrentSession!!.id)
                        .findFirst()

                if(dataUpdateTelephoneLine?.telephoneLineYesNO == 1){
                    isTelephoneLane = true
                    ll_quantity_telephone_line.visibility = View.VISIBLE
                    spinner_telephone_line.setText("Ada")
                    ed_quantity_telephone_line.setText(dataUpdateTelephoneLine.telephoneLineQuantity.toString())
                }else{
                    isTelephoneLane = false
                    ll_quantity_telephone_line.visibility = View.GONE
                    spinner_telephone_line.setText("Tidak ada")
                    ed_quantity_telephone_line.setText(null)
                }
            }

            results.jenisFasilitasNameOther.isEmpty() && results.jenisFasilitasName.equals("Saluran Tegangan Listrik", ignoreCase = true) -> {
                lljenissumberAir.visibility = View.GONE
                llpagerKeliling.visibility = View.GONE
                ll_swiming_pool.visibility = View.GONE
                ll_air_conditioner.visibility = View.GONE
                ll_elevator.visibility = View.GONE
                ll_wwtp.visibility = View.GONE
                ll_clean_water_proccessing.visibility = View.GONE
                ll_pavement.visibility = View.GONE
                lldayaListrik.visibility = View.VISIBLE

                clearSwimmingPool()
                clearAirConditioner()
                clearElevatorLift()
                clearWwtpFacility()
                clearCleanWaterProcessingFacility()
                clearPavementacility()

                spinnersumberAir.text = null
                spinnerPagerKeliling.text = null
                edpanjangPagarKeliling.text = null
                edlebarPagarKeliling.text = null
                edtebalPagarKeliling.text = null
                tvKategoriFasilitas.text = "Kapasitas Listrik Terpasang"

                val liveIncome = results.kapasitasValue
                val replac1 = liveIncome.replace("[", "")
                val replac2 = replac1.replace("]", "")
                eddayalistrik.setText(replac2)

                sendKapasitasSaranaPelengkap?.clear()
                sendKapasitasValueSaranaPelengkap?.clear()
                sendKapasitasValueSaranaPelengkap?.add(replac2)
                sendKapasitasValueSatuanSaranaPelengkap?.clear()
            }

            results.jenisFasilitasNameOther.isEmpty() && results.jenisFasilitasName.equals("Sumber Air Bersih", ignoreCase = true) -> {
                lldayaListrik.visibility = View.GONE
                llpagerKeliling.visibility = View.GONE
                ll_swiming_pool.visibility = View.GONE
                ll_air_conditioner.visibility = View.GONE
                ll_elevator.visibility = View.GONE
                ll_wwtp.visibility = View.GONE
                ll_clean_water_proccessing.visibility = View.GONE
                ll_pavement.visibility = View.GONE
                lljenissumberAir.visibility = View.VISIBLE

                clearSwimmingPool()
                clearAirConditioner()
                clearElevatorLift()
                clearWwtpFacility()
                clearCleanWaterProcessingFacility()
                clearPavementacility()

                eddayalistrik.text = null
                spinnerPagerKeliling.text = null
                edpanjangPagarKeliling.text = null
                edlebarPagarKeliling.text = null
                edtebalPagarKeliling.text = null

                tvKategoriFasilitas.text = "Jenis Sumber Air"
                val liveIncome = results.kapasitas
                val replac1 = liveIncome.replace("[", "")
                val replac2 = replac1.replace("]", "")
                spinnersumberAir.setText(replac2)

                sendKapasitasSaranaPelengkap?.clear()
                sendKapasitasSaranaPelengkap?.add(replac2)
                sendKapasitasValueSaranaPelengkap?.clear()
                sendKapasitasValueSatuanSaranaPelengkap?.clear()
            }

            results.jenisFasilitasNameOther.isEmpty() && results.jenisFasilitasName.equals("Pagar Keliling", ignoreCase = true) -> {
                lldayaListrik.visibility = View.GONE
                lljenissumberAir.visibility = View.GONE
                ll_swiming_pool.visibility = View.GONE
                ll_air_conditioner.visibility = View.GONE
                ll_elevator.visibility = View.GONE
                ll_wwtp.visibility = View.GONE
                ll_clean_water_proccessing.visibility = View.GONE
                ll_pavement.visibility = View.GONE
                llpagerKeliling.visibility = View.VISIBLE

                //Swimming pool
                clearSwimmingPool()
                clearAirConditioner()
                clearElevatorLift()
                clearWwtpFacility()
                clearCleanWaterProcessingFacility()
                clearPavementacility()

                spinnersumberAir.text = null
                eddayalistrik.text = null
                tvKategoriFasilitas.text = "Jenis Pagar Keliling"

                val liveName = results.kapasitas
                val replace1 = liveName.replace("[", "")
                val replace2 = replace1.replace("]", "")
                spinnerPagerKeliling.setText(replace2)
                sendKapasitasSaranaPelengkap?.clear()
                sendKapasitasSaranaPelengkap?.add(replace2)

                val liveIncome = results.kapasitasValue
                val replac1 = liveIncome.replace("[", "['")
                val replac2 = replac1.replace("]", "']")
                val replac3 = replac2.replace(",", "','")

                val listNameAr = Gson().fromJson(replac3, Array<String>::class.java).toList()
                edpanjangPagarKeliling.setText(listNameAr[0])
                edlebarPagarKeliling.setText(listNameAr[1])
                edtebalPagarKeliling.setText(listNameAr[2])

                isipanjangPagerKeliling = listNameAr[0]
                isilebarPagerKeliling = listNameAr[1]
                isitebalPagerKeliling = listNameAr[2]
            }

            results.jenisFasilitasNameOther.isEmpty() && results.jenisFasilitasName.equals("Kolam Renang", ignoreCase = true) -> {
                lldayaListrik.visibility = View.GONE
                lljenissumberAir.visibility = View.GONE
                llpagerKeliling.visibility = View.GONE
                ll_air_conditioner.visibility = View.GONE
                ll_elevator.visibility = View.GONE
                ll_wwtp.visibility = View.GONE
                ll_clean_water_proccessing.visibility = View.GONE
                ll_pavement.visibility = View.GONE
                ll_swiming_pool.visibility = View.VISIBLE

                //Swimming pool
                clearAirConditioner()
                clearElevatorLift()
                clearWwtpFacility()
                clearCleanWaterProcessingFacility()
                clearPavementacility()

                val datUpdateswimmingPool = realm
                        .where(OtherFacilityOfSwimmingPool::class.java)
                        .equalTo("swimmingPoolFacilityId", mcurrentSession!!.id)
                        .findFirst()

                tv_pick_year_build_swimming_pool.setText(datUpdateswimmingPool?.swimmingPoolYear.toString())
                ed_length_of_swimming_pool.setText(datUpdateswimmingPool?.swimmingPoolLength.toString())
                ed_width_of_swimming_pool.setText(datUpdateswimmingPool?.swimmingPoolWidth.toString())
                ed_depth_of_swimming_pool.setText(datUpdateswimmingPool?.swimmingPoolDept.toString())

                spinnersumberAir.text = null
                eddayalistrik.text = null
                spinnerPagerKeliling.text = null
                edpanjangPagarKeliling.text = null
                edlebarPagarKeliling.text = null
                edtebalPagarKeliling.text = null
                tvKategoriFasilitas.text = "Kolam renang"
            }

            results.jenisFasilitasNameOther.isEmpty() && results.jenisFasilitasName.equals("Air Conditioner (AC)", ignoreCase = true) -> {
                lldayaListrik.visibility = View.GONE
                lljenissumberAir.visibility = View.GONE
                llpagerKeliling.visibility = View.GONE
                ll_swiming_pool.visibility = View.GONE
                ll_elevator.visibility = View.GONE
                ll_wwtp.visibility = View.GONE
                ll_clean_water_proccessing.visibility = View.GONE
                ll_pavement.visibility = View.GONE
                ll_air_conditioner.visibility = View.VISIBLE

                clearSwimmingPool()
                clearElevatorLift()
                clearWwtpFacility()
                clearCleanWaterProcessingFacility()
                clearPavementacility()

                val datUpdateAirConditioner = realm
                        .where(OtherFacilityOfAirConditioner::class.java)
                        .equalTo("airConditionerFacilityId", mcurrentSession!!.id)
                        .findFirst()

                ed_brand_of_ac.setText(datUpdateAirConditioner?.airConditionerBrand.toString())
                ed_type_of_ac.setText(datUpdateAirConditioner?.airConditionerType)
                ed_made_in_of_ac.setText(datUpdateAirConditioner?.airConditionerMadeIn.toString())
                ed_capacity_of_ac.setText(datUpdateAirConditioner?.airConditionerCapacity.toString())
                ed_quantity_of_ac.setText(datUpdateAirConditioner?.airConditionerQuantity.toString())
                tv_year_production_of_ac.setText(datUpdateAirConditioner?.airConditionerYearProduction.toString())

                spinnersumberAir.text = null
                eddayalistrik.text = null
                spinnerPagerKeliling.text = null
                edpanjangPagarKeliling.text = null
                edlebarPagarKeliling.text = null
                edtebalPagarKeliling.text = null
                tvKategoriFasilitas.text = "Air Conditioner (AC)"
            }

            results.jenisFasilitasNameOther.isEmpty() && results.jenisFasilitasName.equals("Elevator/Lift", ignoreCase = true) -> {
                lldayaListrik.visibility = View.GONE
                lljenissumberAir.visibility = View.GONE
                llpagerKeliling.visibility = View.GONE
                ll_swiming_pool.visibility = View.GONE
                ll_air_conditioner.visibility = View.GONE
                ll_wwtp.visibility = View.GONE
                ll_clean_water_proccessing.visibility = View.GONE
                ll_pavement.visibility = View.GONE
                ll_elevator.visibility = View.VISIBLE

                clearSwimmingPool()
                clearAirConditioner()
                clearWwtpFacility()
                clearCleanWaterProcessingFacility()
                clearPavementacility()

                val datUpdateElevator = realm
                        .where(OtherFacilityOfElevator::class.java)
                        .equalTo("elevatorFacilityId", mcurrentSession!!.id)
                        .findFirst()

                ed_brand_of_elevator.setText(datUpdateElevator?.elevatorBrand.toString())
                ed_made_in_of_elevator.setText(datUpdateElevator?.elevatorMadeIn.toString())
                ed_max_person_quantity_of_elevator.setText(datUpdateElevator?.elevatorMaximumPersonQuantity.toString())
                ed_max_weight_of_elevator.setText(datUpdateElevator?.elevatorMaximumWeight.toString())
                ed_length_of_elevator.setText(datUpdateElevator?.elevatorLength.toString())
                ed_width_of_elevator.setText(datUpdateElevator?.elevatorWidth.toString())
                ed_height_of_elevator.setText(datUpdateElevator?.elevatorHeight.toString())
                ed_quantity_of_elevator.setText(datUpdateElevator?.elevatorQuantity.toString())
                tv_year_production_of_elevator.setText(datUpdateElevator?.elevatorQYearProduction.toString())

                spinnersumberAir.text = null
                eddayalistrik.text = null
                spinnerPagerKeliling.text = null
                edpanjangPagarKeliling.text = null
                edlebarPagarKeliling.text = null
                edtebalPagarKeliling.text = null
                tvKategoriFasilitas.text = "Elevator/Lift"
            }

            results.jenisFasilitasNameOther.isEmpty() && results.jenisFasilitasName.equals("Wastewater Treatment Plant (WWTP)", ignoreCase = true) -> {
                lldayaListrik.visibility = View.GONE
                lljenissumberAir.visibility = View.GONE
                llpagerKeliling.visibility = View.GONE
                ll_swiming_pool.visibility = View.GONE
                ll_air_conditioner.visibility = View.GONE
                ll_clean_water_proccessing.visibility = View.GONE
                ll_pavement.visibility = View.GONE
                ll_elevator.visibility = View.GONE
                ll_wwtp.visibility = View.VISIBLE

                clearSwimmingPool()
                clearAirConditioner()
                clearElevatorLift()
                clearCleanWaterProcessingFacility()
                clearPavementacility()

                val datUpdateWwtp = realm
                        .where(OtherFacilityOfWWTP::class.java)
                        .equalTo("wwtpFacilityId", mcurrentSession!!.id)
                        .findFirst()

                ed_capacity_of_wwtp.setText(datUpdateWwtp?.wwtpCapacity.toString())
                tv_year_production_of_wwtp.setText(datUpdateWwtp?.wwtpYearBuilt.toString())

                spinnersumberAir.text = null
                eddayalistrik.text = null
                spinnerPagerKeliling.text = null
                edpanjangPagarKeliling.text = null
                edlebarPagarKeliling.text = null
                edtebalPagarKeliling.text = null
                tvKategoriFasilitas.text = "Wastewater Treatment Plant (WWTP)"
            }

            results.jenisFasilitasNameOther.isEmpty() && results.jenisFasilitasName.equals("Clean Water Processing", ignoreCase = true) -> {
                lldayaListrik.visibility = View.GONE
                lljenissumberAir.visibility = View.GONE
                llpagerKeliling.visibility = View.GONE
                ll_swiming_pool.visibility = View.GONE
                ll_air_conditioner.visibility = View.GONE
                ll_pavement.visibility = View.GONE
                ll_elevator.visibility = View.GONE
                ll_wwtp.visibility = View.GONE
                ll_clean_water_proccessing.visibility = View.VISIBLE

                clearSwimmingPool()
                clearAirConditioner()
                clearElevatorLift()
                clearWwtpFacility()
                clearPavementacility()

                val datUpdatecleanWaterProcessing = realm
                        .where(OtherFacilityOfCleanWaterProcessingInstallation::class.java)
                        .equalTo("cleanWaterProcessingInstallationFacilityId", mcurrentSession!!.id)
                        .findFirst()

                ed_capacity_of_clean_water_proccessing.setText(datUpdatecleanWaterProcessing?.cleanWaterProcessingInstallationCapacity.toString())
                tv_year_production_of_clean_water_proccessing.text = datUpdatecleanWaterProcessing?.cleanWaterProcessingYearProduction.toString()

                spinnersumberAir.text = null
                eddayalistrik.text = null
                spinnerPagerKeliling.text = null
                edpanjangPagarKeliling.text = null
                edlebarPagarKeliling.text = null
                edtebalPagarKeliling.text = null
                tvKategoriFasilitas.text = "Clean Water Processing"
            }

            results.jenisFasilitasNameOther.isEmpty() && results.jenisFasilitasName.equals("Pavement", ignoreCase = true) -> {
                lldayaListrik.visibility = View.GONE
                lljenissumberAir.visibility = View.GONE
                llpagerKeliling.visibility = View.GONE
                ll_swiming_pool.visibility = View.GONE
                ll_air_conditioner.visibility = View.GONE
                ll_elevator.visibility = View.GONE
                ll_wwtp.visibility = View.GONE
                ll_clean_water_proccessing.visibility = View.GONE
                ll_pavement.visibility = View.VISIBLE

                clearSwimmingPool()
                clearAirConditioner()
                clearElevatorLift()
                clearWwtpFacility()
                clearCleanWaterProcessingFacility()

                val dataUpdatePavement = realm
                        .where(OtherFacilityOfPavement::class.java)
                        .equalTo("pavementFacilityId", mcurrentSession!!.id)
                        .findFirst()

                ed_capacity_of_pavement.setText(dataUpdatePavement?.pavementCapacity.toString())

                spinnersumberAir.text = null
                eddayalistrik.text = null
                spinnerPagerKeliling.text = null
                edpanjangPagarKeliling.text = null
                edlebarPagarKeliling.text = null
                edtebalPagarKeliling.text = null
                tvKategoriFasilitas.text = "Pavement"
            }

            else -> {
                lldayaListrik.visibility = View.GONE
                lljenissumberAir.visibility = View.GONE
                llpagerKeliling.visibility = View.GONE
                ll_swiming_pool.visibility = View.GONE
                ll_air_conditioner.visibility = View.GONE
                ll_elevator.visibility = View.GONE
                ll_wwtp.visibility = View.GONE
                ll_clean_water_proccessing.visibility = View.GONE
                ll_pavement.visibility = View.GONE

                clearSwimmingPool()
                clearAirConditioner()
                clearElevatorLift()
                clearWwtpFacility()
                clearCleanWaterProcessingFacility()
                clearPavementacility()
            }
        }


        /*when (sendidJenisFasilitas?.get(0)) {
            1 -> {

            }
            2 -> {

            }
            3 -> {

            }
        }*/

    }

    private fun getPresenter(): FormAddObjekLainPresenter? {
        formAddObjekLainPresenter = FormAddObjekLainPresenter()
        formAddObjekLainPresenter.onAttach(this)
        return formAddObjekLainPresenter
    }

    private fun clearSwimmingPool() {
        tv_pick_year_build_swimming_pool.text = null
        ed_length_of_swimming_pool.text = null
        ed_width_of_swimming_pool.text = null
        ed_depth_of_swimming_pool.text = null
    }

    private fun clearAirConditioner() {
        ed_brand_of_ac.text = null
        ed_type_of_ac.text = null
        ed_made_in_of_ac.text = null
        ed_capacity_of_ac.text = null
        ed_quantity_of_ac.text = null
        tv_year_production_of_ac.text = null
    }

    private fun clearElevatorLift() {
        ed_brand_of_elevator.text = null
        ed_made_in_of_elevator.text = null
        ed_max_person_quantity_of_elevator.text = null
        ed_max_weight_of_elevator.text = null
        ed_length_of_elevator.text = null
        ed_width_of_elevator.text = null
        ed_height_of_elevator.text = null
        ed_quantity_of_elevator.text = null
        tv_year_production_of_elevator.text = null
    }

    private fun clearWwtpFacility() {
        ed_capacity_of_wwtp.text = null
        tv_year_production_of_wwtp.text = null
    }

    private fun clearCleanWaterProcessingFacility() {
        ed_capacity_of_clean_water_proccessing.text = null
        tv_year_production_of_clean_water_proccessing.text = null
    }

    private fun clearPavementacility() {
        ed_capacity_of_pavement.text = null
    }

    private fun validate(): Boolean {
        val sweetAlretLoading = SweetAlertDialog(this@ActicityFormAddObjekLain, SweetAlertDialog.WARNING_TYPE)
        sweetAlretLoading.setCancelable(false)
        sweetAlretLoading.confirmText = "OK"
        sweetAlretLoading.setConfirmClickListener { sDialog ->
            sDialog?.let { if (it.isShowing) it.dismiss() }
        }

        val valid: Boolean
        when {
            sendidJenisFasilitas!!.isEmpty() || partyTypdeId!!.isEmpty() -> {
                sweetAlretLoading.titleText = "Pilih jenis fasilitas"
                valid = false
                sweetAlretLoading.show()
            }

            sendJenisFasilitasName.equals("Saluran Tegangan Listrik", ignoreCase = true) && isidayaListrik.isEmpty() -> {
                sweetAlretLoading.titleText = "Isi daya listrik terlebih dahulu"
                valid = false
                sweetAlretLoading.show()
            }

            sendJenisFasilitasName.equals("Sumber Air Bersih", ignoreCase = true) && sendKapasitasSaranaPelengkap!!.isEmpty() -> {
                sweetAlretLoading.titleText = "Isi kapasitas terlebih dahulu"
                valid = false
                sweetAlretLoading.show()
            }

            sendJenisFasilitasName.equals("Pagar Keliling", ignoreCase = true) && isipanjangPagerKeliling.isEmpty()
                    || sendJenisFasilitasName.equals("Pagar Keliling", ignoreCase = true) && isilebarPagerKeliling.isEmpty()
                    || sendJenisFasilitasName.equals("Pagar Keliling", ignoreCase = true) && isitebalPagerKeliling.isEmpty() -> {
                sweetAlretLoading.titleText = "Harap lengkapi data"
                valid = false
                sweetAlretLoading.show()
            }

            sendJenisFasilitasName.equals("Telephone Line", ignoreCase = true)  -> {
                if(isTelephoneLane && ed_quantity_telephone_line.text.toString().isEmpty() ||
                        !isTelephoneLane && ed_quantity_telephone_line.text.toString().isEmpty()){
                    sweetAlretLoading.titleText = "Harap lengkapi data"
                    valid = false
                    sweetAlretLoading.show()
                }else{
                    valid = true
                }
            }

            sendJenisFasilitasName.equals("Kolam Renang", ignoreCase = true) && tv_pick_year_build_swimming_pool.text.toString().isEmpty()
                    || sendJenisFasilitasName.equals("Kolam Renang", ignoreCase = true) && ed_length_of_swimming_pool.text.toString().isEmpty()
                    || sendJenisFasilitasName.equals("Kolam Renang", ignoreCase = true) && ed_width_of_swimming_pool.text.toString().isEmpty()
                    || sendJenisFasilitasName.equals("Kolam Renang", ignoreCase = true) && ed_depth_of_swimming_pool.text.toString().isEmpty() -> {
                sweetAlretLoading.titleText = "Harap lengkapi data"
                valid = false
                sweetAlretLoading.show()
            }

            sendJenisFasilitasName.equals("Air Conditioner (AC)", ignoreCase = true) && ed_type_of_ac.text.toString().isEmpty()
                    || sendJenisFasilitasName.equals("Air Conditioner (AC)", ignoreCase = true) && ed_made_in_of_ac.text.toString().isEmpty()
                    || sendJenisFasilitasName.equals("Air Conditioner (AC)", ignoreCase = true) && ed_capacity_of_ac.text.toString().isEmpty()
                    || sendJenisFasilitasName.equals("Air Conditioner (AC)", ignoreCase = true) && ed_quantity_of_ac.text.toString().isEmpty()
                    || sendJenisFasilitasName.equals("Air Conditioner (AC)", ignoreCase = true) && tv_year_production_of_ac.text.toString().isEmpty() -> {
                sweetAlretLoading.titleText = "Harap lengkapi data"
                valid = false
                sweetAlretLoading.show()
            }

            sendJenisFasilitasName.equals("Elevator/Lift", ignoreCase = true) && ed_made_in_of_elevator.text.toString().isEmpty()
                    || sendJenisFasilitasName.equals("Elevator/Lift", ignoreCase = true) && ed_max_person_quantity_of_elevator.text.toString().isEmpty()
                    || sendJenisFasilitasName.equals("Elevator/Lift", ignoreCase = true) && ed_max_weight_of_elevator.text.toString().isEmpty()
                    || sendJenisFasilitasName.equals("Elevator/Lift", ignoreCase = true) && ed_length_of_elevator.text.toString().isEmpty()

                    || sendJenisFasilitasName.equals("Elevator/Lift", ignoreCase = true) && ed_width_of_elevator.text.toString().isEmpty()
                    || sendJenisFasilitasName.equals("Elevator/Lift", ignoreCase = true) && ed_height_of_elevator.text.toString().isEmpty()
                    || sendJenisFasilitasName.equals("Elevator/Lift", ignoreCase = true) && ed_quantity_of_elevator.text.toString().isEmpty()
                    || sendJenisFasilitasName.equals("Elevator/Lift", ignoreCase = true) && tv_year_production_of_elevator.text.toString().isEmpty() -> {
                sweetAlretLoading.titleText = "Harap lengkapi data"
                valid = false
                sweetAlretLoading.show()
            }

            sendJenisFasilitasName.equals("Wastewater Treatment Plant (WWTP)", ignoreCase = true) && ed_capacity_of_wwtp.text.toString().isEmpty()
                    || sendJenisFasilitasName.equals("Wastewater Treatment Plant (WWTP)", ignoreCase = true) && tv_year_production_of_wwtp.text.toString().isEmpty() -> {
                sweetAlretLoading.titleText = "Harap lengkapi data"
                valid = false
                sweetAlretLoading.show()
            }

            sendJenisFasilitasName.equals("Clean Water Processing", ignoreCase = true) && ed_capacity_of_clean_water_proccessing.text.toString().isEmpty()
                    || sendJenisFasilitasName.equals("Clean Water Processing", ignoreCase = true) && tv_year_production_of_clean_water_proccessing.text.toString().isEmpty() -> {
                sweetAlretLoading.titleText = "Harap lengkapi data"
                valid = false
                sweetAlretLoading.show()
            }

            sendJenisFasilitasName.equals("Pavement", ignoreCase = true) && ed_capacity_of_pavement.text.toString().isEmpty() -> {
                sweetAlretLoading.titleText = "Harap lengkapi data"
                valid = false
                sweetAlretLoading.show()
            }

            sendJenisFasilitasName.equals("Lain-Nya", ignoreCase = true) && edsaranaPelenglapLain.text.toString().isEmpty() -> {
                sweetAlretLoading.titleText = "Harap lengkapi data"
                valid = false
                sweetAlretLoading.show()
            }

            else -> {
                valid = true
            }
        }

        return valid
    }


    fun removeItemSumberAir() {
        list?.clear()
        mStepListFilter!!.clear()
        adapterSumberAir.notifyDataSetChanged()

        if (list == null || list!!.size == 0 || list!!.isEmpty()) {
            list = java.util.ArrayList()
            list!!.add("")
        }
    }

    private fun onProfileImageClick() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions()
                        }
                        if (report.isAnyPermissionPermanentlyDenied) {
                            showSettingsDialog()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).check()
    }

    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, object : ImagePickerActivity.PickerOptionListener {
            override fun onTakeCameraSelected() {
                launchCameraIntent()
            }

            override fun onChooseGallerySelected() {
                launchGalleryIntent()
            }
        })
    }

    private fun launchCameraIntent() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE)
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    private fun launchGalleryIntent() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE)
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.dialog_permission_title))
        builder.setMessage(getString(R.string.dialog_permission_message))
        builder.setPositiveButton(getString(R.string.go_to_settings)) { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(getString(android.R.string.cancel)) { dialog, which -> dialog.cancel() }
        builder.show()
    }


    fun removeItemPagarKeliling() {
        listpagerKeliling?.clear()
        //mStepListFilter!!.clear()
        adapterPagerKeliling.notifyDataSetChanged()

        /*if (listpagerKeliling == null || listpagerKeliling!!.size == 0 || listpagerKeliling!!.isEmpty()) {
            listpagerKeliling = java.util.ArrayList()
            listpagerKeliling!!.add("")
        }*/
    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    private fun getEncoded64ImageStringFromBitmap(bitmap: Bitmap): String {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val byteFormat = stream.toByteArray()
        return Base64.encodeToString(byteFormat, Base64.DEFAULT)
    }

    fun addphotoItem() {
        val dataimagefasilitas = DataImageFacilities()
        dataimagefasilitas.imageId = null
        dataimagefasilitas.imagepathLand = ""
        dataimagefasilitas.imagenameLand = ""
        adapterTakePictureOtherFacilities.addItems(dataimagefasilitas)
        adapterTakePictureOtherFacilities.notifyDataSetChanged()
        rvlistphotoSaranaPelengkap.scrollToPosition(adapterTakePictureOtherFacilities.itemCount - 1)
    }

    override fun onsuccessgetdaftaJenisPagarKeliling(responseDataJenisPagarKeliling: MutableList<ResponseDataJenisPagarKeliling>) {
        adapterPagerKeliling = AdapterPagerKeliling(responseDataJenisPagarKeliling, listpagerKeliling, this@ActicityFormAddObjekLain)
        rvpagarKeliling.apply {
            layoutManager = LinearLayoutManager(this@ActicityFormAddObjekLain, LinearLayoutManager.VERTICAL, false)
            setHasFixedSize(true)
            adapter = adapterPagerKeliling
        }

        val adapterspinnerPagerKeliling = ArrayAdapter<ResponseDataJenisPagarKeliling>(this@ActicityFormAddObjekLain,
                R.layout.item_spinner, responseDataJenisPagarKeliling)
        spinnerPagerKeliling.setAdapter(adapterspinnerPagerKeliling)
    }

    override fun onsuccessgetlistdataPemilik(responseDataListPemilik: MutableList<ResponseDataListPemilik>) {
        val adapterpemiliksaranaPelengkap = ArrayAdapter<ResponseDataListPemilik>(this@ActicityFormAddObjekLain,
                R.layout.item_spinner, responseDataListPemilik)
        adapterpemiliksaranaPelengkap.setNotifyOnChange(true)
        spinnerpemilikSaranaPelengkap.setAdapter(adapterpemiliksaranaPelengkap)
        adapterpemiliksaranaPelengkap.setNotifyOnChange(true)
    }

    override fun onsuccessgetdaftaJenisFasilitas(responseDataJenisFasilitas: MutableList<ResponseDataJenisFasilitas>) {
        /*val adapterJenisFasilitas = ArrayAdapter<ResponseDataJenisFasilitas>(this@ActicityFormAddObjekLain,
                R.layout.item_spinner, responseDataJenisFasilitas)
        spinnerjenisFasilitas.setAdapter(adapterJenisFasilitas)*/
    }

    override fun onsuccessgetdaftaJenisSumberAir(responseDataJenisSumberAir: MutableList<ResponseDataJenisSumberAir>) {
        //datajenisSumberAir.addAll(responseDataJenisSumberAir)
        //adapterSumberAir.addItems(responseDataJenisSumberAir)
        /*adapterSumberAir = AdapterSumberAir(responseDataJenisSumberAir, list, this@ActicityFormAddObjekLain)
        rvsumberAir.apply {
            layoutManager = LinearLayoutManager(this@ActicityFormAddObjekLain, LinearLayoutManager.VERTICAL, false)
            setHasFixedSize(true)
            adapter = adapterSumberAir
        }

        val adapterSumberAddObjekLainPresenter = ArrayAdapter<ResponseDataJenisSumberAir>(this@ActicityFormAddObjekLain,
                R.layout.item_spinner, responseDataJenisSumberAir)
        spinnersumberAir.setAdapter(adapterSumberAddObjekLainPresenter)*/
    }

    override fun onsuccesspostSaranaPelengkap() {
        val dialogPopup = DialogSuccessAddMesin()
        dialogPopup.show(supportFragmentManager, "Dalog_popup")
    }


    private fun dialogsuccessTransaksi(title_dialog: String) {
        val dialogPopup = DialogSuccessAddMesin.newInstance(title_dialog)
        dialogPopup.show(supportFragmentManager, "Dalog_popup")
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data!!.getParcelableExtra<Uri>("path")
                try {
                    // You can update this bitmap to your server
                    val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, uri)

                    val dataimagefasilitas = DataImageFacilities()
                    dataimagefasilitas.imageId = null
                    dataimagefasilitas.imagepathLand = uri.path
                    dataimagefasilitas.imagenameLand = ""
                    adapterTakePictureOtherFacilities.updateDateAfterClickImage(mpositionImageClick!!, dataimagefasilitas)
                    adapterTakePictureOtherFacilities.notifyDataSetChanged()
                    Log.d(TAG, "Image cache path: $uri")
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun OnItemImageClick(position: Int) {
        mpositionImageClick = position
        onProfileImageClick()
    }

    override fun onDestroy() {
        super.onDestroy()
        getPresenter()?.onDetach()
        session.setIdAndStatus("", "", "")
    }


}