package mki.siojt2.ui.activity_form_add_objek_lain.adapter_recyleview

import android.content.Context
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import mki.siojt2.R
import android.widget.AdapterView
import com.google.android.material.textfield.TextInputEditText
import mki.siojt2.model.DataMultiplePagerKeliling
import mki.siojt2.model.master_data_sarana_lain.ResponseDataJenisPagarKeliling
import mki.siojt2.ui.activity_form_add_objek_lain.view.ActicityFormAddObjekLain


class AdapterPagerKeliling internal constructor(data: MutableList<ResponseDataJenisPagarKeliling>, steps: ArrayList<DataMultiplePagerKeliling>?, context: Context) : androidx.recyclerview.widget.RecyclerView.Adapter<AdapterPagerKeliling.ViewHolder>() {

    private var mcontext: Context = context
    private var mStepList: ArrayList<DataMultiplePagerKeliling>? = steps
    private var mData: MutableList<ResponseDataJenisPagarKeliling>? = data

    inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        internal var spinnerPagerKeliling: com.weiwangcn.betterspinner.library.BetterSpinner = itemView.findViewById(R.id.spinnerjenisPagarKeliling)
        internal var plus: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnPlusPagarKeliling)
        internal var minus: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnminusPagarKeliling)

        internal var edPanjang: TextInputEditText = itemView.findViewById(R.id.edpanjangPagarKeliling)
        internal var edTinggi: TextInputEditText = itemView.findViewById(R.id.edtinggiPagarKeliling)
        internal var edTebal: TextInputEditText = itemView.findViewById(R.id.edtebalPagarKeliling)

        init {
            minus.setOnClickListener {
                val position = adapterPosition
                try {
                    mStepList!!.removeAt(position)
                    notifyItemRemoved(position)
                    //Toast.makeText(mcontext, mStepList!![position], Toast.LENGTH_SHORT).show()
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                }
            }
            plus.setOnClickListener {
                val position = adapterPosition
                Toast.makeText(mcontext, "${position + 1}", Toast.LENGTH_SHORT).show()
                try {
                    mStepList!!.add(position + 1, DataMultiplePagerKeliling("", ""))
                    notifyItemInserted(position + 1)
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                }
            }

            spinnerPagerKeliling.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
                val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataJenisPagarKeliling
                val valueSpinner = adapterView.getItemAtPosition(position).toString()
                //val idsumberAir = selectedProject.cleanWaterSourceId
                //sendvalueSumberAir = selectedProject.cleanWaterSourceName
                mStepList?.set(adapterPosition, DataMultiplePagerKeliling(selectedProject.circumferentialFenceName.toString(), ""))
                Toast.makeText(mcontext, mStepList.toString(), Toast.LENGTH_SHORT).show()
            }

            /*step.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    steps.set(adapterPosition, s.toString())
                }

                override fun afterTextChanged(s: Editable) {}
            })*/
        }
    }

    override fun getItemCount(): Int {
        return mStepList!!.size
    }

    @NonNull
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_pagar_keliling, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val x = holder.layoutPosition

        Log.d("bancet", mData.toString())
        val adapterspinnerpagerKeliling = ArrayAdapter<ResponseDataJenisPagarKeliling>(mcontext, R.layout.item_spinner, mData!!)
        holder.spinnerPagerKeliling.setAdapter(adapterspinnerpagerKeliling)

        if (x == 0) {
            holder.minus.visibility = View.GONE
        }


        /*holder.spinnerSumberAir.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                val gender = holder.spinnerSumberAir.text.toString()
                Toast.makeText(mcontext, gender, Toast.LENGTH_LONG).show()

            }
        })*/
        /*if (stepList.get(x).length() > 0)
        {
            holder.step.setText(stepList.get(x))
        }
        else
        {
            holder.step.setText(null)
            holder.step.setHint("Next Step")
            holder.step.requestFocus()
        }*/
    }

    fun clear() {
        val size = this.mStepList!!.size
        this.mStepList!!.clear()
        notifyItemRangeRemoved(0, size)
        notifyDataSetChanged()

        if (mStepList!!.isEmpty() || mStepList!!.isNullOrEmpty()) {
            if (mcontext is ActicityFormAddObjekLain) {
                (mcontext as ActicityFormAddObjekLain).removeItemPagarKeliling()
            }
        }
    }

    fun getStepList(): java.util.ArrayList<DataMultiplePagerKeliling> {
        return mStepList!!
    }
}