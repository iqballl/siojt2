package mki.siojt2.ui.activity_form_add_objek_lain.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.ResponseDataListPemilik
import mki.siojt2.model.master_data_sarana_lain.ResponseDataJenisFasilitas
import mki.siojt2.model.master_data_sarana_lain.ResponseDataJenisPagarKeliling
import mki.siojt2.model.master_data_sarana_lain.ResponseDataJenisSumberAir

interface FormAddObjekLainView: MvpView{

    fun onsuccessgetlistdataPemilik(responseDataListPemilik: MutableList<ResponseDataListPemilik>)
    fun onsuccessgetdaftaJenisFasilitas(responseDataJenisFasilitas: MutableList<ResponseDataJenisFasilitas>)
    fun onsuccessgetdaftaJenisSumberAir(responseDataJenisSumberAir: MutableList<ResponseDataJenisSumberAir>)
    fun onsuccessgetdaftaJenisPagarKeliling(responseDataJenisPagarKeliling: MutableList<ResponseDataJenisPagarKeliling>)
    fun onsuccesspostSaranaPelengkap()
}