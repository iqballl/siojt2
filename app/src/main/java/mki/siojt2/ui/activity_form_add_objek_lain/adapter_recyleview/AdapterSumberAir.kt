package mki.siojt2.ui.activity_form_add_objek_lain.adapter_recyleview

import android.content.Context
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import mki.siojt2.R
import mki.siojt2.model.master_data_sarana_lain.ResponseDataJenisSumberAir
import android.widget.AdapterView
import mki.siojt2.ui.activity_form_add_objek_lain.view.ActicityFormAddObjekLain


class AdapterSumberAir internal constructor(data: MutableList<ResponseDataJenisSumberAir>, steps: ArrayList<String>?, context: Context) : androidx.recyclerview.widget.RecyclerView.Adapter<AdapterSumberAir.ViewHolder>() {

    private var mcontext: Context = context
    private var mStepList: ArrayList<String>? = steps
    private var mData: MutableList<ResponseDataJenisSumberAir>? = data

    inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        internal var spinnerSumberAir: com.weiwangcn.betterspinner.library.BetterSpinner = itemView.findViewById(R.id.spinnersumberAir)
        internal var plus: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnPlus)
        internal var minus: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnminus)

        init {
            minus.setOnClickListener {
                val position = adapterPosition
                try {
                    mStepList!!.removeAt(position)
                    notifyItemRemoved(position)
                    Toast.makeText(mcontext, position.toString(), Toast.LENGTH_SHORT).show()
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                }
            }
            plus.setOnClickListener {
                val position = adapterPosition
                Toast.makeText(mcontext, "${position + 1}", Toast.LENGTH_SHORT).show()
                try {
                    mStepList!!.add(position + 1, "")
                    notifyItemInserted(position + 1)
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                }
            }

            spinnerSumberAir.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
                val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataJenisSumberAir
                val valueSpinner = adapterView.getItemAtPosition(position).toString()
                //val idsumberAir = selectedProject.cleanWaterSourceId
                //sendvalueSumberAir = selectedProject.cleanWaterSourceName
                mStepList!![adapterPosition] = selectedProject.cleanWaterSourceName.toString()
                Toast.makeText(mcontext, mStepList.toString(), Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun getItemCount(): Int {
        return mStepList!!.size
    }

    @NonNull
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_sumber_air, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val x = holder.layoutPosition

        Log.d("bancet", mData.toString())
        val adapterspinnerSumberAir2 = ArrayAdapter<ResponseDataJenisSumberAir>(mcontext, R.layout.item_spinner, mData!!)
        holder.spinnerSumberAir.setAdapter(adapterspinnerSumberAir2)

        if (x == 0) {
            holder.minus.visibility = View.GONE
        }


        /*holder.spinnerSumberAir.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                val gender = holder.spinnerSumberAir.text.toString()
                Toast.makeText(mcontext, gender, Toast.LENGTH_LONG).show()

            }
        })*/
        /*if (stepList.get(x).length() > 0)
        {
            holder.step.setText(stepList.get(x))
        }
        else
        {
            holder.step.setText(null)
            holder.step.setHint("Next Step")
            holder.step.requestFocus()
        }*/
    }

    fun clear() {
        val size = this.mStepList!!.size
        this.mStepList!!.clear()
        notifyItemRangeRemoved(0, size)
        notifyDataSetChanged()

        if (mStepList!!.isEmpty() || mStepList!!.isNullOrEmpty()) {
            if (mcontext is ActicityFormAddObjekLain) {
                (mcontext as ActicityFormAddObjekLain).removeItemSumberAir()
            }
        }
    }


    fun getStepList(): java.util.ArrayList<String> {
        return mStepList!!
    }
}