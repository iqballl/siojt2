package mki.siojt2.ui.activity_form_add_objek_lain.presenter

import android.util.Log
import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.ui.activity_form_add_objek_lain.view.FormAddObjekLainView
import mki.siojt2.utils.ErrorHandler
import mki.siojt2.utils.rxJava.RxUtils

class FormAddObjekLainPresenter : BasePresenter(), FormAddObjekLainMVPPresenter {

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun getlistdataPemilik(projectId: Int, landId: String) {
        view().onShowLoading()
        compositeDisposable.add(
                dataManager.getAllParties(projectId, landId)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ responseData ->
                            view().onDismissLoading()
                            if (responseData.status == Constant.STATUS_ERROR) {
                                view().onFailed(responseData.message!!)
                            } else {
                                if (responseData.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetlistdataPemilik(responseData.result!!)
                                } else {
                                    view().onFailed(responseData.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            Log.d("throwable", throwable.localizedMessage)
                        }
        )
    }

    override fun getdaftaJenisFasilitas() {
        view().onShowLoading()
        compositeDisposable.add(
                dataManager.getdaftarjenisFasilitas()
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdaftaJenisFasilitas(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    override fun getdaftaJenisSumberAir() {
        view().onShowLoading()
        compositeDisposable.add(
                dataManager.getdaftarsumberAir()
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdaftaJenisSumberAir(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    override fun getdaftaJenisPagarKeliling() {
        compositeDisposable.add(
                dataManager.getdaftarpagarKeliling()
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdaftaJenisPagarKeliling(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    override fun postSaranaPelengkap(userId: Int, projectLandId: Int, projectPartyId: Int, facilityTypeId: ArrayList<Int>, pOtherFacilityName: ArrayList<String>,
                                     pOtherFacilityValue: ArrayList<String>, pOtherFacilityDescription: ArrayList<String>, accessToken: String) {
        view().onShowLoading()
        compositeDisposable.add(
                dataManager.postOtherFacility(userId, projectLandId, projectPartyId, facilityTypeId, pOtherFacilityName,
                        pOtherFacilityValue, pOtherFacilityDescription, accessToken)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccesspostSaranaPelengkap()
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    private fun view(): FormAddObjekLainView {
        return getView() as FormAddObjekLainView
    }
}