package mki.siojt2.ui.activity_form_add_objek_lain.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface FormAddObjekLainMVPPresenter: MVPPresenter {
    fun getlistdataPemilik(projectId: Int, landId: String)
    fun getdaftaJenisFasilitas()
    fun getdaftaJenisSumberAir()
    fun getdaftaJenisPagarKeliling()

    fun postSaranaPelengkap(userId: Int, projectLandId: Int, projectPartyId: Int, facilityTypeId: ArrayList<Int>, pOtherFacilityName: ArrayList<String>,
                            pOtherFacilityValue: ArrayList<String>, pOtherFacilityDescription: ArrayList<String>, accessToken: String)
}