package mki.siojt2.ui.paint_activity

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.MotionEvent
import android.view.View
import mki.siojt2.model.FingerPath
import kotlin.math.abs
import android.graphics.Bitmap
import android.icu.lang.UCharacter.GraphemeClusterBreak.T



class PaintView(context: Context?, attrs: AttributeSet?) : View(context, attrs) {

    private var mX: Float = 0.toFloat()
    private var mY: Float = 0.toFloat()
    private lateinit var mPath: Path
    private val mPaint: Paint = Paint()
    private val paths: ArrayList<FingerPath> = ArrayList()
    private var currentColor: Int = 0
    private var strokeWidth: Int = 0
    private var emboss: Boolean = false
    private var blur: Boolean = false
    private val mEmboss: MaskFilter
    private val mBlur: MaskFilter
    private lateinit var mBitmap: Bitmap
    private lateinit var mCanvas: Canvas
    private val mBitmapPaint = Paint(Paint.DITHER_FLAG)

    init {
        mPaint.isAntiAlias = true
        mPaint.isDither = true
        mPaint.color = DEFAULT_COLOR
        mPaint.style = Paint.Style.STROKE
        mPaint.strokeJoin = Paint.Join.ROUND
        mPaint.strokeCap = Paint.Cap.ROUND
        mPaint.xfermode = null
        mPaint.alpha = 0xff

        mEmboss = EmbossMaskFilter(floatArrayOf(1f, 1f, 1f), 0.4f, 6F, 3.5f)
        mBlur = BlurMaskFilter(5F, BlurMaskFilter.Blur.NORMAL)
    }

    fun init(metrics: DisplayMetrics) : Bitmap {
        val height = metrics.heightPixels
        val width = metrics.widthPixels
        mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        mCanvas = Canvas(mBitmap)
        currentColor = DEFAULT_COLOR
        strokeWidth = BRUSH_SIZE
        return mBitmap
    }

    fun normal() {
        emboss = false
        blur = false
    }

    fun emboss() {
        emboss = true
        blur = false
    }

    fun blur() {
        emboss = false
        blur = true
    }

    fun clear() {
        backgroundColor = DEFAULT_BG_COLOR
        paths.clear()
        normal()
        invalidate()
    }

    override fun onDraw(canvas: Canvas?) {
        canvas!!.save()
        mCanvas.drawColor(backgroundColor)

        for (fp in paths) {
            mPaint.color = fp.mColor
            mPaint.strokeWidth = fp.mStrokeWidth.toFloat()
            mPaint.maskFilter = null

            if (fp.mEmboss) {
                mPaint.maskFilter = mEmboss
            } else if (fp.mBlur) {
                mPaint.maskFilter = mBlur
            }

            mCanvas.drawPath(fp.mPath, mPaint)

        }

        canvas.drawBitmap(mBitmap, 0F, 0F, mBitmapPaint)
        canvas.restore()
    }


    private fun touchStart(x: Float, y: Float) {
        mPath = Path()
        val fp = FingerPath(currentColor, emboss, blur, strokeWidth, mPath)
        paths.add(fp)


        mPath.reset()
        mPath.moveTo(x, y)
        mX = x
        mY = y
    }

    private fun touchMove(x: Float, y: Float) {
        val dx = abs(x - mX)
        val dy = abs(y - mY)

        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2)
            mX = x
            mY = y
        }
    }

    private fun touchUp() {
        mPath.lineTo(mX, mY)
    }

    private fun getBitmap() : Bitmap{
        this.isDrawingCacheEnabled = true
        this.buildDrawingCache()
        val bmp = Bitmap.createBitmap(this.drawingCache)
        this.isDrawingCacheEnabled = false

        return bmp
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val x = event!!.x
        val y = event.y


        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                touchStart(x, y)
                invalidate()
            }
            MotionEvent.ACTION_MOVE -> {
                touchMove(x, y)
                invalidate()
            }
            MotionEvent.ACTION_UP -> {
                touchUp()
                invalidate()
            }


        }

        return true
    }

    companion object {
        var BRUSH_SIZE = 10
        val DEFAULT_COLOR = Color.parseColor("#3AD323")
        val DEFAULT_BG_COLOR = Color.WHITE
        private var backgroundColor = DEFAULT_BG_COLOR
        private val TOUCH_TOLERANCE = 4f
    }
}