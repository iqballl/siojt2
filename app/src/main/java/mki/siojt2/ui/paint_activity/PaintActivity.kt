package mki.siojt2.ui.paint_activity

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.DisplayMetrics
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_paint.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Typeface
import android.text.Spanned
import android.text.TextPaint
import android.text.style.TypefaceSpan
import android.graphics.Paint
import android.graphics.Bitmap.CompressFormat
import android.os.Environment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import androidx.appcompat.app.AlertDialog
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.EditText
import android.widget.Toast
import com.google.gson.Gson
import id.zelory.compressor.Compressor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import mki.siojt2.model.data_form_tanah.DataFormTanah1
import mki.siojt2.model.data_form_tanah.DataImageCanvas
import mki.siojt2.utils.Utils
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream


class PaintActivity : BaseActivity(), DialogAddCanvasMaps.onSaveCanvas {

    lateinit var paintView: PaintView

    private var mBitmap: Bitmap? = null

    private var dataImageCanvas: MutableList<DataImageCanvas> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paint)

        /* set toolbar*/
        if (toolbarPaint != null) {
            setSupportActionBar(toolbarPaint)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarPaint.setNavigationOnClickListener {
            finish()
        }

        paintView = findViewById(R.id.paintView)
        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)
        paintView.init(metrics)
        mBitmap = paintView.init(metrics)
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        /* menuInflater.inflate(R.menu.menu_paint_draw, menu)
         for(i in 0 until menu!!.size()){
             val item = menu.getItem(i)
             val spanString = SpannableString(menu.getItem(i).title.toString())
             spanString.setSpan(ForegroundColorSpan(Color.parseColor("#757575")), 0, spanString.length, 0)
             item.title = spanString
         }
         return true*/

        val inflater = menuInflater
        inflater.inflate(R.menu.menu_paint_draw, menu)

        val item = menu!!.findItem(R.id.item_save_canvas)
        val spanString = SpannableString(menu.findItem(R.id.item_save_canvas).title.toString())
        spanString.setSpan(ForegroundColorSpan(Color.parseColor("#FFFFFF")), 0, spanString.length, 0)
        item.title = spanString
        val typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-Regular.ttf")
        for (i in 0 until menu.size()) {
            val menuItem = menu.getItem(i)
            val subMenu = menuItem.subMenu
            if (subMenu != null && subMenu.size() > 0) {
                for (j in 0 until subMenu.size()) {
                    val subMenuItem = subMenu.getItem(j)
                    applyFont(subMenuItem, typeface)
                }
            }
            applyFont(menuItem, typeface)
        }
        return super.onCreateOptionsMenu(menu)
    }

    private fun applyFont(menuItem: MenuItem, font: Typeface) {
        val spannableString = SpannableString(menuItem.title)
        spannableString.setSpan(CustomTypeFace("", font), 0, spannableString.length, Spanned.SPAN_INCLUSIVE_INCLUSIVE)
        menuItem.title = spannableString
    }

    private inner class CustomTypeFace(family: String, typeface: Typeface) : TypefaceSpan(family) {

        val mTypeface: Typeface = typeface

        override fun updateDrawState(textPaint: TextPaint) {
            applyCustomTypeFace(textPaint, mTypeface)
        }

        override fun updateMeasureState(textPaint: TextPaint) {
            applyCustomTypeFace(textPaint, mTypeface)
        }

        private fun applyCustomTypeFace(paint: Paint, typeface: Typeface) {
            val oldStyle: Int
            val old = paint.typeface
            oldStyle = old?.style ?: 0

            val fake = oldStyle and typeface.style.inv()
            if (fake and Typeface.BOLD != 0) {
                paint.isFakeBoldText = true
            }
            paint.color = Color.parseColor("#3AD323")
            paint.textSize = 22F

            if (fake and Typeface.ITALIC != 0) {
                paint.textSkewX = -0.25f
            }
            paint.typeface = typeface
        }
    }

    private fun saveImageCanvasTemporary(name: String, bitmap: Bitmap) {
        val stream = ByteArrayOutputStream()
        bitmap.compress(CompressFormat.PNG, 100, stream)
        val byteFormat = stream.toByteArray()

        Utils.deletedataImageCanvas(this)
        dataImageCanvas.clear()
        dataImageCanvas.add(DataImageCanvas(name, byteFormat))
        val jsonImageCanvas = Gson().toJson(dataImageCanvas)
        Utils.savedataImageCanvas(jsonImageCanvas, this)
        //return Base64.decode(byteFormat, Base64.NO_WRAP)
    }


    @SuppressLint("SdCardPath")
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item?.itemId) {
            R.id.itemPilihPaint -> {
                return true
            }
            R.id.jenisPaint1 -> {
                paintView.normal()
                return true
            }
            R.id.jenisPaint2 -> {
                paintView.emboss()
                return true
            }
            R.id.jenisPaint3 -> {
                paintView.blur()
                return true
            }
            R.id.item_save_canvas -> {
                val modalBottomSheet = DialogAddCanvasMaps.newInstance()
                modalBottomSheet.show(supportFragmentManager, "save_canvas_image")
            }
            R.id.itemBersihkanKanvas -> {
                paintView.clear()
                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }

    override fun onsaveCanvas(input: String) {
        Toast.makeText(this, "success on Click", Toast.LENGTH_SHORT).show()
        val name = input
        val bitmap = mBitmap

        //val path = Environment.getExternalStorageDirectory().absolutePath
        val path = File(Environment.getExternalStorageDirectory().toString() + File.separator + "Hasil", "Downloads")
        val file = File(path, "$name.png")

        Log.d("file", path.toString())
        Log.d("file 2", "$file")

        val ostream: OutputStream
        try {
            if (!path.exists()) {
                path.mkdirs()
            }

            if (!file.exists()) {
                file.delete()
                file.createNewFile()
            }
            ostream = FileOutputStream(file)
            //val byteArray = file.toByteArray()
            Log.d("file 3", "$ostream")

            bitmap!!.compress(CompressFormat.PNG, 100, ostream)
            saveImageCanvasTemporary(input, mBitmap!!)
            println("saving......................................................$path")
            ostream.flush()
            ostream.close()
            paintView.invalidate()
            finish()
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {

            paintView.isDrawingCacheEnabled = false
        }
        /*val editalert = AlertDialog.Builder(this@PaintActivity)
              editalert.setTitle("Please Enter the name with which you want to Save")
              val input = EditText(this@PaintActivity)
              val lp = LinearLayout.LayoutParams(
                      LinearLayout.LayoutParams.FILL_PARENT,
                      LinearLayout.LayoutParams.FILL_PARENT)
              input.layoutParams = lp
              editalert.setView(input)
              editalert.setPositiveButton("OK") { dialog, whichButton ->
                  val name = input.text.toString()
                  val bitmap = mBitmap

                  //val path = Environment.getExternalStorageDirectory().absolutePath
                  val path = File(Environment.getExternalStorageDirectory().toString() + File.separator + "Hasil", "Downloads")
                  val file = File(path, "$name.png")

                  Log.d("file", path.toString())
                  Log.d("file 2", "$file")

                  val ostream: OutputStream
                  try {
                      if (!path.exists()) {
                          path.mkdirs()
                      }

                      if (!file.exists()) {
                          file.delete()
                          file.createNewFile()
                      }
                      ostream = FileOutputStream(file)
                      Log.d("file 3", "$ostream")

                      bitmap!!.compress(CompressFormat.PNG, 100, ostream)
                      println("saving......................................................$path")
                      ostream.flush()
                      ostream.close()
                      paintView.invalidate()
                  } catch (e: Exception) {
                      e.printStackTrace()
                  } finally {

                      paintView.isDrawingCacheEnabled = false
                  }
              }

              editalert.show()*/
    }


    override fun onFragmentAttached() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFragmentDetached(tag: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, PaintActivity::class.java)
        }
    }
}