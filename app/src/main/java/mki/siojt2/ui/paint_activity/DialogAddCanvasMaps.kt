package mki.siojt2.ui.paint_activity

import android.content.Context
import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.dialog_canvas_name.view.*
import mki.siojt2.R

class DialogAddCanvasMaps : BottomSheetDialogFragment() {

    private var mListener: onSaveCanvas? = null

    companion object {
        fun newInstance(): DialogAddCanvasMaps {
            return DialogAddCanvasMaps()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_canvas_name, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.img_close.setOnClickListener {
            dismiss()
        }

        view.img_done.setOnClickListener {
            dismiss()
            if (view.editTextNameCanvas.text.toString().isEmpty())
                Toast.makeText(context, "Nama gambar tidak boleh kosong", Toast.LENGTH_SHORT).show()
            else
                mListener?.onsaveCanvas(view.editTextNameCanvas.text.toString())
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            mListener = activity as onSaveCanvas?
        } catch (e: ClassCastException) {
            //Log.e(TAG, "onAttach: ClassCastException: " + e.message)
            throw ClassCastException("Calling Fragment must implement onSaveCanvas : ${e.message}")
        }

    }

    interface onSaveCanvas {
        //void onFragmentInteraction(Uri uri);
        fun onsaveCanvas(input: String)
    }
}