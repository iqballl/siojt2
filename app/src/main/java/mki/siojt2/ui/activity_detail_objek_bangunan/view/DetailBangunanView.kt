package mki.siojt2.ui.activity_detail_objek_bangunan.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.data_detail_objek_bangunan.ResponseDataDetailBangunan

interface DetailBangunanView : MvpView {
    fun onsuccessgetDetalBangunan(responseDataDetailBangunan: ResponseDataDetailBangunan)
}