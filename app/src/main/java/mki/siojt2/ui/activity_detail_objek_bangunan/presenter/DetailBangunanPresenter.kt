package mki.siojt2.ui.activity_detail_objek_bangunan.presenter

import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.ui.activity_detail_objek_bangunan.view.DetailBangunanView
import mki.siojt2.utils.ErrorHandler
import mki.siojt2.utils.rxJava.RxUtils

class DetailBangunanPresenter : BasePresenter(), DetailBangunanMVPPresenter {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun getDetalBangunan(projectBuildingId: Int, accessToken: String) {
        view().onShowLoading()
        compositeDisposable.add(
                dataManager.getdetailobjekBangunan(projectBuildingId, accessToken)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetDetalBangunan(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }


    private fun view(): DetailBangunanView {
        return getView() as DetailBangunanView
    }
}