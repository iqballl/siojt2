package mki.siojt2.ui.activity_detail_objek_bangunan.presenter

import mki.siojt2.base.presenter.MVPPresenter
import mki.siojt2.model.accesstoken.AccessToken

interface DetailBangunanMVPPresenter: MVPPresenter {

    fun getDetalBangunan(projectBuildingId: Int, accessToken: String)
}