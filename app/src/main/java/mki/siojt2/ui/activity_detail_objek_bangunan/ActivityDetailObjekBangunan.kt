package mki.siojt2.ui.activity_detail_objek_bangunan

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import android.util.Log
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_detail_bangunan.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.fragment.detail_jenis_objek.bangunan.DialogMenuObjekBangunan
import mki.siojt2.fragment.detail_jenis_objek.bangunan.JenisDetailObjekBangunan1
import mki.siojt2.model.response_data_objek.ResponseDataListGedung
import mki.siojt2.ui.activity_detail_tanah.presenter.DetailTanahPresenter
import mki.siojt2.utils.extension.removeFragment

class ActivityDetailObjekBangunan : BaseActivity(), DialogMenuObjekBangunan.OnChangeBangunanFragmentListener {

    var codeKategoriObjek: Int? = 1
    var nameKategoriObjek: String? = "Tanah"

    lateinit var detailTanahPresenter: DetailTanahPresenter

    private var dataDetailBangunan: ResponseDataListGedung? = null

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityDetailObjekBangunan::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_bangunan)

        val historyData = intent?.extras?.getString("data_current_bangunan")
        val toJson = Gson().fromJson(historyData , Array<String>::class.java).toList()
        //val turnsType = object : TypeToken<ResponseDataListGedung>() {}.type
        //val dataconvert = Gson().fromJson<ResponseDataListGedung>(historyData, turnsType)

        /*codeKategoriObjek = intent.getIntExtra("jenis_kategori", 0)
        nameKategoriObjek = intent.getStringExtra("jenis_name_kategori")*/

        /* set toolbar*/
        if (toolbarCustomDetailObjekBangunan != null) {
            setSupportActionBar(toolbarCustomDetailObjekBangunan)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        //tvFormToolbarTitle.text = "Tambah data objek - Tanah"

        toolbarCustomDetailObjekBangunan.setNavigationOnClickListener {
            finish()
        }

        /* set current menu*/
        tvcurrentMenuDetailKategori.text = nameKategoriObjek
        tvToolbarTitleCustomDetailObjekBangunan.text = toJson[3]


        /* set current fragment*/
        setFragment(JenisDetailObjekBangunan1.newInstance(toJson[0].toInt(), toJson[4]), JenisDetailObjekBangunan1.TAG)
        /* when (codeKategoriObjek) {
             1 ->{
                 setFragment(JenisMainObjekTanah.newInstance("", ""), JenisMainObjekTanah.TAG)
             }
             2 -> {
                 setFragment(JenisMainObjekBangunan.newInstance("", ""), JenisMainObjekBangunan.TAG)
             }
             3 -> {
                 setFragment(JenisObjekTanaman.newInstance("", ""), JenisMainObjekBangunan.TAG)
             }
             4 -> {
                 setFragment(JenisObjekMesinPerlatan.newInstance("", ""), JenisObjekMesinPerlatan.TAG)
             }
         }*/


        cardmainMenuObjekBangunan.setOnClickListener {
            try {
                val dialogPopup = DialogMenuObjekBangunan.newInstance(codeKategoriObjek!!, nameKategoriObjek!!, toJson[0].toInt(), toJson[2])

                val transaction = (this@ActivityDetailObjekBangunan as FragmentActivity)
                        .supportFragmentManager
                        .beginTransaction()

                dialogPopup.show(transaction, "dialog_menu")

            } catch (e: Exception) {
                Log.e("ClickMenuObject", "exception", e)
            }


            /* val dialogPopup = DialogPilihKategoriObjek()
             dialogPopup.show(supportFragmentManager, "Dalog_popup")*/

        }

        //getPresenter()?.getdatadetaiLTanah(dataconvert.projectLandId!!, dataconvert.projectPartyId!!, Preference.accessToken)
    }

    private fun getPresenter(): DetailTanahPresenter? {
        detailTanahPresenter = DetailTanahPresenter()
        detailTanahPresenter.onAttach(this)
        return detailTanahPresenter
    }

    private fun setFragment(fragment: androidx.fragment.app.Fragment?, fragmentTag: String) {

        if (fragment != null) {
            refreshBackStack()
            val transact = supportFragmentManager.beginTransaction()
            transact.setCustomAnimations(R.anim.fragment_enter_from_right, R.anim.fragment_exit_to_left, R.anim.fragment_enter_from_left, R.anim.fragment_exit_to_right)
            transact.replace(R.id.flmainObjekBangunan2, fragment, fragmentTag).commit()
        }

    }

    private fun refreshBackStack() {
        supportFragmentManager.popBackStack(null, androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    /* override fun onsuccesgetdatadetaiLTanah(responseDataDetailTanah: ResponseDataDetailTanah) {

         this.dataDetailTanah = responseDataDetailTanah

     }*/

    /* override fun onSendFragment(fragment: Fragment, fragmentTag: String, codestatusCurrent: Int, namestatusCurrent: String) {
         setFragment(fragment, fragmentTag)
         codeKategoriObjek = codestatusCurrent
         nameKategoriObjek = namestatusCurrent
         tvcurrentMenuDetailKategori.text = namestatusCurrent

     }*/

    override fun onSendFragmentBangunan(fragment: androidx.fragment.app.Fragment, fragmentTag: String, codestatusCurrent: Int, namestatusCurrent: String) {
        setFragment(fragment, fragmentTag)
        codeKategoriObjek = codestatusCurrent
        nameKategoriObjek = namestatusCurrent
        tvstatuscurrentmenuObjekBangunan2.text = namestatusCurrent
    }


    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {
        supportFragmentManager.removeFragment(tag = tag)
    }
}