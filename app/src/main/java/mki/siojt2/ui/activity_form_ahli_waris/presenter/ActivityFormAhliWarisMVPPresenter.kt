package mki.siojt2.ui.activity_form_ahli_waris.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface ActivityFormAhliWarisMVPPresenter : MVPPresenter {

    fun postaddAhliWaris(userId: Int, projectId: Int, landId: String, partyOwnerType: String,
                            projectPartyType: String, partyIdentitySourceId: String,
                            partyIdentitySourceOther: String, partyFullName: String,
                            partyBirthPlaceCode: String, partyBirthPlaceOther: String,
                            partyBirthDate: String, artyOccupationId: String,
                            partyOccupationOther: String, partyProvinceCode: String,
                            partyRegencyCode: String, partyDistrictCode: String,
                            partyVillageName: String, partyRT: String,
                            partyRW: String, partyNumber: String, partyBlock: String,
                            partyStreetName: String, partyComplexName: String,
                            partyPostalCode: String, partyNPWP: String,
                            partyYearStayBegin: String, partyIdentityNumber: String,
                            partyLivelihoodLivelihoodId: ArrayList<String>?, partyLivelihoodLivelihoodOther: ArrayList<String>?, partyLivelihoodIncome: ArrayList<String>?)

}