package mki.siojt2.ui.activity_form_ahli_waris.presenter

import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.ui.activity_form_ahli_waris.AcitivtyFormAhliWarisView
import mki.siojt2.utils.ErrorHandler
import mki.siojt2.utils.rxJava.RxUtils

class ActivityFormAhliWarisPresenter : BasePresenter(), ActivityFormAhliWarisMVPPresenter {

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun postaddAhliWaris(userId: Int, projectId: Int, landId: String, partyOwnerType: String, projectPartyType: String, partyIdentitySourceId: String, partyIdentitySourceOther: String, partyFullName: String, partyBirthPlaceCode: String, partyBirthPlaceOther: String, partyBirthDate: String, artyOccupationId: String, partyOccupationOther: String, partyProvinceCode: String, partyRegencyCode: String, partyDistrictCode: String, partyVillageName: String, partyRT: String, partyRW: String, partyNumber: String, partyBlock: String, partyStreetName: String, partyComplexName: String, partyPostalCode: String, partyNPWP: String, partyYearStayBegin: String, partyIdentityNumber: String, partyLivelihoodLivelihoodId: ArrayList<String>?, partyLivelihoodLivelihoodOther: ArrayList<String>?, partyLivelihoodIncome: ArrayList<String>?) {
        view().onShowLoading()
        compositeDisposable.add(
                dataManager.postaddParties(userId, projectId, landId, partyOwnerType, projectPartyType, partyIdentitySourceId, partyIdentitySourceOther,
                        partyFullName, partyBirthPlaceCode, partyBirthPlaceOther, partyBirthDate, artyOccupationId, partyOccupationOther,
                        partyProvinceCode, partyRegencyCode, partyDistrictCode, partyVillageName, partyRT, partyRW, partyNumber, partyBlock,
                        partyStreetName, partyComplexName, partyPostalCode, partyNPWP, partyYearStayBegin, partyIdentityNumber, partyLivelihoodLivelihoodId,
                        partyLivelihoodLivelihoodOther, partyLivelihoodIncome)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessaddAhliWaris()
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }


    private fun view(): AcitivtyFormAhliWarisView {
        return getView() as AcitivtyFormAhliWarisView
    }
}