package mki.siojt2.ui.activity_form_ahli_waris

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.core.content.ContextCompat
import android.util.Log
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_form_subjek.*
import kotlinx.android.synthetic.main.toolbar_form.*
import mki.siojt2.fragment.form_subjek.*
import mki.siojt2.model.data_form_subjek.DataFormSubjek1
import mki.siojt2.model.data_form_subjek.DataFormSubjek2
import mki.siojt2.model.data_form_subjek.DataFormSubjek3
import mki.siojt2.persistence.Preference
import mki.siojt2.utils.Utils
import mki.siojt2.utils.JsonDateDeserializer
import com.google.gson.GsonBuilder
import io.realm.Realm
import io.realm.RealmList
import mki.siojt2.model.localsave.AhliWaris
import mki.siojt2.model.localsave.PartyAddImage
import mki.siojt2.model.localsave.Session
import mki.siojt2.ui.activity_form_ahli_waris.presenter.ActivityFormAhliWarisPresenter
import mki.siojt2.utils.realm.RealmController
import java.util.*


class ActivityFormAhliWaris : BaseActivity(), AcitivtyFormAhliWarisView, FormSubjek1.OnStepOneListener, FormSubjek2.OnStepTwoListener,
        FormSubjek3.OnStepThreeListener, FormSubjek4.OnStepFourListener {

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityFormAhliWaris::class.java)
        }
    }

    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * [FragmentPagerAdapter] derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v13.app.FragmentStatePagerAdapter].
     */
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    private lateinit var activityformahliWarisPresenter: ActivityFormAhliWarisPresenter
    private lateinit var realm: Realm
    private lateinit var session: Session

    /**
     * The [ViewPager] that will host the section contents.
     */
    //private var mViewPager: NonSwipeableViewPager? = null

    //private val stepperIndicator: StepperIndicator? = null
    var projectId: Int = 0
    var landId: Int = 0
    var landIdSub: Int = 0
    var statusPost = 1
    var dataId = ""

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_subjek)
        session = Session(this)

        val historyData = intent?.extras?.getString("data_current_tanah")

        val gson = GsonBuilder().registerTypeAdapter(Date::class.java, JsonDateDeserializer()).create()
        val toJson = Gson().fromJson(historyData, Array<String>::class.java).toList()
        //val turnsType = object : TypeToken<ResponseDataListProject>() {}.type
        //val dataconvert = gson.fromJson<ResponseDataListProject>(historyData, turnsType)

        projectId = toJson[1].toInt()
        landId = toJson[0].toInt()
        landIdSub = toJson[3].toInt()
        statusPost = toJson[4].toInt()
        dataId = toJson[5]
        /*Log.d("pemilik1", historyData.toString())
        Log.d("pemilik1", dataconvert.toString())*/

        /* set toolbar*/
        if (toolbarCustom != null) {
            setSupportActionBar(toolbarCustom)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarCustom.setNavigationOnClickListener {
            dialogToHandleExitFromCurrentPage()
        }

        if (session.status == "2") {
            tvFormToolbarTitle.text = "Edit data ahli waris"
        } else {
            tvFormToolbarTitle.text = "Tambah data ahli waris"
        }

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)
        mSectionsPagerAdapter!!.addFrag(FormSubjek1.newInstance(landId.toString(), ""))
        mSectionsPagerAdapter!!.addFrag(FormSubjek2.newInstance("1", "1"))
        mSectionsPagerAdapter!!.addFrag(FormSubjek3.newInstance("1", ""))

        // Set up the ViewPager with the sections adapter.
        viewpagerformSubjek.adapter = mSectionsPagerAdapter
        stepperIndicator.showLabels(true)
        stepperIndicator.setViewPager(viewpagerformSubjek)

        // or keep last page as "end page"
        stepperIndicator.setViewPager(viewpagerformSubjek, viewpagerformSubjek.adapter!!.count - 1) //}

        viewpagerformSubjek.setOnPageChangeListener(object : androidx.viewpager.widget.ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                mSectionsPagerAdapter!!.notifyDataSetChanged()
            }
        })

        realm = RealmController.with(this).realm
        realm.isAutoRefresh
    }

    private fun getPresenter(): ActivityFormAhliWarisPresenter? {
        activityformahliWarisPresenter = ActivityFormAhliWarisPresenter()
        activityformahliWarisPresenter.onAttach(this)
        return activityformahliWarisPresenter
    }

    private fun dialogsuccessTransaksi(title_dialog: String) {
        Utils.deletedataform1(this@ActivityFormAhliWaris)
        Utils.deletedataform3(this@ActivityFormAhliWaris)
        Utils.deletedataform3(this@ActivityFormAhliWaris)
        val dialogPopup = DialogSuccessAddSubjek.newInstance(title_dialog)
        dialogPopup.show(supportFragmentManager, "Dalog_popup")
    }

    private fun dialogToHandleExitFromCurrentPage() {
        val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> {
                    dialog.dismiss()
                    super.onBackPressed()
                    session.setIdAndStatus("", "", "")
                    finish()
                }

                DialogInterface.BUTTON_NEGATIVE -> {
                    dialog.dismiss()
                }
            }
        }

        MaterialAlertDialogBuilder(this, R.style.CustomAlertDialogMaterialTheme)
                .setTitle(getString(R.string.app_name))
                .setIcon(R.mipmap.ic_launcher)
                .setMessage("Data yang telah dibuat akan hilang, yakin keluar?")
                .setCancelable(false)
                .setPositiveButton("KELUAR", dialogClickListener)
                .setNegativeButton("BATAL", dialogClickListener)
                .show()
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    class SectionsPagerAdapter(fm: androidx.fragment.app.FragmentManager) : androidx.fragment.app.FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        private val mFragmentList = arrayListOf<Fragment>()


        fun addFrag(fragment: Fragment) {
            mFragmentList.add(fragment)
        }

        override fun getCount(): Int {
            return 3
        }

        override fun getItem(position: Int): androidx.fragment.app.Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            /*when (position) {
                0 -> return FormSubjek1.newInstance("1", "")
                1 -> return FormSubjek2.newInstance("", "")
                2 -> return FormSubjek3.newInstance("", "")
            }*/
            return mFragmentList[position]
        }

        override fun getPageTitle(position: Int): CharSequence? {
            when (position) {
                0 -> return "Data Ahli Waris"
                1 -> return "Dokumen (kelengkapan)"
                2 -> return "Catatan"
            }
            return null
        }
    }

    override fun onsuccessaddAhliWaris() {
        Utils.deletedataform1(this@ActivityFormAhliWaris)
        Utils.deletedataform3(this@ActivityFormAhliWaris)
        Utils.deletedataform3(this@ActivityFormAhliWaris)
        val dialogPopup = DialogSuccessAddSubjek()
        dialogPopup.show(supportFragmentManager, "Dalog_popup")
    }

    override fun gotomainSuccess() {

    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    override fun onBackPressed(fragment: androidx.fragment.app.Fragment) {
        mSectionsPagerAdapter!!.notifyDataSetChanged()
        when (fragment) {
            is FormSubjek2 -> viewpagerformSubjek.setCurrentItem(0, true)
            is FormSubjek3 -> viewpagerformSubjek.setCurrentItem(1, true)
        }
    }

    override fun onNextPressed(fragment: androidx.fragment.app.Fragment) {
        mSectionsPagerAdapter!!.notifyDataSetChanged()
        when (fragment) {
            is FormSubjek1 -> viewpagerformSubjek.setCurrentItem(1, true)
            is FormSubjek2 -> viewpagerformSubjek.setCurrentItem(2, true)
            is FormSubjek3 -> {
                /*val intent = ActicitySuccessAddSubjek.getStartIntent(this@ActivityFormSubjek)
                startActivity(intent)
                finish()*/
                val turnsType = object : TypeToken<MutableList<DataFormSubjek1>>() {}.type
                val dataconvert = Gson().fromJson<MutableList<DataFormSubjek1>>(Utils.getdataform1(this), turnsType)

                val turnsType2 = object : TypeToken<MutableList<DataFormSubjek2>>() {}.type
                val dataconvert2 = Gson().fromJson<MutableList<DataFormSubjek2>>(Utils.getdataform2(this), turnsType2)

                val turnsType3 = object : TypeToken<MutableList<DataFormSubjek3>>() {}.type
                val dataconvert3 = Gson().fromJson<MutableList<DataFormSubjek3>>(Utils.getdataform3(this), turnsType3)

                /*-------- Edit -----*/
                if (statusPost == 2) {
                    realm.executeTransactionAsync({inrealm ->
                        val party = inrealm.where(AhliWaris::class.java).equalTo("AhliWarisIdTemp", dataId).findFirst()
//                        party!!.partyIdentitySourceId = dataconvert2[0].partyIdentitySourceId?.toInt()
//                        party.partyIdentitySourceName = dataconvert2[0].partyIdentitySourceName.toString()
//                        if (dataconvert[0].partyOwnerType!!.isNotEmpty()) {
//                            party.partyOwnerType = dataconvert[0].partyOwnerType?.toInt()
//                        }
//                        party.partyIdentitySourceOther = dataconvert2[0].partyIdentitySourceOther.toString()
                        party!!.partyFullName = dataconvert[0].partyFullName.toString()
                        party.partyBirthPlaceCode = dataconvert[0].partyBirthPlaceCode.toString()
                        party.partyBirthPlaceName = dataconvert[0].partyBirthPlaceName.toString()
                        party.partyBirthPlaceOther = dataconvert[0].partyBirthPlaceOther.toString()
                        party.partyBirthDate = dataconvert[0].partyBirthDate.toString()
                        party.partyOccupationId = dataconvert[0].partyOccupationId.toString()
                        party.partyOccupationName = dataconvert[0].partyOccupationName.toString()
                        party.partyOccupationOther = dataconvert[0].partyOccupationOther.toString()
                        party.partyProvinceCode = dataconvert[0].partyProvinceCode.toString()
                        party.partyProvinceName = dataconvert[0].partyProvinceName.toString()
                        party.partyRegencyCode = dataconvert[0].partyRegencyCode.toString()
                        party.partyRegencyName = dataconvert[0].partyRegencyName.toString()
                        party.partyDistrictCode = dataconvert[0].partyDistrictCode.toString()
                        party.partyDistrictName = dataconvert[0].partyDistrictName.toString()
                        party.partyVillageId = dataconvert[0].partyVillageName.toString()
                        party.partyVillageName = dataconvert[0].partyVillage.toString()
                        party.partyRT = dataconvert[0].partyRT.toString()
                        party.partyRW = dataconvert[0].partyRW.toString()
                        party.partyNumber = dataconvert[0].partyNumber.toString()
                        party.partyBlock = dataconvert[0].partyBlock.toString()
                        party.partyStreetName = dataconvert[0].partyStreetName.toString()
                        party.partyComplexName = dataconvert[0].partyComplexName.toString()
                        party.partyPostalCode = dataconvert[0].partyPostalCode.toString()
                        party.partyNPWP = dataconvert2[0].partyNPWP.toString()
                        party.partyYearStayBegin = dataconvert2[0].partyYearStayBegin?.toInt()
                        party.partyIdentityNumber = dataconvert[0].nik.toString()
                        party.partyPhone = dataconvert[0].phone
                        party.partyEmail = dataconvert[0].email
                        party.partyOwnershipLandData = dataconvert2[0].ownershipLand
                        party.partyOwnershipType = dataconvert2[0].ownershipType
                        party.partyNotes = dataconvert3[0].notes
                        party.filePaths = dataconvert2[0].filePaths
                        party.fileName = dataconvert2[0].fileName
//                        party.partyIdentityNumber = dataconvert2[0].partyIdentityNumber.toString()
//                        party.livelihoodLiveId = dataconvert3[0].partyLivelihoodLivelihoodId.toString()
//                        party.livelihoodLiveName = dataconvert3[0].partyLivelihoodName.toString()
//                        party.livelihoodOther = dataconvert3[0].partyLivelihoodLivelihoodOther.toString()
//                        party.livelihoodIncome = dataconvert3[0].partyLivelihoodIncome.toString()
                        party.partyStatus = 1
                        party.partyCreateBy = Preference.auth.dataUser?.mobileUserId!!
                        party.isCreate = 1



                        val resultdataImageBuilding = inrealm.where(PartyAddImage::class.java).findAll()
                        val dataimageBuilding = resultdataImageBuilding.where().equalTo("imageId", session!!.id!!).findAll()
                        dataimageBuilding?.deleteAllFromRealm()

                        val dataImagePlusId: MutableList<PartyAddImage> = ArrayList()
                        for (element in dataconvert2[0].image.indices) {
                            if (!dataconvert2[0].image[element]?.imagepathLand.isNullOrEmpty() && !dataconvert2[0].image[element]?.imagenameLand.isNullOrEmpty()) {
                                val dataImage = PartyAddImage()
                                dataImage.imageId = session.id!!
                                dataImage.imagepathLand = dataconvert2[0].image[element]?.imagepathLand
                                dataImage.imagenameLand = dataconvert2[0].image[element]?.imagenameLand
                                dataImagePlusId.add(dataImage)
                            }
                            //Log.d("hasil", dataconvert2[0].image[element].imageId.toString())
                            //Log.d("hasil", dataconvert2[0].image[element].imagenameLand.toString())
                        }
                        if (dataImagePlusId.isNotEmpty()) {
                            val dataPhotoBangunan = inrealm.copyToRealmOrUpdate(dataImagePlusId)
                            val dataPhotoBangunan2: RealmList<PartyAddImage> = RealmList()
                            for (element in dataPhotoBangunan) {
                                dataPhotoBangunan2.add(element)
                            }
                            party.partyAddImage = dataPhotoBangunan2
                        }
                    }, {
                        Log.d("ahli_waris", "success update ahli_waris")
                        runOnUiThread {
                            onDismissLoading()
                            dialogsuccessTransaksi("""
                                Data
                                berhasil
                                diperbaharui
                            """.trimIndent())
                        }
                    }, {
                        Log.d("ahli_waris", "failed update ahliw_waris")

                    })

                }
                else {
                    /*-------- Save Into Realm -----*/
                    realm.executeTransactionAsync({inrealm ->
                        val currentIdNum = inrealm.where(AhliWaris::class.java).max("AhliWarisIdTemp")
                        val nextId: Int
                        nextId = if (currentIdNum == null) {
                            1
                        } else {
                            currentIdNum.toInt() + 1

                        }
                        val party = inrealm.createObject(AhliWaris::class.java)
                        party.ahliWarisIdTemp = nextId
                        party.ahliwarisId = null
                        party.projectId = projectId
                        party.landIdTemp = landId
                        party.landIdTempSub = landIdSub
                        party.landId = null
                        party.ahliWarisProjectPartyType = "9"

//                        party.partyIdentitySourceId = dataconvert2[0].partyIdentitySourceId?.toInt()
//                        party.partyIdentitySourceName = dataconvert2[0].partyIdentitySourceName.toString()
//                        if (dataconvert[0].partyOwnerType!!.isNotEmpty()) {
//                            party.partyOwnerType = dataconvert[0].partyOwnerType?.toInt()
//                        }
//                        party.partyIdentitySourceOther = dataconvert2[0].partyIdentitySourceOther.toString()
                        party.partyFullName = dataconvert[0].partyFullName.toString()
                        party.partyBirthPlaceCode = dataconvert[0].partyBirthPlaceCode.toString()
                        party.partyBirthPlaceName = dataconvert[0].partyBirthPlaceName.toString()
                        party.partyBirthPlaceOther = dataconvert[0].partyBirthPlaceOther.toString()
                        party.partyBirthDate = dataconvert[0].partyBirthDate.toString()
                        party.partyOccupationId = dataconvert[0].partyOccupationId.toString()
                        party.partyOccupationName = dataconvert[0].partyOccupationName.toString()
                        party.partyOccupationOther = dataconvert[0].partyOccupationOther.toString()
                        party.partyProvinceCode = dataconvert[0].partyProvinceCode.toString()
                        party.partyProvinceName = dataconvert[0].partyProvinceName.toString()
                        party.partyRegencyCode = dataconvert[0].partyRegencyCode.toString()
                        party.partyRegencyName = dataconvert[0].partyRegencyName.toString()
                        party.partyDistrictCode = dataconvert[0].partyDistrictCode.toString()
                        party.partyDistrictName = dataconvert[0].partyDistrictName.toString()
                        party.partyVillageId = dataconvert[0].partyVillageName.toString()
                        party.partyVillageName = dataconvert[0].partyVillage.toString()
                        party.partyRT = dataconvert[0].partyRT.toString()
                        party.partyRW = dataconvert[0].partyRW.toString()
                        party.partyNumber = dataconvert[0].partyNumber.toString()
                        party.partyBlock = dataconvert[0].partyBlock.toString()
                        party.partyStreetName = dataconvert[0].partyStreetName.toString()
                        party.partyComplexName = dataconvert[0].partyComplexName.toString()
                        party.partyPostalCode = dataconvert[0].partyPostalCode.toString()
                        party.partyNPWP = dataconvert2[0].partyNPWP.toString()
                        party.partyYearStayBegin = dataconvert2[0].partyYearStayBegin?.toInt()
                        party.partyIdentityNumber = dataconvert[0].nik.toString()
                        party.partyPhone = dataconvert[0].phone
                        party.partyEmail = dataconvert[0].email
                        party.partyOwnershipLandData = dataconvert2[0].ownershipLand
                        party.partyOwnershipType = dataconvert2[0].ownershipType
                        party.partyNotes = dataconvert3[0].notes
                        party.filePaths = dataconvert2[0].filePaths
                        party.fileName = dataconvert2[0].fileName
//                        party.livelihoodLiveId = dataconvert3[0].partyLivelihoodLivelihoodId.toString()
//                        party.livelihoodLiveName = dataconvert3[0].partyLivelihoodName.toString()
//                        party.livelihoodOther = dataconvert3[0].partyLivelihoodLivelihoodOther.toString()
//                        party.livelihoodIncome = dataconvert3[0].partyLivelihoodIncome.toString()
                        party.partyStatus = 1
                        party.partyCreateBy = Preference.auth.dataUser?.mobileUserId!!

                        val dataImagePlusId: MutableList<PartyAddImage> = ArrayList()
                        for (element in dataconvert2[0].image.indices) {
                            if (!dataconvert2[0].image[element]?.imagepathLand.isNullOrEmpty() && !dataconvert2[0].image[element]?.imagenameLand.isNullOrEmpty()) {
                                val dataImage = PartyAddImage()
                                dataImage.imageId = session.id!!
                                dataImage.imagepathLand = dataconvert2[0].image[element]?.imagepathLand
                                dataImage.imagenameLand = dataconvert2[0].image[element]?.imagenameLand
                                dataImagePlusId.add(dataImage)
                            }
                        }
                        if (dataImagePlusId.isNotEmpty()) {
                            val dataPhotoBangunan = inrealm.copyToRealmOrUpdate(dataImagePlusId)
                            val dataPhotoBangunan2: RealmList<PartyAddImage> = RealmList()
                            for (element in dataPhotoBangunan) {
                                dataPhotoBangunan2.add(element)
                            }
                            party.partyAddImage = dataPhotoBangunan2
                        }
                    }, {
                        Log.d("ahli_waris", "success add ahli_waris")
                        runOnUiThread {
                            onDismissLoading()
                            onsuccessaddAhliWaris()
                        }
                    }, {
                        Log.d("ahli_waris", "failed add ahliw_waris")

                    })

                }
//                getPresenter()?.postaddAhliWaris(
//                        Preference.auth.dataUser?.mobileUserId!!,
//                        projectId,
//                        "",
//                        dataconvert[0].partyOwnerType.toString(),
//                        "1",
//                        dataconvert2[0].partyIdentitySourceId.toString(),
//                        dataconvert2[0].partyIdentitySourceOther.toString(),
//                        dataconvert[0].partyFullName.toString(), dataconvert[0].partyBirthPlaceCode.toString(),
//                        dataconvert[0].partyBirthPlaceOther.toString(), dataconvert[0].partyBirthDate.toString(),
//                        dataconvert[0].partyOccupationId.toString(), dataconvert[0].partyOccupationOther.toString(),
//                        dataconvert[0].partyProvinceCode.toString(), dataconvert[0].partyRegencyCode.toString(),
//                        dataconvert[0].partyDistrictCode.toString(), dataconvert[0].partyVillageName.toString(),
//                        dataconvert[0].partyRT.toString(), dataconvert[0].partyRW.toString(),
//                        dataconvert[0].partyNumber.toString(), dataconvert[0].partyBlock.toString(),
//                        dataconvert[0].partyStreetName.toString(), dataconvert[0].partyComplexName.toString(),
//                        dataconvert[0].partyPostalCode.toString(), dataconvert2[0].partyNPWP.toString(),
//                        dataconvert2[0].partyYearStayBegin.toString(), dataconvert2[0].partyIdentityNumber.toString(),
//                        dataconvert3[0].partyLivelihoodLivelihoodId, dataconvert3[0].partyLivelihoodLivelihoodOther,
//                        dataconvert3[0].partyLivelihoodIncome
//
//                )

                /*Log.d("babab", dataconvert.toString())
                Toast.makeText(this, dataconvert.toString(), Toast.LENGTH_SHORT).show()*/
            }
        }
    }

    override fun onBackPressed() {
        dialogToHandleExitFromCurrentPage()
    }

    override fun onDestroy() {
        session.setIdAndStatus("", "", "")
        super.onDestroy()
        //getPresenter()?.onDetach()
    }


}