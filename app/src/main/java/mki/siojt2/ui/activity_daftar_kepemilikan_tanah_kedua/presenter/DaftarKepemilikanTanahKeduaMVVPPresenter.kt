package mki.siojt2.ui.activity_daftar_kepemilikan_tanah_kedua.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface DaftarKepemilikanTanahKeduaMVVPPresenter : MVPPresenter {

    fun getdaftarkepemilikanTanahKedua(projectId: Int, isFirstParty: Int, landId: Int, accessToken: String)
}