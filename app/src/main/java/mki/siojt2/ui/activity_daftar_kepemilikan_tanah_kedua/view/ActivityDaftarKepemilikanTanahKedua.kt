package mki.siojt2.ui.activity_daftar_kepemilikan_tanah_kedua.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.widget.LinearLayout
import com.google.gson.Gson
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_daftar_tanah_kepemilikan_kedua.*
import kotlinx.android.synthetic.main.toolbar_with_sub_title.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.model.ResponDataObjekKepemilikanKedua
import mki.siojt2.model.ResponseDataListPemilik
import mki.siojt2.model.localsave.Subjek2
import mki.siojt2.ui.activity_daftar_kepemilikan_tanah_kedua.presenter.DaftarKepemilikanTanahKeduaPresenter
import mki.siojt2.ui.activity_daftar_kepemilikan_tanah_kedua.recycleview_adapter.AdapterDaftarKepemilikanTanahKedua
import mki.siojt2.ui.activity_form_subjek_2.ActivityFormSubjek2
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.Utils


class ActivityDaftarKepemilikanTanahKedua : BaseActivity(), DaftarKepemilikanTanahKeduaView, AdapterDaftarKepemilikanTanahKedua.Callback {

    lateinit var daftarKepemilikanTanahKeduaPresenter: DaftarKepemilikanTanahKeduaPresenter

    private lateinit var adapterdaftaKepemilikanTanahKedua: AdapterDaftarKepemilikanTanahKedua

    private var datalisObjek: MutableList<ResponDataObjekKepemilikanKedua>? = ArrayList()

    private var mprojectPartyProjectId: Int? = 0
    private var mprojectLandId: Int? = 0

    private lateinit var realm:Realm

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityDaftarKepemilikanTanahKedua::class.java)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daftar_tanah_kepemilikan_kedua)


        val historyData = intent?.extras?.getString("data_current_tanah")
        val toJson = Gson().fromJson(historyData , Array<String>::class.java).toList()


//        val turnsType = object : TypeToken<JsonLand>() {}.type
//        val dataconvert = Gson().fromJson<JsonLand>(historyData, turnsType)
//
//        Log.e("historyData",dataconvert.toString())
        mprojectPartyProjectId = toJson[1].toInt()
        mprojectLandId = toJson[0].toInt()

        /* set toolbar*/
        if (toolbarCustom != null) {
            setSupportActionBar(toolbarCustom)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarCustom.setNavigationOnClickListener {
            finish()
        }


        tvToolbarTitle.text = toJson[2]
        tvToolbarSubTitle.text = "Kepemilikan Tanah Ke-2"


        datalisObjek?.add(ResponDataObjekKepemilikanKedua("1", "Sdr. Yayan Sudrajat",
                "Sdr. Yayan Sudrajat", "Kel. Sindangparit Kec. Mandalajati Kab. Bandung"))
        datalisObjek?.add(ResponDataObjekKepemilikanKedua("2", "Sdr. Wawan Sudrajat",
                "Sdr. Tatang Sudrajat", "Kel. Cimahi Tengah"))
        datalisObjek?.add(ResponDataObjekKepemilikanKedua("2", "Sdr. Wawan Sudrajat",
                "Sdr. Tatang Sudrajat", "Kel. Cimahi Utara"))

        adapterdaftaKepemilikanTanahKedua = AdapterDaftarKepemilikanTanahKedua(this)
        adapterdaftaKepemilikanTanahKedua.setCallback(this)
        rvObjekKepemilikanKedua.let {
            it.layoutManager = LinearLayoutManager(this@ActivityDaftarKepemilikanTanahKedua, LinearLayout.VERTICAL, false)
            it.setHasFixedSize(true)
            it.itemAnimator = DefaultItemAnimator()
            it.adapter = adapterdaftaKepemilikanTanahKedua
        }

        btnaddObjekTanahObjekKepemilikanKedua.setSafeOnClickListener {
            Utils.deletedataform1(this@ActivityDaftarKepemilikanTanahKedua)
            Utils.deletedataform2(this@ActivityDaftarKepemilikanTanahKedua)
            Utils.deletedataform3(this@ActivityDaftarKepemilikanTanahKedua)
            val intent = ActivityFormSubjek2.getStartIntent(this@ActivityDaftarKepemilikanTanahKedua)
            intent.putExtra("data_current_tanah", historyData)
            startActivity(intent)
        }

        getData(mprojectLandId.toString())
        /* get data api */
        //getPresenter()?.getdaftarkepemilikanTanahKedua(dataconvert.projectPartyProjectId!!, 2, dataconvert.projectLandId!!, Preference.accessToken)

    }

    private fun getData(historyData: String?) {
        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        val results = realm.where(Subjek2::class.java).equalTo("LandIdTemp",historyData?.toInt()).findAll()
        Log.e("result",results.toString())
        if (results != null) {
            adapterdaftaKepemilikanTanahKedua.clear()
            adapterdaftaKepemilikanTanahKedua.addItems(results)
            adapterdaftaKepemilikanTanahKedua.notifyDataSetChanged()
        }else{
            Log.e("data","kosong")
        }
    }

//    private fun getPresenter(): DaftarKepemilikanTanahKeduaPresenter? {
//        daftarKepemilikanTanahKeduaPresenter = DaftarKepemilikanTanahKeduaPresenter()
//        daftarKepemilikanTanahKeduaPresenter.onAttach(this)
//        return daftarKepemilikanTanahKeduaPresenter
//    }

    override fun onsuccessgetlistdataPemilikKedua(responseDataListPemilikKedua: MutableList<ResponseDataListPemilik>) {
//        adapterdaftaKepemilikanTanahKedua.clear()
//        adapterdaftaKepemilikanTanahKedua.addItems(responseDataListPemilikKedua)
    }

    override fun onRepoEmptyViewRetryClick() {

    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    override fun onRestart() {
        super.onRestart()
        //getPresenter()?.getdaftarkepemilikanTanahKedua(mprojectPartyProjectId!!, 2, mprojectLandId!!, Preference.accessToken)
        getData(mprojectLandId.toString())

    }

}