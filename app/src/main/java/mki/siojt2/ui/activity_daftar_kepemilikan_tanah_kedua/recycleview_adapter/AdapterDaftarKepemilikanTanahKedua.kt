package mki.siojt2.ui.activity_daftar_kepemilikan_tanah_kedua.recycleview_adapter

import android.annotation.SuppressLint
import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import io.realm.RealmResults
import kotlinx.android.synthetic.main.item_daftar_subjek_vertical_2.view.*
import mki.siojt2.R
import mki.siojt2.base.BaseViewHolder
import mki.siojt2.model.localsave.Subjek2
import mki.siojt2.ui.activity_detail_subjek2.ActivityDetailSubjek2
import mki.siojt2.utils.extension.OnSingleClickListener


class AdapterDaftarKepemilikanTanahKedua(context: Context) : androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder>() {

    private val mContext: Context? = context
    val mDataset: MutableList<Subjek2>? = ArrayList()
    var rowLayout = R.layout.item_daftar_subjek_vertical_2
    var emptyrowLayout = R.layout.item_empty_view
    var pos = 0


    private var mCallback: Callback? = null

    fun setCallback(callback: Callback) {
        mCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            VIEW_TYPE_NORMAL -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(rowLayout, parent, false))
            VIEW_TYPE_EMPTY -> EmptyViewHolder(
                    LayoutInflater.from(parent.context).inflate(R.layout.item_empty_view, parent, false))
            else -> EmptyViewHolder(LayoutInflater.from(parent.context).inflate(emptyrowLayout, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (mDataset != null && mDataset.isNotEmpty()) {
            VIEW_TYPE_NORMAL
        } else {
            VIEW_TYPE_EMPTY
        }
    }

    override fun getItemCount(): Int {
        return if (mDataset!!.isNotEmpty()) {
            mDataset.size
        } else {
            1
        }
    }

    /*inner class RuangViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var r_no: TextView = itemView.findViewById(R.id.rowNo) as TextView
        var r_tvTitle: TextView = itemView.findViewById(R.id.tvTitle) as TextView
        var r_tvContent1: TextView = itemView.findViewById(R.id.tvContent1) as TextView
        var r_tvContent2: TextView = itemView.findViewById(R.id.tvContent2) as TextView

    }*/

    fun clear() {
        this.mDataset!!.clear()
    }

    fun addItems(data: RealmResults<Subjek2>) {
        this.mDataset!!.addAll(data)
        notifyDataSetChanged()
    }

    interface Callback {
        fun onRepoEmptyViewRetryClick()
    }

    inner class ViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }

        @SuppressLint("SetTextI18n")
        override fun onBind(position: Int) {
            super.onBind(position)

            val dataList = mDataset!![position]

            inflateData(dataList)

            //val pos = pos + position + 1
            itemView.tvnamaSubjek2.text = dataList.subjek2FullName
            itemView.tvalamat1Subjek2.text = "${dataList.subjek2VillageName} ${dataList.subjek2StreetName} RT ${dataList.subjek2RT} RW ${dataList.subjek2RW} No.${dataList.subjek2Number}"
            itemView.tvalamat2Subjek2.text = "${dataList.subjek2DistrictName} ${dataList.subjek2RegencyName} ${dataList.subjek2ProvinceName}"

            itemView.tvkodeposalamatSubjek2.text = dataList.subjek2PostalCode

            itemView.btndetailSelngkapnyaPemilikKedua.setOnClickListener(object : OnSingleClickListener() {
                override fun onSingleClick(v: View) {
                    /*val param = "['${dataList.subjekIdTemp}','${dataList.subjek2FullName}','${dataList.subjek2OccupationName}','${dataList.subjek2ComplexName}','${dataList.subjek2StreetName}','${dataList.subjek2Block}','${dataList.subjek2Number}','${dataList.subjek2VillageName}','${dataList.subjek2DistrictName}','${dataList.subjek2RegencyName}','${dataList.subjek2ProvinceName}','${dataList.subjek2PostalCode}','${dataList.subjek2IdentitySourceName}'," +
                            "'${dataList.subjek2IdentityNumber}','${dataList.subjek2NPWP}','${dataList.subjek2YearStayBegin}','${dataList.livelihoodLiveName}','${dataList.livelihoodIncome}','2','${dataList.landIdTemp}','${dataList.projectId}']"*/
                    val intent = ActivityDetailSubjek2.getStartIntent(mContext!!)
                    intent.putExtra("data_current_pemilik2", dataList.subjekIdTemp)
                    mContext.startActivity(intent)
                }
            })


        }

        private fun inflateData(dataList: Subjek2) {

        }
    }

    inner class EmptyViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }


    }

    companion object {
        const val VIEW_TYPE_EMPTY = 0
        const val VIEW_TYPE_NORMAL = 1
    }
}
