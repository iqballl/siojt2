package mki.siojt2.ui.activity_form_add_objek_room.presenter

interface FormAddObjekRoomMVPPresenter {
    fun postBuildingObjek(userId: Int, projectLandId: Int, projectPartyId: Int,
                          projectBuildingTypeId: Int, projectBuildingFloorTotal: Int,
                          projectBuildingAreaUpperRoom: String, projectBuildingAreaLowerRoom: String,
                          ceilingTypeId: ArrayList<String>, pbCeilingTypeAreaTotal: ArrayList<Float>,
                          doorWindowTypeld: ArrayList<String>, pbDoorWindowTypeAreaTotal: ArrayList<Float>,
                          floorTypeId: ArrayList<String>, pbFloorTypeAreaTotal: ArrayList<Float>,
                          foundationTypeId: ArrayList<String>, pbFoundationTypeAreaTotal: ArrayList<Float>,
                          roofCoveringTypeId: ArrayList<String>, pbRoofCoveringTypeAreaTotal: ArrayList<Float>,
                          roofFrameTypeId: ArrayList<String>, pbRoofFrameTypeAreaTotal: ArrayList<Float>,
                          structureTypeId: ArrayList<String>, pbStructureTypeAreaTotal: ArrayList<Float>,
                          wallTypeId: ArrayList<String>, pbWallTypeAreaTotal: ArrayList<Float>,
                          accessToken: String)
}