package mki.siojt2.ui.activity_form_add_objek_room.view

import mki.siojt2.base.view.MvpView

interface FormAddObjekRoomView: MvpView{
    fun onsuccesspostBuildingObjek()
}