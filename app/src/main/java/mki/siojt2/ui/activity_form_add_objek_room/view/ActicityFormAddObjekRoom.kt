package mki.siojt2.ui.activity_form_add_objek_room.view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.realm.Realm
import io.realm.RealmList
import kotlinx.android.synthetic.main.activity_form_add_objek.*
import kotlinx.android.synthetic.main.dialog_confirm_delete.view.*
import kotlinx.android.synthetic.main.toolbar_form.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.fragment.form_object_room.DialogSuccessAddRoom
import mki.siojt2.fragment.form_object_room.FormObjekRoom1
import mki.siojt2.fragment.form_object_room.FormObjekRoomPhoto
import mki.siojt2.model.UAVObjectsItem
import mki.siojt2.model.local_save_image.DataImageRoom
import mki.siojt2.model.localsave.Room
import mki.siojt2.model.localsave.Session
import mki.siojt2.persistence.Preference
import mki.siojt2.ui.activity_form_add_objek_room.presenter.FormAddObjekRoomPresenter
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.Utils

class ActicityFormAddObjekRoom : BaseActivity(), FormAddObjekRoomView, FormObjekRoom1.OnStepOneListener,
        FormObjekRoomPhoto.OnStepTwoListener{

    open var saveData: Room = Room()

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    lateinit var formAddObjekBangunanPresenter: FormAddObjekRoomPresenter

    private var projectLandId: Int? = 0
    private var partyTypdeId: Int? = 0

    private var LandIdTemp: Int? = 0
    var ProjectId: Int? = 0

    private lateinit var realm: Realm
    private lateinit var session: Session

    private lateinit var mdialogConfirmDelete: AlertDialog
    private lateinit var mDialogView: View
    private var sessionFlagsCRUD: mki.siojt2.model.Session? = null
    var selectedLinkedObjectUAVId: Int? = null
    var nextId: Int = 0

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActicityFormAddObjekRoom::class.java)
        }
    }

    @SuppressLint("SetTextI18n", "InflateParams")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_add_objek)

        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        session = Session(this)

        val historyData = intent?.extras?.getString("data_current_tanah")
        val toJson = Gson().fromJson(historyData, Array<String>::class.java).toList()

        val sessionData = intent?.extras?.getString("data_current_session")
        //val toJson = Gson().fromJson(historyData , Array<String>::class.java).toList()
        //val turnsType = object : TypeToken<ResponseDataListGedung>() {}.type
        //val dataconvert = Gson().fromJson<ResponseDataListGedung>(historyData, turnsType)

        if (sessionData != null) {
            val turnsType = object : TypeToken<mki.siojt2.model.Session>() {}.type
            sessionFlagsCRUD = Gson().fromJson<mki.siojt2.model.Session>(sessionData, turnsType)
            //Log.d("session", sessionFlagsCRUD!!.statusPost)

            if (sessionFlagsCRUD!!.statusPost == "2") {
                tvFormToolbarTitle.text = "Edit data objek - Ruang Atas dan Bawah Tanah"
            }
        } else {
            tvFormToolbarTitle.text = "Tambah data objek - Ruang Atas dan Bawah Tanah"
        }

        //projectLandId = dataconvert.projectLandId
        //partyTypdeId = dataconvert.projectPartyId

        LandIdTemp = toJson[0].toInt()
        ProjectId = toJson[1].toInt()

        /* set dialog confirm delete data */
        mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_confirm_delete, null)
        val mBuilder = AlertDialog.Builder(this)
        mdialogConfirmDelete = mBuilder.create()

        mdialogConfirmDelete.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        mdialogConfirmDelete.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mdialogConfirmDelete.setCancelable(false)
        val window = mdialogConfirmDelete.window
        if (window != null) {
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            mdialogConfirmDelete.window!!.setLayout(width, height)
        }
        mdialogConfirmDelete.setView(mDialogView)

        /* set toolbar*/
        if (toolbarCustom != null) {
            setSupportActionBar(toolbarCustom)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarCustom.setNavigationOnClickListener {
            dialogToHandleExitFromCurrentPage()
        }

        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager, 20, "")
        mSectionsPagerAdapter!!.addFrag(FormObjekRoom1.newInstance(LandIdTemp!!, projectLandId!!))
        mSectionsPagerAdapter!!.addFrag(FormObjekRoomPhoto.newInstance("", ""))

        // Set up the ViewPager with the sections adapter.
        viewpagerObjekTanah.adapter = mSectionsPagerAdapter
        stepperIndicatorObjekTanah.showLabels(true)
        stepperIndicatorObjekTanah.setViewPager(viewpagerObjekTanah)

        // or keep last page as "end page"
        stepperIndicatorObjekTanah.setViewPager(viewpagerObjekTanah, viewpagerObjekTanah.adapter!!.count - 1) //}

        viewpagerObjekTanah.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                mSectionsPagerAdapter!!.notifyDataSetChanged()
            }
        })

        showDialog()
    }

    private fun showDialog() {
        if (sessionFlagsCRUD != null) {
            if (sessionFlagsCRUD!!.id != null && sessionFlagsCRUD!!.statusPost == "3") {
                mDialogView.btnYesDelete.setOnClickListener {
                    realm.executeTransactionAsync({ inRealm ->
                        val resultRealmBanguanan = inRealm.where(Room::class.java).findAll()
                        val dataParty = resultRealmBanguanan.where()
                                .equalTo("roomId", sessionFlagsCRUD!!.id!!)
                                .findFirst()
                        val resultdataImageBuilding = inRealm.where(DataImageRoom::class.java).findAll()
                        val dataimageBuilding = resultdataImageBuilding.where().equalTo("imageId", sessionFlagsCRUD!!.id!!).findAll()
                        dataimageBuilding?.deleteAllFromRealm()
                        dataParty?.deleteFromRealm()
                    }, {
                        Log.d("delete", "onSuccess : delete single object")
                        mdialogConfirmDelete.dismiss()
                        finish()
                    }, {
                        Log.d("delete", "onFailed : ${it.localizedMessage}")
                        Log.d("delete", "onFailed : delete single object")
                    })
                }

                mDialogView.btnNoDelete.setOnClickListener {
                    mdialogConfirmDelete.dismiss()
                    finish()
                }
                mdialogConfirmDelete.show()
                /*val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
                    when (which) {
                        DialogInterface.BUTTON_POSITIVE -> {
                            val bangunan = realm.where(Bangunan::class.java).equalTo("BangunanIdTemp", session.id.toInt()).findFirst()
                            realm.beginTransaction()
                            bangunan!!.deleteFromRealm()
                            realm.commitTransaction()
                            dialog.dismiss()
                            finish()
                        }

                        DialogInterface.BUTTON_NEGATIVE -> {
                            dialog.dismiss()
                            finish()
                        }
                    }
                }

                val builder = android.support.v7.app.AlertDialog.Builder(this)
                builder.setTitle("SIOJT")
                        .setMessage("Yakin akan dihapus?")
                        .setPositiveButton("Ya", dialogClickListener)
                        .setNegativeButton("Tidak", dialogClickListener)
                        .setIcon(ContextCompat.getDrawable(this@ActicityFormAddObjekBangunan, R.drawable.ic_logo_splash)!!)
                        .show()*/
            }
        }

    }

    private fun getPresenter(): FormAddObjekRoomPresenter? {
        formAddObjekBangunanPresenter = FormAddObjekRoomPresenter()
        formAddObjekBangunanPresenter.onAttach(this)
        return formAddObjekBangunanPresenter
    }

    private fun dialogsuccessTransaksi(title_dialog: String) {
        Utils.deletedataform1(this@ActicityFormAddObjekRoom)
        Utils.deletedataform3(this@ActicityFormAddObjekRoom)
        Utils.deletedataform3(this@ActicityFormAddObjekRoom)
        Utils.deletedataformImage(this@ActicityFormAddObjekRoom)
        val dialogPopup = DialogSuccessAddRoom.newInstance(title_dialog)
        dialogPopup.show(supportFragmentManager, "Dalog_popup")
    }

    private fun dialogToHandleExitFromCurrentPage() {
        val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> {
                    dialog.dismiss()
                    super.onBackPressed()
                    session.setIdAndStatus("", "", "")
                    finish()
                }

                DialogInterface.BUTTON_NEGATIVE -> {
                    dialog.dismiss()
                }
            }
        }

        MaterialAlertDialogBuilder(this, R.style.CustomAlertDialogMaterialTheme)
                .setTitle(getString(R.string.app_name))
                .setIcon(R.mipmap.ic_launcher)
                .setMessage("Data yang telah dibuat akan hilang, yakin keluar?")
                .setCancelable(false)
                .setPositiveButton("KELUAR", dialogClickListener)
                .setNegativeButton("BATAL", dialogClickListener)
                .show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 333) {
                selectedLinkedObjectUAVId = data?.getIntExtra("objectId", 0)
            }
        }
    }

    override fun onsuccesspostBuildingObjek() {
        Utils.deletedatabangunan1(this@ActicityFormAddObjekRoom)
        Utils.deletedatabangunan2(this@ActicityFormAddObjekRoom)
        Utils.deletedataformImage(this@ActicityFormAddObjekRoom)
        val dialogPopup = DialogSuccessAddRoom()
        dialogPopup.show(supportFragmentManager, "Dalog_popup")
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    class SectionsPagerAdapter(fm: androidx.fragment.app.FragmentManager, id: Int, landId: String) : androidx.fragment.app.FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        private var getId: Int? = id
        private var getLandId: String = landId
        private val mFragmentList = arrayListOf<Fragment>()

        fun addFrag(fragment: Fragment) {
            mFragmentList.add(fragment)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            /*when (position) {
                0 -> return FormObjekBangunan1.newInstance(getId!!, getLandId)
                1 -> return FormObjekBangunan2.newInstance("", "")
                2 -> return FormObjekBangunanPhoto.newInstance("", "")
            }*/
            return mFragmentList[position]
        }

        override fun getPageTitle(position: Int): CharSequence? {
            when (position) {
                0 -> return "Ruang Atas dan Bawah Tanah"
                1 -> return "Catatan"
            }
            return null
        }
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    override fun onBackPressed(fragment: Fragment) {
        mSectionsPagerAdapter!!.notifyDataSetChanged()
    }

    override fun onNextPressed(fragment: Fragment) {
        mSectionsPagerAdapter!!.notifyDataSetChanged()
        when (fragment) {
            is FormObjekRoom1 -> viewpagerObjekTanah.setCurrentItem(1, true)
            is FormObjekRoomPhoto -> {

                if (sessionFlagsCRUD != null) {
                    if (sessionFlagsCRUD!!.statusPost == "2") {
                        onShowLoading()
                        realm.executeTransactionAsync({ inrealm ->
                            var bangunan = inrealm.where(Room::class.java)
                                    .equalTo("roomId", sessionFlagsCRUD!!.id!!)
                                    .findFirst()
                            nextId = bangunan?.roomId!!
                            bangunan.ProjectId = ProjectId
                            bangunan.LandIdTemp = LandIdTemp

                            //bayu
                            bangunan.landNumberList = saveData.landNumberList
                            bangunan.ownerDataId = saveData.ownerDataId
                            bangunan.ownerDataName = saveData.ownerDataName
                            bangunan.year = saveData.year
                            bangunan.upperRoom = saveData.upperRoom
                            bangunan.lowerRoom = saveData.lowerRoom
                            bangunan.notes = saveData.notes

                            val dataImagePlusId: MutableList<DataImageRoom> = ArrayList()
                            for (element in saveData.dataImageRoom!!.indices) {
                                if (!saveData.dataImageRoom?.get(element)?.imagepathLand.isNullOrEmpty() && !saveData.dataImageRoom?.get(element)?.imagenameLand.isNullOrEmpty()) {
                                    val dataImage = DataImageRoom()
                                    dataImage.imageId = nextId
                                    dataImage.imagepathLand = saveData.dataImageRoom?.get(element)!!.imagepathLand
                                    dataImage.imagenameLand = saveData.dataImageRoom?.get(element)!!.imagenameLand
                                    dataImagePlusId.add(dataImage)
                                }
                                //Log.d("hasil", dataPhoto.dataImagePhoto!![element].imagenameLand.toString())
                            }

                            if (dataImagePlusId.isNotEmpty()) {
                                val dataPhotoBangunan = inrealm.copyToRealmOrUpdate(dataImagePlusId)
                                val dataPhotoBangunan2: RealmList<DataImageRoom> = RealmList()
                                for (element in dataPhotoBangunan) {
                                    dataPhotoBangunan2.add(element)
                                }

                                bangunan.dataImageRoom = dataPhotoBangunan2
                            }

                            inrealm.insertOrUpdate(bangunan)
                            Log.d("building", "success update building objects")
                            runOnUiThread {
                                onDismissLoading()
                                dialogsuccessTransaksi("""
                                Data
                                berhasil
                                diperbaharui
                            """.trimIndent())
                            }
                        }, {
                        }, {
                            Log.d("room", "failed update room")
                            Log.d("room", it.localizedMessage!!)
                            Log.d("room", it.message!!)
                            runOnUiThread {
                                onDismissLoading()
                            }
                        })
                    }
                }
                else {
                    // Begin Saving
                    onShowLoading()
                    realm.executeTransactionAsync({ inrealm ->
                        //Log.d("hasil", dataconvert2.toString())
                        val currentIdNum = inrealm.where(Room::class.java).max("roomId")
                        nextId = if (currentIdNum == null) {
                            1
                        } else {
                            currentIdNum.toInt() + 1
                        }

                        val bangunan = inrealm.createObject(Room::class.java, nextId)
                        bangunan.ProjectId = ProjectId
                        bangunan.LandIdTemp = LandIdTemp
                        bangunan.landNumberList = saveData.landNumberList
                        bangunan.ownerDataId = saveData.ownerDataId
                        bangunan.ownerDataName = saveData.ownerDataName
                        bangunan.year = saveData.year
                        bangunan.upperRoom = saveData.upperRoom
                        bangunan.lowerRoom = saveData.lowerRoom
                        bangunan.notes = saveData.notes

                        val dataImagePlusId: MutableList<DataImageRoom> = ArrayList()
                        for (element in saveData.dataImageRoom!!.indices) {
                            if (!saveData.dataImageRoom?.get(element)?.imagepathLand.isNullOrEmpty() && !saveData.dataImageRoom?.get(element)?.imagenameLand.isNullOrEmpty()) {
                                val dataImage = DataImageRoom()
                                dataImage.imageId = nextId
                                dataImage.imagepathLand = saveData.dataImageRoom?.get(element)!!.imagepathLand
                                dataImage.imagenameLand = saveData.dataImageRoom?.get(element)!!.imagenameLand
                                dataImagePlusId.add(dataImage)
                            }
                            //Log.d("hasil", dataPhoto.dataImagePhoto!![element].imagenameLand.toString())
                        }

                        if (dataImagePlusId.isNotEmpty()) {
                            val dataPhotoBangunan = inrealm.copyToRealmOrUpdate(dataImagePlusId)
                            val dataPhotoBangunan2: RealmList<DataImageRoom> = RealmList()
                            for (element in dataPhotoBangunan) {
                                dataPhotoBangunan2.add(element)
                            }

                            bangunan.dataImageRoom = dataPhotoBangunan2
                        }

                        Log.d("room", "success adds room objects")
                        runOnUiThread {
                            onDismissLoading()
                            onsuccesspostBuildingObjek()
                        }
                    }, {

                    }, {
                        Log.d("room", "failed adds room")
                        Log.d("room", it.localizedMessage!!)
                        Log.d("room", it.message!!)
                    })

                }
            }
        }
    }

    override fun onBackPressed() {
        dialogToHandleExitFromCurrentPage()
    }

    override fun onDestroy() {
        super.onDestroy()
        session.setIdAndStatus("", "", "")
    }
}