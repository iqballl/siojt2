package mki.siojt2.ui.acitivity_maps_drawing

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.RectF
import android.os.Bundle
import android.os.Handler
import android.view.*
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.Toast
import cn.pedant.SweetAlert.SweetAlertDialog
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.geojson.*
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationComponentConstants
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.style.layers.*
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.*
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_mapbox.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.model.ResponseDataListProjectLocal
import mki.siojt2.model.data_form_tanah.DataKoordinatMapBox
import mki.siojt2.model.localsave.Bangunan
import mki.siojt2.model.localsave.ProjectLand
import mki.siojt2.model.mapbox.MapProjectPolylines
import mki.siojt2.persistence.Preference
import mki.siojt2.utils.extension.MapBoxUtils
import mki.siojt2.utils.realm.RealmController


class MapsActivityDrawingMapBox : BaseActivity(), PermissionsListener{

    private var permissionsManager: PermissionsManager? = null
    private var mapboxMap: MapboxMap? = null
    private var firstPointOfPolygon: Point? = null
    var parentlistPolygons: ArrayList<ArrayList<String>> = ArrayList()
    var childlistPolygons: ArrayList<String> = ArrayList()
    private val listLatLng = ArrayList<LatLng>()
    private var fillLayerPointList: MutableList<Point> = arrayListOf()
    private var lineLayerPointList: MutableList<Point> = arrayListOf()
    private var circleLayerFeatureList: MutableList<Feature> = arrayListOf()
    private var listOfList: MutableList<MutableList<Point>> = arrayListOf()
    private var circleSource: GeoJsonSource? = null
    private var fillSource: GeoJsonSource? = null
    private var lineSource: GeoJsonSource? = null
    private val CIRCLE_SOURCE_ID = "circle-source-id"
    private val FILL_SOURCE_ID = "fill-source-id"
    private val LINE_SOURCE_ID = "line-source-id"
    private val CIRCLE_LAYER_ID = "circle-layer-id"
    private val FILL_LAYER_ID = "fill-layer-polygon-id"
    private val LINE_LAYER_ID = "line-layer-id"
    private val MARKER_ID = "MARKER_ID"
    private val MARKER_SOURCE_ID = "MARKER_SOURCE_ID"
    private val MARKER_LAYER_ID = "MARKER_LAYER_ID"
    private var isAddingKoordinatMaps: Boolean = false

    lateinit var realm: Realm
    private val IMAGE_OVERLAY_SOURCE_IDs = arrayListOf<String>()
    private val polygonLayerList: MutableList<MapProjectPolylines> = arrayListOf()
    private var polygonProject: MapProjectPolylines? = null
    lateinit var selectedStyle: Style

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token))
        setContentView(R.layout.activity_mapbox)

        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        /* set toolbar*/
        if (toolbarMaps != null) {
            setSupportActionBar(toolbarMaps)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync { mapboxMap ->
            this.mapboxMap = mapboxMap
            initMapInteraction()
            mapboxMap.setStyle(
                    Style.Builder().fromUri(Style.LIGHT)
                    .withImage(MARKER_ID, BitmapFactory.decodeResource(
                            this.resources, R.drawable.mapbox_marker_icon_default))
                    .withSource(GeoJsonSource(MARKER_SOURCE_ID,
                            FeatureCollection.fromFeatures(arrayListOf())))
                    .withLayer(SymbolLayer(MARKER_LAYER_ID, MARKER_SOURCE_ID)
                            .withProperties(
                                    iconImage(MARKER_ID),
                                    iconAllowOverlap(true),
                                    iconIgnorePlacement(true)
                            )
                    )) { style ->

                setUAVData(style)

                selectedStyle = style
                // Add sources to the map
                circleSource = initCircleSource(style)
                fillSource = initFillSource(style)
                lineSource = initLineSource(style)

                enableLocationComponent(style)

                // Add layers to the map
                initCircleLayer(style)
                initLineLayer(style)
                initFillLayer(style)
            }
        }

        toolbarMaps.setNavigationOnClickListener {
            finish()
        }

        btnMenu.setOnClickListener {
            if(map_type_selection.visibility == View.GONE){
                map_type_selection.visibility = View.VISIBLE
            }
            else{
                map_type_selection.visibility = View.GONE
            }
        }

        swImageOverlay.setOnCheckedChangeListener { _, b ->
            for (i in IMAGE_OVERLAY_SOURCE_IDs) {
                if (b) {
                    selectedStyle.getLayer(i)?.setProperties(visibility(Property.VISIBLE))
                } else {
                    selectedStyle.getLayer(i)?.setProperties(visibility(Property.NONE))
                }
            }
        }

        swObjectUAV.setOnCheckedChangeListener { _, b ->
            for (i in polygonLayerList) {
                if (i.type == "uav-object") {
                    if (b) {
                        selectedStyle.getLayer(i.CIRCLE_LAYER_ID)?.setProperties(visibility(Property.VISIBLE))
                        selectedStyle.getLayer(i.FILL_LAYER_ID)?.setProperties(visibility(Property.VISIBLE))
                        selectedStyle.getLayer(i.LINE_LAYER_ID)?.setProperties(visibility(Property.VISIBLE))
                    } else {
                        selectedStyle.getLayer(i.CIRCLE_LAYER_ID)?.setProperties(visibility(Property.NONE))
                        selectedStyle.getLayer(i.FILL_LAYER_ID)?.setProperties(visibility(Property.NONE))
                        selectedStyle.getLayer(i.LINE_LAYER_ID)?.setProperties(visibility(Property.NONE))
                    }
                }
            }
        }

        swProject.setOnCheckedChangeListener { _, b ->
            if (polygonProject != null) {
                if (b) {
                    selectedStyle.getLayer(polygonProject!!.CIRCLE_LAYER_ID)?.setProperties(visibility(Property.VISIBLE))
                    selectedStyle.getLayer(polygonProject!!.FILL_LAYER_ID)?.setProperties(visibility(Property.VISIBLE))
                    selectedStyle.getLayer(polygonProject!!.LINE_LAYER_ID)?.setProperties(visibility(Property.VISIBLE))
                } else {
                    selectedStyle.getLayer(polygonProject!!.CIRCLE_LAYER_ID)?.setProperties(visibility(Property.NONE))
                    selectedStyle.getLayer(polygonProject!!.FILL_LAYER_ID)?.setProperties(visibility(Property.NONE))
                    selectedStyle.getLayer(polygonProject!!.LINE_LAYER_ID)?.setProperties(visibility(Property.NONE))
                }
            }
        }
    }

    private fun setUAVData(style: Style) {
        val results = realm.where(ResponseDataListProjectLocal::class.java)
                .equalTo("projectAssignProjectId", intent.getIntExtra("projectId", 0))
                .findFirst()

        if (results != null) {
            if (!results.getUAVDataResponse()?.images.isNullOrEmpty()) {
                for ((index, i) in results.getUAVDataResponse()?.images!!.withIndex()) {
                    val overlaySourceId = "IMAGE_OVERLAY_SOURCE_ID_$index"
                    MapBoxUtils().addImageOverlay(style,
                            LatLng(i.UAVPosition?.topLeft?.lat!!, i.UAVPosition?.topLeft?.lng!!),
                            LatLng(i.UAVPosition?.topRight?.lat!!, i.UAVPosition?.topRight?.lng!!),
                            LatLng(i.UAVPosition?.bottomRight?.lat!!, i.UAVPosition?.bottomRight?.lng!!),
                            LatLng(i.UAVPosition?.bottomLeft?.lat!!, i.UAVPosition?.bottomLeft?.lng!!),
                            overlaySourceId,
                            overlaySourceId,
                            i.imgPath!!,
                            LocationComponentConstants.SHADOW_LAYER)
                    IMAGE_OVERLAY_SOURCE_IDs.add(overlaySourceId)
                }
            }
            uavObjectPolygon(style, results)
            uavProjectPolygon(style, results)
        }
    }

    private fun uavObjectPolygon(style: Style, results: ResponseDataListProjectLocal) {
        if (!results.getUAVDataResponse()?.objects.isNullOrEmpty()) {
            for ((index, value) in results.getUAVDataResponse()?.objects!!.withIndex()) {
                val polylines = MapProjectPolylines()
                polylines.id = value.id
                polylines.CIRCLE_SOURCE_ID = "circle-source-id-uav-object-$index"
                polylines.FILL_SOURCE_ID = "fill-source-id-uav-object-$index"
                polylines.LINE_SOURCE_ID = "line-source-id-uav-object-$index"
                polylines.CIRCLE_LAYER_ID = "circle-layer-id-uav-object-$index"
                polylines.FILL_LAYER_ID = "fill-layer-id-uav-object-$index"
                polylines.LINE_LAYER_ID = "line-layer-id-uav-object-$index"
                polylines.uavObject = value
//                polylines.circleSource = initCircleSource(style, polylines.CIRCLE_SOURCE_ID)
//                polylines.fillSource = initFillSource(style, polylines.FILL_SOURCE_ID)
//                polylines.lineSource = initFillSource(style, polylines.LINE_SOURCE_ID)
                polylines.type = "uav-object"

                polylines.objectType = 1 //hardcode tanah

                val resultsDataTanah = realm.where(ProjectLand::class.java).equalTo("selectedLinkedObjectUAVId", value?.id).findFirst()
                polylines.landData = resultsDataTanah

                val resultsDataBuilding = realm.where(Bangunan::class.java).equalTo("selectedLinkedObjectUAVId", value?.id).findFirst()
                polylines.buildingData = resultsDataBuilding

                if (!value.geom.isNullOrEmpty()) {
                    val l: MutableList<List<Point>> = arrayListOf()
                    val coordinate: MutableList<Point> = arrayListOf()
                    for (i in value.geom!!) {
                        coordinate.add(Point.fromLngLat(i.lng, i.lat))
                    }
                    l.add(coordinate)
                    polylines.coordinat = l
                }

                style.addSource(GeoJsonSource(polylines.FILL_SOURCE_ID, Polygon.fromLngLats(polylines.coordinat)))
                style.addLayerBelow(FillLayer(polylines.FILL_LAYER_ID, polylines.FILL_SOURCE_ID).withProperties(
                        when {
                            polylines.landData != null || polylines.buildingData != null -> {
                                fillColor(Color.parseColor("#bfbfbf"))
                            }
                            else -> {
                                fillColor(Color.parseColor("#ff006a"))
                            }
                        }), LocationComponentConstants.SHADOW_LAYER)

                style.addSource(GeoJsonSource(polylines.LINE_SOURCE_ID, LineString.fromLngLats(polylines.coordinat[0])))
                style.addLayerBelow(LineLayer(polylines.LINE_LAYER_ID, polylines.LINE_SOURCE_ID).withProperties(
                        lineColor(Color.WHITE),
                        lineWidth(1f)
                ), LocationComponentConstants.SHADOW_LAYER)

                polygonLayerList.add(polylines)
            }
        }
    }

    private fun uavProjectPolygon(style: Style, results: ResponseDataListProjectLocal) {
        var centerProjectLat = 0.0
        var centerProjectLng = 0.0

        if (!results.getUAVDataResponse()?.uAVProject?.geom.isNullOrEmpty()) {
            polygonProject = MapProjectPolylines()
            polygonProject?.CIRCLE_SOURCE_ID = "circle-source-id-uav-project"
            polygonProject?.FILL_SOURCE_ID = "fill-source-id-uav-project"
            polygonProject?.LINE_SOURCE_ID = "line-source-id-uav-project"
            polygonProject?.CIRCLE_LAYER_ID = "circle-layer-id-uav-project"
            polygonProject?.FILL_LAYER_ID = "fill-layer-id-uav-project"
            polygonProject?.LINE_LAYER_ID = "line-layer-id-uav-project"
            polygonProject?.type = "uav-project"
//            val coordinate: MutableList<LatLng> = arrayListOf()
//            for (i in results.getUAVDataResponse()?.uAVProject?.geom!!) {
//                coordinate.add(LatLng(i.lat, i.lng))
//                polygonProject?.coordinat = coordinate
//            }
            if (!results.getUAVDataResponse()?.uAVProject?.geom.isNullOrEmpty()) {
                var x1 = results.getUAVDataResponse()?.uAVProject?.geom!![0]?.lat!! // lowest x (lat)
                var x2 = results.getUAVDataResponse()?.uAVProject?.geom!![0]?.lat!! // highest x (lat)
                var y1 = results.getUAVDataResponse()?.uAVProject?.geom!![0]?.lng!! // lowest y (lng)
                var y2 = results.getUAVDataResponse()?.uAVProject?.geom!![0]?.lng!! //highest y (lng)

                val l: MutableList<List<Point>> = arrayListOf()
                val coordinate: MutableList<Point> = arrayListOf()
                for (i in results.getUAVDataResponse()?.uAVProject?.geom!!) {
                    coordinate.add(Point.fromLngLat(i.lng, i.lat))
                    if(x1 > i.lat){
                        x1 = i.lat
                    }

                    if(x2 < i.lat){
                        x2 = i.lat
                    }

                    if(y1 > i.lng){
                        y1 = i.lng
                    }

                    if(y2 < i.lng){
                        y2 = i.lng
                    }
                }

                centerProjectLat = x1 + ((x2 - x1) / 2)
                centerProjectLng = y1 + ((y2 - y1) / 2)

                l.add(coordinate)
                polygonProject?.coordinat = l
            }

            val polygon = Polygon.fromLngLats(polygonProject!!.coordinat)

            style.addSource(GeoJsonSource(polygonProject?.FILL_SOURCE_ID, polygon))
            style.addLayerBelow(FillLayer(polygonProject?.FILL_LAYER_ID, polygonProject?.FILL_SOURCE_ID).withProperties(
                    fillColor(Color.parseColor("#ff8400"))), LocationComponentConstants.SHADOW_LAYER)

            style.addSource(GeoJsonSource(polygonProject?.LINE_SOURCE_ID, LineString.fromLngLats(polygonProject!!.coordinat[0])))
            style.addLayerBelow(LineLayer(polygonProject?.LINE_LAYER_ID, polygonProject?.LINE_SOURCE_ID).withProperties(
                    lineColor(Color.WHITE),
                    lineWidth(1f)
            ), LocationComponentConstants.SHADOW_LAYER)

            btnProjectLocation.setOnClickListener {
                mapboxMap?.animateCamera(CameraUpdateFactory.newLatLng(
                        LatLng(centerProjectLat, centerProjectLng)))
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun enableLocationComponent(loadedMapStyle: Style) {
        selectedStyle = loadedMapStyle
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            val locationComponent = mapboxMap!!.locationComponent
            locationComponent.activateLocationComponent(
                    LocationComponentActivationOptions.builder(this, loadedMapStyle).build())
            locationComponent.isLocationComponentEnabled = true
            locationComponent.cameraMode = CameraMode.TRACKING
            locationComponent.renderMode = RenderMode.COMPASS

            initClick()
            initDataLocal(loadedMapStyle)
        } else {
            permissionsManager = PermissionsManager(this)
            permissionsManager?.requestLocationPermissions(this)
        }
    }

    @SuppressLint("NewApi")
    private fun initMapInteraction(){
        mapboxMap?.addOnMapClickListener {
            val pointf = mapboxMap!!.projection.toScreenLocation(it)
            val rectF = RectF(pointf.x - 10, pointf.y - 10, pointf.x + 10, pointf.y + 10)
            val featureList: List<Feature> = mapboxMap!!.queryRenderedFeatures(rectF, FILL_LAYER_ID)
            if (featureList.isNotEmpty()) {
                for (feature in featureList) {
                    fabAddCoordinate.visibility = View.GONE
                    fabDoneAddCoordinate.visibility = View.GONE
                    val anim = ViewAnimationUtils.createCircularReveal(
                            llActionPickMaps,
                            llActionPickMaps.width - (map_type_FAB.width / 2),
                            map_type_FAB.height / 2,
                            map_type_FAB.width / 2f,
                            llActionPickMaps.width.toFloat())
                    anim.duration = 500
                    anim.interpolator = AccelerateDecelerateInterpolator()
                    anim.addListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationStart(animation: Animator?) {
                            super.onAnimationEnd(animation)
                            llActionPickMaps.visibility = View.VISIBLE
                        }
                    })
                    anim.start()
                }
                true
            }

            //click icon layer
            val selectedFeature: List<Feature> = mapboxMap!!.queryRenderedFeatures(pointf, CIRCLE_LAYER_ID)
            if(selectedFeature.isNotEmpty()){

            }
            false
        }

    }

    @SuppressLint("NewApi")
    private fun initClick(){
        btnmyLocation.setOnClickListener {
            val lastKnownLocation = mapboxMap?.locationComponent?.lastKnownLocation

            mapboxMap?.animateCamera(CameraUpdateFactory.newLatLng(
                    LatLng(lastKnownLocation?.latitude!!, lastKnownLocation.longitude)))
        }

        btnZoomIn.setOnClickListener {
            mapboxMap?.animateCamera(CameraUpdateFactory.zoomIn())
        }

        btnZoomOut.setOnClickListener {
            mapboxMap?.animateCamera(CameraUpdateFactory.zoomOut())
        }

        map_type_FAB.setSafeOnClickListener {
            ivcenterAimCoordinat.visibility = View.VISIBLE
            val anim = ViewAnimationUtils.createCircularReveal(
                    llActionPickMaps,
                    llActionPickMaps.width - (map_type_FAB.width / 2),
                    map_type_FAB.height / 2,
                    map_type_FAB.width / 2f,
                    llActionPickMaps.width.toFloat())
            anim.duration = 500
            anim.interpolator = AccelerateDecelerateInterpolator()

            anim.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationStart(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    llActionPickMaps.visibility = View.VISIBLE
                }
            })

            anim.start()
            map_type_FAB.visibility = View.INVISIBLE
        }

        map_type_default.setOnClickListener {
            map_type_selection.visibility = View.GONE
            map_type_default_background.visibility = View.VISIBLE
            map_type_satellite_background.visibility = View.INVISIBLE
            map_type_terrain_background.visibility = View.INVISIBLE
            map_type_default_text.setTextColor(Color.BLUE)
            map_type_satellite_text.setTextColor(Color.parseColor("#808080"))
            map_type_terrain_text.setTextColor(Color.parseColor("#808080"))
//            mapboxMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL

            mapboxMap?.setStyle(
                    Style.Builder().fromUri(Style.LIGHT)
                            .withImage(MARKER_ID, BitmapFactory.decodeResource(
                                    this.resources, R.drawable.mapbox_marker_icon_default))
                            .withSource(GeoJsonSource(MARKER_SOURCE_ID,
                                    FeatureCollection.fromFeatures(arrayListOf())))
                            .withLayer(SymbolLayer(MARKER_LAYER_ID, MARKER_SOURCE_ID)
                                    .withProperties(
                                            iconImage(MARKER_ID),
                                            iconAllowOverlap(true),
                                            iconIgnorePlacement(true)
                                    )
                            )) { style ->
                selectedStyle = style
                // Add sources to the map
                setUAVData(style)

                circleSource = initCircleSource(style)
                fillSource = initFillSource(style)
                lineSource = initLineSource(style)

                enableLocationComponent(style)

                // Add layers to the map
                initCircleLayer(style)
                initLineLayer(style)
                initFillLayer(style)

                if (circleSource != null) {
                    circleSource!!.setGeoJson(FeatureCollection.fromFeatures(circleLayerFeatureList))
                }
                if (lineSource != null) {
                    lineSource!!.setGeoJson(FeatureCollection.fromFeatures(arrayOf(Feature.fromGeometry(LineString.fromLngLats(lineLayerPointList)))))
                }

                val finalFeatureList: MutableList<Feature> = java.util.ArrayList()
                finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)))
                val newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList)
                if (fillSource != null) {
                    fillSource!!.setGeoJson(newFeatureCollection)
                }
            }
        }

        // Handle selection of the Satellite map type
        map_type_satellite.setOnClickListener {
            map_type_selection.visibility = View.GONE
            map_type_default_background.visibility = View.INVISIBLE
            map_type_satellite_background.visibility = View.VISIBLE
            map_type_terrain_background.visibility = View.INVISIBLE
            map_type_default_text.setTextColor(Color.parseColor("#808080"))
            map_type_satellite_text.setTextColor(Color.BLUE)
            map_type_terrain_text.setTextColor(Color.parseColor("#808080"))
//            mMap!!.mapType = GoogleMap.MAP_TYPE_HYBRID
            mapboxMap?.setStyle(
                    Style.Builder().fromUri(Style.SATELLITE)
                            .withImage(MARKER_ID, BitmapFactory.decodeResource(
                                    this.resources, R.drawable.mapbox_marker_icon_default))
                            .withSource(GeoJsonSource(MARKER_SOURCE_ID,
                                    FeatureCollection.fromFeatures(arrayListOf())))
                            .withLayer(SymbolLayer(MARKER_LAYER_ID, MARKER_SOURCE_ID)
                                    .withProperties(
                                            iconImage(MARKER_ID),
                                            iconAllowOverlap(true),
                                            iconIgnorePlacement(true)
                                    )
                            )) { style ->
                selectedStyle = style
                // Add sources to the map
                setUAVData(style)

                circleSource = initCircleSource(style)
                fillSource = initFillSource(style)
                lineSource = initLineSource(style)

                enableLocationComponent(style)

                // Add layers to the map
                initCircleLayer(style)
                initLineLayer(style)
                initFillLayer(style)


                if (circleSource != null) {
                    circleSource!!.setGeoJson(FeatureCollection.fromFeatures(circleLayerFeatureList))
                }
                if (lineSource != null) {
                    lineSource!!.setGeoJson(FeatureCollection.fromFeatures(arrayOf(Feature.fromGeometry(LineString.fromLngLats(lineLayerPointList)))))
                }

                val finalFeatureList: MutableList<Feature> = java.util.ArrayList()
                finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)))
                val newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList)
                if (fillSource != null) {
                    fillSource!!.setGeoJson(newFeatureCollection)
                }
            }
        }

        // Handle selection of the terrain map type
        map_type_terrain.setOnClickListener {
            map_type_default_background.visibility = View.INVISIBLE
            map_type_satellite_background.visibility = View.INVISIBLE
            map_type_terrain_background.visibility = View.VISIBLE
            map_type_default_text.setTextColor(Color.parseColor("#808080"))
            map_type_satellite_text.setTextColor(Color.parseColor("#808080"))
            map_type_terrain_text.setTextColor(Color.BLUE)
            mapboxMap?.setStyle(Style.MAPBOX_STREETS)
        }

        mapboxMap?.addOnCameraIdleListener {

            val midlatLang: LatLng = mapboxMap!!.cameraPosition.target

            fabAddCoordinate.setOnClickListener {
                // Use the map click location to create a Point object
                val mapTargetPoint = Point.fromLngLat(mapboxMap!!.cameraPosition.target.longitude,
                        mapboxMap!!.cameraPosition.target.latitude)


                // Make note of the first map click location so that it can be used to create a closed polygon later on
                if (circleLayerFeatureList.isEmpty()) {
                    firstPointOfPolygon = mapTargetPoint
                }
                // Add the click point to the circle layer and update the display of the circle layer data

                // Add the click point to the circle layer and update the display of the circle layer data
                circleLayerFeatureList.add(Feature.fromGeometry(mapTargetPoint))
                if (circleSource != null) {
                    circleSource!!.setGeoJson(FeatureCollection.fromFeatures(circleLayerFeatureList))
                }

                // Add the click point to the line layer and update the display of the line layer data
                when {
                    circleLayerFeatureList.size < 3 -> {
                        lineLayerPointList.add(mapTargetPoint)
                    }
                    circleLayerFeatureList.size == 3 -> {
                        lineLayerPointList.add(mapTargetPoint)
                        lineLayerPointList.add(firstPointOfPolygon!!)
                    }
                    else -> {
                        lineLayerPointList.removeAt(circleLayerFeatureList.size - 1)
                        lineLayerPointList.add(mapTargetPoint)
                        lineLayerPointList.add(firstPointOfPolygon!!)
                    }
                }
                if (lineSource != null) {
                    lineSource!!.setGeoJson(FeatureCollection.fromFeatures(arrayOf(Feature.fromGeometry(LineString.fromLngLats(lineLayerPointList)))))
                }

                // Add the click point to the fill layer and update the display of the fill layer data
                when {
                    circleLayerFeatureList.size < 3 -> {
                        fillLayerPointList.add(mapTargetPoint)
                    }
                    circleLayerFeatureList.size == 3 -> {
                        fillLayerPointList.add(mapTargetPoint)
                        fillLayerPointList.add(firstPointOfPolygon!!)
                    }
                    else -> {
                        fillLayerPointList.removeAt(fillLayerPointList.size - 1)
                        fillLayerPointList.add(mapTargetPoint)
                        fillLayerPointList.add(firstPointOfPolygon!!)
                    }
                }

                listOfList = arrayListOf()
                listOfList.add(fillLayerPointList)
                val finalFeatureList: MutableList<Feature> = java.util.ArrayList()
                finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)))
                val newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList)
                if (fillSource != null) {
                    fillSource!!.setGeoJson(newFeatureCollection)
                }

                val LatLong: String = mapTargetPoint.latitude().toString() + "," + mapTargetPoint.longitude().toString()
                childlistPolygons.add(LatLong)

                listLatLng.add(midlatLang)
            }
        }

        fabClearCoordinate.setOnClickListener {
            clearEntireMap()
        }

        fabDoneAddCoordinate.setOnClickListener {
            if (listLatLng.isNotEmpty()) {
                ivcenterAimCoordinat.visibility = View.INVISIBLE
                val anim = ViewAnimationUtils.createCircularReveal(
                        llActionPickMaps,
                        llActionPickMaps.width - (map_type_FAB.width / 2),
                        map_type_FAB.height / 2,
                        llActionPickMaps.width.toFloat(),
                        map_type_FAB.width / 2f)
                anim.duration = 200
                anim.interpolator = AccelerateDecelerateInterpolator()

                anim.addListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator?) {
                        super.onAnimationEnd(animation)
                        llActionPickMaps.visibility = View.INVISIBLE
                    }
                })
                anim.start()

                Handler().postDelayed({
                    SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Simpan data?")
                            .setContentText("Pastikan kembali area sebelum menyimpan!")
                            .setConfirmText("Simpan")
                            .setCancelText("Batal")
                            .setConfirmClickListener { sDialog ->
                                sDialog.dismissWithAnimation()
                                val saveKoordinatMaps = DataKoordinatMapBox(listLatLng)
                                Preference.saveDataKoordinatMapBox(saveKoordinatMaps)
                                listLatLng.clear()
                                parentlistPolygons.add(childlistPolygons)

                                val lisMauDikirim: ArrayList<String> = ArrayList()
                                for (i in 0 until parentlistPolygons.size) {
                                    var semuaKoordinatDalamSatuKey = ""
                                    for (j in 0 until parentlistPolygons[i].size) {
                                        if (semuaKoordinatDalamSatuKey != "") {
                                            semuaKoordinatDalamSatuKey += ";"
                                        }
                                        semuaKoordinatDalamSatuKey += parentlistPolygons[i][j]
                                    }

                                    lisMauDikirim.add(semuaKoordinatDalamSatuKey)
                                }
                                finish()
                            }
                            .setCancelClickListener { sDialog ->
                                sDialog.dismissWithAnimation()
                                ivcenterAimCoordinat.visibility = View.VISIBLE
                                // Start animator to reveal the selection view, starting from the FAB itself
                                val anim = ViewAnimationUtils.createCircularReveal(
                                        llActionPickMaps,
                                        llActionPickMaps.width - (map_type_FAB.width / 2),
                                        map_type_FAB.height / 2,
                                        map_type_FAB.width / 2f,
                                        llActionPickMaps.width.toFloat())
                                anim.duration = 500
                                anim.interpolator = AccelerateDecelerateInterpolator()

                                anim.addListener(object : AnimatorListenerAdapter() {
                                    override fun onAnimationStart(animation: Animator?) {
                                        super.onAnimationEnd(animation)
                                        llActionPickMaps.visibility = View.VISIBLE
                                    }
                                })

                                anim.start()
                            }
                            .show()
                }, 400)


                //lisMauDikirim = ["1.121,32.3;23.332,432.32;43.32,32.43","1.121,32.3;23.332,432.32;43.32,32.43"];
            } else {
                Toast.makeText(this, "Tentukan titik koordinat", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun initDataLocal(style: Style){
        if (Preference.dataKoordinatMapBox() != null && !Preference.dataKoordinatMapBox()?.coordinat.isNullOrEmpty()) {
            isAddingKoordinatMaps = false
            map_type_FAB.visibility = View.INVISIBLE
            val coodinateToArrayList: ArrayList<LatLng> = ArrayList()
            coodinateToArrayList.addAll(Preference.dataKoordinatMapBox()!!.coordinat!!)

            circleLayerFeatureList.clear()
            lineLayerPointList.clear()
            fillLayerPointList.clear()

            firstPointOfPolygon = Point.fromLngLat(coodinateToArrayList[0].longitude, coodinateToArrayList[0].latitude)

            for((index, data) in coodinateToArrayList.withIndex()){
                val mapTargetPoint = Point.fromLngLat(data.longitude,data.latitude)

                //set data titik or dot
                circleLayerFeatureList.add(Feature.fromGeometry(mapTargetPoint))

                //set data line
                when {
                    index < 2 -> {
                        lineLayerPointList.add(mapTargetPoint)
                    }
                    index == 2 -> {
                        lineLayerPointList.add(mapTargetPoint)
                        lineLayerPointList.add(firstPointOfPolygon!!)
                    }
                    else -> {
                        lineLayerPointList.removeAt(index)
                        lineLayerPointList.add(mapTargetPoint)
                        lineLayerPointList.add(firstPointOfPolygon!!)
                    }
                }

                //set data filler
                when {
                    index < 2 -> {
                        fillLayerPointList.add(mapTargetPoint)
                    }
                    index == 2 -> {
                        fillLayerPointList.add(mapTargetPoint)
                        fillLayerPointList.add(firstPointOfPolygon!!)
                    }
                    else -> {
                        fillLayerPointList.removeAt(index)
                        fillLayerPointList.add(mapTargetPoint)
                        fillLayerPointList.add(firstPointOfPolygon!!)
                    }
                }

                listOfList = arrayListOf()
                listOfList.add(fillLayerPointList)

                circleSource?.setGeoJson(FeatureCollection.fromFeatures(circleLayerFeatureList))
                lineSource!!.setGeoJson(FeatureCollection.fromFeatures(arrayOf(Feature.fromGeometry(LineString.fromLngLats(lineLayerPointList)))))

                val finalFeatureList: MutableList<Feature> = java.util.ArrayList()
                finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)))
                val newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList)
                fillSource?.setGeoJson(newFeatureCollection)
            }

        } else {
            isAddingKoordinatMaps = true
        }
    }

    /**
     * Remove the drawn area from the map by resetting the FeatureCollections used by the layers' sources
     */
    @SuppressLint("NewApi")
    private fun clearEntireMap() {
        if (!isAddingKoordinatMaps) {

            SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Kondirmasi Hapus data?")
                    .setContentText("Data area yang dihapus tidak bisa dikembalikan kembali")
                    .setConfirmText("Hapus")
                    .setCancelText("Batal")
                    .setConfirmClickListener { sDialog ->
                        sDialog.dismissWithAnimation()

                        Preference.deleteDataKoordinatMapBox()
                        clearLayerView()

                        fabAddCoordinate.visibility = View.VISIBLE
                        fabDoneAddCoordinate.visibility = View.VISIBLE
                        val anim = ViewAnimationUtils.createCircularReveal(
                                llActionPickMaps,
                                llActionPickMaps.width - (map_type_FAB.width / 2),
                                map_type_FAB.height / 2,
                                llActionPickMaps.width.toFloat(),
                                map_type_FAB.width / 2f)
                        anim.duration = 200
                        anim.interpolator = AccelerateDecelerateInterpolator()
                        anim.addListener(object : AnimatorListenerAdapter() {
                            override fun onAnimationEnd(animation: Animator?) {
                                super.onAnimationEnd(animation)
                                llActionPickMaps.visibility = View.INVISIBLE
                            }
                        })
                        anim.start()
                        isAddingKoordinatMaps = true
                        map_type_FAB.visibility = View.VISIBLE

                    }
                    .setCancelClickListener { sDialog ->
                        sDialog.dismissWithAnimation()
                    }
                    .show()

        }
        else{
            clearLayerView()
        }
    }

    private fun clearLayerView(){
        listLatLng.clear()
        fillLayerPointList = java.util.ArrayList()
        circleLayerFeatureList = java.util.ArrayList()
        lineLayerPointList = java.util.ArrayList()
        circleSource?.setGeoJson(FeatureCollection.fromFeatures(arrayOf()))
        lineSource?.setGeoJson(FeatureCollection.fromFeatures(arrayOf()))
        fillSource?.setGeoJson(FeatureCollection.fromFeatures(arrayOf()))
    }

    /**
     * Set up the CircleLayer source for showing map click points
     */
    private fun initCircleSource(loadedMapStyle: Style): GeoJsonSource? {
        val circleFeatureCollection = FeatureCollection.fromFeatures(arrayOf())
        val circleGeoJsonSource = GeoJsonSource(CIRCLE_SOURCE_ID, circleFeatureCollection)
        loadedMapStyle.addSource(circleGeoJsonSource)
        return circleGeoJsonSource
    }

    /**
     * Set up the CircleLayer for showing polygon click points
     */
    private fun initCircleLayer(loadedMapStyle: Style) {
//        val circleLayer = CircleLayer(CIRCLE_LAYER_ID,
//                CIRCLE_SOURCE_ID)
//        circleLayer.setProperties(
//                PropertyFactory.circleRadius(7f),
////                PropertyFactory.circleColor(Color.parseColor("#d004d3"))
//                PropertyFactory.circleColor(resources.getColor(R.color.transparent))
//        )

        val circleLayer = SymbolLayer(CIRCLE_LAYER_ID,
                CIRCLE_SOURCE_ID)
        circleLayer.withProperties(
                iconImage(MARKER_ID),
                iconAllowOverlap(true),
                iconIgnorePlacement(true)
        )
        loadedMapStyle.addLayerBelow(circleLayer, LocationComponentConstants.SHADOW_LAYER)
    }

    /**
     * Set up the FillLayer source for showing map click points
     */
    private fun initFillSource(loadedMapStyle: Style): GeoJsonSource? {
        val fillFeatureCollection = FeatureCollection.fromFeatures(arrayOf())
        val fillGeoJsonSource = GeoJsonSource(FILL_SOURCE_ID, fillFeatureCollection)
        loadedMapStyle.addSource(fillGeoJsonSource)
        return fillGeoJsonSource
    }

    /**
     * Set up the FillLayer for showing the set boundaries' polygons
     */
    private fun initFillLayer(loadedMapStyle: Style) {
        val fillLayer = FillLayer(FILL_LAYER_ID,
                FILL_SOURCE_ID)
        fillLayer.setProperties(
                fillOpacity(.6f),
                fillColor(Color.parseColor("#00e9ff"))
        )
        loadedMapStyle.addLayerBelow(fillLayer, LINE_LAYER_ID)
    }

    /**
     * Set up the LineLayer source for showing map click points
     */
    private fun initLineSource(loadedMapStyle: Style): GeoJsonSource? {
        val lineFeatureCollection = FeatureCollection.fromFeatures(arrayOf())
        val lineGeoJsonSource = GeoJsonSource(LINE_SOURCE_ID, lineFeatureCollection)
        loadedMapStyle.addSource(lineGeoJsonSource)
        return lineGeoJsonSource
    }

    /**
     * Set up the LineLayer for showing the set boundaries' polygons
     */
    private fun initLineLayer(loadedMapStyle: Style) {
        val lineLayer = LineLayer(LINE_LAYER_ID,
                LINE_SOURCE_ID)
        lineLayer.setProperties(
                lineColor(Color.WHITE),
                lineWidth(1f)
        )
        loadedMapStyle.addLayerBelow(lineLayer, CIRCLE_LAYER_ID)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        permissionsManager?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onExplanationNeeded(permissionsToExplain: List<String?>?) {
        Toast.makeText(this, "Harap aktifkan lokasi",
                Toast.LENGTH_LONG).show()
    }
    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            mapboxMap!!.getStyle { style -> enableLocationComponent(style) }
        } else {
            Toast.makeText(this, "Permission tidak diberi akses", Toast.LENGTH_LONG).show()
        }
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    /*navigation drawer*/
    @SuppressLint("NewApi")
    fun funreplaceFragment(position: Int) {
        when (position) {
            0 -> {
                /*Type map*/
                map_type_selection.visibility = View.GONE
                // Start animator to reveal the selection view, starting from the FAB itself
                val anim = ViewAnimationUtils.createCircularReveal(
                        map_type_selection,
                        map_type_selection.width - (map_type_FAB.width / 2),
                        map_type_FAB.height / 2,
                        map_type_FAB.width / 2f,
                        map_type_selection.width.toFloat())
                anim.duration = 200
                anim.interpolator = AccelerateDecelerateInterpolator()

                anim.addListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationStart(animation: Animator?) {
                        super.onAnimationEnd(animation)
                        map_type_selection.visibility = View.VISIBLE
                    }
                })

                anim.start()
            }
        }
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    companion object {
        internal const val TAG = "MapsActivityDrawing"
        private const val DEFAULT_ZOOM: Float = 18f
        fun getStartIntent(context: Context): Intent {
            return Intent(context, MapsActivityDrawingMapBox::class.java)
        }
    }
}
