package mki.siojt2.ui.activity_daftar_tanah.presenter

import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.ui.activity_daftar_tanah.view.ActivityDaftarTanahView
import mki.siojt2.utils.ErrorHandler
import mki.siojt2.utils.rxJava.RxUtils

class DaftarTanahPresenter : BasePresenter(), DetailTanahMVPPresenter {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun getDaftarTanah(projectId: Int, partyId: Int, accessToken: String) {
        view().onShowLoading()
        compositeDisposable.add(
                dataManager.getdaftarTanah(projectId, partyId, accessToken)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetDaftarTanah(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    private fun view(): ActivityDaftarTanahView {
        return getView() as ActivityDaftarTanahView
    }
}