package mki.siojt2.ui.activity_daftar_tanah.recycleview

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.AsyncTask
import android.os.Environment
import android.os.Handler
import android.text.Html
import android.text.format.DateFormat
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.gson.Gson
import com.itextpdf.text.*
import com.itextpdf.text.pdf.PdfPCell
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfWriter
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.dialog_confirm_export.view.*
import kotlinx.android.synthetic.main.item_daftar_tanah_vertical.view.*
import mki.siojt2.R
import mki.siojt2.base.BaseViewHolder
import mki.siojt2.model.ResponseDataListProjectLocal
import mki.siojt2.model.localsave.PartyAdd
import mki.siojt2.model.localsave.ProjectLand
import mki.siojt2.model.localsave.Subjek2
import mki.siojt2.persistence.Preference
import mki.siojt2.ui.activity_daftar_ahli_waris.view.ActivityDaftarAhliWaris
import mki.siojt2.ui.activity_daftar_objek.ActivityDaftarObjek
import mki.siojt2.ui.activity_daftar_kepemilikan_tanah_kedua.view.ActivityDaftarKepemilikanTanahKedua
import mki.siojt2.ui.activity_daftar_tanah.view.ActivityDaftarTanah
import mki.siojt2.ui.activity_detail_tanah.view.ActivityDetailTanah
import mki.siojt2.utils.extension.OnSingleClickListener
import mki.siojt2.utils.extension.SafeClickListener
import mki.siojt2.utils.realm.RealmController
import org.apache.poi.ss.usermodel.HorizontalAlignment
import org.apache.poi.ss.usermodel.PrintSetup
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.VerticalAlignment
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.xssf.usermodel.XSSFCellStyle
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList


class AdapterDaftarTanah(context: Context, var activity: Activity) : RecyclerView.Adapter<BaseViewHolder>() {

    val mDataset: MutableList<ProjectLand>? = ArrayList()
    private var rowLayout = R.layout.item_daftar_tanah_vertical
    private var emptyrowLayout = R.layout.item_empty_view
    var pos = 0
    var jenisnameObjek: String? = null

    lateinit var realm: Realm

    init {
        realm = RealmController.with(activity).realm
    }

    //export

    private var sweetAlretLoading: SweetAlertDialog? = null
    private var wb: XSSFWorkbook? = null
    lateinit var ws: XSSFSheet
    private var satuamLuas: String? = Html.fromHtml("(m<sup><small>2</small></sup>)").toString()

    lateinit var rowProvinsi: Row
    lateinit var rowKota: Row
    lateinit var rowKabupaten: Row
    lateinit var rowKecamatan: Row
    lateinit var rowDekul: Row
    lateinit var rowrtRw: Row
    lateinit var rowBlokNo: Row
    lateinit var rowJln: Row
    lateinit var rownoBid: Row
    lateinit var rownoPetugas: Row
    lateinit var rowNamaProyek: Row

    /*pemilik tanah*/
    lateinit var rowpemilikTanahName: Row
    lateinit var rowpemilikTanahTtl: Row
    lateinit var rowpemilikTanahPekerjaan: Row
    lateinit var rowpemilikTanahAlamat: Row
    lateinit var rowpemilikTanahNodIdentitas: Row

    lateinit var rowLuasTanah: Row
    lateinit var rowPenggunaanTanah: Row
    lateinit var rowStatusTanah: Row
    lateinit var rowluasStatusTanah: Row

    lateinit var rowoptionNameStatusTanah2: Row
    lateinit var rowoptionLuasStatusTanah2: Row
    lateinit var rowoptionNameStatusTanah3: Row
    lateinit var rowoptionNameStatusTanah4: Row

    /* bukti kepemilikan tanah*/
    lateinit var rowtitleBuktiKepemilikan: Row
    lateinit var rowoptionBuktiKepemilikan2: Row
    lateinit var rowoptionBuktiKepemilikan3: Row
    lateinit var rowoptionBuktiKepemilikan4: Row

    /* lama memiliki tanah */
    lateinit var rowtitleLamaKepemilikanTanah: Row
    lateinit var rowtitleSitaJaminanTanah: Row

    lateinit var rowtitleBagian3: Row

    /*row excel*/
    private var lastRow12 = 0
    private var oldLastRow = 0
    private var currentLastRow = 0

    private var oldLastRow2 = 0
    private var currentLastRow2 = 0

    private val dateTemplate = "dd MMMM yyyy"

    private val mContext: Context? = context
    private var mCallback: Callback? = null

    fun setCallback(callback: Callback) {
        mCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            VIEW_TYPE_NORMAL -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(rowLayout, parent, false))
            VIEW_TYPE_EMPTY -> EmptyViewHolder(
                    LayoutInflater.from(parent.context).inflate(R.layout.item_empty_view, parent, false))
            else -> EmptyViewHolder(LayoutInflater.from(parent.context).inflate(emptyrowLayout, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (mDataset != null && mDataset.isNotEmpty()) {
            VIEW_TYPE_NORMAL
        } else {
            VIEW_TYPE_EMPTY
        }
    }

    override fun getItemCount(): Int {
        return if (mDataset!!.isNotEmpty()) {
            mDataset.size
        } else {
            1
        }
    }

    /*inner class RuangViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var r_no: TextView = itemView.findViewById(R.id.rowNo) as TextView
        var r_tvTitle: TextView = itemView.findViewById(R.id.tvTitle) as TextView
        var r_tvContent1: TextView = itemView.findViewById(R.id.tvContent1) as TextView
        var r_tvContent2: TextView = itemView.findViewById(R.id.tvContent2) as TextView

    }*/

    fun clear() {
        this.mDataset!!.clear()
    }

    fun addItems(data: RealmResults<ProjectLand>) {
        this.mDataset!!.addAll(data)
        notifyDataSetChanged()
    }

    interface Callback {
        fun onRepoEmptyViewRetryClick()
        fun onDismissBottomsheet()
    }

    inner class ViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }

        @SuppressLint("SetTextI18n")
        override fun onBind(position: Int) {
            super.onBind(position)

            val dataList = mDataset!![position]


            //val pos = pos + position + 1
            itemView.tvnomorTanah.text = dataList.landNumberList
            itemView.tvnamaObjek.text = dataList.landDistrictName
            itemView.tvketObjek1.text = "Alamat : ${dataList.landComplekName} ${dataList.landBlok} No.${dataList.landNumber}"
            itemView.tvketObjek2.text = "Kec.${dataList.landDistrictName} ${dataList.landRegencyName}"
            itemView.tvketObjek3.text = "${dataList.landProvinceName}, ${dataList.landPostalCode}"
            /*if (dataList.idJenis == "1") {
                jenisObjek = 1
                getMenu = Constant.MENUOBJEK1
            } else if (dataList.idJenis == "2") {
                jenisObjek = 2
                getMenu = Constant.MENUOBJEK2
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    itemView.ivJenisObject.setImageDrawable(mContext?.resources?.getDrawable(R.drawable.test2, mContext.applicationContext.theme))
                } else {
                    itemView.ivJenisObject.setImageDrawable(mContext?.resources?.getDrawable(R.drawable.test2))
                }
            } else if (dataList.idJenis == "3") {
                jenisObjek = 3
                getMenu = Constant.MENUOBJEK3
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    itemView.ivJenisObject.setImageDrawable(mContext?.resources?.getDrawable(R.drawable.test, mContext.applicationContext.theme))
                } else {
                    itemView.ivJenisObject.setImageDrawable(mContext?.resources?.getDrawable(R.drawable.test))
                }
            } else if (dataList.idJenis == "4"){
                jenisObjek = 4
                getMenu = Constant.MENUOBJEK4
            }*/

            itemView.btnexportTanah.setSafeOnClickListener {
                try {
                    saveExcelFile("Project name", dataList.landIdTemp, dataList.landDistrictName)
                }
                catch (ex: Exception){
                    ex.printStackTrace()
                }
            }

            itemView.btnahliWaris.setSafeOnClickListener {
                /* val intent = ActivityDetailTanah.getStartIntent(mContext!!)
                     intent.putExtra("jenis_kategori",jenisObjek)
                     intent.putExtra("jenis_name_kategori",getMenu)
                     mContext.startActivity(intent)*/
                mCallback?.onDismissBottomsheet()
                val param = "['${dataList.areaId}','${dataList.projectId}','${dataList.landDistrictName}','${dataList.landIdTemp}',1,'']"
                val intent = ActivityDaftarAhliWaris.getStartIntent(mContext!!)
                intent.putExtra("data_current_tanah", param)
                Log.d("xxxx3", dataList.areaId.toString())
                mContext.startActivity(intent)
            }

            itemView.btnSelngkapnya.setSafeOnClickListener {
                /* val intent = ActivityDetailTanah.getStartIntent(mContext!!)
                   intent.putExtra("jenis_kategori",jenisObjek)
                   intent.putExtra("jenis_name_kategori",getMenu)
                   mContext.startActivity(intent)*/
                mCallback?.onDismissBottomsheet()
                val param = "['${dataList.landIdTemp}','${dataList.projectId}','${dataList.landDistrictName}']"
                val intent = ActivityDetailTanah.getStartIntent(mContext!!)
                intent.putExtra("data_current_tanah", param)
                mContext.startActivity(intent)
            }

//            !!Important
//            itemView.btnDaftarObjek.setSafeOnClickListener {
//                val param = "['${dataList.landIdTemp}','${dataList.projectId}','${dataList.landDistrictName}','${dataList.partyIdTemp}']"
//                val intent = ActivityDaftarObjek.getStartIntent(mContext!!)
//                intent.putExtra("data_current_tanah", param)
//                mContext.startActivity(intent)
//            }

            itemView.btndaftarpihakKeduaTanah.setSafeOnClickListener {
                mCallback?.onDismissBottomsheet()
                val param = "['${dataList.landIdTemp}','${dataList.projectId}','${dataList.landDistrictName}']"
                val intent = ActivityDaftarKepemilikanTanahKedua.getStartIntent(mContext!!)
                intent.putExtra("data_current_tanah", param)
                mContext.startActivity(intent)
            }

            inflateData(dataList)
        }

        private fun inflateData(dataList: ProjectLand) {

        }
    }

    inner class EmptyViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }


    }

    private fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
        val safeClickListener = SafeClickListener {
            onSafeClick(it)
        }
        setOnClickListener(safeClickListener)
    }

    companion object {
        const val VIEW_TYPE_EMPTY = 0
        const val VIEW_TYPE_NORMAL = 1
    }

    //export


    @SuppressLint("SdCardPath", "DefaultLocale")
    @Suppress("DEPRECATION")
    fun saveExcelFile(filenameExport: String, landId: Int?, projectName: String) {

        val mDialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_confirm_export, null)
        val mBuilder = AlertDialog.Builder(activity)
        val mDialog = mBuilder.create()

        mDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val window = mDialog.window
        if (window != null) {
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            mDialog.window!!.setLayout(width, height)
        }
        mDialog.setView(mDialogView)
        mDialogView.btnExportCSV.visibility = View.INVISIBLE
        mDialogView.btnExportExcel.setOnClickListener {
            mDialog.dismiss()
            createExcelFile(landId, projectName)
        }

        mDialogView.btnExportPdf.setOnClickListener {
            mDialog.dismiss()
            convertDocToPdfitext(landId, projectName)
        }

        mDialog.show()
    }

    private fun createExcelFile(landId: Int?, projectName: String) {
        CreateFormatExcelTask(landId, projectName).execute()
    }

    @SuppressLint("StaticFieldLeak")
    inner class CreateFormatExcelTask(landId: Int?, projectName: String) : AsyncTask<Void, Void, Void>() {
        val projectName = projectName;
        val landId = landId;
        override fun doInBackground(vararg params: Void?): Void? {
            wb = XSSFWorkbook()
            ws = wb!!.createSheet("Sheet1")
            ws.printSetup.landscape = true
            ws.printSetup.paperSize = PrintSetup.A4_PAPERSIZE

            ws.setColumnWidth(1, 3 * 256)
            ws.setColumnWidth(0, 3 * 256)
            ws.setColumnWidth(2, 25 * 256)
            ws.setColumnWidth(3, 25 * 256)
            ws.setColumnWidth(4, 3 * 256)
            ws.setColumnWidth(5, 3 * 256)
            ws.setColumnWidth(15, 3 * 256)
            //ws.addIgnoredErrors(CellRangeAddress(0, 9999, 0, 9999), IgnoredErrorType.NUMBER_STORED_AS_TEXT)

            lastRow12 = 0
            oldLastRow = 0
            currentLastRow = 0

            oldLastRow2 = 0
            currentLastRow2 = 0

            val defaultFont = wb!!.createFont()
            defaultFont.fontHeight = (16.0 * 20).toShort()
            defaultFont.bold = true

            val defaultFontNo = wb!!.createFont()
            defaultFontNo.fontHeight = (12.0 * 20).toShort()
            defaultFontNo.bold = true

            val defaultFont3 = wb!!.createFont()
            defaultFont3.fontHeight = (12.0 * 20).toShort()

            /* style */
            val cellStyleHeader = wb!!.createCellStyle()
            cellStyleHeader.wrapText = true
            cellStyleHeader.setFont(defaultFont)
            // justify text alignment\
            cellStyleHeader.setAlignment(HorizontalAlignment.CENTER)

            //style no
            val cellStyleNo = wb!!.createCellStyle()
            cellStyleNo.wrapText = true
            cellStyleNo.setFont(defaultFontNo)
            // justify text alignment\
            cellStyleNo.setVerticalAlignment(VerticalAlignment.CENTER)
            cellStyleNo.setAlignment(HorizontalAlignment.CENTER)

            //style titik 2
            val defaultTitik2 = wb!!.createFont()
            defaultTitik2.fontHeight = (12.0 * 20).toShort()

            val cellStyleTitik2 = wb!!.createCellStyle()
            cellStyleTitik2.wrapText = true
            cellStyleTitik2.setFont(defaultTitik2)
            // justify text alignment\
            cellStyleTitik2.setVerticalAlignment(VerticalAlignment.CENTER)
            cellStyleTitik2.setAlignment(HorizontalAlignment.CENTER)

            //style 2
            val defaultFont2 = wb!!.createFont()
            defaultFont2.fontHeight = (12.0 * 20).toShort()
            defaultFont2.bold = true

            val cellStyleHeader2 = wb!!.createCellStyle()
            cellStyleHeader2.wrapText = true
            cellStyleHeader2.setFont(defaultFont2)
            // justify text alignment\
            cellStyleHeader2.setVerticalAlignment(VerticalAlignment.CENTER)

            //style 3
            val cellStyleHeader3 = wb!!.createCellStyle()
            cellStyleHeader3.wrapText = true
            cellStyleHeader3.setFont(defaultFont3)
            // justify text alignment\
            cellStyleHeader3.setVerticalAlignment(VerticalAlignment.CENTER)

            //header 1
            val rowheader1 = ws.createRow(2)
            val colheader1 = rowheader1.createCell(1)
            colheader1.cellStyle = cellStyleHeader
            colheader1.setCellValue("PELAKSANAAN PENGADAAN TANAH UNTUK PEMBANGUNAN")

            //merge for header 2
            ws.addMergedRegion(CellRangeAddress(2, 2, 1, 11))

            //header 2
            rowNamaProyek = ws.createRow(3)

            //merge for header 2
            ws.addMergedRegion(CellRangeAddress(3, 3, 1, 11))

            //header 3
            val rowheader3 = ws.createRow(5)
            val colheader3 = rowheader3.createCell(1)
            colheader3.cellStyle = cellStyleHeader
            colheader3.setCellValue("DATA PEMILIK TANAH YANG TERKENA PENGADAAN TANAH")

            //merge for header 3
            ws.addMergedRegion(CellRangeAddress(5, 5, 1, 11))


            /* LETAK TANAH */
            val rowLetakTanah = ws.createRow(7)
            val colnoLetakTanah = rowLetakTanah.createCell(1)
            colnoLetakTanah.cellStyle = cellStyleNo
            colnoLetakTanah.setCellValue("I")

            val colrowtitleLetakTanah = rowLetakTanah.createCell(2)
            colrowtitleLetakTanah.cellStyle = cellStyleHeader2
            colrowtitleLetakTanah.setCellValue("LETAK TANAH")
            //ws.addMergedRegion(CellRangeAddress(7, 7, 2, 3))

            //propinsi
            rowProvinsi = ws.createRow(8)
            val colProvinsi = rowProvinsi.createCell(2)
            colProvinsi.cellStyle = cellStyleHeader3
            colProvinsi.setCellValue("- Provinsi")

            val colProvinsi2 = rowProvinsi.createCell(4)
            colProvinsi2.cellStyle = cellStyleTitik2
            colProvinsi2.setCellValue(":")

            //Kota
            rowKota = ws.createRow(9)
            val colKota = rowKota.createCell(2)
            colKota.cellStyle = cellStyleHeader3
            colKota.setCellValue("- Kota")

            val colKota2 = rowKota.createCell(4)
            colKota2.cellStyle = cellStyleTitik2
            colKota2.setCellValue(":")

            //Kabupatan
            rowKabupaten = ws.createRow(10)
            val colKabupaten = rowKabupaten.createCell(2)
            colKabupaten.cellStyle = cellStyleHeader3
            colKabupaten.setCellValue("- Kabupaten")

            val colKabupaten2 = rowKabupaten.createCell(4)
            colKabupaten2.cellStyle = cellStyleTitik2
            colKabupaten2.setCellValue(":")

            //Kecamatan
            rowKecamatan = ws.createRow(11)
            val colKecamatan = rowKecamatan.createCell(2)
            colKecamatan.cellStyle = cellStyleHeader3
            colKecamatan.setCellValue("- Kecamatan")

            val colKecamatan2 = rowKecamatan.createCell(4)
            colKecamatan2.cellStyle = cellStyleTitik2
            colKecamatan2.setCellValue(":")

            //Desa/kelurahan
            rowDekul = ws.createRow(12)
            val colDekul = rowDekul.createCell(2)
            colDekul.cellStyle = cellStyleHeader3
            colDekul.setCellValue("- Desa / Kelurahan")

            val colDekul2 = rowDekul.createCell(4)
            colDekul2.cellStyle = cellStyleTitik2
            colDekul2.setCellValue(":")

            //Rt/rw
            rowrtRw = ws.createRow(13)
            val colrtRw = rowrtRw.createCell(2)
            colrtRw.cellStyle = cellStyleHeader3
            colrtRw.setCellValue("- RT / RW")

            val colrtRw2 = rowrtRw.createCell(4)
            colrtRw2.cellStyle = cellStyleTitik2
            colrtRw2.setCellValue(":")

            //Blok/no
            rowBlokNo = ws.createRow(14)
            val colblokNo = rowBlokNo.createCell(2)
            colblokNo.cellStyle = cellStyleHeader3
            colblokNo.setCellValue("- Blok / No")

            val colblokNo2 = rowBlokNo.createCell(4)
            colblokNo2.cellStyle = cellStyleTitik2
            colblokNo2.setCellValue(":")

            //Jalan
            rowJln = ws.createRow(15)
            val colJln = rowJln.createCell(2)
            colJln.cellStyle = cellStyleHeader3
            colJln.setCellValue("- Jalan")

            val colJln2 = rowJln.createCell(4)
            colJln2.cellStyle = cellStyleTitik2
            colJln2.setCellValue(":")

            //No bidang
            rownoBid = ws.createRow(16)
            val colnoBid = rownoBid.createCell(2)
            colnoBid.cellStyle = cellStyleHeader3
            colnoBid.setCellValue("- Nomor Bidang")

            val colnoBid2 = rownoBid.createCell(4)
            colnoBid2.cellStyle = cellStyleTitik2
            colnoBid2.setCellValue(":")

            //Petugas
            rownoPetugas = ws.createRow(17)
            val colPetugas = rownoPetugas.createCell(2)
            colPetugas.cellStyle = cellStyleHeader3
            colPetugas.setCellValue("- Petugas Satgas B")

            val colPetugas2 = rownoPetugas.createCell(4)
            colPetugas2.cellStyle = cellStyleTitik2
            colPetugas2.setCellValue(":")

            /* PEMILIK TANAH */
            val rowPemilikTanah = ws.createRow(19)
            val colnopemilikTanah = rowPemilikTanah.createCell(1)
            colnopemilikTanah.cellStyle = cellStyleNo
            colnopemilikTanah.setCellValue("II")

            val coltitlepemilikTanah = rowPemilikTanah.createCell(2)
            coltitlepemilikTanah.cellStyle = cellStyleHeader2
            coltitlepemilikTanah.setCellValue("PEMILIK TANAH")
            //ws.addMergedRegion(CellRangeAddress(7, 7, 2, 3))

            rowpemilikTanahName = ws.createRow(20)
            val colpemilikTanahName = rowpemilikTanahName.createCell(2)
            colpemilikTanahName.cellStyle = cellStyleHeader3
            colpemilikTanahName.setCellValue("- Nama")

            val colpemilikTanahName2 = rowpemilikTanahName.createCell(4)
            colpemilikTanahName2.cellStyle = cellStyleTitik2
            colpemilikTanahName2.setCellValue(":")

            rowpemilikTanahTtl = ws.createRow(21)
            val colpemilikTanahTtl = rowpemilikTanahTtl.createCell(2)
            colpemilikTanahTtl.cellStyle = cellStyleHeader3
            colpemilikTanahTtl.setCellValue("- Tempat / Tanggal Lahir")

            val colpemilikTanahTtl2 = rowpemilikTanahTtl.createCell(4)
            colpemilikTanahTtl2.cellStyle = cellStyleTitik2
            colpemilikTanahTtl2.setCellValue(":")

            rowpemilikTanahPekerjaan = ws.createRow(22)
            val colpemilikTanahPekerjaan = rowpemilikTanahPekerjaan.createCell(2)
            colpemilikTanahPekerjaan.cellStyle = cellStyleHeader3
            colpemilikTanahPekerjaan.setCellValue("- Pekerjaan")

            val colpemilikTanahPekerjaan2 = rowpemilikTanahPekerjaan.createCell(4)
            colpemilikTanahPekerjaan2.cellStyle = cellStyleTitik2
            colpemilikTanahPekerjaan2.setCellValue(":")

            rowpemilikTanahAlamat = ws.createRow(23)
            val colpemilikTanahAlamat = rowpemilikTanahAlamat.createCell(2)
            colpemilikTanahAlamat.cellStyle = cellStyleHeader3
            colpemilikTanahAlamat.setCellValue("- Alamat")

            val colpemilikTanahAlamat2 = rowpemilikTanahAlamat.createCell(4)
            colpemilikTanahAlamat2.cellStyle = cellStyleTitik2
            colpemilikTanahAlamat2.setCellValue(":")

            /* end of alamat pemilik */

            rowpemilikTanahNodIdentitas = ws.createRow(27)
            val coltitleNoIdentititas = rowpemilikTanahNodIdentitas.createCell(2)
            coltitleNoIdentititas.cellStyle = cellStyleHeader3
            coltitleNoIdentititas.setCellValue("- NIK / No. SIM")

            val coltitleNoIdentititas2 = rowpemilikTanahNodIdentitas.createCell(4)
            coltitleNoIdentititas2.cellStyle = cellStyleTitik2
            coltitleNoIdentititas2.setCellValue(":")

            rowLuasTanah = ws.createRow(28)
            val coltitleLuasTanah = rowLuasTanah.createCell(2)
            coltitleLuasTanah.cellStyle = cellStyleHeader3
            coltitleLuasTanah.setCellValue("- Luas Tanah Terkena")

            val coltitleLuasTanah2 = rowLuasTanah.createCell(4)
            coltitleLuasTanah2.cellStyle = cellStyleTitik2
            coltitleLuasTanah2.setCellValue(":")

            rowPenggunaanTanah = ws.createRow(30)
            val coltitlePenggunaanTanah = rowPenggunaanTanah.createCell(2)
            coltitlePenggunaanTanah.cellStyle = cellStyleHeader3
            coltitlePenggunaanTanah.setCellValue("- Penggunaan / Pemanfaatan Tanah")

            val coltitlePenggunaanTanah2 = rowPenggunaanTanah.createCell(4)
            coltitlePenggunaanTanah2.cellStyle = cellStyleTitik2
            coltitlePenggunaanTanah2.setCellValue(":")
            ws.addMergedRegion(CellRangeAddress(30, 30, 2, 3))

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)

            val mfilename = "$projectName.xlsx"

            val calendar = Calendar.getInstance(Locale.getDefault())

            val currentDate = DateFormat.format(dateTemplate, calendar.time)

            realm.executeTransactionAsync({ inrealm ->
                val results = inrealm.where(ProjectLand::class.java).equalTo("LandIdTemp", landId).findFirst()
                if (results != null) {
                    //Log.d("Hasil", results.toString())

                    val defaultFontBoldHeader = wb!!.createFont()
                    defaultFontBoldHeader.fontHeight = (16.0 * 20).toShort()
                    defaultFontBoldHeader.bold = true

                    val cellStyleHeader = wb!!.createCellStyle()
                    cellStyleHeader.wrapText = true
                    cellStyleHeader.setFont(defaultFontBoldHeader)
                    // justify text alignment\
                    cellStyleHeader.setAlignment(HorizontalAlignment.CENTER)

                    //style 3
                    val defaultFont3 = wb!!.createFont()
                    defaultFont3.fontHeight = (12.0 * 20).toShort()

                    val defaultFontBold = wb!!.createFont()
                    defaultFontBold.fontHeight = (12.0 * 20).toShort()
                    defaultFontBold.bold = true

                    val defaultFontTtdPetugas = wb!!.createFont()
                    defaultFontTtdPetugas.fontHeight = (14.0 * 20).toShort()
                    defaultFontTtdPetugas.bold = true

                    val cellStyleHeader3 = wb!!.createCellStyle()
                    cellStyleHeader3.wrapText = true
                    cellStyleHeader3.setFont(defaultFont3)
                    // justify text alignment\
                    cellStyleHeader3.setVerticalAlignment(VerticalAlignment.CENTER)

                    val cellStyleNumber = wb!!.createCellStyle()
                    cellStyleNumber.wrapText = true
                    cellStyleNumber.setFont(defaultFont3)
                    // justify text alignment\
                    cellStyleNumber.setVerticalAlignment(VerticalAlignment.CENTER)
                    cellStyleNumber.setAlignment(HorizontalAlignment.CENTER)

                    val cellStyleNoWithBold = wb!!.createCellStyle()
                    cellStyleNoWithBold.wrapText = true
                    cellStyleNoWithBold.setFont(defaultFont3)
                    // justify text alignment\
                    cellStyleNoWithBold.setVerticalAlignment(VerticalAlignment.CENTER)
                    cellStyleNoWithBold.setAlignment(HorizontalAlignment.CENTER)

                    val cellStyleTitleWithBold = wb!!.createCellStyle()
                    cellStyleTitleWithBold.wrapText = true
                    cellStyleTitleWithBold.setFont(defaultFontBold)
                    // justify text alignment\
                    cellStyleTitleWithBold.setVerticalAlignment(VerticalAlignment.CENTER)

                    /* style ttd */
                    val cellStyleTglTtd = wb!!.createCellStyle()
                    cellStyleTglTtd.wrapText = true
                    cellStyleTglTtd.setFont(defaultFontBold)
                    // justify text alignment\\
                    cellStyleTglTtd.setAlignment(HorizontalAlignment.RIGHT)

                    val cellStyleTtd = wb!!.createCellStyle()
                    cellStyleTtd.wrapText = true
                    cellStyleTtd.setFont(defaultFontTtdPetugas)
                    // justify text alignment\\
                    cellStyleTtd.setAlignment(HorizontalAlignment.CENTER)

                    //propinsi
                    val colProvinsi = rowProvinsi.createCell(5)
                    colProvinsi.cellStyle = cellStyleHeader3
                    colProvinsi.setCellValue(results.landProvinceName)
                    ws.addMergedRegion(CellRangeAddress(8, 8, 5, 14))

                    //Kota
                    val colKota = rowKota.createCell(5)
                    colKota.cellStyle = cellStyleHeader3
                    colKota.setCellValue(results.landRegencyName)
                    ws.addMergedRegion(CellRangeAddress(9, 9, 5, 14))

                    //Kabupatan
                    val colKabupaten = rowKabupaten.createCell(5)
                    colKabupaten.cellStyle = cellStyleHeader3
                    colKabupaten.setCellValue(results.landRegencyName)
                    ws.addMergedRegion(CellRangeAddress(10, 10, 5, 14))

                    //Kecamatan
                    val colKecamatan = rowKecamatan.createCell(5)
                    colKecamatan.cellStyle = cellStyleHeader3
                    colKecamatan.setCellValue(results.landDistrictName)
                    ws.addMergedRegion(CellRangeAddress(11, 11, 5, 14))

                    //Desa/kelurahan
                    val colDekul = rowDekul.createCell(5)
                    colDekul.cellStyle = cellStyleHeader3
                    colDekul.setCellValue(results.landVillageName)
                    ws.addMergedRegion(CellRangeAddress(12, 12, 5, 14))

                    //Rt/rw
                    val colrtRw = rowrtRw.createCell(5)
                    colrtRw.cellStyle = cellStyleHeader3
                    if (results.landRT.toString().isNotEmpty() || results.landRW.toString().isNotEmpty()) colrtRw.setCellValue(results.landRT + "/" + results.landRW) else colrtRw.setCellValue("-")
                    ws.addMergedRegion(CellRangeAddress(13, 13, 5, 14))

                    //Blok/no
                    val colblokNo = rowBlokNo.createCell(5)
                    colblokNo.cellStyle = cellStyleHeader3
                    if (results.landBlok.toString().isNotEmpty()) colblokNo.setCellValue(results.landBlok) else colblokNo.setCellValue("-")
                    ws.addMergedRegion(CellRangeAddress(14, 14, 5, 14))

                    //Jalan
                    val colJln = rowJln.createCell(5)
                    colJln.cellStyle = cellStyleHeader3
                    if (results.landStreet.toString().isNotEmpty()) colJln.setCellValue(results.landStreet) else colJln.setCellValue("-")
                    ws.addMergedRegion(CellRangeAddress(15, 15, 5, 14))

                    //No bidang
                    val colnoBid = rownoBid.createCell(5)
                    colnoBid.cellStyle = cellStyleHeader3
                    colnoBid.setCellValue(results.landNumberList)
                    ws.addMergedRegion(CellRangeAddress(16, 16, 5, 14))

                    //Petugas
                    val colPetugas = rownoPetugas.createCell(5)
                    colPetugas.cellStyle = cellStyleHeader3
                    colPetugas.setCellValue(Preference.auth.dataUser!!.mobileUserEmployeeName)
                    ws.addMergedRegion(CellRangeAddress(17, 17, 5, 14))

                    val dataPemilikPertama = inrealm.where(PartyAdd::class.java).equalTo("TempId", results.subjectId).findFirst()
                    if (dataPemilikPertama != null) {

                        val dataProyek = inrealm.where(ResponseDataListProjectLocal::class.java).equalTo("projectAssignProjectId", results.projectId).findFirst()
                        val colNamaProyek = rowNamaProyek.createCell(1)
                        colNamaProyek.cellStyle = cellStyleHeader
                        colNamaProyek.setCellValue(dataProyek!!.getProjectName().toString().toUpperCase())

                        val colnamaPemilik = rowpemilikTanahName.createCell(5)
                        colnamaPemilik.cellStyle = cellStyleHeader3
                        colnamaPemilik.setCellValue(dataPemilikPertama.partyFullName)
                        ws.addMergedRegion(CellRangeAddress(20, 20, 5, 14))

                        val colttlPemilik = rowpemilikTanahTtl.createCell(5)
                        colttlPemilik.cellStyle = cellStyleHeader3

                        if (dataPemilikPertama.partyBirthPlaceName.toString().isNotEmpty() || dataPemilikPertama.partyBirthDate.toString().isNotEmpty()) colttlPemilik.setCellValue(dataPemilikPertama.partyBirthPlaceName + ", ${dataPemilikPertama.partyBirthDate}") else colttlPemilik.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(21, 21, 5, 14))

                        val colpekerjaanPemilik = rowpemilikTanahPekerjaan.createCell(5)
                        colpekerjaanPemilik.cellStyle = cellStyleHeader3
                        if (dataPemilikPertama.partyOccupationName.toString().isNotEmpty()) colpekerjaanPemilik.setCellValue(dataPemilikPertama.partyOccupationName) else colpekerjaanPemilik.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(22, 22, 5, 14))

                        /* alamat pemilik pertama*/
                        val colalamatJlnPemilik = rowpemilikTanahAlamat.createCell(5)
                        colalamatJlnPemilik.cellStyle = cellStyleHeader3
                        if (dataPemilikPertama.partyStreetName.toString().isNotEmpty()) colalamatJlnPemilik.setCellValue(dataPemilikPertama.partyStreetName) else colalamatJlnPemilik.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(23, 23, 5, 8))

                        val colalamatBlokPemilik = rowpemilikTanahAlamat.createCell(10)
                        colalamatBlokPemilik.cellStyle = cellStyleHeader3
                        if (dataPemilikPertama.partyBlock.toString().isNotEmpty()) colalamatBlokPemilik.setCellValue(dataPemilikPertama.partyBlock) else colalamatBlokPemilik.setCellValue("-")

                        /*val colalamatrtrwPemilik = rowpemilikTanahAlamat.createCell(12)
                        colalamatrtrwPemilik.cellStyle = cellStyleHeader3
                        if (dataPemilikPertama.partyRT.toString().isNotEmpty() || dataPemilikPertama.partyRW.toString().isNotEmpty()) colalamatrtrwPemilik.setCellValue(dataPemilikPertama.partyRT + " / ${dataPemilikPertama.partyRW}") else colalamatrtrwPemilik.setCellValue("-")*/

                        val rowDesa = ws.createRow(24)
                        val colDesa = rowDesa.createCell(5)
                        colDesa.cellStyle = cellStyleHeader3
                        colDesa.setCellValue(dataPemilikPertama.partyVillageName)
                        ws.addMergedRegion(CellRangeAddress(24, 24, 5, 8))

                        val rowKelurahan = ws.createRow(25)
                        val colKelurahan = rowKelurahan.createCell(5)
                        colKelurahan.cellStyle = cellStyleHeader3
                        colKelurahan.setCellValue(dataPemilikPertama.partyDistrictName)
                        ws.addMergedRegion(CellRangeAddress(25, 25, 5, 8))

                        val rowalamatKotaProvPemilik = ws.createRow(26)
                        val colalamatKotaPemilik = rowalamatKotaProvPemilik.createCell(5)
                        colalamatKotaPemilik.cellStyle = cellStyleHeader3
                        colalamatKotaPemilik.setCellValue(dataPemilikPertama.partyRegencyName)
                        ws.addMergedRegion(CellRangeAddress(26, 26, 5, 8))

                        val colalamatProvPemilik = rowalamatKotaProvPemilik.createCell(10)
                        colalamatProvPemilik.cellStyle = cellStyleHeader3
                        colalamatProvPemilik.setCellValue(dataPemilikPertama.partyProvinceName)
                        ws.addMergedRegion(CellRangeAddress(26, 26, 10, 12))
                        /* end of alamat pemilik */

                        val colnoIdentitas = rowpemilikTanahNodIdentitas.createCell(5)
                        colnoIdentitas.cellStyle = cellStyleHeader3
                        if (dataPemilikPertama.partyIdentityNumber.toString().isNotEmpty()) colnoIdentitas.setCellValue(dataPemilikPertama.partyIdentityNumber) else colnoIdentitas.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(27, 27, 5, 8))

                    }

                    val colluasTanah = rowLuasTanah.createCell(5)
                    colluasTanah.cellStyle = cellStyleHeader3
                    if (results.landAreaAffected.toString().isNotEmpty()) colluasTanah.setCellValue(results.landAreaAffected.toString() + " $satuamLuas") else colluasTanah.setCellValue("-")
                    ws.addMergedRegion(CellRangeAddress(28, 28, 5, 7))

                    val landUtilatization = results.landUtilizationLandUtilizationId
                    val landUtilatization1 = landUtilatization.replace("[", "['")
                    val landUtilatization2 = landUtilatization1.replace("]", "']")
                    val landUtilatization3 = landUtilatization2.replace(", ", "','")

                    val liveIncome = results.landUtilizationLandUtilizationName
                    val replac1 = liveIncome.replace("[", "['")
                    val replac2 = replac1.replace("]", "']")
                    val replac3 = replac2.replace(", ", "','")

                    val landUtilatizationNameOther = results.landUtilizationLandUtilizationNameOther
                    val landUtilatizationNameOther1 = landUtilatizationNameOther.replace("[", "['")
                    val landUtilatizationNameOther2 = landUtilatizationNameOther1.replace("]", "']")
                    val landUtilatizationNameOther3 = landUtilatizationNameOther2.replace(", ", "','")

                    val landUtilatizationId = Gson().fromJson(landUtilatization3, Array<String>::class.java).toList()
                    val landUtilatizationName = Gson().fromJson(replac3, Array<String>::class.java).toList()
                    val landUtilatizationNameOtherRep = Gson().fromJson(landUtilatizationNameOther3, Array<String>::class.java).toList()

                    /* penggunaan tanah */
                    var lastRow1 = 31
                    if (landUtilatizationId.isNotEmpty()) {
                        for (element in landUtilatizationId.indices) {
                            lastRow12 = lastRow1++
                            if (element == 0) {
                                rowPenggunaanTanah.let {
                                    it.createCell(5).let {
                                        it.cellStyle = cellStyleNumber
                                        it.setCellValue("${element + 1}")
                                    }

                                    it.createCell(6).let {
                                        it.cellStyle = cellStyleHeader3
                                        if (landUtilatizationNameOtherRep[element].isEmpty()) {
                                            it.setCellValue(landUtilatizationName[element])
                                        } else {
                                            it.setCellValue(landUtilatizationNameOtherRep[element])
                                        }

                                    }
                                }
                                ws.addMergedRegion(CellRangeAddress(30, 30, 6, 8))

                            } else {
                                val rownextPenggunaanTanah = ws.createRow(lastRow12)
                                rownextPenggunaanTanah.let {
                                    it.createCell(5).let {
                                        it.cellStyle = cellStyleNumber
                                        it.setCellValue("${element + 1}")
                                    }
                                    it.createCell(6).let {
                                        it.cellStyle = cellStyleHeader3
                                        if (landUtilatizationNameOtherRep[element].isEmpty()) {
                                            it.setCellValue(landUtilatizationName[element])
                                        } else {
                                            it.setCellValue(landUtilatizationNameOtherRep[element])
                                        }
                                    }
                                }
                                ws.addMergedRegion(CellRangeAddress(lastRow12, lastRow12, 6, 8))
                            }

                        }
                    } else {
                        rowPenggunaanTanah.let {
                            it.createCell(5).let {
                                it.cellStyle = cellStyleNumber
                                it.setCellValue("-")
                            }

                            it.createCell(6).let {
                                it.cellStyle = cellStyleHeader3
                                it.setCellValue("-")
                            }
                        }
                        ws.addMergedRegion(CellRangeAddress(30, 30, 6, 8))
                    }

                    /*rowStatusTanah = ws.createRow(lastRow12 + 1)
                    val coltitleStatusTanah = rowStatusTanah.createCell(2)
                    coltitleStatusTanah.cellStyle = cellStyleHeader3
                    coltitleStatusTanah.setCellValue("- Status Tanah")

                    val coltitleStatusTanah2 = rowStatusTanah.createCell(4)
                    coltitleStatusTanah2.cellStyle = cellStyleNumber
                    coltitleStatusTanah2.setCellValue(":")*/

                    //style
                    val defaultFont = wb!!.createFont()
                    defaultFont.fontHeight = (12.0 * 20).toShort()

                    val cellStyleNormalTitle = wb!!.createCellStyle()
                    cellStyleNormalTitle.wrapText = true
                    cellStyleNormalTitle.setFont(defaultFont)
                    // justify text alignment\
                    cellStyleNormalTitle.setVerticalAlignment(VerticalAlignment.CENTER)


                    /*status tanah */
                    createRowTitle(lastRow12 + 1, 2, "- Status Tanah", cellStyleNormalTitle)
                    rowStatusTanah = ws.getRow(oldLastRow)

                    val coltitleStatusTanah = rowStatusTanah.createCell(4)
                    coltitleStatusTanah.cellStyle = cellStyleNumber
                    coltitleStatusTanah.setCellValue(":")

                    //option 1 status tanah
                    val coloptionNoStatusTanah = rowStatusTanah.createCell(5)
                    coloptionNoStatusTanah.cellStyle = cellStyleNumber
                    coloptionNoStatusTanah.setCellValue("1")

                    val coloptionValueStatusTanah1 = rowStatusTanah.createCell(6)
                    coloptionValueStatusTanah1.cellStyle = cellStyleHeader3
                    if (results.landOwnershipNameOther.isNotEmpty()) coloptionValueStatusTanah1.setCellValue(results.landOwnershipNameOther) else coloptionValueStatusTanah1.setCellValue(results.landOwnershipName)
                    ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 6, 8))

                    /*val collanNIBTanah = rowStatusTanah.createCell(12)
                    collanNIBTanah.cellStyle = cellStyleHeader3
                    collanNIBTanah.setCellValue("No.${results.landCertificateNumber}")
                    ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 12, 14))*/

                    //
                    createRowTitle(currentLastRow, 6, "Luas ${results.landAreaCertificate} $satuamLuas", cellStyleNormalTitle)
                    ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 6, 8))
                    rowluasStatusTanah = ws.getRow(oldLastRow)

                    val colatsNamaStatusTanah = rowluasStatusTanah.createCell(9)
                    colatsNamaStatusTanah.cellStyle = cellStyleNumber
                    colatsNamaStatusTanah.setCellValue("a/n")

                    //option 2 status tanah
                    createRowTitle(currentLastRow, 5, "2", cellStyleNumber)
                    rowoptionNameStatusTanah2 = ws.getRow(oldLastRow)

                    val colnameoptionSatatusTanah2 = rowoptionNameStatusTanah2.createCell(6)
                    colnameoptionSatatusTanah2.cellStyle = cellStyleHeader3
                    colnameoptionSatatusTanah2.setCellValue("Bekas")
                    ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 6, 8))

                    /*val colnooptionSatatusTanah2 = rowoptionNameStatusTanah2.createCell(12)
                    colnooptionSatatusTanah2.cellStyle = cellStyleHeader3
                    colnooptionSatatusTanah2.setCellValue("No.")
                    ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 12, 14))*/

                    createRowTitle(currentLastRow, 6, "Luas - $satuamLuas", cellStyleHeader3)
                    rowoptionLuasStatusTanah2 = ws.getRow(oldLastRow)
                    ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 6, 8))

                    val coloptionLuasStatusTanah2 = rowoptionLuasStatusTanah2.createCell(9)
                    coloptionLuasStatusTanah2.cellStyle = cellStyleNumber
                    coloptionLuasStatusTanah2.setCellValue("a/n")

                    //option 3 status tanah
                    createRowTitle(currentLastRow, 5, "3", cellStyleNumber)
                    rowoptionNameStatusTanah3 = ws.getRow(oldLastRow)

                    val colnameoptionSatatusTanah3 = rowoptionNameStatusTanah3.createCell(6)
                    colnameoptionSatatusTanah3.cellStyle = cellStyleHeader3
                    colnameoptionSatatusTanah3.setCellValue("Tanah Pemerintah / Pemprov / Pemkab / Pemdes")
                    ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 6, 12))

                    //option 4 status tanah
                    createRowTitle(currentLastRow, 5, "4", cellStyleNumber)
                    rowoptionNameStatusTanah4 = ws.getRow(oldLastRow)

                    val colnameoptionSatatusTanah4 = rowoptionNameStatusTanah4.createCell(6)
                    colnameoptionSatatusTanah4.cellStyle = cellStyleHeader3
                    colnameoptionSatatusTanah4.setCellValue("Tanah Negara")
                    ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 6, 8))

                    /* bukti kepemilikan */
                    createRowTitle(currentLastRow, 2, "- Bukti Penguasaan/Dokumen Kepemilikan", cellStyleNormalTitle)
                    rowtitleBuktiKepemilikan = ws.getRow(oldLastRow)
                    ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 2, 3))

                    val coltitleBuktiKepemilkanTanah = rowtitleBuktiKepemilikan.createCell(4)
                    coltitleBuktiKepemilkanTanah.cellStyle = cellStyleNumber
                    coltitleBuktiKepemilkanTanah.setCellValue(":")

                    /* option kepemilikan tanah 1*/
                    val coloptionKepemilkanTanah1 = rowtitleBuktiKepemilikan.createCell(5)
                    coloptionKepemilkanTanah1.cellStyle = cellStyleNumber
                    coloptionKepemilkanTanah1.setCellValue("1")

                    val coloptionnameKepemilkanTanah1 = rowtitleBuktiKepemilikan.createCell(6)
                    coloptionnameKepemilkanTanah1.cellStyle = cellStyleHeader3
                    coloptionnameKepemilkanTanah1.setCellValue("")
                    ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 6, 14))

                    /* option kepemilikan tanah 2*/
                    createRowTitle(currentLastRow, 5, "2", cellStyleNumber)
                    rowoptionBuktiKepemilikan2 = ws.getRow(oldLastRow)

                    val coloptionnameKepemilkanTanah2 = rowoptionBuktiKepemilikan2.createCell(6)
                    coloptionnameKepemilkanTanah2.cellStyle = cellStyleHeader3
                    coloptionnameKepemilkanTanah2.setCellValue("")
                    ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 6, 14))

                    /* option kepemilikan tanah 3*/
                    createRowTitle(currentLastRow, 5, "3", cellStyleNumber)
                    rowoptionBuktiKepemilikan3 = ws.getRow(oldLastRow)

                    val coloptionnameKepemilkanTanah3 = rowoptionBuktiKepemilikan3.createCell(6)
                    coloptionnameKepemilkanTanah3.cellStyle = cellStyleHeader3
                    coloptionnameKepemilkanTanah3.setCellValue("")
                    ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 6, 14))

                    /* option kepemilikan tanah 4*/
                    createRowTitle(currentLastRow, 5, "4", cellStyleNumber)
                    rowoptionBuktiKepemilikan4 = ws.getRow(oldLastRow)

                    val coloptionnameKepemilkanTanah4 = rowoptionBuktiKepemilikan4.createCell(6)
                    coloptionnameKepemilkanTanah4.cellStyle = cellStyleHeader3
                    coloptionnameKepemilkanTanah4.setCellValue("")
                    ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 6, 14))


                    /* Lama kepemilikan tanah */
                    createRowTitle(currentLastRow, 2, "- Lama Menguasai / Memiliki Tanah", cellStyleHeader3)
                    rowtitleLamaKepemilikanTanah = ws.getRow(oldLastRow)
                    ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 2, 3))

                    val coltitleLamaKepemilkanTanah = rowtitleLamaKepemilikanTanah.createCell(4)
                    coltitleLamaKepemilkanTanah.cellStyle = cellStyleNumber
                    coltitleLamaKepemilkanTanah.setCellValue(":")

                    val colLamaKepemilkanTanah = rowtitleLamaKepemilikanTanah.createCell(5)
                    colLamaKepemilkanTanah.cellStyle = cellStyleHeader3
                    colLamaKepemilkanTanah.setCellValue("${dataPemilikPertama?.partyYearStayBegin} Tahun")
                    ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 5, 8))

                    /* Sita jaminan */
                    createRowTitle(currentLastRow, 2, "- Pembebanan Hak / Sita Jaminan", cellStyleHeader3)
                    rowtitleSitaJaminanTanah = ws.getRow(oldLastRow)
                    ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 2, 3))

                    val coltitleSitaJamaninanTanah = rowtitleSitaJaminanTanah.createCell(4)
                    coltitleSitaJamaninanTanah.cellStyle = cellStyleNumber
                    coltitleSitaJamaninanTanah.setCellValue(":")

                    /* Bagian 3*/
                    val dataPemilikKedua = inrealm.where(Subjek2::class.java).equalTo("LandIdTemp", landId).findAll()
                    if (dataPemilikKedua != null) {
                        createRowTitle2(currentLastRow + 1, 1, "III", cellStyleNoWithBold)
                        rowtitleBagian3 = ws.getRow(oldLastRow2)

                        val coltitleBagian3 = rowtitleBagian3.createCell(2)
                        coltitleBagian3.cellStyle = cellStyleTitleWithBold
                        coltitleBagian3.setCellValue("PENGGARAP / PENYEWA / PENGUASAAN TANAH")
                        ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 2, 3))

                        for (element in 0 until dataPemilikKedua.size) {
                            /*val rownameOwner2 = ws.createRow(firstRowDataPemilikKedua++)
                            val coltitlenameOwner2 = rownameOwner2.createCell(2)*/
                            createRowTitle2(currentLastRow2, 2, "- Nama Lengkap", cellStyleNormalTitle)

                            val rownameOwner2 = ws.getRow(oldLastRow2)
                            val coltitlenameOwner2 = rownameOwner2.createCell(4)
                            coltitlenameOwner2.cellStyle = cellStyleNumber
                            coltitlenameOwner2.setCellValue(":")

                            val colnameOwner2 = rownameOwner2.createCell(5)
                            colnameOwner2.cellStyle = cellStyleHeader3
                            colnameOwner2.setCellValue(dataPemilikKedua[element]?.subjek2FullName.toString())
                            ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 5, 14))

                            /* tempat tgl lahir */
                            createRowTitle2(currentLastRow2, 2, "- Tempat / Tanggal Lahir", cellStyleNormalTitle)
                            val rowttlOwner2 = ws.getRow(oldLastRow2)
                            val coltitlettlOwner2 = rowttlOwner2.createCell(4)
                            coltitlettlOwner2.cellStyle = cellStyleNumber
                            coltitlettlOwner2.setCellValue(":")

                            val colttlOwner2 = rowttlOwner2.createCell(5)
                            colttlOwner2.cellStyle = cellStyleHeader3
                            if (!dataPemilikKedua[element]?.subjek2BirthPlaceName.isNullOrEmpty() || dataPemilikKedua[element]?.subjek2BirthDate.toString().isNotEmpty()) colttlOwner2.setCellValue("${dataPemilikKedua[element]?.subjek2BirthPlaceName}, ${dataPemilikKedua[element]?.subjek2BirthDate}") else colttlOwner2.setCellValue("-")
                            ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 5, 14))

                            /* pekerjaan */
                            createRowTitle2(currentLastRow2, 2, "- Pekerjaan", cellStyleNormalTitle)
                            val rowpekerjaanOwner2 = ws.getRow(oldLastRow2)
                            val coltitlepekerjaanOwner2 = rowttlOwner2.createCell(4)
                            coltitlepekerjaanOwner2.cellStyle = cellStyleNumber
                            coltitlepekerjaanOwner2.setCellValue(":")

                            val colpekerjaanOwner2 = rowpekerjaanOwner2.createCell(5)
                            colpekerjaanOwner2.cellStyle = cellStyleHeader3
                            if (!dataPemilikKedua[element]?.subjek2OccupationName.isNullOrEmpty()) colpekerjaanOwner2.setCellValue(dataPemilikKedua[element]?.subjek2OccupationName) else colpekerjaanOwner2.setCellValue("-")
                            ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 5, 14))

                            /* Alamat */
                            createRowTitle2(currentLastRow2, 2, "- Alamat", cellStyleNormalTitle)
                            val rowalamat1 = ws.getRow(oldLastRow2)
                            val coltitlealamatOwner2 = rowalamat1.createCell(4)
                            coltitlealamatOwner2.cellStyle = cellStyleNumber
                            coltitlealamatOwner2.setCellValue(":")

                            val colJlnAlamatOwner2 = rowalamat1.createCell(5)
                            colJlnAlamatOwner2.cellStyle = cellStyleHeader3
                            if (!dataPemilikKedua[element]?.subjek2StreetName.isNullOrEmpty()) colJlnAlamatOwner2.setCellValue(dataPemilikKedua[element]?.subjek2StreetName) else colJlnAlamatOwner2.setCellValue("-")
                            ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 5, 8))

                            val colblokAlamatOwner2 = rowalamat1.createCell(11)
                            colblokAlamatOwner2.cellStyle = cellStyleHeader3
                            if (!dataPemilikKedua[element]?.subjek2Block.isNullOrEmpty()) colblokAlamatOwner2.setCellValue(dataPemilikKedua[element]?.subjek2Block) else colblokAlamatOwner2.setCellValue("-")

                            val colrtrwkAlamatOwner2 = rowalamat1.createCell(13)
                            colrtrwkAlamatOwner2.cellStyle = cellStyleHeader3
                            colrtrwkAlamatOwner2.setCellValue("RT ${dataPemilikKedua[element]?.subjek2RT} / ${dataPemilikKedua[element]?.subjek2RW}")
                            if (dataPemilikKedua[element]?.subjek2RT.toString().isNotEmpty() || dataPemilikKedua[element]?.subjek2RW.toString().isNotEmpty()) colrtrwkAlamatOwner2.setCellValue(dataPemilikKedua[element]?.subjek2RT + "/" + dataPemilikKedua[element]?.subjek2RW) else colrtrwkAlamatOwner2.setCellValue("-")

                            /* desa */
                            createRowTitle2(currentLastRow2, 5, dataPemilikKedua[element]?.subjek2VillageName, cellStyleNormalTitle)
                            ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 5, 8))

                            /*kecamatan*/
                            createRowTitle2(currentLastRow2, 5, dataPemilikKedua[element]?.subjek2DistrictName, cellStyleNormalTitle)
                            ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 5, 8))

                            /*kota & provinsi*/
                            createRowTitle2(currentLastRow2, 5, dataPemilikKedua[element]?.subjek2RegencyName, cellStyleNormalTitle)
                            ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 5, 8))

                            val rowkotaProvinsi = ws.getRow(oldLastRow2)
                            val colProvinsiAlamatOwner2 = rowkotaProvinsi.createCell(11)
                            colProvinsiAlamatOwner2.cellStyle = cellStyleHeader3
                            if (!dataPemilikKedua[element]?.subjek2ProvinceName.isNullOrEmpty()) colProvinsiAlamatOwner2.setCellValue(dataPemilikKedua[element]?.subjek2ProvinceName) else colProvinsiAlamatOwner2.setCellValue("-")
                            ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 11, 13))

                            /*nik / no sim*/
                            createRowTitle2(currentLastRow2, 2, "- NIK / No. SIM", cellStyleNormalTitle)

                            val rownoIdentitasOwner2 = ws.getRow(oldLastRow2)
                            val coltitleIdentitasOwner2 = rownoIdentitasOwner2.createCell(4)
                            coltitleIdentitasOwner2.cellStyle = cellStyleNumber
                            coltitleIdentitasOwner2.setCellValue(":")

                            val colnoIdentitasOwner2 = rownoIdentitasOwner2.createCell(5)
                            colnoIdentitasOwner2.cellStyle = cellStyleHeader3
                            if (!dataPemilikKedua[element]?.subjek2IdentityNumber.isNullOrEmpty()) colnoIdentitasOwner2.setCellValue(dataPemilikKedua[element]?.subjek2IdentityNumber) else colnoIdentitasOwner2.setCellValue("-")
                            ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 5, 8))

                            /* luas tanah yg digarap */
                            createRowTitle2(currentLastRow2, 2, "- Luas Tanah Digarap / Disewa", cellStyleNormalTitle)
                            ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 2, 3))
                            val rowluasTanahOwner2 = ws.getRow(oldLastRow2)
                            val coltitleluasTanahOwner2 = rowluasTanahOwner2.createCell(4)
                            coltitleluasTanahOwner2.cellStyle = cellStyleNumber
                            coltitleluasTanahOwner2.setCellValue(":")

                            val colluasTanahOwner2 = rowluasTanahOwner2.createCell(5)
                            colluasTanahOwner2.cellStyle = cellStyleHeader3
                            colluasTanahOwner2.setCellValue("-")
                            ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 5, 7))

                            createRowTitle2(currentLastRow2, 2, "", cellStyleNormalTitle)

                            /*penggunaan tanah yg digarap */
                            createRowTitle2(currentLastRow2, 2, "- Penggunaan / Pemanfaatan Tanah", cellStyleNormalTitle)
                            ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 2, 3))

                            val rowpenggunaantanahOwner2 = ws.getRow(oldLastRow2)
                            val coltitlepenggunaantanahOwner2 = rowpenggunaantanahOwner2.createCell(4)
                            coltitlepenggunaantanahOwner2.cellStyle = cellStyleNumber
                            coltitlepenggunaantanahOwner2.setCellValue(":")

                            /* jangka waktu sewa */
                            createRowTitle2(currentLastRow2, 2, "- Jangka Waktu Sewa", cellStyleNormalTitle)
                            ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 2, 3))

                            val rowlamaSewatanahOwner2 = ws.getRow(oldLastRow2)
                            val coltitlelamaSewatanahOwner2 = rowlamaSewatanahOwner2.createCell(4)
                            coltitlelamaSewatanahOwner2.cellStyle = cellStyleNumber
                            coltitlelamaSewatanahOwner2.setCellValue(":")

                            currentLastRow2++
                        }
                    } else {
                        createRowTitle2(currentLastRow + 1, 1, "", cellStyleNormalTitle)
                    }

                    /* tanda tangan */
                    createRowTitle2(currentLastRow2 + 1, 1, "", cellStyleHeader3)
                    val rowdateTTD = ws.getRow(oldLastRow2)
                    val colttD = rowdateTTD.createCell(7)
                    colttD.cellStyle = cellStyleTglTtd
                    colttD.setCellValue("Tempat, $currentDate")
                    ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 7, 10))

                    /* nama ttd petugas & pemilik tanah */
                    createRowTitle2(currentLastRow2 + 1, 0, "Petugas Satgas B", cellStyleTtd)
                    ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 0, 2))

                    val rowttd1 = ws.getRow(oldLastRow2)
                    val colttd1 = rowttd1.createCell(7)
                    colttd1.cellStyle = cellStyleTtd
                    colttd1.setCellValue("Pemilik Tanah")
                    ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 7, 10))

                    /* garis ttd petugas & pemilik tanah */
                    createRowTitle2(currentLastRow2 + 4, 0, "___________________________________", cellStyleTtd)
                    ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 0, 2))

                    val rowgaristtd1 = ws.getRow(oldLastRow2)
                    val colgaristtd1 = rowgaristtd1.createCell(7)
                    colgaristtd1.cellStyle = cellStyleTtd
                    colgaristtd1.setCellValue("___________________________________")
                    ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 7, 10))

                    /* nama ttd mengetahui  */
                    createRowTitle2(currentLastRow2 + 2, 3, "Mengetahui", cellStyleTtd)
                    ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 3, 6))

                    createRowTitle2(currentLastRow2 + 4, 3, "___________________________________", cellStyleTtd)
                    ws.addMergedRegion(CellRangeAddress(oldLastRow2, oldLastRow2, 3, 6))

                }
            },
                {
                    activity.runOnUiThread {
                        val pathDir = File(Environment.getExternalStorageDirectory().toString() + File.separator + "SIOJT", "Downloads")
                        val outputFile = File(pathDir, mfilename)

                        var ostream2: FileOutputStream? = null
                        try {
                            if (!pathDir.exists()) {
                                pathDir.mkdirs()
                            }

                            if (!outputFile.exists()) {
                                outputFile.delete()
                                outputFile.createNewFile()
                            }

                            ostream2 = FileOutputStream(outputFile)
                            wb?.write(ostream2)
                            ostream2.flush()
                        } catch (e: FileNotFoundException) {
                            e.printStackTrace()
                            Log.d("Error", "${e.message}")

                        } catch (e: Exception) {
                            e.printStackTrace()
                            Log.d("Error", "Export File Gagal")
                        } finally {
                            if (ostream2 != null) {
                                ostream2.close()
                                Log.d("kuy", lastRow12.toString())
                                try {
                                    sweetAlretLoading = SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                    sweetAlretLoading?.titleText = "Export Data Berhasil"
                                    sweetAlretLoading?.setCancelable(false)
                                    sweetAlretLoading?.contentText = outputFile.toString()
                                    sweetAlretLoading?.confirmText = "OK"
                                    sweetAlretLoading?.setConfirmClickListener { sDialog ->
                                        sDialog?.let { if (it.isShowing) it.dismiss() }
                                        openFile(mfilename)
                                    }
                                    sweetAlretLoading?.show()
                                } catch (e: IOException) {
                                    Log.d("Error", "File gagal dibuka ${e.message}")
                                }
                            }


                        }
                    }


                },
                {
                    Log.d("Error", "Gagal export data")
                    Log.d("Error", it.toString())
                    activity.runOnUiThread {
                        sweetAlretLoading = SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                        sweetAlretLoading?.titleText = "Failed"
                        sweetAlretLoading?.setCancelable(false)
                        sweetAlretLoading?.contentText = "Export Data Gagal"
                        sweetAlretLoading?.confirmText = "OK"
                        sweetAlretLoading?.setConfirmClickListener { sDialog ->
                            sDialog?.let { if (it.isShowing) it.dismiss() }
                        }
                        sweetAlretLoading?.show()
                    }
                }
            )
        }
    }

    @SuppressLint("DefaultLocale")
    private fun convertDocToPdfitext(landId: Int?, projectName: String) {
        val document: com.itextpdf.text.Document = Document(PageSize.A4.rotate(), 40f, 40f, 35f, 40f)
        val pathDir = File(Environment.getExternalStorageDirectory().toString() + File.separator + "SIOJT", "Downloads")
        val mfileName = "$projectName.pdf"
        val outputFile = File(pathDir, mfileName)

        Handler().postDelayed({
            try {
                val fos = FileOutputStream(outputFile)

                val pdfWriter = PdfWriter.getInstance(document, fos)
                document.open()

                val fontBold = Font(Font.FontFamily.HELVETICA, 16f, Font.BOLD)
                val headerTitle1 = Paragraph("PELAKSANAAN PENGADAAN TANAH UNTUK PEMBANGUNAN", fontBold)
                headerTitle1.alignment = Element.ALIGN_CENTER

                val headerTitle12 = Paragraph("DATA PEMILIK TANAH YANG TERKENA PENGADAAN TANAH", fontBold)
                headerTitle12.alignment = Element.ALIGN_CENTER
                headerTitle12.spacingAfter = 20f

                var headerproyekName: Paragraph? = null
                realm.executeTransaction { inrealm ->
                    val results = inrealm.where(ProjectLand::class.java).equalTo("LandIdTemp", landId).findFirst()
                    if (results != null) {
                        val dataProyek = inrealm.where(ResponseDataListProjectLocal::class.java).equalTo("projectAssignProjectId", results.projectId).findFirst()
                        headerproyekName = Paragraph(dataProyek!!.getProjectName().toString().toUpperCase(), fontBold)
                        headerproyekName!!.alignment = Element.ALIGN_CENTER
                        headerproyekName!!.spacingAfter = 20f
                    }
                }

                document.add(headerTitle1)
                document.add(headerproyekName)
                document.add(headerTitle12)

                val firstTable = createFirstTable(landId)
                val secondTable = createSecondTable(landId)
                document.add(firstTable)
                document.add(secondTable)

                document.close()
                pdfWriter.close()
            } catch (e: DocumentException) {
                Log.d("DocumentException", e.message.toString())
            } catch (e: Exception) {
                Log.d("Exception", e.message.toString())
            } finally {
                activity.runOnUiThread {
                    sweetAlretLoading = SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                    sweetAlretLoading!!.titleText = "Export Data Berhasil"
                    sweetAlretLoading!!.setCancelable(false)
                    sweetAlretLoading!!.contentText = outputFile.toString()
                    sweetAlretLoading!!.confirmText = "OK"
                    sweetAlretLoading!!.setConfirmClickListener { sDialog ->
                        sDialog?.let { if (it.isShowing) it.dismiss() }
                        openFile(mfileName)
                    }
                    sweetAlretLoading!!.show()
                }
            }
        }, 5000)

    }

    private fun createFirstTable(landId: Int?): PdfPTable {
        val table = PdfPTable(15)
        table.defaultCell.border = 0
        //setColumnWidth
        table.setTotalWidth(floatArrayOf(20f, 144f, 144f, 20f, 20f,
            50f, 50f, 50f, 50f, 50f,
            50f, 50f, 50f, 50f, 20f))
        table.widthPercentage = 100f

        //Letak tanah
        var letakProvinsi = ""
        var letakKota = ""
        var letakKecamatan = ""
        var letakKelurahan = ""
        var letakRTRW = ""
        var letakNo = ""
        var letakJalan = ""
        var letakNoBidang = ""

        //Pemilik pertama tanah
        var namaPemilikPertama = ""
        var ttlPemilikPertama = ""
        var pekerjaanPemilikPertama = ""
        var alamatJlnPemilikPertama = ""
        var alamatBlokPemilikPertama = ""
        var alamatRtRwPemilikPertama = ""
        var alamatKelurahanPemilikPertama = ""
        var alamatKecamatanPemilikPertama = ""
        var alamatKotaPemilikPertama = ""
        var alamatProvinsiPemilikPertama = ""

        var nomorIdentitasPemilikPertama = ""
        var luasTerkenaDampakTanah = ""
        val penggunaanTanah: ArrayList<String> = ArrayList()
        var statusTanah = ""
        var luastotalTanah = ""
        var lamaTinggalTanah = ""


        realm.executeTransaction { inrealm ->
            val results = inrealm.where(ProjectLand::class.java).equalTo("LandIdTemp", landId).findFirst()
            if (results != null) {
                letakProvinsi = ""
                letakKota = ""
                letakKecamatan = ""
                letakKelurahan = ""
                letakRTRW = ""
                letakNo = ""
                letakJalan = ""
                letakNoBidang = ""

                namaPemilikPertama = ""
                ttlPemilikPertama = ""
                pekerjaanPemilikPertama = ""
                alamatJlnPemilikPertama = ""
                alamatBlokPemilikPertama = ""
                alamatRtRwPemilikPertama = ""
                alamatKelurahanPemilikPertama = ""
                alamatKecamatanPemilikPertama = ""
                alamatKotaPemilikPertama = ""
                alamatProvinsiPemilikPertama = ""

                nomorIdentitasPemilikPertama = ""
                luasTerkenaDampakTanah = ""
                penggunaanTanah.clear()
                statusTanah = ""
                luastotalTanah = ""
                lamaTinggalTanah = ""

                //letak tanah
                letakProvinsi = if (results.landProvinceName.isNotEmpty()) {
                    results.landProvinceName
                } else {
                    "-"
                }

                letakKota = if (results.landRegencyName.isNotEmpty()) {
                    results.landRegencyName
                } else {
                    "-"
                }

                letakKecamatan = if (results.landDistrictName.isNotEmpty()) {
                    results.landDistrictName
                } else {
                    "-"
                }

                letakKelurahan = if (results.landVillageName.isNotEmpty()) {
                    results.landVillageName
                } else {
                    "-"
                }

                letakRTRW = if (results.landRT.isNotEmpty() || results.landRW.isNotEmpty()) {
                    "${results.landRT} / ${results.landRW}"
                } else {
                    "-"
                }

                letakJalan = if (results.landStreet.isNotEmpty()) {
                    results.landStreet
                } else {
                    "-"
                }

                letakNo = if (results.landBlok.isNotEmpty()) {
                    results.landBlok
                } else {
                    "-"
                }

                letakNoBidang = if (results.landNumberList.isNotEmpty()) {
                    results.landNumberList
                } else {
                    "-"
                }

                val dataPemilikPertama = inrealm.where(PartyAdd::class.java).equalTo("TempId", results.subjectId).findFirst()
                if (dataPemilikPertama != null) {

                    val tmpLahirPemilik = if (dataPemilikPertama.partyBirthPlaceName.toString().isNotEmpty()) {
                        dataPemilikPertama.partyBirthPlaceName.toString()
                    } else {
                        "-"
                    }

                    val tglLahirPemilik = if (dataPemilikPertama.partyBirthDate.toString().isNotEmpty()) {
                        dataPemilikPertama.partyBirthDate.toString()
                    } else {
                        "-"
                    }

                    val alamartRT = if (dataPemilikPertama.partyRT.toString().isNotEmpty()) {
                        dataPemilikPertama.partyRT.toString()
                    } else {
                        "-"
                    }

                    val alamartRW = if (dataPemilikPertama.partyRW.toString().isNotEmpty()) {
                        dataPemilikPertama.partyRW.toString()
                    } else {
                        "-"
                    }

                    namaPemilikPertama = dataPemilikPertama.partyFullName
                    ttlPemilikPertama = "$tmpLahirPemilik, $tglLahirPemilik"
                    pekerjaanPemilikPertama = if (dataPemilikPertama.partyOccupationName.toString().isNotEmpty()) {
                        dataPemilikPertama.partyOccupationName.toString()
                    } else {
                        "-"
                    }

                    /* alamat pemilik pertama*/
                    alamatJlnPemilikPertama = if (dataPemilikPertama.partyStreetName.toString().isNotEmpty()) {
                        dataPemilikPertama.partyStreetName.toString()
                    } else {
                        "-"
                    }

                    alamatBlokPemilikPertama = if (dataPemilikPertama.partyBlock.toString().isNotEmpty()) {
                        dataPemilikPertama.partyBlock.toString()
                    } else {
                        "-"
                    }

                    alamatRtRwPemilikPertama = "$alamartRT / $alamartRW"
                    alamatKelurahanPemilikPertama = dataPemilikPertama.partyVillageName
                    alamatKecamatanPemilikPertama = dataPemilikPertama.partyDistrictName
                    alamatKotaPemilikPertama = dataPemilikPertama.partyRegencyName
                    alamatProvinsiPemilikPertama = dataPemilikPertama.partyProvinceName
                    nomorIdentitasPemilikPertama = if (dataPemilikPertama.partyIdentityNumber.toString().isNotEmpty()) {
                        dataPemilikPertama.partyIdentityNumber
                    } else {
                        "-"
                    }

                    //lama tinggal
                    lamaTinggalTanah = "${dataPemilikPertama.partyYearStayBegin.toString()} Tahun"
                }

                luasTerkenaDampakTanah = if (results.landAreaAffected.toString().isNotEmpty()) {
                    results.landAreaAffected.toString()
                } else {
                    "-"
                }


                val landUtilatization = results.landUtilizationLandUtilizationId
                val landUtilatization1 = landUtilatization.replace("[", "['")
                val landUtilatization2 = landUtilatization1.replace("]", "']")
                val landUtilatization3 = landUtilatization2.replace(", ", "','")

                val liveIncome = results.landUtilizationLandUtilizationName
                val replac1 = liveIncome.replace("[", "['")
                val replac2 = replac1.replace("]", "']")
                val replac3 = replac2.replace(", ", "','")

                val landUtilatizationNameOther = results.landUtilizationLandUtilizationNameOther
                val landUtilatizationNameOther1 = landUtilatizationNameOther.replace("[", "['")
                val landUtilatizationNameOther2 = landUtilatizationNameOther1.replace("]", "']")
                val landUtilatizationNameOther3 = landUtilatizationNameOther2.replace(", ", "','")

                val landUtilatizationId = Gson().fromJson(landUtilatization3, Array<String>::class.java).toList()
                val landUtilatizationName = Gson().fromJson(replac3, Array<String>::class.java).toList()
                val landUtilatizationNameOtherRep = Gson().fromJson(landUtilatizationNameOther3, Array<String>::class.java).toList()

                /* penggunaan tanah */
                for ((indexLanUtilatizationId, valueLandUtilazaionId) in landUtilatizationId.withIndex()) {
                    if (landUtilatizationNameOtherRep[indexLanUtilatizationId].isEmpty()) {
                        penggunaanTanah.add("${indexLanUtilatizationId + 1}. ${landUtilatizationName[indexLanUtilatizationId]}")
                    } else {
                        penggunaanTanah.add("${indexLanUtilatizationId + 1}. ${landUtilatizationNameOtherRep[indexLanUtilatizationId]}")
                    }
                }

                //status tanah
                statusTanah = if (results.landOwnershipNameOther.isNotEmpty()) {
                    "1 ${results.landOwnershipNameOther}"
                } else {
                    "1 ${results.landOwnershipName}"
                }

                //total luas tanah
                luastotalTanah = if (results.landAreaCertificate.isNotEmpty()) {
                    "Luas ${results.landAreaCertificate} $satuamLuas"
                } else {
                    "-"
                }

            }
        }


        table.addCell(createCellItextPdfBoldTextCenter("I", true))
        table.addCell(createCellItextPdfWithColspanBold("LETAK TANAH", 14, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, true))

        //Provinsi
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Provinsi", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakProvinsi, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Kota
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Kota", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakKota, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Kabupaten
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Kabupaten", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakKota, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Kecamatan
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Kecamatan", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakKecamatan, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Desa/Kelurahan
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Desa / Kelurahan", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakKelurahan, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //RT/RW
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- RT / RW", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakRTRW, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //RT/RW
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Jalan", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakJalan, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Blok/No
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Blok / No", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakNo, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //No bidang
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- No Bidang", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakNoBidang, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Petugas Satgas
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Petugas Satgas B", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(Preference.auth.dataUser!!.mobileUserEmployeeName.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //space letak dan pemilik pertama tanah
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Pemilik tanah pertama
        table.addCell(createCellItextPdfBoldTextCenter("II", true))
        table.addCell(createCellItextPdfWithColspanBold("PEMILIK TANAH", 14, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, true))

        //nama pemilik pertama
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Nama Lengkap", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(namaPemilikPertama, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //ttl pemilikpertama
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Tempat / Tanggal Lahir", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(ttlPemilikPertama, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //pekerjaan pemilik pertama
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Pekerjaan", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(pekerjaanPemilikPertama, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //alamat pemilik pertama
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Alamat", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        //alamat jalan
        table.addCell(createCellItextPdfWithColspanBold(alamatJlnPemilikPertama, 4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell("")
        //alamat blok
        table.addCell(createCellItextPdfCustomPositionText(alamatBlokPemilikPertama, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell("")
        //alamat rt rw
        table.addCell(createCellItextPdfWithColspanBold(alamatRtRwPemilikPertama, 4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        //alamat kelurahan
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold(alamatKelurahanPemilikPertama, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        //alamat kecamatan
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold(alamatKecamatanPemilikPertama, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        //alamat kota & provinsi
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold(alamatKotaPemilikPertama, 4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold(alamatProvinsiPemilikPertama, 6, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Nik / no. SIM
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- NIK / No. SIM", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(nomorIdentitasPemilikPertama, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //luas tanah terkena dampak
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Luas Tanah Terkena", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold("$luasTerkenaDampakTanah $satuamLuas", 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //penggunaan tanah
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Penggunaan / Pemanfaatan Tanah", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        for (element in penggunaanTanah.indices) {
            if (element == 0) {
                table.addCell(createCellItextPdfWithColspanBold(penggunaanTanah[element], 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
            } else {
                table.addCell("")
                table.addCell("")
                table.addCell("")
                table.addCell("")
                table.addCell(createCellItextPdfWithColspanBold(penggunaanTanah[element], 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
            }
        }

        //status tanah  1
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Status Tanah", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(statusTanah, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //luas tanah 1
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold(luastotalTanah, 4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("a/n", 7, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //status tanah 2
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("2 Bekas", 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //luas tanah 2
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("Luas - $satuamLuas", 4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("a/n", 7, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //status tanah 3
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("3 Tanah Pemerintah / Pemprov / Pemkab / Pemdes", 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //status tanah 4
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("4 Tanah Negara", 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))


        //Bukti Penguasaan/Dokumen Kepemilikan
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Bukti Penguasaan/Dokumen Kepemilikan", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold("1", 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        //Bukti Penguasaan/Dokumen Kepemilikan 2
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("2", 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        //Bukti Penguasaan/Dokumen Kepemilikan 3
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("3", 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        //Bukti Penguasaan/Dokumen Kepemilikan 4
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("4", 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Lama menguasai tanah
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Lama Menguasai / Memiliki Tanah", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(lamaTinggalTanah, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //hak sita
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Pembebanan Hak / Sita Jaminan", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //space letak dan pemilik pertama tanah
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Pemilik tanah kedua
        table.addCell(createCellItextPdfBoldTextCenter("III", true))
        table.addCell(createCellItextPdfWithColspanBold("PENGGARAP / PENYEWA / PENGUASAAN TANAH", 14, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, true))

        return table
    }

    private fun createSecondTable(landId: Int?): PdfPTable {
        val table = PdfPTable(15)
        table.defaultCell.border = 0
        //setColumnWidth
        table.setTotalWidth(floatArrayOf(20f, 144f, 144f, 20f, 20f,
            50f, 50f, 50f, 50f, 50f,
            50f, 50f, 50f, 50f, 20f))
        table.widthPercentage = 100f

        val calendar = Calendar.getInstance(Locale.getDefault())
        //c.get(Calendar.YEAR)
        //c.get(Calendar.MONTH) + 1
        //c.get(Calendar.DAY_OF_MONTH)
        //val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        //val currentDate = sdf.format(c.time)
        val currentDate = DateFormat.format(dateTemplate, calendar.time)


        //Pemilik kedua tanah
        var namaPemilikKedua = ""
        var ttlPemilikKedua = ""
        var pekerjaanPemilikKedua = ""
        var alamatJlnPemilikKedua = ""
        var alamatBlokPemilikKedua = ""
        var alamatRtRwPemilikKedua = ""
        var alamatKelurahanPemilikKedua = ""
        var alamatKecamatanPemilikKedua = ""
        var alamatKotaPemilikKedua = ""
        var alamatProvinsiPemilikKedua = ""
        var nomorIdentitasPemilikKedua = ""

        realm.executeTransaction { inrealm ->
            val dataPemilikKedua = inrealm.where(Subjek2::class.java).equalTo("LandIdTemp", landId).findAll()
            if (dataPemilikKedua.isNotEmpty()) {
                for ((indexPemilikKedua, valuePemilikKedua) in dataPemilikKedua.withIndex()) {
                    val tmpLahirPemilik = if (valuePemilikKedua.subjek2BirthPlaceName.toString().isNotEmpty()) {
                        valuePemilikKedua.subjek2BirthPlaceName.toString()
                    } else {
                        "-"
                    }

                    val tglLahirPemilik = if (valuePemilikKedua.subjek2BirthDate.toString().isNotEmpty()) {
                        valuePemilikKedua.subjek2BirthDate.toString()
                    } else {
                        "-"
                    }

                    val alamartRT = if (valuePemilikKedua.subjek2RT.toString().isNotEmpty()) {
                        valuePemilikKedua.subjek2RT.toString()
                    } else {
                        "-"
                    }

                    val alamartRW = if (valuePemilikKedua.subjek2RW.toString().isNotEmpty()) {
                        valuePemilikKedua.subjek2RW.toString()
                    } else {
                        "-"
                    }

                    namaPemilikKedua = valuePemilikKedua.subjek2FullName
                    ttlPemilikKedua = "$tmpLahirPemilik, $tglLahirPemilik"
                    pekerjaanPemilikKedua = if (valuePemilikKedua.subjek2OccupationName.toString().isNotEmpty()) {
                        valuePemilikKedua.subjek2OccupationName.toString()
                    } else {
                        "-"
                    }

                    /* alamat pemilik pertama*/
                    alamatJlnPemilikKedua = if (valuePemilikKedua.subjek2StreetName.toString().isNotEmpty()) {
                        valuePemilikKedua.subjek2StreetName.toString()
                    } else {
                        "-"
                    }

                    alamatBlokPemilikKedua = if (valuePemilikKedua.subjek2Block.toString().isNotEmpty()) {
                        valuePemilikKedua.subjek2Block.toString()
                    } else {
                        "-"
                    }

                    alamatRtRwPemilikKedua = "$alamartRT / $alamartRW"
                    alamatKelurahanPemilikKedua = valuePemilikKedua.subjek2VillageName
                    alamatKecamatanPemilikKedua = valuePemilikKedua.subjek2DistrictName
                    alamatKotaPemilikKedua = valuePemilikKedua.subjek2RegencyName
                    alamatProvinsiPemilikKedua = valuePemilikKedua.subjek2ProvinceName
                    nomorIdentitasPemilikKedua = if (valuePemilikKedua.subjek2IdentityNumber.toString().isNotEmpty()) {
                        valuePemilikKedua.subjek2IdentityNumber
                    } else {
                        "-"
                    }

                    //nama pemilik pertama
                    table.addCell("")
                    table.addCell(createCellItextPdfWithColspanBold("- Nama Lengkap", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                    table.addCell(createCellItextPdfBoldTextCenter(":", false))
                    table.addCell(createCellItextPdfWithColspanBold(namaPemilikKedua, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

                    //ttl pemilikpertama
                    table.addCell("")
                    table.addCell(createCellItextPdfWithColspanBold("- Tempat / Tanggal Lahir", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                    table.addCell(createCellItextPdfBoldTextCenter(":", false))
                    table.addCell(createCellItextPdfWithColspanBold(ttlPemilikKedua, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

                    //pekerjaan pemilik pertama
                    table.addCell("")
                    table.addCell(createCellItextPdfWithColspanBold("- Pekerjaan", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                    table.addCell(createCellItextPdfBoldTextCenter(":", false))
                    table.addCell(createCellItextPdfWithColspanBold(pekerjaanPemilikKedua, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

                    //alamat pemilik pertama
                    table.addCell("")
                    table.addCell(createCellItextPdfWithColspanBold("- Alamat", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                    table.addCell(createCellItextPdfBoldTextCenter(":", false))
                    //alamat jalan
                    table.addCell(createCellItextPdfWithColspanBold(alamatJlnPemilikKedua, 4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                    table.addCell("")
                    //alamat blok
                    table.addCell(createCellItextPdfCustomPositionText(alamatBlokPemilikKedua, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                    table.addCell("")
                    //alamat rt rw
                    table.addCell(createCellItextPdfWithColspanBold(alamatRtRwPemilikKedua, 4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                    //alamat kelurahan
                    table.addCell("")
                    table.addCell("")
                    table.addCell("")
                    table.addCell("")
                    table.addCell(createCellItextPdfWithColspanBold(alamatKelurahanPemilikKedua, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                    //alamat kecamatan
                    table.addCell("")
                    table.addCell("")
                    table.addCell("")
                    table.addCell("")
                    table.addCell(createCellItextPdfWithColspanBold(alamatKecamatanPemilikKedua, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                    //alamat kota & provinsi
                    table.addCell("")
                    table.addCell("")
                    table.addCell("")
                    table.addCell("")
                    table.addCell(createCellItextPdfWithColspanBold(alamatKotaPemilikKedua, 4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                    table.addCell("")
                    table.addCell(createCellItextPdfWithColspanBold(alamatProvinsiPemilikKedua, 6, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

                    //Nik / no. SIM
                    table.addCell("")
                    table.addCell(createCellItextPdfWithColspanBold("- NIK / No. SIM", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                    table.addCell(createCellItextPdfBoldTextCenter(":", false))
                    table.addCell(createCellItextPdfWithColspanBold(nomorIdentitasPemilikKedua, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

                    //luas tanah yg digarap
                    table.addCell("")
                    table.addCell(createCellItextPdfWithColspanBold("- Luas Tanah Digarap / Disewa", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                    table.addCell(createCellItextPdfBoldTextCenter(":", false))
                    table.addCell(createCellItextPdfWithColspanBold("-", 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

                    //penggunaan pemanfaatan tanah
                    table.addCell("")
                    table.addCell(createCellItextPdfWithColspanBold("- Penggunaan / Pemanfaatan Tanah", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                    table.addCell(createCellItextPdfBoldTextCenter(":", false))
                    table.addCell(createCellItextPdfWithColspanBold("-", 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

                    //jangka waktu sewa
                    table.addCell("")
                    table.addCell(createCellItextPdfWithColspanBold("- Jangka Waktu Sewa", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                    table.addCell(createCellItextPdfBoldTextCenter(":", false))
                    table.addCell(createCellItextPdfWithColspanBold("-", 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

                    //space letak
                    table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                }

                table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                table.addCell(createCellItextPdfWithColspanBold("Tempat, $currentDate", 13, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, false))
                table.addCell("")
                table.addCell("")

                //tanda tangan
                table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, true))

                table.addCell("")
                table.addCell(createCellItextPdfWithColspanBoldFonSize("Petugas Satgas B", 4, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 16f))
                table.addCell("")
                table.addCell("")
                table.addCell(createCellItextPdfWithColspanBoldFonSize("Pemilik Tanah", 6, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 16f))
                table.addCell("")
                table.addCell("")


                table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                table.addCell("")
                table.addCell(createCellItextPdfWithColspanBold("_______________________________", 4, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true))
                table.addCell("")
                table.addCell("")
                table.addCell(createCellItextPdfWithColspanBold("_______________________________", 6, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true))
                table.addCell("")
                table.addCell("")

                //mengetahui ttd
                table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                table.addCell("")
                table.addCell("")
                table.addCell("")
                table.addCell(createCellItextPdfWithColspanBoldFonSize("Mengetahui", 5, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 16f))
                table.addCell("")
                table.addCell("")
                table.addCell("")
                table.addCell("")
                table.addCell("")
                table.addCell("")
                table.addCell("")

                table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
                table.addCell("")
                table.addCell("")
                table.addCell(createCellItextPdfWithColspanBold("_______________________________", 9, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true))
                table.addCell("")
                table.addCell("")
                table.addCell("")
                table.addCell("")

            }

        }

        return table
    }

    private fun createCellItextPdfCustomPositionText(content: String, horizontalElement: Int, verticalElement: Int, isBold: Boolean): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, 12f)
        }
        val cell = PdfPCell(Phrase(content, font))
        cell.isNoWrap = false
        cell.border = Rectangle.NO_BORDER
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }

    private fun createCellItextPdfBoldTextCenter(content: String, isBold: Boolean): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, 12f)
        }

        val cell = PdfPCell(Phrase(content, font))
        cell.isNoWrap = false
        cell.border = Rectangle.NO_BORDER
        cell.horizontalAlignment = Element.ALIGN_CENTER
        cell.verticalAlignment = Element.ALIGN_CENTER
        return cell
    }

    private fun createCellItextPdfWithRowspanBold(content: String, total_rowspan: Int, horizontalElement: Int, verticalElement: Int, isBold: Boolean): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, 12f)
        }
        val cell = PdfPCell(Phrase(content, font))
        cell.isNoWrap = false
        cell.border = Rectangle.NO_BORDER
        cell.rowspan = total_rowspan
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }

    private fun createCellItextPdfWithColspanBoldFonSize(content: String, total_colspan: Int, horizontalElement: Int, verticalElement: Int, isBold: Boolean, fontSize: Float): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, fontSize, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, fontSize)
        }

        val cell = PdfPCell(Phrase(content, font))
        cell.border = Rectangle.NO_BORDER
        cell.colspan = total_colspan
        cell.isNoWrap = false
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }

    private fun createCellItextPdfWithColspanBold(content: String, total_colspan: Int, horizontalElement: Int, verticalElement: Int, isBold: Boolean): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, 12f)
        }

        val cell = PdfPCell(Phrase(content, font))
        cell.border = Rectangle.NO_BORDER
        cell.colspan = total_colspan
        cell.isNoWrap = false
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }

    private fun createRowTitle(numRow: Int, numCol: Int, nameRow: String?, cellStyle: XSSFCellStyle) {
        val row = ws.createRow(numRow)
        val col = row.createCell(numCol)
        col.cellStyle = cellStyle
        col.setCellValue(nameRow)

        oldLastRow = numRow
        this.currentLastRow = numRow + 1
    }

    private fun createRowTitle2(numRow: Int, numCol: Int, nameRow: String?, cellStyle: XSSFCellStyle) {
        val row = ws.createRow(numRow)
        val col = row.createCell(numCol)
        col.setCellStyle(cellStyle)
        col.setCellValue(nameRow)

        oldLastRow2 = numRow
        this.currentLastRow2 = numRow + 1
    }

    @SuppressLint("SdCardPath")
    fun openFile(filename: String) {
        val outputExportExcelFile = File(Environment.getExternalStorageDirectory().toString() + File.separator + "SIOJT" + File.separator + "Downloads", filename)
        //val path = Uri.fromFile(outputExportExcelFile)
        val path = FileProvider.getUriForFile(activity, activity.packageName, outputExportExcelFile)
        var isfilePdf = false

        val intent = Intent(Intent.ACTION_VIEW)
        // Check what kind of file you are trying to open, by comparing the url with extensions.
        // When the if condition is matched, plugin sets the correct intent (mime) type,
        // so Android knew what application to use to open the file
        if (filename.contains(".doc") || filename.contains(".docx")) {
            // Word document
            intent.setDataAndType(path, "application/msword")
        } else if (filename.contains(".pdf")) {
            // PDF file
            intent.setDataAndType(path, "application/pdf")
            //isfilePdf = true
        } else if (filename.contains(".ppt") || filename.contains(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(path, "application/vnd.ms-powerpoint")
        } else if (filename.contains(".xls") || filename.contains(".xlsx")) {
            // Excel file
            intent.setDataAndType(path, "application/vnd.ms-excel")
        } else if (filename.contains(".zip") || filename.contains(".rar")) {
            // WAV audio file
            intent.setDataAndType(path, "application/x-wav")
        } else if (filename.contains(".rtf")) {
            // RTF file
            intent.setDataAndType(path, "application/rtf")
        } else if (filename.contains(".wav") || filename.contains(".mp3")) {
            // WAV audio file
            intent.setDataAndType(path, "audio/x-wav")
        } else if (filename.contains(".gif")) {
            // GIF file
            intent.setDataAndType(path, "image/gif")
        } else if (filename.contains(".jpg") || filename.contains(".jpeg") || filename.contains(".png")) {
            // JPG file
            intent.setDataAndType(path, "image/jpeg")
        } else if (filename.contains(".txt")) {
            // Text file
            intent.setDataAndType(path, "text/plain")
        } else if (filename.contains(".3gp") || filename.contains(".mpg") || filename.contains(".mpeg") || filename.contains(".mpe") || filename.contains(".mp4") || filename.contains(".avi")) {
            // Video files
            intent.setDataAndType(path, "video/*")
        } else {
            //if you want you can also define the intent type for any other file

            //additionally use else clause below, to manage other unknown extensions
            //in this case, Android will show all applications installed on the device
            //so you can choose which application to use
            intent.setDataAndType(path, "*/*")
        }

        when {
            isfilePdf -> {
                Toast.makeText(activity, "Export to pdf", Toast.LENGTH_SHORT).show()
            }
            else -> {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                try {
                    activity.startActivity(Intent.createChooser(intent, "Pilih aplikasi"))
                } catch (e: ActivityNotFoundException) {
                    //Toast.makeText(this@Detailsurat_masuk_activity, "No Application available to view PDF", Toast.LENGTH_SHORT).show()
                    Toast.makeText(activity, "Tidak ada aplikasi yang tersedia untuk membuka file", Toast.LENGTH_SHORT).show()
                }
            }
        }

    }
}
