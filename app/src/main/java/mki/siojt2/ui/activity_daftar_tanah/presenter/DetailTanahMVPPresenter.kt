package mki.siojt2.ui.activity_daftar_tanah.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface DetailTanahMVPPresenter : MVPPresenter {

    fun getDaftarTanah(projectId: Int, partyId: Int, accessToken: String)

}