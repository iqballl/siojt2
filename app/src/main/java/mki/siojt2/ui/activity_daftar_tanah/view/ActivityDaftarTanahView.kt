package mki.siojt2.ui.activity_daftar_tanah.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.ResponseDataListTanah

interface ActivityDaftarTanahView : MvpView{

    fun onsuccessgetDaftarTanah(responseDataListTanah: MutableList<ResponseDataListTanah>)
}