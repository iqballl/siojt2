package mki.siojt2.ui.activity_daftar_objek.tanaman.adapter_recylceview

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.AsyncTask
import android.os.Environment
import android.os.Handler
import android.text.format.DateFormat
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.gson.Gson
import com.itextpdf.text.*
import com.itextpdf.text.pdf.PdfPCell
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfWriter
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.dialog_confirm_export.view.*
import kotlinx.android.synthetic.main.item_daftar_objek_tanaman.view.*
import mki.siojt2.R
import mki.siojt2.base.BaseViewHolder
import mki.siojt2.model.ResponseDataListProjectLocal
import mki.siojt2.model.localsave.*
import mki.siojt2.persistence.Preference
import mki.siojt2.ui.activity_daftar_objek.tanaman.view.ActivityDaftarObjekTanaman
import mki.siojt2.ui.activity_detail_objek_tanaman.ActivityDetailObjekTanaman
import mki.siojt2.ui.activity_form_add_objek_tanaman.ActivityFormAddObjekTanaman
import mki.siojt2.utils.extension.SafeClickListener
import mki.siojt2.utils.realm.RealmController
import org.apache.poi.ss.usermodel.HorizontalAlignment
import org.apache.poi.ss.usermodel.IgnoredErrorType
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.VerticalAlignment
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList


class AdapterDaftarObjekTanaman(context: Context, val activity: Activity) : RecyclerView.Adapter<BaseViewHolder>() {

    private val mContext: Context? = context
    val mDataset: MutableList<Tanaman>? = ArrayList()
    var rowLayout = R.layout.item_daftar_objek_tanaman
    var emptyrowLayout = R.layout.item_empty_view
    var pos = 0

    private var mCallback: Callback? = null

    private lateinit var session: Session

    lateinit var realm: Realm

    /* excel */
    lateinit var wb: XSSFWorkbook
    lateinit var ws: XSSFSheet
    lateinit var sweetAlretLoading: SweetAlertDialog

    lateinit var rownamaProyek: Row
    lateinit var rowProvinsi: Row
    lateinit var rowKota: Row
    lateinit var rowKabupaten: Row
    lateinit var rowKecamatan: Row
    lateinit var rowDekul: Row
    lateinit var rowrtRw: Row
    lateinit var rowBlokNo: Row
    lateinit var rowJln: Row
    lateinit var rownoBid: Row
    lateinit var rownoPetugas: Row

    /* pemilik tanaman */
    lateinit var rownamalengkapPT: Row
    lateinit var rowttlPT: Row
    lateinit var rowpekerjaanPT: Row
    lateinit var rowalamatPT: Row
    lateinit var rownoIdentitasPT: Row
    lateinit var rowpemilikTanahPT: Row

    private var oldLastRow = 0
    private var currentLastRow = 34

    private var isPdfile = false
    private val dateTemplate = "dd MMMM yyyy"

    init {
        realm = RealmController.with(activity).realm
    }

    fun setCallback(callback: Callback) {
        mCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            VIEW_TYPE_NORMAL -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(rowLayout, parent, false))
            VIEW_TYPE_EMPTY -> EmptyViewHolder(
                    LayoutInflater.from(parent.context).inflate(R.layout.item_empty_view, parent, false))
            else -> EmptyViewHolder(LayoutInflater.from(parent.context).inflate(emptyrowLayout, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (mDataset != null && !mDataset.isNullOrEmpty()) {
            VIEW_TYPE_NORMAL
        } else {
            VIEW_TYPE_EMPTY
        }
    }

    override fun getItemCount(): Int {
        return if (!mDataset.isNullOrEmpty()) {
            mDataset.size
        } else {
            1
        }
    }

    /*inner class RuangViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var r_no: TextView = itemView.findViewById(R.id.rowNo) as TextView
        var r_tvTitle: TextView = itemView.findViewById(R.id.tvTitle) as TextView
        var r_tvContent1: TextView = itemView.findViewById(R.id.tvContent1) as TextView
        var r_tvContent2: TextView = itemView.findViewById(R.id.tvContent2) as TextView

    }*/

    fun clear() {
        this.mDataset!!.clear()
    }

    fun addItems(data: RealmResults<Tanaman>) {
        this.mDataset!!.addAll(data)
        notifyDataSetChanged()
    }

    interface Callback {
        fun onRepoEmptyViewRetryClick()
        fun onDismissBottomsheet()
    }

    inner class ViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }

        @SuppressLint("SetTextI18n")
        override fun onBind(position: Int) {
            super.onBind(position)

            val dataList = mDataset!![position]
            inflateData(dataList)
            session = Session(mContext)

            val aaa: Tanaman = dataList

            val namaTanaman = dataList.namaTanaman
            val namaTanaman1 = namaTanaman.replace("[", "['")
            val namaTanaman2 = namaTanaman1.replace("]", "']")
            val namaTanaman3 = namaTanaman2.replace(",", "','")
            val namaTanaman4 = namaTanaman3.replace(" ", "")
            val namaTanamanToGson = Gson().fromJson(namaTanaman4, Array<String>::class.java).toList()

            val jenisTanaman = dataList.jenisTanamanNama
            val jenisTanaman1 = jenisTanaman.replace("[", "['")
            val jenisTanaman2 = jenisTanaman1.replace("]", "']")
            val jenisTanaman3 = jenisTanaman2.replace(",", "','")
            val jenisTanaman4 = jenisTanaman3.replace(" ", "")

            val jenisTanamanToGson = Gson().fromJson(jenisTanaman4, Array<String>::class.java).toList()

            val jumTanaman = Gson().fromJson(
                dataList.jumlahTanaman
                    .replace("[","['")
                    .replace("]", "']")
                    .replace(",", "','")
                    .replace(" ", ""),
                Array<String>::class.java).toList()

            /*val names:List<String>
            val it = jenisTanamanToGson.iterator()
            while (it.hasNext())
            {
                val name = it.next()
                // Do something
                it.r
            }*/

            val jmlTanamanToArrayList = ArrayList<String>(namaTanamanToGson)
            val jumlahTanaman = removeDuplicates(jmlTanamanToArrayList)

            /*// create test list
            val arrList = ArrayList<Int>()
            for (i in jenisTanamanToGson.indices) {
                arrList.add(i)
            }

            // clone
            val newArrList = ArrayList<Int>()
            for (i in 0..3) {
                newArrList.addAll(arrList)
            }*/

            val pos = pos + position + 1
            val jml = if(jumlahTanaman.size > 2) { 2 } else { jumlahTanaman.size }

            var name = ""
            var totalTanaman = 0
            for(i in 0 until jml){
                totalTanaman += jumTanaman[i].toInt()
                name += jumlahTanaman[i]
                if(i != jml - 1){
                    name += ","
                }else if(jumlahTanaman.size > 2 && i == jml-1){
                    name += ","
                    name += "dll"
                }
            }

            itemView.tvTitle.text = "Tanaman ($name)"

            itemView.tvJumlahTanaman.text = "Jumlah tanaman : $totalTanaman"

            itemView.tvnamapemilikTanaman.text = "Pemilik tanaman : ${dataList.pemilikTanaman}"
            /*itemView.tvalamat1Subjek.text = "Jumlah jenis tanaman : ${jenisTanamanToGson.size}"
            itemView.tvalamat2Subjek.text = "Ukuran : ${dataList.ukuranTanaman}"
            itemView.tvjumlahTanaman.text = "Jumlah : ${dataList.jumlahTanaman}"*/

            itemView.btnexportTanaman.setSafeOnClickListener {
                try {
                    saveExcelFile("file name export", dataList.landIdTemp, dataList.tanamanIdTemp,"Tanaman_${dataList.tanamanIdTemp}")
                }
                catch (ex: Exception){
                    ex.printStackTrace()
                }
            }

            itemView.btEdit.setSafeOnClickListener {
                mCallback?.onDismissBottomsheet()
                val param = "['${dataList.landIdTemp}','${dataList.projectId}','']"
                val intent = ActivityFormAddObjekTanaman.getStartIntent(mContext!!)
                intent.putExtra("data_current_tanah", param)
                val mSession = mki.siojt2.model.Session(dataList.tanamanIdTemp, null,"2")
                intent.putExtra("data_current_session", Gson().toJson(mSession))
                session.setIdAndStatus(dataList.tanamanIdTemp.toString(), "2", "")
                mContext.startActivity(intent)
            }

            itemView.btDelete.setSafeOnClickListener {
                mCallback?.onDismissBottomsheet()
//                val param = "['${dataList.landIdTemp}','${dataList.projectId}','']"
//                val intent = ActivityFormAddObjekTanaman.getStartIntent(mContext!!)
//                intent.putExtra("data_current_tanah", param)
//                session.setIdAndStatus(dataList.tanamanIdTemp.toString(), "3", "")
//                mContext.startActivity(intent)

                val param = "['${dataList.landIdTemp}','${dataList.projectId}','']"
                val intent = ActivityFormAddObjekTanaman.getStartIntent(mContext!!)
                val mSession = mki.siojt2.model.Session(dataList.tanamanIdTemp, dataList.landIdTemp,"3")
                intent.putExtra("data_current_tanah", param)
                intent.putExtra("data_current_session", Gson().toJson(mSession))
                session.setIdAndStatus(dataList.tanamanIdTemp.toString(), "3", "")
                mContext.startActivity(intent)
            }

            //val wrapped = Parcels.wrap(dataList)
            itemView.btnSelngkapnya.setSafeOnClickListener {
                mCallback?.onDismissBottomsheet()
                val intent = ActivityDetailObjekTanaman.getStartIntent(mContext!!)
                intent.putExtra("data_current_tanaman", dataList.tanamanIdTemp)
                session.setIdAndStatus(dataList.tanamanIdTemp.toString(), "3", "")
                mContext.startActivity(intent)
                //val aaa = Gson().toJson(mDataset[position])

                /*Log.d("iqbal", dataList.pemilikTanaman)
                Log.d("iqbal", aaa.toString())*/
            }

        }

        private fun inflateData(dataList: Tanaman) {
            //nama?.let { itemView.tvMatapelajaran.text = it }
//            itemView.tvContent1.text = dataList.namaJenis
//            itemView.tvContent2.text= dataList.keterangan
        }
    }

    inner class EmptyViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }

    }

    // Function to remove duplicates from an ArrayList
    fun <T> removeDuplicates(list:ArrayList<T>):ArrayList<T> {
        // Create a new ArrayList
        val newList = ArrayList<T>()
        // Traverse through the first list
        for (element in list)
        {
            // If this element is not present in newList
            // then add it
            if (!newList.contains(element))
            {
                newList.add(element)
            }
        }
        // return the new list
        return newList
    }

    private fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
        val safeClickListener = SafeClickListener {
            onSafeClick(it)
        }
        setOnClickListener(safeClickListener)
    }

    companion object {
        const val VIEW_TYPE_EMPTY = 0
        const val VIEW_TYPE_NORMAL = 1
    }


    @SuppressLint("StaticFieldLeak")
    inner class CreateFormatExcelTask : AsyncTask<Void, Void, Void>() {
        override fun doInBackground(vararg params: Void?): Void? {
            wb = XSSFWorkbook()
            ws = wb.createSheet("Sheet1")
            ws.setColumnWidth(1, 3 * 256)
            ws.setColumnWidth(0, 3 * 256)
            ws.setColumnWidth(2, 12 * 256)
            ws.setColumnWidth(3, 25 * 256)
            ws.setColumnWidth(4, 3 * 256)
            ws.setColumnWidth(5, 3 * 256)
            ws.setColumnWidth(15, 3 * 256)
            ws.addIgnoredErrors(CellRangeAddress(0, 9999, 0, 9999), IgnoredErrorType.NUMBER_STORED_AS_TEXT)

            /* set to default */
            oldLastRow = 0
            currentLastRow = 34

            val defaultFontHeader = wb.createFont()
            defaultFontHeader.fontHeight = (16.0 * 20).toShort()
            defaultFontHeader.bold = true

            val defaultFontNo = wb.createFont()
            defaultFontNo.fontHeight = (12.0 * 20).toShort()
            defaultFontNo.bold = true

            val defaultFontTitle = wb.createFont()
            defaultFontTitle.fontHeight = (12.0 * 20).toShort()

            /* style */
            val cellStyleHeader = wb.createCellStyle()
            cellStyleHeader.wrapText = true
            cellStyleHeader.setFont(defaultFontHeader)
            // justify text alignment\
            cellStyleHeader.setAlignment(HorizontalAlignment.CENTER)

            val cellStyleHeader2 = wb.createCellStyle()
            cellStyleHeader2.wrapText = true
            cellStyleHeader2.setFont(defaultFontNo)
            // justify text alignment\
            cellStyleHeader2.setVerticalAlignment(VerticalAlignment.CENTER)

            /* style no*/
            val cellStyleNo = wb.createCellStyle()
            cellStyleNo.wrapText = true
            cellStyleNo.setFont(defaultFontNo)
            // justify text alignment\
            cellStyleNo.setVerticalAlignment(VerticalAlignment.CENTER)
            cellStyleNo.setAlignment(HorizontalAlignment.CENTER)

            /* style titik 2*/
            val cellStyleTitik2 = wb.createCellStyle()
            cellStyleTitik2.wrapText = true
            cellStyleTitik2.setFont(defaultFontTitle)
            // justify text alignment\
            cellStyleTitik2.setVerticalAlignment(VerticalAlignment.CENTER)
            cellStyleTitik2.setAlignment(HorizontalAlignment.CENTER)

            /* style normal */
            val cellStyleNoramlTitle = wb.createCellStyle()
            cellStyleNoramlTitle.wrapText = true
            cellStyleNoramlTitle.setFont(defaultFontTitle)
            // justify text alignment\\
            cellStyleNoramlTitle.setAlignment(HorizontalAlignment.LEFT)

            //header 1
            val rowheader1 = ws.createRow(2)
            val colheader1 = rowheader1.createCell(1)
            colheader1.cellStyle = cellStyleHeader
            colheader1.setCellValue("PELAKSANAAN PENGADAAN TANAH UNTUK PEMBANGUNAN")
            ws.addMergedRegion(CellRangeAddress(2, 2, 1, 14))

            //header 2
            /*val rowheader2 = ws.createRow(3)
            val colheader2 = rowheader2.createCell(1)
            colheader2.cellStyle = cellStyleHeader
            colheader2.setCellValue("PROYEK : Daerah Irigasi Batang Bayang")
            ws.addMergedRegion(CellRangeAddress(3, 3, 1, 15))*/

            //header 3
            val rowheader3 = ws.createRow(5)
            val colheader3 = rowheader3.createCell(1)
            colheader3.cellStyle = cellStyleHeader
            colheader3.setCellValue("DATA PEMILIK TANAMAN YANG TERKENA PENGADAAN TANAH")
            ws.addMergedRegion(CellRangeAddress(5, 5, 1, 15))

            /* LETAK TANAH */
            val rowLetakTanah = ws.createRow(7)
            val colnoLetakTanah = rowLetakTanah.createCell(1)
            colnoLetakTanah.cellStyle = cellStyleNo
            colnoLetakTanah.setCellValue("I")

            val colrowtitleLetakTanah = rowLetakTanah.createCell(2)
            colrowtitleLetakTanah.cellStyle = cellStyleHeader2
            colrowtitleLetakTanah.setCellValue("LETAK TANAH")
            ws.addMergedRegion(CellRangeAddress(7, 7, 2, 3))

            //propinsi
            rowProvinsi = ws.createRow(8)
            val colProvinsi = rowProvinsi.createCell(2)
            colProvinsi.cellStyle = cellStyleNoramlTitle
            colProvinsi.setCellValue("- Provinsi")

            val colProvinsi2 = rowProvinsi.createCell(4)
            colProvinsi2.cellStyle = cellStyleTitik2
            colProvinsi2.setCellValue(":")

            //Kota
            rowKota = ws.createRow(9)
            val colKota = rowKota.createCell(2)
            colKota.cellStyle = cellStyleNoramlTitle
            colKota.setCellValue("- Kota")

            val colKota2 = rowKota.createCell(4)
            colKota2.cellStyle = cellStyleTitik2
            colKota2.setCellValue(":")

            //Kabupatan
            rowKabupaten = ws.createRow(10)
            val colKabupaten = rowKabupaten.createCell(2)
            colKabupaten.cellStyle = cellStyleNoramlTitle
            colKabupaten.setCellValue("- Kabupaten")

            val colKabupaten2 = rowKabupaten.createCell(4)
            colKabupaten2.cellStyle = cellStyleTitik2
            colKabupaten2.setCellValue(":")

            //Kecamatan
            rowKecamatan = ws.createRow(11)
            val colKecamatan = rowKecamatan.createCell(2)
            colKecamatan.cellStyle = cellStyleNoramlTitle
            colKecamatan.setCellValue("- Kecamatan")

            val colKecamatan2 = rowKecamatan.createCell(4)
            colKecamatan2.cellStyle = cellStyleTitik2
            colKecamatan2.setCellValue(":")

            //Desa/kelurahan
            rowDekul = ws.createRow(12)
            val colDekul = rowDekul.createCell(2)
            colDekul.cellStyle = cellStyleNoramlTitle
            colDekul.setCellValue("- Desa / Kelurahan")
            ws.addMergedRegion(CellRangeAddress(12, 12, 2, 3))

            val colDekul2 = rowDekul.createCell(4)
            colDekul2.cellStyle = cellStyleTitik2
            colDekul2.setCellValue(":")

            //Rt/rw
            rowrtRw = ws.createRow(13)
            val colrtRw = rowrtRw.createCell(2)
            colrtRw.cellStyle = cellStyleNoramlTitle
            colrtRw.setCellValue("- RT / RW")

            val colrtRw2 = rowrtRw.createCell(4)
            colrtRw2.cellStyle = cellStyleTitik2
            colrtRw2.setCellValue(":")

            //Blok/no
            rowBlokNo = ws.createRow(14)
            val colblokNo = rowBlokNo.createCell(2)
            colblokNo.cellStyle = cellStyleNoramlTitle
            colblokNo.setCellValue("- Blok / No")

            val colblokNo2 = rowBlokNo.createCell(4)
            colblokNo2.cellStyle = cellStyleTitik2
            colblokNo2.setCellValue(":")

            //Jalan
            rowJln = ws.createRow(15)
            val colJln = rowJln.createCell(2)
            colJln.cellStyle = cellStyleNoramlTitle
            colJln.setCellValue("- Jalan")

            val colJln2 = rowJln.createCell(4)
            colJln2.cellStyle = cellStyleTitik2
            colJln2.setCellValue(":")

            //No bidang
            rownoBid = ws.createRow(16)
            val colnoBid = rownoBid.createCell(2)
            colnoBid.cellStyle = cellStyleNoramlTitle
            colnoBid.setCellValue("- Nomor Bidang")
            ws.addMergedRegion(CellRangeAddress(16, 16, 2, 3))

            val colnoBid2 = rownoBid.createCell(4)
            colnoBid2.cellStyle = cellStyleTitik2
            colnoBid2.setCellValue(":")

            //Petugas
            rownoPetugas = ws.createRow(17)
            val colPetugas = rownoPetugas.createCell(2)
            colPetugas.cellStyle = cellStyleNoramlTitle
            colPetugas.setCellValue("- Petugas Satgas B")
            ws.addMergedRegion(CellRangeAddress(17, 17, 2, 3))


            val colPetugas2 = rownoPetugas.createCell(4)
            colPetugas2.cellStyle = cellStyleTitik2
            colPetugas2.setCellValue(":")


            /* PEMILIK TANAMAN */
            val rowPemilikBangunan = ws.createRow(19)
            val colNoPB = rowPemilikBangunan.createCell(1)
            colNoPB.cellStyle = cellStyleNo
            colNoPB.setCellValue("II")

            val coltitlePB = rowPemilikBangunan.createCell(2)
            coltitlePB.cellStyle = cellStyleHeader2
            coltitlePB.setCellValue("PEMILIK TANAMAN")
            ws.addMergedRegion(CellRangeAddress(19, 19, 2, 3))

            //Nama lenkgap
            rownamalengkapPT = ws.createRow(20)
            val coltitleNamaLengkapTanaman = rownamalengkapPT.createCell(2)
            coltitleNamaLengkapTanaman.cellStyle = cellStyleNoramlTitle
            coltitleNamaLengkapTanaman.setCellValue("- Nama Lengkap")
            ws.addMergedRegion(CellRangeAddress(20, 20, 2, 3))

            val coltitleNamaLengkapTanaman2 = rownamalengkapPT.createCell(4)
            coltitleNamaLengkapTanaman2.cellStyle = cellStyleTitik2
            coltitleNamaLengkapTanaman2.setCellValue(":")

            //Ttl
            rowttlPT = ws.createRow(21)
            val coltitlettlPT = rowttlPT.createCell(2)
            coltitlettlPT.cellStyle = cellStyleNoramlTitle
            coltitlettlPT.setCellValue("- Tempat / Tanggal Lahir")
            ws.addMergedRegion(CellRangeAddress(21, 21, 2, 3))

            val coltitlettlPT2 = rowttlPT.createCell(4)
            coltitlettlPT2.cellStyle = cellStyleTitik2
            coltitlettlPT2.setCellValue(":")

            //Pekerjaan
            rowpekerjaanPT = ws.createRow(22)
            val coltitlepekerjaanPT = rowpekerjaanPT.createCell(2)
            coltitlepekerjaanPT.cellStyle = cellStyleNoramlTitle
            coltitlepekerjaanPT.setCellValue("- Pekerjaan")

            val coltitlepekerjaanPT2 = rowpekerjaanPT.createCell(4)
            coltitlepekerjaanPT2.cellStyle = cellStyleTitik2
            coltitlepekerjaanPT2.setCellValue(":")

            //Alamat
            rowalamatPT = ws.createRow(23)
            val coltitlealamatPT = rowalamatPT.createCell(2)
            coltitlealamatPT.cellStyle = cellStyleNoramlTitle
            coltitlealamatPT.setCellValue("- Alamat")

            val coltitlealamatPT2 = rowalamatPT.createCell(4)
            coltitlealamatPT2.cellStyle = cellStyleTitik2
            coltitlealamatPT2.setCellValue(":")

            //No Identitas
            rownoIdentitasPT = ws.createRow(27)
            val coltitlenoIdentitasPT = rownoIdentitasPT.createCell(2)
            coltitlenoIdentitasPT.cellStyle = cellStyleNoramlTitle
            coltitlenoIdentitasPT.setCellValue("- NIK / No. SIM")
            ws.addMergedRegion(CellRangeAddress(27, 27, 2, 3))

            val coltitlenoIdentitasPT2 = rownoIdentitasPT.createCell(4)
            coltitlenoIdentitasPT2.cellStyle = cellStyleTitik2
            coltitlenoIdentitasPT2.setCellValue(":")

            //Pemilik tanah asli
            rowpemilikTanahPT = ws.createRow(28)
            val coltitlepemilikAsliPT = rowpemilikTanahPT.createCell(2)
            coltitlepemilikAsliPT.cellStyle = cellStyleNoramlTitle
            coltitlepemilikAsliPT.setCellValue("- Terletak di atas tanah milik")
            ws.addMergedRegion(CellRangeAddress(28, 28, 2, 3))

            val coltitlepemilikAsliPT2 = rowpemilikTanahPT.createCell(4)
            coltitlepemilikAsliPT2.cellStyle = cellStyleTitik2
            coltitlepemilikAsliPT2.setCellValue(":")

            /* title spesifikasi tanaman */
            val rowheaderSpesifikasi = ws.createRow(30)
            val colheaderSpesifikasi = rowheaderSpesifikasi.createCell(2)
            colheaderSpesifikasi.cellStyle = cellStyleHeader
            colheaderSpesifikasi.setCellValue("SPESIFIKASI TANAMAN")
            ws.addMergedRegion(CellRangeAddress(30, 30, 2, 14))

            /* table title spesifikasi bangunan */
            val rowheadertableSpsefikasi = ws.createRow(32)
            rowheadertableSpsefikasi.height = 2 * 256
            val colnoheadertableSpsefikasi = rowheadertableSpsefikasi.createCell(2)
            colnoheadertableSpsefikasi.cellStyle = cellStyleNo
            colnoheadertableSpsefikasi.setCellValue("No.")
            ws.addMergedRegion(CellRangeAddress(32, 33, 2, 2))

            val colheadertableBagianSpsefikasi = rowheadertableSpsefikasi.createCell(3)
            colheadertableBagianSpsefikasi.cellStyle = cellStyleNo
            colheadertableBagianSpsefikasi.setCellValue("Nama Tanaman")
            ws.addMergedRegion(CellRangeAddress(32, 33, 3, 6))

            val colheadertabletitleJmlhTanamanSpsefikasi = rowheadertableSpsefikasi.createCell(7)
            colheadertabletitleJmlhTanamanSpsefikasi.cellStyle = cellStyleNo
            colheadertabletitleJmlhTanamanSpsefikasi.setCellValue("Jumlah Tanaman")
            ws.addMergedRegion(CellRangeAddress(32, 32, 7, 14))

            val rowheadertableSpsefikasi2 = ws.createRow(33)
            rowheadertableSpsefikasi2.height = 2 * 256

            val colukuranTanamanSpsefikasi1 = rowheadertableSpsefikasi2.createCell(7)
            colukuranTanamanSpsefikasi1.cellStyle = cellStyleNo
            colukuranTanamanSpsefikasi1.setCellValue("Kecil")
            ws.addMergedRegion(CellRangeAddress(33, 33, 7, 8))

            val colukuranTanamanSpsefikasi2 = rowheadertableSpsefikasi2.createCell(9)
            colukuranTanamanSpsefikasi2.cellStyle = cellStyleNo
            colukuranTanamanSpsefikasi2.setCellValue("Sedang")
            ws.addMergedRegion(CellRangeAddress(33, 33, 9, 10))

            val colukuranTanamanSpsefikasi3 = rowheadertableSpsefikasi2.createCell(11)
            colukuranTanamanSpsefikasi3.cellStyle = cellStyleNo
            colukuranTanamanSpsefikasi3.setCellValue("Besar")
            ws.addMergedRegion(CellRangeAddress(33, 33, 11, 12))

            val colukuranTanamanSpsefikasi4 = rowheadertableSpsefikasi2.createCell(13)
            colukuranTanamanSpsefikasi4.cellStyle = cellStyleNo
            colukuranTanamanSpsefikasi4.setCellValue("Rumpun")
            ws.addMergedRegion(CellRangeAddress(33, 33, 13, 14))


            return null
        }

    }

    @SuppressLint("SdCardPath")
    @Suppress("DEPRECATION")
    fun saveExcelFile(filenameExport: String, landId: Int?, tanamanTempId: Int?, tanamanName: String) {
        val mDialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_confirm_export, null)
        val mBuilder = AlertDialog.Builder(activity)
        val mDialog = mBuilder.create()

        mDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val window = mDialog.window
        if (window != null) {
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            mDialog.window!!.setLayout(width, height)
        }
        mDialog.setView(mDialogView)
        mDialogView.btnExportCSV.visibility = View.INVISIBLE
        mDialogView.btnExportExcel.setOnClickListener {
            isPdfile = false
            mDialog.dismiss()
            createExcelFile(tanamanName, landId, tanamanTempId, tanamanName)
        }

        mDialogView.btnExportPdf.setOnClickListener {
            isPdfile = true
            mDialog.dismiss()
            convertDocToPdfitext(landId, tanamanTempId, tanamanName)
        }

        mDialog.show()

    }

    private fun createExcelFile(filenameExport: String, landId: Int?, tanamanTempId: Int?, tanamanName: String) {
        CreateFormatExcelTask().execute()
        val mfilename = "$tanamanName.xlsx"

        val calendar = Calendar.getInstance(Locale.getDefault())
        //c.get(Calendar.YEAR)
        //c.get(Calendar.MONTH) + 1
        //c.get(Calendar.DAY_OF_MONTH)
        //val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        //val currentDate = sdf.format(c.time)
        val currentDate = DateFormat.format(dateTemplate, calendar.time)

        Handler().postDelayed({
            realm.executeTransactionAsync({ inrealm ->
                val resultsDataTanah = inrealm.where(ProjectLand::class.java).equalTo("PartyIdTemp", landId).findFirst()

                val defaultFontHeader = wb.createFont()
                defaultFontHeader.fontHeight = (16.0 * 20).toShort()
                defaultFontHeader.bold = true

                val defaultFontNo = wb.createFont()
                defaultFontNo.fontHeight = (12.0 * 20).toShort()
                defaultFontNo.bold = true

                val defaultFontTitle = wb.createFont()
                defaultFontTitle.fontHeight = (12.0 * 20).toShort()

                val defaultFontTTD = wb.createFont()
                defaultFontTTD.fontHeight = (14.0 * 20).toShort()
                defaultFontTTD.bold = true

                /* style */
                val cellStyleHeader = wb.createCellStyle()
                cellStyleHeader.wrapText = true
                cellStyleHeader.setFont(defaultFontHeader)
                // justify text alignment\
                cellStyleHeader.setAlignment(HorizontalAlignment.CENTER)

                val cellStyleHeader2 = wb.createCellStyle()
                cellStyleHeader2.wrapText = true
                cellStyleHeader2.setFont(defaultFontNo)
                // justify text alignment\
                cellStyleHeader2.setVerticalAlignment(VerticalAlignment.CENTER)

                /* style no*/
                val cellStyleNo = wb.createCellStyle()
                cellStyleNo.wrapText = true
                cellStyleNo.setFont(defaultFontTitle)
                // justify text alignment\
                cellStyleNo.setVerticalAlignment(VerticalAlignment.CENTER)
                cellStyleNo.setAlignment(HorizontalAlignment.CENTER)

                /* style titik 2*/
                val cellStyleTitik2 = wb.createCellStyle()
                cellStyleTitik2.wrapText = true
                cellStyleTitik2.setFont(defaultFontNo)
                // justify text alignment\
                cellStyleTitik2.setVerticalAlignment(VerticalAlignment.CENTER)
                cellStyleTitik2.setAlignment(HorizontalAlignment.CENTER)

                /* style normal */
                val cellStyleNoramlTitle = wb.createCellStyle()
                cellStyleNoramlTitle.wrapText = true
                cellStyleNoramlTitle.setFont(defaultFontTitle)
                // justify text alignment\\
                cellStyleNoramlTitle.setAlignment(HorizontalAlignment.LEFT)

                val cellStyleNoramlTitleCenter = wb.createCellStyle()
                cellStyleNoramlTitleCenter.wrapText = true
                cellStyleNoramlTitleCenter.setFont(defaultFontTitle)
                // justify text alignment\\
                cellStyleNoramlTitleCenter.setVerticalAlignment(VerticalAlignment.CENTER)
                cellStyleNoramlTitleCenter.setAlignment(HorizontalAlignment.CENTER)

                /* style ttd */
                val cellStyleTglTtd = wb.createCellStyle()
                cellStyleTglTtd.wrapText = true
                cellStyleTglTtd.setFont(defaultFontNo)
                // justify text alignment\\
                cellStyleTglTtd.setAlignment(HorizontalAlignment.RIGHT)

                val cellStyleTtd = wb.createCellStyle()
                cellStyleTtd.wrapText = true
                cellStyleTtd.setFont(defaultFontTTD)
                // justify text alignment\\
                cellStyleTtd.setAlignment(HorizontalAlignment.CENTER)

                /* nama proyek */
                val resultsProyek = inrealm.where(ResponseDataListProjectLocal::class.java).equalTo("projectAssignProjectId", resultsDataTanah?.projectId).findFirst()
                //header 2
                rownamaProyek = ws.createRow(3)
                val colheader2 = rownamaProyek.createCell(1)

                colheader2.cellStyle = cellStyleHeader
                if (resultsProyek != null) {
                    colheader2.setCellValue(resultsProyek.getProjectName().toString().toUpperCase())
                } else {
                    colheader2.setCellValue("-")
                }
                ws.addMergedRegion(CellRangeAddress(3, 3, 1, 15))


                /* letak tanah */
                val colProvinsiLT = rowProvinsi.createCell(5)
                colProvinsiLT.cellStyle = cellStyleNoramlTitle
                colProvinsiLT.setCellValue(resultsDataTanah?.landProvinceName)
                ws.addMergedRegion(CellRangeAddress(8, 8, 5, 14))

                val colKotaLT = rowKota.createCell(5)
                colKotaLT.cellStyle = cellStyleNoramlTitle
                colKotaLT.setCellValue(resultsDataTanah?.landRegencyName)
                ws.addMergedRegion(CellRangeAddress(9, 9, 5, 14))

                val colKabupatenLT = rowKabupaten.createCell(5)
                colKabupatenLT.cellStyle = cellStyleNoramlTitle
                colKabupatenLT.setCellValue(resultsDataTanah?.landRegencyName)
                ws.addMergedRegion(CellRangeAddress(10, 10, 5, 14))

                val colKecamatanLT = rowKecamatan.createCell(5)
                colKecamatanLT.cellStyle = cellStyleNoramlTitle
                colKecamatanLT.setCellValue(resultsDataTanah?.landDistrictName)
                ws.addMergedRegion(CellRangeAddress(11, 11, 5, 14))

                val colKelurahanLT = rowDekul.createCell(5)
                colKelurahanLT.cellStyle = cellStyleNoramlTitle
                colKelurahanLT.setCellValue(resultsDataTanah?.landVillageName)
                ws.addMergedRegion(CellRangeAddress(12, 12, 5, 14))

                val colrtrwLT = rowrtRw.createCell(5)
                colrtrwLT.cellStyle = cellStyleNoramlTitle
                if (!resultsDataTanah?.landRT.toString().isNullOrEmpty() || !resultsDataTanah?.landRW.toString().isNullOrEmpty()) colrtrwLT.setCellValue(resultsDataTanah?.landRT + "/" + resultsDataTanah?.landRW) else colrtrwLT.setCellValue("-")
                ws.addMergedRegion(CellRangeAddress(13, 13, 5, 14))

                val colBlokLT = rowBlokNo.createCell(5)
                colBlokLT.cellStyle = cellStyleNoramlTitle
                if (!resultsDataTanah?.landBlok.isNullOrEmpty()) colBlokLT.setCellValue(resultsDataTanah?.landBlok) else colBlokLT.setCellValue("-")
                ws.addMergedRegion(CellRangeAddress(14, 14, 5, 14))

                val colJlnLT = rowJln.createCell(5)
                colJlnLT.cellStyle = cellStyleNoramlTitle
                if (!resultsDataTanah?.landStreet.isNullOrEmpty()) colJlnLT.setCellValue(resultsDataTanah?.landStreet) else colJlnLT.setCellValue("-")
                ws.addMergedRegion(CellRangeAddress(15, 15, 5, 14))

                val colNoBidLT = rownoBid.createCell(5)
                colNoBidLT.cellStyle = cellStyleNoramlTitle
                if (!resultsDataTanah?.landNumberList.isNullOrEmpty()) colNoBidLT.setCellValue(resultsDataTanah?.landNumberList) else colNoBidLT.setCellValue("-")
                ws.addMergedRegion(CellRangeAddress(16, 16, 5, 14))

                val colPetugasLT = rownoPetugas.createCell(5)
                colPetugasLT.cellStyle = cellStyleNoramlTitle
                colPetugasLT.setCellValue(Preference.auth.dataUser!!.mobileUserEmployeeName)
                ws.addMergedRegion(CellRangeAddress(17, 17, 5, 14))

                /* spesifikasi tanaman */
                val resultsDataSepsifikasiTanaman = inrealm.where(Tanaman::class.java)
                    .equalTo("LandIdTemp", landId)
                    .and()
                    .equalTo("TanamanIdTemp", tanamanTempId)
                    .findFirst()


                /* pemilik tanaman */
                val idPemilikTanaman = resultsDataSepsifikasiTanaman?.pemilikTanamanId
                val firstCharacterOfIdPemilkTanaman = idPemilikTanaman?.get(0)
                val lastCharacterOfIdPemilkTanaman = idPemilikTanaman?.substring(idPemilikTanaman.indexOf("$firstCharacterOfIdPemilkTanaman") + 1)

                when {
                    firstCharacterOfIdPemilkTanaman.toString() == "P" -> {
                        val resultDataPemilikPertama = inrealm.where(PartyAdd::class.java)
                            .equalTo("TempId", lastCharacterOfIdPemilkTanaman?.toInt())
                            .findFirst()

                        /* nama lengkap */
                        val colNamaLengkap = rownamalengkapPT.createCell(5)
                        colNamaLengkap.cellStyle = cellStyleNoramlTitle
                        colNamaLengkap.setCellValue(resultDataPemilikPertama?.partyFullName)
                        ws.addMergedRegion(CellRangeAddress(20, 20, 5, 14))

                        /* ttl*/
                        val colTTLPB = rowttlPT.createCell(5)
                        colTTLPB.cellStyle = cellStyleNoramlTitle

                        val tmptLahirPB: String?
                        val tglLahirPB: String?
                        tmptLahirPB = if (!resultDataPemilikPertama?.partyBirthPlaceName.isNullOrEmpty()) resultDataPemilikPertama?.partyBirthPlaceName else "-"
                        tglLahirPB = if (!resultDataPemilikPertama?.partyBirthDate.isNullOrEmpty()) resultDataPemilikPertama?.partyBirthDate else "-"

                        colTTLPB.setCellValue("$tmptLahirPB / $tglLahirPB")
                        ws.addMergedRegion(CellRangeAddress(21, 21, 5, 14))

                        /* pekerjaan */
                        val colPekerjaanPB = rowpekerjaanPT.createCell(5)
                        colPekerjaanPB.cellStyle = cellStyleNoramlTitle
                        if (!resultDataPemilikPertama?.partyOccupationName.isNullOrEmpty()) colPekerjaanPB.setCellValue(resultDataPemilikPertama?.partyOccupationName) else colPekerjaanPB.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(22, 22, 5, 14))

                        /* alamat */
                        val colAlamatJalan = rowalamatPT.createCell(5)
                        colAlamatJalan.cellStyle = cellStyleNoramlTitle
                        if (!resultDataPemilikPertama?.partyDistrictName.isNullOrEmpty()) colAlamatJalan.setCellValue(resultDataPemilikPertama?.partyDistrictName) else colAlamatJalan.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(23, 23, 5, 8))

                        val colAlamatBlok = rowalamatPT.createCell(11)
                        colAlamatBlok.cellStyle = cellStyleNoramlTitle
                        if (!resultDataPemilikPertama?.partyBlock.isNullOrEmpty()) colAlamatBlok.setCellValue(resultDataPemilikPertama?.partyBlock) else colAlamatBlok.setCellValue("-")

                        val colAlamatRTRW = rowalamatPT.createCell(13)
                        colAlamatRTRW.cellStyle = cellStyleNoramlTitle
                        val noRTPB: String?
                        val noRWPB: String?

                        noRTPB = if (!resultDataPemilikPertama?.partyRT.isNullOrEmpty()) resultDataPemilikPertama?.partyRT else "-"
                        noRWPB = if (!resultDataPemilikPertama?.partyRW.isNullOrEmpty()) resultDataPemilikPertama?.partyRW else "-"
                        colAlamatRTRW.setCellValue("$noRTPB / $noRWPB")

                        val rowAlamatDesa = ws.createRow(24)
                        val colAlamatDesa = rowAlamatDesa.createCell(5)
                        colAlamatDesa.cellStyle = cellStyleNoramlTitle
                        colAlamatDesa.setCellValue(resultDataPemilikPertama?.partyVillageName)
                        ws.addMergedRegion(CellRangeAddress(24, 24, 5, 8))

                        val rowAlamatKecamatan = ws.createRow(25)
                        val colAlamatKecamatan = rowAlamatKecamatan.createCell(5)
                        colAlamatKecamatan.cellStyle = cellStyleNoramlTitle
                        colAlamatKecamatan.setCellValue(resultDataPemilikPertama?.partyDistrictName)
                        ws.addMergedRegion(CellRangeAddress(25, 25, 5, 8))

                        val rowAlamatKota = ws.createRow(26)
                        val colAlamatKota = rowAlamatKota.createCell(5)
                        colAlamatKota.cellStyle = cellStyleNoramlTitle
                        colAlamatKota.setCellValue(resultDataPemilikPertama?.partyRegencyName)
                        ws.addMergedRegion(CellRangeAddress(26, 26, 5, 8))

                        val colAlamatProvinsi = rowAlamatKota.createCell(11)
                        colAlamatProvinsi.cellStyle = cellStyleNoramlTitle
                        colAlamatProvinsi.setCellValue(resultDataPemilikPertama?.partyProvinceName)
                        ws.addMergedRegion(CellRangeAddress(26, 26, 11, 13))


                        /* identitas pemilik */
                        val colIdentitasPemilik = rownoIdentitasPT.createCell(5)
                        colIdentitasPemilik.cellStyle = cellStyleNoramlTitle
                        if (!resultDataPemilikPertama?.partyIdentityNumber.isNullOrEmpty()) colIdentitasPemilik.setCellValue(resultDataPemilikPertama?.partyIdentityNumber) else colIdentitasPemilik.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(27, 27, 5, 8))

                    }
                    firstCharacterOfIdPemilkTanaman.toString() == "S" -> {
                        val resultDataPemilikKedua = inrealm.where(Subjek2::class.java)
                            .equalTo("SubjekIdTemp", lastCharacterOfIdPemilkTanaman?.toInt())
                            .findFirst()

                        /* nama lengkap */
                        val colNamaLengkap = rownamalengkapPT.createCell(5)
                        colNamaLengkap.cellStyle = cellStyleNoramlTitle
                        colNamaLengkap.setCellValue(resultDataPemilikKedua?.subjek2FullName)
                        ws.addMergedRegion(CellRangeAddress(20, 20, 5, 14))

                        /* ttl*/
                        val colTTLPB = rowttlPT.createCell(5)
                        colTTLPB.cellStyle = cellStyleNoramlTitle

                        val tmptLahirPB: String?
                        val tglLahirPB: String?
                        tmptLahirPB = if (!resultDataPemilikKedua?.subjek2BirthPlaceName.isNullOrEmpty()) resultDataPemilikKedua?.subjek2BirthPlaceName else "-"
                        tglLahirPB = if (!resultDataPemilikKedua?.subjek2BirthDate.isNullOrEmpty()) resultDataPemilikKedua?.subjek2BirthDate else "-"

                        colTTLPB.setCellValue("$tmptLahirPB / $tglLahirPB")
                        ws.addMergedRegion(CellRangeAddress(21, 21, 5, 14))

                        /* pekerjaan */
                        val colPekerjaanPB = rowpekerjaanPT.createCell(5)
                        colPekerjaanPB.cellStyle = cellStyleNoramlTitle
                        if (!resultDataPemilikKedua?.subjek2OccupationName.isNullOrEmpty()) colPekerjaanPB.setCellValue(resultDataPemilikKedua?.subjek2OccupationName) else colPekerjaanPB.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(22, 22, 5, 14))

                        /* alamat */
                        val colAlamatJalan = rowalamatPT.createCell(5)
                        colAlamatJalan.cellStyle = cellStyleNoramlTitle
                        if (!resultDataPemilikKedua?.subjek2DistrictName.isNullOrEmpty()) colAlamatJalan.setCellValue(resultDataPemilikKedua?.subjek2DistrictName) else colAlamatJalan.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(23, 23, 5, 8))

                        val colAlamatBlok = rowalamatPT.createCell(11)
                        colAlamatBlok.cellStyle = cellStyleNoramlTitle
                        if (!resultDataPemilikKedua?.subjek2Block.isNullOrEmpty()) colAlamatBlok.setCellValue(resultDataPemilikKedua?.subjek2Block) else colAlamatBlok.setCellValue("-")

                        val colAlamatRTRW = rowalamatPT.createCell(13)
                        colAlamatRTRW.cellStyle = cellStyleNoramlTitle
                        val noRTPB: String?
                        val noRWPB: String?

                        noRTPB = if (!resultDataPemilikKedua?.subjek2RT.isNullOrEmpty()) resultDataPemilikKedua?.subjek2RT else "-"
                        noRWPB = if (!resultDataPemilikKedua?.subjek2RW.isNullOrEmpty()) resultDataPemilikKedua?.subjek2RW else "-"
                        colAlamatRTRW.setCellValue("$noRTPB / $noRWPB")

                        val rowAlamatDesa = ws.createRow(24)
                        val colAlamatDesa = rowAlamatDesa.createCell(5)
                        colAlamatDesa.cellStyle = cellStyleNoramlTitle
                        colAlamatDesa.setCellValue(resultDataPemilikKedua?.subjek2VillageName)
                        ws.addMergedRegion(CellRangeAddress(24, 24, 5, 8))

                        val rowAlamatKecamatan = ws.createRow(25)
                        val colAlamatKecamatan = rowAlamatKecamatan.createCell(5)
                        colAlamatKecamatan.cellStyle = cellStyleNoramlTitle
                        colAlamatKecamatan.setCellValue(resultDataPemilikKedua?.subjek2DistrictName)
                        ws.addMergedRegion(CellRangeAddress(25, 25, 5, 8))

                        val rowAlamatKota = ws.createRow(26)
                        val colAlamatKota = rowAlamatKota.createCell(5)
                        colAlamatKota.cellStyle = cellStyleNoramlTitle
                        colAlamatKota.setCellValue(resultDataPemilikKedua?.subjek2RegencyName)
                        ws.addMergedRegion(CellRangeAddress(26, 26, 5, 8))

                        val colAlamatProvinsi = rowAlamatKota.createCell(11)
                        colAlamatProvinsi.cellStyle = cellStyleNoramlTitle
                        colAlamatProvinsi.setCellValue(resultDataPemilikKedua?.subjek2ProvinceName)
                        ws.addMergedRegion(CellRangeAddress(26, 26, 11, 13))


                        /* identitas pemilik */
                        val colIdentitasPemilik = rownoIdentitasPT.createCell(5)
                        colIdentitasPemilik.cellStyle = cellStyleNoramlTitle
                        if (!resultDataPemilikKedua?.subjek2IdentityNumber.isNullOrEmpty()) colIdentitasPemilik.setCellValue(resultDataPemilikKedua?.subjek2IdentityNumber) else colIdentitasPemilik.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(27, 27, 5, 8))

                    }
                    else -> {
                        val resultDataAhliWaris = inrealm.where(AhliWaris::class.java)
                            .equalTo("AhliWarisIdTemp", lastCharacterOfIdPemilkTanaman?.toInt())
                            .findFirst()

                        val colNamaLengkap = rownamalengkapPT.createCell(5)
                        colNamaLengkap.cellStyle = cellStyleNoramlTitle
                        colNamaLengkap.setCellValue(resultDataAhliWaris?.partyFullName)
                        ws.addMergedRegion(CellRangeAddress(20, 20, 5, 14))

                        /* ttl*/
                        val colTTLPB = rowttlPT.createCell(5)
                        colTTLPB.cellStyle = cellStyleNoramlTitle

                        val tmptLahirPB: String?
                        val tglLahirPB: String?
                        tmptLahirPB = if (!resultDataAhliWaris?.partyBirthPlaceName.isNullOrEmpty()) resultDataAhliWaris?.partyBirthPlaceName else "-"
                        tglLahirPB = if (!resultDataAhliWaris?.partyBirthDate.isNullOrEmpty()) resultDataAhliWaris?.partyBirthDate else "-"

                        colTTLPB.setCellValue("$tmptLahirPB / $tglLahirPB")
                        ws.addMergedRegion(CellRangeAddress(21, 21, 5, 14))

                        /* pekerjaan */
                        val colPekerjaanPB = rowpekerjaanPT.createCell(5)
                        colPekerjaanPB.cellStyle = cellStyleNoramlTitle
                        if (!resultDataAhliWaris?.partyOccupationName.isNullOrEmpty()) colPekerjaanPB.setCellValue(resultDataAhliWaris?.partyOccupationName) else colPekerjaanPB.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(22, 22, 5, 14))

                        /* alamat */
                        val colAlamatJalan = rowalamatPT.createCell(5)
                        colAlamatJalan.cellStyle = cellStyleNoramlTitle
                        if (!resultDataAhliWaris?.partyDistrictName.isNullOrEmpty()) colAlamatJalan.setCellValue(resultDataAhliWaris?.partyDistrictName) else colAlamatJalan.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(23, 23, 5, 8))

                        val colAlamatBlok = rowalamatPT.createCell(11)
                        colAlamatBlok.cellStyle = cellStyleNoramlTitle
                        if (!resultDataAhliWaris?.partyBlock.isNullOrEmpty()) colAlamatBlok.setCellValue(resultDataAhliWaris?.partyBlock) else colAlamatBlok.setCellValue("-")

                        val colAlamatRTRW = rowalamatPT.createCell(13)
                        colAlamatRTRW.cellStyle = cellStyleNoramlTitle
                        val noRTPB: String?
                        val noRWPB: String?

                        noRTPB = if (!resultDataAhliWaris?.partyRT.isNullOrEmpty()) resultDataAhliWaris?.partyRT else "-"
                        noRWPB = if (!resultDataAhliWaris?.partyRW.isNullOrEmpty()) resultDataAhliWaris?.partyRW else "-"
                        colAlamatRTRW.setCellValue("$noRTPB / $noRWPB")

                        val rowAlamatDesa = ws.createRow(24)
                        val colAlamatDesa = rowAlamatDesa.createCell(5)
                        colAlamatDesa.cellStyle = cellStyleNoramlTitle
                        colAlamatDesa.setCellValue(resultDataAhliWaris?.partyVillageName)
                        ws.addMergedRegion(CellRangeAddress(24, 24, 5, 8))

                        val rowAlamatKecamatan = ws.createRow(25)
                        val colAlamatKecamatan = rowAlamatKecamatan.createCell(5)
                        colAlamatKecamatan.cellStyle = cellStyleNoramlTitle
                        colAlamatKecamatan.setCellValue(resultDataAhliWaris?.partyDistrictName)
                        ws.addMergedRegion(CellRangeAddress(25, 25, 5, 8))

                        val rowAlamatKota = ws.createRow(26)
                        val colAlamatKota = rowAlamatKota.createCell(5)
                        colAlamatKota.cellStyle = cellStyleNoramlTitle
                        colAlamatKota.setCellValue(resultDataAhliWaris?.partyRegencyName)
                        ws.addMergedRegion(CellRangeAddress(26, 26, 5, 8))

                        val colAlamatProvinsi = rowAlamatKota.createCell(11)
                        colAlamatProvinsi.cellStyle = cellStyleNoramlTitle
                        colAlamatProvinsi.setCellValue(resultDataAhliWaris?.partyProvinceName)
                        ws.addMergedRegion(CellRangeAddress(26, 26, 11, 13))

                        /* identitas pemilik */
                        val colIdentitasPemilik = rownoIdentitasPT.createCell(5)
                        colIdentitasPemilik.cellStyle = cellStyleNoramlTitle
                        if (!resultDataAhliWaris?.partyIdentityNumber.isNullOrEmpty()) colIdentitasPemilik.setCellValue(resultDataAhliWaris?.partyIdentityNumber) else colIdentitasPemilik.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(27, 27, 5, 8))
                    }
                }

                val convertTanamanId = resultsDataSepsifikasiTanaman?.jenisTanamanId
                val convertTanamanId2 = convertTanamanId?.replace("[", "['")
                val convertTanamanId3 = convertTanamanId2?.replace("]", "']")
                val convertTanamanId4 = convertTanamanId3?.replace(", ", "','")

                val convertTanamanNama = resultsDataSepsifikasiTanaman?.namaTanaman
                val convertTanamanNama2 = convertTanamanNama?.replace("[", "['")
                val convertTanamanNama3 = convertTanamanNama2?.replace("]", "']")
                val convertTanamanNama4 = convertTanamanNama3?.replace(", ", "','")

                val convertTanamanNamaOther = resultsDataSepsifikasiTanaman?.namaTanamanOther
                val convertTanamanNamaOther2 = convertTanamanNamaOther?.replace("[", "['")
                val convertTanamanNamaOther3 = convertTanamanNamaOther2?.replace("]", "']")
                val convertTanamanNamaOther4 = convertTanamanNamaOther3?.replace(", ", "','")

                val convertTanamanUkuran = resultsDataSepsifikasiTanaman?.ukuranTanaman
                val convertTanamanUkuran2 = convertTanamanUkuran?.replace("[", "['")
                val convertTanamanUkuran3 = convertTanamanUkuran2?.replace("]", "']")
                val convertTanamanUkuran4 = convertTanamanUkuran3?.replace(", ", "','")

                val convertTanamanJumlah = resultsDataSepsifikasiTanaman?.jumlahTanaman
                val convertTanamanJumlah2 = convertTanamanJumlah?.replace("[", "['")
                val convertTanamanJumlah3 = convertTanamanJumlah2?.replace("]", "']")
                val convertTanamanJumlah4 = convertTanamanJumlah3?.replace(", ", "','")

                val dataJenisTanamanId = Gson().fromJson(convertTanamanId4, Array<String>::class.java).toList()
                val dataTanamanName = Gson().fromJson(convertTanamanNama4, Array<String>::class.java).toList()
                val dataTanamanNameOther = Gson().fromJson(convertTanamanNamaOther4, Array<String>::class.java).toList()
                val dataTanamanUkuran = Gson().fromJson(convertTanamanUkuran4, Array<String>::class.java).toList()
                val dataTanamanJumlah = Gson().fromJson(convertTanamanJumlah4, Array<String>::class.java).toList()


                if (!dataJenisTanamanId.isNullOrEmpty()) {
                    for (element in dataJenisTanamanId.indices) {
                        createRow(currentLastRow)
                        val rowNamaTanaman = ws.getRow(oldLastRow)
                        rowNamaTanaman.height = 2 * 256

                        val colNoTanaman = rowNamaTanaman.createCell(2)
                        colNoTanaman.cellStyle = cellStyleNoramlTitleCenter
                        colNoTanaman.setCellValue("${element + 1}")

                        val colTanamanName = rowNamaTanaman.createCell(3)
                        colTanamanName.let {
                            it.cellStyle = cellStyleNoramlTitleCenter
                            if (!dataTanamanNameOther[element].isNullOrEmpty()) {
                                it.setCellValue(dataTanamanNameOther[element])
                            } else {
                                it.setCellValue(dataTanamanName[element])
                            }
                        }
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 3, 6))

                        val colukuranTanamnKecil = rowNamaTanaman.createCell(7)
                        colukuranTanamnKecil.cellStyle = cellStyleNoramlTitleCenter

                        val colukuranTanamnSedang = rowNamaTanaman.createCell(9)
                        colukuranTanamnSedang.cellStyle = cellStyleNoramlTitleCenter

                        val colukuranTanamnBesar = rowNamaTanaman.createCell(11)
                        colukuranTanamnBesar.cellStyle = cellStyleNoramlTitleCenter

                        when {
                            dataTanamanUkuran[element] == "Kecil" -> {
                                colukuranTanamnKecil.setCellValue(dataTanamanJumlah[element])
                                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 7, 8))
                            }
                            dataTanamanUkuran[element] == "Sedang" -> {
                                colukuranTanamnSedang.setCellValue(dataTanamanJumlah[element])
                                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 9, 10))
                            }
                            dataTanamanUkuran[element] == "Besar" -> {
                                colukuranTanamnBesar.setCellValue(dataTanamanJumlah[element])
                                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 11, 12))
                            }
                        }
                    }
                }

                /* tanda tangan */
                createRow(currentLastRow + 1)
                val rowdateTTD = ws.getRow(oldLastRow)
                val colttD = rowdateTTD.createCell(9)
                colttD.cellStyle = cellStyleTglTtd
                colttD.setCellValue("Tempat, $currentDate")
                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 9, 14))

                /* nama ttd petugas & pemilik tanah */
                createRow(currentLastRow + 1)
                val rowpetugasdanpemilikTTD = ws.getRow(oldLastRow)

                val colpetugasTTD = rowpetugasdanpemilikTTD.createCell(2)
                colpetugasTTD.cellStyle = cellStyleTtd
                colpetugasTTD.setCellValue("Petugas Satgas B")
                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 2, 3))

                val colpemilikTTD = rowpetugasdanpemilikTTD.createCell(9)
                colpemilikTTD.cellStyle = cellStyleTtd
                colpemilikTTD.setCellValue("Pemilik Tanah")
                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 9, 14))

                /* garis ttd petugas & pemilik tanah */
                createRow(currentLastRow + 4)
                val rowgarispetugaspemiliTTD = ws.getRow(oldLastRow)
                val colgarispetugasTTD = rowgarispetugaspemiliTTD.createCell(2)
                colgarispetugasTTD.cellStyle = cellStyleTtd
                colgarispetugasTTD.setCellValue("___________________________________")
                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 2, 3))

                val colgarispemilikTTD = rowgarispetugaspemiliTTD.createCell(9)
                colgarispemilikTTD.cellStyle = cellStyleTtd
                colgarispemilikTTD.setCellValue("___________________________________")
                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 9, 14))


                /* nama ttd mengetahui  */
                createRow(currentLastRow + 2)
                val rowmengetahuiTTD = ws.getRow(oldLastRow)
                val colmengetahuiTTD = rowmengetahuiTTD.createCell(4)
                colmengetahuiTTD.cellStyle = cellStyleTtd
                colmengetahuiTTD.setCellValue("Mengetahui")
                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 4, 9))

                createRow(currentLastRow + 4)
                val rowgarismengetahuiTTD = ws.getRow(oldLastRow)
                val colgarismengetahuiTTD = rowgarismengetahuiTTD.createCell(4)
                colgarismengetahuiTTD.cellStyle = cellStyleTtd
                colgarismengetahuiTTD.setCellValue("___________________________________")
                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 4, 9))

            }, {
                activity.runOnUiThread {
                    val pathDir = File(Environment.getExternalStorageDirectory().toString() + File.separator + "SIOJT", "Downloads")
                    val outputFile = File(pathDir, mfilename)

                    var ostream: FileOutputStream? = null
                    try {
                        if (!pathDir.exists()) {
                            pathDir.mkdirs()
                        }

                        if (!outputFile.exists()) {
                            outputFile.delete()
                            outputFile.createNewFile()
                        }

                        ostream = FileOutputStream(outputFile)
                        wb.write(ostream)
                        ostream.flush()
                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                        Log.d("Error", "${e.message}")

                    } catch (e: Exception) {
                        e.printStackTrace()
                        Log.d("Error", "Export File Gagal")
                    } finally {
                        if (ostream != null) {
                            ostream.close()
                            try {
                                sweetAlretLoading = SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                sweetAlretLoading.titleText = "Export Data Berhasil"
                                sweetAlretLoading.setCancelable(false)
                                sweetAlretLoading.contentText = outputFile.toString()
                                sweetAlretLoading.confirmText = "OK"
                                sweetAlretLoading.setConfirmClickListener { sDialog ->
                                    sDialog?.let { if (it.isShowing) it.dismiss() }
                                    openFile(mfilename)
                                }
                                sweetAlretLoading.show()
                            } catch (e: IOException) {
                                Log.d("Error", "File gagal dibuka ${e.message}")
                            }
                        }


                    }
                }
            }, {
                Log.d("Error", "Gagal export data")
                Log.d("Error", it.message.toString())
                activity.runOnUiThread {
                    sweetAlretLoading = SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                    sweetAlretLoading.titleText = "Failed"
                    sweetAlretLoading.setCancelable(false)
                    sweetAlretLoading.contentText = "Export Data Gagal"
                    sweetAlretLoading.confirmText = "OK"
                    sweetAlretLoading.setConfirmClickListener { sDialog ->
                        sDialog?.let { if (it.isShowing) it.dismiss() }

                    }
                    sweetAlretLoading.show()
                }
            })
        }, 10000)
    }

    private fun convertDocToPdfitext(landId: Int?, tanamanTempId: Int?, tanamanName: String) {
        val document = Document(PageSize.A4.rotate(), 40f, 40f, 35f, 40f)
        val pathDir = File(Environment.getExternalStorageDirectory().toString() + File.separator + "SIOJT", "Downloads")
        val mfileName = "$tanamanName.pdf"
        val outputFile = File(pathDir, mfileName)

        Handler().postDelayed({
            try {
                val fos = FileOutputStream(outputFile)

                val pdfWriter = PdfWriter.getInstance(document, fos)
                document.open()

                val fontBold = Font(Font.FontFamily.HELVETICA, 16f, Font.BOLD)
                val headerTitle1 = Paragraph("PELAKSANAAN PENGADAAN TANAH UNTUK PEMBANGUNAN", fontBold)
                headerTitle1.alignment = Element.ALIGN_CENTER

                val headerTitle12 = Paragraph("DATA PEMILIK TANAMAN YANG TERKENA PENGADAAN TANAH", fontBold)
                headerTitle12.alignment = Element.ALIGN_CENTER
                headerTitle12.spacingAfter = 20f

                var headerproyekName: Paragraph? = null
                realm.executeTransaction { inrealm ->
                    val results = inrealm.where(ProjectLand::class.java).equalTo("PartyIdTemp", landId).findFirst()
                    if (results != null) {
                        val dataProyek = inrealm.where(ResponseDataListProjectLocal::class.java).equalTo("projectAssignProjectId", results.projectId).findFirst()
                        headerproyekName = Paragraph(dataProyek?.getProjectName().toString().toUpperCase(), fontBold)
                        headerproyekName?.alignment = Element.ALIGN_CENTER
                        headerproyekName?.spacingAfter = 20f
                    }
                }

                document.add(headerTitle1)
                document.add(headerproyekName)
                document.add(headerTitle12)

                val firstTable = createFirstTable(landId, tanamanTempId)
                val secondTable = createSecondTable(landId, tanamanTempId)
                document.add(firstTable)
                document.add(secondTable)

                document.close()
                pdfWriter.close()
            } catch (e: DocumentException) {
                Log.d("DocumentException", e.message.toString())
            } catch (e: Exception) {
                Log.d("Exception", e.message.toString())
            } finally {
                activity.runOnUiThread {
                    sweetAlretLoading = SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                    sweetAlretLoading.titleText = "Export Data Berhasil"
                    sweetAlretLoading.setCancelable(false)
                    sweetAlretLoading.contentText = outputFile.toString()
                    sweetAlretLoading.confirmText = "OK"
                    sweetAlretLoading.setConfirmClickListener { sDialog ->
                        sDialog?.let { if (it.isShowing) it.dismiss() }
                        openFile(mfileName)
                    }
                    sweetAlretLoading.show()
                }
            }
        }, 5000)

    }

    private fun createFirstTable(landId: Int?, tanamanTempId: Int?): PdfPTable {
        val table = PdfPTable(15)
        table.defaultCell.border = 0
        //setColumnWidth
        table.setTotalWidth(floatArrayOf(20f, 50f, 144f, 20f, 20f,
            50f, 50f, 50f, 50f, 50f,
            50f, 50f, 50f, 50f, 20f))
        table.widthPercentage = 100f

        //Letak tanah
        var letakProvinsi: String? = ""
        var letakKota: String? = ""
        var letakKecamatan: String? = ""
        var letakKelurahan: String? = ""
        var letakRTRW: String? = ""
        var letakNo: String? = ""
        var letakJalan: String? = ""
        var letakNoBidang: String? = ""

        //Pemilik pertama tanah
        var namaPemilikPertama: String? = ""
        var ttlPemilikPertama: String? = ""
        var pekerjaanPemilikPertama: String? = ""
        var alamatJlnPemilikPertama: String? = ""
        var alamatBlokPemilikPertama: String? = ""
        var alamatRtRwPemilikPertama: String? = ""
        var alamatKelurahanPemilikPertama: String? = ""
        var alamatKecamatanPemilikPertama: String? = ""
        var alamatKotaPemilikPertama: String? = ""
        var alamatProvinsiPemilikPertama: String? = ""

        var nomorIdentitasPemilikPertama: String? = ""

        realm.executeTransaction { inrealm ->
            val results = inrealm.where(ProjectLand::class.java).equalTo("PartyIdTemp", landId).findFirst()
            if (results != null) {
                letakProvinsi = ""
                letakKota = ""
                letakKecamatan = ""
                letakKelurahan = ""
                letakRTRW = ""
                letakNo = ""
                letakJalan = ""
                letakNoBidang = ""

                namaPemilikPertama = ""
                ttlPemilikPertama = ""
                pekerjaanPemilikPertama = ""
                alamatJlnPemilikPertama = ""
                alamatBlokPemilikPertama = ""
                alamatRtRwPemilikPertama = ""
                alamatKelurahanPemilikPertama = ""
                alamatKecamatanPemilikPertama = ""
                alamatKotaPemilikPertama = ""
                alamatProvinsiPemilikPertama = ""

                nomorIdentitasPemilikPertama = ""

                //letak tanah
                letakProvinsi = if (!results.landProvinceName.isNullOrEmpty()) {
                    results.landProvinceName
                } else {
                    "-"
                }

                letakKota = if (!results.landRegencyName.isNullOrEmpty()) {
                    results.landRegencyName
                } else {
                    "-"
                }

                letakKecamatan = if (!results.landDistrictName.isNullOrEmpty()) {
                    results.landDistrictName
                } else {
                    "-"
                }

                letakKelurahan = if (!results.landVillageName.isNullOrEmpty()) {
                    results.landVillageName
                } else {
                    "-"
                }

                letakRTRW = if (!results.landRT.isNullOrEmpty() || !results.landRW.isNullOrEmpty()) {
                    "${results.landRT} / ${results.landRW}"
                } else {
                    "-"
                }

                letakJalan = if (!results.landStreet.isNullOrEmpty()) {
                    results.landStreet
                } else {
                    "-"
                }

                letakNo = if (!results.landBlok.isNullOrEmpty()) {
                    results.landBlok
                } else {
                    "-"
                }

                letakNoBidang = if (!results.landNumberList.isNullOrEmpty()) {
                    results.landNumberList
                } else {
                    "-"
                }

                /* spesifikasi tanaman */
                val resultsDataSepsifikasiTanaman = inrealm.where(Tanaman::class.java)
                    .equalTo("LandIdTemp", landId)
                    .and()
                    .equalTo("TanamanIdTemp", tanamanTempId)
                    .findFirst()


                /* pemilik tanaman */
                val idPemilikTanaman = resultsDataSepsifikasiTanaman?.pemilikTanamanId
                val firstCharacterOfIdPemilkTanaman = idPemilikTanaman?.get(0)
                val lastCharacterOfIdPemilkTanaman = idPemilikTanaman?.substring(idPemilikTanaman.indexOf("$firstCharacterOfIdPemilkTanaman") + 1)

                when {
                    firstCharacterOfIdPemilkTanaman.toString() == "P" -> {
                        val resultDataPemilikPertama = inrealm.where(PartyAdd::class.java)
                            .equalTo("TempId", lastCharacterOfIdPemilkTanaman?.toInt())
                            .findFirst()

                        val tmpLahirPemilik = if (!resultDataPemilikPertama?.partyBirthPlaceName.toString().isNullOrEmpty()) {
                            resultDataPemilikPertama?.partyBirthPlaceName.toString()
                        } else {
                            "-"
                        }

                        val tglLahirPemilik = if (!resultDataPemilikPertama?.partyBirthDate.toString().isNullOrEmpty()) {
                            resultDataPemilikPertama?.partyBirthDate.toString()
                        } else {
                            "-"
                        }

                        val alamartRT = if (!resultDataPemilikPertama?.partyRT.toString().isNullOrEmpty()) {
                            resultDataPemilikPertama?.partyRT.toString()
                        } else {
                            "-"
                        }

                        val alamartRW = if (!resultDataPemilikPertama?.partyRW.toString().isNullOrEmpty()) {
                            resultDataPemilikPertama?.partyRW.toString()
                        } else {
                            "-"
                        }

                        namaPemilikPertama = resultDataPemilikPertama?.partyFullName
                        ttlPemilikPertama = "$tmpLahirPemilik, $tglLahirPemilik"

                        pekerjaanPemilikPertama = if (!resultDataPemilikPertama?.partyOccupationName.toString().isNullOrEmpty()) {
                            resultDataPemilikPertama?.partyOccupationName.toString()
                        } else {
                            "-"
                        }

                        /* alamat pemilik pertama*/
                        alamatJlnPemilikPertama = if (!resultDataPemilikPertama?.partyStreetName.toString().isNullOrEmpty()) {
                            resultDataPemilikPertama?.partyStreetName.toString()
                        } else {
                            "-"
                        }

                        alamatBlokPemilikPertama = if (!resultDataPemilikPertama?.partyBlock.toString().isNullOrEmpty()) {
                            resultDataPemilikPertama?.partyBlock.toString()
                        } else {
                            "-"
                        }

                        alamatRtRwPemilikPertama = "$alamartRT / $alamartRW"
                        alamatKelurahanPemilikPertama = resultDataPemilikPertama?.partyVillageName
                        alamatKecamatanPemilikPertama = resultDataPemilikPertama?.partyDistrictName
                        alamatKotaPemilikPertama = resultDataPemilikPertama?.partyRegencyName
                        alamatProvinsiPemilikPertama = resultDataPemilikPertama?.partyProvinceName
                        nomorIdentitasPemilikPertama = if (!resultDataPemilikPertama?.partyIdentityNumber.toString().isNullOrEmpty()) {
                            resultDataPemilikPertama?.partyIdentityNumber
                        } else {
                            "-"
                        }

                    }
                    firstCharacterOfIdPemilkTanaman.toString() == "S" -> {
                        val resultDataPemilikKedua = inrealm.where(Subjek2::class.java)
                            .equalTo("SubjekIdTemp", lastCharacterOfIdPemilkTanaman?.toInt())
                            .findFirst()

                        val tmpLahirPemilik = if (!resultDataPemilikKedua?.subjek2BirthPlaceName.toString().isNullOrEmpty()) {
                            resultDataPemilikKedua?.subjek2BirthPlaceName.toString()
                        } else {
                            "-"
                        }

                        val tglLahirPemilik = if (!resultDataPemilikKedua?.subjek2BirthDate.toString().isNullOrEmpty()) {
                            resultDataPemilikKedua?.subjek2BirthDate.toString()
                        } else {
                            "-"
                        }

                        val alamartRT = if (!resultDataPemilikKedua?.subjek2RT.toString().isNullOrEmpty()) {
                            resultDataPemilikKedua?.subjek2RT.toString()
                        } else {
                            "-"
                        }

                        val alamartRW = if (!resultDataPemilikKedua?.subjek2RW.toString().isNullOrEmpty()) {
                            resultDataPemilikKedua?.subjek2RW.toString()
                        } else {
                            "-"
                        }

                        namaPemilikPertama = resultDataPemilikKedua?.subjek2FullName
                        ttlPemilikPertama = "$tmpLahirPemilik, $tglLahirPemilik"

                        pekerjaanPemilikPertama = if (!resultDataPemilikKedua?.subjek2OccupationName.toString().isNullOrEmpty()) {
                            resultDataPemilikKedua?.subjek2OccupationName.toString()
                        } else {
                            "-"
                        }

                        /* alamat pemilik pertama*/
                        alamatJlnPemilikPertama = if (!resultDataPemilikKedua?.subjek2StreetName.toString().isNullOrEmpty()) {
                            resultDataPemilikKedua?.subjek2StreetName.toString()
                        } else {
                            "-"
                        }

                        alamatBlokPemilikPertama = if (!resultDataPemilikKedua?.subjek2Block.toString().isNullOrEmpty()) {
                            resultDataPemilikKedua?.subjek2Block.toString()
                        } else {
                            "-"
                        }

                        alamatRtRwPemilikPertama = "$alamartRT / $alamartRW"
                        alamatKelurahanPemilikPertama = resultDataPemilikKedua?.subjek2VillageName
                        alamatKecamatanPemilikPertama = resultDataPemilikKedua?.subjek2DistrictName
                        alamatKotaPemilikPertama = resultDataPemilikKedua?.subjek2RegencyName
                        alamatProvinsiPemilikPertama = resultDataPemilikKedua?.subjek2ProvinceName
                        nomorIdentitasPemilikPertama = if (!resultDataPemilikKedua?.subjek2IdentityNumber.toString().isNullOrEmpty()) {
                            resultDataPemilikKedua?.subjek2IdentityNumber
                        } else {
                            "-"
                        }

                    }
                    else -> {
                        val resultDataAhliWaris = inrealm.where(AhliWaris::class.java)
                            .equalTo("AhliWarisIdTemp", lastCharacterOfIdPemilkTanaman?.toInt())
                            .findFirst()

                        val tmpLahirPemilik = if (!resultDataAhliWaris?.partyBirthPlaceName.toString().isNullOrEmpty()) {
                            resultDataAhliWaris?.partyBirthPlaceName.toString()
                        } else {
                            "-"
                        }

                        val tglLahirPemilik = if (!resultDataAhliWaris?.partyBirthDate.toString().isNullOrEmpty()) {
                            resultDataAhliWaris?.partyBirthDate.toString()
                        } else {
                            "-"
                        }

                        val alamartRT = if (!resultDataAhliWaris?.partyRT.toString().isNullOrEmpty()) {
                            resultDataAhliWaris?.partyRT.toString()
                        } else {
                            "-"
                        }

                        val alamartRW = if (!resultDataAhliWaris?.partyRW.toString().isNullOrEmpty()) {
                            resultDataAhliWaris?.partyRW.toString()
                        } else {
                            "-"
                        }

                        namaPemilikPertama = resultDataAhliWaris?.partyFullName
                        ttlPemilikPertama = "$tmpLahirPemilik, $tglLahirPemilik"

                        pekerjaanPemilikPertama = if (!resultDataAhliWaris?.partyOccupationName.toString().isNullOrEmpty()) {
                            resultDataAhliWaris?.partyOccupationName.toString()
                        } else {
                            "-"
                        }

                        /* alamat pemilik pertama*/
                        alamatJlnPemilikPertama = if (!resultDataAhliWaris?.partyStreetName.toString().isNullOrEmpty()) {
                            resultDataAhliWaris?.partyStreetName.toString()
                        } else {
                            "-"
                        }

                        alamatBlokPemilikPertama = if (!resultDataAhliWaris?.partyBlock.toString().isNullOrEmpty()) {
                            resultDataAhliWaris?.partyBlock.toString()
                        } else {
                            "-"
                        }

                        alamatRtRwPemilikPertama = "$alamartRT / $alamartRW"
                        alamatKelurahanPemilikPertama = resultDataAhliWaris?.partyVillageName
                        alamatKecamatanPemilikPertama = resultDataAhliWaris?.partyDistrictName
                        alamatKotaPemilikPertama = resultDataAhliWaris?.partyRegencyName
                        alamatProvinsiPemilikPertama = resultDataAhliWaris?.partyProvinceName
                        nomorIdentitasPemilikPertama = if (!resultDataAhliWaris?.partyIdentityNumber.toString().isNullOrEmpty()) {
                            resultDataAhliWaris?.partyIdentityNumber
                        } else {
                            "-"
                        }
                    }

                }

            }//letak tanah
        }

        table.addCell(createCellItextPdfBoldTextCenter("I", true))
        table.addCell(createCellItextPdfWithColspanBold("LETAK TANAH", 14, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, true))

        //Provinsi
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Provinsi", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakProvinsi.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Kota
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Kota", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakKota.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Kabupaten
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Kabupaten", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakKota.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Kecamatan
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Kecamatan", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakKecamatan.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Desa/Kelurahan
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Desa / Kelurahan", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakKelurahan.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //RT/RW
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- RT / RW", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakRTRW.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //RT/RW
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Jalan", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakJalan.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Blok/No
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Blok / No", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakNo.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //No bidang
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- No Bidang", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakNoBidang.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Petugas Satgas
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Petugas Satgas B", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(Preference.auth.dataUser?.mobileUserEmployeeName.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))


        //space letak dan pemilik pertama tanah
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Pemilik tanah pertama
        table.addCell(createCellItextPdfBoldTextCenter("II", true))
        table.addCell(createCellItextPdfWithColspanBold("PEMILIK TANAMAN", 14, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, true))

        //nama pemilik pertama
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Nama Lengkap", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(namaPemilikPertama.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //ttl pemilikpertama
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Tempat / Tanggal Lahir", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(ttlPemilikPertama.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //pekerjaan pemilik pertama
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Pekerjaan", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(pekerjaanPemilikPertama.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //alamat pemilik pertama
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Alamat", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        //alamat jalan
        table.addCell(createCellItextPdfWithColspanBold(alamatJlnPemilikPertama.toString(), 4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell("")
        //alamat blok
        table.addCell(createCellItextPdfCustomPositionText(alamatBlokPemilikPertama.toString(), Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell("")
        //alamat rt rw
        table.addCell(createCellItextPdfWithColspanBold(alamatRtRwPemilikPertama.toString(), 4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        //alamat kelurahan
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold(alamatKelurahanPemilikPertama.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        //alamat kecamatan
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold(alamatKecamatanPemilikPertama.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        //alamat kota & provinsi
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold(alamatKotaPemilikPertama.toString(), 4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold(alamatProvinsiPemilikPertama.toString(), 6, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Nik / no. SIM
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- NIK / No. SIM", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(nomorIdentitasPemilikPertama.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Terletak diatas tanah milik", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold("-", 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //space letak
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBoldCustomFonSize("SPESIFIKASI TANAMAN", 15, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 16f))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //header table spesifikasi tanaman
        table.addCell(createCellItextPdfWithRowspanColspanBoldCustomHeightCell("No.", 2, 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, isBold = true, isBorder = true, heightColumn = 30f))
        table.addCell(createCellItextPdfWithRowspanColspanBoldCustomHeightCell("Nama Tanaman", 2, 5, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, isBold = true, isBorder = true, heightColumn = 30f))
        table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell("Jumlah Tanaman", 8, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, isBold = true, isBorder = true, heightColumn = 30f))
        table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell("Kecil", 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, isBold = true, isBorder = true, heightColumn = 30f))
        table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell("Sedang", 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, isBold = true, isBorder = true, heightColumn = 30f))
        table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell("Besar", 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, isBold = true, isBorder = true, heightColumn = 30f))
        table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell("Rumpun", 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, isBold = true, isBorder = true, heightColumn = 30f))

        return table
    }

    private fun createSecondTable(landId: Int?, tanamanTempId: Int?): PdfPTable {
        val table = PdfPTable(15)
        table.defaultCell.border = 0
        //setColumnWidth
        table.setTotalWidth(floatArrayOf(20f, 50f, 144f, 20f, 20f,
            50f, 50f, 50f, 50f, 50f,
            50f, 50f, 50f, 50f, 20f))
        table.widthPercentage = 100f

        val calendar = Calendar.getInstance(Locale.getDefault())
        //c.get(Calendar.YEAR)
        //c.get(Calendar.MONTH) + 1
        //c.get(Calendar.DAY_OF_MONTH)
        //val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        //val currentDate = sdf.format(c.time)
        val currentDate = DateFormat.format(dateTemplate, calendar.time)

        realm.executeTransaction { inrealm ->
            /* spesifikasi tanaman */
            val resultsDataSepsifikasiTanaman = inrealm.where(Tanaman::class.java)
                .equalTo("LandIdTemp", landId)
                .and()
                .equalTo("TanamanIdTemp", tanamanTempId)
                .findFirst()

            val convertTanamanId = resultsDataSepsifikasiTanaman?.jenisTanamanId
            val convertTanamanId2 = convertTanamanId?.replace("[", "['")
            val convertTanamanId3 = convertTanamanId2?.replace("]", "']")
            val convertTanamanId4 = convertTanamanId3?.replace(", ", "','")

            val convertTanamanNama = resultsDataSepsifikasiTanaman?.namaTanaman
            val convertTanamanNama2 = convertTanamanNama?.replace("[", "['")
            val convertTanamanNama3 = convertTanamanNama2?.replace("]", "']")
            val convertTanamanNama4 = convertTanamanNama3?.replace(", ", "','")

            val convertTanamanNamaOther = resultsDataSepsifikasiTanaman?.namaTanamanOther
            val convertTanamanNamaOther2 = convertTanamanNamaOther?.replace("[", "['")
            val convertTanamanNamaOther3 = convertTanamanNamaOther2?.replace("]", "']")
            val convertTanamanNamaOther4 = convertTanamanNamaOther3?.replace(", ", "','")

            val convertTanamanUkuran = resultsDataSepsifikasiTanaman?.ukuranTanaman
            val convertTanamanUkuran2 = convertTanamanUkuran?.replace("[", "['")
            val convertTanamanUkuran3 = convertTanamanUkuran2?.replace("]", "']")
            val convertTanamanUkuran4 = convertTanamanUkuran3?.replace(", ", "','")

            val convertTanamanJumlah = resultsDataSepsifikasiTanaman?.jumlahTanaman
            val convertTanamanJumlah2 = convertTanamanJumlah?.replace("[", "['")
            val convertTanamanJumlah3 = convertTanamanJumlah2?.replace("]", "']")
            val convertTanamanJumlah4 = convertTanamanJumlah3?.replace(", ", "','")

            val dataJenisTanamanId = Gson().fromJson(convertTanamanId4, Array<String>::class.java).toList()
            val dataTanamanName = Gson().fromJson(convertTanamanNama4, Array<String>::class.java).toList()
            val dataTanamanNameOther = Gson().fromJson(convertTanamanNamaOther4, Array<String>::class.java).toList()
            val dataTanamanUkuran = Gson().fromJson(convertTanamanUkuran4, Array<String>::class.java).toList()
            val dataTanamanJumlah = Gson().fromJson(convertTanamanJumlah4, Array<String>::class.java).toList()


            if (!dataJenisTanamanId.isNullOrEmpty()) {
                for ((indexIdTanaman, valueIdTanaman) in dataJenisTanamanId.withIndex()) {
                    table.addCell(createCellItextPdfWithRowspanColspanBoldCustomHeightCell("${indexIdTanaman + 1}", 2, 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, isBold = false, isBorder = true, heightColumn = 20f))

                    if (!dataTanamanNameOther[indexIdTanaman].isNullOrEmpty()) {
                        table.addCell(createCellItextPdfWithRowspanColspanBoldCustomHeightCell(dataTanamanNameOther[indexIdTanaman], 2, 5, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, isBold = false, isBorder = true, heightColumn = 20f))
                    } else {
                        table.addCell(createCellItextPdfWithRowspanColspanBoldCustomHeightCell(dataTanamanName[indexIdTanaman], 2, 5, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, isBold = false, isBorder = true, heightColumn = 20f))
                    }

                    if (!dataTanamanUkuran[indexIdTanaman].isNullOrEmpty() && dataTanamanUkuran[indexIdTanaman] == "Kecil") {
                        table.addCell(createCellItextPdfWithRowspanColspanBoldCustomHeightCell(dataTanamanJumlah[indexIdTanaman], 2, 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, isBold = false, isBorder = true, heightColumn = 20f))
                    } else {
                        table.addCell(createCellItextPdfWithRowspanColspanBoldCustomHeightCell("-", 2, 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, isBold = false, isBorder = true, heightColumn = 20f))
                    }

                    if (!dataTanamanUkuran[indexIdTanaman].isNullOrEmpty() && dataTanamanUkuran[indexIdTanaman] == "Sedang") {
                        table.addCell(createCellItextPdfWithRowspanColspanBoldCustomHeightCell(dataTanamanJumlah[indexIdTanaman], 2, 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, isBold = false, isBorder = true, heightColumn = 20f))
                    } else {
                        table.addCell(createCellItextPdfWithRowspanColspanBoldCustomHeightCell("-", 2, 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, isBold = false, isBorder = true, heightColumn = 20f))
                    }

                    if (!dataTanamanUkuran[indexIdTanaman].isNullOrEmpty() && dataTanamanUkuran[indexIdTanaman] == "Besar") {
                        table.addCell(createCellItextPdfWithRowspanColspanBoldCustomHeightCell(dataTanamanJumlah[indexIdTanaman], 2, 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, isBold = false, isBorder = true, heightColumn = 20f))
                    } else {
                        table.addCell(createCellItextPdfWithRowspanColspanBoldCustomHeightCell("-", 2, 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, isBold = false, isBorder = true, heightColumn = 20f))
                    }

                    table.addCell(createCellItextPdfWithRowspanColspanBoldCustomHeightCell("-", 2, 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, isBold = false, isBorder = true, heightColumn = 20f))

                }//end loop dataId Tanaman
            }//end if dataId Tanaman
        }

        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("Tempat, $currentDate", 13, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, false))
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //tanda tangan
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, true))

        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBoldFonSize("Petugas Satgas B", 4, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 16f))
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBoldFonSize("Pemilik Tanah", 5, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 16f))
        table.addCell("")
        table.addCell("")

        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("_______________________________", 4, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true))
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("_______________________________", 5, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true))
        table.addCell("")
        table.addCell("")

        //mengetahui ttd
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBoldFonSize("Mengetahui", 7, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 16f))
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")


        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("_______________________________", 7, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true))
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")

        return table
    }

    private fun createCellItextPdfWithColspanBoldFonSize(content: String, total_colspan: Int, horizontalElement: Int, verticalElement: Int, isBold: Boolean, fontSize: Float): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, fontSize, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, fontSize)
        }

        val cell = PdfPCell(Phrase(content, font))
        cell.border = Rectangle.NO_BORDER
        cell.colspan = total_colspan
        cell.isNoWrap = false
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }

    private fun createCellItextPdfCustomPositionText(content: String, horizontalElement: Int, verticalElement: Int, isBold: Boolean): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, 12f)
        }
        val cell = PdfPCell(Phrase(content, font))
        cell.isNoWrap = false
        cell.border = Rectangle.NO_BORDER
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }

    private fun createCellItextPdfBoldTextCenter(content: String, isBold: Boolean): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, 12f)
        }

        val cell = PdfPCell(Phrase(content, font))
        cell.isNoWrap = false
        cell.border = Rectangle.NO_BORDER
        cell.horizontalAlignment = Element.ALIGN_CENTER
        cell.verticalAlignment = Element.ALIGN_CENTER
        return cell
    }

    private fun createCellItextPdfWithRowspanBold(content: String, total_rowspan: Int, horizontalElement: Int, verticalElement: Int, isBold: Boolean): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, 12f)
        }
        val cell = PdfPCell(Phrase(content, font))
        cell.isNoWrap = false
        cell.border = Rectangle.NO_BORDER
        cell.rowspan = total_rowspan
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }

    private fun createCellItextPdfWithColspanBoldCustomFonSize(content: String, total_colspan: Int, horizontalElement: Int, verticalElement: Int, isBold: Boolean, fontSize: Float): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, fontSize, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, fontSize)
        }

        val cell = PdfPCell(Phrase(content, font))
        cell.border = Rectangle.NO_BORDER
        cell.colspan = total_colspan
        cell.isNoWrap = false
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }

    private fun createCellItextPdfWithColspanBold(content: String, total_colspan: Int, horizontalElement: Int, verticalElement: Int, isBold: Boolean): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, 12f)
        }

        val cell = PdfPCell(Phrase(content, font))
        cell.border = Rectangle.NO_BORDER
        cell.colspan = total_colspan
        cell.isNoWrap = false
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }

    private fun createCellItextPdfWithColspanBoldCustomHeightCell(content: String, total_colspan: Int, horizontalElement: Int, verticalElement: Int, isBold: Boolean, isBorder: Boolean, heightColumn: Float): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, 12f)
        }

        val cell = PdfPCell(Phrase(content, font))
        cell.colspan = total_colspan

        if (!isBorder) {
            cell.border = Rectangle.NO_BORDER
        }

        cell.isNoWrap = false
        cell.fixedHeight = heightColumn
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }

    private fun createCellItextPdfWithRowspanBoldCustomHeightCell(content: String, total_rowspan: Int, horizontalElement: Int, verticalElement: Int, isBold: Boolean, heightColumn: Float): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, 12f)
        }

        val cell = PdfPCell(Phrase(content, font))
        cell.border = Rectangle.NO_BORDER
        cell.rowspan = total_rowspan
        cell.isNoWrap = false
        cell.fixedHeight = heightColumn
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }

    private fun createCellItextPdfWithRowspanColspanBoldCustomHeightCell(content: String, total_rowspan: Int, total_colspan: Int, horizontalElement: Int, verticalElement: Int, isBold: Boolean, isBorder: Boolean, heightColumn: Float): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, 12f)
        }

        val cell = PdfPCell(Phrase(content, font))
        if (!isBorder) {
            cell.border = Rectangle.NO_BORDER
        }
        cell.rowspan = total_rowspan
        cell.colspan = total_colspan
        cell.isNoWrap = false
        cell.fixedHeight = heightColumn
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }


    @SuppressLint("SdCardPath")
    fun openFile(filename: String) {

        val outputExportExcelFile = File(Environment.getExternalStorageDirectory().toString() + File.separator + "SIOJT" + File.separator + "Downloads", filename)
        //val path = Uri.fromFile(outputExportExcelFile)
        val path = FileProvider.getUriForFile(activity, activity.packageName, outputExportExcelFile)
        val intent = Intent(Intent.ACTION_VIEW)
        // Check what kind of file you are trying to open, by comparing the url with extensions.
        // When the if condition is matched, plugin sets the correct intent (mime) type,
        // so Android knew what application to use to open the file
        if (filename.contains(".doc") || filename.contains(".docx")) {
            // Word document
            intent.setDataAndType(path, "application/msword")
        } else if (filename.contains(".pdf")) {
            // PDF file
            intent.setDataAndType(path, "application/pdf")
        } else if (filename.contains(".ppt") || filename.contains(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(path, "application/vnd.ms-powerpoint")
        } else if (filename.contains(".xls") || filename.contains(".xlsx")) {
            // Excel file
            intent.setDataAndType(path, "application/vnd.ms-excel")
        } else if (filename.contains(".zip") || filename.contains(".rar")) {
            // WAV audio file
            intent.setDataAndType(path, "application/x-wav")
        } else if (filename.contains(".rtf")) {
            // RTF file
            intent.setDataAndType(path, "application/rtf")
        } else if (filename.contains(".wav") || filename.contains(".mp3")) {
            // WAV audio file
            intent.setDataAndType(path, "audio/x-wav")
        } else if (filename.contains(".gif")) {
            // GIF file
            intent.setDataAndType(path, "image/gif")
        } else if (filename.contains(".jpg") || filename.contains(".jpeg") || filename.contains(".png")) {
            // JPG file
            intent.setDataAndType(path, "image/jpeg")
        } else if (filename.contains(".txt")) {
            // Text file
            intent.setDataAndType(path, "text/plain")
        } else if (filename.contains(".3gp") || filename.contains(".mpg") || filename.contains(".mpeg") || filename.contains(".mpe") || filename.contains(".mp4") || filename.contains(".avi")) {
            // Video files
            intent.setDataAndType(path, "video/*")
        } else {
            //if you want you can also define the intent type for any other file

            //additionally use else clause below, to manage other unknown extensions
            //in this case, Android will show all applications installed on the device
            //so you can choose which application to use
            intent.setDataAndType(path, "*/*")
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION

        try {
            activity.startActivity(Intent.createChooser(intent, "Plih aplikasi"))
        } catch (e: ActivityNotFoundException) {
            //Toast.makeText(this@Detailsurat_masuk_activity, "No Application available to view PDF", Toast.LENGTH_SHORT).show()
            Toast.makeText(activity, "Tidak ada aplikasi yang tersedia untuk membuka file", Toast.LENGTH_SHORT).show()
        }


    }

    private fun createRow(numRow: Int) {
        ws.createRow(numRow)
        oldLastRow = numRow
        this.currentLastRow = numRow + 1
    }

}
