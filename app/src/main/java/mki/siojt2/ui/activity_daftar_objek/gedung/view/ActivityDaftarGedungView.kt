package mki.siojt2.ui.activity_daftar_objek.gedung.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.response_data_objek.ResponseDataListGedung

interface ActivityDaftarGedungView: MvpView {

    fun oncsuccessgetDaftarGedung(responseDataListGedung: MutableList<ResponseDataListGedung>)
}