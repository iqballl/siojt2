package mki.siojt2.ui.activity_daftar_objek.fasilitas_lain

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.response_data_objek.ResponseDataListDetailFasilitasLain
import mki.siojt2.model.response_data_objek.ResponseDataListFasilitasLain

interface DaftarObjekFasilitasLainView: MvpView {

    fun onsuccessgetdaftarFasilitasLain(responseDataListFasilitasLain: MutableList<ResponseDataListFasilitasLain>)
    fun onsuccessgetdetailFasilitasLain(responseDataListDetailFasilitasLain: ResponseDataListDetailFasilitasLain)
}