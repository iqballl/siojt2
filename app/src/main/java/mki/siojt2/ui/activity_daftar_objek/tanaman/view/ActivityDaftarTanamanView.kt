package mki.siojt2.ui.activity_daftar_objek.tanaman.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.response_data_objek.ResponseDataListGedung
import mki.siojt2.model.response_data_objek.ResponseDataListTanaman

interface ActivityDaftarTanamanView: MvpView {

    fun oncsuccessgetDaftarTanaman(responseDataListTanaman: MutableList<ResponseDataListTanaman>)
}