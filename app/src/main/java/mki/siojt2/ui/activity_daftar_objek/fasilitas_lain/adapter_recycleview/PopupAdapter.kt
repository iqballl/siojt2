package mki.siojt2.ui.activity_daftar_objek.fasilitas_lain.adapter_recycleview

import android.annotation.SuppressLint
import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.gson.Gson
import mki.siojt2.R
import mki.siojt2.model.response_data_objek.ResponseDataListDetailFasilitasLain


class PopupAdapter(mDatasetxes: String, private val context: Context) : androidx.recyclerview.widget.RecyclerView.Adapter<PopupAdapter.DetailViewHolder>() {

    private var mDataset: List<String>? = null
    var rowLayout = R.layout.item_popup
    var pos = 0



    init {
        val toJson = Gson().fromJson(mDatasetxes , Array<String>::class.java).toList()
        this.mDataset = toJson
        pos = 0

        Log.e("saranadetail",toJson.toString())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(rowLayout, parent, false)

        return DetailViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: DetailViewHolder, position: Int) {

        val liveName = mDataset!![9]
        val replace1 = liveName.replace("[", "")
        val replace2 = replace1.replace("]", "")

        if (replace2.equals("1")) {
            holder.title2.visibility = View.GONE
            holder.isi2.visibility = View.GONE
        }

        holder.txttitlenamaPemilik.text = "Pemilik sarana pelengkap"
        holder.txtnamaPemilik.text = mDataset!![7]


        holder.title.text = "Jenis Benda lain yang berhubungan dengan tanah"
        holder.isi.text = mDataset!![10]

        val sarana = mDataset!![11]
        val rep1 = sarana.replace("[", "")
        val rep2 = rep1.replace("]", "")
        holder.title2.text = "Nama sarana pelengkap"
        holder.isi2.text = rep2


    }

    override fun getItemCount(): Int {
        return 1
    }

    inner class DetailViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var txttitlenamaPemilik: TextView = itemView.findViewById(R.id.tvtitledetailnamapemilikSarana) as TextView
        var txtnamaPemilik: TextView = itemView.findViewById(R.id.tvdetailnamapemilikSarana) as TextView

        var title: TextView = itemView.findViewById(R.id.title) as TextView
        var isi: TextView = itemView.findViewById(R.id.isi) as TextView
        var title2: TextView = itemView.findViewById(R.id.title2) as TextView
        var isi2: TextView = itemView.findViewById(R.id.isi2) as TextView
    }


}