package mki.siojt2.ui.activity_daftar_objek.presenter

import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.ui.activity_daftar_objek.fasilitas_lain.DaftarObjekFasilitasLainView
import mki.siojt2.ui.activity_daftar_objek.tanaman.view.ActivityDaftarTanamanView
import mki.siojt2.utils.ErrorHandler
import mki.siojt2.utils.rxJava.RxUtils

class DaftarObjekPresenter: BasePresenter(), DaftarObjekMVPPresenter {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun getDaftarObjekTanaman(projectLandId: Int, accessToken: String) {
        viewTanaman().onShowLoading()
        compositeDisposable.add(
                dataManager.getdaftarobjekTanaman(projectLandId, accessToken)
                        .doOnTerminate { viewTanaman().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            viewTanaman().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                viewTanaman().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    viewTanaman().oncsuccessgetDaftarTanaman(authResponseDataObject.result!!)
                                } else {
                                    viewTanaman().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            viewTanaman().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(viewTanaman(), throwable)
                        }
        )
    }

    override fun getDaftarFasilitasLain(projectLandId: Int, accessToken: String) {
        viewObjekLain().onShowLoading()
        compositeDisposable.add(
                dataManager.getdaftarobjekLain(projectLandId, accessToken)
                        .doOnTerminate { viewObjekLain().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            viewObjekLain().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                viewObjekLain().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    viewObjekLain().onsuccessgetdaftarFasilitasLain(authResponseDataObject.result!!)
                                } else {
                                    viewObjekLain().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            viewObjekLain().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(viewObjekLain(), throwable)
                        }
        )
    }

    override fun getdetailobjekLain(projectLandId: Int, projectPartyId: Int, facilityTypeId : Int, accessToken: String) {
        viewObjekLain().onShowLoading()
        compositeDisposable.add(
                dataManager.getdetailobjekLain(projectLandId, projectPartyId, facilityTypeId, accessToken)
                        .doOnTerminate { viewObjekLain().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            viewObjekLain().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                viewObjekLain().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    viewObjekLain().onsuccessgetdetailFasilitasLain(authResponseDataObject.result!!)
                                } else {
                                    viewObjekLain().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            viewObjekLain().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(viewObjekLain(), throwable)
                        }
        )
    }


    private fun viewTanaman(): ActivityDaftarTanamanView{
        return getView() as ActivityDaftarTanamanView
    }

    private fun viewObjekLain(): DaftarObjekFasilitasLainView {
        return getView() as DaftarObjekFasilitasLainView
    }
}