package mki.siojt2.ui.activity_daftar_objek.gedung.presenter

import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.ui.activity_daftar_objek.gedung.view.ActivityDaftarGedungView
import mki.siojt2.utils.ErrorHandler
import mki.siojt2.utils.rxJava.RxUtils

class DaftarGedungPresenter : BasePresenter(), DaftarGedungMVPPresenter{

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun getDaftarObjekGedung(projecLandId: Int, accessToken: String) {
        view().onShowLoading()
        compositeDisposable.add(
                dataManager.getdaftarobjekGedung(projecLandId, accessToken)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().oncsuccessgetDaftarGedung(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    private fun view(): ActivityDaftarGedungView {
        return getView() as ActivityDaftarGedungView
    }
}