package mki.siojt2.ui.activity_daftar_objek

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.gson.Gson
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_daftar_objek.*
import kotlinx.android.synthetic.main.toolbar_with_sub_title.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.model.localsave.*
import mki.siojt2.ui.activity_daftar_objek.fasilitas_lain.ActivityDaftarObjekFasilitasLain
import mki.siojt2.ui.activity_daftar_objek.gedung.view.ActivityDaftarObjekGedung
import mki.siojt2.ui.activity_daftar_objek.tanaman.view.ActivityDaftarObjekTanaman
import mki.siojt2.utils.realm.RealmController

class ActivityDaftarObjek : BaseActivity() {

    private var mprojectPartyProjectId: Int? = 0
    private var mprojectLandId: Int? = 0
    private lateinit var realm: Realm

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daftar_objek)

        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        val historyData = intent?.extras?.getString("data_current_tanah")
        val toJson = Gson().fromJson(historyData, Array<String>::class.java).toList()
        //val turnsType = object : TypeToken<ResponseDataListTanah>() {}.type
        //val dataconvert = Gson().fromJson<ResponseDataListTanah>(historyData, turnsType)

        mprojectLandId = toJson[0].toInt()
        mprojectPartyProjectId = toJson[1].toInt()

        /* set toolbar*/
        if (toolbarCustom != null) {
            setSupportActionBar(toolbarCustom)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarCustom.setNavigationOnClickListener {
            finish()
        }

        tvToolbarTitle.text = toJson[2]
        tvToolbarSubTitle.text = "Daftar Objek Tanah"

        btndaftarGedung.setSafeOnClickListener {
            val intent = ActivityDaftarObjekGedung.getStartIntent(this@ActivityDaftarObjek)
            intent.putExtra("data_current_tanah", historyData)
            startActivity(intent)
        }

        btndaftarTanaman.setSafeOnClickListener {
            val intent = ActivityDaftarObjekTanaman.getStartIntent(this@ActivityDaftarObjek)
            intent.putExtra("data_current_tanah", historyData)
            startActivity(intent)
        }

        btndaftarObjekLain.setSafeOnClickListener {
            val intent = ActivityDaftarObjekFasilitasLain.getStartIntent(this@ActivityDaftarObjek)
            intent.putExtra("data_current_tanah", historyData)
            startActivity(intent)
            /*val intent = ActivityDaftarSaranaPelengkap.getStartIntent(this@ActivityDaftarObjek)
            intent.putExtra("data_current_tanah", historyData)
            startActivity(intent)*/
        }

        getDataSpinner(toJson[0], toJson[1], toJson[3])
    }

    private fun getDataSpinner(landId: String, projectId: String, partyTempId: String) {
        realm.beginTransaction()
        realm.delete(TempPemilikObjek::class.java)
        realm.commitTransaction()

        //partyAdd
        val results = realm.where(PartyAdd::class.java)
                .equalTo("TempId", partyTempId.toInt())
                .equalTo("ProjectId", projectId.toInt()).findAll()
        if (results != null) {
            for (i in 0 until results.size) {
                realm.beginTransaction()
                val tempPemilik = realm.createObject(TempPemilikObjek::class.java)
                tempPemilik.pemilikId = "P${results[i]?.tempId.toString()}"
                tempPemilik.pemilikName = results[i]?.partyFullName
                realm.commitTransaction()
            }
        }

        val results2 = realm.where(AhliWaris::class.java)
                .equalTo("LandIdTemp", landId.toInt())
                .equalTo("ProjectId", projectId.toInt()).findAll()
        if (results2 != null) {
            for (i in 0 until results2.size) {
                realm.beginTransaction()
                val tempPemilik = realm.createObject(TempPemilikObjek::class.java)
                tempPemilik.pemilikId = "A${results2[i]?.ahliWarisIdTemp.toString()}"
                tempPemilik.pemilikName = "${results2[i]?.partyFullName}"
                realm.commitTransaction()
            }
        }

        val results3 = realm.where(Subjek2::class.java)
                .equalTo("LandIdTemp", landId.toInt())
                .equalTo("ProjectId", projectId.toInt()).findAll()
        if (results3 != null) {
            for (i in 0 until results3.size) {
                realm.beginTransaction()
                val tempPemilik = realm.createObject(TempPemilikObjek::class.java)
                tempPemilik.pemilikId = "S${results3[i]?.subjekIdTemp.toString()}"
                tempPemilik.pemilikName = "${results3[i]?.subjek2FullName}"
                realm.commitTransaction()
            }
        }
    }


    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityDaftarObjek::class.java)
        }
    }
}