package mki.siojt2.ui.activity_daftar_objek.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface DaftarObjekMVPPresenter: MVPPresenter {

    fun getDaftarObjekTanaman(projectLandId: Int, accessToken: String)
    fun getDaftarFasilitasLain(projectLandId: Int, accessToken: String)

    fun getdetailobjekLain(projectLandId: Int, projectPartyId: Int, facilityTypeId : Int, accessToken: String)
}