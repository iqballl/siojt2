package mki.siojt2.ui.activity_daftar_objek.gedung.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface DaftarGedungMVPPresenter: MVPPresenter {

    fun getDaftarObjekGedung(projecLandId: Int, accessToken: String)
}