package mki.siojt2.ui.activity_daftar_objek.gedung.adapter_recycleview

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.AsyncTask
import android.os.Environment
import android.os.Handler
import androidx.recyclerview.widget.RecyclerView
import android.text.Html
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.gson.Gson
import com.itextpdf.text.*
import com.itextpdf.text.pdf.PdfPCell
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfWriter
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.dialog_confirm_export.view.*
import kotlinx.android.synthetic.main.item_daftar_objek_bangunan.view.*
import mki.siojt2.R
import mki.siojt2.base.BaseViewHolder
import mki.siojt2.model.ResponseDataListProjectLocal
import mki.siojt2.model.localsave.*
import mki.siojt2.persistence.Preference
import mki.siojt2.ui.activity_detail_objek_bangunan.ActivityDetailObjekBangunan
import mki.siojt2.ui.activity_form_add_bangunan.view.ActicityFormAddObjekBangunan
import mki.siojt2.utils.extension.SafeClickListener
import mki.siojt2.utils.realm.RealmController
import org.apache.poi.ss.usermodel.HorizontalAlignment
import org.apache.poi.ss.usermodel.IgnoredErrorType
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.VerticalAlignment
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList


class AdapterDaftarObjekGedung(context: Context, val activity: Activity) : RecyclerView.Adapter<BaseViewHolder>() {

    private val mContext: Context? = context
    val mDataset: MutableList<Bangunan>? = ArrayList()
    var rowLayout = R.layout.item_daftar_objek_bangunan
    var emptyrowLayout = R.layout.item_empty_view
    var pos = 0

    private var mCallback: Callback? = null
    private lateinit var session: Session

    //export

    private lateinit var realm: Realm

    /* excel */
    lateinit var wb: XSSFWorkbook
    lateinit var ws: XSSFSheet
    lateinit var sweetAlretLoading: SweetAlertDialog

    lateinit var rownamaProyek: Row
    /* letak tanah */
    lateinit var rowProvinsi: Row
    lateinit var rowKota: Row
    lateinit var rowKabupaten: Row
    lateinit var rowKecamatan: Row
    lateinit var rowDekul: Row
    lateinit var rowrtRw: Row
    lateinit var rowBlokNo: Row
    lateinit var rowJln: Row
    lateinit var rownoBid: Row
    lateinit var rownoPetugas: Row

    /* pemilik bangunan */
    lateinit var rownamalengkapPB: Row
    lateinit var rowttlPB: Row
    lateinit var rowpekerjaanPB: Row
    lateinit var rowalamatPB: Row
    lateinit var rownoIdentitasPB: Row
    lateinit var rowpemilikTanahPB: Row

    private var indexNoSpesifikasi = 0
    private var oldLastRow = 0
    private var currentLastRow = 33

    private var isPdfile = false
    private val dateTemplate = "dd MMMM yyyy"

    init {
        realm = RealmController.with(activity).realm
    }

    fun setCallback(callback: Callback) {
        mCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            VIEW_TYPE_NORMAL -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(rowLayout, parent, false))
            VIEW_TYPE_EMPTY -> EmptyViewHolder(
                    LayoutInflater.from(parent.context).inflate(R.layout.item_empty_view, parent, false))
            else -> EmptyViewHolder(LayoutInflater.from(parent.context).inflate(emptyrowLayout, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (mDataset != null && mDataset.isNotEmpty()) {
            VIEW_TYPE_NORMAL
        } else {
            VIEW_TYPE_EMPTY
        }
    }

    override fun getItemCount(): Int {
        return if (mDataset!!.isNotEmpty()) {
            mDataset.size
        } else {
            1
        }
    }

    /*inner class RuangViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var r_no: TextView = itemView.findViewById(R.id.rowNo) as TextView
        var r_tvTitle: TextView = itemView.findViewById(R.id.tvTitle) as TextView
        var r_tvContent1: TextView = itemView.findViewById(R.id.tvContent1) as TextView
        var r_tvContent2: TextView = itemView.findViewById(R.id.tvContent2) as TextView

    }*/

    fun clear() {
        this.mDataset!!.clear()
    }

    fun addItems(data: RealmResults<Bangunan>) {
        this.mDataset!!.addAll(data)
        notifyDataSetChanged()
    }

    interface Callback {
        fun onRepoEmptyViewRetryClick()
        fun onDismissBottomsheet()
    }

    inner class ViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }

        @SuppressLint("SetTextI18n")
        override fun onBind(position: Int) {
            super.onBind(position)

            val dataList = mDataset!![position]

            inflateData(dataList)

            session = Session(mContext)

            val pos = pos + position + 1
            if(dataList.projectBuildingTypeNameOther.isNotEmpty()){
                itemView.tvnamaSubjek.text = dataList.projectBuildingTypeNameOther
            }else{
                itemView.tvnamaSubjek.text = dataList.projectBuildingTypeName
            }

            itemView.tvnamapemilikObjek.text = "Pemilik Gedung : ${dataList.projectPartyName}"
            itemView.tvalamat1Subjek.text = "Total Luas Lantai Atas : ${Html.fromHtml("${dataList.projectBuildingAreaUpperRoom} (m<sup><small>2</small></sup>)")}"
            itemView.tvalamat2Subjek.text = "Total Luas Lantai Bawah : ${Html.fromHtml("${dataList.projectBuildingAreaLowerRoom} (m<sup><small>2</small></sup>)")}"
            itemView.tvkodeposalamatSubjek.text = "Total Lantai : ${dataList.projectBuildingFloorTotal} lantai"


            itemView.btnexportBangunan.setSafeOnClickListener {
                mCallback?.onDismissBottomsheet()
                val buildingName = if(dataList.projectBuildingTypeNameOther.isNotEmpty()){
                    dataList.projectBuildingTypeNameOther
                }else{
                    dataList.projectBuildingTypeName
                }

                try {
                    saveExcelFile("Project Name", dataList.landIdTemp, dataList.bangunanIdTemp, buildingName)
                }
                catch (ex:Exception){
                    ex.printStackTrace()
                }
            }

            itemView.btnSelngkapnya.setSafeOnClickListener {
                mCallback?.onDismissBottomsheet()
                val buildingName = if(dataList.projectBuildingTypeNameOther.isNotEmpty()){
                    dataList.projectBuildingTypeNameOther
                }else{
                    dataList.projectBuildingTypeName
                }
                val param = "['${dataList.bangunanIdTemp}','${dataList.landIdTemp}','${dataList.projectId}','${buildingName}','${dataList.projectPartyId}']"
                val intent = ActivityDetailObjekBangunan.getStartIntent(mContext!!)
                intent.putExtra("data_current_bangunan", param)
                mContext.startActivity(intent)
            }

            itemView.btnEdit.setSafeOnClickListener {
                mCallback?.onDismissBottomsheet()
                val param = "['${dataList.landIdTemp}','${dataList.projectId}','']"
                val intent = ActicityFormAddObjekBangunan.getStartIntent(mContext!!)
                val mSession = mki.siojt2.model.Session(dataList.bangunanIdTemp, dataList.landIdTemp,"2")
                intent.putExtra("data_current_tanah", param)
                intent.putExtra("data_current_session", Gson().toJson(mSession))
                session.setIdAndStatus(dataList.bangunanIdTemp.toString(), "2", "0")
                mContext.startActivity(intent)
            }

            itemView.btnDelete.setSafeOnClickListener {
                mCallback?.onDismissBottomsheet()
                val param = "['${dataList.landIdTemp}','${dataList.projectId}','']"
                val intent = ActicityFormAddObjekBangunan.getStartIntent(mContext!!)
                val mSession = mki.siojt2.model.Session(dataList.bangunanIdTemp, dataList.landIdTemp,"3")
                intent.putExtra("data_current_tanah", param)
                intent.putExtra("data_current_session", Gson().toJson(mSession))
                session.setIdAndStatus(dataList.bangunanIdTemp.toString(), "3", "")
                mContext.startActivity(intent)
            }

            /*itemView.btnDaftarObjek.visibility = View.VISIBLE
             itemView.btnDaftarObjek.setOnClickListener(object : OnSingleClickListener() {
                override fun onSingleClick(v: View) {
                    *//* val intent = ActivityDaftarTanah.getStartIntent(mContext!!)
                     intent.putExtra("data_current_pemilik", Gson().toJson(dataList))
                     mContext.startActivity(intent)*//*
                }
            })*/


        }

        private fun inflateData(dataList: Bangunan) {
            //nama?.let { itemView.tvMatapelajaran.text = it }
//            itemView.tvContent1.text = dataList.namaJenis
//            itemView.tvContent2.text= dataList.keterangan
        }
    }

    inner class EmptyViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }

    }

    private fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
        val safeClickListener = SafeClickListener {
            onSafeClick(it)
        }
        setOnClickListener(safeClickListener)
    }

    companion object {
        const val VIEW_TYPE_EMPTY = 0
        const val VIEW_TYPE_NORMAL = 1
    }

    //export

    @SuppressLint("StaticFieldLeak")
    inner class CreateFormatExcelTask(val filenameExport: String,
                                      val landId: Int?, val bangunanIdTemp: Int?,
                                      val projectName: String
    ) : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            wb = XSSFWorkbook()
            ws = wb.createSheet("Sheet1")
            ws.setColumnWidth(1, 3 * 256)
            ws.setColumnWidth(0, 3 * 256)
            ws.setColumnWidth(2, 12 * 256)
            ws.setColumnWidth(3, 25 * 256)
            ws.setColumnWidth(4, 3 * 256)
            ws.setColumnWidth(5, 3 * 256)
            ws.setColumnWidth(15, 3 * 256)
            ws.addIgnoredErrors(CellRangeAddress(0, 9999, 0, 9999), IgnoredErrorType.NUMBER_STORED_AS_TEXT)

            /* set to default */
            indexNoSpesifikasi = 0
            oldLastRow = 0
            currentLastRow = 33

            val defaultFontHeader = wb.createFont()
            defaultFontHeader.fontHeight = (16.0 * 20).toShort()
            defaultFontHeader.bold = true

            val defaultFontNo = wb.createFont()
            defaultFontNo.fontHeight = (12.0 * 20).toShort()
            defaultFontNo.bold = true

            val defaultFontTitle = wb.createFont()
            defaultFontTitle.fontHeight = (12.0 * 20).toShort()

            /* style */
            val cellStyleHeader = wb.createCellStyle()
            cellStyleHeader.wrapText = true
            cellStyleHeader.setFont(defaultFontHeader)
            // justify text alignment\
            cellStyleHeader.setAlignment(HorizontalAlignment.CENTER)

            val cellStyleHeader2 = wb.createCellStyle()
            cellStyleHeader2.wrapText = true
            cellStyleHeader2.setFont(defaultFontNo)
            // justify text alignment\
            cellStyleHeader2.setVerticalAlignment(VerticalAlignment.CENTER)

            /* style no*/
            val cellStyleNo = wb.createCellStyle()
            cellStyleNo.wrapText = true
            cellStyleNo.setFont(defaultFontNo)
            // justify text alignment\
            cellStyleNo.setVerticalAlignment(VerticalAlignment.CENTER)
            cellStyleNo.setAlignment(HorizontalAlignment.CENTER)

            /* style titik 2*/
            val cellStyleTitik2 = wb.createCellStyle()
            cellStyleTitik2.wrapText = true
            cellStyleTitik2.setFont(defaultFontTitle)
            // justify text alignment\
            cellStyleTitik2.setVerticalAlignment(VerticalAlignment.CENTER)
            cellStyleTitik2.setAlignment(HorizontalAlignment.CENTER)

            /* style normal */
            val cellStyleNoramlTitle = wb.createCellStyle()
            cellStyleNoramlTitle.wrapText = true
            cellStyleNoramlTitle.setFont(defaultFontTitle)
            // justify text alignment\\
            cellStyleNoramlTitle.setAlignment(HorizontalAlignment.LEFT)

            //header 1
            val rowheader1 = ws.createRow(2)
            val colheader1 = rowheader1.createCell(1)
            colheader1.cellStyle = cellStyleHeader
            colheader1.setCellValue("PELAKSANAAN PENGADAAN TANAH UNTUK PEMBANGUNAN")
            ws.addMergedRegion(CellRangeAddress(2, 2, 1, 14))

            //header 3
            val rowheader3 = ws.createRow(5)
            val colheader3 = rowheader3.createCell(1)
            colheader3.cellStyle = cellStyleHeader
            colheader3.setCellValue("DATA PEMILIK BANGUNAN YANG TERKENA PENGADAAN TANAH")
            ws.addMergedRegion(CellRangeAddress(5, 5, 1, 15))

            /* LETAK TANAH */
            val rowLetakTanah = ws.createRow(7)
            val colnoLetakTanah = rowLetakTanah.createCell(1)
            colnoLetakTanah.cellStyle = cellStyleNo
            colnoLetakTanah.setCellValue("I")

            val colrowtitleLetakTanah = rowLetakTanah.createCell(2)
            colrowtitleLetakTanah.cellStyle = cellStyleHeader2
            colrowtitleLetakTanah.setCellValue("LETAK TANAH")
            ws.addMergedRegion(CellRangeAddress(7, 7, 2, 3))

            //propinsi
            rowProvinsi = ws.createRow(8)
            val colProvinsi = rowProvinsi.createCell(2)
            colProvinsi.cellStyle = cellStyleNoramlTitle
            colProvinsi.setCellValue("- Provinsi")

            val colProvinsi2 = rowProvinsi.createCell(4)
            colProvinsi2.cellStyle = cellStyleTitik2
            colProvinsi2.setCellValue(":")

            //Kota
            rowKota = ws.createRow(9)
            val colKota = rowKota.createCell(2)
            colKota.cellStyle = cellStyleNoramlTitle
            colKota.setCellValue("- Kota")

            val colKota2 = rowKota.createCell(4)
            colKota2.cellStyle = cellStyleTitik2
            colKota2.setCellValue(":")

            //Kabupatan
            rowKabupaten = ws.createRow(10)
            val colKabupaten = rowKabupaten.createCell(2)
            colKabupaten.cellStyle = cellStyleNoramlTitle
            colKabupaten.setCellValue("- Kabupaten")

            val colKabupaten2 = rowKabupaten.createCell(4)
            colKabupaten2.cellStyle = cellStyleTitik2
            colKabupaten2.setCellValue(":")

            //Kecamatan
            rowKecamatan = ws.createRow(11)
            val colKecamatan = rowKecamatan.createCell(2)
            colKecamatan.cellStyle = cellStyleNoramlTitle
            colKecamatan.setCellValue("- Kecamatan")

            val colKecamatan2 = rowKecamatan.createCell(4)
            colKecamatan2.cellStyle = cellStyleTitik2
            colKecamatan2.setCellValue(":")

            //Desa/kelurahan
            rowDekul = ws.createRow(12)
            val colDekul = rowDekul.createCell(2)
            colDekul.cellStyle = cellStyleNoramlTitle
            colDekul.setCellValue("- Desa / Kelurahan")
            ws.addMergedRegion(CellRangeAddress(12, 12, 2, 3))

            val colDekul2 = rowDekul.createCell(4)
            colDekul2.cellStyle = cellStyleTitik2
            colDekul2.setCellValue(":")

            //Rt/rw
            rowrtRw = ws.createRow(13)
            val colrtRw = rowrtRw.createCell(2)
            colrtRw.cellStyle = cellStyleNoramlTitle
            colrtRw.setCellValue("- RT / RW")

            val colrtRw2 = rowrtRw.createCell(4)
            colrtRw2.cellStyle = cellStyleTitik2
            colrtRw2.setCellValue(":")

            //Blok/no
            rowBlokNo = ws.createRow(14)
            val colblokNo = rowBlokNo.createCell(2)
            colblokNo.cellStyle = cellStyleNoramlTitle
            colblokNo.setCellValue("- Blok / No")

            val colblokNo2 = rowBlokNo.createCell(4)
            colblokNo2.cellStyle = cellStyleTitik2
            colblokNo2.setCellValue(":")

            //Jalan
            rowJln = ws.createRow(15)
            val colJln = rowJln.createCell(2)
            colJln.cellStyle = cellStyleNoramlTitle
            colJln.setCellValue("- Jalan")

            val colJln2 = rowJln.createCell(4)
            colJln2.cellStyle = cellStyleTitik2
            colJln2.setCellValue(":")

            //No bidang
            rownoBid = ws.createRow(16)
            val colnoBid = rownoBid.createCell(2)
            colnoBid.cellStyle = cellStyleNoramlTitle
            colnoBid.setCellValue("- Nomor Bidang")
            ws.addMergedRegion(CellRangeAddress(16, 16, 2, 3))

            val colnoBid2 = rownoBid.createCell(4)
            colnoBid2.cellStyle = cellStyleTitik2
            colnoBid2.setCellValue(":")

            //Petugas
            rownoPetugas = ws.createRow(17)
            val colPetugas = rownoPetugas.createCell(2)
            colPetugas.cellStyle = cellStyleNoramlTitle
            colPetugas.setCellValue("- Petugas Satgas B")
            ws.addMergedRegion(CellRangeAddress(17, 17, 2, 3))

            val colPetugas2 = rownoPetugas.createCell(4)
            colPetugas2.cellStyle = cellStyleTitik2
            colPetugas2.setCellValue(":")

            /*Pemilik Bangunan*/
            val rowPemilikBangunan = ws.createRow(19)
            val colNoPB = rowPemilikBangunan.createCell(1)
            colNoPB.cellStyle = cellStyleNo
            colNoPB.setCellValue("II")

            val coltitlePB = rowPemilikBangunan.createCell(2)
            coltitlePB.cellStyle = cellStyleHeader2
            coltitlePB.setCellValue("PEMILIK BANGUNAN")
            ws.addMergedRegion(CellRangeAddress(19, 19, 2, 3))

            //Nama lenkgap
            rownamalengkapPB = ws.createRow(20)
            val coltitleNamaLengkapPB = rownamalengkapPB.createCell(2)
            coltitleNamaLengkapPB.cellStyle = cellStyleNoramlTitle
            coltitleNamaLengkapPB.setCellValue("- Nama Lengkap")
            ws.addMergedRegion(CellRangeAddress(20, 20, 2, 3))

            val coltitleNamaLengkapPB2 = rownamalengkapPB.createCell(4)
            coltitleNamaLengkapPB2.cellStyle = cellStyleTitik2
            coltitleNamaLengkapPB2.setCellValue(":")

            //Ttl
            rowttlPB = ws.createRow(21)
            val coltitlettlPB = rowttlPB.createCell(2)
            coltitlettlPB.cellStyle = cellStyleNoramlTitle
            coltitlettlPB.setCellValue("- Tempat / Tanggal Lahir")
            ws.addMergedRegion(CellRangeAddress(21, 21, 2, 3))

            val coltitlettlPB2 = rowttlPB.createCell(4)
            coltitlettlPB2.cellStyle = cellStyleTitik2
            coltitlettlPB2.setCellValue(":")

            //Pekerjaan
            rowpekerjaanPB = ws.createRow(22)
            val coltitlepekerjaanPB = rowpekerjaanPB.createCell(2)
            coltitlepekerjaanPB.cellStyle = cellStyleNoramlTitle
            coltitlepekerjaanPB.setCellValue("- Pekerjaan")

            val coltitlepekerjaanPB2 = rowpekerjaanPB.createCell(4)
            coltitlepekerjaanPB2.cellStyle = cellStyleTitik2
            coltitlepekerjaanPB2.setCellValue(":")

            //Alamat
            rowalamatPB = ws.createRow(23)
            val coltitlealamatPB = rowalamatPB.createCell(2)
            coltitlealamatPB.cellStyle = cellStyleNoramlTitle
            coltitlealamatPB.setCellValue("- Alamat")

            val coltitlealamatPB2 = rowalamatPB.createCell(4)
            coltitlealamatPB2.cellStyle = cellStyleTitik2
            coltitlealamatPB2.setCellValue(":")

            //No Identitas
            rownoIdentitasPB = ws.createRow(27)
            val coltitlenoIdentitasPB = rownoIdentitasPB.createCell(2)
            coltitlenoIdentitasPB.cellStyle = cellStyleNoramlTitle
            coltitlenoIdentitasPB.setCellValue("- NIK / No. SIM")
            ws.addMergedRegion(CellRangeAddress(27, 27, 2, 3))

            val coltitlenoIdentitasPB2 = rownoIdentitasPB.createCell(4)
            coltitlenoIdentitasPB2.cellStyle = cellStyleTitik2
            coltitlenoIdentitasPB2.setCellValue(":")

            //Pemilik tanah asli
            rowpemilikTanahPB = ws.createRow(28)
            val coltitlepemilikAsliPB = rowpemilikTanahPB.createCell(2)
            coltitlepemilikAsliPB.cellStyle = cellStyleNoramlTitle
            coltitlepemilikAsliPB.setCellValue("- Terletak di atas tanah milik")
            ws.addMergedRegion(CellRangeAddress(28, 28, 2, 3))

            val coltitlepemilikAsliPB2 = rowpemilikTanahPB.createCell(4)
            coltitlepemilikAsliPB2.cellStyle = cellStyleTitik2
            coltitlepemilikAsliPB2.setCellValue(":")

            /* title spesifikasi bangunan */
            val rowheaderSpesifikasi = ws.createRow(30)
            val colheaderSpesifikasi = rowheaderSpesifikasi.createCell(2)
            colheaderSpesifikasi.cellStyle = cellStyleHeader
            colheaderSpesifikasi.setCellValue("SPESIFIKASI BANGUNAN")
            ws.addMergedRegion(CellRangeAddress(30, 30, 2, 14))

            /* table title spesifikasi bangunan */
            val rowheadertableSpsefikasi = ws.createRow(32)
            rowheadertableSpsefikasi.height = 2 * 256
            val colnoheadertableSpsefikasi = rowheadertableSpsefikasi.createCell(2)
            colnoheadertableSpsefikasi.cellStyle = cellStyleNo
            colnoheadertableSpsefikasi.setCellValue("No.")

            val colheadertableBagianSpsefikasi = rowheadertableSpsefikasi.createCell(3)
            colheadertableBagianSpsefikasi.cellStyle = cellStyleNo
            colheadertableBagianSpsefikasi.setCellValue("Bagian Bangunan")
            ws.addMergedRegion(CellRangeAddress(32, 32, 3, 6))

            val colheadertablevolumSpsefikasi = rowheadertableSpsefikasi.createCell(7)
            colheadertablevolumSpsefikasi.cellStyle = cellStyleNo
            colheadertablevolumSpsefikasi.setCellValue("Volume (Luas)")
            ws.addMergedRegion(CellRangeAddress(32, 32, 7, 8))

            val colheadertablesatuanpsefikasi = rowheadertableSpsefikasi.createCell(9)
            colheadertablesatuanpsefikasi.cellStyle = cellStyleNo
            colheadertablesatuanpsefikasi.setCellValue("m3 (m2)")

            val colheadertableKeteranganSpefikasi = rowheadertableSpsefikasi.createCell(10)
            colheadertableKeteranganSpefikasi.cellStyle = cellStyleNo
            colheadertableKeteranganSpefikasi.setCellValue("Keterangan")
            ws.addMergedRegion(CellRangeAddress(32, 32, 10, 14))


            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            val mfilename = "$projectName.xlsx"

            val calendar = Calendar.getInstance(Locale.getDefault())
            val currentDate = DateFormat.format(dateTemplate, calendar.time)

            realm.executeTransactionAsync({ inrealm ->
                val resultsDataTanah = inrealm.where(ProjectLand::class.java).equalTo("PartyIdTemp", landId).findFirst()

                val defaultFontHeader = wb.createFont()
                defaultFontHeader.fontHeight = (16.0 * 20).toShort()
                defaultFontHeader.bold = true

                val defaultFontNo = wb.createFont()
                defaultFontNo.fontHeight = (12.0 * 20).toShort()
                defaultFontNo.bold = true

                val defaultFontTTD = wb.createFont()
                defaultFontTTD.fontHeight = (14.0 * 20).toShort()
                defaultFontTTD.bold = true

                val defaultFontTitle = wb.createFont()
                defaultFontTitle.fontHeight = (12.0 * 20).toShort()

                /* style */
                val cellStyleHeader = wb.createCellStyle()
                cellStyleHeader.wrapText = true
                cellStyleHeader.setFont(defaultFontHeader)
                // justify text alignment\
                cellStyleHeader.setAlignment(HorizontalAlignment.CENTER)

                val cellStyleHeader2 = wb.createCellStyle()
                cellStyleHeader2.wrapText = true
                cellStyleHeader2.setFont(defaultFontNo)
                // justify text alignment\
                cellStyleHeader2.setVerticalAlignment(VerticalAlignment.CENTER)

                /* style no*/
                val cellStyleNo = wb.createCellStyle()
                cellStyleNo.wrapText = true
                cellStyleNo.setFont(defaultFontNo)
                // justify text alignment\
                cellStyleNo.setVerticalAlignment(VerticalAlignment.CENTER)
                cellStyleNo.setAlignment(HorizontalAlignment.CENTER)

                /* style titik 2*/
                val cellStyleTitik2 = wb.createCellStyle()
                cellStyleTitik2.wrapText = true
                cellStyleTitik2.setFont(defaultFontNo)
                // justify text alignment\
                cellStyleTitik2.setVerticalAlignment(VerticalAlignment.CENTER)
                cellStyleTitik2.setAlignment(HorizontalAlignment.CENTER)

                /* style normal */
                val cellStyleNoramlTitle = wb.createCellStyle()
                cellStyleNoramlTitle.wrapText = true
                cellStyleNoramlTitle.setFont(defaultFontTitle)
                // justify text alignment\\
                cellStyleNoramlTitle.setAlignment(HorizontalAlignment.LEFT)

                /* style ttd */
                val cellStyleTglTtd = wb.createCellStyle()
                cellStyleTglTtd.wrapText = true
                cellStyleTglTtd.setFont(defaultFontNo)
                // justify text alignment\\
                cellStyleTglTtd.setAlignment(HorizontalAlignment.RIGHT)

                val cellStyleTtd = wb.createCellStyle()
                cellStyleTtd.wrapText = true
                cellStyleTtd.setFont(defaultFontTTD)
                // justify text alignment\\
                cellStyleTtd.setAlignment(HorizontalAlignment.CENTER)

                val cellStyleNoramlTitleCenter = wb.createCellStyle()
                cellStyleNoramlTitleCenter.wrapText = true
                cellStyleNoramlTitleCenter.setFont(defaultFontTitle)
                // justify text alignment\\
                cellStyleNoramlTitleCenter.setVerticalAlignment(VerticalAlignment.CENTER)
                cellStyleNoramlTitleCenter.setAlignment(HorizontalAlignment.CENTER)

                /* nama proyek */
                val resultsProyek = inrealm.where(ResponseDataListProjectLocal::class.java).equalTo("projectAssignProjectId", resultsDataTanah?.projectId).findFirst()
                //header 2
                rownamaProyek = ws.createRow(3)
                val colheader2 = rownamaProyek.createCell(1)

                colheader2.cellStyle = cellStyleHeader
                if (resultsProyek != null) {
                    colheader2.setCellValue(resultsProyek.getProjectName().toString().toUpperCase())
                } else {
                    colheader2.setCellValue("-")
                }
                ws.addMergedRegion(CellRangeAddress(3, 3, 1, 15))


                /* letak tanah */
                val colProvinsiLT = rowProvinsi.createCell(5)
                colProvinsiLT.cellStyle = cellStyleNoramlTitle
                colProvinsiLT.setCellValue(resultsDataTanah?.landProvinceName)
                ws.addMergedRegion(CellRangeAddress(8, 8, 5, 14))

                val colKotaLT = rowKota.createCell(5)
                colKotaLT.cellStyle = cellStyleNoramlTitle
                colKotaLT.setCellValue(resultsDataTanah?.landRegencyName)
                ws.addMergedRegion(CellRangeAddress(9, 9, 5, 14))

                val colKabupatenLT = rowKabupaten.createCell(5)
                colKabupatenLT.cellStyle = cellStyleNoramlTitle
                colKabupatenLT.setCellValue(resultsDataTanah?.landRegencyName)
                ws.addMergedRegion(CellRangeAddress(10, 10, 5, 14))

                val colKecamatanLT = rowKecamatan.createCell(5)
                colKecamatanLT.cellStyle = cellStyleNoramlTitle
                colKecamatanLT.setCellValue(resultsDataTanah?.landDistrictName)
                ws.addMergedRegion(CellRangeAddress(11, 11, 5, 14))

                val colKelurahanLT = rowDekul.createCell(5)
                colKelurahanLT.cellStyle = cellStyleNoramlTitle
                colKelurahanLT.setCellValue(resultsDataTanah?.landVillageName)
                ws.addMergedRegion(CellRangeAddress(12, 12, 5, 14))

                val colrtrwLT = rowrtRw.createCell(5)
                colrtrwLT.cellStyle = cellStyleNoramlTitle
                if (resultsDataTanah?.landRT.toString().isNotEmpty() || resultsDataTanah?.landRW.toString().isNotEmpty()) colrtrwLT.setCellValue(resultsDataTanah?.landRT + "/" + resultsDataTanah?.landRW) else colrtrwLT.setCellValue("-")
                ws.addMergedRegion(CellRangeAddress(13, 13, 5, 14))

                val colBlokLT = rowBlokNo.createCell(5)
                colBlokLT.cellStyle = cellStyleNoramlTitle
                if (!resultsDataTanah?.landBlok.isNullOrEmpty()) colBlokLT.setCellValue(resultsDataTanah?.landBlok) else colBlokLT.setCellValue("-")
                ws.addMergedRegion(CellRangeAddress(14, 14, 5, 14))

                val colJlnLT = rowJln.createCell(5)
                colJlnLT.cellStyle = cellStyleNoramlTitle
                if (!resultsDataTanah?.landStreet.isNullOrEmpty()) colJlnLT.setCellValue(resultsDataTanah?.landStreet) else colJlnLT.setCellValue("-")
                ws.addMergedRegion(CellRangeAddress(15, 15, 5, 14))

                val colNoBidLT = rownoBid.createCell(5)
                colNoBidLT.cellStyle = cellStyleNoramlTitle
                if (!resultsDataTanah?.landNumberList.isNullOrEmpty()) colNoBidLT.setCellValue(resultsDataTanah?.landNumberList) else colNoBidLT.setCellValue("-")
                ws.addMergedRegion(CellRangeAddress(16, 16, 5, 14))

                val colPetugasLT = rownoPetugas.createCell(5)
                colPetugasLT.cellStyle = cellStyleNoramlTitle
                colPetugasLT.setCellValue(Preference.auth.dataUser?.mobileUserEmployeeName)
                ws.addMergedRegion(CellRangeAddress(17, 17, 5, 14))


                /* spsesfikasi bangunan*/
                val resultsDataBangunan = inrealm.where(Bangunan::class.java)
                    .equalTo("LandIdTemp", landId)
                    .and()
                    .equalTo("BangunanIdTemp", bangunanIdTemp)
                    .findFirst()

                /* pemilik bangunan */
                val idPemilikBangunan = resultsDataBangunan?.projectPartyId
                val firstCharacterOfIdPemilkBangunan = idPemilikBangunan?.get(0)
                val lastCharacterOfIdPemilkBangunan = idPemilikBangunan?.substring(idPemilikBangunan.indexOf("$firstCharacterOfIdPemilkBangunan") + 1)

                when {
                    firstCharacterOfIdPemilkBangunan.toString() == "P" -> {
                        val resultDataPemilikPertama = inrealm.where(PartyAdd::class.java)
                            .equalTo("TempId", lastCharacterOfIdPemilkBangunan?.toInt())
                            .findFirst()

                        val colNamaLengkap = rownamalengkapPB.createCell(5)
                        colNamaLengkap.cellStyle = cellStyleNoramlTitle
                        colNamaLengkap.setCellValue(resultDataPemilikPertama?.partyFullName)
                        ws.addMergedRegion(CellRangeAddress(20, 20, 5, 14))

                        /* ttl*/
                        val colTTLPB = rowttlPB.createCell(5)
                        colTTLPB.cellStyle = cellStyleNoramlTitle

                        val tmptLahirPB: String?
                        val tglLahirPB: String?
                        tmptLahirPB = if (!resultDataPemilikPertama?.partyBirthPlaceName.isNullOrEmpty()) resultDataPemilikPertama?.partyBirthPlaceName else "-"
                        tglLahirPB = if (!resultDataPemilikPertama?.partyBirthDate.isNullOrEmpty()) resultDataPemilikPertama?.partyBirthDate else "-"

                        colTTLPB.setCellValue("$tmptLahirPB / $tglLahirPB")
                        ws.addMergedRegion(CellRangeAddress(21, 21, 5, 14))

                        /* pekerjaan */
                        val colPekerjaanPB = rowpekerjaanPB.createCell(5)
                        colPekerjaanPB.cellStyle = cellStyleNoramlTitle
                        if (!resultDataPemilikPertama?.partyOccupationName.isNullOrEmpty()) colPekerjaanPB.setCellValue(resultDataPemilikPertama?.partyOccupationName) else colPekerjaanPB.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(22, 22, 5, 14))

                        /* alamat */
                        val colAlamatJalan = rowalamatPB.createCell(5)
                        colAlamatJalan.cellStyle = cellStyleNoramlTitle
                        if (!resultDataPemilikPertama?.partyDistrictName.isNullOrEmpty()) colAlamatJalan.setCellValue(resultDataPemilikPertama?.partyDistrictName) else colAlamatJalan.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(23, 23, 5, 8))

                        val colAlamatBlok = rowalamatPB.createCell(11)
                        colAlamatBlok.cellStyle = cellStyleNoramlTitle
                        if (!resultDataPemilikPertama?.partyBlock.isNullOrEmpty()) colAlamatBlok.setCellValue(resultDataPemilikPertama?.partyBlock) else colAlamatBlok.setCellValue("-")

                        val colAlamatRTRW = rowalamatPB.createCell(13)
                        colAlamatRTRW.cellStyle = cellStyleNoramlTitle
                        val noRTPB: String?
                        val noRWPB: String?

                        noRTPB = if (!resultDataPemilikPertama?.partyRT.isNullOrEmpty()) resultDataPemilikPertama?.partyRT else "-"
                        noRWPB = if (!resultDataPemilikPertama?.partyRW.isNullOrEmpty()) resultDataPemilikPertama?.partyRW else "-"
                        colAlamatRTRW.setCellValue("$noRTPB / $noRWPB")

                        val rowAlamatDesa = ws.createRow(24)
                        val colAlamatDesa = rowAlamatDesa.createCell(5)
                        colAlamatDesa.cellStyle = cellStyleNoramlTitle
                        colAlamatDesa.setCellValue(resultDataPemilikPertama?.partyVillageName)
                        ws.addMergedRegion(CellRangeAddress(24, 24, 5, 8))

                        val rowAlamatKecamatan = ws.createRow(25)
                        val colAlamatKecamatan = rowAlamatKecamatan.createCell(5)
                        colAlamatKecamatan.cellStyle = cellStyleNoramlTitle
                        colAlamatKecamatan.setCellValue(resultDataPemilikPertama?.partyDistrictName)
                        ws.addMergedRegion(CellRangeAddress(25, 25, 5, 8))

                        val rowAlamatKota = ws.createRow(26)
                        val colAlamatKota = rowAlamatKota.createCell(5)
                        colAlamatKota.cellStyle = cellStyleNoramlTitle
                        colAlamatKota.setCellValue(resultDataPemilikPertama?.partyRegencyName)
                        ws.addMergedRegion(CellRangeAddress(26, 26, 5, 8))

                        val colAlamatProvinsi = rowAlamatKota.createCell(11)
                        colAlamatProvinsi.cellStyle = cellStyleNoramlTitle
                        colAlamatProvinsi.setCellValue(resultDataPemilikPertama?.partyProvinceName)
                        ws.addMergedRegion(CellRangeAddress(26, 26, 11, 13))


                        /* identitas pemilik */
                        val colIdentitasPemilik = rownoIdentitasPB.createCell(5)
                        colIdentitasPemilik.cellStyle = cellStyleNoramlTitle
                        if (!resultDataPemilikPertama?.partyIdentityNumber.isNullOrEmpty()) colIdentitasPemilik.setCellValue(resultDataPemilikPertama?.partyIdentityNumber) else colIdentitasPemilik.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(27, 27, 5, 8))


                    }
                    firstCharacterOfIdPemilkBangunan.toString() == "S" -> {
                        val resultDataPemilikKedua = inrealm.where(Subjek2::class.java)
                            .equalTo("SubjekIdTemp", lastCharacterOfIdPemilkBangunan?.toInt())
                            .findFirst()

                        /* nama lengkap */
                        val colNamaLengkap = rownamalengkapPB.createCell(5)
                        colNamaLengkap.cellStyle = cellStyleNoramlTitle
                        colNamaLengkap.setCellValue(resultDataPemilikKedua?.subjek2FullName)
                        ws.addMergedRegion(CellRangeAddress(20, 20, 5, 14))

                        /* ttl*/
                        val colTTLPB = rowttlPB.createCell(5)
                        colTTLPB.cellStyle = cellStyleNoramlTitle

                        val tmptLahirPB: String?
                        val tglLahirPB: String?
                        tmptLahirPB = if (!resultDataPemilikKedua?.subjek2BirthPlaceName.isNullOrEmpty()) resultDataPemilikKedua?.subjek2BirthPlaceName else "-"
                        tglLahirPB = if (!resultDataPemilikKedua?.subjek2BirthDate.isNullOrEmpty()) resultDataPemilikKedua?.subjek2BirthDate else "-"

                        colTTLPB.setCellValue("$tmptLahirPB / $tglLahirPB")
                        ws.addMergedRegion(CellRangeAddress(21, 21, 5, 14))

                        /* pekerjaan */
                        val colPekerjaanPB = rowpekerjaanPB.createCell(5)
                        colPekerjaanPB.cellStyle = cellStyleNoramlTitle
                        if (!resultDataPemilikKedua?.subjek2OccupationName.isNullOrEmpty()) colPekerjaanPB.setCellValue(resultDataPemilikKedua?.subjek2OccupationName) else colPekerjaanPB.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(22, 22, 5, 14))

                        /* alamat */
                        val colAlamatJalan = rowalamatPB.createCell(5)
                        colAlamatJalan.cellStyle = cellStyleNoramlTitle
                        if (!resultDataPemilikKedua?.subjek2DistrictName.isNullOrEmpty()) colAlamatJalan.setCellValue(resultDataPemilikKedua?.subjek2DistrictName) else colAlamatJalan.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(23, 23, 5, 8))

                        val colAlamatBlok = rowalamatPB.createCell(11)
                        colAlamatBlok.cellStyle = cellStyleNoramlTitle
                        if (!resultDataPemilikKedua?.subjek2Block.isNullOrEmpty()) colAlamatBlok.setCellValue(resultDataPemilikKedua?.subjek2Block) else colAlamatBlok.setCellValue("-")

                        val colAlamatRTRW = rowalamatPB.createCell(13)
                        colAlamatRTRW.cellStyle = cellStyleNoramlTitle
                        val noRTPB: String?
                        val noRWPB: String?

                        noRTPB = if (!resultDataPemilikKedua?.subjek2RT.isNullOrEmpty()) resultDataPemilikKedua?.subjek2RT else "-"
                        noRWPB = if (!resultDataPemilikKedua?.subjek2RW.isNullOrEmpty()) resultDataPemilikKedua?.subjek2RW else "-"
                        colAlamatRTRW.setCellValue("$noRTPB / $noRWPB")

                        val rowAlamatDesa = ws.createRow(24)
                        val colAlamatDesa = rowAlamatDesa.createCell(5)
                        colAlamatDesa.cellStyle = cellStyleNoramlTitle
                        colAlamatDesa.setCellValue(resultDataPemilikKedua?.subjek2VillageName)
                        ws.addMergedRegion(CellRangeAddress(24, 24, 5, 8))

                        val rowAlamatKecamatan = ws.createRow(25)
                        val colAlamatKecamatan = rowAlamatKecamatan.createCell(5)
                        colAlamatKecamatan.cellStyle = cellStyleNoramlTitle
                        colAlamatKecamatan.setCellValue(resultDataPemilikKedua?.subjek2DistrictName)
                        ws.addMergedRegion(CellRangeAddress(25, 25, 5, 8))

                        val rowAlamatKota = ws.createRow(26)
                        val colAlamatKota = rowAlamatKota.createCell(5)
                        colAlamatKota.cellStyle = cellStyleNoramlTitle
                        colAlamatKota.setCellValue(resultDataPemilikKedua?.subjek2RegencyName)
                        ws.addMergedRegion(CellRangeAddress(26, 26, 5, 8))

                        val colAlamatProvinsi = rowAlamatKota.createCell(11)
                        colAlamatProvinsi.cellStyle = cellStyleNoramlTitle
                        colAlamatProvinsi.setCellValue(resultDataPemilikKedua?.subjek2ProvinceName)
                        ws.addMergedRegion(CellRangeAddress(26, 26, 11, 13))


                        /* identitas pemilik */
                        val colIdentitasPemilik = rownoIdentitasPB.createCell(5)
                        colIdentitasPemilik.cellStyle = cellStyleNoramlTitle
                        if (!resultDataPemilikKedua?.subjek2IdentityNumber.isNullOrEmpty()) colIdentitasPemilik.setCellValue(resultDataPemilikKedua?.subjek2IdentityNumber) else colIdentitasPemilik.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(27, 27, 5, 8))


                    }
                    else -> {
                        val resultDataAhliWaris = inrealm.where(AhliWaris::class.java)
                            .equalTo("AhliWarisIdTemp", lastCharacterOfIdPemilkBangunan?.toInt())
                            .findFirst()

                        val colNamaLengkap = rownamalengkapPB.createCell(5)
                        colNamaLengkap.cellStyle = cellStyleNoramlTitle
                        colNamaLengkap.setCellValue(resultDataAhliWaris?.partyFullName)
                        ws.addMergedRegion(CellRangeAddress(20, 20, 5, 14))

                        /* ttl*/
                        val colTTLPB = rowttlPB.createCell(5)
                        colTTLPB.cellStyle = cellStyleNoramlTitle

                        val tmptLahirPB: String?
                        val tglLahirPB: String?
                        tmptLahirPB = if (!resultDataAhliWaris?.partyBirthPlaceName.isNullOrEmpty()) resultDataAhliWaris?.partyBirthPlaceName else "-"
                        tglLahirPB = if (!resultDataAhliWaris?.partyBirthDate.isNullOrEmpty()) resultDataAhliWaris?.partyBirthDate else "-"

                        colTTLPB.setCellValue("$tmptLahirPB / $tglLahirPB")
                        ws.addMergedRegion(CellRangeAddress(21, 21, 5, 14))

                        /* pekerjaan */
                        val colPekerjaanPB = rowpekerjaanPB.createCell(5)
                        colPekerjaanPB.cellStyle = cellStyleNoramlTitle
                        if (!resultDataAhliWaris?.partyOccupationName.isNullOrEmpty()) colPekerjaanPB.setCellValue(resultDataAhliWaris?.partyOccupationName) else colPekerjaanPB.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(22, 22, 5, 14))

                        /* alamat */
                        val colAlamatJalan = rowalamatPB.createCell(5)
                        colAlamatJalan.cellStyle = cellStyleNoramlTitle
                        if (!resultDataAhliWaris?.partyDistrictName.isNullOrEmpty()) colAlamatJalan.setCellValue(resultDataAhliWaris?.partyDistrictName) else colAlamatJalan.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(23, 23, 5, 8))

                        val colAlamatBlok = rowalamatPB.createCell(11)
                        colAlamatBlok.cellStyle = cellStyleNoramlTitle
                        if (!resultDataAhliWaris?.partyBlock.isNullOrEmpty()) colAlamatBlok.setCellValue(resultDataAhliWaris?.partyBlock) else colAlamatBlok.setCellValue("-")

                        val colAlamatRTRW = rowalamatPB.createCell(13)
                        colAlamatRTRW.cellStyle = cellStyleNoramlTitle
                        val noRTPB: String?
                        val noRWPB: String?

                        noRTPB = if (!resultDataAhliWaris?.partyRT.isNullOrEmpty()) resultDataAhliWaris?.partyRT else "-"
                        noRWPB = if (!resultDataAhliWaris?.partyRW.isNullOrEmpty()) resultDataAhliWaris?.partyRW else "-"
                        colAlamatRTRW.setCellValue("$noRTPB / $noRWPB")

                        val rowAlamatDesa = ws.createRow(24)
                        val colAlamatDesa = rowAlamatDesa.createCell(5)
                        colAlamatDesa.cellStyle = cellStyleNoramlTitle
                        colAlamatDesa.setCellValue(resultDataAhliWaris?.partyVillageName)
                        ws.addMergedRegion(CellRangeAddress(24, 24, 5, 8))

                        val rowAlamatKecamatan = ws.createRow(25)
                        val colAlamatKecamatan = rowAlamatKecamatan.createCell(5)
                        colAlamatKecamatan.cellStyle = cellStyleNoramlTitle
                        colAlamatKecamatan.setCellValue(resultDataAhliWaris?.partyDistrictName)
                        ws.addMergedRegion(CellRangeAddress(25, 25, 5, 8))

                        val rowAlamatKota = ws.createRow(26)
                        val colAlamatKota = rowAlamatKota.createCell(5)
                        colAlamatKota.cellStyle = cellStyleNoramlTitle
                        colAlamatKota.setCellValue(resultDataAhliWaris?.partyRegencyName)
                        ws.addMergedRegion(CellRangeAddress(26, 26, 5, 8))

                        val colAlamatProvinsi = rowAlamatKota.createCell(11)
                        colAlamatProvinsi.cellStyle = cellStyleNoramlTitle
                        colAlamatProvinsi.setCellValue(resultDataAhliWaris?.partyProvinceName)
                        ws.addMergedRegion(CellRangeAddress(26, 26, 11, 13))

                        /* identitas pemilik */
                        val colIdentitasPemilik = rownoIdentitasPB.createCell(5)
                        colIdentitasPemilik.cellStyle = cellStyleNoramlTitle
                        if (!resultDataAhliWaris?.partyIdentityNumber.isNullOrEmpty()) colIdentitasPemilik.setCellValue(resultDataAhliWaris?.partyIdentityNumber) else colIdentitasPemilik.setCellValue("-")
                        ws.addMergedRegion(CellRangeAddress(27, 27, 5, 8))

                    }

                }

                val convertPondasiBangunanId = resultsDataBangunan?.foundationTypeId
                val convertPondasiBangunanId2 = convertPondasiBangunanId?.replace("[", "['")
                val convertPondasiBangunanId3 = convertPondasiBangunanId2?.replace("]", "']")
                val convertPondasiBangunanId4 = convertPondasiBangunanId3?.replace(", ", "','")

                val convertPondasiBangunanName = resultsDataBangunan?.foundationTypeName
                val convertPondasiBangunanName2 = convertPondasiBangunanName?.replace("[", "['")
                val convertPondasiBangunanName3 = convertPondasiBangunanName2?.replace("]", "']")
                val convertPondasiBangunanName4 = convertPondasiBangunanName3?.replace(", ", "','")

                val convertPondasiBangunanNameOther = resultsDataBangunan?.foundationTypeNameOther
                val convertPondasiBangunanNameOther2 = convertPondasiBangunanNameOther?.replace("[", "['")
                val convertPondasiBangunanNameOther3 = convertPondasiBangunanNameOther2?.replace("]", "']")
                val convertPondasiBangunanNameOther4 = convertPondasiBangunanNameOther3?.replace(", ", "','")

                val convertStrukturPondasiVolume = resultsDataBangunan?.pbFoundationTypeAreaTotal
                val convertStrukturPondasiVolume2 = convertStrukturPondasiVolume?.replace("[", "['")
                val convertStrukturPondasiVolume3 = convertStrukturPondasiVolume2?.replace("]", "']")
                val convertStrukturPondasiVolume4 = convertStrukturPondasiVolume3?.replace(", ", "','")

                val dataPondasiId = Gson().fromJson(convertPondasiBangunanId4, Array<String>::class.java).toList()
                val dataPondasiName = Gson().fromJson(convertPondasiBangunanName4, Array<String>::class.java).toList()
                val dataPondasiNameOther = Gson().fromJson(convertPondasiBangunanNameOther4, Array<String>::class.java).toList()
                val dataPondasiVolume = Gson().fromJson(convertStrukturPondasiVolume4, Array<String>::class.java).toList()

                /*struktur*/
                val convertStrukturBangunanId = resultsDataBangunan?.structureTypeId
                val convertStrukturBangunanId2 = convertStrukturBangunanId?.replace("[", "['")
                val convertStrukturBangunanId3 = convertStrukturBangunanId2?.replace("]", "']")
                val convertStrukturBangunanId4 = convertStrukturBangunanId3?.replace(", ", "','")

                val convertStrukturBangunanName = resultsDataBangunan?.structureTypeName
                val convertStrukturBangunanName2 = convertStrukturBangunanName?.replace("[", "['")
                val convertStrukturBangunanName3 = convertStrukturBangunanName2?.replace("]", "']")
                val convertStrukturBangunanName4 = convertStrukturBangunanName3?.replace(", ", "','")

                val convertStrukturBangunanNameOther = resultsDataBangunan?.structureTypeNameOther
                val convertStrukturBangunanNameOther2 = convertStrukturBangunanNameOther?.replace("[", "['")
                val convertStrukturBangunanNameOther3 = convertStrukturBangunanNameOther2?.replace("]", "']")
                val convertStrukturBangunanNameOther4 = convertStrukturBangunanNameOther3?.replace(", ", "','")

                val convertStrukturBangunanVolume = resultsDataBangunan?.pbStructureTypeAreaTotal
                val convertStrukturBangunanVolume2 = convertStrukturBangunanVolume?.replace("[", "['")
                val convertStrukturBangunanVolume3 = convertStrukturBangunanVolume2?.replace("]", "']")
                val convertStrukturBangunanVolume4 = convertStrukturBangunanVolume3?.replace(", ", "','")

                val dataStrukturId = Gson().fromJson(convertStrukturBangunanId4, Array<String>::class.java).toList()
                val dataStrukturName = Gson().fromJson(convertStrukturBangunanName4, Array<String>::class.java).toList()
                val dataStrukturNameOther = Gson().fromJson(convertStrukturBangunanNameOther4, Array<String>::class.java).toList()
                val dataStrukturVolume = Gson().fromJson(convertStrukturBangunanVolume4, Array<String>::class.java).toList()

                /*kerangka atap */
                val convertKeratapBangunanId = resultsDataBangunan?.roofFrameTypeId
                val convertKeratapBangunanId2 = convertKeratapBangunanId?.replace("[", "['")
                val convertKeratapBangunanId3 = convertKeratapBangunanId2?.replace("]", "']")
                val convertKeratapBangunanId4 = convertKeratapBangunanId3?.replace(", ", "','")

                val convertKeratapBangunanName = resultsDataBangunan?.roofFrameTypeName
                val convertKeratapBangunanName2 = convertKeratapBangunanName?.replace("[", "['")
                val convertKeratapBangunanName3 = convertKeratapBangunanName2?.replace("]", "']")
                val convertKeratapBangunanName4 = convertKeratapBangunanName3?.replace(", ", "','")

                val convertKeratapBangunanNameOther = resultsDataBangunan?.roofFrameTypeNameOther
                val convertKeratapBangunanNameOther2 = convertKeratapBangunanNameOther?.replace("[", "['")
                val convertKeratapBangunanNameOther3 = convertKeratapBangunanNameOther2?.replace("]", "']")
                val convertKeratapBangunanNameOther4 = convertKeratapBangunanNameOther3?.replace(", ", "','")

                val convertKeratapBangunanVolume = resultsDataBangunan?.pbRoofFrameTypeAreaTotal
                val convertKeratapBangunanVolume2 = convertKeratapBangunanVolume?.replace("[", "['")
                val convertKeratapBangunanVolume3 = convertKeratapBangunanVolume2?.replace("]", "']")
                val convertKeratapBangunanVolume4 = convertKeratapBangunanVolume3?.replace(", ", "','")

                val dataKeratapId = Gson().fromJson(convertKeratapBangunanId4, Array<String>::class.java).toList()
                val dataKeratapName = Gson().fromJson(convertKeratapBangunanName4, Array<String>::class.java).toList()
                val dataKeratapNameOther = Gson().fromJson(convertKeratapBangunanNameOther4, Array<String>::class.java).toList()
                val dataKeratapVolume = Gson().fromJson(convertKeratapBangunanVolume4, Array<String>::class.java).toList()

                /*penutup atap */
                val convertPenutapBangunanId = resultsDataBangunan?.roofCoveringTypeId
                val convertPenutapBangunanId2 = convertPenutapBangunanId?.replace("[", "['")
                val convertPenutapBangunanId3 = convertPenutapBangunanId2?.replace("]", "']")
                val convertPenutapBangunanId4 = convertPenutapBangunanId3?.replace(", ", "','")

                val convertPenutapBangunanName = resultsDataBangunan?.roofCoveringTypeName
                val convertPenutapBangunanNam2 = convertPenutapBangunanName?.replace("[", "['")
                val convertPenutapBangunanNam3 = convertPenutapBangunanNam2?.replace("]", "']")
                val convertPenutapBangunanNam4 = convertPenutapBangunanNam3?.replace(", ", "','")

                val convertPenutapBangunanNameOther = resultsDataBangunan?.roofCoveringTypeNameOther
                val convertPenutapBangunanNameOther2 = convertPenutapBangunanNameOther?.replace("[", "['")
                val convertPenutapBangunanNameOther3 = convertPenutapBangunanNameOther2?.replace("]", "']")
                val convertPenutapBangunanNameOther4 = convertPenutapBangunanNameOther3?.replace(", ", "','")

                val convertPenutapBangunanVolume = resultsDataBangunan?.pbRoofCoveringTypeAreaTotal
                val convertPenutapBangunanVolum2 = convertPenutapBangunanVolume?.replace("[", "['")
                val convertPenutapBangunanVolum3 = convertPenutapBangunanVolum2?.replace("]", "']")
                val convertPenutapBangunanVolum4 = convertPenutapBangunanVolum3?.replace(", ", "','")

                val dataPenutapId = Gson().fromJson(convertPenutapBangunanId4, Array<String>::class.java).toList()
                val dataPenutapName = Gson().fromJson(convertPenutapBangunanNam4, Array<String>::class.java).toList()
                val dataPenutapNameOther = Gson().fromJson(convertPenutapBangunanNameOther4, Array<String>::class.java).toList()
                val dataPenutapVolume = Gson().fromJson(convertPenutapBangunanVolum4, Array<String>::class.java).toList()

                /* plafon */
                val convertPlafonBangunanId = resultsDataBangunan?.ceilingTypeId
                val convertPlafonBangunanId2 = convertPlafonBangunanId?.replace("[", "['")
                val convertPlafonBangunanId3 = convertPlafonBangunanId2?.replace("]", "']")
                val convertPlafonBangunanId4 = convertPlafonBangunanId3?.replace(", ", "','")

                val convertPlafonBangunanName = resultsDataBangunan?.ceilingTypeName
                val convertPlafonBangunanName2 = convertPlafonBangunanName?.replace("[", "['")
                val convertPlafonBangunanName3 = convertPlafonBangunanName2?.replace("]", "']")
                val convertPlafonBangunanName4 = convertPlafonBangunanName3?.replace(", ", "','")

                val convertPlafonBangunanNameOther = resultsDataBangunan?.ceilingTypeNameOther
                val convertPlafonBangunanNameOther3 = convertPlafonBangunanNameOther?.replace("[", "['")
                val convertPlafonBangunanNameOther4 = convertPlafonBangunanNameOther3?.replace("]", "']")
                val convertPlafonBangunanNameOther5 = convertPlafonBangunanNameOther4?.replace(", ", "','")

                val convertPlafonBangunanVolume = resultsDataBangunan?.pbCeilingTypeAreaTotal
                val convertPlafonBangunanVolume2 = convertPlafonBangunanVolume?.replace("[", "['")
                val convertPlafonBangunanVolume3 = convertPlafonBangunanVolume2?.replace("]", "']")
                val convertPlafonBangunanVolume4 = convertPlafonBangunanVolume3?.replace(", ", "','")

                val dataPlafonId = Gson().fromJson(convertPlafonBangunanId4, Array<String>::class.java).toList()
                val dataPlafonName = Gson().fromJson(convertPlafonBangunanName4, Array<String>::class.java).toList()
                val dataPlafonNameOther = Gson().fromJson(convertPlafonBangunanNameOther5, Array<String>::class.java).toList()
                val dataPlafonVolume = Gson().fromJson(convertPlafonBangunanVolume4, Array<String>::class.java).toList()

                /* Dinding */
                val convertDindingBangunanId = resultsDataBangunan?.wallTypeId
                val convertDindingBangunanId2 = convertDindingBangunanId?.replace("[", "['")
                val convertDindingBangunanId3 = convertDindingBangunanId2?.replace("]", "']")
                val convertDindingBangunanId4 = convertDindingBangunanId3?.replace(", ", "','")

                val convertDindingBangunanName = resultsDataBangunan?.wallTypeName
                val convertDindingBangunanName2 = convertDindingBangunanName?.replace("[", "['")
                val convertDindingBangunanName3 = convertDindingBangunanName2?.replace("]", "']")
                val convertDindingBangunanName4 = convertDindingBangunanName3?.replace(", ", "','")

                val convertDindingBangunanNameOther = resultsDataBangunan?.wallTypeNameOther
                val convertDindingBangunanNameOther2 = convertDindingBangunanNameOther?.replace("[", "['")
                val convertDindingBangunanNameOther3 = convertDindingBangunanNameOther2?.replace("]", "']")
                val convertDindingBangunanNameOther4 = convertDindingBangunanNameOther3?.replace(", ", "','")

                val convertDindingBangunanVolume = resultsDataBangunan?.pbfWallTypeAreaTotal
                val convertDindingBangunanVolume2 = convertDindingBangunanVolume?.replace("[", "['")
                val convertDindingBangunanVolume3 = convertDindingBangunanVolume2?.replace("]", "']")
                val convertDindingBangunanVolume4 = convertDindingBangunanVolume3?.replace(", ", "','")

                val dataDindingId = Gson().fromJson(convertDindingBangunanId4, Array<String>::class.java).toList()
                val dataDindingName = Gson().fromJson(convertDindingBangunanName4, Array<String>::class.java).toList()
                val dataDindingNameOther = Gson().fromJson(convertDindingBangunanNameOther4, Array<String>::class.java).toList()
                val dataDindingVolume = Gson().fromJson(convertDindingBangunanVolume4, Array<String>::class.java).toList()

                /* Pintu jendela */
                val convertPinjelBangunanId = resultsDataBangunan?.doorWindowTypeld
                val convertPinjelBangunanId2 = convertPinjelBangunanId?.replace("[", "['")
                val convertPinjelBangunanId3 = convertPinjelBangunanId2?.replace("]", "']")
                val convertPinjelBangunanId4 = convertPinjelBangunanId3?.replace(", ", "','")

                val convertPinjelBangunanName = resultsDataBangunan?.doorWindowTypeName
                val convertPinjelBangunanName2 = convertPinjelBangunanName?.replace("[", "['")
                val convertPinjelBangunanName3 = convertPinjelBangunanName2?.replace("]", "']")
                val convertPinjelBangunanName4 = convertPinjelBangunanName3?.replace(", ", "','")

                val convertPinjelBangunanNameOther = resultsDataBangunan?.doorWindowTypeNameOther
                val convertPinjelBangunanNameOther2 = convertPinjelBangunanNameOther?.replace("[", "['")
                val convertPinjelBangunanNameOther3 = convertPinjelBangunanNameOther2?.replace("]", "']")
                val convertPinjelBangunanNameOther4 = convertPinjelBangunanNameOther3?.replace(", ", "','")

                val convertPinjelBangunanVolume = resultsDataBangunan?.pbDoorWindowTypeAreaTotal
                val convertPinjelBangunanVolume2 = convertPinjelBangunanVolume?.replace("[", "['")
                val convertPinjelBangunanVolume3 = convertPinjelBangunanVolume2?.replace("]", "']")
                val convertPinjelBangunanVolume4 = convertPinjelBangunanVolume3?.replace(", ", "','")

                val dataPinjelId = Gson().fromJson(convertPinjelBangunanId4, Array<String>::class.java).toList()
                val dataPinjelName = Gson().fromJson(convertPinjelBangunanName4, Array<String>::class.java).toList()
                val dataPinjelNameOther = Gson().fromJson(convertPinjelBangunanNameOther4, Array<String>::class.java).toList()
                val dataPinjelVolume = Gson().fromJson(convertPinjelBangunanVolume4, Array<String>::class.java).toList()

                /* Lantai */
                val convertLantaiBangunanId = resultsDataBangunan?.floorTypeId
                val convertLantaiBangunanId2 = convertLantaiBangunanId?.replace("[", "['")
                val convertLantaiBangunanId3 = convertLantaiBangunanId2?.replace("]", "']")
                val convertLantaiBangunanId4 = convertLantaiBangunanId3?.replace(", ", "','")

                val convertLantaiBangunanName = resultsDataBangunan?.floorTypeName
                val convertLantaiBangunanName2 = convertLantaiBangunanName?.replace("[", "['")
                val convertLantaiBangunanName3 = convertLantaiBangunanName2?.replace("]", "']")
                val convertLantaiBangunanName4 = convertLantaiBangunanName3?.replace(", ", "','")

                val convertLantaiBangunanNameOther = resultsDataBangunan?.floorTypeNameOther
                val convertLantaiBangunanNameOther2 = convertLantaiBangunanNameOther?.replace("[", "['")
                val convertLantaiBangunanNameOther3 = convertLantaiBangunanNameOther2?.replace("]", "']")
                val convertLantaiBangunanNameOther4 = convertLantaiBangunanNameOther3?.replace(", ", "','")

                val convertLantaiBangunanVolume = resultsDataBangunan?.pbFloorTypeAreaTotal
                val convertLantaiBangunanVolume2 = convertLantaiBangunanVolume?.replace("[", "['")
                val convertLantaiBangunanVolume3 = convertLantaiBangunanVolume2?.replace("]", "']")
                val convertLantaiBangunanVolume4 = convertLantaiBangunanVolume3?.replace(", ", "','")

                val dataLantaiId = Gson().fromJson(convertLantaiBangunanId4, Array<String>::class.java).toList()
                val dataLantaiName = Gson().fromJson(convertLantaiBangunanName4, Array<String>::class.java).toList()
                val dataLantaiNameOther = Gson().fromJson(convertLantaiBangunanNameOther4, Array<String>::class.java).toList()
                val dataLantaiVolume = Gson().fromJson(convertLantaiBangunanVolume4, Array<String>::class.java).toList()

                //pondasi
                if (dataPondasiId.isNotEmpty()) {
                    for (element in dataPondasiId.indices) {
                        indexNoSpesifikasi++
                        createRow(currentLastRow)
                        val rowPondasi = ws.getRow(oldLastRow)
                        rowPondasi.height = 2 * 256

                        val colPondasiNo = rowPondasi.createCell(2)
                        colPondasiNo.cellStyle = cellStyleNoramlTitleCenter
                        colPondasiNo.setCellValue("$indexNoSpesifikasi")

                        val colPondasiName = rowPondasi.createCell(3)
                        colPondasiName.let {
                            it.cellStyle = cellStyleNoramlTitleCenter
                            if (dataPondasiNameOther[element].isNotEmpty()) {
                                it.setCellValue(dataPondasiNameOther[element])
                            } else {
                                it.setCellValue(dataPondasiName[element])
                            }
                        }
                        //colPondasiName.cellStyle = cellStyleNoramlTitleCenter
                        //colPondasiName.setCellValue(dataPondasiName[element])
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 3, 6))

                        val colPondasiVolume = rowPondasi.createCell(7)
                        colPondasiVolume.cellStyle = cellStyleNoramlTitleCenter
                        colPondasiVolume.setCellValue(dataPondasiVolume[element])
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 7, 8))

                        val colPondasiKeterangan = rowPondasi.createCell(10)
                        colPondasiKeterangan.cellStyle = cellStyleNoramlTitleCenter
                        colPondasiKeterangan.setCellValue("Pondasi Bangunan")
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 10, 14))
                    }
                }

                //struktur
                if (dataStrukturId.isNotEmpty()) {
                    for (element in dataStrukturId.indices) {
                        indexNoSpesifikasi++
                        createRow(currentLastRow)
                        val rowStruktur = ws.getRow(oldLastRow)
                        rowStruktur.height = 2 * 256

                        val colStrukturNo = rowStruktur.createCell(2)
                        colStrukturNo.cellStyle = cellStyleNoramlTitleCenter
                        colStrukturNo.setCellValue("$indexNoSpesifikasi")

                        val colStrukturName = rowStruktur.createCell(3)
                        colStrukturName.let {
                            it.cellStyle = cellStyleNoramlTitleCenter
                            if (dataStrukturNameOther[element].isNotEmpty()) {
                                it.setCellValue(dataStrukturNameOther[element])
                            } else {
                                it.setCellValue(dataStrukturName[element])
                            }
                        }
                        //colPondasiName.cellStyle = cellStyleNoramlTitleCenter
                        //colPondasiName.setCellValue(dataPondasiName[element])
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 3, 6))

                        val colStrukturVolume = rowStruktur.createCell(7)
                        colStrukturVolume.cellStyle = cellStyleNoramlTitleCenter
                        colStrukturVolume.setCellValue(dataStrukturVolume[element])
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 7, 8))

                        val colStrukturKeterangan = rowStruktur.createCell(10)
                        colStrukturKeterangan.cellStyle = cellStyleNoramlTitleCenter
                        colStrukturKeterangan.setCellValue("Struktur Bangunan")
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 10, 14))

                    }
                }

                //kerangka atap
                if (dataKeratapId.isNotEmpty()) {
                    for (element in dataKeratapId.indices) {
                        indexNoSpesifikasi++
                        createRow(currentLastRow)
                        val rowKertap = ws.getRow(oldLastRow)
                        rowKertap.height = 2 * 256

                        val colKertapNo = rowKertap.createCell(2)
                        colKertapNo.cellStyle = cellStyleNoramlTitleCenter
                        colKertapNo.setCellValue("$indexNoSpesifikasi")

                        val colKertapName = rowKertap.createCell(3)
                        colKertapName.let {
                            it.cellStyle = cellStyleNoramlTitleCenter
                            if (dataKeratapNameOther[element].isNotEmpty()) {
                                it.setCellValue(dataKeratapNameOther[element])
                            } else {
                                it.setCellValue(dataKeratapName[element])
                            }
                        }
                        //colPondasiName.cellStyle = cellStyleNoramlTitleCenter
                        //colPondasiName.setCellValue(dataPondasiName[element])
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 3, 6))

                        val colKertapVolume = rowKertap.createCell(7)
                        colKertapVolume.cellStyle = cellStyleNoramlTitleCenter
                        colKertapVolume.setCellValue(dataKeratapVolume[element])
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 7, 8))

                        val colKertapKeterangan = rowKertap.createCell(10)
                        colKertapKeterangan.cellStyle = cellStyleNoramlTitleCenter
                        colKertapKeterangan.setCellValue("Kerangka Atap")
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 10, 14))

                    }
                }

                //penutup atap
                if (dataPenutapId.isNotEmpty()) {
                    for (element in dataPenutapId.indices) {
                        createRow(currentLastRow)
                        indexNoSpesifikasi++
                        val rowPenTap = ws.getRow(oldLastRow)
                        rowPenTap.height = 2 * 256

                        val colPentapNo = rowPenTap.createCell(2)
                        colPentapNo.cellStyle = cellStyleNoramlTitleCenter
                        colPentapNo.setCellValue("$indexNoSpesifikasi")

                        val colPentapName = rowPenTap.createCell(3)
                        colPentapName.let {
                            it.cellStyle = cellStyleNoramlTitleCenter
                            if (dataPenutapNameOther[element].isNotEmpty()) {
                                it.setCellValue(dataPenutapNameOther[element])
                            } else {
                                it.setCellValue(dataPenutapName[element])
                            }
                        }
                        //colPondasiName.cellStyle = cellStyleNoramlTitleCenter
                        //colPondasiName.setCellValue(dataPondasiName[element])
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 3, 6))

                        val colPentapVolume = rowPenTap.createCell(7)
                        colPentapVolume.cellStyle = cellStyleNoramlTitleCenter
                        colPentapVolume.setCellValue(dataPenutapVolume[element])
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 7, 8))

                        val colPentapKeterangan = rowPenTap.createCell(10)
                        colPentapKeterangan.cellStyle = cellStyleNoramlTitleCenter
                        colPentapKeterangan.setCellValue("Penutup Atap")
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 10, 14))

                    }
                }

                //plafon
                if (dataPlafonId.isNotEmpty()) {
                    for (element in dataPlafonId.indices) {
                        indexNoSpesifikasi++
                        createRow(currentLastRow)
                        val rowPlafon = ws.getRow(oldLastRow)
                        rowPlafon.height = 2 * 256

                        val colPentapNo = rowPlafon.createCell(2)
                        colPentapNo.cellStyle = cellStyleNoramlTitleCenter
                        colPentapNo.setCellValue("$indexNoSpesifikasi")

                        val colPentapName = rowPlafon.createCell(3)
                        colPentapName.let {
                            it.cellStyle = cellStyleNoramlTitleCenter
                            if (dataPlafonNameOther[element].isNotEmpty()) {
                                it.setCellValue(dataPlafonNameOther[element])
                            } else {
                                it.setCellValue(dataPlafonName[element])
                            }
                        }
                        //colPondasiName.cellStyle = cellStyleNoramlTitleCenter
                        //colPondasiName.setCellValue(dataPondasiName[element])
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 3, 6))

                        val colPlafonVolume = rowPlafon.createCell(7)
                        colPlafonVolume.cellStyle = cellStyleNoramlTitleCenter
                        colPlafonVolume.setCellValue(dataPlafonVolume[element])
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 7, 8))

                        val colPlafonKeterangan = rowPlafon.createCell(10)
                        colPlafonKeterangan.cellStyle = cellStyleNoramlTitleCenter
                        colPlafonKeterangan.setCellValue("Plafon")
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 10, 14))

                    }
                }

                //dinding
                if (dataDindingId.isNotEmpty()) {
                    for (element in dataDindingId.indices) {
                        indexNoSpesifikasi++
                        createRow(currentLastRow)
                        val rowDinding = ws.getRow(oldLastRow)
                        rowDinding.height = 2 * 256

                        val colDindingNo = rowDinding.createCell(2)
                        colDindingNo.cellStyle = cellStyleNoramlTitleCenter
                        colDindingNo.setCellValue("$indexNoSpesifikasi")

                        val colDindingName = rowDinding.createCell(3)
                        colDindingName.let {
                            it.cellStyle = cellStyleNoramlTitleCenter
                            if (dataDindingNameOther[element].isNotEmpty()) {
                                it.setCellValue(dataDindingNameOther[element])
                            } else {
                                it.setCellValue(dataDindingName[element])
                            }
                        }
                        //colPondasiName.cellStyle = cellStyleNoramlTitleCenter
                        //colPondasiName.setCellValue(dataPondasiName[element])
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 3, 6))

                        val colDindingVolume = rowDinding.createCell(7)
                        colDindingVolume.cellStyle = cellStyleNoramlTitleCenter
                        colDindingVolume.setCellValue(dataDindingVolume[element])
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 7, 8))

                        val colDindingKeterangan = rowDinding.createCell(10)
                        colDindingKeterangan.cellStyle = cellStyleNoramlTitleCenter
                        colDindingKeterangan.setCellValue("Dinding")
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 10, 14))
                    }
                }

                //pintu & jendela
                if (dataPinjelId.isNotEmpty()) {
                    for (element in dataPinjelId.indices) {
                        indexNoSpesifikasi++
                        createRow(currentLastRow)
                        val rowPinjen = ws.getRow(oldLastRow)
                        rowPinjen.height = 2 * 256

                        val colPinjenNo = rowPinjen.createCell(2)
                        colPinjenNo.cellStyle = cellStyleNoramlTitleCenter
                        colPinjenNo.setCellValue("$indexNoSpesifikasi")

                        val colPinjenName = rowPinjen.createCell(3)
                        colPinjenName.let {
                            it.cellStyle = cellStyleNoramlTitleCenter
                            if (dataPinjelNameOther[element].isNotEmpty()) {
                                it.setCellValue(dataPinjelNameOther[element])
                            } else {
                                it.setCellValue(dataPinjelName[element])
                            }
                        }
                        //colPondasiName.cellStyle = cellStyleNoramlTitleCenter
                        //colPondasiName.setCellValue(dataPondasiName[element])
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 3, 6))

                        val colPinJenVolume = rowPinjen.createCell(7)
                        colPinJenVolume.cellStyle = cellStyleNoramlTitleCenter
                        colPinJenVolume.setCellValue(dataPinjelVolume[element])
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 7, 8))

                        val colPinJenKeterangan = rowPinjen.createCell(10)
                        colPinJenKeterangan.cellStyle = cellStyleNoramlTitleCenter
                        colPinJenKeterangan.setCellValue("Pintu & Jendela")
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 10, 14))

                    }
                }

                //lantai
                if (dataLantaiId.isNotEmpty()) {
                    for (element in dataLantaiId.indices) {
                        indexNoSpesifikasi++
                        createRow(currentLastRow)
                        val rowLantai = ws.getRow(oldLastRow)
                        rowLantai.height = 2 * 256

                        val colLantaiNo = rowLantai.createCell(2)
                        colLantaiNo.cellStyle = cellStyleNoramlTitleCenter
                        colLantaiNo.setCellValue("$indexNoSpesifikasi")

                        val colLantaiName = rowLantai.createCell(3)
                        colLantaiName.let {
                            it.cellStyle = cellStyleNoramlTitleCenter
                            if (dataLantaiNameOther[element].isNotEmpty()) {
                                it.setCellValue(dataLantaiNameOther[element])
                            } else {
                                it.setCellValue(dataLantaiName[element])
                            }
                        }
                        //colPondasiName.cellStyle = cellStyleNoramlTitleCenter
                        //colPondasiName.setCellValue(dataPondasiName[element])
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 3, 6))

                        val colLantaiVolume = rowLantai.createCell(7)
                        colLantaiVolume.cellStyle = cellStyleNoramlTitleCenter
                        colLantaiVolume.setCellValue(dataLantaiVolume[element])
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 7, 8))

                        val colLantaiKeterangan = rowLantai.createCell(10)
                        colLantaiKeterangan.cellStyle = cellStyleNoramlTitleCenter
                        colLantaiKeterangan.setCellValue("Lantai")
                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 10, 14))

                    }
                }

                /* tanda tangan */
                createRow(currentLastRow + 1)
                val rowdateTTD = ws.getRow(oldLastRow)
                val colttD = rowdateTTD.createCell(9)
                colttD.cellStyle = cellStyleTglTtd
                colttD.setCellValue("Tempat, $currentDate")
                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 9, 14))


                /* nama ttd petugas & pemilik tanah */
                createRow(currentLastRow + 1)
                val rowpetugasdanpemilikTTD = ws.getRow(oldLastRow)

                val colpetugasTTD = rowpetugasdanpemilikTTD.createCell(2)
                colpetugasTTD.cellStyle = cellStyleTtd
                colpetugasTTD.setCellValue("Petugas Satgas B")
                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 2, 3))

                val colpemilikTTD = rowpetugasdanpemilikTTD.createCell(9)
                colpemilikTTD.cellStyle = cellStyleTtd
                colpemilikTTD.setCellValue("Pemilik Tanah")
                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 9, 14))

                /* garis ttd petugas & pemilik tanah */
                createRow(currentLastRow + 4)
                val rowgarispetugaspemiliTTD = ws.getRow(oldLastRow)
                val colgarispetugasTTD = rowgarispetugaspemiliTTD.createCell(2)
                colgarispetugasTTD.cellStyle = cellStyleTtd
                colgarispetugasTTD.setCellValue("___________________________________")
                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 2, 3))

                val colgarispemilikTTD = rowgarispetugaspemiliTTD.createCell(9)
                colgarispemilikTTD.cellStyle = cellStyleTtd
                colgarispemilikTTD.setCellValue("___________________________________")
                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 9, 14))


                /* nama ttd mengetahui  */
                createRow(currentLastRow + 2)
                val rowmengetahuiTTD = ws.getRow(oldLastRow)
                val colmengetahuiTTD = rowmengetahuiTTD.createCell(4)
                colmengetahuiTTD.cellStyle = cellStyleTtd
                colmengetahuiTTD.setCellValue("Mengetahui")
                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 4, 9))

                createRow(currentLastRow + 4)
                val rowgarismengetahuiTTD = ws.getRow(oldLastRow)
                val colgarismengetahuiTTD = rowgarismengetahuiTTD.createCell(4)
                colgarismengetahuiTTD.cellStyle = cellStyleTtd
                colgarismengetahuiTTD.setCellValue("___________________________________")
                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 4, 9))

            }, {
                activity.runOnUiThread {
                    val pathDir = File(Environment.getExternalStorageDirectory().toString() + File.separator + "SIOJT", "Downloads")
                    val outputFile = File(pathDir, mfilename)

                    var ostream: FileOutputStream? = null
                    try {
                        if (!pathDir.exists()) {
                            pathDir.mkdirs()
                        }

                        if (!outputFile.exists()) {
                            outputFile.delete()
                            outputFile.createNewFile()
                        }

                        ostream = FileOutputStream(outputFile)
                        wb.write(ostream)
                        ostream.flush()
                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                        Log.d("Error", "${e.message}")

                    } catch (e: Exception) {
                        e.printStackTrace()
                        Log.d("Error", "Export File Gagal")
                    } finally {
                        if (ostream != null) {
                            ostream.close()
                            try {
                                sweetAlretLoading = SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                sweetAlretLoading.titleText = "Export Data Berhasil"
                                sweetAlretLoading.setCancelable(false)
                                sweetAlretLoading.contentText = outputFile.toString()
                                sweetAlretLoading.confirmText = "OK"
                                sweetAlretLoading.setConfirmClickListener { sDialog ->
                                    sDialog?.let { if (it.isShowing) it.dismiss() }
                                    openFile(mfilename)
                                }
                                sweetAlretLoading.show()
                            } catch (e: IOException) {
                                Log.d("Error", "File gagal dibuka ${e.message}")
                            }
                        }


                    }
                }
            },null)
        }
    }

    @SuppressLint("SdCardPath", "DefaultLocale")
    @Suppress("DEPRECATION")
    fun saveExcelFile(filenameExport: String, landId: Int?, bangunanIdTemp: Int?, projectName: String) {

        val mDialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_confirm_export, null)
        val mBuilder = AlertDialog.Builder(activity)
        val mDialog = mBuilder.create()

        mDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val window = mDialog.window
        if (window != null) {
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            mDialog.window!!.setLayout(width, height)
        }
        mDialog.setView(mDialogView)
        mDialogView.btnExportCSV.visibility = View.INVISIBLE
        mDialogView.btnExportExcel.setOnClickListener {
            isPdfile = false
            mDialog.dismiss()
            createExcelFile(projectName, landId, bangunanIdTemp, projectName)
        }

        mDialogView.btnExportPdf.setOnClickListener {
            isPdfile = true
            mDialog.dismiss()
            convertDocToPdfitext(landId, bangunanIdTemp, projectName)
        }

        mDialog.show()

    }

    private fun createExcelFile(filenameExport: String, landId: Int?, bangunanIdTemp: Int?, projectName: String) {
        CreateFormatExcelTask(filenameExport, landId, bangunanIdTemp, projectName).execute()
    }

    private fun convertDocToPdfitext(landId: Int?, bangunanIdTemp: Int?, projectName: String) {
        val document = Document(PageSize.A4.rotate(), 40f, 40f, 35f, 40f)
        val pathDir = File(Environment.getExternalStorageDirectory().toString() + File.separator + "SIOJT", "Downloads")
        val mfileName = "$projectName.pdf"
        val outputFile = File(pathDir, mfileName)

        Handler().postDelayed({
            try {
                val fos = FileOutputStream(outputFile)
                val pdfWriter = PdfWriter.getInstance(document, fos)
                document.open()

                val fontBold = Font(Font.FontFamily.HELVETICA, 16f, Font.BOLD)
                val headerTitle1 = Paragraph("PELAKSANAAN PENGADAAN TANAH UNTUK PEMBANGUNAN", fontBold)
                headerTitle1.alignment = Element.ALIGN_CENTER

                val headerTitle12 = Paragraph("DATA PEMILIK BANGUNAN YANG TERKENA PENGADAAN TANAH", fontBold)
                headerTitle12.alignment = Element.ALIGN_CENTER
                headerTitle12.spacingAfter = 20f

                var headerproyekName: Paragraph? = null
                headerproyekName = Paragraph(projectName, fontBold)
                headerproyekName.alignment = Element.ALIGN_CENTER
                headerproyekName.spacingAfter = 20f

                document.add(headerTitle1)
                document.add(headerproyekName)
                document.add(headerTitle12)

                val firstTable = createFirstTable(landId, bangunanIdTemp)
                //val secondTable = createSecondTable(landId)
                document.add(firstTable)
                //document.add(secondTable)

                document.close()
                pdfWriter.close()
            }
            catch (e: DocumentException) {
                Log.d("DocumentException", e.message.toString())
            } catch (e: Exception) {
                Log.d("Exception", e.message.toString())
            } finally {
                activity.runOnUiThread {
                    sweetAlretLoading = SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                    sweetAlretLoading.titleText = "Export Data Berhasil"
                    sweetAlretLoading.setCancelable(false)
                    sweetAlretLoading.contentText = outputFile.toString()
                    sweetAlretLoading.confirmText = "OK"
                    sweetAlretLoading.setConfirmClickListener { sDialog ->
                        sDialog?.let { if (it.isShowing) it.dismiss() }
                        openFile(mfileName)
                    }
                    sweetAlretLoading.show()
                }
            }

        }, 5000)

    }

    private fun createFirstTable(landId: Int?, bangunanIdTemp: Int?): PdfPTable {
        val table = PdfPTable(15)
        table.defaultCell.border = 0
        //setColumnWidth
        table.setTotalWidth(floatArrayOf(20f, 50f, 144f, 20f, 20f,
            50f, 50f, 50f, 50f, 50f,
            50f, 50f, 50f, 50f, 20f))
        table.widthPercentage = 100f

        val calendar = Calendar.getInstance(Locale.getDefault())
        //c.get(Calendar.YEAR)
        //c.get(Calendar.MONTH) + 1
        //c.get(Calendar.DAY_OF_MONTH)
        //val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        //val currentDate = sdf.format(c.time)
        val currentDate = DateFormat.format(dateTemplate, calendar.time)

        //Letak tanah
        var letakProvinsi = ""
        var letakKota = ""
        var letakKecamatan = ""
        var letakKelurahan = ""
        var letakRTRW = ""
        var letakNo = ""
        var letakJalan = ""
        var letakNoBidang = ""

        //Pemilik pertama tanah
        var namaPemilikPertama: String? = ""
        var ttlPemilikPertama: String? = ""
        var pekerjaanPemilikPertama: String? = ""
        var alamatJlnPemilikPertama: String? = ""
        var alamatBlokPemilikPertama: String? = ""
        var alamatRtRwPemilikPertama: String? = ""
        var alamatKelurahanPemilikPertama: String? = ""
        var alamatKecamatanPemilikPertama: String? = ""
        var alamatKotaPemilikPertama: String? = ""
        var alamatProvinsiPemilikPertama: String? = ""

        var nomorIdentitasPemilikPertama: String? = ""
        var indexTotalSpesifikasiBangunan = 0

        val ponddasiName: ArrayList<String> = ArrayList()
        val ponddasiVolume: ArrayList<String> = ArrayList()

        val strukturName: ArrayList<String> = ArrayList()
        val strukturVolume: ArrayList<String> = ArrayList()

        val kertapName: ArrayList<String> = ArrayList()
        val kertapVolume: ArrayList<String> = ArrayList()

        val petapName: ArrayList<String> = ArrayList()
        val petapVolume: ArrayList<String> = ArrayList()

        val plafonName: ArrayList<String> = ArrayList()
        val plafonVolume: ArrayList<String> = ArrayList()

        val dindingName: ArrayList<String> = ArrayList()
        val dindingVolume: ArrayList<String> = ArrayList()

        val pinjelName: ArrayList<String> = ArrayList()
        val pinjelVolume: ArrayList<String> = ArrayList()

        val lantaiName: ArrayList<String> = ArrayList()
        val lantaiVolume: ArrayList<String> = ArrayList()


        realm.executeTransaction { inrealm ->
            val results = inrealm.where(ProjectLand::class.java).equalTo("PartyIdTemp", landId).findFirst()
            if (results != null) {
                letakProvinsi = ""
                letakKota = ""
                letakKecamatan = ""
                letakKelurahan = ""
                letakRTRW = ""
                letakNo = ""
                letakJalan = ""
                letakNoBidang = ""

                namaPemilikPertama = ""
                ttlPemilikPertama = ""
                pekerjaanPemilikPertama = ""
                alamatJlnPemilikPertama = ""
                alamatBlokPemilikPertama = ""
                alamatRtRwPemilikPertama = ""
                alamatKelurahanPemilikPertama = ""
                alamatKecamatanPemilikPertama = ""
                alamatKotaPemilikPertama = ""
                alamatProvinsiPemilikPertama = ""

                nomorIdentitasPemilikPertama = ""
                indexNoSpesifikasi = 0

                ponddasiName.clear()
                ponddasiVolume.clear()

                strukturName.clear()
                strukturVolume.clear()

                kertapName.clear()
                kertapVolume.clear()

                petapName.clear()
                petapVolume.clear()

                plafonName.clear()
                plafonVolume.clear()

                dindingName.clear()
                dindingVolume.clear()

                pinjelName.clear()
                pinjelVolume.clear()

                lantaiName.clear()
                lantaiVolume.clear()

                //letak tanah
                letakProvinsi = if (!results.landProvinceName.isNullOrEmpty()) {
                    results.landProvinceName
                } else {
                    "-"
                }

                letakKota = if (!results.landRegencyName.isNullOrEmpty()) {
                    results.landRegencyName
                } else {
                    "-"
                }

                letakKecamatan = if (!results.landDistrictName.isNullOrEmpty()) {
                    results.landDistrictName
                } else {
                    "-"
                }

                letakKelurahan = if (!results.landVillageName.isNullOrEmpty()) {
                    results.landVillageName
                } else {
                    "-"
                }

                letakRTRW = if (!results.landRT.isNullOrEmpty() || results.landRW.isNullOrEmpty()) {
                    "${results.landRT} / ${results.landRW}"
                } else {
                    "-"
                }

                letakJalan = if (!results.landStreet.isNullOrEmpty()) {
                    results.landStreet
                } else {
                    "-"
                }

                letakNo = if (!results.landBlok.isNullOrEmpty()) {
                    results.landBlok
                } else {
                    "-"
                }

                letakNoBidang = if (!results.landNumberList.isNullOrEmpty()) {
                    results.landNumberList
                } else {
                    "-"
                }

                /* spsesfikasi bangunan*/
                val resultsDataBangunan = inrealm.where(Bangunan::class.java)
                    .equalTo("LandIdTemp", landId)
                    .and()
                    .equalTo("BangunanIdTemp", bangunanIdTemp)
                    .findFirst()

                /* pemilik bangunan */
                val idPemilikBangunan = resultsDataBangunan?.projectPartyId
                val firstCharacterOfIdPemilkBangunan = idPemilikBangunan?.get(0)
                val lastCharacterOfIdPemilkBangunan = idPemilikBangunan?.substring(idPemilikBangunan.indexOf("$firstCharacterOfIdPemilkBangunan") + 1)

                when {
                    firstCharacterOfIdPemilkBangunan.toString() == "P" -> {
                        val resultDataPemilikPertama = inrealm.where(PartyAdd::class.java)
                            .equalTo("TempId", lastCharacterOfIdPemilkBangunan?.toInt())
                            .findFirst()

                        val tmpLahirPemilik = if (!resultDataPemilikPertama?.partyBirthPlaceName.isNullOrEmpty()) {
                            resultDataPemilikPertama?.partyBirthPlaceName.toString()
                        } else {
                            "-"
                        }

                        val tglLahirPemilik = if (!resultDataPemilikPertama?.partyBirthDate.isNullOrEmpty()) {
                            resultDataPemilikPertama?.partyBirthDate.toString()
                        } else {
                            "-"
                        }

                        val alamartRT = if (!resultDataPemilikPertama?.partyRT.isNullOrEmpty()) {
                            resultDataPemilikPertama?.partyRT.toString()
                        } else {
                            "-"
                        }

                        val alamartRW = if (!resultDataPemilikPertama?.partyRW.isNullOrEmpty()) {
                            resultDataPemilikPertama?.partyRW.toString()
                        } else {
                            "-"
                        }

                        namaPemilikPertama = resultDataPemilikPertama?.partyFullName
                        ttlPemilikPertama = "$tmpLahirPemilik, $tglLahirPemilik"

                        pekerjaanPemilikPertama = if (!resultDataPemilikPertama?.partyOccupationName.isNullOrEmpty()) {
                            resultDataPemilikPertama?.partyOccupationName.toString()
                        } else {
                            "-"
                        }

                        /* alamat pemilik pertama*/
                        alamatJlnPemilikPertama = if (!resultDataPemilikPertama?.partyStreetName.isNullOrEmpty()) {
                            resultDataPemilikPertama?.partyStreetName.toString()
                        } else {
                            "-"
                        }

                        alamatBlokPemilikPertama = if (!resultDataPemilikPertama?.partyBlock.toString().isNullOrEmpty()) {
                            resultDataPemilikPertama?.partyBlock.toString()
                        } else {
                            "-"
                        }

                        alamatRtRwPemilikPertama = "$alamartRT / $alamartRW"
                        alamatKelurahanPemilikPertama = resultDataPemilikPertama?.partyVillageName
                        alamatKecamatanPemilikPertama = resultDataPemilikPertama?.partyDistrictName
                        alamatKotaPemilikPertama = resultDataPemilikPertama?.partyRegencyName
                        alamatProvinsiPemilikPertama = resultDataPemilikPertama?.partyProvinceName
                        nomorIdentitasPemilikPertama = if (!resultDataPemilikPertama?.partyIdentityNumber.isNullOrEmpty()) {
                            resultDataPemilikPertama?.partyIdentityNumber
                        } else {
                            "-"
                        }

                    }
                    firstCharacterOfIdPemilkBangunan.toString() == "S" -> {
                        val resultDataPemilikKedua = inrealm.where(Subjek2::class.java)
                            .equalTo("SubjekIdTemp", lastCharacterOfIdPemilkBangunan?.toInt())
                            .findFirst()

                        val tmpLahirPemilik = if (!resultDataPemilikKedua?.subjek2BirthPlaceName.toString().isNullOrEmpty()) {
                            resultDataPemilikKedua?.subjek2BirthPlaceName.toString()
                        } else {
                            "-"
                        }

                        val tglLahirPemilik = if (!resultDataPemilikKedua?.subjek2BirthDate.toString().isNullOrEmpty()) {
                            resultDataPemilikKedua?.subjek2BirthDate.toString()
                        } else {
                            "-"
                        }

                        val alamartRT = if (!resultDataPemilikKedua?.subjek2RT.toString().isNullOrEmpty()) {
                            resultDataPemilikKedua?.subjek2RT.toString()
                        } else {
                            "-"
                        }

                        val alamartRW = if (!resultDataPemilikKedua?.subjek2RW.toString().isNullOrEmpty()) {
                            resultDataPemilikKedua?.subjek2RW.toString()
                        } else {
                            "-"
                        }

                        namaPemilikPertama = resultDataPemilikKedua?.subjek2FullName
                        ttlPemilikPertama = "$tmpLahirPemilik, $tglLahirPemilik"

                        pekerjaanPemilikPertama = if (!resultDataPemilikKedua?.subjek2OccupationName.toString().isNullOrEmpty()) {
                            resultDataPemilikKedua?.subjek2OccupationName.toString()
                        } else {
                            "-"
                        }

                        /* alamat pemilik pertama*/
                        alamatJlnPemilikPertama = if (!resultDataPemilikKedua?.subjek2StreetName.toString().isNullOrEmpty()) {
                            resultDataPemilikKedua?.subjek2StreetName.toString()
                        } else {
                            "-"
                        }

                        alamatBlokPemilikPertama = if (!resultDataPemilikKedua?.subjek2Block.toString().isNullOrEmpty()) {
                            resultDataPemilikKedua?.subjek2Block.toString()
                        } else {
                            "-"
                        }

                        alamatRtRwPemilikPertama = "$alamartRT / $alamartRW"
                        alamatKelurahanPemilikPertama = resultDataPemilikKedua?.subjek2VillageName
                        alamatKecamatanPemilikPertama = resultDataPemilikKedua?.subjek2DistrictName
                        alamatKotaPemilikPertama = resultDataPemilikKedua?.subjek2RegencyName
                        alamatProvinsiPemilikPertama = resultDataPemilikKedua?.subjek2ProvinceName
                        nomorIdentitasPemilikPertama = if (!resultDataPemilikKedua?.subjek2IdentityNumber.toString().isNullOrEmpty()) {
                            resultDataPemilikKedua?.subjek2IdentityNumber
                        } else {
                            "-"
                        }

                    }
                    else -> {
                        val resultDataAhliWaris = inrealm.where(AhliWaris::class.java)
                            .equalTo("AhliWarisIdTemp", lastCharacterOfIdPemilkBangunan?.toInt())
                            .findFirst()

                        val tmpLahirPemilik = if (!resultDataAhliWaris?.partyBirthPlaceName.isNullOrEmpty()) {
                            resultDataAhliWaris?.partyBirthPlaceName.toString()
                        } else {
                            "-"
                        }

                        val tglLahirPemilik = if (!resultDataAhliWaris?.partyBirthDate.isNullOrEmpty()) {
                            resultDataAhliWaris?.partyBirthDate.toString()
                        } else {
                            "-"
                        }

                        val alamartRT = if (!resultDataAhliWaris?.partyRT.isNullOrEmpty()) {
                            resultDataAhliWaris?.partyRT.toString()
                        } else {
                            "-"
                        }

                        val alamartRW = if (!resultDataAhliWaris?.partyRW.isNullOrEmpty()) {
                            resultDataAhliWaris?.partyRW.toString()
                        } else {
                            "-"
                        }

                        namaPemilikPertama = resultDataAhliWaris?.partyFullName
                        ttlPemilikPertama = "$tmpLahirPemilik, $tglLahirPemilik"

                        pekerjaanPemilikPertama = if (!resultDataAhliWaris?.partyOccupationName.toString().isNullOrEmpty()) {
                            resultDataAhliWaris?.partyOccupationName.toString()
                        } else {
                            "-"
                        }

                        /* alamat pemilik pertama*/
                        alamatJlnPemilikPertama = if (!resultDataAhliWaris?.partyStreetName.toString().isNullOrEmpty()) {
                            resultDataAhliWaris?.partyStreetName.toString()
                        } else {
                            "-"
                        }

                        alamatBlokPemilikPertama = if (!resultDataAhliWaris?.partyBlock.toString().isNullOrEmpty()) {
                            resultDataAhliWaris?.partyBlock.toString()
                        } else {
                            "-"
                        }

                        alamatRtRwPemilikPertama = "$alamartRT / $alamartRW"
                        alamatKelurahanPemilikPertama = resultDataAhliWaris?.partyVillageName
                        alamatKecamatanPemilikPertama = resultDataAhliWaris?.partyDistrictName
                        alamatKotaPemilikPertama = resultDataAhliWaris?.partyRegencyName
                        alamatProvinsiPemilikPertama = resultDataAhliWaris?.partyProvinceName
                        nomorIdentitasPemilikPertama = if (!resultDataAhliWaris?.partyIdentityNumber.toString().isNullOrEmpty()) {
                            resultDataAhliWaris?.partyIdentityNumber
                        } else {
                            "-"
                        }
                    }
                }//end of condition id pemilik bangunan

                val convertPondasiBangunanId = resultsDataBangunan?.foundationTypeId
                val convertPondasiBangunanId2 = convertPondasiBangunanId?.replace("[", "['")
                val convertPondasiBangunanId3 = convertPondasiBangunanId2?.replace("]", "']")
                val convertPondasiBangunanId4 = convertPondasiBangunanId3?.replace(", ", "','")

                val convertPondasiBangunanName = resultsDataBangunan?.foundationTypeName
                val convertPondasiBangunanName2 = convertPondasiBangunanName?.replace("[", "['")
                val convertPondasiBangunanName3 = convertPondasiBangunanName2?.replace("]", "']")
                val convertPondasiBangunanName4 = convertPondasiBangunanName3?.replace(", ", "','")

                val convertPondasiBangunanNameOther = resultsDataBangunan?.foundationTypeNameOther
                val convertPondasiBangunanNameOther2 = convertPondasiBangunanNameOther?.replace("[", "['")
                val convertPondasiBangunanNameOther3 = convertPondasiBangunanNameOther2?.replace("]", "']")
                val convertPondasiBangunanNameOther4 = convertPondasiBangunanNameOther3?.replace(", ", "','")

                val convertStrukturPondasiVolume = resultsDataBangunan?.pbFoundationTypeAreaTotal
                val convertStrukturPondasiVolume2 = convertStrukturPondasiVolume?.replace("[", "['")
                val convertStrukturPondasiVolume3 = convertStrukturPondasiVolume2?.replace("]", "']")
                val convertStrukturPondasiVolume4 = convertStrukturPondasiVolume3?.replace(", ", "','")

                val dataPondasiId = Gson().fromJson(convertPondasiBangunanId4, Array<String>::class.java).toList()
                val dataPondasiName = Gson().fromJson(convertPondasiBangunanName4, Array<String>::class.java).toList()
                val dataPondasiNameOther = Gson().fromJson(convertPondasiBangunanNameOther4, Array<String>::class.java).toList()
                val dataPondasiVolume = Gson().fromJson(convertStrukturPondasiVolume4, Array<String>::class.java).toList()

                /*struktur*/
                val convertStrukturBangunanId = resultsDataBangunan?.structureTypeId
                val convertStrukturBangunanId2 = convertStrukturBangunanId?.replace("[", "['")
                val convertStrukturBangunanId3 = convertStrukturBangunanId2?.replace("]", "']")
                val convertStrukturBangunanId4 = convertStrukturBangunanId3?.replace(", ", "','")

                val convertStrukturBangunanName = resultsDataBangunan?.structureTypeName
                val convertStrukturBangunanName2 = convertStrukturBangunanName?.replace("[", "['")
                val convertStrukturBangunanName3 = convertStrukturBangunanName2?.replace("]", "']")
                val convertStrukturBangunanName4 = convertStrukturBangunanName3?.replace(", ", "','")

                val convertStrukturBangunanNameOther = resultsDataBangunan?.structureTypeNameOther
                val convertStrukturBangunanNameOther2 = convertStrukturBangunanNameOther?.replace("[", "['")
                val convertStrukturBangunanNameOther3 = convertStrukturBangunanNameOther2?.replace("]", "']")
                val convertStrukturBangunanNameOther4 = convertStrukturBangunanNameOther3?.replace(", ", "','")

                val convertStrukturBangunanVolume = resultsDataBangunan?.pbStructureTypeAreaTotal
                val convertStrukturBangunanVolume2 = convertStrukturBangunanVolume?.replace("[", "['")
                val convertStrukturBangunanVolume3 = convertStrukturBangunanVolume2?.replace("]", "']")
                val convertStrukturBangunanVolume4 = convertStrukturBangunanVolume3?.replace(", ", "','")

                val dataStrukturId = Gson().fromJson(convertStrukturBangunanId4, Array<String>::class.java).toList()
                val dataStrukturName = Gson().fromJson(convertStrukturBangunanName4, Array<String>::class.java).toList()
                val dataStrukturNameOther = Gson().fromJson(convertStrukturBangunanNameOther4, Array<String>::class.java).toList()
                val dataStrukturVolume = Gson().fromJson(convertStrukturBangunanVolume4, Array<String>::class.java).toList()

                /*kerangka atap */
                val convertKeratapBangunanId = resultsDataBangunan?.roofFrameTypeId
                val convertKeratapBangunanId2 = convertKeratapBangunanId?.replace("[", "['")
                val convertKeratapBangunanId3 = convertKeratapBangunanId2?.replace("]", "']")
                val convertKeratapBangunanId4 = convertKeratapBangunanId3?.replace(", ", "','")

                val convertKeratapBangunanName = resultsDataBangunan?.roofFrameTypeName
                val convertKeratapBangunanName2 = convertKeratapBangunanName?.replace("[", "['")
                val convertKeratapBangunanName3 = convertKeratapBangunanName2?.replace("]", "']")
                val convertKeratapBangunanName4 = convertKeratapBangunanName3?.replace(", ", "','")

                val convertKeratapBangunanNameOther = resultsDataBangunan?.roofFrameTypeNameOther
                val convertKeratapBangunanNameOther2 = convertKeratapBangunanNameOther?.replace("[", "['")
                val convertKeratapBangunanNameOther3 = convertKeratapBangunanNameOther2?.replace("]", "']")
                val convertKeratapBangunanNameOther4 = convertKeratapBangunanNameOther3?.replace(", ", "','")

                val convertKeratapBangunanVolume = resultsDataBangunan?.pbRoofFrameTypeAreaTotal
                val convertKeratapBangunanVolume2 = convertKeratapBangunanVolume?.replace("[", "['")
                val convertKeratapBangunanVolume3 = convertKeratapBangunanVolume2?.replace("]", "']")
                val convertKeratapBangunanVolume4 = convertKeratapBangunanVolume3?.replace(", ", "','")

                val dataKeratapId = Gson().fromJson(convertKeratapBangunanId4, Array<String>::class.java).toList()
                val dataKeratapName = Gson().fromJson(convertKeratapBangunanName4, Array<String>::class.java).toList()
                val dataKeratapNameOther = Gson().fromJson(convertKeratapBangunanNameOther4, Array<String>::class.java).toList()
                val dataKeratapVolume = Gson().fromJson(convertKeratapBangunanVolume4, Array<String>::class.java).toList()

                /*penutup atap */
                val convertPenutapBangunanId = resultsDataBangunan?.roofCoveringTypeId
                val convertPenutapBangunanId2 = convertPenutapBangunanId?.replace("[", "['")
                val convertPenutapBangunanId3 = convertPenutapBangunanId2?.replace("]", "']")
                val convertPenutapBangunanId4 = convertPenutapBangunanId3?.replace(", ", "','")

                val convertPenutapBangunanName = resultsDataBangunan?.roofCoveringTypeName
                val convertPenutapBangunanNam2 = convertPenutapBangunanName?.replace("[", "['")
                val convertPenutapBangunanNam3 = convertPenutapBangunanNam2?.replace("]", "']")
                val convertPenutapBangunanNam4 = convertPenutapBangunanNam3?.replace(", ", "','")

                val convertPenutapBangunanNameOther = resultsDataBangunan?.roofCoveringTypeNameOther
                val convertPenutapBangunanNameOther2 = convertPenutapBangunanNameOther?.replace("[", "['")
                val convertPenutapBangunanNameOther3 = convertPenutapBangunanNameOther2?.replace("]", "']")
                val convertPenutapBangunanNameOther4 = convertPenutapBangunanNameOther3?.replace(", ", "','")

                val convertPenutapBangunanVolume = resultsDataBangunan?.pbRoofCoveringTypeAreaTotal
                val convertPenutapBangunanVolum2 = convertPenutapBangunanVolume?.replace("[", "['")
                val convertPenutapBangunanVolum3 = convertPenutapBangunanVolum2?.replace("]", "']")
                val convertPenutapBangunanVolum4 = convertPenutapBangunanVolum3?.replace(", ", "','")

                val dataPenutapId = Gson().fromJson(convertPenutapBangunanId4, Array<String>::class.java).toList()
                val dataPenutapName = Gson().fromJson(convertPenutapBangunanNam4, Array<String>::class.java).toList()
                val dataPenutapNameOther = Gson().fromJson(convertPenutapBangunanNameOther4, Array<String>::class.java).toList()
                val dataPenutapVolume = Gson().fromJson(convertPenutapBangunanVolum4, Array<String>::class.java).toList()

                /* plafon */
                val convertPlafonBangunanId = resultsDataBangunan?.ceilingTypeId
                val convertPlafonBangunanId2 = convertPlafonBangunanId?.replace("[", "['")
                val convertPlafonBangunanId3 = convertPlafonBangunanId2?.replace("]", "']")
                val convertPlafonBangunanId4 = convertPlafonBangunanId3?.replace(", ", "','")

                val convertPlafonBangunanName = resultsDataBangunan?.ceilingTypeName
                val convertPlafonBangunanName2 = convertPlafonBangunanName?.replace("[", "['")
                val convertPlafonBangunanName3 = convertPlafonBangunanName2?.replace("]", "']")
                val convertPlafonBangunanName4 = convertPlafonBangunanName3?.replace(", ", "','")

                val convertPlafonBangunanNameOther = resultsDataBangunan?.ceilingTypeNameOther
                val convertPlafonBangunanNameOther3 = convertPlafonBangunanNameOther?.replace("[", "['")
                val convertPlafonBangunanNameOther4 = convertPlafonBangunanNameOther3?.replace("]", "']")
                val convertPlafonBangunanNameOther5 = convertPlafonBangunanNameOther4?.replace(", ", "','")

                val convertPlafonBangunanVolume = resultsDataBangunan?.pbCeilingTypeAreaTotal
                val convertPlafonBangunanVolume2 = convertPlafonBangunanVolume?.replace("[", "['")
                val convertPlafonBangunanVolume3 = convertPlafonBangunanVolume2?.replace("]", "']")
                val convertPlafonBangunanVolume4 = convertPlafonBangunanVolume3?.replace(", ", "','")

                val dataPlafonId = Gson().fromJson(convertPlafonBangunanId4, Array<String>::class.java).toList()
                val dataPlafonName = Gson().fromJson(convertPlafonBangunanName4, Array<String>::class.java).toList()
                val dataPlafonNameOther = Gson().fromJson(convertPlafonBangunanNameOther5, Array<String>::class.java).toList()
                val dataPlafonVolume = Gson().fromJson(convertPlafonBangunanVolume4, Array<String>::class.java).toList()

                /* Dinding */
                val convertDindingBangunanId = resultsDataBangunan?.wallTypeId
                val convertDindingBangunanId2 = convertDindingBangunanId?.replace("[", "['")
                val convertDindingBangunanId3 = convertDindingBangunanId2?.replace("]", "']")
                val convertDindingBangunanId4 = convertDindingBangunanId3?.replace(", ", "','")

                val convertDindingBangunanName = resultsDataBangunan?.wallTypeName
                val convertDindingBangunanName2 = convertDindingBangunanName?.replace("[", "['")
                val convertDindingBangunanName3 = convertDindingBangunanName2?.replace("]", "']")
                val convertDindingBangunanName4 = convertDindingBangunanName3?.replace(", ", "','")

                val convertDindingBangunanNameOther = resultsDataBangunan?.wallTypeNameOther
                val convertDindingBangunanNameOther2 = convertDindingBangunanNameOther?.replace("[", "['")
                val convertDindingBangunanNameOther3 = convertDindingBangunanNameOther2?.replace("]", "']")
                val convertDindingBangunanNameOther4 = convertDindingBangunanNameOther3?.replace(", ", "','")

                val convertDindingBangunanVolume = resultsDataBangunan?.pbfWallTypeAreaTotal
                val convertDindingBangunanVolume2 = convertDindingBangunanVolume?.replace("[", "['")
                val convertDindingBangunanVolume3 = convertDindingBangunanVolume2?.replace("]", "']")
                val convertDindingBangunanVolume4 = convertDindingBangunanVolume3?.replace(", ", "','")

                val dataDindingId = Gson().fromJson(convertDindingBangunanId4, Array<String>::class.java).toList()
                val dataDindingName = Gson().fromJson(convertDindingBangunanName4, Array<String>::class.java).toList()
                val dataDindingNameOther = Gson().fromJson(convertDindingBangunanNameOther4, Array<String>::class.java).toList()
                val dataDindingVolume = Gson().fromJson(convertDindingBangunanVolume4, Array<String>::class.java).toList()

                /* Pintu jendela */
                val convertPinjelBangunanId = resultsDataBangunan?.doorWindowTypeld
                val convertPinjelBangunanId2 = convertPinjelBangunanId?.replace("[", "['")
                val convertPinjelBangunanId3 = convertPinjelBangunanId2?.replace("]", "']")
                val convertPinjelBangunanId4 = convertPinjelBangunanId3?.replace(", ", "','")

                val convertPinjelBangunanName = resultsDataBangunan?.doorWindowTypeName
                val convertPinjelBangunanName2 = convertPinjelBangunanName?.replace("[", "['")
                val convertPinjelBangunanName3 = convertPinjelBangunanName2?.replace("]", "']")
                val convertPinjelBangunanName4 = convertPinjelBangunanName3?.replace(", ", "','")

                val convertPinjelBangunanNameOther = resultsDataBangunan?.doorWindowTypeNameOther
                val convertPinjelBangunanNameOther2 = convertPinjelBangunanNameOther?.replace("[", "['")
                val convertPinjelBangunanNameOther3 = convertPinjelBangunanNameOther2?.replace("]", "']")
                val convertPinjelBangunanNameOther4 = convertPinjelBangunanNameOther3?.replace(", ", "','")

                val convertPinjelBangunanVolume = resultsDataBangunan?.pbDoorWindowTypeAreaTotal
                val convertPinjelBangunanVolume2 = convertPinjelBangunanVolume?.replace("[", "['")
                val convertPinjelBangunanVolume3 = convertPinjelBangunanVolume2?.replace("]", "']")
                val convertPinjelBangunanVolume4 = convertPinjelBangunanVolume3?.replace(", ", "','")

                val dataPinjelId = Gson().fromJson(convertPinjelBangunanId4, Array<String>::class.java).toList()
                val dataPinjelName = Gson().fromJson(convertPinjelBangunanName4, Array<String>::class.java).toList()
                val dataPinjelNameOther = Gson().fromJson(convertPinjelBangunanNameOther4, Array<String>::class.java).toList()
                val dataPinjelVolume = Gson().fromJson(convertPinjelBangunanVolume4, Array<String>::class.java).toList()

                /* Lantai */
                val convertLantaiBangunanId = resultsDataBangunan?.floorTypeId
                val convertLantaiBangunanId2 = convertLantaiBangunanId?.replace("[", "['")
                val convertLantaiBangunanId3 = convertLantaiBangunanId2?.replace("]", "']")
                val convertLantaiBangunanId4 = convertLantaiBangunanId3?.replace(", ", "','")

                val convertLantaiBangunanName = resultsDataBangunan?.floorTypeName
                val convertLantaiBangunanName2 = convertLantaiBangunanName?.replace("[", "['")
                val convertLantaiBangunanName3 = convertLantaiBangunanName2?.replace("]", "']")
                val convertLantaiBangunanName4 = convertLantaiBangunanName3?.replace(", ", "','")

                val convertLantaiBangunanNameOther = resultsDataBangunan?.floorTypeNameOther
                val convertLantaiBangunanNameOther2 = convertLantaiBangunanNameOther?.replace("[", "['")
                val convertLantaiBangunanNameOther3 = convertLantaiBangunanNameOther2?.replace("]", "']")
                val convertLantaiBangunanNameOther4 = convertLantaiBangunanNameOther3?.replace(", ", "','")

                val convertLantaiBangunanVolume = resultsDataBangunan?.pbFloorTypeAreaTotal
                val convertLantaiBangunanVolume2 = convertLantaiBangunanVolume?.replace("[", "['")
                val convertLantaiBangunanVolume3 = convertLantaiBangunanVolume2?.replace("]", "']")
                val convertLantaiBangunanVolume4 = convertLantaiBangunanVolume3?.replace(", ", "','")

                val dataLantaiId = Gson().fromJson(convertLantaiBangunanId4, Array<String>::class.java).toList()
                val dataLantaiName = Gson().fromJson(convertLantaiBangunanName4, Array<String>::class.java).toList()
                val dataLantaiNameOther = Gson().fromJson(convertLantaiBangunanNameOther4, Array<String>::class.java).toList()
                val dataLantaiVolume = Gson().fromJson(convertLantaiBangunanVolume4, Array<String>::class.java).toList()

                //pondasi
                if (dataPondasiId.isNotEmpty()) {
                    for ((indexPondasi, valuePondasi) in dataPondasiId.withIndex()) {
                        if (dataPondasiNameOther[indexPondasi].isEmpty()) {
                            ponddasiName.add(dataPondasiName[indexPondasi])
                        } else {
                            ponddasiName.add(dataPondasiNameOther[indexPondasi])
                        }

                        ponddasiVolume.add(dataPondasiVolume[indexPondasi])
                    }//looping data pondasi
                }

                //struktur
                if (dataStrukturId.isNotEmpty()) {
                    for ((indexStruktur, valueStruktur) in dataStrukturId.withIndex()) {
                        if (dataStrukturNameOther[indexStruktur].isNotEmpty()) {
                            strukturName.add(dataStrukturNameOther[indexStruktur])
                        } else {
                            strukturName.add(dataStrukturName[indexStruktur])
                        }

                        strukturVolume.add(dataStrukturVolume[indexStruktur])
                    }//looping data pondasi
                }

                //kerangka atap
                if (dataKeratapId.isNotEmpty()) {
                    for ((indexKertap, valueKertapId) in dataKeratapId.withIndex()) {
                        if (dataKeratapNameOther[indexKertap].isEmpty()) {
                            kertapName.add(dataKeratapName[indexKertap])
                        } else {
                            kertapName.add(dataKeratapNameOther[indexKertap])
                        }

                        kertapVolume.add(dataKeratapVolume[indexKertap])
                    }//looping data kerangka atap
                }

                //penutup atap
                if (dataPenutapId.isNotEmpty()) {
                    for ((indexPetap, valuepetapId) in dataPenutapId.withIndex()) {
                        if (dataPenutapNameOther[indexPetap].isEmpty()) {
                            petapName.add(dataPenutapName[indexPetap])
                        } else {
                            petapName.add(dataPenutapNameOther[indexPetap])
                        }

                        petapVolume.add(dataPenutapVolume[indexPetap])
                    }//looping data penutup atap
                }

                //plafon
                if (dataPlafonId.isNotEmpty()) {
                    for ((indexPlafon, valuePlafonId) in dataPlafonId.withIndex()) {
                        if (dataPlafonNameOther[indexPlafon].isEmpty()) {
                            plafonName.add(dataPlafonName[indexPlafon])
                        } else {
                            plafonName.add(dataPlafonNameOther[indexPlafon])
                        }

                        plafonVolume.add(dataPlafonVolume[indexPlafon])
                    }//looping data plafon
                }

                //dinding
                if (dataDindingId.isNotEmpty()) {
                    for ((indexDinding, valueDindigId) in dataDindingId.withIndex()) {
                        if (dataDindingNameOther[indexDinding].isNotEmpty()) {
                            dindingName.add(dataDindingNameOther[indexDinding])
                        } else {
                            dindingName.add(dataDindingName[indexDinding])
                        }

                        dindingVolume.add(dataDindingVolume[indexDinding])
                    }//looping data dinding
                }

                //pinjel
                if (dataPinjelId.isNotEmpty()) {
                    for ((indexPinjel, valuePinjelId) in dataPinjelId.withIndex()) {
                        if (dataPinjelNameOther[indexPinjel].isNotEmpty()) {
                            pinjelName.add(dataPinjelNameOther[indexPinjel])
                        } else {
                            pinjelName.add(dataPinjelName[indexPinjel])
                        }

                        pinjelVolume.add(dataPinjelVolume[indexPinjel])
                    }//looping data pinjel
                }

                //lantai
                if (dataLantaiId.isNotEmpty()) {
                    for ((indexLantai, valueLantaiId) in dataLantaiId.withIndex()) {
                        if (dataLantaiNameOther[indexLantai].isEmpty()) {
                            lantaiName.add(dataLantaiName[indexLantai])
                        } else {
                            lantaiName.add(dataLantaiNameOther[indexLantai])
                        }

                        lantaiVolume.add(dataLantaiVolume[indexLantai])
                    }//looping data lantai
                }

            }//letak tanah
        }

        table.addCell(createCellItextPdfBoldTextCenter("I", true))
        table.addCell(createCellItextPdfWithColspanBold("LETAK TANAH", 14, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, true))

        //Provinsi
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Provinsi", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakProvinsi, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Kota
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Kota", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakKota, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Kabupaten
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Kabupaten", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakKota, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Kecamatan
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Kecamatan", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakKecamatan, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Desa/Kelurahan
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Desa / Kelurahan", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakKelurahan, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //RT/RW
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- RT / RW", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakRTRW, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //RT/RW
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Jalan", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakJalan, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Blok/No
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Blok / No", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakNo, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //No bidang
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- No Bidang", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(letakNoBidang, 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Petugas Satgas
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Petugas Satgas B", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(Preference.auth.dataUser?.mobileUserEmployeeName.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))


        //space letak dan pemilik pertama tanah
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Pemilik tanah pertama
        table.addCell(createCellItextPdfBoldTextCenter("II", true))
        table.addCell(createCellItextPdfWithColspanBold("PEMILIK BANGUNAN", 14, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, true))

        //nama pemilik pertama
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Nama Lengkap", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(namaPemilikPertama.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //ttl pemilikpertama
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Tempat / Tanggal Lahir", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(ttlPemilikPertama.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //pekerjaan pemilik pertama
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Pekerjaan", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(pekerjaanPemilikPertama.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //alamat pemilik pertama
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Alamat", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        //alamat jalan
        table.addCell(createCellItextPdfWithColspanBold(alamatJlnPemilikPertama.toString(), 4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell("")
        //alamat blok
        table.addCell(createCellItextPdfCustomPositionText(alamatBlokPemilikPertama.toString(), Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell("")
        //alamat rt rw
        table.addCell(createCellItextPdfWithColspanBold(alamatRtRwPemilikPertama.toString(), 4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        //alamat kelurahan
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold(alamatKelurahanPemilikPertama.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        //alamat kecamatan
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold(alamatKecamatanPemilikPertama.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        //alamat kota & provinsi
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold(alamatKotaPemilikPertama.toString(), 4, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold(alamatProvinsiPemilikPertama.toString(), 6, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //Nik / no. SIM
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- NIK / No. SIM", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold(nomorIdentitasPemilikPertama.toString(), 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("- Terletak diatas tanah milik", 2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfBoldTextCenter(":", false))
        table.addCell(createCellItextPdfWithColspanBold("-", 11, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //space letak
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBoldCustomFonSize("SPESIFIKASI BANGUNAN", 15, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 16f))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //header table spesifikasi bangunan
        table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell("No.", 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 40f))
        table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell("Bagian Bangunan", 4, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 40f))
        table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell("Volumen (Luas)", 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 40f))
        table.addCell(createCellItextPdfWithColspanBold("m3 (m2)", 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true))
        table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell("Keterangan", 5, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 40f))

        //set pondasi
        for ((indexPondasi, valuePondasi) in ponddasiName.withIndex()) {
            indexTotalSpesifikasiBangunan++
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(indexTotalSpesifikasiBangunan.toString(), 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 50f))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(ponddasiName[indexPondasi], 4, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 30f))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(ponddasiVolume[indexPondasi], 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 30f))
            table.addCell(createCellItextPdfWithColspanBold("   ", 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell("Pondasi Bangunan", 5, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 50f))
        }//looping data pondasi

        //set struktur
        for ((indexStruktur, valueStruktur) in strukturName.withIndex()) {
            indexTotalSpesifikasiBangunan++
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(indexTotalSpesifikasiBangunan.toString(), 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 50f))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(strukturName[indexStruktur], 4, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 30f))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(strukturVolume[indexStruktur], 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 30f))
            table.addCell(createCellItextPdfWithColspanBold("", 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell("Struktur Bangunan", 5, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 30f))
        }//looping data struktur

        //set kertap
        for ((indexKertap, valueKertap) in kertapName.withIndex()) {
            indexTotalSpesifikasiBangunan++
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(indexTotalSpesifikasiBangunan.toString(), 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 50f))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(kertapName[indexKertap], 4, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 30f))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(kertapVolume[indexKertap], 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 30f))
            table.addCell(createCellItextPdfWithColspanBold("   ", 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell("Kerangka Atap Bangunan", 5, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 30f))
        }//looping data kertap

        //set petap
        for ((indexPetap, valuePetap) in petapName.withIndex()) {
            indexTotalSpesifikasiBangunan++
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(indexTotalSpesifikasiBangunan.toString(), 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 50f))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(petapName[indexPetap], 4, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 30f))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(petapVolume[indexPetap], 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 30f))
            table.addCell(createCellItextPdfWithColspanBold("   ", 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell("Pentup Atap Bangunan", 5, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 30f))
        }//looping data petap

        //set plafon
        for ((indexPlafon, valuePlafon) in plafonName.withIndex()) {
            indexTotalSpesifikasiBangunan++
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(indexTotalSpesifikasiBangunan.toString(), 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 50f))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(plafonName[indexPlafon], 4, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 30f))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(plafonVolume[indexPlafon], 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 30f))
            table.addCell(createCellItextPdfWithColspanBold("   ", 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell("Plafon Bangunan", 5, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 30f))
        }//looping data kertap

        //set dinding
        for ((indexDinding, valueDinding) in dindingName.withIndex()) {
            indexTotalSpesifikasiBangunan++
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(indexTotalSpesifikasiBangunan.toString(), 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 50f))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(dindingName[indexDinding], 4, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 30f))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(dindingVolume[indexDinding], 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 30f))
            table.addCell(createCellItextPdfWithColspanBold("   ", 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell("Dinding Bangunan", 5, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 30f))
        }//looping data dinding

        //set pinjel
        for ((indexPinjel, valuePinjel) in pinjelName.withIndex()) {
            indexTotalSpesifikasiBangunan++
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(indexTotalSpesifikasiBangunan.toString(), 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 50f))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(pinjelName[indexPinjel], 4, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 30f))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(pinjelVolume[indexPinjel], 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 30f))
            table.addCell(createCellItextPdfWithColspanBold("   ", 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell("Pintu & Jendeela Bangunan", 5, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 30f))
        }//looping data pinjel

        //set lantai
        for ((indexLantai, valueLantai) in lantaiName.withIndex()) {
            indexTotalSpesifikasiBangunan++
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(indexTotalSpesifikasiBangunan.toString(), 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 50f))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(lantaiName[indexLantai], 4, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 30f))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell(lantaiVolume[indexLantai], 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false, 30f))
            table.addCell(createCellItextPdfWithColspanBold("   ", 2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, false))
            table.addCell(createCellItextPdfWithColspanBoldCustomHeightCell("Lantai Bangunan", 5, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 30f))
        }//looping data lantai


        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("Tempat, $currentDate", 13, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, false))
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))

        //tanda tangan
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, true))

        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBoldFonSize("Petugas Satgas B", 4, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 16f))
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBoldFonSize("Pemilik Tanah", 4, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 16f))
        table.addCell("")
        table.addCell("")


        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("_______________________________", 4, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true))
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("_______________________________", 4, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true))
        table.addCell("")
        table.addCell("")

        //mengetahui ttd
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBoldFonSize("Mengetahui", 6, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true, 16f))
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")


        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell(createCellItextPdfWithColspanBold("   ", 15, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, false))
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell(createCellItextPdfWithColspanBold("_______________________________", 6, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, true))
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        table.addCell("")
        return table
    }

    /*private fun createSecondTable(landId: Int?, bangunanIdTemp: Int?): PdfPTable {
        val table = PdfPTable(15)
        table.defaultCell.border = 0
        //setColumnWidth
        table.setTotalWidth(floatArrayOf(20f, 144f, 144f, 20f, 20f,
                50f, 50f, 50f, 50f, 50f,
                50f, 50f, 50f, 50f, 20f))
        table.widthPercentage = 100f

        return table

    }*/

    private fun createRow(numRow: Int) {
        val row = ws.createRow(numRow)
        oldLastRow = numRow
        this.currentLastRow = numRow + 1
    }

    private fun createCellItextPdfCustomPositionText(content: String, horizontalElement: Int, verticalElement: Int, isBold: Boolean): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, 12f)
        }
        val cell = PdfPCell(Phrase(content, font))
        cell.isNoWrap = false
        cell.border = Rectangle.NO_BORDER
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }

    private fun createCellItextPdfBoldTextCenter(content: String, isBold: Boolean): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, 12f)
        }

        val cell = PdfPCell(Phrase(content, font))
        cell.isNoWrap = false
        cell.border = Rectangle.NO_BORDER
        cell.horizontalAlignment = Element.ALIGN_CENTER
        cell.verticalAlignment = Element.ALIGN_CENTER
        return cell
    }


    private fun createCellItextPdfWithRowspanBold(content: String, total_rowspan: Int, horizontalElement: Int, verticalElement: Int, isBold: Boolean): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, 12f)
        }
        val cell = PdfPCell(Phrase(content, font))
        cell.isNoWrap = false
        cell.border = Rectangle.NO_BORDER
        cell.rowspan = total_rowspan
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }

    private fun createCellItextPdfWithColspanBoldCustomFonSize(content: String, total_colspan: Int, horizontalElement: Int, verticalElement: Int, isBold: Boolean, fontSize: Float): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, fontSize, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, fontSize)
        }

        val cell = PdfPCell(Phrase(content, font))
        cell.border = Rectangle.NO_BORDER
        cell.colspan = total_colspan
        cell.isNoWrap = false
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }

    private fun createCellItextPdfWithColspanBoldFonSize(content: String, total_colspan: Int, horizontalElement: Int, verticalElement: Int, isBold: Boolean, fontSize: Float): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, fontSize, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, fontSize)
        }

        val cell = PdfPCell(Phrase(content, font))
        cell.border = Rectangle.NO_BORDER
        cell.colspan = total_colspan
        cell.isNoWrap = false
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }

    private fun createCellItextPdfWithColspanBold(content: String, total_colspan: Int, horizontalElement: Int, verticalElement: Int, isBold: Boolean): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, 12f)
        }

        val cell = PdfPCell(Phrase(content, font))
        cell.border = Rectangle.NO_BORDER
        cell.colspan = total_colspan
        cell.isNoWrap = false
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }

    private fun createCellItextPdfWithColspanBoldCustomHeightCell(content: String, total_colspan: Int, horizontalElement: Int, verticalElement: Int, isBold: Boolean, heightColumn: Float): PdfPCell {
        val font: Font? = if (isBold) {
            Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD)
        } else {
            Font(Font.FontFamily.HELVETICA, 12f)
        }

        val cell = PdfPCell(Phrase(content, font))
        cell.border = Rectangle.NO_BORDER
        cell.colspan = total_colspan
        cell.isNoWrap = false
        cell.fixedHeight = heightColumn
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }


    @SuppressLint("SdCardPath")
    fun openFile(filename: String) {

        val outputExportExcelFile = File(Environment.getExternalStorageDirectory().toString() + File.separator + "SIOJT" + File.separator + "Downloads", filename)
        //val path = Uri.fromFile(outputExportExcelFile)
        val path = FileProvider.getUriForFile(activity, activity.packageName, outputExportExcelFile)
        val intent = Intent(Intent.ACTION_VIEW)
        // Check what kind of file you are trying to open, by comparing the url with extensions.
        // When the if condition is matched, plugin sets the correct intent (mime) type,
        // so Android knew what application to use to open the file
        if (filename.contains(".doc") || filename.contains(".docx")) {
            // Word document
            intent.setDataAndType(path, "application/msword")
        } else if (filename.contains(".pdf")) {
            // PDF file
            intent.setDataAndType(path, "application/pdf")
        } else if (filename.contains(".ppt") || filename.contains(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(path, "application/vnd.ms-powerpoint")
        } else if (filename.contains(".xls") || filename.contains(".xlsx")) {
            // Excel file
            intent.setDataAndType(path, "application/vnd.ms-excel")
        } else if (filename.contains(".zip") || filename.contains(".rar")) {
            // WAV audio file
            intent.setDataAndType(path, "application/x-wav")
        } else if (filename.contains(".rtf")) {
            // RTF file
            intent.setDataAndType(path, "application/rtf")
        } else if (filename.contains(".wav") || filename.contains(".mp3")) {
            // WAV audio file
            intent.setDataAndType(path, "audio/x-wav")
        } else if (filename.contains(".gif")) {
            // GIF file
            intent.setDataAndType(path, "image/gif")
        } else if (filename.contains(".jpg") || filename.contains(".jpeg") || filename.contains(".png")) {
            // JPG file
            intent.setDataAndType(path, "image/jpeg")
        } else if (filename.contains(".txt")) {
            // Text file
            intent.setDataAndType(path, "text/plain")
        } else if (filename.contains(".3gp") || filename.contains(".mpg") || filename.contains(".mpeg") || filename.contains(".mpe") || filename.contains(".mp4") || filename.contains(".avi")) {
            // Video files
            intent.setDataAndType(path, "video/*")
        } else {
            //if you want you can also define the intent type for any other file

            //additionally use else clause below, to manage other unknown extensions
            //in this case, Android will show all applications installed on the device
            //so you can choose which application to use
            intent.setDataAndType(path, "*/*")
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION

        try {
            activity.startActivity(Intent.createChooser(intent, "Pilih aplikasi"))
        } catch (e: ActivityNotFoundException) {
            //Toast.makeText(this@Detailsurat_masuk_activity, "No Application available to view PDF", Toast.LENGTH_SHORT).show()
            Toast.makeText(activity, "Tidak ada aplikasi yang tersedia untuk membuka file", Toast.LENGTH_SHORT).show()
        }
    }
}
