package mki.siojt2.ui.activity_daftar_ahli_waris.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface DaftarAhliWarisMVVPPresenter : MVPPresenter {

    fun getdaftarahliWaris(projectId: Int, isFirstParty: Int, landId: Int, accessToken: String)
}