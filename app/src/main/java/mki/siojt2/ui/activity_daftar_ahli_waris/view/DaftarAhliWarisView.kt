package mki.siojt2.ui.activity_daftar_ahli_waris.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.ResponseDataListPemilik

/**
 * Created by iqbal on 10/01/18.
 */
interface DaftarAhliWarisView : MvpView {
    fun onsuccessgetlistdataAhliWaris(responseDataListPemilikKedua: MutableList<ResponseDataListPemilik>)
}