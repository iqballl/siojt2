package mki.siojt2.ui.activity_daftar_ahli_waris.presenter

import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.ui.activity_daftar_ahli_waris.view.DaftarAhliWarisView
import mki.siojt2.utils.ErrorHandler
import mki.siojt2.utils.rxJava.RxUtils

class DaftarAhliWarisPresenter : BasePresenter(), DaftarAhliWarisMVVPPresenter {

    private var disposables: CompositeDisposable = CompositeDisposable()

    override fun getdaftarahliWaris(projectId: Int, isFirstParty: Int, landId: Int, accessToken: String) {
        view().onShowLoading()
        disposables.add(
                dataManager.getlistParties2(projectId, isFirstParty, landId, accessToken)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetlistdataAhliWaris(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    private fun view(): DaftarAhliWarisView {
        return getView() as DaftarAhliWarisView
    }
}