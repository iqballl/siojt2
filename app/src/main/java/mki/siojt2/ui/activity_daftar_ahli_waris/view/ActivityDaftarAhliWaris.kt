package mki.siojt2.ui.activity_daftar_ahli_waris.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.widget.LinearLayout
import com.google.gson.Gson
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_daftar_tanah_kepemilikan_kedua.*
import kotlinx.android.synthetic.main.toolbar_with_sub_title.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.model.ResponDataObjekKepemilikanKedua
import mki.siojt2.model.ResponseDataListPemilik
import mki.siojt2.model.localsave.AhliWaris
import mki.siojt2.ui.activity_daftar_ahli_waris.presenter.DaftarAhliWarisPresenter
import mki.siojt2.ui.activity_daftar_ahli_waris.recycleview_adapter.AdapterDaftarAhliWaris
import mki.siojt2.ui.activity_form_ahli_waris.ActivityFormAhliWaris
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.Utils

class ActivityDaftarAhliWaris : BaseActivity(), DaftarAhliWarisView, AdapterDaftarAhliWaris.Callback {

    lateinit var daftarAhliWarisPresenter: DaftarAhliWarisPresenter

    private lateinit var adapterDaftarAhliWaris: AdapterDaftarAhliWaris

    private var datalisObjek: MutableList<ResponDataObjekKepemilikanKedua>? = ArrayList()

    private var mprojectLandId: Int? = 0
    private var mprojectLandIdSub: Int? = 0

    lateinit var realm: Realm

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityDaftarAhliWaris::class.java)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daftar_tanah_kepemilikan_kedua)

        val historyData = intent?.extras?.getString("data_current_tanah")
        val toJson = Gson().fromJson(historyData, Array<String>::class.java).toList()
        //val turnsType = object : TypeToken<ResponseDataListTanah>() {}.type
        //val dataconvert = Gson().fromJson<ResponseDataListTanah>(historyData, turnsType)

        mprojectLandId = toJson[0].toInt()
        mprojectLandIdSub = toJson[3].toInt()

        /* set toolbar*/
        if (toolbarCustom != null) {
            setSupportActionBar(toolbarCustom)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarCustom.setNavigationOnClickListener {
            finish()
        }

        tvToolbarTitle.text = toJson[2]
        tvToolbarSubTitle.text = "Ahli waris"
        txtbtnKepemilikanTanahKedua.text = "TAMBAH DATA AHLI WARIS"

        datalisObjek?.add(ResponDataObjekKepemilikanKedua("1", "Sdr. Yayan Sudrajat",
                "Sdr. Yayan Sudrajat", "Kel. Sindangparit Kec. Mandalajati Kab. Bandung"))
        datalisObjek?.add(ResponDataObjekKepemilikanKedua("2", "Sdr. Wawan Sudrajat",
                "Sdr. Tatang Sudrajat", "Kel. Cimahi Tengah"))
        datalisObjek?.add(ResponDataObjekKepemilikanKedua("2", "Sdr. Wawan Sudrajat",
                "Sdr. Tatang Sudrajat", "Kel. Cimahi Utara"))

        adapterDaftarAhliWaris = AdapterDaftarAhliWaris(this)
        adapterDaftarAhliWaris.setCallback(this)
        rvObjekKepemilikanKedua.let {
            it.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@ActivityDaftarAhliWaris, LinearLayout.VERTICAL, false)
            it.setHasFixedSize(true)
            it.itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()
            it.adapter = adapterDaftarAhliWaris
        }

        btnaddObjekTanahObjekKepemilikanKedua.setSafeOnClickListener {
            Utils.deletedataform1(this@ActivityDaftarAhliWaris)
            Utils.deletedataform2(this@ActivityDaftarAhliWaris)
            Utils.deletedataform3(this@ActivityDaftarAhliWaris)
            val intent = ActivityFormAhliWaris.getStartIntent(this@ActivityDaftarAhliWaris)
            intent.putExtra("data_current_tanah", historyData)
            startActivity(intent)
        }


        /* get data api */
        //getPresenter()?.getdaftarahliWaris(dataconvert.projectPartyProjectId!!, 2, dataconvert.projectLandId!!, Preference.accessToken)
        getDataDaftarTanah(mprojectLandId!!)
    }

    private fun getDataDaftarTanah(historyData: Int) {
        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        val results = realm.where(AhliWaris::class.java).equalTo("LandIdTemp", historyData).equalTo("LandIdTempSub", mprojectLandIdSub).findAll()

        if (results != null) {
            adapterDaftarAhliWaris.clear()
            adapterDaftarAhliWaris.addItems(results)
            adapterDaftarAhliWaris.notifyDataSetChanged()
        } else {
            Log.e("data", "kosong")
        }
    }

    private fun getPresenter(): DaftarAhliWarisPresenter? {
        daftarAhliWarisPresenter = DaftarAhliWarisPresenter()
        daftarAhliWarisPresenter.onAttach(this)
        return daftarAhliWarisPresenter
    }

    override fun onsuccessgetlistdataAhliWaris(responseDataListPemilikKedua: MutableList<ResponseDataListPemilik>) {
        //adapterDaftarAhliWaris.clear()
        //adapterDaftarAhliWaris.addItems(responseDataListPemilikKedua)
        getDataDaftarTanah(mprojectLandId!!)
    }

    override fun onRepoEmptyViewRetryClick() {

    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    override fun onRestart() {
        super.onRestart()
        //getPresenter()?.getdaftarahliWaris(mprojectPartyProjectId!!, 2, mprojectLandId!!, Preference.accessToken)
        getDataDaftarTanah(mprojectLandId!!)

    }

    override fun onDestroy() {
        super.onDestroy()
        getPresenter()?.onDetach()
    }

}