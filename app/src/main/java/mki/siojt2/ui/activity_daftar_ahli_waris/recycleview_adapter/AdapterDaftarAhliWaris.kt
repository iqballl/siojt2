package mki.siojt2.ui.activity_daftar_ahli_waris.recycleview_adapter

import android.annotation.SuppressLint
import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import io.realm.RealmResults
import kotlinx.android.synthetic.main.item_daftar_subjek_vertical_2.view.*
import mki.siojt2.R
import mki.siojt2.base.BaseViewHolder
import mki.siojt2.model.localsave.AhliWaris
import mki.siojt2.ui.activity_detail_ahliwaris.ActivityDetailAhliWaris
import mki.siojt2.utils.extension.OnSingleClickListener


class AdapterDaftarAhliWaris(context: Context) : RecyclerView.Adapter<BaseViewHolder>() {

    private val mContext: Context? = context
    val mDataset: MutableList<AhliWaris>? = ArrayList()
    var rowLayout = R.layout.item_daftar_subjek_vertical_2
    var emptyrowLayout = R.layout.item_empty_view
    var pos = 0

    private var mCallback: Callback? = null

    fun setCallback(callback: Callback) {
        mCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            VIEW_TYPE_NORMAL -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(rowLayout, parent, false))
            VIEW_TYPE_EMPTY -> EmptyViewHolder(
                    LayoutInflater.from(parent.context).inflate(R.layout.item_empty_view, parent, false))
            else -> EmptyViewHolder(LayoutInflater.from(parent.context).inflate(emptyrowLayout, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (mDataset != null && mDataset.isNotEmpty()) {
            VIEW_TYPE_NORMAL
        } else {
            VIEW_TYPE_EMPTY
        }
    }

    override fun getItemCount(): Int {
        return if (mDataset!!.isNotEmpty()) {
            mDataset.size
        } else {
            1
        }
    }

    /*inner class RuangViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var r_no: TextView = itemView.findViewById(R.id.rowNo) as TextView
        var r_tvTitle: TextView = itemView.findViewById(R.id.tvTitle) as TextView
        var r_tvContent1: TextView = itemView.findViewById(R.id.tvContent1) as TextView
        var r_tvContent2: TextView = itemView.findViewById(R.id.tvContent2) as TextView

    }*/

    fun clear() {
        this.mDataset!!.clear()
    }

    fun addItems(data: RealmResults<AhliWaris>) {
        this.mDataset!!.addAll(data)
        notifyDataSetChanged()
    }

    interface Callback {
        fun onRepoEmptyViewRetryClick()
    }

    inner class ViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }

        @SuppressLint("SetTextI18n")
        override fun onBind(position: Int) {
            super.onBind(position)

            val dataList = mDataset!![position]

            inflateData(dataList)

            //val pos = pos + position + 1
            itemView.tvnamaSubjek2.text = dataList.partyFullName
            itemView.tvalamat1Subjek2.text = "${dataList.partyVillageName} ${dataList.partyStreetName} RT ${dataList.partyRT} RW ${dataList.partyRW} No.${dataList.partyNumber}"
            itemView.tvalamat2Subjek2.text = "${dataList.partyDistrictName} ${dataList.partyRegencyName} ${dataList.partyProvinceName}"

            itemView.tvkodeposalamatSubjek2.text = dataList.partyPostalCode

            itemView.btndetailSelngkapnyaPemilikKedua.setOnClickListener(object : OnSingleClickListener() {
                override fun onSingleClick(v: View) {
                    val intent = ActivityDetailAhliWaris.getStartIntent(mContext!!)
                    intent.putExtra("data_current_ahliwaris", dataList.ahliWarisIdTemp.toString())
                    mContext.startActivity(intent)
                }
            })


        }

        private fun inflateData(dataList: AhliWaris) {

        }
    }

    inner class EmptyViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }


    }

    companion object {
        const val VIEW_TYPE_EMPTY = 0
        const val VIEW_TYPE_NORMAL = 1
    }
}
