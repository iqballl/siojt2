package mki.siojt2.ui.activity_daftar_sarana_pelengkap

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.toolbar_with_sub_title.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.model.ResponseDataListTanah

class ActivityDaftarSaranaPelengkap : BaseActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daftar_sarana_pelengkap)

        val historyData = intent?.extras?.getString("data_current_tanah")

        val turnsType = object : TypeToken<ResponseDataListTanah>() {}.type
        val dataconvert = Gson().fromJson<ResponseDataListTanah>(historyData, turnsType)

        /* set toolbar*/
        if (toolbarCustom != null) {
            setSupportActionBar(toolbarCustom)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarCustom.setNavigationOnClickListener {
            finish()
        }

        tvToolbarTitle.text = dataconvert.districtName
        tvToolbarSubTitle.text = "Daftar Objek Sarana Pelengkap"

    }

    override fun onFragmentAttached() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFragmentDetached(tag: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityDaftarSaranaPelengkap::class.java)
        }
    }
}