package mki.siojt2.ui.activity_help_assistance

import android.content.Context
import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_daftar_bantuan.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.ui.activity_pdf_viewer.ActivityPdfViewer

class ActivityHelpAssistance : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daftar_bantuan)

        /* set toolbar*/
        if (toolbarHelp != null) {
            setSupportActionBar(toolbarHelp)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarHelp.setNavigationOnClickListener {
            finish()
        }
        toolbarHelp.title = "Bantuan"

        lluserGuidline.setSafeOnClickListener {
            val intent = ActivityPdfViewer.getStartIntent(this)
            startActivity(intent)
        }

        var isopen = false
        clfaq1.setSafeOnClickListener {
            isopen = if(isopen) {
                ivfaq1.animate().rotation(00f).start()
                expandable_layout_faq1.collapse()
                false
            } else{
                ivfaq1.animate().rotation(90f).start()
                expandable_layout_faq1.toggle()
                true
            }
        }

        var isopen2 = false
        clfaq2.setSafeOnClickListener {
            isopen2 = if(isopen2) {
                ivfaq2.animate().rotation(00f).start()
                expandable_layout_faq2.collapse()
                false
            } else{
                ivfaq2.animate().rotation(90f).start()
                expandable_layout_faq2.toggle()
                true
            }
        }

        var isopen3 = false
        clfaq3.setSafeOnClickListener {
            isopen3 = if(isopen3) {
                ivfaq3.animate().rotation(00f).start()
                expandable_layout_faq3.collapse()
                false
            } else{
                ivfaq3.animate().rotation(90f).start()
                expandable_layout_faq3.toggle()
                true
            }
        }

        var isopen4 = false
        clfaq4.setSafeOnClickListener {
            isopen4 = if(isopen4) {
                ivfaq4.animate().rotation(00f).start()
                expandable_layout_faq4.collapse()
                false
            } else{
                ivfaq4.animate().rotation(90f).start()
                expandable_layout_faq4.toggle()
                true
            }
        }
    }

    override fun onFragmentAttached() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFragmentDetached(tag: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityHelpAssistance::class.java)
        }
    }
}