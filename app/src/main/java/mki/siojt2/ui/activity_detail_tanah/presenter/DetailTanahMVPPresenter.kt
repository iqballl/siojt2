package mki.siojt2.ui.activity_detail_tanah.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface DetailTanahMVPPresenter: MVPPresenter {
    fun getdatadetaiLTanah(projectLandId: Int, accessToken: String)
}