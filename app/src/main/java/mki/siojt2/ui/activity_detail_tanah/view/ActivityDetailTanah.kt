package mki.siojt2.ui.activity_detail_tanah.view

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.appcompat.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.google.gson.Gson
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_detail_tanah.*
import kotlinx.android.synthetic.main.dialog_confirm_delete.view.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.fragment.detail_jenis_objek.tanah.JenisMainObjekTanah
import mki.siojt2.model.data_detail_tanah.ResponseDataDetailTanah2
import mki.siojt2.model.local_save_image.DataImageBuilding
import mki.siojt2.model.local_save_image.DataImageFacilities
import mki.siojt2.model.local_save_image.DataImageLand
import mki.siojt2.model.local_save_image.DataImagePlants
import mki.siojt2.model.localsave.*
import mki.siojt2.ui.activity_detail_tanah.DialogAddObjekLain
import mki.siojt2.ui.activity_detail_tanah.DialogPilihKategoriObjek
import mki.siojt2.ui.activity_detail_tanah.presenter.DetailTanahPresenter
import mki.siojt2.ui.activity_form_add_objek_tanah.view.ActicityFormAddObjekTanah
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.extension.removeFragment

class ActivityDetailTanah : BaseActivity(), DetailTanahView, DialogPilihKategoriObjek.OnChangeFragmentListener {

    var codeKategoriObjek: Int? = 1
    var nameKategoriObjek: String? = "Tanah"

    lateinit var detailTanahPresenter: DetailTanahPresenter

    private var dataDetailTanah: ResponseDataDetailTanah2? = null
    lateinit var session: Session
    lateinit var realm: Realm

    private lateinit var mdialogConfirmDelete: AlertDialog
    private lateinit var mDialogView: View

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityDetailTanah::class.java)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_tanah)

        session = Session(this)
        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        val historyData = intent?.extras?.getString("data_current_tanah")

        //val turnsType = object : TypeToken<ResponseDataListTanah>() {}.type
        //val dataconvert = Gson().fromJson<ResponseDataListTanah>(historyData, turnsType)

        //Log.d("anjing", dataconvert.projectPartyId.toString())

        /*codeKategoriObjek = intent.getIntExtra("jenis_kategori", 0)
        nameKategoriObjek = intent.getStringExtra("jenis_name_kategori")*/

        val toJson = Gson().fromJson(historyData, Array<String>::class.java).toList()

        /* set toolbar*/
        if (toolbarCustomDetailObjek != null) {
            setSupportActionBar(toolbarCustomDetailObjek)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        //tvFormToolbarTitle.text = "Tambah data objek - Tanah"

        toolbarCustomDetailObjek.setNavigationOnClickListener {
            finish()
        }

        /* set dialog confirm delete data */
        mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_confirm_delete, null)
        val mBuilder = AlertDialog.Builder(this)
        mdialogConfirmDelete = mBuilder.create()

        mdialogConfirmDelete.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        mdialogConfirmDelete.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mdialogConfirmDelete.setCancelable(false)
        val window = mdialogConfirmDelete.window
        if (window != null) {
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            mdialogConfirmDelete.window!!.setLayout(width, height)
        }
        mdialogConfirmDelete.setView(mDialogView)

        /* set current menu*/
        tvToolbarTitleCustomDetailObjek.text = toJson[2]
        tvcurrentMenuDetailKategori.text = nameKategoriObjek

        /* set current fragment*/
        setFragment(JenisMainObjekTanah.newInstance(toJson[0].toInt(), toJson[1].toInt()), JenisMainObjekTanah.TAG)
        /* when (codeKategoriObjek) {
             1 ->{
                 setFragment(JenisMainObjekTanah.newInstance("", ""), JenisMainObjekTanah.TAG)
             }
             2 -> {
                 setFragment(JenisMainObjekBangunan.newInstance("", ""), JenisMainObjekBangunan.TAG)
             }
             3 -> {
                 setFragment(JenisObjekTanaman.newInstance("", ""), JenisMainObjekBangunan.TAG)
             }
             4 -> {
                 setFragment(JenisObjekMesinPerlatan.newInstance("", ""), JenisObjekMesinPerlatan.TAG)
             }
         }*/

        btnaddkategoriTanah.setOnClickListener {
            val dialogPopup = DialogAddObjekLain()
            dialogPopup.show(supportFragmentManager, "Dalog_popup")
        }

        cardPilihKategoriObject.setOnClickListener {
            try {
                val dialogPopup = DialogPilihKategoriObjek.newInstance(codeKategoriObjek!!, nameKategoriObjek!!)

                val transaction = (this@ActivityDetailTanah as androidx.fragment.app.FragmentActivity)
                        .supportFragmentManager
                        .beginTransaction()

                dialogPopup.show(transaction, "dialog_menu")

            } catch (e: Exception) {
                Log.e("ClickMenuObject", "exception", e)
            }


            /* val dialogPopup = DialogPilihKategoriObjek()
             dialogPopup.show(supportFragmentManager, "Dalog_popup")*/

        }

        /* update data */
        cardEdit.setOnClickListener {
            //id, projectid, districname
            val intent = ActicityFormAddObjekTanah.getStartIntent(this@ActivityDetailTanah)
            val mSession = mki.siojt2.model.Session(toJson[0].toInt(), null,"2")
            intent.putExtra("param_to_form_tanah", historyData)
            intent.putExtra("session_to_form_tanah", Gson().toJson(mSession))
            session.setIdAndStatus(toJson[0], "2", "")
            startActivity(intent)
        }

        /* delete data */
        cardDelete.setOnClickListener {
            mDialogView.btnYesDelete.setOnClickListener {
                realm.executeTransactionAsync({ inRealm ->
                    val resultsRealmTanah = inRealm.where(ProjectLand::class.java).findAll()
                    val dataLandById = resultsRealmTanah.where().equalTo("LandIdTemp", toJson[0].toInt()).findFirst()
                    val resultdataImageLand = inRealm.where(DataImageLand::class.java).findAll()
                    val dataimageLand = resultdataImageLand.where().equalTo("imageId", toJson[0].toInt()).findAll()

                    /* bangunan */
                    val dataBanguanan = inRealm.where(Bangunan::class.java)
                            .equalTo("LandIdTemp", dataLandById!!.landIdTemp.toInt())
                            .findAll()

                    /* tanaman */
                    val dataTanaman= inRealm.where(Tanaman::class.java)
                            .equalTo("LandIdTemp", dataLandById.landIdTemp.toInt())
                            .findAll()

                    /* saranaPlengkap */
                    val dataSaranaPelengkap= inRealm.where(SaranaLain::class.java)
                            .equalTo("LandIdTemp", dataLandById.landIdTemp.toInt())
                            .findAll()

                    for(i in 0 until dataSaranaPelengkap.size){
                        val resultdataImagePlants = inRealm.where(DataImageFacilities::class.java)
                                .equalTo("imageId", dataSaranaPelengkap[i]!!.saranaIdTemp)
                                .findAll()

                        resultdataImagePlants.deleteAllFromRealm()
                    }

                    for(i in 0 until dataTanaman.size){
                        val resultdataImagePlants = inRealm.where(DataImagePlants::class.java)
                                .equalTo("imageId", dataTanaman[i]!!.tanamanIdTemp)
                                .findAll()

                        resultdataImagePlants.deleteAllFromRealm()
                    }

                    for(i in 0 until dataBanguanan.size){
                        val resultdataImageFacilities = inRealm.where(DataImageBuilding::class.java)
                                .equalTo("imageId", dataBanguanan[i]!!.bangunanIdTemp)
                                .findAll()

                        resultdataImageFacilities.deleteAllFromRealm()
                    }

                    dataBanguanan.deleteAllFromRealm()
                    dataTanaman.deleteAllFromRealm()
                    dataSaranaPelengkap.deleteAllFromRealm()

                    dataimageLand?.deleteAllFromRealm()
                    dataLandById.deleteFromRealm()
                }, {
                    Log.d("delete", "onSuccess : delete single object")
                    mdialogConfirmDelete.dismiss()
                    finish()
                }, {
                    Log.d("delete", "onFailed : ${it.localizedMessage}")
                    Log.d("delete", "onFailed : delete single object")
                })
            }

            mDialogView.btnNoDelete.setOnClickListener {
                mdialogConfirmDelete.dismiss()
            }
            mdialogConfirmDelete.show()
            /*val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                        val deleted = realm.where(ProjectLand::class.java).equalTo("LandIdTemp", toJson[0].toInt()).findFirst()
                        realm.beginTransaction()
                        deleted!!.deleteFromRealm()
                        realm.commitTransaction()
                        dialog.dismiss()
                        finish()
                    }

                    DialogInterface.BUTTON_NEGATIVE -> {
                        dialog.dismiss()
                    }
                }
            }

            val builder = android.support.v7.app.AlertDialog.Builder(this)
            builder.setTitle("SIOJT")
                    .setMessage("Yakin akan dihapus?")
                    .setPositiveButton("Ya", dialogClickListener)
                    .setNegativeButton("Tidak", dialogClickListener)
                    .setIcon(ContextCompat.getDrawable(this@ActivityDetailTanah, R.drawable.ic_logo_splash)!!)
                    .show()*/
        }
        //getPresenter()?.getdatadetaiLTanah(dataconvert.projectLandId!!, dataconvert.projectPartyId!!, Preference.accessToken)
    }

    private fun getPresenter(): DetailTanahPresenter? {
        detailTanahPresenter = DetailTanahPresenter()
        detailTanahPresenter.onAttach(this)
        return detailTanahPresenter
    }

    private fun setFragment(fragment: androidx.fragment.app.Fragment?, fragmentTag: String) {

        if (fragment != null) {
            refreshBackStack()
            val transact = supportFragmentManager.beginTransaction()
            transact.setCustomAnimations(R.anim.fragment_enter_from_right, R.anim.fragment_exit_to_left, R.anim.fragment_enter_from_left, R.anim.fragment_exit_to_right)
            transact.replace(R.id.flkategoriObjek, fragment, fragmentTag).commit()
        }

    }

    private fun refreshBackStack() {
        supportFragmentManager.popBackStack(null, androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    override fun onsuccesgetdatadetaiLTanah(responseDataDetailTanah: ResponseDataDetailTanah2) {

        this.dataDetailTanah = responseDataDetailTanah

    }

    override fun onSendFragment(fragment: androidx.fragment.app.Fragment, fragmentTag: String, codestatusCurrent: Int, namestatusCurrent: String) {
        setFragment(fragment, fragmentTag)
        codeKategoriObjek = codestatusCurrent
        nameKategoriObjek = namestatusCurrent
        tvcurrentMenuDetailKategori.text = namestatusCurrent
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {
        supportFragmentManager.removeFragment(tag = tag)
    }
}