package mki.siojt2.ui.activity_detail_tanah

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import mki.siojt2.R
import androidx.appcompat.app.AlertDialog
import android.view.*
import android.widget.Toast
import android.content.Context
import mki.siojt2.constant.Constant
import mki.siojt2.fragment.detail_jenis_objek.bangunan.JenisMainObjekBangunan
import mki.siojt2.fragment.detail_jenis_objek.tanah.JenisMainObjekTanah
import mki.siojt2.fragment.detail_jenis_objek.JenisObjekTanaman
import mki.siojt2.fragment.detail_jenis_objek.mesin.JenisObjekMesinPerlatan
import android.graphics.Paint.STRIKE_THRU_TEXT_FLAG
import android.graphics.Paint
import android.util.Log
import kotlinx.android.synthetic.main.item_ubah_menu_objek_2.view.*


class DialogPilihKategoriObjek : androidx.fragment.app.DialogFragment() {

    lateinit var mView: View
    private val TAG = "MyCustomDialog"
    private var mListener: OnChangeFragmentListener? = null

    private var statusCurrentMenu: Int? = null
    private var statusCurrentMenuName: String? = null

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_PARAM1 = "statusCurrentMenu"
        private const val ARG_PARAM2 = "statusCurrentMenuName"
        internal const val TAG = "DialogPilihKategoriObjek"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: Int, param2: String): DialogPilihKategoriObjek {
            val fragment = DialogPilihKategoriObjek()
            val args = Bundle()
            args.putInt(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            val mArgs = arguments
            statusCurrentMenu = mArgs?.getInt(ARG_PARAM1)
            statusCurrentMenuName = mArgs?.getString(ARG_PARAM2)

            Log.d("menuObjek", statusCurrentMenu.toString())
        }

    }

    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val dialog = super.onCreateDialog(savedInstanceState)

        val dialogBuilder = AlertDialog.Builder(activity!!)
        val inflater = LayoutInflater.from(context)
        val dialogView = inflater.inflate(R.layout.item_ubah_menu_objek_2, null)
        dialogBuilder.setView(dialogView)

        initSubViews(dialogView)

        // request a window without the title
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        return dialogBuilder.create()
    }


    private fun initSubViews(rootView: View) {
        //val tvnamecurrentMenu = rootView.tvnamecurrentMenu
        val btncloseDialog = rootView.ivcloseDialog

        val btngotoTanah = rootView.cardTanah
        val txtTanah = rootView.tvTanah

        val btngotoBangunan = rootView.cardBangunan
        val txtBangunan = rootView.tvBangunan

        val btngotoTanaman = rootView.cardTanaman
        val txtTanaman = rootView.tvTanaman

        val btngotoMesin = rootView.cardMesin
        val txtMesin = rootView.tvMesin

        //tvnamecurrentMenu.text = statusCurrentMenuName


        when (statusCurrentMenu) {
            1 -> {
                btngotoTanah.isEnabled = false
                btngotoBangunan.isEnabled = true
                btngotoTanaman.isEnabled = true
                btngotoMesin.isEnabled = true
                txtTanah.paintFlags = txtTanah.paintFlags or STRIKE_THRU_TEXT_FLAG
                txtBangunan.paintFlags = txtBangunan.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtTanaman.paintFlags = txtTanaman.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtMesin.paintFlags = txtMesin.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()

            }
            2 -> {
                btngotoTanah.isEnabled = true
                btngotoBangunan.isEnabled = false
                btngotoTanaman.isEnabled = true
                btngotoMesin.isEnabled = true
                txtTanah.paintFlags = txtTanah.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtBangunan.paintFlags = txtBangunan.paintFlags or STRIKE_THRU_TEXT_FLAG
                txtTanaman.paintFlags = txtTanaman.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtMesin.paintFlags = txtMesin.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
            }
            3 -> {
                btngotoTanah.isEnabled = true
                btngotoBangunan.isEnabled = true
                btngotoTanaman.isEnabled = false
                btngotoMesin.isEnabled = true
                txtTanah.paintFlags = txtTanah.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtBangunan.paintFlags = txtBangunan.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtTanaman.paintFlags = txtTanaman.paintFlags or STRIKE_THRU_TEXT_FLAG
                txtMesin.paintFlags = txtMesin.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
            }
            else -> {
                btngotoTanah.isEnabled = true
                btngotoBangunan.isEnabled = true
                btngotoTanaman.isEnabled = true
                btngotoMesin.isEnabled = false
                txtTanah.paintFlags = txtTanah.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtBangunan.paintFlags = txtBangunan.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtTanaman.paintFlags = txtTanaman.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtMesin.paintFlags = txtMesin.paintFlags or STRIKE_THRU_TEXT_FLAG
            }
        }


        btncloseDialog.setOnClickListener {
            dismiss()
        }


        btngotoTanah.setOnClickListener {
            if (mListener != null) {
                statusCurrentMenu = 1
                statusCurrentMenuName = Constant.MENUOBJEK1
                mListener!!.onSendFragment(
                        JenisMainObjekTanah.newInstance(0, 0),
                        JenisMainObjekTanah.TAG,
                        statusCurrentMenu!!,
                        statusCurrentMenuName!!
                )
            }
            dialog!!.dismiss()
        }

        btngotoBangunan.setOnClickListener {
            if (mListener != null) {
                statusCurrentMenu = 2
                statusCurrentMenuName = Constant.MENUOBJEK2
                mListener!!.onSendFragment(
                        JenisMainObjekBangunan.newInstance("", ""),
                        JenisMainObjekBangunan.TAG,
                        statusCurrentMenu!!,
                        statusCurrentMenuName!!
                )
            }
            dialog!!.dismiss()
        }

        btngotoTanaman.setOnClickListener {
            if (mListener != null) {
                statusCurrentMenu = 3
                statusCurrentMenuName = Constant.MENUOBJEK3
                mListener!!.onSendFragment(
                        JenisObjekTanaman.newInstance("", ""),
                        JenisObjekTanaman.TAG,
                        statusCurrentMenu!!,
                        statusCurrentMenuName!!
                )
            }
            dialog!!.dismiss()
        }


        btngotoMesin.setOnClickListener {
            if (mListener != null) {
                statusCurrentMenu = 4
                statusCurrentMenuName = Constant.MENUOBJEK4
                mListener!!.onSendFragment(
                        JenisObjekMesinPerlatan.newInstance("", ""),
                        JenisObjekMesinPerlatan.TAG,
                        statusCurrentMenu!!,
                        statusCurrentMenuName!!
                )
            }
            dialog!!.dismiss()
        }

        /* val btngotoaddBangunan = rootView.cardBangunan
         val btngotoaddTanaman = rootView.cardTanaman
         val btngotoaddMesin = rootView.cardMesin

         btngotoaddBangunan.setOnClickListener(object : OnSingleClickListener() {
             override fun onSingleClick(v: View) {
                 dismiss()
                 val intent = ActicityFormAddObjekBangunan.getStartIntent(activity!!)
                 activity!!.startActivity(intent)
             }

         })

         btngotoaddTanaman.setOnClickListener(object : OnSingleClickListener() {
             override fun onSingleClick(v: View) {
                 dismiss()
                 val intent = ActicityFormAddObjekTanaman.getStartIntent(activity!!)
                 activity!!.startActivity(intent)
             }

         })

         btngotoaddMesin.setOnClickListener(object : OnSingleClickListener() {
             override fun onSingleClick(v: View) {
                 dismiss()
                 val intent = ActicityFormAddObjekLain.getStartIntent(activity!!)
                 activity!!.startActivity(intent)
             }

         })
 */
    }

    fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    override fun onStart() {
        super.onStart()

        //set transparent background
        val window = dialog!!.window
        if (window != null) {
            val width = WindowManager.LayoutParams.MATCH_PARENT
            val height = WindowManager.LayoutParams.MATCH_PARENT
            dialog!!.window!!.setLayout(width, height)
        }
        window!!.setBackgroundDrawableResource(android.R.color.transparent)

        //disable buttons from dialog
        val alertDialog = dialog as AlertDialog
        alertDialog.getButton(Dialog.BUTTON_POSITIVE).isEnabled = false
        alertDialog.getButton(Dialog.BUTTON_NEGATIVE).isEnabled = false
        alertDialog.getButton(Dialog.BUTTON_NEUTRAL).isEnabled = false
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            mListener = activity as OnChangeFragmentListener?
        } catch (e: ClassCastException) {
            //Log.e(TAG, "onAttach: ClassCastException: " + e.message)
            throw ClassCastException("Calling Fragment must implement OnChangeFragmentListener : ${e.message}")
        }

    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }


    interface OnChangeFragmentListener {
        //void onFragmentInteraction(Uri uri);
        fun onSendFragment(fragment: androidx.fragment.app.Fragment, fragmentTag: String, codestatusCurrent: Int, namestatusCurrent: String)
    }


}