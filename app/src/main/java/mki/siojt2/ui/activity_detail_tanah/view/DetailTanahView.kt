package mki.siojt2.ui.activity_detail_tanah.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.data_detail_tanah.ResponseDataDetailTanah2

interface DetailTanahView: MvpView {

    fun onsuccesgetdatadetaiLTanah(responseDataDetailTanah: ResponseDataDetailTanah2)
}