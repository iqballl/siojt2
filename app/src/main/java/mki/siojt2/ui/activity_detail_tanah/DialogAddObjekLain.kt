package mki.siojt2.ui.activity_detail_tanah

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import mki.siojt2.R
import androidx.appcompat.app.AlertDialog
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import kotlinx.android.synthetic.main.item_tambah_kategori_objek.view.*
import mki.siojt2.ui.activity_form_add_objek_lain.view.ActicityFormAddObjekLain
import mki.siojt2.ui.activity_form_add_objek_tanaman.ActivityFormAddObjekTanaman
import mki.siojt2.ui.activity_form_add_bangunan.view.ActicityFormAddObjekBangunan
import mki.siojt2.utils.extension.OnSingleClickListener


class DialogAddObjekLain : androidx.fragment.app.DialogFragment() {

    lateinit var mView: View

    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val dialog = super.onCreateDialog(savedInstanceState)

        val dialogBuilder = AlertDialog.Builder(activity!!)
        val inflater = LayoutInflater.from(context)
        val dialogView = inflater.inflate(R.layout.item_tambah_kategori_objek, null)
        dialogBuilder.setView(dialogView)

        initSubViews(dialogView)


        // request a window without the title
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        return dialogBuilder.create()
    }


    private fun initSubViews(rootView: View) {

        val btngotoaddBangunan = rootView.cardBangunan
        val btngotoaddTanaman = rootView.cardTanaman
        val btngotoaddMesin = rootView.cardMesin

        btngotoaddBangunan.setOnClickListener(object : OnSingleClickListener() {
            override fun onSingleClick(v: View) {
                dismiss()
                val intent = ActicityFormAddObjekBangunan.getStartIntent(activity!!)
                activity!!.startActivity(intent)
            }

        })

        btngotoaddTanaman.setOnClickListener(object : OnSingleClickListener() {
            override fun onSingleClick(v: View) {
                dismiss()
                val intent = ActivityFormAddObjekTanaman.getStartIntent(activity!!)
                activity!!.startActivity(intent)
            }

        })

        btngotoaddMesin.setOnClickListener(object : OnSingleClickListener() {
            override fun onSingleClick(v: View) {
                dismiss()
                val intent = ActicityFormAddObjekLain.getStartIntent(activity!!)
                activity!!.startActivity(intent)
            }

        })

    }

    fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }


    override fun onStart() {
        super.onStart()

        //set transparent background
        val window = dialog!!.window
        window!!.setBackgroundDrawableResource(android.R.color.transparent)

        //disable buttons from dialog
        val alertDialog = dialog as AlertDialog
        alertDialog.getButton(Dialog.BUTTON_POSITIVE).isEnabled = false
        alertDialog.getButton(Dialog.BUTTON_NEGATIVE).isEnabled = false
        alertDialog.getButton(Dialog.BUTTON_NEUTRAL).isEnabled = false
    }

}