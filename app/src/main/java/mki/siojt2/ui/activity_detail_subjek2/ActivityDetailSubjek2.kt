package mki.siojt2.ui.activity_detail_subjek2

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import com.google.gson.Gson
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_detail_subjek.*
import kotlinx.android.synthetic.main.dialog_confirm_delete.view.*
import kotlinx.android.synthetic.main.toolbar_main.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.model.ResponseDataDetailPemilik
import mki.siojt2.model.localsave.AhliWaris
import mki.siojt2.model.localsave.Session
import mki.siojt2.model.localsave.Subjek2
import mki.siojt2.ui.activity_detail_subjek.presenter.DetailSubjekPresenter
import mki.siojt2.ui.activity_detail_subjek.view.DetailSubjekView
import mki.siojt2.ui.activity_form_subjek_2.ActivityFormSubjek2
import mki.siojt2.utils.realm.RealmController

class ActivityDetailSubjek2 : BaseActivity(), DetailSubjekView {

    private lateinit var presenter: DetailSubjekPresenter
    private lateinit var session: Session
    private lateinit var realm: Realm

    private lateinit var mdialogConfirmDelete: AlertDialog
    private lateinit var mDialogView: View

    private var msubjekTempId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_subjek)

        session = Session(this)

        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        val dataPassing = intent?.extras?.getInt("data_current_pemilik2")
        msubjekTempId = dataPassing
        //val toJson = Gson().fromJson(historyData, Array<String>::class.java).toList()

        //val turnsType = object : TypeToken<ResponseDataListPemilik>() {}.type
        //val dataconvert = Gson().fromJson<ResponseDataListPemilik>(historyData, turnsType)

        /* set dialog confirm delete data */
        mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_confirm_delete, null)
        val mBuilder = AlertDialog.Builder(this)
        mdialogConfirmDelete = mBuilder.create()

        mdialogConfirmDelete.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        mdialogConfirmDelete.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mdialogConfirmDelete.setCancelable(false)
        val window = mdialogConfirmDelete.window
        if (window != null) {
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            mdialogConfirmDelete.window!!.setLayout(width, height)
        }
        mdialogConfirmDelete.setView(mDialogView)

        /* set toolbar */
        if (toolbarCustom != null) {
            setSupportActionBar(toolbarCustom)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarCustom.setNavigationOnClickListener {
            finish()
        }

        //tvToolbarTitle.text = "Detail"
        tvToolbarTitle.setTextColor(Color.parseColor("#3DC528"))

        btnshowDaftarObjek.setOnClickListener {
            finish()
        }

        getdatapemilik2()
        //getPresenter()?.getdetailSubjek(dataconvert.partyId!!, Preference.accessToken)
    }

    private fun getdatapemilik2() {
        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        val results = realm.where(Subjek2::class.java).equalTo("SubjekIdTemp", msubjekTempId).findFirst()
        if (results != null) {
            showData(results)
            Log.d("results", results.toString())
        } else {
            Log.e("data", "kosong")
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showData(data: Subjek2) {
        tvToolbarTitle.text = data.subjek2FullName.toString()
        tvNIK.text = if(!data.subjek2IdentityNumber.isNullOrEmpty()) {data.subjek2IdentityNumber.toString()} else "-"
        tvName.text = if(!data.subjek2FullName.isNullOrEmpty()) {data.subjek2FullName.toString()} else "-"
        tvPhone.text = if(!data.subjek2Number.isNullOrEmpty()) {data.subjek2Number.toString()} else "-"
        tvBirthplace.text = if(!data.subjek2BirthPlaceName.isNullOrEmpty()) {data.subjek2BirthPlaceName.toString()} else "-"
        tvBirthdate.text = if(!data.subjek2BirthDate.isNullOrEmpty()) {data.subjek2BirthDate.toString()} else "-"
        tvJob.text = if(!data.subjek2OccupationName.isNullOrEmpty()) {data.subjek2OccupationName.toString()} else "-"
        tvProvince.text = if(!data.subjek2ProvinceName.isNullOrEmpty()) {data.subjek2ProvinceName.toString()} else "-"
        tvCity.text = if(!data.subjek2RegencyName.isNullOrEmpty()) {data.subjek2RegencyName.toString()} else "-"
        tvDistrict.text = if(!data.subjek2DistrictName.isNullOrEmpty()) {data.subjek2DistrictName.toString()} else "-"
        tvVillage.text = if(!data.subjek2VillageName.isNullOrEmpty()) {data.subjek2VillageName.toString()} else "-"
        tvBlock.text = if(!data.subjek2Block.isNullOrEmpty()) {data.subjek2Block.toString()} else "-"
        tvKomplek.text = if(!data.subjek2ComplexName.isNullOrEmpty()) {data.subjek2ComplexName.toString()} else "-"
        tvNo.text = if(!data.subjek2Number.isNullOrEmpty()) {data.subjek2Number.toString()} else "-"
        tvStreet.text = if(!data.subjek2StreetName.isNullOrEmpty()) {data.subjek2StreetName.toString()} else "-"
        tvRT.text = if(!data.subjek2RT.isNullOrEmpty()) {data.subjek2RT.toString()} else "-"
        tvRW.text = if(!data.subjek2RW.isNullOrEmpty()) {data.subjek2RW.toString()} else "-"
        tvPostalCode.text = if(!data.subjek2PostalCode.isNullOrEmpty()) {data.subjek2PostalCode.toString()} else "-"
        tvNPWP.text = if(!data.subjek2NPWP.isNullOrEmpty()) {data.subjek2NPWP.toString()} else "-"
        tvDomisili.text = if(data.subjek2YearStayBegin != null) {data.subjek2YearStayBegin.toString()} else "-"
        //convertListtoJSON
//        val liveName = data.livelihoodLiveName
//        val replace1 = liveName.replace("[", "['")
//        val replace2 = replace1.replace("]", "']")
//        val replace3 = replace2.replace(",", "','")
//
//        val liveNameOther = data.livelihoodOther
//        val liveNameOther2 = liveNameOther.replace("[", "['")
//        val liveNameOther3 = liveNameOther2.replace("]", "']")
//        val liveNameOther4 = liveNameOther3.replace(",", "','")
//
//        val liveIncome = data.livelihoodIncome
//        val replac1 = liveIncome.replace("[", "['")
//        val replac2 = replac1.replace("]", "']")
//        val replac3 = replac2.replace(",", "','")
//
//        val gson = Gson()
//        val listNameAr = gson.fromJson(replace3, Array<String>::class.java).toList()
//        val listNameArOther = gson.fromJson(liveNameOther4, Array<String>::class.java).toList()
//        val listIncomeAr = gson.fromJson(replac3, Array<String>::class.java).toList()
//        if (listNameAr.isNotEmpty()) {
//            for (i in listNameAr.indices) {
//                val child = layoutInflater.inflate(R.layout.item_detail_jenis_usaha_vertical, null)
//                val jenisUsaha = child.findViewById(R.id.tvjenisUsaha) as com.pixplicity.fontview.FontTextView
//                val penghasilanUsaha = child.findViewById(R.id.tvPenghasilan) as com.pixplicity.fontview.FontTextView
//
//                if (listNameArOther[i].isNotEmpty()) {
//                    jenisUsaha.text = listNameArOther[i]
//                    penghasilanUsaha.text = "Rp ${listIncomeAr[i]}"
//                } else {
//                    jenisUsaha.text = listNameAr[i]
//                    penghasilanUsaha.text = "Rp ${listIncomeAr[i]}"
//                }
//
//                item.addView(child)
//            }
//        }

        //Edit
        cardtitle2.setOnClickListener {
            /*if (toJson[18] == "2") {
                val param = "['${toJson[19]}','${toJson[20]}','']"
                val intent = ActivityFormSubjek2.getStartIntent(this)
                intent.putExtra("data_current_tanah", param)
                session.setIdAndStatus(toJson[0], "2", toJson[18])
                startActivity(intent)
            }*/

            val param = "['${data.landIdTemp}','${data.projectId}']"
            val intent = ActivityFormSubjek2.getStartIntent(this)
            intent.putExtra("data_current_tanah", param)
            session.setIdAndStatus(msubjekTempId.toString(), "2", "2")
            startActivity(intent)
        }

        //delete
        cardtitle3.setOnClickListener {
            mDialogView.btnYesDelete.setOnClickListener {
                realm.executeTransactionAsync({ inRealm ->
                    val results = inRealm.where(Subjek2::class.java).findAll()
                    val dataParty = results.where().equalTo("SubjekIdTemp", msubjekTempId).findFirst()
                    dataParty?.deleteFromRealm()

                }, {
                    Log.d("delete", "onSuccess : delete single object")
                    mdialogConfirmDelete.dismiss()
                    finish()
                }, {
                    Log.d("delete", "onFailed : ${it.localizedMessage}")
                    Log.d("delete", "onFailed : delete single object")
                })
            }

            mDialogView.btnNoDelete.setOnClickListener {
                mdialogConfirmDelete.dismiss()
            }
            mdialogConfirmDelete.show()
            /*val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                        if (toJson[18].equals("2")) {
                            val subjek2 = realm.where(Subjek2::class.java).equalTo("SubjekIdTemp", toJson[0].toInt()).findFirst()
                            Log.e("aaa", subjek2.toString())
                            realm.beginTransaction()
                            subjek2!!.deleteFromRealm()
                            realm.commitTransaction()
                        } else if (toJson[18].equals("3")) {
                            val party = realm.where(AhliWaris::class.java).equalTo("AhliWarisIdTemp", toJson[0].toInt()).findFirst()
                            Log.e("bbbb", party.toString())
                            realm.beginTransaction()
                            party!!.deleteFromRealm()
                            realm.commitTransaction()
                        }
                        dialog.dismiss()
                        finish()
                    }

                    DialogInterface.BUTTON_NEGATIVE -> {
                        dialog.dismiss()
                    }
                }
            }

            val builder = android.support.v7.app.AlertDialog.Builder(this)
            builder.setTitle("SIOJT")
                    .setMessage("Yakin akan dihapus?")
                    .setPositiveButton("Ya", dialogClickListener)
                    .setNegativeButton("Tidak", dialogClickListener)
                    .setIcon(ContextCompat.getDrawable(this@ActivityDetailSubjek2, R.drawable.ic_logo_splash)!!)
                    .show()*/
        }

    }

    private fun getPresenter(): DetailSubjekPresenter? {
        presenter = DetailSubjekPresenter()
        presenter.onAttach(this)
        return presenter
    }

    @SuppressLint("SetTextI18n", "InflateParams")
    override fun onsuccessgetdetailSubjek(responseDataDetailPemilik: ResponseDataDetailPemilik) {
//        tvPekerjaan.text = responseDataDetailPemilik.partyDetail?.occupationName ?: ""
        tvNIK.text = if(!responseDataDetailPemilik.partyDetail?.partyIdentityNumber.isNullOrEmpty()) {responseDataDetailPemilik.partyDetail?.partyIdentityNumber.toString()} else "-"
        tvName.text = if(!responseDataDetailPemilik.partyDetail?.partyFullName.isNullOrEmpty()) {responseDataDetailPemilik.partyDetail?.partyFullName.toString()} else "-"
        tvBirthplace.text = if(responseDataDetailPemilik.partyDetail?.partyBirthPlaceName.toString()
                .isNotEmpty()) {responseDataDetailPemilik.partyDetail?.partyBirthPlaceName.toString()} else "-"
        tvBirthdate.text = if(!responseDataDetailPemilik.partyDetail?.partyBirthDate.isNullOrEmpty()) {responseDataDetailPemilik.partyDetail?.partyBirthDate.toString()} else "-"
        tvJob.text = if(!responseDataDetailPemilik.partyDetail?.occupationName.isNullOrEmpty()) {responseDataDetailPemilik.partyDetail?.occupationName.toString()} else "-"
        tvProvince.text = if(!responseDataDetailPemilik.partyDetail?.provinceName.isNullOrEmpty()) {responseDataDetailPemilik.partyDetail?.provinceName.toString()} else "-"
        tvCity.text = if(!responseDataDetailPemilik.partyDetail?.regencyName.isNullOrEmpty()) {responseDataDetailPemilik.partyDetail?.regencyName.toString()} else "-"
        tvDistrict.text = if(!responseDataDetailPemilik.partyDetail?.districtName.isNullOrEmpty()) {responseDataDetailPemilik.partyDetail?.districtName.toString()} else "-"
        tvVillage.text = if(!responseDataDetailPemilik.partyDetail?.partyVillageName.isNullOrEmpty()) {responseDataDetailPemilik.partyDetail?.partyVillageName.toString()} else "-"
        tvBlock.text = if(responseDataDetailPemilik.partyDetail?.partyBlock.toString().isNotEmpty()) {responseDataDetailPemilik.partyDetail?.partyBlock.toString()} else "-"
        tvKomplek.text = if(responseDataDetailPemilik.partyDetail?.partyComplexName.toString().isNotEmpty()) {responseDataDetailPemilik.partyDetail?.partyComplexName.toString()} else "-"
        tvNo.text = if(!responseDataDetailPemilik.partyDetail?.partyNumber.isNullOrEmpty()) {responseDataDetailPemilik.partyDetail?.partyNumber.toString()} else "-"
        tvStreet.text = if(!responseDataDetailPemilik.partyDetail?.partyStreetName.isNullOrEmpty()) {responseDataDetailPemilik.partyDetail?.partyStreetName.toString()} else "-"
        tvRT.text = if(!responseDataDetailPemilik.partyDetail?.partyRT.isNullOrEmpty()) {responseDataDetailPemilik.partyDetail?.partyRT.toString()} else "-"
        tvRW.text = if(!responseDataDetailPemilik.partyDetail?.partyRW.isNullOrEmpty()) {responseDataDetailPemilik.partyDetail?.partyRW.toString()} else "-"
        tvPostalCode.text = if(!responseDataDetailPemilik.partyDetail?.partyPostalCode.isNullOrEmpty()) {responseDataDetailPemilik.partyDetail?.partyPostalCode.toString()} else "-"
        tvNPWP.text = if(!responseDataDetailPemilik.partyDetail?.partyNPWP.isNullOrEmpty()) {responseDataDetailPemilik.partyDetail?.partyNPWP.toString()} else "-"
        tvDomisili.text = if(responseDataDetailPemilik.partyDetail?.partyYearStayBegin != null) {responseDataDetailPemilik.partyDetail?.partyYearStayBegin.toString()} else "-"

        val item = findViewById<LinearLayout>(R.id.llitemfac2)
        item.removeAllViews()
        responseDataDetailPemilik.livelihoodLists!!.forEach {
            val child = layoutInflater.inflate(R.layout.item_detail_jenis_usaha_vertical, null)
            val jenisUsaha = child.findViewById(R.id.tvjenisUsaha) as com.pixplicity.fontview.FontTextView
            val penghasilanUsaha = child.findViewById(R.id.tvPenghasilan) as com.pixplicity.fontview.FontTextView
            jenisUsaha.text = it?.livelihoodName ?: "-"
            penghasilanUsaha.text = "Rp ${it?.partyLivelihoodIncome ?: "-"}"
            item.addView(child)

            //Log.d("Hasil", it?.livelihoodName.toString())
        }
    }

    override fun onRestart() {
        super.onRestart()
        /* retrieve data detail pemilik  */
        getdatapemilik2()
    }

    override fun onFragmentAttached() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFragmentDetached(tag: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onDestroy() {
        super.onDestroy()
        getPresenter()?.onDetach()
    }

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityDetailSubjek2::class.java)
        }
    }

}