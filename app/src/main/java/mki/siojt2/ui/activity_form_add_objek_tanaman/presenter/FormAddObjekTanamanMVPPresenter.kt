package mki.siojt2.ui.activity_form_add_objek_tanaman.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface FormAddObjekTanamanMVPPresenter : MVPPresenter {
    fun postPlant(userId: Int, projectLandId: Int, projectPartyId: Int, plantId: Int, pPlantPlantType: Int,
                  pPlantPlantSizedId: Int, pPlantNumber: Int, accessToken: String)

    fun getKategoriTanman(plantType: Int)
    fun getlistdataPemilik(projectId: Int, landId: String)
}