package mki.siojt2.ui.activity_form_add_objek_tanaman.recycleview_adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.MediaStore
import androidx.annotation.NonNull
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import mki.siojt2.R
import android.widget.ImageView
import com.google.android.material.textfield.TextInputEditText
import cn.pedant.SweetAlert.SweetAlertDialog
import kotlin.collections.ArrayList
import mki.siojt2.model.local_save_image.DataImagePlants
import mki.siojt2.ui.activity_form_add_objek_tanaman.ActivityFormAddObjekTanaman
import mki.siojt2.ui.activity_image_picker.ActivityImagePreview
import java.io.ByteArrayOutputStream


class AdapterTakePicturePlants internal constructor(context: Context, var mActivity: Activity) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    private var mcontext: Context = context

    private val mDataset: MutableList<DataImagePlants>? = ArrayList()

    private val TYPE_HEADER = 0
    private val TYPE_FOOTER = 1
    private val TYPE_ITEM: Int = 0

    private var mOnItemImageClickListener: OnItemImageClickListener? = null

    var mUri: Uri? = null

    inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        internal var tvtitleFoto: com.pixplicity.fontview.FontTextView = itemView.findViewById(R.id.tvtitleFoto)
        internal var imagePhoto: ImageView = itemView.findViewById(R.id.ivselect)
        internal var imageTitle: TextInputEditText = itemView.findViewById(R.id.edtitleimage)
        internal var imageClearPhoto: FrameLayout = itemView.findViewById(R.id.flclearImageSelect)

        init {
            imagePhoto.setOnLongClickListener {
                if(mOnItemImageClickListener != null){
                    mOnItemImageClickListener!!.OnItemImageClick(adapterPosition)
                }
                return@setOnLongClickListener true
            }

            imageClearPhoto.setOnClickListener {
                if (mDataset!![adapterPosition].imagepathLand!!.isNotEmpty()) {
                    mDataset[adapterPosition].imagepathLand = ""
                    notifyDataSetChanged()
                }else{
                    Toast.makeText(mcontext, "Harap tambahkan foto terlebih dahulu", Toast.LENGTH_SHORT).show()
                }
            }

            imagePhoto.setOnClickListener {
                if(mDataset!![adapterPosition].imagepathLand!!.isNotEmpty()){
                    //val imageAsBytes = Base64.decode(mDataset[adapterPosition].imagepathLand!!.toByteArray(), Base64.NO_WRAP)
                    //val decodeImage = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.size)

                    //val stream = ByteArrayOutputStream()
                    //decodeImage.compress(Bitmap.CompressFormat.JPEG, 60, stream)
                    //val byteFormat = stream.toByteArray()

                    val intent = ActivityImagePreview.getStartIntent(mcontext)
                    intent.putExtra("bitmap", mDataset[adapterPosition].imagepathLand)
                    mcontext.startActivity(intent)
                }else{
                    Toast.makeText(mcontext, "Tekan dan tahan beberapa detik untuk menambah foto", Toast.LENGTH_SHORT).show()
                }

            }

            imageTitle.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                    mDataset!![adapterPosition].imagenameLand = s.toString()
                }

                override fun afterTextChanged(s: Editable) {}
            })
        }
    }

    inner class ViewHolderFooter(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        internal var plus: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnPlusMataPencaharian2)
        internal var minus: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnMinusMataPencaharian2)

        init {
            minus.setOnClickListener {
                val position = adapterPosition
                try {
                    mDataset!!.removeAt(adapterPosition - 1)
                    notifyItemRemoved(position)
                    notifyDataSetChanged()
                    notifyItemChanged(position)
                    //Toast.makeText(mcontext, position.toString(), Toast.LENGTH_SHORT).show()
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                }
            }
            plus.setOnClickListener {
                val position = adapterPosition
                //Toast.makeText(mcontext, adapterPosition.toString(), Toast.LENGTH_SHORT).show()
                if (mDataset?.get(position - 1)!!.imagepathLand!!.isEmpty()|| mDataset[position - 1].imagenameLand!!.isEmpty()) {
                    val sweetAlretLoading = SweetAlertDialog(mcontext, SweetAlertDialog.WARNING_TYPE)
                    sweetAlretLoading.titleText = "LENGKAPI DATA"
                    sweetAlretLoading.setCancelable(false)
                    sweetAlretLoading.confirmText = "OK"
                    sweetAlretLoading.setConfirmClickListener { sDialog ->
                        sDialog?.let { if (it.isShowing) it.dismiss() }
                    }
                    sweetAlretLoading.show()
                } else {
                    /**
                     * 2nd development - up to 5 adding photos
                     * */
                    (mActivity as ActivityFormAddObjekTanaman).addphotoItem()
                    /*try {
                        if (mDataset.size >= 5){
                            Toast.makeText(mcontext, "Batas maksimal foto hanya 5", Toast.LENGTH_SHORT).show()
                        }else{
                            (mActivity as ActivityFormAddObjekTanaman).addphotoItem()
                        }
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        e.printStackTrace()
                    }*/
                }

            }
        }
    }


    @NonNull
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return when (viewType) {
            //Inflating footer view
            TYPE_ITEM -> ViewHolder(
                    LayoutInflater.from(viewGroup.context).inflate(R.layout.item_take_picture, viewGroup, false))
            //Inflating recycle view item layout
            else -> ViewHolderFooter(
                    LayoutInflater.from(viewGroup.context).inflate(R.layout.item_mata_pencaharian_footer, viewGroup, false))
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        val x = holder.layoutPosition

        Log.d("kuy1", x.toString())
        Log.d("kuy2", position.toString())
        Log.d("kuy3", mDataset!!.size.toString())
        if (holder is ViewHolder) {
            holder.tvtitleFoto.text = "Foto properti tanaman ${x + 1}"

            val options = BitmapFactory.Options()
            options.inPreferredConfig = Bitmap.Config.ARGB_8888
            val decodeImage = BitmapFactory.decodeFile(mDataset[x].imagepathLand!!, options)

            holder.imagePhoto.setImageBitmap(decodeImage)
            holder.imageTitle.setText(mDataset[x].imagenameLand)

            if (mDataset[x].imagepathLand!!.isNotEmpty()){
                holder.imageClearPhoto.visibility = View.VISIBLE
            }else{
                holder.imageClearPhoto.visibility = View.GONE
                holder.imagePhoto.setImageResource(R.drawable.nullpicture)
            }

            if (mDataset[x].imagenameLand!!.isEmpty()){
                holder.imageTitle.text = null
            }


            /*if (mDataset!![x].imagepathLand!!.isNotEmpty()|| mDataset[x].imagenameLand!!.isNotEmpty()) {

                val options = BitmapFactory.Options()
                options.inPreferredConfig = Bitmap.Config.ARGB_8888
                val decodeImage = BitmapFactory.decodeFile(mDataset[x].imagepathLand!!, options)

                holder.imagePhoto.setImageBitmap(decodeImage)
                holder.imageTitle.setText(mDataset[x].imagenameLand)

                //holder.imagePhoto.setImageURI(mDataset[x].imageEncode)
                //val imageAsBytes = Base64.decode(mDataset[x].imagepathLand!!.toByteArray(), Base64.NO_WRAP)
                //val decodeImage = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.size)

                *//*holder.imagePhoto.setOnClickListener {
                    val intent = ActivityImagePreview.getStartIntent(mcontext)
                    intent.putExtra("bitmap", converttoUri(decodeImage))
                    mcontext.startActivity(intent)
                }*//*
            }else{
                holder.imagePhoto.setImageResource(R.drawable.nullpicture)
                holder.imageTitle.text = null
            }*/

        } else if (holder is ViewHolderFooter) {
            when {
                x <= 1 -> {
                    holder.minus.visibility = View.GONE
                }
                else -> {
                    holder.plus.visibility = View.VISIBLE
                    holder.minus.visibility = View.VISIBLE
                }
            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        return if (position == mDataset!!.size) TYPE_FOOTER else TYPE_ITEM
    }

    override fun getItemCount(): Int {
        return mDataset!!.size + 1
    }

    private fun converttoUri(bitmap: Bitmap): String {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val path = MediaStore.Images.Media.insertImage(mcontext.contentResolver, bitmap, "title", null)

        Log.d("image file", Uri.parse(path).toString())
        return path
    }

    fun clear() {
        this.mDataset!!.clear()
        notifyDataSetChanged()
    }

    fun addItems(data: DataImagePlants) {
        this.mDataset!!.add(data)
        notifyDataSetChanged()
    }

    fun updateDateAfterClickImage(position: Int, data: DataImagePlants){
        mDataset!!.removeAt(position)
        mDataset.add(position, data)
        notifyItemRemoved(position)
        notifyItemChanged(position)
        notifyDataSetChanged()
    }

    fun returnDataPhoto(): List<DataImagePlants> {
        return mDataset!!
    }

    /* set listener*/
    fun setListnerItemImageClick(onItemImageClickListener: OnItemImageClickListener) {
        mOnItemImageClickListener = onItemImageClickListener
    }

    interface OnItemImageClickListener{
        fun OnItemImageClick(position: Int)
    }


}