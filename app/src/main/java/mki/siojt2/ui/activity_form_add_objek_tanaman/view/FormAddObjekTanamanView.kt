package mki.siojt2.ui.activity_form_add_objek_tanaman.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.ResponseDataListPemilik
import mki.siojt2.model.master_data_tanaman.ResponseDataListKategoriTanaman

interface FormAddObjekTanamanView: MvpView {
    fun onsuccesspostPlant()
    fun onsuccessgetKategoriTanman(responseDataListKategoriTanaman: MutableList<ResponseDataListKategoriTanaman>)
    fun onsuccessgetlistdataPemilik(responseDataListPemilik: MutableList<ResponseDataListPemilik>)

}