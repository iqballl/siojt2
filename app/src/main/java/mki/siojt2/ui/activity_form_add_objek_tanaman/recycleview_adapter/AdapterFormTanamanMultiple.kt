package mki.siojt2.ui.activity_form_add_objek_tanaman.recycleview_adapter

import android.annotation.SuppressLint
import android.content.Context
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import mki.siojt2.R
import android.widget.AdapterView
import android.widget.LinearLayout
import com.google.android.material.textfield.TextInputEditText
import cn.pedant.SweetAlert.SweetAlertDialog
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.item_tanaman.view.*
import mki.siojt2.model.data_from_objek_tanaman.DataTanaman
import kotlin.collections.ArrayList
import mki.siojt2.model.master_data_tanaman.*
import mki.siojt2.ui.activity_form_add_objek_tanaman.ActivityFormAddObjekTanaman


class AdapterFormTanamanMultiple internal constructor(realm: Realm, context: Context,
                                                      dataJenisTanaman: MutableList<ResponseJenisTanaman>, dataUkuranTanaman: MutableList<ResponseSizePlants>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var mRealm: Realm = realm
    private var mcontext: Context = context
    private var mdataJenisTanaman: MutableList<ResponseJenisTanaman>? = dataJenisTanaman
    private var mdataUkuranTanaman: MutableList<ResponseSizePlants>? = dataUkuranTanaman

    private var mRealmResultJenisKategoriTanaman: RealmResults<ResponseJenisTanaman>? = null
    private var mDatamJenisKategoriTanaman: MutableList<ResponseDataListKategoriTanamanWithOutParam>
    private var mDatamJenisKategoriRumpun: MutableList<ResponseDataListKategoriRumpunWithOutParam>
    private var mDataSet: MutableList<DataTanaman>? = ArrayList()

    private val TYPE_HEADER = 0
    private val TYPE_FOOTER = 1
    private val TYPE_ITEM: Int = 0

    init {
        val resultRealmKategoriTanaman = mRealm.where(ResponseDataListKategoriTanamanWithOutParam::class.java).findAllAsync()
        this.mDatamJenisKategoriTanaman = realm.copyFromRealm(resultRealmKategoriTanaman)

        val resultRealmKategoriRumpun = realm.where(ResponseDataListKategoriRumpunWithOutParam::class.java).findAllAsync()
        this.mDatamJenisKategoriRumpun = realm.copyFromRealm(resultRealmKategoriRumpun)
    }

    @SuppressLint("SetTextI18n")
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var spinnerJenisTanaman: com.weiwangcn.betterspinner.library.BetterSpinner = itemView.findViewById(R.id.itemspinnerJenisTanaman)
        internal var spinnerKategoriTanaman: com.weiwangcn.betterspinner.library.BetterSpinner = itemView.findViewById(R.id.itemspinnerKategoriTanaman)
        internal var spinnerUkuranTanaman: com.weiwangcn.betterspinner.library.BetterSpinner = itemView.findViewById(R.id.itemspinnersizePlant)
        internal var edJumlahTanaman: TextInputEditText = itemView.findViewById(R.id.itemedjumlahTanaman)

        internal var txtmainTitleTanaman: com.pixplicity.fontview.FontTextView = itemView.findViewById(R.id.mainTitleTanaman)
        internal var txttvnamaKategori: com.pixplicity.fontview.FontTextView = itemView.findViewById(R.id.tvnamaKategori)
        internal var txttvukuranKategori: com.pixplicity.fontview.FontTextView = itemView.findViewById(R.id.tvukuranKategori)
        internal var txttvjmlKategori: com.pixplicity.fontview.FontTextView = itemView.findViewById(R.id.tvjmlKategori)

        internal var viewkategoriLain: LinearLayout = itemView.findViewById(R.id.llnamakategoriTanamanLain)
        internal var ednamakategoriLain: TextInputEditText = itemView.ednamakategoriLain
        internal var txtnamakategoriLain: com.pixplicity.fontview.FontTextView = itemView.findViewById(R.id.tvnamakategoriLain)


        init {
            /*when (midTanaman!![adapterPosition]) {
                1 -> {
                    val adapterKategoriTanaman = ArrayAdapter<ResponseDataListKategoriTanamanWithOutParam>(mcontext,
                            R.layout.item_spinner, mDatamJenisKategoriTanaman)
                    adapterKategoriTanaman.setNotifyOnChange(true)
                    spinnerKategoriTanaman.setAdapter(adapterKategoriTanaman)
                    adapterKategoriTanaman.setNotifyOnChange(true)
                }
                else -> {
                    val adapterKategoriRumpun = ArrayAdapter<ResponseDataListKategoriRumpunWithOutParam>(mcontext,
                            R.layout.item_spinner, mDatamJenisKategoriRumpun)
                    adapterKategoriRumpun.setNotifyOnChange(true)
                    spinnerKategoriTanaman.setAdapter(adapterKategoriRumpun)
                    adapterKategoriRumpun.setNotifyOnChange(true)
                }
            }*/

            mDataSet!![0].tanamanId = mdataJenisTanaman?.get(0)?.plantTypeId!!.toString()
            mDataSet!![0].tanamanName = mdataJenisTanaman?.get(0)?.plantTypeName.toString()

            val adapterKategoriTanaman = ArrayAdapter<ResponseDataListKategoriTanamanWithOutParam>(mcontext,
                R.layout.item_spinner, mDatamJenisKategoriTanaman)
            adapterKategoriTanaman.setNotifyOnChange(true)
            spinnerKategoriTanaman.setAdapter(adapterKategoriTanaman)
            adapterKategoriTanaman.setNotifyOnChange(true)

            txttvnamaKategori.text = "Jenis tanaman"
            txttvukuranKategori.text = "Ukuran tanaman"
            txttvjmlKategori.text = "Jumlah tanaman"
            txtnamakategoriLain.text = ""
            spinnerKategoriTanaman.setText("")
            spinnerUkuranTanaman.setText("")

            spinnerJenisTanaman.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
                val selectedProject = adapterView.getItemAtPosition(position) as ResponseJenisTanaman
                mDataSet!![adapterPosition].tanamanId = selectedProject.plantTypeId!!.toString()
                mDataSet!![adapterPosition].tanamanName = selectedProject.plantTypeName.toString()

                when (mDataSet!![adapterPosition].tanamanId) {
                    "1" -> {
                        val adapterKategoriTanaman = ArrayAdapter<ResponseDataListKategoriTanamanWithOutParam>(mcontext,
                                R.layout.item_spinner, mDatamJenisKategoriTanaman)
                        adapterKategoriTanaman.setNotifyOnChange(true)
                        spinnerKategoriTanaman.setAdapter(adapterKategoriTanaman)
                        adapterKategoriTanaman.setNotifyOnChange(true)

                        txttvnamaKategori.text = "Jenis tanaman"
                        txttvukuranKategori.text = "Ukuran tanaman"
                        txttvjmlKategori.text = "Jumlah tanaman"
                        txtnamakategoriLain.text = ""
                        spinnerKategoriTanaman.setText("")
                        spinnerUkuranTanaman.setText("")
                    }
                    else -> {
                        val adapterKategoriRumpun = ArrayAdapter<ResponseDataListKategoriRumpunWithOutParam>(mcontext,
                                R.layout.item_spinner, mDatamJenisKategoriRumpun)
                        adapterKategoriRumpun.setNotifyOnChange(true)
                        spinnerKategoriTanaman.setAdapter(adapterKategoriRumpun)
                        adapterKategoriRumpun.setNotifyOnChange(true)

                        txttvnamaKategori.text = "Jenis rumpun"
                        txttvukuranKategori.text = "Ukuran rumpun"
                        txttvjmlKategori.text = "Jumlah rumpun"
                        spinnerKategoriTanaman.setText("")
                        spinnerUkuranTanaman.setText("")
                        txtnamakategoriLain.text = ""
                        edJumlahTanaman.text = null
                        edJumlahTanaman.hint = "....."
                    }
                }

                //Toast.makeText(mcontext, midTanaman.toString(), Toast.LENGTH_SHORT).show()
            }

            spinnerKategoriTanaman.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
                /*val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListKategoriTanamanWithOutParam
                midTanaman!![adapterPosition] = selectedProject.plantTypeId!!
                mvalueTanaman!![adapterPosition] = selectedProject.plantTypeName.toString()*/

                mDataSet!![adapterPosition].tanamanId = mdataJenisTanaman?.get(0)?.plantTypeId!!.toString()
                mDataSet!![adapterPosition].tanamanName = mdataJenisTanaman?.get(0)?.plantTypeName.toString()

                if (mDataSet!![adapterPosition].tanamanId == "1") {
                    val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListKategoriTanamanWithOutParam
                    val valueSpinner = adapterView.getItemAtPosition(position).toString()
                    mDataSet!![adapterPosition].kategoriTanamanId = selectedProject.getplantId().toString()
                    mDataSet!![adapterPosition].kategoriTanamanName = selectedProject.getplantName()!!
                    when {
                        mDataSet!![adapterPosition].kategoriTanamanName == "Lain-Nya" -> {
                            viewkategoriLain.visibility = View.VISIBLE
                            txtnamakategoriLain.text = "Jenis tanaman Lain"

                            spinnerUkuranTanaman.setText("")
                            mDataSet!![adapterPosition].ukuranTanamanId = ""
                            mDataSet!![adapterPosition].ukuranTanamanName = ""

                            mDataSet!![adapterPosition].jumlahTanaman = ""
                            edJumlahTanaman.text = null
                        }
                        else -> {
                            viewkategoriLain.visibility = View.GONE
                            ednamakategoriLain.text = null

                            spinnerUkuranTanaman.setText("")
                            mDataSet!![adapterPosition].ukuranTanamanId = ""
                            mDataSet!![adapterPosition].ukuranTanamanName = ""

                            mDataSet!![adapterPosition].jumlahTanaman = ""
                            edJumlahTanaman.text = null
                        }
                    }

                    //Toast.makeText(mcontext, midKategoriTanaman.toString(), Toast.LENGTH_SHORT).show()
                } else {
                    val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListKategoriRumpunWithOutParam
                    val valueSpinner = adapterView.getItemAtPosition(position).toString()
                    mDataSet!![adapterPosition].kategoriTanamanId = selectedProject.getplantId().toString()
                    mDataSet!![adapterPosition].kategoriTanamanName = selectedProject.getplantName()!!
                    when {
                        mDataSet!![adapterPosition].kategoriTanamanName == "Lain-Nya" -> {
                            viewkategoriLain.visibility = View.VISIBLE
                            txtnamakategoriLain.text = "Jenis rumpun Lain"
                        }
                        else -> {
                            viewkategoriLain.visibility = View.GONE
                            ednamakategoriLain.text = null

                            mDataSet!![adapterPosition].jumlahTanaman = ""
                            edJumlahTanaman.text = null
                        }
                    }

                    //Toast.makeText(mcontext, midKategoriTanaman.toString(), Toast.LENGTH_SHORT).show()
                }
            }

            ednamakategoriLain.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    mDataSet!![adapterPosition].kategoriTanamanNameOther = s.toString()
                }

                override fun afterTextChanged(s: Editable) {}
            })

            spinnerUkuranTanaman.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
                val selectedProject = adapterView.getItemAtPosition(position) as ResponseSizePlants
                val valueSpinner = adapterView.getItemAtPosition(position).toString()
                mDataSet!![adapterPosition].ukuranTanamanId = selectedProject.plantSizeId.toString()
                mDataSet!![adapterPosition].ukuranTanamanName = selectedProject.plantSizeName!!

                //Toast.makeText(mcontext, midUkuranTanaman.toString(), Toast.LENGTH_SHORT).show()
            }

            edJumlahTanaman.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    mDataSet!![adapterPosition].jumlahTanaman = s.toString()
                }

                override fun afterTextChanged(s: Editable) {}
            })

            /*edJumlahTanaman.setOnEditorActionListener(object : TextView.OnEditorActionListener {
                override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                    // Can get the next focus or the previous one according to the needs
                    val nextView = v!!.focusSearch(View.FOCUS_DOWN)
                    if (nextView != null){
                        nextView.requestFocus(View.FOCUS_DOWN)
                    }

                    //This must be returned here.true
                    return true
                }

            })*/
        }
    }

    inner class ViewHolderFooter(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var plus: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnPlusMataPencaharian2)
        internal var minus: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnMinusMataPencaharian2)

        init {
            minus.setOnClickListener {
                val position = adapterPosition
                try {
                    mDataSet!!.removeAt(position - 1)
                    notifyItemRemoved(position)
                    notifyDataSetChanged()
                    notifyItemChanged(position)
                    //Toast.makeText(mcontext, position.toString(), Toast.LENGTH_SHORT).show()
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                }
            }
            plus.setOnClickListener {
                val position = adapterPosition
                //Toast.makeText(mcontext, "${position}", Toast.LENGTH_SHORT).show()

                when {
                    mDataSet?.get(position - 1)!!.tanamanId!!.isEmpty() || mDataSet?.get(position - 1)!!.kategoriTanamanId!!.isEmpty() || mDataSet?.get(position - 1)!!.jumlahTanaman!!.isEmpty() -> {
                        val sweetAlretLoading = SweetAlertDialog(mcontext, SweetAlertDialog.WARNING_TYPE)
                        sweetAlretLoading.titleText = "LENGKAPI DATA"
                        sweetAlretLoading.setCancelable(false)
                        sweetAlretLoading.confirmText = "OK"
                        sweetAlretLoading.setConfirmClickListener { sDialog ->
                            sDialog?.let { if (it.isShowing) it.dismiss() }
                        }
                        sweetAlretLoading.show()
                    }

                    mDataSet?.get(position - 1)!!.kategoriTanamanName!! == "Lain-Nya" && mDataSet?.get(position - 1)!!.jumlahTanaman!!.isEmpty() || mDataSet?.get(position - 1)!!.kategoriTanamanName!! == "Lain-Nya" && mDataSet?.get(position - 1)!!.kategoriTanamanNameOther!!.isEmpty() -> {
                        val sweetAlretLoading = SweetAlertDialog(mcontext, SweetAlertDialog.WARNING_TYPE)
                        sweetAlretLoading.titleText = "LENGKAPI DATA"
                        sweetAlretLoading.setCancelable(false)
                        sweetAlretLoading.confirmText = "OK"
                        sweetAlretLoading.setConfirmClickListener { sDialog ->
                            sDialog?.let { if (it.isShowing) it.dismiss() }
                        }
                        sweetAlretLoading.show()
                    }

                    else -> {
                        try {
                            if (mcontext is ActivityFormAddObjekTanaman) {
                                (mcontext as ActivityFormAddObjekTanaman).addItem()
                            }
                        } catch (e: ArrayIndexOutOfBoundsException) {
                            e.printStackTrace()
                        }
                    }
                }


                /*if (midTanaman?.get(position - 1)!!.isEmpty() ||
                        mvalueTanaman?.get(position - 1)!!.isEmpty() ||

                        midKategoriTanaman?.get(position - 1)!!.isEmpty() ||
                        mvalueKategoriTanaman?.get(position - 1)!!.isEmpty() ||

                        midUkuranTanaman?.get(position - 1)!!.isEmpty() ||
                        mvalueUkuranTanaman?.get(position - 1)!!.isEmpty() ||
                        mjumlahTanaman?.get(position - 1)!!.isEmpty()) {
                    val sweetAlretLoading = SweetAlertDialog(mcontext, SweetAlertDialog.WARNING_TYPE)
                    sweetAlretLoading.titleText = "LENGKAPI DATA"
                    sweetAlretLoading.setCancelable(false)
                    sweetAlretLoading.confirmText = "OK"
                    sweetAlretLoading.setConfirmClickListener { sDialog ->
                        sDialog?.let { if (it.isShowing) it.dismiss() }
                    }
                    sweetAlretLoading.show()
                } else {
                    try {
                        if (mcontext is ActivityFormAddObjekTanaman) {
                            (mcontext as ActivityFormAddObjekTanaman).addItem(adapterPosition + 2)
                        }
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        e.printStackTrace()
                    }
                }*/

            }
        }
    }


    @NonNull
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ITEM -> ViewHolder(
                    LayoutInflater.from(viewGroup.context).inflate(R.layout.item_tanaman, viewGroup, false))
            else -> ViewHolderFooter(
                    LayoutInflater.from(viewGroup.context).inflate(R.layout.item_mata_pencaharian_footer, viewGroup, false))
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val x = holder.layoutPosition
        if (holder is ViewHolder) {
            holder.txtmainTitleTanaman.text = "Jenis tanaman ${x + 1}"
            if (mDataSet!![x].tanamanId!!.isNotEmpty() || mDataSet!![x].kategoriTanamanId!!.isNotEmpty() ||
                    mDataSet!![x].jumlahTanaman!!.isNotEmpty() || mDataSet!![x].ukuranTanamanId!!.isNotEmpty()) {
                holder.spinnerJenisTanaman.setText(mDataSet?.get(x)!!.tanamanName.toString())
                holder.spinnerKategoriTanaman.setText(mDataSet?.get(x)!!.kategoriTanamanName.toString())
                holder.spinnerUkuranTanaman.setText(mDataSet?.get(x)!!.ukuranTanamanName.toString())
                holder.edJumlahTanaman.setText(mDataSet?.get(x)!!.jumlahTanaman.toString())
                //holder.txtmainTitleTanaman.text = "cuy ${x + 1}"

                if (mDataSet?.get(x)!!.kategoriTanamanNameOther.toString().isNotEmpty()) {
                    holder.viewkategoriLain.visibility = View.VISIBLE
                    holder.ednamakategoriLain.setText(mDataSet?.get(x)!!.kategoriTanamanNameOther.toString())
                } else {
                    holder.viewkategoriLain.visibility = View.GONE
                }

                when (mDataSet!![x].tanamanId!!) {
                    "1" -> {
                        val adapterKategoriTanaman = ArrayAdapter<ResponseDataListKategoriTanamanWithOutParam>(mcontext,
                                R.layout.item_spinner, mDatamJenisKategoriTanaman)
                        adapterKategoriTanaman.setNotifyOnChange(true)
                        holder.spinnerKategoriTanaman.setAdapter(adapterKategoriTanaman)
                        adapterKategoriTanaman.setNotifyOnChange(true)

                        holder.txttvnamaKategori.text = "Nama tanaman"
                        holder.txttvukuranKategori.text = "Ukuran tanaman"
                        holder.txttvjmlKategori.text = "Jumlah tanaman"
                    }
                    else -> {
                        val adapterKategoriRumpun = ArrayAdapter<ResponseDataListKategoriRumpunWithOutParam>(mcontext,
                                R.layout.item_spinner, mDatamJenisKategoriRumpun)
                        adapterKategoriRumpun.setNotifyOnChange(true)
                        holder.spinnerKategoriTanaman.setAdapter(adapterKategoriRumpun)
                        adapterKategoriRumpun.setNotifyOnChange(true)

                        holder.txttvnamaKategori.text = "Nama rumpun"
                        holder.txttvukuranKategori.text = "Ukuran rumpun"
                        holder.txttvjmlKategori.text = "Jumlah rumpun"
                    }
                }

            } else {
                holder.viewkategoriLain.visibility = View.GONE
                holder.spinnerJenisTanaman.setText("")
                holder.spinnerKategoriTanaman.setText("")
                holder.spinnerUkuranTanaman.setText("")
                holder.edJumlahTanaman.text = null
                holder.edJumlahTanaman.hint = "....."
            }

            val adapterJenisTanaman = ArrayAdapter<ResponseJenisTanaman>(mcontext, R.layout.item_spinner, mdataJenisTanaman!!)
            holder.spinnerJenisTanaman.setAdapter(adapterJenisTanaman)

            val adapterUkuranTanaman = ArrayAdapter<ResponseSizePlants>(mcontext, R.layout.item_spinner, mdataUkuranTanaman!!)
            holder.spinnerUkuranTanaman.setAdapter(adapterUkuranTanaman)


        } else if (holder is ViewHolderFooter) {
            if (x <= 1) {
                holder.minus.visibility = View.GONE
            } else {
                holder.minus.visibility = View.VISIBLE
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == mDataSet!!.size) TYPE_FOOTER else TYPE_ITEM
    }

    override fun getItemCount(): Int {
        return mDataSet!!.size + 1
    }

    fun adddataTanaman(data: DataTanaman) {
        this.mDataSet!!.add(data)
        notifyDataSetChanged()
    }

    fun callbackDataTanaman(): List<DataTanaman> {
        return mDataSet!!
    }

    /*fun callbackIdTanaman(): java.util.ArrayList<String> {
        return midTanaman!!
    }

    fun callbackValueNameTanaman(): java.util.ArrayList<String> {
        return mvalueTanaman!!
    }

    fun callbackIdKategoriTanaman(): java.util.ArrayList<String> {
        return midKategoriTanaman!!
    }

    fun callbackValueNameKategoriTanaman(): java.util.ArrayList<String> {
        return mvalueKategoriTanaman!!
    }

    fun callbackIdUkuranTanaman(): java.util.ArrayList<String> {
        return midUkuranTanaman!!
    }

    fun callbackValueNameUkuranTanaman(): java.util.ArrayList<String> {
        return mvalueUkuranTanaman!!
    }

    fun callbackJmlhTanaman(): java.util.ArrayList<String> {
        return mjumlahTanaman!!
    }*/


}