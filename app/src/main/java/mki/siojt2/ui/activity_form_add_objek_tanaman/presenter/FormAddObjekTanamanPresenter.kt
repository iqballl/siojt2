package mki.siojt2.ui.activity_form_add_objek_tanaman.presenter

import android.util.Log
import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.ui.activity_form_add_objek_tanaman.view.FormAddObjekTanamanView
import mki.siojt2.utils.ErrorHandler
import mki.siojt2.utils.rxJava.RxUtils

class FormAddObjekTanamanPresenter : BasePresenter(), FormAddObjekTanamanMVPPresenter {

    private var disposables: CompositeDisposable = CompositeDisposable()

    override fun getlistdataPemilik(projectId: Int, landId: String) {
        view().onShowLoading()
        disposables.add(
                dataManager.getAllParties(projectId, landId)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ responseData ->
                            view().onDismissLoading()
                            if (responseData.status == Constant.STATUS_ERROR) {
                                view().onFailed(responseData.message!!)
                            } else {
                                if (responseData.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetlistdataPemilik(responseData.result!!)
                                } else {
                                    view().onFailed(responseData.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            Log.d("throwable", throwable.localizedMessage)
                        }
        )
    }


    override fun getKategoriTanman(plantType: Int) {
        view().onShowLoading()
        disposables.add(
                dataManager.getkategoriTanaman(plantType)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ responseData ->
                            view().onDismissLoading()
                            if (responseData.status == Constant.STATUS_ERROR) {
                                view().onFailed(responseData.message!!)
                            } else {
                                if (responseData.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetKategoriTanman(responseData.result!!)
                                } else {
                                    view().onFailed(responseData.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            Log.d("throwable", throwable.localizedMessage)
                        }
        )
    }

    override fun postPlant(userId: Int, projectLandId: Int, projectPartyId: Int, plantId: Int, pPlantPlantType: Int, pPlantPlantSizedId: Int, pPlantNumber: Int, accessToken: String) {
        view().onShowLoading()
        disposables.add(
                dataManager.postPlant(userId, projectLandId, projectPartyId, plantId, pPlantPlantType, pPlantPlantSizedId, pPlantNumber, accessToken)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccesspostPlant()
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }


    private fun view(): FormAddObjekTanamanView {
        return getView() as FormAddObjekTanamanView
    }
}