package mki.siojt2.ui.activity_form_add_objek_tanaman

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_form_subjek_4.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.utils.extension.OnSingleClickListener

class ActicitySuccessAddObjekTanaman : BaseActivity(){

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActicitySuccessAddObjekTanaman::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_form_subjek_4)

        /* set toolbar*/
        /*if (toolbarCustom != null) {
            setSupportActionBar(toolbarCustom)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarCustom.setNavigationOnClickListener {
            finish()
        }*/

        btnsuccessaddData.setOnClickListener(object : OnSingleClickListener() {
            override fun onSingleClick(v: View) {
                finish()
            }

        })

    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }


}