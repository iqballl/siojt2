package mki.siojt2.ui.activity_form_add_objek_tanaman

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import cn.pedant.SweetAlert.SweetAlertDialog
import io.realm.Realm
import io.realm.RealmList
import kotlinx.android.synthetic.main.activity_form_add_objek_tanaman.*
import kotlinx.android.synthetic.main.dialog_confirm_delete.view.*

import kotlinx.android.synthetic.main.toolbar_form.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.model.ResponseDataListPemilik
import mki.siojt2.model.data_from_objek_tanaman.DataTanaman
import mki.siojt2.model.local_save_image.DataImagePlants
import mki.siojt2.model.localsave.Session
import mki.siojt2.model.localsave.Tanaman
import mki.siojt2.model.localsave.TempPemilikObjek
import mki.siojt2.model.master_data_tanaman.*
import mki.siojt2.ui.activity_form_add_objek_tanaman.presenter.FormAddObjekTanamanPresenter
import mki.siojt2.ui.activity_form_add_objek_tanaman.recycleview_adapter.AdapterFormTanamanMultiple
import mki.siojt2.ui.activity_form_add_objek_tanaman.recycleview_adapter.AdapterTakePicturePlants
import mki.siojt2.ui.activity_form_add_objek_tanaman.view.FormAddObjekTanamanView
import mki.siojt2.ui.activity_image_picker.ImagePickerActivity
import mki.siojt2.utils.realm.RealmController
import java.io.ByteArrayOutputStream
import java.io.IOException
import kotlin.collections.ArrayList

class ActivityFormAddObjekTanaman : BaseActivity(), FormAddObjekTanamanView, AdapterTakePicturePlants.OnItemImageClickListener {

    companion object {
        private val REQUEST_IMAGE = 100
        internal const val TAG = "ActivityAddObjekTanaman"
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityFormAddObjekTanaman::class.java)
        }
    }

    lateinit var formAddObjekTanamanPresenter: FormAddObjekTanamanPresenter

    private var jenisTanaman: MutableList<ResponseJenisTanaman>? = ArrayList()
    private var ukuranTanaman: MutableList<ResponseSizePlants>? = ArrayList()

    private var sendIdUserPemilik: Int? = 0
    private var sendIdJenisTanaman: Int? = 0
    private var sendIdKategoriTanaman: Int? = 0
    private var sendIdUkuranTanaman: Int? = 0
    private var sendjumlahTanaman: Int? = 0

    private var projectLandId: Int? = 0
    private var partyTypdeId: String? = ""

    private var mLandId: Int? = 0
    private var mProjectId: Int? = 0

    private var mDataKategoriTanaman: MutableList<ResponseDataListKategoriTanamanWithOutParam> = ArrayList()
    private var mDataKategoriRumpun: MutableList<ResponseDataListKategoriRumpunWithOutParam> = ArrayList()

    private lateinit var realm: Realm
    private lateinit var session: Session
    private lateinit var mdialogConfirmDelete: AlertDialog
    private lateinit var mDialogView: View

    lateinit var adapterFormTanamanMultiple: AdapterFormTanamanMultiple
    private var mDataHistory: Tanaman? = null

    private var validPemilikTanaman: Boolean = false
    private var validdataTanaman: Boolean = false

    private var dialogValidate: SweetAlertDialog? = null

    private var mpositionImageClick: Int? = null
    lateinit var adapterTakePicturePlants: AdapterTakePicturePlants

    private var mcurrentSession: mki.siojt2.model.Session? = null

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_add_objek_tanaman)
        toolbar_elevation.visibility = View.VISIBLE

        /* init realm */
        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        //session = Session(this)
        val mdataSession = intent?.extras?.getString("data_current_session")
        if (mdataSession != null) {
            val turnsType = object : TypeToken<mki.siojt2.model.Session>() {}.type
            mcurrentSession = Gson().fromJson<mki.siojt2.model.Session>(mdataSession, turnsType)

            if (mcurrentSession!!.id != null && mcurrentSession!!.statusPost == "2") {
                tvFormToolbarTitle.text = "Edit data objek - Tanaman"
            }
        } else {
            tvFormToolbarTitle.text = "Tambah data objek - Tanaman"
        }

        /* set toolbar*/
        if (toolbarCustom != null) {
            setSupportActionBar(toolbarCustom)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        /* set ini apabila action dari button tambah data tanaman */
        val historyData = intent?.extras?.getString("data_current_tanah")
        var toJson: List<String> = ArrayList()
        if (historyData != null) {
            toJson = Gson().fromJson(historyData, Array<String>::class.java).toList()
            mLandId = toJson[0].toInt()
            mProjectId = toJson[1].toInt()
        }

        ednomorTanah.setText(mLandId.toString())

        dialogValidate = SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
        dialogValidate!!.setCancelable(false)
        dialogValidate!!.confirmText = "OK"
        dialogValidate!!.setConfirmClickListener { sDialog ->
            sDialog?.let { if (it.isShowing) it.dismiss() }
        }

        /* set ini apabila action dari detail tanaman action button edit tanaman */
        //val historyData2 = intent?.extras?.getString("data_current_tanaman")
        //val turnsType = object : TypeToken<Tanaman>() {}.type
        //mDataHistory = Gson().fromJson<Tanaman>(historyData2, turnsType)

        jenisTanaman?.add(ResponseJenisTanaman(1, "Tanaman"))
//        jenisTanaman?.add(ResponseJenisTanaman(2, "Rumpun"))

        ukuranTanaman?.add(ResponseSizePlants(1, "Besar"))
        ukuranTanaman?.add(ResponseSizePlants(2, "Sedang"))
        ukuranTanaman?.add(ResponseSizePlants(3, "Kecil"))

        /* set adapter & recycleview foto tanaman */
        adapterTakePicturePlants = AdapterTakePicturePlants(this, this@ActivityFormAddObjekTanaman)
        adapterTakePicturePlants.setListnerItemImageClick(this)

        val linearLayoutManagerphotoTanaman = androidx.recyclerview.widget.LinearLayoutManager(this@ActivityFormAddObjekTanaman, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        rvlistphotoTanaman.adapter = adapterTakePicturePlants
        rvlistphotoTanaman.layoutManager = linearLayoutManagerphotoTanaman

        /* set adapter & recycleview form tanaman */
        adapterFormTanamanMultiple = AdapterFormTanamanMultiple(realm, this,
                jenisTanaman!!, ukuranTanaman!!)

        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@ActivityFormAddObjekTanaman, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        rvFormTanaman.layoutManager = linearLayoutManager
        rvFormTanaman.adapter = adapterFormTanamanMultiple

        /* function check edit */
        setupForEdit()

        /* set dialog confirm delete data */
        mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_confirm_delete, null)
        val mBuilder = AlertDialog.Builder(this)
        mdialogConfirmDelete = mBuilder.create()

        mdialogConfirmDelete.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        mdialogConfirmDelete.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mdialogConfirmDelete.setCancelable(false)
        val window = mdialogConfirmDelete.window
        if (window != null) {
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            mdialogConfirmDelete.window!!.setLayout(width, height)
        }
        mdialogConfirmDelete.setView(mDialogView)

        toolbarCustom.setNavigationOnClickListener {
            finish()
        }

        /* set kepemilikan objek */
        val results = realm.where(TempPemilikObjek::class.java).findAll()
        if (results != null) {
            val adapterpemilikTanaman = ArrayAdapter<TempPemilikObjek>(this@ActivityFormAddObjekTanaman,
                    R.layout.item_spinner, results)
            adapterpemilikTanaman.setNotifyOnChange(true)
            spinnerpemilikTanaman.setAdapter(adapterpemilikTanaman)
            adapterpemilikTanaman.setNotifyOnChange(true)
        } else {
            Log.e("data", "kosong")
        }

        spinnerpemilikTanaman.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val selectedProject = adapterView.getItemAtPosition(position) as TempPemilikObjek
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            partyTypdeId = selectedProject.pemilikId
        }

        /*val adapterjenisTanaman = ArrayAdapter<ResponseJenisTanaman>(this@ActicityFormAddObjekTanaman, R.layout.item_spinner, jenisTanaman)
        spinnerJenisTanaman.setAdapter(adapterjenisTanaman)

        val adapterukuranTanaman = ArrayAdapter<ResponseSizePlants>(this@ActicityFormAddObjekTanaman, R.layout.item_spinner, ukuranTanaman)
        spinnersizePlant.setAdapter(adapterukuranTanaman)*/

        /*//data offline kategori tanaman
        val resultRealmKategoriTanaman = realm.where(ResponseDataListKategoriTanamanWithOutParam::class.java).findAllAsync()
        mDataKategoriTanaman = realm.copyFromRealm(resultRealmKategoriTanaman)

        //data offline kategori rumpun
        val resultRealmKategoriRumpun = realm.where(ResponseDataListKategoriRumpunWithOutParam::class.java).findAllAsync()
        mDataKategoriRumpun = realm.copyFromRealm(resultRealmKategoriRumpun)


        btnaddTanaman.setSafeOnClickListener {
            val a: RealmList<Int> = RealmList()
            for (i in adapterFormTanamanMultiple.callbackIdTanaman()) {
                a.add(i)
            }

            Log.d("babab", a.toString())
            realm.beginTransaction()
            onShowLoading()
            val tanaman = realm.createObject(Tanaman::class.java)
            tanaman.jenisTanamanId = a
            realm.commitTransaction()
            Log.d("realm response", "Success")
            Log.d("size response", realm.where(Tanaman::class.java).findAll().size.toString())
            onsuccesspostPlant()
        }

        spinnerJenisTanaman.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseJenisTanaman
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            sendIdJenisTanaman = selectedProject.plantTypeId

            if (sendIdJenisTanaman == 1) {
                tvnamaKategori.text = "Nama Tanaman"
                tvukuranKategori.text = "Ukuran Tanaman"
                tvjmlKategori.text = "Jumlah Tanaman"

                spinnerKategoriTanaman.setText("")
                sendIdKategoriTanaman = 0
                setKategoriTanaman(sendIdJenisTanaman!!)
                //getPresenter()?.getKategoriTanman(sendIdJenisTanaman!!)
            } else {
                tvnamaKategori.text = "Nama Rumpun"
                tvukuranKategori.text = "Ukuran Rumpun"
                tvjmlKategori.text = "Jumlah Rumpun"
                spinnerKategoriTanaman.setText("")
                sendIdKategoriTanaman = 0
                setKategoriTanaman(sendIdJenisTanaman!!)
                //getPresenter()?.getKategoriTanman(sendIdJenisTanaman!!)
            }
            //Toast.makeText(this@ActicityFormAddObjekTanaman, sendIdJenisTanaman.toString(), Toast.LENGTH_SHORT).show()
        }

        spinnerKategoriTanaman.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListKategoriTanaman
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            sendIdKategoriTanaman = selectedProject.plantId

            sendIdKategoriTanaman = when (sendIdJenisTanaman) {
                1 -> {
                    val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListKategoriTanamanWithOutParam
                    val valueSpinner = adapterView.getItemAtPosition(position).toString()
                    selectedProject.getplantId()
                }
                else -> {
                    val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListKategoriRumpunWithOutParam
                    val valueSpinner = adapterView.getItemAtPosition(position).toString()
                    selectedProject.getplantId()
                }
            }

            //Toast.makeText(this@ActicityFormAddObjekTanaman, sendIdKategoriTanaman.toString(), Toast.LENGTH_SHORT).show()
        }

        spinnersizePlant.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseSizePlants
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            sendIdUkuranTanaman = selectedProject.plantSizeId

            //Toast.makeText(this@ActicityFormAddObjekTanaman, sendIdUkuranTanaman.toString(), Toast.LENGTH_SHORT).show()
        }

        elegantNumberButtonTanaman.setOnClickListener(ElegantNumberButton.OnClickListener {
            val number = elegantNumberButtonTanaman.number
            sendjumlahTanaman = number.toInt()
            elegantNumberButtonTanaman.number = number
        })
        elegantNumberButtonTanaman.setOnValueChangeListener { view, oldValue, newValue ->
            sendjumlahTanaman = newValue
            Log.d("ganteng", String.format("oldValue: %d   newValue: %d", oldValue, newValue))
        }*/

        btnaddTanaman.setSafeOnClickListener {
            hideKeyboard()

            val mdataTanaman = adapterFormTanamanMultiple.callbackDataTanaman()
            val dataPhoto: List<DataImagePlants> = adapterTakePicturePlants.returnDataPhoto()
            validPemilikTanaman = partyTypdeId!!.isNotEmpty()

            val midTanaman: ArrayList<String> = ArrayList()
            val mnamaTanaman: ArrayList<String> = ArrayList()
            val midkategoriTanaman: ArrayList<String> = ArrayList()
            val mnamakategoriTanaman: ArrayList<String> = ArrayList()
            val mnamakategoriTanamanOther: ArrayList<String> = ArrayList()
            val midukuranTanaman: ArrayList<String> = ArrayList()
            val mnamaukuranTanaman: ArrayList<String> = ArrayList()
            val mjumlahTanaman: ArrayList<String> = ArrayList()
            loop@ for (i in mdataTanaman.indices) {
                when {
                    mdataTanaman[i].tanamanId!!.isEmpty() || mdataTanaman[i].jumlahTanaman!!.isEmpty() || mdataTanaman[i].ukuranTanamanId!!.isEmpty() -> {
                        validdataTanaman = false
                        dialogValidate!!.titleText = "Gagal menambahkan"
                        dialogValidate!!.contentText = "Data tanaman tidak boleh kosong!"
                        dialogValidate!!.show()
                        break@loop
                    }
                    mdataTanaman[i].kategoriTanamanName!! == "Lain-Nya" && mdataTanaman[i].jumlahTanaman!!.isEmpty() ||
                            mdataTanaman[i].kategoriTanamanName!! == "Lain-Nya" && mdataTanaman[i].kategoriTanamanNameOther!!.isEmpty() -> {
                        validdataTanaman = false
                        dialogValidate!!.titleText = "Gagal menambahkan"
                        dialogValidate!!.contentText = "Data tanaman tidak boleh kosong!"
                        dialogValidate!!.show()
                        break@loop
                    }
                    else -> {
                        midTanaman.add(mdataTanaman[i].tanamanId!!)
                        mnamaTanaman.add(mdataTanaman[i].tanamanName!!)

                        midkategoriTanaman.add(mdataTanaman[i].kategoriTanamanId!!)
                        mnamakategoriTanaman.add(mdataTanaman[i].kategoriTanamanName!!)
                        mnamakategoriTanamanOther.add(mdataTanaman[i].kategoriTanamanNameOther!!)

                        midukuranTanaman.add(mdataTanaman[i].ukuranTanamanId!!)
                        mnamaukuranTanaman.add(mdataTanaman[i].ukuranTanamanName!!)

                        mjumlahTanaman.add(mdataTanaman[i].jumlahTanaman!!)
                        validdataTanaman = true
                    }
                }
            }

            var isvalidPhoto = false
            for (i in dataPhoto.indices) {
                if (dataPhoto[i].imagepathLand!!.toString().isNotEmpty() && dataPhoto[i].imagenameLand!!.toString().isEmpty() ||
                        dataPhoto[i].imagepathLand!!.toString().isEmpty() && dataPhoto[i].imagenameLand!!.toString().isNotEmpty()) {
                    val sweetAlretLoading = SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                    sweetAlretLoading.titleText = "LENGKAPI DATA"
                    sweetAlretLoading.setCancelable(false)
                    sweetAlretLoading.confirmText = "OK"
                    sweetAlretLoading.setConfirmClickListener { sDialog ->
                        sDialog?.let { if (it.isShowing) it.dismiss() }
                    }
                    sweetAlretLoading.show()
                    isvalidPhoto = false
                    break
                } else {
                    isvalidPhoto = true
                    //mimagePath.add(dataPhoto[i].imageEncode.toString())
                    //mimageTitle.add(dataPhoto[i].imageName.toString())
                }
            }

            if (validPemilikTanaman && validdataTanaman && isvalidPhoto) {
                if (mcurrentSession != null && mcurrentSession!!.statusPost == "2") {
                    onShowLoading()
                    realm.executeTransactionAsync({ inrealm ->
                        val tanaman = inrealm.where(Tanaman::class.java).equalTo("TanamanIdTemp", mcurrentSession!!.id).findFirst()
                        tanaman!!.pemilikTanamanId = partyTypdeId
                        tanaman.pemilikTanaman = spinnerpemilikTanaman.text.toString()
                        tanaman.jenisTanamanId = midTanaman.toString()
                        tanaman.jenisTanamanNama = mnamaTanaman.toString()
                        tanaman.namaTanamanId = midkategoriTanaman.toString()
                        tanaman.namaTanaman = mnamakategoriTanaman.toString()
                        tanaman.namaTanamanOther = mnamakategoriTanamanOther.toString()
                        tanaman.ukuranTanamanId = midukuranTanaman.toString()
                        tanaman.ukuranTanaman = mnamaukuranTanaman.toString()
                        tanaman.jumlahTanaman = mjumlahTanaman.toString()
                        tanaman.notes = edNotes.text.toString()

                        val resultdataImagePlants = inrealm.where(DataImagePlants::class.java).findAll()
                        val dataimagePlants = resultdataImagePlants.where().equalTo("imageId", mcurrentSession!!.id).findAll()
                        dataimagePlants?.deleteAllFromRealm()

                        val dataImagePlantPlusId: MutableList<DataImagePlants> = ArrayList()
                        for (element in dataPhoto.indices) {
                            if (dataPhoto[element].imagepathLand.isNotEmpty() && dataPhoto[element].imagenameLand.isNotEmpty()) {
                                val createdataImage = DataImagePlants()
                                createdataImage.imageId = mcurrentSession!!.id
                                createdataImage.imagepathLand = dataPhoto[element].imagepathLand
                                createdataImage.imagenameLand = dataPhoto[element].imagenameLand
                                dataImagePlantPlusId.add(createdataImage)
                            }
                            //Log.d("hasil", dataPhoto.dataImagePhoto!![element].imagenameLand.toString())
                        }

                        if (dataImagePlantPlusId.isNotEmpty()) {
                            val dataPhotoPlants = inrealm.copyToRealmOrUpdate(dataImagePlantPlusId)
                            val dataPhotoPlants2: RealmList<DataImagePlants> = RealmList()
                            for (element in dataPhotoPlants) {
                                dataPhotoPlants2.add(element)
                            }
                            tanaman.dataImagePlants = dataPhotoPlants2
                        }

                        tanaman.isCreate = 2

                    }, {
                        Log.d("tanaman", "success update plant objects")
                        runOnUiThread {
                            onDismissLoading()
                            dialogsuccessTransaksi("""
                                Data
                                berhasil
                                diperbaharui
                            """.trimIndent())
                        }
                    }, {
                        Log.d("tanaman", "failed update plant objects")
                        Log.d("tanaman", it.localizedMessage)
                        runOnUiThread {
                            onDismissLoading()
                        }
                    })

                }
                else {
                    onShowLoading()
                    realm.executeTransactionAsync({ inrealm ->
                        val currentIdNum = inrealm.where(Tanaman::class.java).max("TanamanIdTemp")
                        val nextId: Int
                        nextId = if (currentIdNum == null) {
                            1
                        } else {
                            currentIdNum.toInt() + 1
                        }
                        val tanaman = inrealm.createObject(Tanaman::class.java, nextId)
                        //tanaman.tanamanIdTemp = nextId
                        tanaman.tanamanId = null
                        tanaman.landIdTemp = mLandId
                        tanaman.landId = null
                        tanaman.projectId = mProjectId
                        tanaman.notes = edNotes.text.toString()

                        tanaman!!.pemilikTanamanId = partyTypdeId
                        tanaman.pemilikTanaman = spinnerpemilikTanaman.text.toString()
                        tanaman.jenisTanamanId = midTanaman.toString()
                        tanaman.jenisTanamanNama = mnamaTanaman.toString()
                        tanaman.namaTanamanId = midkategoriTanaman.toString()
                        tanaman.namaTanaman = mnamakategoriTanaman.toString()
                        tanaman.namaTanamanOther = mnamakategoriTanamanOther.toString()
                        tanaman.ukuranTanamanId = midukuranTanaman.toString()
                        tanaman.ukuranTanaman = mnamaukuranTanaman.toString()
                        tanaman.jumlahTanaman = mjumlahTanaman.toString()

                        val dataImagePlantPlusId: MutableList<DataImagePlants> = ArrayList()
                        for (element in dataPhoto.indices) {
                            if (dataPhoto[element].imagepathLand.isNotEmpty() && dataPhoto[element].imagenameLand.isNotEmpty()) {
                                val createdataImage = DataImagePlants()
                                createdataImage.imageId = nextId
                                createdataImage.imagepathLand = dataPhoto[element].imagepathLand
                                createdataImage.imagenameLand = dataPhoto[element].imagenameLand
                                dataImagePlantPlusId.add(createdataImage)
                            }

                            //Log.d("hasil", dataPhoto.dataImagePhoto!![element].imagenameLand.toString())
                        }

                        if (dataImagePlantPlusId.isNotEmpty()) {
                            val dataPhotoPlants = inrealm.copyToRealmOrUpdate(dataImagePlantPlusId)
                            val dataPhotoPlants2: RealmList<DataImagePlants> = RealmList()
                            for (element in dataPhotoPlants) {
                                dataPhotoPlants2.add(element)
                            }
                            tanaman.dataImagePlants = dataPhotoPlants2
                        }
                        tanaman.isCreate = 1

                        /*val projectLandImage = inrealm.copyToRealmOrUpdate(dataPhoto)
                        val projectLandImage2: RealmList<DataImagePlants> = RealmList()
                        for(element in projectLandImage){
                            projectLandImage2.add(element)
                        }*/
                        /*for(element in dataImageLand.dataImagePhoto!!){
                            projectLandImage.add(element)
                        }*/

                    }, {
                        Log.d("tanaman", "success adds plant objects")
                        runOnUiThread {
                            onDismissLoading()
                            onsuccesspostPlant()
                        }
                    }, {
                        Log.d("tanaman", "failed adds plant objects")
                        Log.d("tanaman", it.localizedMessage)
                        runOnUiThread {
                            onDismissLoading()
                        }
                    })

                }
            } else {
                val sweetAlretLoading = SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                sweetAlretLoading.titleText = "Gagal menambahkan"
                sweetAlretLoading.contentText = "Lengkapi data!"
                sweetAlretLoading.setCancelable(false)
                sweetAlretLoading.confirmText = "OK"
                sweetAlretLoading.setConfirmClickListener { sDialog ->
                    sDialog?.let { if (it.isShowing) it.dismiss() }
                }
                sweetAlretLoading.show()
            }


            /*sendmidJenisTanaman!!.clear()
            sendmidKategoriTanaman!!.clear()
            sendmidUkuranTanaman!!.clear()
            sendmvalueNameTanaman!!.clear()
            sendmvalueNameKategoriTanaman!!.clear()
            sendmvalueNameUkuranTanaman!!.clear()
            sendmjumlahTanaman!!.clear()

            for (i in dataPhoto.indices) {
                Log.d("baba", dataPhoto[i].imagepathLand)
                Log.d("baba2", dataPhoto[i].imagenameLand)
            }

            for (i in 0 until adapterFormTanamanMultiple.callbackIdTanaman().size) {
                if (adapterFormTanamanMultiple.callbackIdTanaman()[i].isEmpty()) {
                    break
                } else {
                    validIdJenisTanaman = true
                }
                sendmidJenisTanaman!!.add(adapterFormTanamanMultiple.callbackIdTanaman()[i])
            }

            for (i in 0 until adapterFormTanamanMultiple.callbackIdKategoriTanaman().size) {
                if (adapterFormTanamanMultiple.callbackIdKategoriTanaman()[i].isEmpty()) {
                    validIdKategoriTanaman = false
                    break
                } else {
                    validIdKategoriTanaman = true
                }
                sendmidUkuranTanaman!!.add(adapterFormTanamanMultiple.callbackIdKategoriTanaman()[i])
            }


            for (i in 0 until adapterFormTanamanMultiple.callbackIdUkuranTanaman().size) {
                if (adapterFormTanamanMultiple.callbackIdUkuranTanaman()[i].isEmpty()) {
                    validIdUkuranTanaman = false
                    break
                } else {
                    validIdUkuranTanaman = true
                }
                sendmidKategoriTanaman!!.add(adapterFormTanamanMultiple.callbackIdUkuranTanaman()[i])
            }

            for (i in 0 until adapterFormTanamanMultiple.callbackJmlhTanaman().size) {
                if (adapterFormTanamanMultiple.callbackJmlhTanaman()[i].isEmpty()) {
                    validJmlhTanaman = false
                    break
                } else {
                    validJmlhTanaman = true
                }
                sendmjumlahTanaman!!.add(adapterFormTanamanMultiple.callbackJmlhTanaman()[i])
            }

            adapterFormTanamanMultiple.callbackValueNameTanaman().forEach {
                if (it.isNotEmpty()) {
                    sendmvalueNameTanaman!!.add(it)
                }
            }

            adapterFormTanamanMultiple.callbackValueNameKategoriTanaman().forEach {
                if (it.isNotEmpty()) {
                    sendmvalueNameKategoriTanaman!!.add(it)
                }
            }

            adapterFormTanamanMultiple.callbackValueNameUkuranTanaman().forEach {
                if (it.isNotEmpty()) {
                    sendmvalueNameUkuranTanaman!!.add(it)
                }
            }


            *//*adapterFormTanamanMultiple.callbackIdTanaman().forEach {
                if (it.isNotEmpty()) {
                    sendmidJenisTanaman!!.add(it)
                }
            }

            adapterFormTanamanMultiple.callbackIdKategoriTanaman().forEach {
                if (it.isNotEmpty()) {
                    sendmidKategoriTanaman!!.add(it)
                }
            }

            adapterFormTanamanMultiple.callbackIdUkuranTanaman().forEach {
                if (it.isNotEmpty()) {
                    sendmidUkuranTanaman!!.add(it)
                }
            }

            adapterFormTanamanMultiple.callbackValueNameTanaman().forEach {
                if (it.isNotEmpty()) {
                    sendmvalueNameTanaman!!.add(it)
                }
            }

            adapterFormTanamanMultiple.callbackValueNameKategoriTanaman().forEach {
                if (it.isNotEmpty()) {
                    sendmvalueNameKategoriTanaman!!.add(it)
                }
            }

            adapterFormTanamanMultiple.callbackValueNameUkuranTanaman().forEach {
                if (it.isNotEmpty()) {
                    sendmvalueNameUkuranTanaman!!.add(it)
                }
            }

            adapterFormTanamanMultiple.callbackJmlhTanaman().forEach {
                if (it.isNotEmpty()) {
                    sendmjumlahTanaman!!.add(it)
                }
            }*//*

            if (isvalidPhoto && validPemilikTanaman && validIdJenisTanaman && validIdKategoriTanaman && validIdUkuranTanaman && validJmlhTanaman) {
                if (mcurrentSession != null && mcurrentSession!!.statusPost == "2") {
                    onShowLoading()
                    realm.executeTransactionAsync({ inrealm ->
                        val tanaman = inrealm.where(Tanaman::class.java).equalTo("TanamanIdTemp", mcurrentSession!!.id).findFirst()
                        tanaman!!.pemilikTanamanId = partyTypdeId
                        tanaman.pemilikTanaman = spinnerpemilikTanaman.text.toString()
                        tanaman.jenisTanamanId = sendmidJenisTanaman.toString()
                        tanaman.jenisTanamanNama = sendmvalueNameTanaman.toString()
                        tanaman.namaTanamanId = sendmidKategoriTanaman.toString()
                        tanaman.namaTanaman = sendmvalueNameKategoriTanaman.toString()
                        tanaman.ukuranTanamanId = sendmidUkuranTanaman.toString()
                        tanaman.ukuranTanaman = sendmvalueNameUkuranTanaman.toString()
                        tanaman.jumlahTanaman = sendmjumlahTanaman.toString()

                        val resultdataImagePlants = inrealm.where(DataImagePlants::class.java).findAll()
                        val dataimagePlants = resultdataImagePlants.where().equalTo("imageId", mcurrentSession!!.id).findAll()
                        dataimagePlants?.deleteAllFromRealm()

                        val dataImagePlantPlusId: MutableList<DataImagePlants> = ArrayList()
                        for (element in dataPhoto.indices) {
                            val createdataImage = DataImagePlants()
                            createdataImage.imageId = mcurrentSession!!.id
                            createdataImage.imagepathLand = dataPhoto[element].imagepathLand
                            createdataImage.imagenameLand = dataPhoto[element].imagenameLand
                            dataImagePlantPlusId.add(createdataImage)
                            //Log.d("hasil", dataPhoto.dataImagePhoto!![element].imagenameLand.toString())
                        }

                        val dataPhotoPlants = inrealm.copyToRealmOrUpdate(dataImagePlantPlusId)
                        val dataPhotoPlants2: RealmList<DataImagePlants> = RealmList()
                        for (element in dataPhotoPlants) {
                            dataPhotoPlants2.add(element)
                        }
                        tanaman.dataImagePlants = dataPhotoPlants2
                        tanaman.isCreate = 2

                    }, {
                        Log.d("tanaman", "success update plant objects")
                        runOnUiThread {
                            onDismissLoading()
                            onsuccesspostPlant()
                        }
                    }, {
                        Log.d("tanaman", "failed update plant objects")
                        Log.d("tanaman", it.localizedMessage)
                        runOnUiThread {
                            onDismissLoading()
                        }
                    })

                } else {
                    onShowLoading()
                    realm.executeTransactionAsync({ inrealm ->
                        val currentIdNum = inrealm.where(Tanaman::class.java).max("TanamanIdTemp")
                        val nextId: Int
                        nextId = if (currentIdNum == null) {
                            1
                        } else {
                            currentIdNum.toInt() + 1
                        }
                        val tanaman = inrealm.createObject(Tanaman::class.java, nextId)
                        //tanaman.tanamanIdTemp = nextId
                        tanaman.tanamanId = null
                        tanaman.landIdTemp = mLandId
                        tanaman.landId = null
                        tanaman.projectId = mProjectId

                        tanaman!!.pemilikTanamanId = partyTypdeId
                        tanaman.pemilikTanaman = spinnerpemilikTanaman.text.toString()
                        tanaman.jenisTanamanId = sendmidJenisTanaman.toString()
                        tanaman.jenisTanamanNama = sendmvalueNameTanaman.toString()
                        tanaman.namaTanamanId = sendmidKategoriTanaman.toString()
                        tanaman.namaTanaman = sendmvalueNameKategoriTanaman.toString()
                        tanaman.ukuranTanamanId = sendmidUkuranTanaman.toString()
                        tanaman.ukuranTanaman = sendmvalueNameUkuranTanaman.toString()
                        tanaman.jumlahTanaman = sendmjumlahTanaman.toString()

                        val dataImagePlantPlusId: MutableList<DataImagePlants> = ArrayList()
                        for (element in dataPhoto.indices) {
                            val createdataImage = DataImagePlants()
                            createdataImage.imageId = nextId
                            createdataImage.imagepathLand = dataPhoto[element].imagepathLand
                            createdataImage.imagenameLand = dataPhoto[element].imagenameLand
                            dataImagePlantPlusId.add(createdataImage)
                            //Log.d("hasil", dataPhoto.dataImagePhoto!![element].imagenameLand.toString())
                        }

                        val dataPhotoPlants = inrealm.copyToRealmOrUpdate(dataImagePlantPlusId)
                        val dataPhotoPlants2: RealmList<DataImagePlants> = RealmList()
                        for (element in dataPhotoPlants) {
                            dataPhotoPlants2.add(element)
                        }
                        tanaman.dataImagePlants = dataPhotoPlants2

                        *//*val projectLandImage = inrealm.copyToRealmOrUpdate(dataPhoto)
                        val projectLandImage2: RealmList<DataImagePlants> = RealmList()
                        for(element in projectLandImage){
                            projectLandImage2.add(element)
                        }*//*
                        *//*for(element in dataImageLand.dataImagePhoto!!){
                            projectLandImage.add(element)
                        }*//*

                        tanaman.isCreate = 1
                    }, {
                        Log.d("tanaman", "success adds plant objects")
                        runOnUiThread {
                            onDismissLoading()
                            onsuccesspostPlant()
                        }
                    }, {
                        Log.d("tanaman", "failed adds plant objects")
                        Log.d("tanaman", it.localizedMessage)
                        runOnUiThread {
                            onDismissLoading()
                        }
                    })

                }
//                    getPresenter()?.postPlant(Preference.auth.dataUser?.mobileUserId!!,
//                            projectLandId!!, partyTypdeId!!, sendIdKategoriTanaman!!,
//                            sendIdJenisTanaman!!, sendIdUkuranTanaman!!, edjumlahTanaman.text.toString().toInt(),
//                            Preference.accessToken)
            } else {
                val sweetAlretLoading = SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                sweetAlretLoading.titleText = "Gagal menambahkan"
                sweetAlretLoading.contentText = "Lengkapi data!"
                sweetAlretLoading.setCancelable(false)
                sweetAlretLoading.confirmText = "OK"
                sweetAlretLoading.setConfirmClickListener { sDialog ->
                    sDialog?.let { if (it.isShowing) it.dismiss() }
                }
                sweetAlretLoading.show()
            }*/
        }

        showDialog()
    }

    private fun showDialog() {
        if (mcurrentSession != null) {
            if (mcurrentSession!!.id != null && mcurrentSession!!.statusPost == "3") {
                mDialogView.btnYesDelete.setOnClickListener {
                    realm.executeTransactionAsync({ inRealm ->
                        val resultRealmBanguanan = inRealm.where(Tanaman::class.java).findAll()
                        val dataParty = resultRealmBanguanan.where()
                            .equalTo("TanamanIdTemp", mcurrentSession!!.id)
                            .and()
                            .equalTo("LandIdTemp", mcurrentSession!!.id2!!)
                            .findFirst()

                        val resultdataImageBuilding = inRealm.where(DataImagePlants::class.java).findAll()
                        val dataimageBuilding = resultdataImageBuilding.where().equalTo("imageId", mcurrentSession!!.id!!).findAll()
                        dataimageBuilding?.deleteAllFromRealm()
                        dataParty?.deleteFromRealm()
                    }, {
                        Log.d("delete", "onSuccess : delete single object")
                        mdialogConfirmDelete.dismiss()
                        finish()
                    }, {
                        Log.d("delete", "onFailed : ${it.localizedMessage}")
                        Log.d("delete", "onFailed : delete single object")
                    })
                }

                mDialogView.btnNoDelete.setOnClickListener {
                    mdialogConfirmDelete.dismiss()
                    finish()
                }
                mdialogConfirmDelete.show()
                /*val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
                    when (which) {
                        DialogInterface.BUTTON_POSITIVE -> {
                            val bangunan = realm.where(Bangunan::class.java).equalTo("BangunanIdTemp", session.id.toInt()).findFirst()
                            realm.beginTransaction()
                            bangunan!!.deleteFromRealm()
                            realm.commitTransaction()
                            dialog.dismiss()
                            finish()
                        }

                        DialogInterface.BUTTON_NEGATIVE -> {
                            dialog.dismiss()
                            finish()
                        }
                    }
                }

                val builder = android.support.v7.app.AlertDialog.Builder(this)
                builder.setTitle("SIOJT")
                        .setMessage("Yakin akan dihapus?")
                        .setPositiveButton("Ya", dialogClickListener)
                        .setNegativeButton("Tidak", dialogClickListener)
                        .setIcon(ContextCompat.getDrawable(this@ActicityFormAddObjekBangunan, R.drawable.ic_logo_splash)!!)
                        .show()*/
            }
        }

    }

    private fun setupForEdit() {
        if (mcurrentSession != null) {
            if (mcurrentSession!!.id != null && mcurrentSession!!.statusPost == "2") {
                val results: Tanaman? = realm.where(Tanaman::class.java).equalTo("TanamanIdTemp", mcurrentSession!!.id).findFirst()
                if (results != null) {
                    partyTypdeId = results.pemilikTanamanId
                    spinnerpemilikTanaman.setText(results.pemilikTanaman)

                    val jenisTanamanId = results.jenisTanamanId
                    val jenisTanamanIdreplace1 = jenisTanamanId.replace("[", "['")
                    val jenisTanamanIdreplace2 = jenisTanamanIdreplace1.replace("]", "']")
                    val jenisTanamanIdreplace3 = jenisTanamanIdreplace2.replace(", ", "','")

                    val jenisTanaman = results.jenisTanamanNama
                    val jenisTanamanreplace1 = jenisTanaman.replace("[", "['")
                    val jenisTanamanreplace2 = jenisTanamanreplace1.replace("]", "']")
                    val jenisTanamanreplace3 = jenisTanamanreplace2.replace(", ", "','")

                    val kategoriIdTanaman = results.namaTanamanId
                    val kategoriIdTanamanreplace1 = kategoriIdTanaman.replace("[", "['")
                    val kategoriIdTanamanreplace2 = kategoriIdTanamanreplace1.replace("]", "']")
                    val kategoriIdTanamanreplace3 = kategoriIdTanamanreplace2.replace(", ", "','")

                    val kategoriTanaman = results.namaTanaman
                    val kategoriTanamanreplace1 = kategoriTanaman.replace("[", "['")
                    val kategoriTanamanreplace2 = kategoriTanamanreplace1.replace("]", "']")
                    val kategoriTanamanreplace3 = kategoriTanamanreplace2.replace(", ", "','")

                    val kategoriTanamanOther = results.namaTanamanOther
                    val kategoriTanamanOther1 = kategoriTanamanOther.replace("[", "['")
                    val kategoriTanamanOther2 = kategoriTanamanOther1.replace("]", "']")
                    val kategoriTanamanOther3 = kategoriTanamanOther2.replace(", ", "','")

                    val ukuranIdTanaman = results.ukuranTanamanId
                    val ukuranIdTanamanreplace1 = ukuranIdTanaman.replace("[", "['")
                    val ukuranIdTanamanreplace2 = ukuranIdTanamanreplace1.replace("]", "']")
                    val ukuranIdTanamanreplace3 = ukuranIdTanamanreplace2.replace(", ", "','")

                    val ukuranTanaman = results.ukuranTanaman
                    val ukuranTanamanreplace1 = ukuranTanaman.replace("[", "['")
                    val ukuranTanamanreplace2 = ukuranTanamanreplace1.replace("]", "']")
                    val ukuranTanamanreplace3 = ukuranTanamanreplace2.replace(", ", "','")

                    val jumlahTanaman = results.jumlahTanaman
                    val jumlahTanamanreplace1 = jumlahTanaman.replace("[", "['")
                    val jumlahTanamanreplace2 = jumlahTanamanreplace1.replace("]", "']")
                    val jumlahTanamanreplace3 = jumlahTanamanreplace2.replace(", ", "','")

                    val gson = Gson()
                    val jenisTanamanIdConverter = gson.fromJson(jenisTanamanIdreplace3, Array<String>::class.java).toList()
                    val jenisTanamanConverter = gson.fromJson(jenisTanamanreplace3, Array<String>::class.java).toList()

                    val kategoriIdTanamanConverter = gson.fromJson(kategoriIdTanamanreplace3, Array<String>::class.java).toList()
                    val kategoriTanamanConverter = gson.fromJson(kategoriTanamanreplace3, Array<String>::class.java).toList()
                    val kategoriTanamanOtherConverter = gson.fromJson(kategoriTanamanOther3, Array<String>::class.java).toList()

                    val ukuaranIdTanamanConverter = gson.fromJson(ukuranIdTanamanreplace3, Array<String>::class.java).toList()
                    val ukuaranTanamanConverter = gson.fromJson(ukuranTanamanreplace3, Array<String>::class.java).toList()
                    val jumlahTanamanConverter = gson.fromJson(jumlahTanamanreplace3, Array<String>::class.java).toList()

                    for (i in jenisTanamanIdConverter.indices) {
                        val mdataTanaman = DataTanaman()
                        mdataTanaman.tanamanId = jenisTanamanIdConverter[i] //id
                        mdataTanaman.tanamanName = jenisTanamanConverter[i] //name

                        mdataTanaman.kategoriTanamanId = kategoriIdTanamanConverter[i] //id
                        mdataTanaman.kategoriTanamanName = kategoriTanamanConverter[i] //name
                        mdataTanaman.kategoriTanamanNameOther = kategoriTanamanOtherConverter[i] //name_other

                        mdataTanaman.ukuranTanamanId = ukuaranIdTanamanConverter[i]//id
                        mdataTanaman.ukuranTanamanName = ukuaranTanamanConverter[i] //name

                        mdataTanaman.jumlahTanaman = jumlahTanamanConverter[i] //jumlah

                        adapterFormTanamanMultiple.adddataTanaman(mdataTanaman)
                        adapterFormTanamanMultiple.notifyDataSetChanged()
                    }
                    //Log.d("mantap", jenisTanamanConverter.toString())
                    val mdatafotoTanaman = realm.copyFromRealm(results.dataImagePlants)
                    if (mdatafotoTanaman.isNotEmpty()) {
                        for (i in 0 until mdatafotoTanaman.size) {
                            adapterTakePicturePlants.addItems(mdatafotoTanaman[i])
                            adapterTakePicturePlants.notifyDataSetChanged()
                        }
                    } else {
                        val dataImage = DataImagePlants()
                        dataImage.imageId = null
                        dataImage.imagepathLand = ""
                        dataImage.imagenameLand = ""
                        adapterTakePicturePlants.addItems(dataImage)
                        adapterTakePicturePlants.notifyDataSetChanged()
                    }

                }

            }
        } else {
            val dataTanaman = DataTanaman()
            dataTanaman.tanamanId = ""
            dataTanaman.tanamanName = ""
            dataTanaman.kategoriTanamanId = ""
            dataTanaman.kategoriTanamanName = ""
            dataTanaman.kategoriTanamanNameOther = ""
            dataTanaman.ukuranTanamanId = ""
            dataTanaman.ukuranTanamanName = ""
            dataTanaman.jumlahTanaman = ""
            adapterFormTanamanMultiple.adddataTanaman(dataTanaman)
            adapterFormTanamanMultiple.notifyDataSetChanged()

            val dataImage = DataImagePlants()
            dataImage.imageId = null
            dataImage.imagepathLand = ""
            dataImage.imagenameLand = ""
            adapterTakePicturePlants.addItems(dataImage)
            adapterTakePicturePlants.notifyDataSetChanged()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun tampilkanData(results: Tanaman) {
        /*spinnerpemilikTanaman.setText(results.pemilikTanaman)
        partyTypdeId = results.pemilikTanamanId
        spinnerJenisTanaman.setText(results.jenisTanamanNama)
        sendIdJenisTanaman = results.jenisTanamanId
        spinnerKategoriTanaman.setText(results.namaTanaman)
        sendIdKategoriTanaman = results.namaTanamanId
        spinnersizePlant.setText(results.ukuranTanaman)
        sendIdUkuranTanaman = results.ukuranTanamanId
        edjumlahTanaman.setText(results.jumlahTanaman.toString())

        Log.d("babab", sendIdJenisTanaman.toString())

        if (sendIdJenisTanaman == 1) {
            tvnamaKategori.text = "Nama Tanaman"
            tvukuranKategori.text = "Ukuran Tanaman"
            tvjmlKategori.text = "Jumlah Tanaman"
            setKategoriTanaman(sendIdJenisTanaman)
        } else {
            tvnamaKategori.text = "Nama Rumpun"
            tvukuranKategori.text = "Ukuran Rumpun"
            tvjmlKategori.text = "Jumlah Rumpun"
            setKategoriTanaman(sendIdJenisTanaman)
        }*/
    }

    private fun getPresenter(): FormAddObjekTanamanPresenter? {
        formAddObjekTanamanPresenter = FormAddObjekTanamanPresenter()
        formAddObjekTanamanPresenter.onAttach(this)
        return formAddObjekTanamanPresenter
    }

    private fun validate(): Boolean {
        val valid: Boolean
        /*if (sendIdJenisTanaman == 0 || sendIdKategoriTanaman == 0 || sendIdUkuranTanaman == 0 || edjumlahTanaman.text.toString().trim().isEmpty()) {

            val sweetAlretLoading = SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
            sweetAlretLoading.titleText = "LENGKAPI DATA"
            sweetAlretLoading.setCancelable(false)
            sweetAlretLoading.confirmText = "OK"
            sweetAlretLoading.setConfirmClickListener { sDialog ->
                sDialog?.let { if (it.isShowing) it.dismiss() }
            }
            sweetAlretLoading.show()

            valid = false

        } else {
            valid = true
        }*/
        val sweetAlretLoading = SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
        sweetAlretLoading.setCancelable(false)
        sweetAlretLoading.confirmText = "OK"
        sweetAlretLoading.setConfirmClickListener { sDialog ->
            sDialog?.let { if (it.isShowing) it.dismiss() }
        }
        /*if (midJenisTanaman.isNullOrEmpty() ||
                midKategoriTanaman.isNullOrEmpty() || midUkuranTanaman.isNullOrEmpty() ||
                mvalueNameTanaman!!.isEmpty() || mvalueNameKategoriTanaman!!.isEmpty() ||
                mvalueNameUkuranTanaman!!.isEmpty() || mjumlahTanaman!!.isEmpty()) {

            sweetAlretLoading.titleText = "Lengkapi data"
            sweetAlretLoading.show()
            valid = false

        } else*/
        if (partyTypdeId!!.isEmpty()) {
            sweetAlretLoading.titleText = "Pilih pemilik objek"
            sweetAlretLoading.show()
            spinnerpemilikTanaman.requestFocus()
            valid = false
        } else {
            valid = true
        }

        return valid
    }

    private fun setKategoriTanaman(idJenisTanaman: Int?) {
        /*when (idJenisTanaman) {
            1 -> {
                val adapterKategoriTanaman = ArrayAdapter<ResponseDataListKategoriTanamanWithOutParam>(this@ActicityFormAddObjekTanaman,
                        R.layout.item_spinner, mDataKategoriTanaman)
                adapterKategoriTanaman.setNotifyOnChange(true)
                spinnerKategoriTanaman.setAdapter(adapterKategoriTanaman)
                adapterKategoriTanaman.setNotifyOnChange(true)
            }
            else -> {
                val adapterKategoriRumpun = ArrayAdapter<ResponseDataListKategoriRumpunWithOutParam>(this@ActicityFormAddObjekTanaman,
                        R.layout.item_spinner, mDataKategoriRumpun)
                adapterKategoriRumpun.setNotifyOnChange(true)
                spinnerKategoriTanaman.setAdapter(adapterKategoriRumpun)
                adapterKategoriRumpun.setNotifyOnChange(true)
            }
        }*/
    }

    private fun onProfileImageClick() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions()
                        }
                        if (report.isAnyPermissionPermanentlyDenied) {
                            showSettingsDialog()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).check()
    }


    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, object : ImagePickerActivity.PickerOptionListener {
            override fun onTakeCameraSelected() {
                launchCameraIntent()
            }

            override fun onChooseGallerySelected() {
                launchGalleryIntent()
            }
        })
    }

    private fun launchCameraIntent() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE)
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    private fun launchGalleryIntent() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE)
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.dialog_permission_title))
        builder.setMessage(getString(R.string.dialog_permission_message))
        builder.setPositiveButton(getString(R.string.go_to_settings)) { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(getString(android.R.string.cancel)) { dialog, which -> dialog.cancel() }
        builder.show()
    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    fun getEncoded64ImageStringFromBitmap(bitmap: Bitmap): String {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val byteFormat = stream.toByteArray()

        return Base64.encodeToString(byteFormat, Base64.DEFAULT)
    }

    fun addItem() {
        val dataTanaman = DataTanaman()
        dataTanaman.tanamanId = ""
        dataTanaman.tanamanName = ""
        dataTanaman.kategoriTanamanId = ""
        dataTanaman.kategoriTanamanName = ""
        dataTanaman.kategoriTanamanNameOther = ""
        dataTanaman.ukuranTanamanId = ""
        dataTanaman.ukuranTanamanName = ""
        dataTanaman.jumlahTanaman = ""
        adapterFormTanamanMultiple.adddataTanaman(dataTanaman)
        adapterFormTanamanMultiple.notifyDataSetChanged()
        rvFormTanaman.scrollToPosition(adapterFormTanamanMultiple.itemCount - 1)
    }

    fun addphotoItem() {
        val dataImage = DataImagePlants()
        dataImage.imageId = null
        dataImage.imagepathLand = ""
        dataImage.imagenameLand = ""
        adapterTakePicturePlants.addItems(dataImage)
        adapterTakePicturePlants.notifyDataSetChanged()
        rvlistphotoTanaman.scrollToPosition(adapterTakePicturePlants.itemCount - 1)
    }

    override fun OnItemImageClick(position: Int) {
        mpositionImageClick = position
        onProfileImageClick()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data!!.getParcelableExtra<Uri>("path")
                try {
                    // You can update this bitmap to your server
                    val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, uri)

                    val dataImage = DataImagePlants()
                    dataImage.imageId = null
                    dataImage.imagepathLand = uri.path
                    dataImage.imagenameLand = ""
                    adapterTakePicturePlants.updateDateAfterClickImage(mpositionImageClick!!, dataImage)
                    adapterTakePicturePlants.notifyDataSetChanged()

                    Log.d(TAG, "Image cache path: $uri")
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onsuccessgetlistdataPemilik(responseDataListPemilik: MutableList<ResponseDataListPemilik>) {
        Log.e("responsePemilik", responseDataListPemilik.toString())
        val adapterpemilikTanaman = ArrayAdapter<ResponseDataListPemilik>(this@ActivityFormAddObjekTanaman,
                R.layout.item_spinner, responseDataListPemilik)
        adapterpemilikTanaman.setNotifyOnChange(true)
        spinnerpemilikTanaman.setAdapter(adapterpemilikTanaman)
        adapterpemilikTanaman.setNotifyOnChange(true)
    }

    override fun onsuccessgetKategoriTanman(responseDataListKategoriTanaman: MutableList<ResponseDataListKategoriTanaman>) {
        /*val adapterKategoriTanaman = ArrayAdapter<ResponseDataListKategoriTanaman>(this@ActicityFormAddObjekTanaman,
                R.layout.item_spinner, responseDataListKategoriTanaman)
        adapterKategoriTanaman.setNotifyOnChange(true)
        spinnerKategoriTanaman.setAdapter(adapterKategoriTanaman)
        adapterKategoriTanaman.setNotifyOnChange(true)*/

    }

    private fun dialogsuccessTransaksi(title_dialog: String) {
        val dialogPopup = DialogSuccessAddTanaman.newInstance(title_dialog)
        dialogPopup.show(supportFragmentManager, "Dalog_popup")
    }

    override fun onsuccesspostPlant() {
        val dialogPopup = DialogSuccessAddTanaman()
        dialogPopup.show(supportFragmentManager, "Dalog_popup")
    }


    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    override fun onDestroy() {
        super.onDestroy()
        getPresenter()?.onDetach()
    }

}