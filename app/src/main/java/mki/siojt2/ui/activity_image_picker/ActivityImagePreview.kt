package mki.siojt2.ui.activity_image_picker

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_gallery_preview.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import java.io.ByteArrayOutputStream

class ActivityImagePreview : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery_preview)

        /* set toolbar*/
        if (toolbarimagePreview != null) {
            setSupportActionBar(toolbarimagePreview)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarimagePreview.setNavigationOnClickListener {
            finish()
        }
        toolbarimagePreview.title = "Preview"

        val dataImage = intent?.extras!!.getString("bitmap")
        val uriImage = Uri.parse(dataImage)
        //Log.d("babab", uriImage.toString())

        val options = BitmapFactory.Options()
        options.inPreferredConfig = Bitmap.Config.ARGB_8888
        val decodeImage = BitmapFactory.decodeFile(dataImage, options)

        Glide.with(this)
                .load(decodeImage)
                .into(GalleryPreviewImg)
    }


    override fun onFragmentAttached() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFragmentDetached(tag: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityImagePreview::class.java)
        }
    }
}