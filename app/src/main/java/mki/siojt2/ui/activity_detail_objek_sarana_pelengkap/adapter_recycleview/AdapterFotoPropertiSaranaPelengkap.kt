package mki.siojt2.ui.activity_detail_objek_sarana_pelengkap.adapter_recycleview

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import androidx.recyclerview.widget.RecyclerView
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_foto_properti_horizontal.view.*
import mki.siojt2.R
import mki.siojt2.base.BaseViewHolder
import mki.siojt2.model.local_save_image.DataImageFacilities
import mki.siojt2.ui.activity_image_picker.ActivityImagePreview


class AdapterFotoPropertiSaranaPelengkap(context: Context) : androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder>() {

    private val mContext: Context? = context
    val mDataset: MutableList<DataImageFacilities>? = ArrayList()
    var rowLayout = R.layout.item_foto_properti_horizontal
    var emptyrowLayout = R.layout.item_empty_view
    var pos = 0


    private var mCallback: Callback? = null

    fun setCallback(callback: Callback) {
        mCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            VIEW_TYPE_NORMAL -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(rowLayout, parent, false))
            VIEW_TYPE_EMPTY -> EmptyViewHolder(
                    LayoutInflater.from(parent.context).inflate(R.layout.item_empty_view, parent, false))
            else -> EmptyViewHolder(LayoutInflater.from(parent.context).inflate(emptyrowLayout, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (mDataset != null && mDataset.isNotEmpty()) {
            VIEW_TYPE_NORMAL
        } else {
            VIEW_TYPE_EMPTY
        }
    }

    override fun getItemCount(): Int {
        return if (mDataset!!.isNotEmpty()) {
            mDataset.size
        } else {
            1
        }
    }

    /*inner class RuangViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var r_no: TextView = itemView.findViewById(R.id.rowNo) as TextView
        var r_tvTitle: TextView = itemView.findViewById(R.id.tvTitle) as TextView
        var r_tvContent1: TextView = itemView.findViewById(R.id.tvContent1) as TextView
        var r_tvContent2: TextView = itemView.findViewById(R.id.tvContent2) as TextView

    }*/

    fun clear() {
        this.mDataset!!.clear()
    }

    fun addItems(data: List<DataImageFacilities>) {
        this.mDataset!!.addAll(data)
        notifyDataSetChanged()
    }

    interface Callback {
        fun onRepoEmptyViewRetryClick()
    }

    inner class ViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }

        override fun onBind(position: Int) {
            super.onBind(position)

            val dataList = mDataset!![position]
            inflateData(dataList)

            //val imageAsBytes = Base64.decode(dataList.imagepathLand!!.toByteArray(), Base64.DEFAULT)
            //val decodeImage = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.size)

            //set image title
            itemView.titlefotoPropertiTanaman.text = dataList.imagenameLand!!

            //set image
            val options = BitmapFactory.Options()
            options.inPreferredConfig = Bitmap.Config.ARGB_8888
            val decodeImage = BitmapFactory.decodeFile(dataList.imagepathLand!!, options)

            Glide.with(mContext!!)
                    .load(decodeImage)
                    .apply(RequestOptions()
                            .placeholder(R.color.colorPrimary)
                            .centerCrop()
                    ) // cache both original
                    .into(itemView.imgProperti)


            itemView.imgProperti.setOnClickListener {
                val intent = ActivityImagePreview.getStartIntent(mContext)
                intent.putExtra("bitmap", dataList.imagepathLand)
                mContext.startActivity(intent)
            }
        }

        private fun inflateData(dataList: DataImageFacilities) {

        }
    }

    inner class EmptyViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }


    }

    companion object {
        const val VIEW_TYPE_EMPTY = 0
        const val VIEW_TYPE_NORMAL = 1
    }
}
