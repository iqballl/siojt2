package mki.siojt2.ui.activity_detail_objek_sarana_pelengkap

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import com.google.gson.Gson
import io.realm.Realm
import io.realm.RealmChangeListener
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_detail_objek_sarana_pelengkap.*
import kotlinx.android.synthetic.main.activity_form_add_sarana_lain.*
import kotlinx.android.synthetic.main.item_daftar_objek_sarana_pelengkap.view.*
import kotlinx.android.synthetic.main.toolbar_main.toolbarCustom
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.model.local_save_image.DataImageFacilities
import mki.siojt2.model.localsave.SaranaLain
import mki.siojt2.model.localsave.Session
import mki.siojt2.model.localsave.Tanaman
import mki.siojt2.model.second_development.*
import mki.siojt2.ui.activity_detail_objek_sarana_pelengkap.adapter_recycleview.AdapterFotoPropertiSaranaPelengkap
import mki.siojt2.ui.activity_detail_subjek.presenter.DetailSubjekPresenter
import mki.siojt2.utils.realm.RealmController

class ActivityDetailObjekSaranaPelengkap : BaseActivity() {

    private lateinit var presenter: DetailSubjekPresenter
    private lateinit var session: Session
    private lateinit var realm: Realm

    private var LandId: Int? = 0
    private var saranaidTemp: Int? = 0
    private var ProjectId: Int? = 0
    lateinit var adapterfotoSaranaPelengkap: AdapterFotoPropertiSaranaPelengkap


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_objek_sarana_pelengkap)

        session = Session(this)
        val historyData = intent?.extras?.getString("data_current_fasilitas")
        val toJson = Gson().fromJson(historyData, Array<String>::class.java).toList()

        LandId = toJson[0].toInt()
        saranaidTemp = toJson[1].toInt()
        ProjectId = toJson[2].toInt()

        /* set toolbar */
        if (toolbarCustom != null) {
            setSupportActionBar(toolbarCustom)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarCustomDetailTanaman.setNavigationOnClickListener {
            finish()
        }

        customtvToolbarTitleDetailTanaman.text = "Detail Sarana Pelengkap"
        customtvToolbarTitleDetailTanaman.setTextColor(Color.parseColor("#3DC528"))

        /* set recycleview photo fasilites*/
        adapterfotoSaranaPelengkap = AdapterFotoPropertiSaranaPelengkap(this)
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        rlfotopropertiSaranaPelengkap.adapter = adapterfotoSaranaPelengkap
        rlfotopropertiSaranaPelengkap.layoutManager = linearLayoutManager

        getdataDetailSaranaPelengkap(saranaidTemp!!)

    }

    private fun getdataDetailSaranaPelengkap(saranaIdTemp: Int) {
        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        val results = realm.where(SaranaLain::class.java)
                .equalTo("SaranaIdTemp", saranaIdTemp)
                .and()
                .equalTo("LandIdTemp", LandId)
                .findFirst()
        listener.onChange(results!!)
    }

    private var listener: RealmChangeListener<SaranaLain> = RealmChangeListener { results ->
        if (results.isLoaded) {
            // results is always up to date here
            // after a write to Realm from ANY thread!
            val a = results
            updateUi(results)
        }
    }


    @SuppressLint("SetTextI18n")
    private fun updateUi(data: SaranaLain) {
        tvnamaPemilikSarana.text = data.perlengkapanName
        tvvalueJenisSarana.text = data.jenisFasilitasName

        if (data.jenisFasilitasNameOther.isNotEmpty()) {
            tvvalueJenisSarana.text = data.jenisFasilitasNameOther?.toString() ?: "-"
        } else {
            tvvalueJenisSarana.text = data.jenisFasilitasName?.toString() ?: "-"
        }

        if(!data.notes.isNullOrEmpty()){
            tvNotes.text = data.notes.toString()
        }
        else {
            tvNotes.text = "-"
        }

        val itemparkeling = findViewById<LinearLayout>(R.id.llpagarkeliling)
        itemparkeling.removeAllViews()
        val jenisfasilitasName = data.jenisFasilitasId
        val jenisfasilitasNamereplace1 = jenisfasilitasName.replace("[", "['")
        val jenisfasilitasNamereplace2 = jenisfasilitasNamereplace1.replace("]", "']")
        val jenisfasilitasNamereplace3 = jenisfasilitasNamereplace2.replace(",", "','")
        val jenisfasilitasNamereplace4 = jenisfasilitasNamereplace3.replace(" ", "")

        val fasilitasName = data.kapasitas
        val fasilitasNamereplace1 = fasilitasName.replace("[", "['")
        val fasilitasNamereplace2 = fasilitasNamereplace1.replace("]", "']")
        val fasilitasNamereplace3 = fasilitasNamereplace2.replace(", ", "','")

        val fasilitasValue = data.kapasitasValue
        val fasilitasValuereplace1 = fasilitasValue.replace("[", "['")
        val fasilitasValuereplace2 = fasilitasValuereplace1.replace("]", "']")
        val fasilitasValuereplace3 = fasilitasValuereplace2.replace(",", "','")
        val fasilitasValuereplace4 = fasilitasValuereplace3.replace(" ", "")

        val gson = Gson()
        val jenisfasilitasnameConverter = gson.fromJson(jenisfasilitasNamereplace4, Array<String>::class.java).toList()
        val fasilitasnameConverter = gson.fromJson(fasilitasNamereplace3, Array<String>::class.java).toList()
        val fasilitasvalueConverter = gson.fromJson(fasilitasValuereplace4, Array<String>::class.java).toList()

        //Log.d("hai", jenisfasilitasnameConverter.toString())

        when {
            data.jenisFasilitasNameOther.isEmpty() && data.jenisFasilitasName.equals("Saluran Tegangan Listrik", ignoreCase = true) -> {
                llvaluesaluranTeganganListrik.visibility = View.VISIBLE
                llvaluesaluranSumberAir.visibility = View.GONE
                ll_pavement_other_facility.visibility = View.GONE
                ll_telephone_lane_other_facility.visibility = View.GONE
                llpagarkeliling.visibility = View.GONE

                if (fasilitasvalueConverter.isNotEmpty()) {
                    for (j in fasilitasvalueConverter.indices) {
                        tvvalueSaluranListrik.text = fasilitasvalueConverter[j]
                    }
                }
            }

            data.jenisFasilitasNameOther.isEmpty() && data.jenisFasilitasName.equals("Sumber Air Bersih", ignoreCase = true) -> {
                llvaluesaluranTeganganListrik.visibility = View.GONE
                llvaluesaluranSumberAir.visibility = View.VISIBLE
                ll_pavement_other_facility.visibility = View.GONE
                llpagarkeliling.visibility = View.GONE

                if (fasilitasnameConverter.isNotEmpty()) {
                    for (j in fasilitasnameConverter.indices) {
                        tvvalueSumberAir.text = fasilitasnameConverter[j]
                    }
                }
            }

            data.jenisFasilitasNameOther.isEmpty() && data.jenisFasilitasName.equals("Pagar Keliling", ignoreCase = true) -> {
                llvaluesaluranTeganganListrik.visibility = View.GONE
                llvaluesaluranSumberAir.visibility = View.GONE
                ll_pavement_other_facility.visibility = View.GONE
                ll_telephone_lane_other_facility.visibility = View.GONE
                llpagarkeliling.visibility = View.GONE

                val child = layoutInflater.inflate(R.layout.item_detail_sarana_pelengkap_pakeling, null)
                val namaPakeling = child.findViewById(R.id.tvisinamaPakerling) as com.pixplicity.fontview.FontTextView
                val panjangPakeling = child.findViewById(R.id.tvisipanjangParkeling) as com.pixplicity.fontview.FontTextView
                val tinggiPakeling = child.findViewById(R.id.tvisitinggiParkeling) as com.pixplicity.fontview.FontTextView
                val lebarPakeling = child.findViewById(R.id.tvisilebarParkeling) as com.pixplicity.fontview.FontTextView
                if (fasilitasnameConverter.isNotEmpty()) {
                    for (j in fasilitasnameConverter.indices) {
                        namaPakeling.text = fasilitasnameConverter[j]
                        if (fasilitasvalueConverter.isNotEmpty()) {
                            for (index in fasilitasvalueConverter.indices) {
                                panjangPakeling.text = fasilitasvalueConverter[0]
                                tinggiPakeling.text = fasilitasvalueConverter[1]
                                lebarPakeling.text = fasilitasvalueConverter[2]
                            }
                        }
                        itemparkeling.addView(child)
                    }
                }
            }
            data.jenisFasilitasNameOther.isEmpty() && data.jenisFasilitasName.equals("Kolam Renang", ignoreCase = true) -> {
                llvaluesaluranTeganganListrik.visibility = View.GONE
                llvaluesaluranSumberAir.visibility = View.GONE
                ll_pavement_other_facility.visibility = View.GONE
                ll_telephone_lane_other_facility.visibility = View.GONE
                llpagarkeliling.visibility = View.GONE

                val child = layoutInflater.inflate(R.layout.item_detail_sarana_swimming_pool, null)
                val tvyearOfBuiltSwimmingPool = child.findViewById(R.id.tvTahunSimmingPool) as com.pixplicity.fontview.FontTextView
                val tvlengthOfSwimmingPool = child.findViewById(R.id.tvisipanjangSwimmingPool) as com.pixplicity.fontview.FontTextView
                val tvwidhtOfSwimmingPool = child.findViewById(R.id.tvisilebarSwimmingPool) as com.pixplicity.fontview.FontTextView
                val tvdepthOfSwimmingPool = child.findViewById(R.id.tvisikedalamanSimmingPool) as com.pixplicity.fontview.FontTextView

                val resultsSwimmingPool = realm.where(OtherFacilityOfSwimmingPool::class.java)
                        .equalTo("msaranaLain.SaranaIdTemp", data.saranaIdTemp)
                        .and()
                        .equalTo("msaranaLain.LandIdTemp", LandId)
                        .findFirst()

                tvyearOfBuiltSwimmingPool.text = resultsSwimmingPool?.swimmingPoolYear.toString()
                        ?: "-"
                tvlengthOfSwimmingPool.text = resultsSwimmingPool?.swimmingPoolLength.toString()
                        ?: "-"
                tvwidhtOfSwimmingPool.text = resultsSwimmingPool?.swimmingPoolWidth.toString()
                        ?: "-"
                tvdepthOfSwimmingPool.text = resultsSwimmingPool?.swimmingPoolDept.toString() ?: "-"
                itemparkeling.addView(child)

                /*val results2 = realm.where(SaranaLain::class.java)
                        .equalTo("otherFacilityOfSwimmingPool.swimmingPoolFacilityId", data.saranaIdTemp)
                        .findAll()
                Log.d("hello2", results2.toString())*/
            }

            data.jenisFasilitasNameOther.isEmpty() && data.jenisFasilitasName.equals("Air Conditioner (AC)", ignoreCase = true) -> {
                llvaluesaluranTeganganListrik.visibility = View.GONE
                llvaluesaluranSumberAir.visibility = View.GONE
                ll_pavement_other_facility.visibility = View.GONE
                ll_telephone_lane_other_facility.visibility = View.GONE
                llpagarkeliling.visibility = View.GONE

                val child = layoutInflater.inflate(R.layout.item_detail_sarana_air_conditioner, null)
                val tvBrandOfAc = child.findViewById(R.id.tv_brand_ac) as com.pixplicity.fontview.FontTextView
                val tvTypeOfAc = child.findViewById(R.id.tv_type_of_ac) as com.pixplicity.fontview.FontTextView
                val tvQuantityOfAc = child.findViewById(R.id.tv_quantity_of_ac) as com.pixplicity.fontview.FontTextView
                val tvCapacityOfAc = child.findViewById(R.id.tv_capacity_of_ac) as com.pixplicity.fontview.FontTextView
                val tvYearProductionOfAc = child.findViewById(R.id.tv_year_production_ac) as com.pixplicity.fontview.FontTextView
                val tvMadeinOfAc = child.findViewById(R.id.tv_made_in_of_ac) as com.pixplicity.fontview.FontTextView

                val resultsAirConditioner = realm.where(OtherFacilityOfAirConditioner::class.java)
                        .equalTo("msaranaLain.SaranaIdTemp", data.saranaIdTemp)
                        .and()
                        .equalTo("msaranaLain.LandIdTemp", LandId)
                        .findFirst()

                tvBrandOfAc.text = resultsAirConditioner?.airConditionerBrand.toString() ?: "-"
                tvTypeOfAc.text = resultsAirConditioner?.airConditionerType.toString() ?: "-"
                tvQuantityOfAc.text = resultsAirConditioner?.airConditionerQuantity.toString()
                        ?: "-"
                tvCapacityOfAc.text = resultsAirConditioner?.airConditionerCapacity.toString()
                        ?: "-"
                tvYearProductionOfAc.text = resultsAirConditioner?.airConditionerYearProduction.toString()
                        ?: "-"
                tvMadeinOfAc.text = resultsAirConditioner?.airConditionerMadeIn.toString() ?: "-"
                itemparkeling.addView(child)

            }

            data.jenisFasilitasNameOther.isEmpty() && data.jenisFasilitasName.equals("Elevator/Lift", ignoreCase = true) -> {
                llvaluesaluranTeganganListrik.visibility = View.GONE
                llvaluesaluranSumberAir.visibility = View.GONE
                ll_pavement_other_facility.visibility = View.GONE
                ll_telephone_lane_other_facility.visibility = View.GONE
                llpagarkeliling.visibility = View.GONE

                val child = layoutInflater.inflate(R.layout.item_detail_sarana_elevator, null)
                val tvBrandOfElevator = child.findViewById(R.id.tv_brand_elevator) as com.pixplicity.fontview.FontTextView
                val tvQuantityOfElevator = child.findViewById(R.id.tv_quantity_elevator) as com.pixplicity.fontview.FontTextView
                val tvMaxWeightOfElevator = child.findViewById(R.id.tv_max_weight_elevator) as com.pixplicity.fontview.FontTextView
                val tvMaxWeightPersonQuantityOfElevator = child.findViewById(R.id.tv_max_weight_person_quantity_elevator) as com.pixplicity.fontview.FontTextView
                val tvLengthOfElevator = child.findViewById(R.id.tv_length_of_elevator) as com.pixplicity.fontview.FontTextView
                val tvWidthOfElevator = child.findViewById(R.id.tv_width_of_elevator) as com.pixplicity.fontview.FontTextView
                val tvHeightOfElevator = child.findViewById(R.id.tv_height_of_elevator) as com.pixplicity.fontview.FontTextView
                val tvYearProductionOfElevator = child.findViewById(R.id.tv_year_production_elevator) as com.pixplicity.fontview.FontTextView
                val tvMadeinOfElevator = child.findViewById(R.id.tv_made_in_of_elevator) as com.pixplicity.fontview.FontTextView

                val resultsElevator = realm.where(OtherFacilityOfElevator::class.java)
                        .equalTo("msaranaLain.SaranaIdTemp", data.saranaIdTemp)
                        .and()
                        .equalTo("msaranaLain.LandIdTemp", LandId)
                        .findFirst()

                tvBrandOfElevator.text = resultsElevator?.elevatorBrand.toString() ?: "-"
                tvQuantityOfElevator.text = resultsElevator?.elevatorQuantity.toString() ?: "-"
                tvMaxWeightOfElevator.text = resultsElevator?.elevatorMaximumWeight.toString()
                        ?: "-"
                tvMaxWeightPersonQuantityOfElevator.text = resultsElevator?.elevatorMaximumPersonQuantity.toString()
                        ?: "-"
                tvLengthOfElevator.text = resultsElevator?.elevatorLength.toString() ?: "-"
                tvWidthOfElevator.text = resultsElevator?.elevatorWidth.toString() ?: "-"
                tvHeightOfElevator.text = resultsElevator?.elevatorHeight.toString() ?: "-"
                tvYearProductionOfElevator.text = resultsElevator?.elevatorQYearProduction.toString()
                        ?: "-"
                tvMadeinOfElevator.text = resultsElevator?.elevatorMadeIn.toString() ?: "-"

                itemparkeling.addView(child)
            }

            data.jenisFasilitasNameOther.isEmpty() && data.jenisFasilitasName.equals("Wastewater Treatment Plant (WWTP)", ignoreCase = true) -> {
                llvaluesaluranTeganganListrik.visibility = View.GONE
                llvaluesaluranSumberAir.visibility = View.GONE
                ll_pavement_other_facility.visibility = View.GONE
                ll_telephone_lane_other_facility.visibility = View.GONE
                llpagarkeliling.visibility = View.GONE

                val child = layoutInflater.inflate(R.layout.item_detail_sarana_wwtp, null)
                val tvYearOfBuiltOfWwtp = child.findViewById(R.id.tv_year_production_wwtp) as com.pixplicity.fontview.FontTextView
                val tvCapacityOfWwtp = child.findViewById(R.id.tv_capacity_of_wwtp) as com.pixplicity.fontview.FontTextView

                val resultsWwtp = realm.where(OtherFacilityOfWWTP::class.java)
                        .equalTo("msaranaLain.SaranaIdTemp", data.saranaIdTemp)
                        .and()
                        .equalTo("msaranaLain.LandIdTemp", LandId)
                        .findFirst()

                tvYearOfBuiltOfWwtp.text = resultsWwtp?.wwtpYearBuilt.toString() ?: "-"
                tvCapacityOfWwtp.text = resultsWwtp?.wwtpCapacity.toString() ?: "-"

                itemparkeling.addView(child)
            }

            data.jenisFasilitasNameOther.isEmpty() && data.jenisFasilitasName.equals("Clean Water Processing", ignoreCase = true) -> {
                llvaluesaluranTeganganListrik.visibility = View.GONE
                llvaluesaluranSumberAir.visibility = View.GONE
                ll_pavement_other_facility.visibility = View.GONE
                ll_telephone_lane_other_facility.visibility = View.GONE
                llpagarkeliling.visibility = View.GONE

                val child = layoutInflater.inflate(R.layout.item_detail_sarana_clean_water_processing, null)
                val tvCapacityOfCleanWaterProcessing = child.findViewById(R.id.tv_capacity_clean_water_processing) as com.pixplicity.fontview.FontTextView
                val tvYearOfBuiltOfCleanWaterProcessing = child.findViewById(R.id.tv_year_production_clean_water_processing) as com.pixplicity.fontview.FontTextView

                val resultsCleanWaterProcessing = realm.where(OtherFacilityOfCleanWaterProcessingInstallation::class.java)
                        .equalTo("msaranaLain.SaranaIdTemp", data.saranaIdTemp)
                        .and()
                        .equalTo("msaranaLain.LandIdTemp", LandId)
                        .findFirst()

                tvYearOfBuiltOfCleanWaterProcessing.text = resultsCleanWaterProcessing?.cleanWaterProcessingYearProduction.toString()
                        ?: "-"
                tvCapacityOfCleanWaterProcessing.text = resultsCleanWaterProcessing?.cleanWaterProcessingInstallationCapacity.toString()
                        ?: "-"

                itemparkeling.addView(child)
            }

            data.jenisFasilitasNameOther.isEmpty() && data.jenisFasilitasName.equals("Pavement", ignoreCase = true) -> {
                llvaluesaluranTeganganListrik.visibility = View.GONE
                llvaluesaluranSumberAir.visibility = View.GONE
                ll_telephone_lane_other_facility.visibility = View.GONE
                llpagarkeliling.visibility = View.GONE
                ll_pavement_other_facility.visibility = View.VISIBLE

                val resultsPavement = realm.where(OtherFacilityOfPavement::class.java)
                        .equalTo("msaranaLain.SaranaIdTemp", data.saranaIdTemp)
                        .and()
                        .equalTo("msaranaLain.LandIdTemp", LandId)
                        .findFirst()

                tv_capacity_of_pavment.text = resultsPavement?.pavementCapacity.toString() ?: "-"
            }

            data.jenisFasilitasNameOther.isEmpty() && data.jenisFasilitasName.equals("Telephone Line", ignoreCase = true) -> {
                llvaluesaluranTeganganListrik.visibility = View.GONE
                llvaluesaluranSumberAir.visibility = View.GONE
                ll_pavement_other_facility.visibility = View.GONE
                llpagarkeliling.visibility = View.GONE
                ll_telephone_lane_other_facility.visibility = View.VISIBLE

                val resultsTelephoneLine = realm.where(OtherFacilityOfTelephoneLine::class.java)
                        .equalTo("msaranaLain.SaranaIdTemp", data.saranaIdTemp)
                        .and()
                        .equalTo("msaranaLain.LandIdTemp", LandId)
                        .findFirst()

                tv_capacity_of_pavment.text = resultsTelephoneLine?.telephoneLineQuantity.toString()
                        ?: "-"
                if (resultsTelephoneLine?.telephoneLineYesNO == 1) {
                    tv_quantity_of_telephone_line.text = resultsTelephoneLine.telephoneLineQuantity.toString()
                } else {
                    tv_quantity_of_telephone_line.text = "-"
                }
            }

            else -> {
                llvaluesaluranTeganganListrik.visibility = View.GONE
                llvaluesaluranSumberAir.visibility = View.GONE
            }
        }

        /*if (jenisfasilitasnameConverter.isNotEmpty()) {
            for (i in jenisfasilitasnameConverter.indices) {
                if (jenisfasilitasnameConverter[i] == "1") {
                    llvaluesaluranTeganganListrik.visibility = View.VISIBLE
                    llvaluesaluranSumberAir.visibility = View.GONE
                    llpagarkeliling.visibility = View.GONE
                    if (fasilitasvalueConverter.isNotEmpty()) {
                        for (j in fasilitasvalueConverter.indices) {
                            tvvalueSaluranListrik.text = fasilitasvalueConverter[j]
                        }
                    }
                } else if (jenisfasilitasnameConverter[i] == "2") {
                    llvaluesaluranTeganganListrik.visibility = View.GONE
                    llvaluesaluranSumberAir.visibility = View.VISIBLE
                    llpagarkeliling.visibility = View.GONE
                    if (fasilitasnameConverter.isNotEmpty()) {
                        for (j in fasilitasnameConverter.indices) {
                            tvvalueSumberAir.text = fasilitasnameConverter[j]
                        }
                    }
                } else if (jenisfasilitasnameConverter[i] == "3") {
                    llvaluesaluranTeganganListrik.visibility = View.GONE
                    llvaluesaluranSumberAir.visibility = View.GONE
                    llpagarkeliling.visibility = View.VISIBLE
                    val child = layoutInflater.inflate(R.layout.item_detail_sarana_pelengkap_pakeling, null)
                    val namaPakeling = child.findViewById(R.id.tvisinamaPakerling) as com.pixplicity.fontview.FontTextView
                    val panjangPakeling = child.findViewById(R.id.tvisipanjangParkeling) as com.pixplicity.fontview.FontTextView
                    val tinggiPakeling = child.findViewById(R.id.tvisitinggiParkeling) as com.pixplicity.fontview.FontTextView
                    val lebarPakeling = child.findViewById(R.id.tvisilebarParkeling) as com.pixplicity.fontview.FontTextView
                    if (fasilitasnameConverter.isNotEmpty()) {
                        for (j in fasilitasnameConverter.indices) {
                            namaPakeling.text = fasilitasnameConverter[j]
                            if (fasilitasvalueConverter.isNotEmpty()) {
                                for (index in fasilitasvalueConverter.indices) {
                                    panjangPakeling.text = fasilitasvalueConverter[0]
                                    tinggiPakeling.text = fasilitasvalueConverter[1]
                                    lebarPakeling.text = fasilitasvalueConverter[2]
                                }
                            }
                            itemparkeling.addView(child)
                        }
                    }
                } else {
                    llvaluesaluranTeganganListrik.visibility = View.INVISIBLE
                    llvaluesaluranSumberAir.visibility = View.INVISIBLE
                }
            }
        }*/

        val mdatafotofasilitas: MutableList<DataImageFacilities> = ArrayList()
        mdatafotofasilitas.addAll(data.dataImageOtherFacilities)
        adapterfotoSaranaPelengkap.clear()
        adapterfotoSaranaPelengkap.addItems(mdatafotofasilitas)

        //setupLayout
        /*val item = findViewById<LinearLayout>(R.id.llitemfac2)
        item.removeAllViews()
        val jenisTanaman = data.jenisTanamanNama
        val jenisTanamanreplace1 = jenisTanaman.replace("[", "['")
        val jenisTanamanreplace2 = jenisTanamanreplace1.replace("]", "']")
        val jenisTanamanreplace3 = jenisTanamanreplace2.replace(",", "','")
        val jenisTanamanreplace4 = jenisTanamanreplace3.replace(" ", "")

        val kategoriTanaman = data.namaTanaman
        val kategoriTanamanreplace1 = kategoriTanaman.replace("[", "['")
        val kategoriTanamanreplace2 = kategoriTanamanreplace1.replace("]", "']")
        val kategoriTanamanreplace3 = kategoriTanamanreplace2.replace(",", "','")
        val kategoriTanamanreplace4 = kategoriTanamanreplace3.replace(" ", "")

        val ukuranTanaman = data.ukuranTanaman
        val ukuranTanamanreplace1 = ukuranTanaman.replace("[", "['")
        val ukuranTanamanreplace2 = ukuranTanamanreplace1.replace("]", "']")
        val ukuranTanamanreplace3 = ukuranTanamanreplace2.replace(",", "','")
        val ukuranTanamanreplace4 = ukuranTanamanreplace3.replace(" ", "")

        val jumlahTanaman = data.jumlahTanaman
        val jumlahTanamanreplace1 = jumlahTanaman.replace("[", "['")
        val jumlahTanamanreplace2 = jumlahTanamanreplace1.replace("]", "']")
        val jumlahTanamanreplace3 = jumlahTanamanreplace2.replace(",", "','")
        val jumlahTanamanreplace4 = jumlahTanamanreplace3.replace(" ", "")

        val gson = Gson()
        val jenisTanamanConverter = gson.fromJson(jenisTanamanreplace4, Array<String>::class.java).toList()
        val kategoriTanamanConverter = gson.fromJson(kategoriTanamanreplace4, Array<String>::class.java).toList()
        val ukuaranTanamanConverter = gson.fromJson(ukuranTanamanreplace4, Array<String>::class.java).toList()
        val jumlahTanamanConverter = gson.fromJson(jumlahTanamanreplace4, Array<String>::class.java).toList()

        val mdatafotoTanaman: MutableList<DataImagePlants> = ArrayList()
        mdatafotoTanaman.addAll(data.dataImagePlants)
        adapterFotoPropertiTanaman.clear()
        adapterFotoPropertiTanaman.addItems(mdatafotoTanaman)

        if (jenisTanamanConverter.isNotEmpty()) {
            for (i in jenisTanamanConverter.indices) {
                val child = layoutInflater.inflate(R.layout.item_detail_tanaman_vertical, null)
                val txtjenisTanaman = child.findViewById(R.id.tvtitlejenisTanaman) as com.pixplicity.fontview.FontTextView
                val txttitleKategoriTanaman = child.findViewById(R.id.tvtitleKategoriTanaman) as com.pixplicity.fontview.FontTextView
                val txtKategoriTanaman = child.findViewById(R.id.tvisiKategoriTanaman) as com.pixplicity.fontview.FontTextView

                val txttitleUkuranTanaman = child.findViewById(R.id.tvtitleukuranTanaman) as com.pixplicity.fontview.FontTextView
                val txtUkuranTanaman = child.findViewById(R.id.tvisiukuranTanaman) as com.pixplicity.fontview.FontTextView

                val txttitleJumlahTanaman = child.findViewById(R.id.tvtitlejmlhTanaman) as com.pixplicity.fontview.FontTextView
                val txtJumlahTanaman = child.findViewById(R.id.tvisijmlhTanaman) as com.pixplicity.fontview.FontTextView

                txtjenisTanaman.text = "Jenis ${jenisTanamanConverter[i]}"
                txttitleKategoriTanaman.text = "Nama ${jenisTanamanConverter[i]}"
                txtKategoriTanaman.text = kategoriTanamanConverter[i]

                txttitleUkuranTanaman.text = "Ukuran ${jenisTanamanConverter[i]}"
                txtUkuranTanaman.text = ukuaranTanamanConverter[i]

                txttitleJumlahTanaman.text = "Jumlah ${jenisTanamanConverter[i]}"
                txtJumlahTanaman.text = jumlahTanamanConverter[i]

                item.addView(child)
            }
        }*/

    }


    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    override fun onRestart() {
        super.onRestart()
        getdataDetailSaranaPelengkap(saranaidTemp!!)
    }

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityDetailObjekSaranaPelengkap::class.java)
        }
    }

}