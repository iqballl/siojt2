package mki.siojt2.ui.login.view

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.*
import android.text.format.DateFormat
import android.transition.Slide
import android.util.Log
import android.view.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.tapadoo.alerter.Alerter
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.dialog_popup_sync.view.*
import mki.siojt2.R
import mki.siojt2.SIOJT2
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.model.DataExpiredDialogSync
import mki.siojt2.model.auth.SessionAuth
import mki.siojt2.model.login.LoginResponse
import mki.siojt2.persistence.Preference
import mki.siojt2.ui.activity_help_assistance.ActivityHelpAssistance
import mki.siojt2.ui.login.presenter.LoginPresenter
import mki.siojt2.ui.main.view.MainActivity
import mki.siojt2.utils.Utils
import mki.siojt2.utils.network.ConnectivityReceiver
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

/**
 * Created by iqbal.
 */

class LoginActivity : BaseActivity(), LoginView, ConnectivityReceiver.ConnectivityReceiverListener {

    private var inUsername = ""
    private var inPassword = ""

    private var vib: Vibrator? = null
    private var loginPresenter: LoginPresenter? = null

    private var receiverNetwork: ConnectivityReceiver? = null
    private var laststatusIsConnected: Boolean = false

    private var mDialogReceiver: BroadcastReceiver? = null
    private val dateTemplate = "dd MMMM yyyy"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (Build.VERSION.SDK_INT >= 21) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = Color.parseColor("#B24F0E")
        }

        receiverNetwork = ConnectivityReceiver()

        llmanualHelp.setSafeOnClickListener {
            hideKeyboard()
            val intent = ActivityHelpAssistance.getStartIntent(this)
            startActivity(intent)
        }

        /*if (Utils.checkReminderDialogSync(this)) {

            val mCalendar = Calendar.getInstance(Locale.getDefault())
            mCalendar.time = Preference.getsaveSyncDataSplashScreen()!!.appSyncDate
            val tglSync = DateFormat.format(dateTemplate, mCalendar.time)

            *//*set 1*//*

        } else {

        }*/

        val turnsType = object : TypeToken<DataExpiredDialogSync>() {}.type
        val dataconvert = Gson().fromJson<DataExpiredDialogSync>(Utils.getemainderDialogSync(this), turnsType)

        //Log.d("cek", "lakukan ${dataconvert.dataExpiredDialogSyncDate}")
        //Log.d("cek", "lakukan ${Date()}")
        if (Date().after(dataconvert.dataExpiredDialogSyncDate)) {
            val mCalendar = Calendar.getInstance(Locale.getDefault())
            mCalendar.time = Preference.getsaveSyncDataSplashScreen()!!.appSyncDate!!
            val tglSync = DateFormat.format(dateTemplate, mCalendar.time)
            showDialogExpireSync("Sinkronisasi ulang pada tanggal\n${"$tglSync"}")
        } else {
            Log.d("cek", "tidak lakukan")
        }

        //loginPresenter.onAttach(this)

        /*
        This activity is created and opened when SplashScreen finishes its animations.
        To ensure a smooth transition between activities, the activity creation animation
        is removed.
        RelativeLayout with EditTexts and Button is animated with a default fade in.
         */
        /*overridePendingTransition(0, 0)
        val animation = AnimationUtils.loadAnimation(this, android.R.anim.fade_in)
        login_container.startAnimation(animation)

        */
        btnLogin.setSafeOnClickListener {
            getAuth()
            /*val intent = MainActivity.getStartIntent(this@LoginActivity)
            startActivity(intent)
            finish()*/
        }
    }

    private fun checkConnection() {
        val isConnected: Boolean = ConnectivityReceiver.isConnected
    }

    private fun statusConnectionInternet(isConnected: Boolean) {
        if (isConnected) {
            Log.d("connection", "Good! connected to internet")
        } else {
            showAlreter("No Internet Connection")
        }
    }

    private fun showAlreter(message: String) {
        Alerter.create(this@LoginActivity)
                .setTitle(R.string.app_name)
                .setText(message)
                .setTextAppearance(R.style.AlertTextAppearance)
                .setBackgroundColorRes(R.color.colorbuttonAlret)
                .setIcon(R.drawable.ic_info)
                .setDuration(4200)
                .enableVibration(true)
                .enableSwipeToDismiss()
                .addButton("OK", R.style.AlertButton, View.OnClickListener {
                    Alerter.hide()
                })
                .show()

    }

    private fun getPresenter(): LoginPresenter? {
        loginPresenter = LoginPresenter()
        loginPresenter!!.onAttach(this)
        return loginPresenter
    }

    private fun getAuth() {
        if (!validate()) {
            return
        } else {
            //NetworkUtil()
            /*getPresenter()?.getAuth(inUsername, inPassword)*/
            inUsername = editTextEmail.text.toString().trim()
            inPassword = editTextPassword.text.toString().trim()
            checkNetworkStatus()
        }
    }

    private fun getLoginLocal() {
        Log.d("Login", "LoginLocal")
        btnLogin.isEnabled = false
        //onShowLoading()

        // Implement your own signup logic here.
        if (Preference.sessionAuth() != null) {
            val dataSessionUsers = Preference.sessionAuth()
            val dataUserName = dataSessionUsers!!.userName
            val dataPassword = dataSessionUsers.userPassword

            //Log.d("keluaran", dataUserName)
            //Log.d("keluaran", dataPassword)
            //Log.d("keluaran", dataSessionUsers.userStatusLogin.toString())

            /* login use the database local, if session users data is not empty */
            if (inUsername.equals(dataUserName, true) && inPassword.equals(dataPassword, true)) {
                Handler().postDelayed({
                    onSuccessLoginLocal()
                    onDismissLoading()
                }, 1000)
            } else {
                /* on failed login */
                Handler().postDelayed({
                    onDismissLoading()
                    btnLogin.isEnabled = true
                    showAlreter("Username dan password salah!.")
                }, 1000)

            }
        } else {
            /* session data is empty, Login is required to use the internet */
            onDismissLoading()
            btnLogin.isEnabled = true
            showAlreter("Pertama kali login.\nPastikan koneksi internet stabil, login aplikasi wajib menggunakan koneksi internet.")
        }


    }

    private fun onSuccessLoginLocal() {
        val dataSessionAuth = SessionAuth()
        dataSessionAuth.userName = inUsername
        dataSessionAuth.userPassword = inPassword
        dataSessionAuth.userStatusLogin = true
        Preference.savesessionAuth(dataSessionAuth)

        btnLogin.isEnabled = true
        openMainActivity()
        /*Handler().postDelayed({
            btnLogin.isEnabled = true
            openMainActivity()
        }, 400)*/
    }

    /* check network status*/
    private fun checkNetworkStatus() {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = cm.activeNetworkInfo
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = cm.activeNetwork
            val capabilities = cm.getNetworkCapabilities(networkCapabilities)

            if (capabilities != null) {
                cm.getNetworkCapabilities(networkCapabilities)?.run {
                    when {
                        hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                            CheckConnectionTask().execute()
                            Log.d("statusNetwork", "WIFI")
                        }
                        hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                            CheckConnectionTask().execute()
                            Log.d("statusNetwork", "MOBILE DATA")
                        }
                        else -> {
                            CheckConnectionTask().execute()
                            Log.d("statusNetwork", "not found 1")
                        }
                    }
                }
            } else {
                CheckConnectionTask().execute()
                Log.d("statusNetwork", "not found 2")
            }

        } else {
            cm.run {
                cm.activeNetworkInfo?.run {
                    when (type) {
                        ConnectivityManager.TYPE_WIFI -> {
                            CheckConnectionTask().execute()
                            Log.d("statusNetwork", "WIFI")
                        }
                        ConnectivityManager.TYPE_MOBILE -> {
                            CheckConnectionTask().execute()
                            Log.d("statusNetwork", "MOBILE DATA")
                        }
                        else -> {
                            CheckConnectionTask().execute()
                            Log.d("statusNetwork", "not found")
                        }
                    }

                }
            }
        }

        /*if (activeNetwork != null) {
            if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                CheckConnectionTask().execute()
                Log.d("statusNetwork", "WIFI")
            } else if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                CheckConnectionTask().execute()
                Log.d("statusNetwork", "MOBILE DATA")
            }
        } else {
            CheckConnectionTask().execute()
            Log.d("statusNetwork", "not found")
        }*/
    }

    /* Checking internet connection by pinging url */
    @SuppressLint("StaticFieldLeak")
    inner class CheckConnectionTask : AsyncTask<Void, Void, Boolean>() {
        override fun doInBackground(vararg params: Void): Boolean {
            return try {
                val urlc = (URL("http://clients3.google.com/generate_204")
                        .openConnection()) as HttpURLConnection
                urlc.setRequestProperty("User-Agent", "Android")
                urlc.setRequestProperty("Connection", "close")
                urlc.connectTimeout = 1500
                urlc.connect()
                urlc.responseCode == 204 && urlc.contentLength == 0
            } catch (e: Exception) {
                Log.e("exception", e.toString())
                false
            }
        }

        override fun onPostExecute(result: Boolean) {
            super.onPostExecute(result)
            onShowLoading()
            if (result) {
                Log.d("Login", "LoginServer")
                getPresenter()?.getAuth(inUsername, inPassword)
            } else {
                Log.d("Login", "LoginLocal")
                //showAlreter("No Internet Connection")
                getLoginLocal()
            }
        }
    }

    private fun validate(): Boolean {
        var valid = true
        vib = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        inUsername = editTextEmail.text.toString()
        inPassword = editTextPassword.text.toString()

        if (inUsername.isEmpty()) {
            showToast("Please enter username")
            vib!!.vibrate(120)
            valid = false
        } else if (inPassword.isEmpty()) {
            showToast("Please enter password")
            vib!!.vibrate(120)
            valid = false
        } else if (inPassword.isEmpty() || inPassword.length < 4 || inPassword.length > 10) {
            showToast("between 4 and 10 alphanumeric characters")
            vib!!.vibrate(120)
            valid = false
        }

        return valid
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private fun setupWindowAnimations() {
        val slide = Slide()
        slide.duration = 1000
        window.exitTransition = slide
    }

    override fun onSuccessLogin(auth: LoginResponse) {
        Preference.saveAuth(auth)
        Preference.saveAccessToken(auth.dataUser?.mobileUserAccessToken.toString())

        val dataSessionAuth = SessionAuth()
        dataSessionAuth.userId = auth.dataUser?.mobileUserId.toString()
        dataSessionAuth.userName = editTextEmail.text.toString()
        dataSessionAuth.userPassword = editTextPassword.text.toString()
        dataSessionAuth.userStatusLogin = true
        Preference.savesessionAuth(dataSessionAuth)

        openMainActivity()
        //Utils.saveToken(this, auth.newToken!!)
        //Utils.saveislogin(true, this@LoginActivity)

//        Handler().postDelayed({
//            openMainActivity()
//        }, 400)

        /*if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
            overridePendingTransition(R.anim.slide_left, R.anim.slide_right)
            val i = Intent(this, MainActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
            finish()
        } else {
            val i = Intent(this, MainActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
            finish()
        }*/
    }

    override fun openMainActivity() {
        val intent = MainActivity.getStartIntent(this@LoginActivity)
        startActivity(intent)
        finish()
    }

    override fun onResume() {
        super.onResume()
        try {
            //register connection receiver
            // Create an IntentFilter instance.
            val intentFilter = IntentFilter()

            // Add network connectivity change action.
            intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
            // Set broadcast receiver priority.
            intentFilter.priority = 100
            registerReceiver(receiverNetwork, intentFilter)

            val intentFilter2 = IntentFilter()
            intentFilter2.addAction(BROADCAST_ACTION)
            registerReceiver(mDialogReceiver, intentFilter2)

            // register connection status listener
            SIOJT2.instance?.setConnectivityListener(this)
        } catch (e: Exception) {
            // already registered
            throw Exception("receiverNetwork not registered : ${e.message}")
        }
    }

    override fun onPause() {
        try {
            if (receiverNetwork != null) {
                unregisterReceiver(receiverNetwork)
            }
        } catch (e: Exception) {
            // already registered
            throw Exception("receiverNetwork not registered : ${e.message}")
        }
        super.onPause()
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        statusConnectionInternet(isConnected)
        this.laststatusIsConnected = isConnected
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        getPresenter()?.onDetach()
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    @SuppressLint("SetTextI18n")
    fun showDialogExpireSync(message: String) {
        val mDialogView = LayoutInflater.from(this@LoginActivity).inflate(R.layout.dialog_popup_sync, null)
        val mBuilder = AlertDialog.Builder(this@LoginActivity)
        val mDialog = mBuilder.create()

        mDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialog.setCancelable(false)
        val window = mDialog.window
        if (window != null) {
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            mDialog.window!!.setLayout(width, height)
        }
        mDialog.setView(mDialogView)

        mDialogView.tvTitlePopup.text = "Sinkoronisasi berhasil"
        mDialogView.tvcontentPopup.text = message
        mDialogView.txtbtnContSync.text = "OK"

        mDialogView.btnContSync.setSafeOnClickListener {
            val mCalendar2 = Calendar.getInstance(Locale.getDefault())
            mCalendar2.add(Calendar.DAY_OF_MONTH, 3)
            val dataExpiredDialogSync = DataExpiredDialogSync()
            dataExpiredDialogSync.dtaExpiredDialogSyncStatus = 1
            dataExpiredDialogSync.dataExpiredDialogSyncDate = mCalendar2.time

            val dataExpireDate = Gson().toJson(dataExpiredDialogSync)
            Utils.saveremainderDialogSync(dataExpireDate, this@LoginActivity)

            mDialog.dismiss()
        }

        mDialog.show()
    }

    companion object {
        private const val BROADCAST_ACTION = "mki.SIOJT2.show.dialog"
        fun getStartIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }
}
