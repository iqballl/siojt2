package mki.siojt2.ui.login.presenter

import android.annotation.SuppressLint
import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.model.auth.SessionAuth
import mki.siojt2.ui.login.view.LoginView
import mki.siojt2.utils.ErrorHandler
import mki.siojt2.utils.rxJava.RxUtils

/**
 * Created by iqbal on 09/10/17.
 */

class LoginPresenter : BasePresenter(), LoginMVPPresenter {

    private var disposables: CompositeDisposable = CompositeDisposable()

    @SuppressLint("CheckResult")
    override fun getAuth(username: String, password: String) {
        //authView().onShowLoading()
        /*disposables.add(
                dataManager.getAuth(username, password)
                        .doOnTerminate { authView().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            authView().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                authView().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    authView().onSuccessLogin(authResponseDataObject.result!!)
                                } else {
                                    authView().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            authView().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(authView(), throwable)
                        }
        )*/

        checkViewAttached()
        disposables.add(
                dataManager.getAuth(username, password)
                        .compose(RxUtils.ioToMainSingleScheduler())
                        .subscribe({
                            if (it.status == Constant.STATUS_ERROR) {
                                authView().onFailed(it.message!!)
                            } else {
                                if (it.status == Constant.STATUS_SUCCESS) {
                                    val dataSessionAuth = SessionAuth()
                                    dataSessionAuth.userId = it.result?.dataUser?.mobileUserId.toString()
                                    dataSessionAuth.userName = username
                                    dataSessionAuth.userPassword = password
                                    dataSessionAuth.userStatusLogin = true
                                    dataManager.updateUserInfo(
                                            it.result?.dataUser?.mobileUserAccessToken.toString(),
                                            it.result!!,
                                            dataSessionAuth
                                    )

                                    authView().let { view ->
                                        view.onDismissLoading()
                                        view.openMainActivity()
                                    }
                                } else {
                                    authView().onFailed(it.message!!)
                                }
                            }

                        }, { throwable ->
                            checkViewAttached()
                            authView().onDismissLoading()
                            // handle the login error here
                            ErrorHandler.handlerErrorPresenter(authView(), throwable)
                        })
        )

    }

    private fun authView(): LoginView {
        return getView() as LoginView
    }
}
