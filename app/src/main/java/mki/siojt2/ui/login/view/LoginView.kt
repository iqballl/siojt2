package mki.siojt2.ui.login.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.login.LoginResponse

/**
 * Created by iqbal on 09/09/19
 */

interface LoginView : MvpView {
    fun onSuccessLogin(auth: LoginResponse)
    fun openMainActivity()
}
