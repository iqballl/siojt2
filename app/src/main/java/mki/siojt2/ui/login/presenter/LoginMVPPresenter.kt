package mki.siojt2.ui.login.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface LoginMVPPresenter : MVPPresenter {

    fun getAuth(username: String, password: String)

}