package mki.siojt2.ui.activity_pdf_viewer

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import com.github.barteksc.pdfviewer.util.FitPolicy
import kotlinx.android.synthetic.main.activity_pdf_viewer.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import java.io.InputStream
import java.lang.ref.WeakReference
import java.net.URL

class ActivityPdfViewer : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdf_viewer)

        /* set toolbar*/
        if (toolbarpdfViewer != null) {
            setSupportActionBar(toolbarpdfViewer)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarpdfViewer.setNavigationOnClickListener {
            finish()
        }
        toolbarpdfViewer.title = "Bantuan"

        MyAsyncTask(this).execute()

//        pdfView.fromAsset("manual_penggunaan_aplikasi_ojt_mobile.pdf")
//                .enableSwipe(true) // allows to block changing pages using swipe
//                .swipeHorizontal(false)
//                .enableDoubletap(true)
//                .defaultPage(0)
//                .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
//                .password(null)
//                .scrollHandle(null)
//                .enableAntialiasing(true) // improve rendering a little bit on low-res screens
//                .spacing(0)
//                .pageFitPolicy(FitPolicy.WIDTH)
//                .load()
    }

    override fun onFragmentAttached() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFragmentDetached(tag: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityPdfViewer::class.java)
        }

        class MyAsyncTask internal constructor(context: ActivityPdfViewer) : AsyncTask<Unit, Unit, InputStream>() {
            private val activityReference: WeakReference<ActivityPdfViewer> = WeakReference(context)
            override fun onPreExecute() {
                val activity = activityReference.get()
                if (activity == null || activity.isFinishing) return
                activity.onShowLoading()
            }

            override fun doInBackground(vararg params: Unit): InputStream {
//                return URL("https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf").openStream()
                return URL("http://api-sipt.mkitech.co.id/guideline.pdf").openStream()
            }

            override fun onPostExecute(result: InputStream) {
                val activity = activityReference.get()
                if (activity == null || activity.isFinishing) return
                activity.pdfView.fromStream(result)
                    .enableSwipe(true) // allows to block changing pages using swipe
                    .swipeHorizontal(false)
                    .enableDoubletap(true)
                    .defaultPage(0)
                    .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                    .password(null)
                    .scrollHandle(null)
                    .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                    // spacing between pages in dp. To define spacing color, set view background
                    .spacing(0)
                    .pageFitPolicy(FitPolicy.WIDTH)
                    .load()
                activity.onDismissLoading()
            }
        }
    }
}