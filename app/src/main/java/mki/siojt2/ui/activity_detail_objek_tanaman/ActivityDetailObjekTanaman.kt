package mki.siojt2.ui.activity_detail_objek_tanaman

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import com.google.gson.Gson
import io.realm.Realm
import io.realm.RealmChangeListener
import kotlinx.android.synthetic.main.activity_detail_objek_tanaman.*
import kotlinx.android.synthetic.main.activity_form_add_objek_tanaman.*
import kotlinx.android.synthetic.main.dialog_confirm_delete.view.*
import kotlinx.android.synthetic.main.toolbar_main.toolbarCustom
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.model.local_save_image.DataImageLand
import mki.siojt2.model.local_save_image.DataImagePlants
import mki.siojt2.model.localsave.Session
import mki.siojt2.model.localsave.Tanaman
import mki.siojt2.ui.activity_detail_objek_tanaman.adapter_recycleview.AdapterFotoPropertiTanaman
import mki.siojt2.ui.activity_detail_subjek.presenter.DetailSubjekPresenter
import mki.siojt2.ui.activity_form_add_objek_tanaman.ActivityFormAddObjekTanaman
import mki.siojt2.utils.realm.RealmController

class ActivityDetailObjekTanaman : BaseActivity() {

    private lateinit var presenter: DetailSubjekPresenter
    private lateinit var session: Session
    private lateinit var realm: Realm

    private lateinit var mdialogConfirmDelete: AlertDialog
    private lateinit var mDialogView: View
    private var mdataHistory: Tanaman? = null
    private var mplantId: Int? = null

    lateinit var adapterFotoPropertiTanaman: AdapterFotoPropertiTanaman


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_objek_tanaman)

        session = Session(this)
        //mdataHistory = Parcels.unwrap(intent.getParcelableExtra("data_current_tanaman"))
        mplantId = intent?.extras?.getInt("data_current_tanaman")

        adapterFotoPropertiTanaman = AdapterFotoPropertiTanaman(this)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL, false)

        rlfotoPropertiTanaman.adapter = adapterFotoPropertiTanaman
        rlfotoPropertiTanaman.layoutManager = linearLayoutManager

        /* set dialog confirm delete data */
        mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_confirm_delete, null)
        val mBuilder = AlertDialog.Builder(this)
        mdialogConfirmDelete = mBuilder.create()

        mdialogConfirmDelete.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        mdialogConfirmDelete.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mdialogConfirmDelete.setCancelable(false)
        val window = mdialogConfirmDelete.window
        if (window != null) {
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            mdialogConfirmDelete.window!!.setLayout(width, height)
        }
        mdialogConfirmDelete.setView(mDialogView)

        /* set toolbar */
        if (toolbarCustom != null) {
            setSupportActionBar(toolbarCustom)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarCustomDetailTanaman.setNavigationOnClickListener {
            finish()
        }

        customtvToolbarTitleDetailTanaman.text = "Detail Tanaman"
        customtvToolbarTitleDetailTanaman.setTextColor(Color.parseColor("#3DC528"))

        getdataDetailTanaman(mplantId!!)

        btneditTanaman.setSafeOnClickListener {
            val intent = ActivityFormAddObjekTanaman.getStartIntent(this)
            val mSession = mki.siojt2.model.Session(mplantId, null,"2")
            //intent.putExtra("data_current_tanaman", Gson().toJson(mdataHistory))
            intent.putExtra("data_current_session", Gson().toJson(mSession))
            session.setIdAndStatus(mplantId.toString(), "2", "")
            startActivity(intent)
        }

        btndeleteTanaman.setSafeOnClickListener {
            mDialogView.btnYesDelete.setOnClickListener {
                realm.executeTransactionAsync({ inRealm ->
                    val results = inRealm.where(Tanaman::class.java).findAll()
                    val dataParty = results.where().equalTo("TanamanIdTemp", mplantId).findFirst()
                    val resultdataImagePlants = inRealm.where(DataImagePlants::class.java).findAll()
                    val dataimagePlants = resultdataImagePlants.where().equalTo("imageId", mplantId).findAll()
                    dataimagePlants?.deleteAllFromRealm()
                    dataParty?.deleteFromRealm()
                }, {
                    Log.d("delete", "onSuccess : delete single object")
                    mdialogConfirmDelete.dismiss()
                    finish()
                }, {
                    Log.d("delete", "onFailed : ${it.localizedMessage}")
                    Log.d("delete", "onFailed : delete single object")
                })
            }

            mDialogView.btnNoDelete.setSafeOnClickListener {
                mdialogConfirmDelete.dismiss()
            }
            mdialogConfirmDelete.show()
        }
    }

    private fun getdataDetailTanaman(tanamanIdTemp: Int) {
        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        val results = realm.where(Tanaman::class.java)
                .equalTo("TanamanIdTemp", tanamanIdTemp)
                .findFirst()
        listener.onChange(results!!)
    }

    private var listener: RealmChangeListener<Tanaman> = RealmChangeListener { results ->
        if (results.isLoaded) {
            // results is always up to date here
            // after a write to Realm from ANY thread!
            val a = results
            updateUi(results)
        }
    }


    @SuppressLint("SetTextI18n")
    private fun updateUi(data: Tanaman) {
        tvnamaPemilikTanaman.text = data.pemilikTanaman

        //setupLayout
        val item = findViewById<LinearLayout>(R.id.llitemfac2)
        item.removeAllViews()

        if(data.notes.isNullOrEmpty()){
            tvNotes.text = "-"
        }
        else {
            tvNotes.text = data.notes
        }

        val jenisTanamanId = data.jenisTanamanId
        val jenisTanamanIdreplace1 = jenisTanamanId.replace("[", "['")
        val jenisTanamanIdreplace2 = jenisTanamanIdreplace1.replace("]", "']")
        val jenisTanamanIdreplace3 = jenisTanamanIdreplace2.replace(", ", "','")

        val jenisTanaman = data.jenisTanamanNama
        val jenisTanamanreplace1 = jenisTanaman.replace("[", "['")
        val jenisTanamanreplace2 = jenisTanamanreplace1.replace("]", "']")
        val jenisTanamanreplace3 = jenisTanamanreplace2.replace(", ", "','")

        val kategoriTanaman = data.namaTanaman
        val kategoriTanamanreplace1 = kategoriTanaman.replace("[", "['")
        val kategoriTanamanreplace2 = kategoriTanamanreplace1.replace("]", "']")
        val kategoriTanamanreplace3 = kategoriTanamanreplace2.replace(", ", "','")

        val kategoriTanamanOther = data.namaTanamanOther
        val kategoriTanamanOther1 = kategoriTanamanOther.replace("[", "['")
        val kategoriTanamanOther2 = kategoriTanamanOther1.replace("]", "']")
        val kategoriTanamanOther3 = kategoriTanamanOther2.replace(", ", "','")

        val ukuranTanaman = data.ukuranTanaman
        val ukuranTanamanreplace1 = ukuranTanaman.replace("[", "['")
        val ukuranTanamanreplace2 = ukuranTanamanreplace1.replace("]", "']")
        val ukuranTanamanreplace3 = ukuranTanamanreplace2.replace(", ", "','")

        val jumlahTanaman = data.jumlahTanaman
        val jumlahTanamanreplace1 = jumlahTanaman.replace("[", "['")
        val jumlahTanamanreplace2 = jumlahTanamanreplace1.replace("]", "']")
        val jumlahTanamanreplace3 = jumlahTanamanreplace2.replace(", ", "','")

        val gson = Gson()
        val jenisTanamanIdConverter = gson.fromJson(jenisTanamanIdreplace3, Array<String>::class.java).toList()
        val jenisTanamanConverter = gson.fromJson(jenisTanamanreplace3, Array<String>::class.java).toList()
        val kategoriTanamanConverter = gson.fromJson(kategoriTanamanreplace3, Array<String>::class.java).toList()
        val kategoriTanamanOtherConverter = gson.fromJson(kategoriTanamanOther3, Array<String>::class.java).toList()
        val ukuaranTanamanConverter = gson.fromJson(ukuranTanamanreplace3, Array<String>::class.java).toList()
        val jumlahTanamanConverter = gson.fromJson(jumlahTanamanreplace3, Array<String>::class.java).toList()


        val mdatafotoTanaman: MutableList<DataImagePlants> = ArrayList()
        mdatafotoTanaman.addAll(data.dataImagePlants)
        adapterFotoPropertiTanaman.clear()
        adapterFotoPropertiTanaman.addItems(mdatafotoTanaman)

        if (jenisTanamanIdConverter.isNotEmpty()) {
            for (i in jenisTanamanIdConverter.indices) {
                val child = layoutInflater.inflate(R.layout.item_detail_tanaman_vertical, null)
                val txtjenisTanaman = child.findViewById(R.id.tvtitlejenisTanaman) as com.pixplicity.fontview.FontTextView
                val txttitleKategoriTanaman = child.findViewById(R.id.tvtitleKategoriTanaman) as com.pixplicity.fontview.FontTextView
                val txtKategoriTanaman = child.findViewById(R.id.tvisiKategoriTanaman) as com.pixplicity.fontview.FontTextView

                val txttitleUkuranTanaman = child.findViewById(R.id.tvtitleukuranTanaman) as com.pixplicity.fontview.FontTextView
                val txtUkuranTanaman = child.findViewById(R.id.tvisiukuranTanaman) as com.pixplicity.fontview.FontTextView

                val txttitleJumlahTanaman = child.findViewById(R.id.tvtitlejmlhTanaman) as com.pixplicity.fontview.FontTextView
                val txtJumlahTanaman = child.findViewById(R.id.tvisijmlhTanaman) as com.pixplicity.fontview.FontTextView

                if(kategoriTanamanOtherConverter[i].isNotEmpty()){
                    txtjenisTanaman.text = "Jenis ${jenisTanamanConverter[i]}"
                    txttitleKategoriTanaman.text = "Nama ${jenisTanamanConverter[i]}"
                    txtKategoriTanaman.text = kategoriTanamanOtherConverter[i]

                    txttitleUkuranTanaman.text = "Ukuran ${jenisTanamanConverter[i]}"
                    txtUkuranTanaman.text = ukuaranTanamanConverter[i]

                    txttitleJumlahTanaman.text = "Jumlah ${jenisTanamanConverter[i]}"
                    txtJumlahTanaman.text = jumlahTanamanConverter[i]
                }else{
                    txtjenisTanaman.text = "Jenis ${jenisTanamanConverter[i]}"
                    txttitleKategoriTanaman.text = "Nama ${jenisTanamanConverter[i]}"
                    txtKategoriTanaman.text = kategoriTanamanConverter[i]

                    txttitleUkuranTanaman.text = "Ukuran ${jenisTanamanConverter[i]}"
                    txtUkuranTanaman.text = ukuaranTanamanConverter[i]

                    txttitleJumlahTanaman.text = "Jumlah ${jenisTanamanConverter[i]}"
                    txtJumlahTanaman.text = jumlahTanamanConverter[i]
                }

                item.addView(child)
            }
        }

    }


    override fun onFragmentAttached() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFragmentDetached(tag: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRestart() {
        super.onRestart()
        getdataDetailTanaman(mplantId!!)
    }

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityDetailObjekTanaman::class.java)
        }
    }

}