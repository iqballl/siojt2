package mki.siojt2.ui.splash.view


import mki.siojt2.base.view.MvpView
import mki.siojt2.model.accesstoken.ResponseAccessToken
import mki.siojt2.model.master_data_bangunan.*
import mki.siojt2.model.master_data_form_tanah.*
import mki.siojt2.model.master_data_sarana_lain.*
import mki.siojt2.model.master_data_subjek.*
import mki.siojt2.model.master_data_tanaman.ResponseDataListKategoriRumpunWithOutParam
import mki.siojt2.model.master_data_tanaman.ResponseDataListKategoriTanaman
import mki.siojt2.model.master_data_tanaman.ResponseDataListKategoriTanamanWithOutParam
import mki.siojt2.model.region.*

/**
 * Created by iqbal on 09/10/17.
 */

interface SplashView : MvpView {

    fun openLoginActivity()

    fun openMainActivity()
    fun onsuccessReapet(responseDataCity: MutableList<ResponseDataCityWithoutParameter>)

    fun onsuccessgetDataKecamatan(responseDataKecamatan: MutableList<ResponseDataKecamatanWithoutParameter>, totalData: Int)

    fun onsuccessgetDataKelurahan(responseDataKelurahan: MutableList<ResponseDataKelurahanWithoutParameter>, totalData: Int)

    fun resultStringOfChecksForEachCondition(resultString: String?)

    fun onSuccessGetDataTanahWithOutParameter(
            responseDataListPenguasaanTanah: MutableList<ResponseDataListPenguasaanTanahWithOutParam>,
            responseDataBentukTanah: MutableList<ResponseDataListBentukTanahWithOutParam>,
            responseDataListTipografi: MutableList<ResponseDataListTipografiWithOutParam>,
            responseDataListPosisiTanah: MutableList<ResponseDataListPosisiTanahWithOutParam>,

            responseDataListKepemilikanAtasTanah: MutableList<ResponseDataListKepemilikanAtasTanahWithOutParam>,
            responseDataListPenggunaanTanah: MutableList<ResponseDataListPenggunaanTanahWithOutParam>,
            responseDataListInformasiTambahanTanah: MutableList<ResponseDataListInformasiTambahanTanahWithOutParam>,
            responseDataListPeruntukanTanah: MutableList<ResponseDataListPeruntukanTanahWithOutParam>
    )

    fun onSuccessGetDataBangunan(
            responseDataListDindingBangunan: MutableList<ResponseDataListDindingBangunanWithOutParam>,
            responseDataListKerangkaAtapBangunan: MutableList<ResponseDataListKerangkaAtapBangunanWithOutParam>,
            responseDataListPenutupAtapBangunan: MutableList<ResponseDataListPenutupAtapBangunanWithOutParam>,
            responseDataListLantaiBangunan: MutableList<ResponseDataListLantaiBangunanWithOutParam>,
            responseDataListPintuJendelaBangunan: MutableList<ResponseDataListPintuJendelaBangunanWithOutParam>,

            responseDataListPlafonBangunan: MutableList<ResponseDataListPlafonBangunanWithOutParam>,
            responseDataListPondasiBangunan: MutableList<ResponseDataListPondasiBangunanWithOutParam>,
            responseDataListStrukturBangunan: MutableList<ResponseDataListStrukturBangunanWithOutParam>,
            responseDataListTipeBangunan: MutableList<ResponseDataListTipeBangunanWithOutParam>
    )

    fun onSuccessgetMultipleRequestDataSubjekSaranaPelengkap(
            responseDataSumberIdentitas: MutableList<ResponseDataSumberIdentitasWithOutParam>,
            responseDataJenisSumberAir: MutableList<ResponseDataJenisSumberAirWithOutParam>,
            responseDataJenisPagarKeliling: MutableList<ResponseDataJenisPagarKelilingWithOutParam>,
            responseDataProvince: MutableList<ResponseDataProvinceWithoutParameter>,
            responseDataCity: MutableList<ResponseDataCityWithoutParameter>
    )

    fun onSucceccgetMultupleRequestDataReferenceDinasmis(
            responseDataMataPencaharianWithOutParam: MutableList<ResponseDataMataPencaharianWithOutParam>,
            responseDataPekerjaan: MutableList<ResponseDataPekerjaanWithOutParam>,
            responseDataListKategoriTanaman: MutableList<ResponseDataListKategoriTanamanWithOutParam>,
            responseDataListKategoriRumpun: MutableList<ResponseDataListKategoriRumpunWithOutParam>,
            responseDataJenisFasilitas: MutableList<ResponseDataJenisFasilitasWithOutParam>
    )
}
