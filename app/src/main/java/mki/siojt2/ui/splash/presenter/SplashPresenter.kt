package mki.siojt2.ui.splash.presenter

import android.annotation.SuppressLint
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.ui.splash.view.SplashView
import mki.siojt2.utils.ErrorHandler
import mki.siojt2.utils.rxJava.RxUtils
import io.reactivex.functions.*
import io.reactivex.schedulers.Schedulers
import mki.siojt2.model.master_data_bangunan.*
import mki.siojt2.model.master_data_form_tanah.*
import mki.siojt2.model.master_data_sarana_lain.ResponseDataJenisFasilitasWithOutParam
import mki.siojt2.model.master_data_sarana_lain.ResponseDataJenisPagarKelilingWithOutParam
import mki.siojt2.model.master_data_sarana_lain.ResponseDataJenisSumberAirWithOutParam
import mki.siojt2.model.master_data_subjek.ResponseDataMataPencaharianWithOutParam
import mki.siojt2.model.master_data_subjek.ResponseDataPekerjaanWithOutParam
import mki.siojt2.model.master_data_subjek.ResponseDataSumberIdentitasWithOutParam
import mki.siojt2.model.master_data_tanaman.ResponseDataListKategoriRumpunWithOutParam
import mki.siojt2.model.master_data_tanaman.ResponseDataListKategoriTanamanWithOutParam
import mki.siojt2.model.merge_result.*
import mki.siojt2.model.region.*
import rx.functions.*

/**
 * Created by iqbal.
 */

class SplashPresenter : BasePresenter(), SplashMVPPresenter {


    private var disposables: CompositeDisposable = CompositeDisposable()
    private var disposables2: CompositeDisposable = CompositeDisposable()
    private var disposables3: CompositeDisposable = CompositeDisposable()

    /* --DATA WILAYAH-- */
    private fun getlistOfProvinceWithOutParameter(): Single<MutableList<ResponseDataProvinceWithoutParameter>> {
        return Single.create { subscriber ->
            dataManager.getlistOfProvinceWithOutParameter()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getlistOfCityWithOutParameter(): Single<MutableList<ResponseDataCityWithoutParameter>> {
        return Single.create { subscriber ->
            dataManager.getlistOfCityWithOutParameter()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getlistOfKecamatanWithOutParameter(limit: Int, offset: Int): Single<MutableList<ResponseDataKecamatanWithoutParameter>> {
        return Single.create { subscriber ->
            dataManager.getlistOfKecamatanWithOutParameter(limit, offset)
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    /*private fun getlistOfKelurahanWithOutParameter(): Single<MutableList<ResponseDataKelurahanWithoutParameter>> {
        return Single.create { subscriber ->
            dataManager.getlistOfKelurahanWithOutParameter()
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }*/


    /* --DATA TANAH--*/
    private fun getlistPenguasaanAtasTanah(): Single<MutableList<ResponseDataListPenguasaanTanahWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarPenguasaanTanahWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }


    private fun getlistBentukTanah(): Single<MutableList<ResponseDataListBentukTanahWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarbentukTanahWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getlistTopografiTanah(): Single<MutableList<ResponseDataListTipografiWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarTopgrafiTanahWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getlistposisitanahTerhadapJalan(): Single<MutableList<ResponseDataListPosisiTanahWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarposisiTanahWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getlistKepemilikanTanah(): Single<MutableList<ResponseDataListKepemilikanAtasTanahWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarKepemilikanTanahWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getlistPenggunaanTanah(): Single<MutableList<ResponseDataListPenggunaanTanahWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarPenggunaanTanahWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getlistinfromasiTambahanTanah(): Single<MutableList<ResponseDataListInformasiTambahanTanahWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarinfoTambahanTanahWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    // peruntukan (kesesuaian RT/RW)
    private fun getlistPeruntukanTanah(): Single<MutableList<ResponseDataListPeruntukanTanahWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarPeruntukanTanahWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }


    /*  --DATA OBJEK BANGUNAN -- */
    private fun getdaftarDindingBangunanWithOutParam(): Single<MutableList<ResponseDataListDindingBangunanWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarDindingBangunanWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getdaftarKerangkaAtapBangunanWithOutParam(): Single<MutableList<ResponseDataListKerangkaAtapBangunanWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarKerangkaAtapBangunanWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getdaftarPenutupAtapBangunanWithOutParam(): Single<MutableList<ResponseDataListPenutupAtapBangunanWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarPenutupAtapBangunanWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getdaftarLantaiBangunanWithOutParam(): Single<MutableList<ResponseDataListLantaiBangunanWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarLantaiBangunanWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getdaftarPintuJendelaBangunanWithOutParam(): Single<MutableList<ResponseDataListPintuJendelaBangunanWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarPintuJendelaBangunanWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getdaftarPlafonBangunanWithOutParam(): Single<MutableList<ResponseDataListPlafonBangunanWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarPlafonBangunanWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getdaftarPondasiBangunanWithOutParam(): Single<MutableList<ResponseDataListPondasiBangunanWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarPondasiBangunanWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getdaftarStrukturBangunanWithOutParam(): Single<MutableList<ResponseDataListStrukturBangunanWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarStrukturBangunanWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getdaftarTipeBangunanWithOutParam(): Single<MutableList<ResponseDataListTipeBangunanWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarTipeBangunanWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    /* -- DATA SUBJEK/PEMILIK -- */
    private fun getdaftarMataPencaharianWithOutParam(): Single<MutableList<ResponseDataMataPencaharianWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarMataPencaharianWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getdaftarPekerjaanWithOutParam(): Single<MutableList<ResponseDataPekerjaanWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarPekerjaanWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    if(response.result!!.find { it.getliveliHoodName()!!.toLowerCase() == "lain-nya" } == null){
                                        val dat = ResponseDataPekerjaanWithOutParam()
                                        dat.setliveliHoodId(9999)
                                        dat.setliveliHoodName("Lain-nya")
                                        dat.setliveliHoodStatus(1)
                                        response.result!!.add(dat)
                                    }
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getdaftarSumberIdentitasWithOutParam(): Single<MutableList<ResponseDataSumberIdentitasWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarSumberIdentitasWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }


    /* -- DATA OBJEK TANAMAN -- */
    private fun getkategoriTanamanWithRealmObject(plantId: String): Single<MutableList<ResponseDataListKategoriTanamanWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getkategoriTanamanWithRealmObject(plantId)
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getkategoriRumpunWithRealmObject(plantId: String): Single<MutableList<ResponseDataListKategoriRumpunWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getkategoriRumpunWithRealmObject(plantId)
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }


    /*  -- DATA OBJEK SARANA PELENGKAP -- */
    private fun getdaftarjenisFasilitasWithOutParam(): Single<MutableList<ResponseDataJenisFasilitasWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarjenisFasilitasWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getdaftarsumberAirWithOutParam(): Single<MutableList<ResponseDataJenisSumberAirWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarsumberAirWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    private fun getdaftarpagarKelilingWithOutParam(): Single<MutableList<ResponseDataJenisPagarKelilingWithOutParam>> {
        return Single.create { subscriber ->
            dataManager.getdaftarpagarKelilingWithOutParam()
                    .compose(RxUtils.applyScheduler())
                    .subscribe({ response ->
                        if (response.status == Constant.STATUS_ERROR) {
                            subscriber.onError(Throwable(response.message))
                        } else {
                            if (response.result != null && response.status == Constant.STATUS_SUCCESS) {
                                if (!subscriber.isDisposed) {
                                    subscriber.onSuccess(response.result!!)
                                }
                            } else {
                                if (!subscriber.isDisposed) {
                                    subscriber.onError(Throwable(response.message))
                                }
                            }
                        }

                    }, { throwable ->
                        throwable.printStackTrace()
                        if (!subscriber.isDisposed) {
                            subscriber.onError(throwable)
                        }
                    })
        }
    }

    /* Merge Data Wilayah */
    /*override fun getMultipleRequestRegion(limit: Int, offset: Int) {
        disposables.add(
                Single.zip(
                        getlistOfProvinceWithOutParameter(),
                        getlistOfCityWithOutParameter(limit, offset),
                        getlistOfKecamatanWithOutParameter(limit, offset),
                        object : Func3<MutableList<ResponseDataProvinceWithoutParameter>,
                                MutableList<ResponseDataCityWithoutParameter>,
                                MutableList<ResponseDataKecamatanWithoutParameter>,
                                MergedResponseRegion>,

                                Function3<MutableList<ResponseDataProvinceWithoutParameter>,
                                        MutableList<ResponseDataCityWithoutParameter>,
                                        MutableList<ResponseDataKecamatanWithoutParameter>,
                                        MergedResponseRegion> {
                            override fun call(t1: MutableList<ResponseDataProvinceWithoutParameter>?, t2: MutableList<ResponseDataCityWithoutParameter>?, t3: MutableList<ResponseDataKecamatanWithoutParameter>?): MergedResponseRegion {
                                return MergedResponseRegion(t1!!, t2!!, t3!!)
                            }

                            override fun apply(t1: MutableList<ResponseDataProvinceWithoutParameter>, t2: MutableList<ResponseDataCityWithoutParameter>, t3: MutableList<ResponseDataKecamatanWithoutParameter>): MergedResponseRegion {
                                return MergedResponseRegion(t1, t2, t3)
                            }
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ response ->
                            //Log.d(" - total health questions downloaded %d", response.listQuestions.size())
                            //Log.d(" - physicians downloaded %d", response.listPhysicians.size())
                            splashView().onSuccessgetlistOfCityWithOutParameter(
                                    response.mlistOfProvince,
                                    response.mlistOfCity,
                                    response.mlistOfKecamatan)
                        }, { errorThrowable ->
                            // process error - show network error message
                            splashView().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(splashView(), errorThrowable)
                        })

        )
    }*/


    /* Merge Data Tanah */
    override fun getMultipleRequestDataLand() {
        //splashView().onShowLoading()
        disposables3.add(
                Single.zip(
                        getlistPenguasaanAtasTanah(),
                        getlistBentukTanah(),
                        getlistTopografiTanah(),
                        getlistposisitanahTerhadapJalan(),
                        getlistKepemilikanTanah(),
                        getlistPenggunaanTanah(),
                        getlistinfromasiTambahanTanah(),
                        getlistPeruntukanTanah(),
                        object : Func8<
                                MutableList<ResponseDataListPenguasaanTanahWithOutParam>,
                                MutableList<ResponseDataListBentukTanahWithOutParam>,
                                MutableList<ResponseDataListTipografiWithOutParam>,
                                MutableList<ResponseDataListPosisiTanahWithOutParam>,
                                MutableList<ResponseDataListKepemilikanAtasTanahWithOutParam>,
                                MutableList<ResponseDataListPenggunaanTanahWithOutParam>,
                                MutableList<ResponseDataListInformasiTambahanTanahWithOutParam>,
                                MutableList<ResponseDataListPeruntukanTanahWithOutParam>,
                                MergedResponseDataTanah>,
                                Function8<
                                        MutableList<ResponseDataListPenguasaanTanahWithOutParam>,
                                        MutableList<ResponseDataListBentukTanahWithOutParam>,
                                        MutableList<ResponseDataListTipografiWithOutParam>,
                                        MutableList<ResponseDataListPosisiTanahWithOutParam>,
                                        MutableList<ResponseDataListKepemilikanAtasTanahWithOutParam>,
                                        MutableList<ResponseDataListPenggunaanTanahWithOutParam>,
                                        MutableList<ResponseDataListInformasiTambahanTanahWithOutParam>,
                                        MutableList<ResponseDataListPeruntukanTanahWithOutParam>,
                                        MergedResponseDataTanah> {
                            override fun call(t1: MutableList<ResponseDataListPenguasaanTanahWithOutParam>?, t2: MutableList<ResponseDataListBentukTanahWithOutParam>?, t4: MutableList<ResponseDataListTipografiWithOutParam>?, t5: MutableList<ResponseDataListPosisiTanahWithOutParam>?, t6: MutableList<ResponseDataListKepemilikanAtasTanahWithOutParam>?, t7: MutableList<ResponseDataListPenggunaanTanahWithOutParam>?, t8: MutableList<ResponseDataListInformasiTambahanTanahWithOutParam>?, t9: MutableList<ResponseDataListPeruntukanTanahWithOutParam>?): MergedResponseDataTanah {
                                return MergedResponseDataTanah(t1!!, t2!!, t4!!, t5!!, t6!!, t7!!, t8!!, t9!!)
                            }

                            override fun apply(t1: MutableList<ResponseDataListPenguasaanTanahWithOutParam>, t2: MutableList<ResponseDataListBentukTanahWithOutParam>, t4: MutableList<ResponseDataListTipografiWithOutParam>, t5: MutableList<ResponseDataListPosisiTanahWithOutParam>, t6: MutableList<ResponseDataListKepemilikanAtasTanahWithOutParam>, t7: MutableList<ResponseDataListPenggunaanTanahWithOutParam>, t8: MutableList<ResponseDataListInformasiTambahanTanahWithOutParam>, t9: MutableList<ResponseDataListPeruntukanTanahWithOutParam>): MergedResponseDataTanah {
                                return MergedResponseDataTanah(t1, t2, t4, t5, t6, t7, t8, t9)
                            }

                        })
                        .compose(RxUtils.ioToMainSingleScheduler())
                        .subscribe({ response ->
                            //Log.d(" - total health questions downloaded %d", response.listQuestions.size())
                            //Log.d(" - physicians downloaded %d", response.listPhysicians.size())
                            //splashView().onsuccessgetlistOfCityWithOutParameter(response.mlistOfCity)
                            splashView().onSuccessGetDataTanahWithOutParameter(
                                    response.mlistOfPenguasaanAtasTanah,
                                    response.mlistOfBentukTanah,
                                    response.mlistOfTipographiTanah,
                                    response.mlistOfPosisiTanah,
                                    response.mlistOfKepemilikanTanah,
                                    response.mlistOfPenggunaanTanah,
                                    response.mlistOfInformasiTambahanTanah,
                                    response.mlistOfPeruntukanTanah)
                        }, { errorThrowable ->
                            // process error - show network error message
                            splashView().onDismissLoading()
                            disposables.clear()
                            disposables.dispose()
                            splashView().resultStringOfChecksForEachCondition(null)
                            ErrorHandler.handlerErrorPresenter(splashView(), errorThrowable)
                        })

        )
    }

    /* Merge Data Objek Bangunan */
    override fun getMultipleRequestDataBangunan() {
        disposables.add(
                Single.zip(
                        getdaftarDindingBangunanWithOutParam(),
                        getdaftarKerangkaAtapBangunanWithOutParam(),
                        getdaftarPenutupAtapBangunanWithOutParam(),
                        getdaftarLantaiBangunanWithOutParam(),
                        getdaftarPintuJendelaBangunanWithOutParam(),

                        getdaftarPlafonBangunanWithOutParam(),
                        getdaftarPondasiBangunanWithOutParam(),
                        getdaftarStrukturBangunanWithOutParam(),
                        getdaftarTipeBangunanWithOutParam(),

                        object : Func9<MutableList<ResponseDataListDindingBangunanWithOutParam>,
                                MutableList<ResponseDataListKerangkaAtapBangunanWithOutParam>,
                                MutableList<ResponseDataListPenutupAtapBangunanWithOutParam>,
                                MutableList<ResponseDataListLantaiBangunanWithOutParam>,
                                MutableList<ResponseDataListPintuJendelaBangunanWithOutParam>,

                                MutableList<ResponseDataListPlafonBangunanWithOutParam>,
                                MutableList<ResponseDataListPondasiBangunanWithOutParam>,
                                MutableList<ResponseDataListStrukturBangunanWithOutParam>,
                                MutableList<ResponseDataListTipeBangunanWithOutParam>,
                                MergedResponseDataBangunan>,

                                Function9<MutableList<ResponseDataListDindingBangunanWithOutParam>,
                                        MutableList<ResponseDataListKerangkaAtapBangunanWithOutParam>,
                                        MutableList<ResponseDataListPenutupAtapBangunanWithOutParam>,
                                        MutableList<ResponseDataListLantaiBangunanWithOutParam>,
                                        MutableList<ResponseDataListPintuJendelaBangunanWithOutParam>,

                                        MutableList<ResponseDataListPlafonBangunanWithOutParam>,
                                        MutableList<ResponseDataListPondasiBangunanWithOutParam>,
                                        MutableList<ResponseDataListStrukturBangunanWithOutParam>,
                                        MutableList<ResponseDataListTipeBangunanWithOutParam>,

                                        MergedResponseDataBangunan> {
                            override fun call(t1: MutableList<ResponseDataListDindingBangunanWithOutParam>?, t2: MutableList<ResponseDataListKerangkaAtapBangunanWithOutParam>?, t3: MutableList<ResponseDataListPenutupAtapBangunanWithOutParam>?, t4: MutableList<ResponseDataListLantaiBangunanWithOutParam>?, t5: MutableList<ResponseDataListPintuJendelaBangunanWithOutParam>?, t6: MutableList<ResponseDataListPlafonBangunanWithOutParam>?, t7: MutableList<ResponseDataListPondasiBangunanWithOutParam>?, t8: MutableList<ResponseDataListStrukturBangunanWithOutParam>?, t9: MutableList<ResponseDataListTipeBangunanWithOutParam>?): MergedResponseDataBangunan {
                                return MergedResponseDataBangunan(t1!!, t2!!, t3!!, t4!!, t5!!, t6!!, t7!!, t8!!, t9!!)
                            }

                            override fun apply(t1: MutableList<ResponseDataListDindingBangunanWithOutParam>, t2: MutableList<ResponseDataListKerangkaAtapBangunanWithOutParam>, t3: MutableList<ResponseDataListPenutupAtapBangunanWithOutParam>, t4: MutableList<ResponseDataListLantaiBangunanWithOutParam>, t5: MutableList<ResponseDataListPintuJendelaBangunanWithOutParam>, t6: MutableList<ResponseDataListPlafonBangunanWithOutParam>, t7: MutableList<ResponseDataListPondasiBangunanWithOutParam>, t8: MutableList<ResponseDataListStrukturBangunanWithOutParam>, t9: MutableList<ResponseDataListTipeBangunanWithOutParam>): MergedResponseDataBangunan {
                                return MergedResponseDataBangunan(t1, t2, t3, t4, t5, t6, t7, t8, t9)
                            }
                            /* override fun call(t1: MutableList<ResponseDataListDindingBangunanWithOutParam>?, t2: MutableList<ResponseDataListKerangkaAtapBangunanWithOutParam>?, t3: MutableList<ResponseDataListPenutupAtapBangunanWithOutParam>?, t4: MutableList<ResponseDataListLantaiBangunanWithOutParam>?, t5: MutableList<ResponseDataListPintuJendelaBangunanWithOutParam>?): MergedResponseDataBangunan {
                                 return MergedResponseDataBangunan(t1!!, t2!!, t3!!, t4!!, t5!!)
                             }

                             override fun apply(t1: MutableList<ResponseDataListDindingBangunanWithOutParam>, t2: MutableList<ResponseDataListKerangkaAtapBangunanWithOutParam>, t3: MutableList<ResponseDataListPenutupAtapBangunanWithOutParam>, t4: MutableList<ResponseDataListLantaiBangunanWithOutParam>, t5: MutableList<ResponseDataListPintuJendelaBangunanWithOutParam>): MergedResponseDataBangunan {
                                 return MergedResponseDataBangunan(t1, t2, t3, t4, t5)
                             }*/

                        })
                        .compose(RxUtils.ioToMainSingleScheduler())
                        .subscribe({ response ->
                            //Log.d(" - total health questions downloaded %d", response.listQuestions.size())
                            //Log.d(" - physicians downloaded %d", response.listPhysicians.size())

                            splashView().onSuccessGetDataBangunan(
                                    response.listOfDindingBangunan,
                                    response.listOfKerangkaAtapBangunan,
                                    response.listOfPenutupAtapBangunan,
                                    response.listOfLantaiBangunan,
                                    response.listOfPintuJendelaBangunan,

                                    response.listOfPlafonBangunan,
                                    response.listOfPondasiBangunan,
                                    response.listOfStukturBangunan,
                                    response.listOfTipeBangunan
                            )

                        }, { errorThrowable ->
                            // process error - show network error message
                            splashView().onDismissLoading()
                            disposables.clear()
                            disposables.dispose()
                            splashView().resultStringOfChecksForEachCondition(null)
                            ErrorHandler.handlerErrorPresenter(splashView(), errorThrowable)
                        })

        )
    }


    override fun getMultipleRequestDataSubjekSaranaPelengkap() {
        disposables.add(
                Single.zip(
                        getdaftarSumberIdentitasWithOutParam(),
                        getdaftarsumberAirWithOutParam(),
                        getdaftarpagarKelilingWithOutParam(),
                        getlistOfProvinceWithOutParameter(),
                        getlistOfCityWithOutParameter(),
                        object : Func5<
                                MutableList<ResponseDataSumberIdentitasWithOutParam>,
                                MutableList<ResponseDataJenisSumberAirWithOutParam>,
                                MutableList<ResponseDataJenisPagarKelilingWithOutParam>,
                                MutableList<ResponseDataProvinceWithoutParameter>,
                                MutableList<ResponseDataCityWithoutParameter>,
                                MergedResponseDataSubjekSaranaPelengkap>,

                                Function5<
                                        MutableList<ResponseDataSumberIdentitasWithOutParam>,
                                        MutableList<ResponseDataJenisSumberAirWithOutParam>,
                                        MutableList<ResponseDataJenisPagarKelilingWithOutParam>,
                                        MutableList<ResponseDataProvinceWithoutParameter>,
                                        MutableList<ResponseDataCityWithoutParameter>,
                                        MergedResponseDataSubjekSaranaPelengkap> {
                            override fun call(t1: MutableList<ResponseDataSumberIdentitasWithOutParam>?, t2: MutableList<ResponseDataJenisSumberAirWithOutParam>?, t3: MutableList<ResponseDataJenisPagarKelilingWithOutParam>?, t4: MutableList<ResponseDataProvinceWithoutParameter>?, t5: MutableList<ResponseDataCityWithoutParameter>?): MergedResponseDataSubjekSaranaPelengkap {
                                return MergedResponseDataSubjekSaranaPelengkap(t1!!, t2!!, t3!!, t4!!, t5!!)
                            }

                            override fun apply(t1: MutableList<ResponseDataSumberIdentitasWithOutParam>, t2: MutableList<ResponseDataJenisSumberAirWithOutParam>, t3: MutableList<ResponseDataJenisPagarKelilingWithOutParam>, t4: MutableList<ResponseDataProvinceWithoutParameter>, t5: MutableList<ResponseDataCityWithoutParameter>): MergedResponseDataSubjekSaranaPelengkap {
                                return MergedResponseDataSubjekSaranaPelengkap(t1, t2, t3, t4, t5)
                            }

                            /*override fun apply(t1: MutableList<ResponseDataMataPencaharianWithOutParam>, t2: MutableList<ResponseDataPekerjaanWithOutParam>, t3: MutableList<ResponseDataSumberIdentitasWithOutParam>, t4: MutableList<ResponseDataListKategoriTanamanWithOutParam>, t5: MutableList<ResponseDataListKategoriRumpunWithOutParam>, t6: MutableList<ResponseDataJenisFasilitasWithOutParam>, t7: MutableList<ResponseDataJenisSumberAirWithOutParam>, t8: MutableList<ResponseDataJenisPagarKelilingWithOutParam>): MergedResponseDataSubjekSaranaPelengkap {
                                return MergedResponseDataSubjekSaranaPelengkap(t1, t2, t3, t4, t5, t6, t7, t8)
                            }

                            override fun call(t1: MutableList<ResponseDataMataPencaharianWithOutParam>?, t2: MutableList<ResponseDataPekerjaanWithOutParam>?, t3: MutableList<ResponseDataSumberIdentitasWithOutParam>?, t4: MutableList<ResponseDataListKategoriTanamanWithOutParam>?, t5: MutableList<ResponseDataListKategoriRumpunWithOutParam>?, t6: MutableList<ResponseDataJenisFasilitasWithOutParam>?, t7: MutableList<ResponseDataJenisSumberAirWithOutParam>?, t8: MutableList<ResponseDataJenisPagarKelilingWithOutParam>?): MergedResponseDataSubjekSaranaPelengkap {
                                return MergedResponseDataSubjekSaranaPelengkap(t1!!, t2!!, t3!!, t4!!, t5!!, t6!!, t7!!, t8!!)
                            }*/


                        })
                        .compose(RxUtils.ioToMainSingleScheduler())
                        .subscribe({ response ->
                            splashView().onDismissLoading()
                            splashView().onSuccessgetMultipleRequestDataSubjekSaranaPelengkap(
                                    response.mlistOfSumberIdentitas,
                                    response.mlistOfSumberAir,
                                    response.mlistOfPagarKeliling,
                                    response.mlistOfProvince,
                                    response.mlistOfCity
                            )
                            //Log.d(" - total health questions downloaded %d", response.listQuestions.size())
                            //Log.d(" - physicians downloaded %d", response.listPhysicians.size())

                        }, { errorThrowable ->
                            // process error - show network error message
                            splashView().onDismissLoading()
                            disposables.clear()
                            disposables.dispose()
                            splashView().resultStringOfChecksForEachCondition(null)
                            ErrorHandler.handlerErrorPresenter(splashView(), errorThrowable)
                        })

        )
    }

    /* Merge data reference dinamis */
    override fun getMultipleRequestDataReferenceDinasmis(plantId: String, rumpunId: String) {
        //splashView().onShowLoading()
        disposables.add(
                Single.zip(
                        getkategoriTanamanWithRealmObject(plantId),
                        getkategoriRumpunWithRealmObject(rumpunId),
                        getdaftarMataPencaharianWithOutParam(),
                        getdaftarPekerjaanWithOutParam(),
                        getdaftarjenisFasilitasWithOutParam(),
                        object : Func5<
                                MutableList<ResponseDataListKategoriTanamanWithOutParam>,
                                MutableList<ResponseDataListKategoriRumpunWithOutParam>,
                                MutableList<ResponseDataMataPencaharianWithOutParam>,
                                MutableList<ResponseDataPekerjaanWithOutParam>,
                                MutableList<ResponseDataJenisFasilitasWithOutParam>,
                                MergedResponseDataReferenceDinamis>,

                                Function5<
                                        MutableList<ResponseDataListKategoriTanamanWithOutParam>,
                                        MutableList<ResponseDataListKategoriRumpunWithOutParam>,
                                        MutableList<ResponseDataMataPencaharianWithOutParam>,
                                        MutableList<ResponseDataPekerjaanWithOutParam>,
                                        MutableList<ResponseDataJenisFasilitasWithOutParam>,
                                        MergedResponseDataReferenceDinamis> {
                            override fun call(t1: MutableList<ResponseDataListKategoriTanamanWithOutParam>?, t2: MutableList<ResponseDataListKategoriRumpunWithOutParam>?, t3: MutableList<ResponseDataMataPencaharianWithOutParam>?, t4: MutableList<ResponseDataPekerjaanWithOutParam>?, t5: MutableList<ResponseDataJenisFasilitasWithOutParam>?): MergedResponseDataReferenceDinamis {
                                return MergedResponseDataReferenceDinamis(t1!!, t2!!, t3!!, t4!!, t5!!)
                            }

                            override fun apply(t1: MutableList<ResponseDataListKategoriTanamanWithOutParam>, t2: MutableList<ResponseDataListKategoriRumpunWithOutParam>, t3: MutableList<ResponseDataMataPencaharianWithOutParam>, t4: MutableList<ResponseDataPekerjaanWithOutParam>, t5: MutableList<ResponseDataJenisFasilitasWithOutParam>): MergedResponseDataReferenceDinamis {
                                return MergedResponseDataReferenceDinamis(t1, t2, t3, t4, t5)
                            }


                        })
                        .compose(RxUtils.ioToMainSingleScheduler())
                        .subscribe({ response ->
                            if(response.listOfPekerjaan.find { it.getliveliHoodName()!!.toLowerCase() == "lain-nya" } == null){
                                val dat = ResponseDataPekerjaanWithOutParam()
                                dat.setliveliHoodId(9999)
                                dat.setliveliHoodName("Lain-nya")
                                dat.setliveliHoodStatus(1)
                                response.listOfPekerjaan.add(dat)
                            }
                            splashView().onDismissLoading()
                            //Log.d(" - total health questions downloaded %d", response.listQuestions.size())
                            //Log.d(" - physicians downloaded %d", response.listPhysicians.size())
                            splashView().onSucceccgetMultupleRequestDataReferenceDinasmis(
                                    response.listOfMataPencaharian,
                                    response.listOfPekerjaan,
                                    response.listOfKategoriTanaman,
                                    response.listOfKategoriRumpun,
                                    response.listOfJenisFasilitas
                            )

                        }, { errorThrowable ->
                            // process error - show network error message
                            splashView().onDismissLoading()
                            disposables.clear()
                            disposables.dispose()
                            splashView().resultStringOfChecksForEachCondition(null)
                            ErrorHandler.handlerErrorPresenter(splashView(), errorThrowable)
                        })

        )
    }


    /*@SuppressLint("CheckResult")
    override fun getregioBirthPlaceWitoutParameter() {

        //val requests = ArrayList<Observable<*>>()
        *//*Observable.zip(
                dataManager.getdaftarjenisFasilitas().compose(RxUtils.applyScheduler()),
                dataManager.getdaftarsumberAir().compose(RxUtils.applyScheduler()),
                object : Func2<ResponseDataList<ResponseDataJenisFasilitas>, ResponseDataList<ResponseDataJenisSumberAir>, UserAndEvents>, BiFunction<ResponseDataList<ResponseDataJenisFasilitas>, ResponseDataList<ResponseDataJenisSumberAir>, Any> {
                    override fun apply(t1: ResponseDataList<ResponseDataJenisFasilitas>, t2: ResponseDataList<ResponseDataJenisSumberAir>): Any {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun call(t1: ResponseDataList<ResponseDataJenisFasilitas>?, t2: ResponseDataList<ResponseDataJenisSumberAir>?): UserAndEvents {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                })
                .subscribe {

                }*//*

        *//*Observable.just(retrofit.create(StoreCouponsApi::class.java)).subscribeOn(Schedulers.computation())
                .flatMap({ s->
                    val couponsObservable = s.getCoupons("topcoupons").subscribeOn(Schedulers.io())
                    val storeInfoObservable = s.getStoreInfo().subscribeOn(Schedulers.io())
                    Observable.concatArray(couponsObservable, storeInfoObservable) })
                .observeOn(AndroidSchedulers.mainThread()).subscribe(???({ this.handleResults() }), ???({ this.handleError() }))*//*
        *//* disposables.add(
                 Observable.just(dataManager)
                         .subscribeOn(Schedulers.computation())
                         .flatMap { s ->
                             Observable.concatArray(
                                     s.getdaftarBirthPlaceWithOutParameter()
                                             .compose(RxUtils.applyScheduler()),
                                     s.getdaftarpagarKeliling()
                                             .compose(RxUtils.applyScheduler()))
                         }
                         .observeOn(AndroidSchedulers.mainThread())
                         .subscribe {

                         }
         )*//*


        *//*disposables.add(
                dataManager.getdaftarBirthPlaceWithOutParameter()
                        .doOnTerminate { accessTokenView().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            accessTokenView().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                accessTokenView().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    accessTokenView().onsuccessgetregioBirthPlace(authResponseDataObject.result!!)
                                } else {
                                    accessTokenView().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )*//*
    }*/

    /* fun pagedDataOf(nextPageStartingFrom: (start: Int) -> Flowable<MutableList): Flowable<List<TData>> {
         return Flowable.generate<List<TData>, Int>(
                 Callable<Int> { 0 },
                 BiFunction<Int, Emitter<List<TData>>, Int> { index, emitter ->
                     nextPageStartingFrom(index)
                             .doOnNext { page ->
                                 if (!page.values.isEmpty()) {
                                     emitter.onNext(page.values)
                                 }

                                 if (page.isLastPage) {
                                     emitter.onComplete()
                                 }
                             }
                             .map { data -> data.nextPageStart }
                             .blockingFirst()
                 })
     }*/


    @SuppressLint("CheckResult")
    override fun requestPagingCity() {
        /*val mlimit = 50
        val page = mlimit
        Observable
                .range(0, Integer.MAX_VALUE - 1)
                .concatMap(object : Func1<Int, Observable<MutableList<ResponseDataCityWithoutParameter>>>,
                        Function<Int, ObservableSource<MutableList<ResponseDataCityWithoutParameter>>> {
                    override fun call(t: Int?): Observable<MutableList<ResponseDataCityWithoutParameter>> {
                        return mgetlistOfCityWithOutParameter(mlimit, t!! * page)
                    }

                    override fun apply(t: Int): ObservableSource<MutableList<ResponseDataCityWithoutParameter>> {
                        return mgetlistOfCityWithOutParameter(mlimit, t * page)
                    }
                })
                .takeWhile(object : Func1<MutableList<ResponseDataCityWithoutParameter>, Boolean>,
                        Predicate<MutableList<ResponseDataCityWithoutParameter>> {
                    override fun call(results: MutableList<ResponseDataCityWithoutParameter>?): Boolean {
                        return results!!.isNotEmpty()
                    }

                    override fun test(results: MutableList<ResponseDataCityWithoutParameter>): Boolean {
                        return results.isNotEmpty()
                    }

                })
                .scan(object : Func2<MutableList<ResponseDataCityWithoutParameter>, MutableList<ResponseDataCityWithoutParameter>, MutableList<ResponseDataCityWithoutParameter>>, BiFunction<MutableList<ResponseDataCityWithoutParameter>, MutableList<ResponseDataCityWithoutParameter>, MutableList<ResponseDataCityWithoutParameter>> {
                    override fun apply(t1: MutableList<ResponseDataCityWithoutParameter>, t2: MutableList<ResponseDataCityWithoutParameter>): MutableList<ResponseDataCityWithoutParameter> {
                        val list: MutableList<ResponseDataCityWithoutParameter> = ArrayList()
                        list.addAll(t1)
                        list.addAll(t2)
                        return list
                    }

                    override fun call(results: MutableList<ResponseDataCityWithoutParameter>, results2: MutableList<ResponseDataCityWithoutParameter>): MutableList<ResponseDataCityWithoutParameter> {
                        val list: MutableList<ResponseDataCityWithoutParameter> = ArrayList()
                        list.addAll(results)
                        list.addAll(results2)
                        return list
                    }
                })*/

    }

    override fun getdatakecamatan(limit: Int, offset: Int) {
        disposables.add(
                dataManager.getlistOfKecamatanWithOutParameter(limit, offset)
                        //.compose<ResponseDataObject<AccessToken>>(RxUtils.applyApiCall<ResponseDataObject<AccessToken>>())
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ accessTokenResponseDataObject ->
                            if (accessTokenResponseDataObject.status == Constant.STATUS_TOKEN_EXPIRED) {
                                splashView().onTokenExpired()
                            } else {
                                if (accessTokenResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    splashView().onsuccessgetDataKecamatan(
                                            accessTokenResponseDataObject.result!!,
                                            accessTokenResponseDataObject.totaldata)
                                } else {
                                    splashView().onFailed(accessTokenResponseDataObject.message!!)
                                }
                            }
                        }
                        ) { throwable ->
                            disposables.clear()
                            disposables.dispose()
                            splashView().onDismissLoading()
                            splashView().resultStringOfChecksForEachCondition(null)
                            ErrorHandler.handlerErrorPresenter(splashView(), throwable)
                        }
        )
    }

    override fun getdatakelurahan(limit: Int, offset: Int) {
        disposables.add(
                dataManager.getlistOfKelurahanWithOutParameter(limit, offset)
                        //.compose<ResponseDataObject<AccessToken>>(RxUtils.applyApiCall<ResponseDataObject<AccessToken>>())
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ accessTokenResponseDataObject ->
                            if (accessTokenResponseDataObject.status == Constant.STATUS_TOKEN_EXPIRED) {
                                splashView().onTokenExpired()
                            } else {
                                if (accessTokenResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    splashView().onsuccessgetDataKelurahan(
                                            accessTokenResponseDataObject.result!!,
                                            accessTokenResponseDataObject.totaldata)
                                } else {
                                    splashView().onFailed(accessTokenResponseDataObject.message!!)
                                }
                            }
                        }
                        ) { throwable ->
                            disposables.clear()
                            disposables.dispose()
                            splashView().resultStringOfChecksForEachCondition(null)
                            ErrorHandler.handlerErrorPresenter(splashView(), throwable)
                        }
        )
    }


    override fun onDetach() {
        super.onDetach()
        disposables.dispose()
    }

    private fun splashView(): SplashView {
        return getView() as SplashView
    }
}
