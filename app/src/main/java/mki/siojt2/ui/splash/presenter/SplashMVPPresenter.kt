package mki.siojt2.ui.splash.presenter

import io.reactivex.Observable
import mki.siojt2.base.presenter.MVPPresenter
import mki.siojt2.model.master_data_form_tanah.ResponseDataListPenguasaanTanahWithOutParam

interface SplashMVPPresenter : MVPPresenter {

    fun getdatakecamatan(limit: Int, offset: Int)
    fun getdatakelurahan(limit: Int, offset: Int)

    fun getMultipleRequestDataLand()

    fun getMultipleRequestDataBangunan()

    fun getMultipleRequestDataSubjekSaranaPelengkap()

    fun getMultipleRequestDataReferenceDinasmis(plantId: String, rumpunId: String)

    fun requestPagingCity()

}