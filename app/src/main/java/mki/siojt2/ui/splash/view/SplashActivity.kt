package mki.siojt2.ui.splash.view

import android.annotation.SuppressLint
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.PixelFormat
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AlertDialog
import android.util.Log
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.tapadoo.alerter.Alerter
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.android.synthetic.main.dialog_popup_sync.view.*
import mki.siojt2.R
import mki.siojt2.SIOJT2
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.model.SyncDataDinamisSplashScreen
import mki.siojt2.model.SyncDataSplashScreen
import mki.siojt2.model.accesstoken.ResponseAccessToken
import mki.siojt2.model.master_data_bangunan.*
import mki.siojt2.model.master_data_form_tanah.*
import mki.siojt2.model.master_data_sarana_lain.ResponseDataJenisFasilitasWithOutParam
import mki.siojt2.model.master_data_sarana_lain.ResponseDataJenisPagarKelilingWithOutParam
import mki.siojt2.model.master_data_sarana_lain.ResponseDataJenisSumberAirWithOutParam
import mki.siojt2.model.master_data_subjek.ResponseDataMataPencaharianWithOutParam
import mki.siojt2.model.master_data_subjek.ResponseDataPekerjaanWithOutParam
import mki.siojt2.model.master_data_subjek.ResponseDataSumberIdentitasWithOutParam
import mki.siojt2.model.master_data_tanaman.ResponseDataListKategoriRumpunWithOutParam
import mki.siojt2.model.master_data_tanaman.ResponseDataListKategoriTanamanWithOutParam
import mki.siojt2.model.region.*
import mki.siojt2.persistence.Preference
import mki.siojt2.ui.login.view.LoginActivity
import mki.siojt2.ui.main.view.MainActivity
import mki.siojt2.ui.splash.presenter.SplashPresenter
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.Utils
import mki.siojt2.utils.extension.SyncConfig
import mki.siojt2.utils.network.ConnectivityReceiver
import mki.siojt2.utils.network.sample.InternetConnectionListener
import java.io.IOException
import java.util.*

class SplashActivity : BaseActivity(), SplashView, InternetConnectionListener, ConnectivityReceiver.ConnectivityReceiverListener {

    private var tt: TimerTask? = null
    private var timer: Timer? = null
    private var isCancelled = false
    private var misshowDialog = false

    private var splashPresenter: SplashPresenter? = null
    private lateinit var realm: Realm
    private var msyncData = SyncDataSplashScreen()
    private var msyncDataDinamis = SyncDataDinamisSplashScreen()
    private var currentDatePlusSevenDays: Date? = null
    private var currentDatePlusOneDays: Date? = null

    private var receiverNetwork: ConnectivityReceiver? = null
    private var laststatusIsConnected: Boolean = false

    lateinit var mCalendar: Calendar
    private val dateTemplate = "dd MMMM yyyy"

    lateinit var synConfig: SyncConfig

    //internal lateinit var splashPresenter: SplashMVPPresenter<SplashView>

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }

    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        //window.decorView.systemUiVisibility = SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = Color.parseColor("#B24F0E")
        }

        /* init realm */
        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        /* init file sync config */
        synConfig = SyncConfig(this@SplashActivity)

        // init receiver
        receiverNetwork = ConnectivityReceiver()
        SIOJT2.instance?.setInternetConnectionListener(this)
        val intentFilter = IntentFilter()

        // Add network connectivity change action.
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        // Set broadcast receiver priority.
        intentFilter.priority = 100
        //register connection receiver
        registerReceiver(receiverNetwork, intentFilter)

        // register connection status listener
        SIOJT2.instance?.setConnectivityListener(this)

        //splashPresenter.onAttach(this)
        /*Simple hold animation to hold ImageView in the centre of the screen at a slightly larger
        scale than the ImageView's original one.*/
        val up = AnimationUtils.loadAnimation(this, R.anim.hold)

        /* Translates ImageView from current position to its original position, scales it from
        larger scale to its original scale.*/
        val translateScale = AnimationUtils.loadAnimation(this, R.anim.translate_scale)


        // get current date with time
        /*val currentDate = Date()
        // convert date to calendar
        val c = Calendar.getInstance(Locale.getDefault())
        c.time = currentDate
        c.add(Calendar.MONTH, 1)
        c.set(Calendar.HOUR_OF_DAY, 0)
        c.set(Calendar.MINUTE, 0)
        c.set(Calendar.SECOND, 0)

        val calendar2 = Calendar.getInstance(Locale.getDefault())
        calendar2.time = currentDate
        calendar2.add(Calendar.DAY_OF_MONTH, 1)
        calendar2.set(Calendar.HOUR_OF_DAY, 0)
        calendar2.set(Calendar.MINUTE, 0)
        calendar2.set(Calendar.SECOND, 0)

        // convert calendar to date
        currentDatePlusSevenDays = c.time
        currentDatePlusOneDays = calendar2.time
        Log.d("dinamis", currentDatePlusOneDays.toString())*/
        //sample compare date
        /*val string = "November 29, 2019 00:05:00"
        val format = SimpleDateFormat("MMMM d, yyyy HH:mm:ss", Locale.ENGLISH)
        val mdate = format.parse(string)*/

    }

    @SuppressLint("SimpleDateFormat")
    private fun checkSyncDataByInternetConnection() {
        /* check sync, if internet connection is available*/
        if (laststatusIsConnected) {
            Log.d("isConnect", "internet available")
            /* apabila user pertama kali install apk */
            /* cek jika user pertama kali install apk( == session data sync kosong) */
            resultStringOfChecksForEachCondition(getString(R.string.hint_check_sync_master_data))
            if (synConfig.checkFirstSync) {
                Log.d("synConfig1", "first run sync")
                try {
                    resultStringOfChecksForEachCondition(getString(R.string.hint_run_sync_master_data))
                    synConfig.setDataSync()
                    idicatorLoadingSplash.visibility = View.VISIBLE
                    getPresenter()?.getdatakecamatan(limitData, kecamatanoffsetStart)
                    //showDialogSync()
                    Log.d("synConfig1", "check if first sync")
                } catch (e: IOException) {
                    Log.d("synConfig1", "Gagal sinkron ${e.localizedMessage}")
                } finally {
                    /*msyncData.appSyncStatus = 1
                    msyncData.appSyncDate = currentDatePlusSevenDays
                    Preference.saveSyncDataSplashScreen(msyncData)*/
                    Log.d("synConfig1", "save tgl sync")
                }
            } else {
                Log.d("synConfig2", "not run the first time sync")
                resultStringOfChecksForEachCondition(getString(R.string.hint_check_sync_master_data))
                if (synConfig.checkFirstSyncWithCurrentDate) {
                    Log.d("synConfig2", "re-sync")
                    try {
                        //showDialogSync()
                        resultStringOfChecksForEachCondition(getString(R.string.hint_run_re_sync_master_data))
                        Log.d("synConfig2", "Success sinkron ulang")
                    } catch (e: IOException) {
                        Log.d("synConfig2", "Gagal sinkron ulaang ${e.localizedMessage}")
                    } finally {
                        /*msyncData.appSyncStatus = 1
                        msyncData.appSyncDate = currentDatePlusSevenDays
                        Preference.saveSyncDataSplashScreen(msyncData)*/

                        if (!isCancelled) {
                            if (Preference.sessionAuth() != null) {
                                val statusLogin = Preference.sessionAuth()!!.userStatusLogin
                                if (statusLogin) {
                                    openMainActivity()
                                } else {
                                    openLoginActivity()
                                }
                            } else {
                                openLoginActivity()
                            }
                        }
                        Log.d("synConfig2", "save tgl sync")
                    }
                } else {
                    Log.d("synConfig2", "not re-sync")
                    if (synConfig.checkDinamisSyncWithCurrentDate) {
                        try {
                            resultStringOfChecksForEachCondition(getString(R.string.hint_run_sync_dinamis_data))
                            synConfig.setDinamisDataSync()
                            retrieveDataReferenceFromServer()
                            //getPresenter()?.getdatakecamatan(limitData, kecamatanoffsetStart)
                        } catch (e: IOException) {
                            Log.d("dataDinamis", "Gagal mengambil data dinamis")
                        } finally {
                            Log.d("dataDinamis", "Success mengambil data dinamis")
                        }
                    } else {
                        resultStringOfChecksForEachCondition(getString(R.string.hint_no_schedule_sync_master_data))
                        try {
                            Handler().postDelayed({
                                //setelah loading maka akan langsung berpindah ke home activity
                                if (!isCancelled) {
                                    if (Preference.sessionAuth() != null) {
                                        val statusLogin = Preference.sessionAuth()!!.userStatusLogin
                                        if (statusLogin) {
                                            openMainActivity()
                                        } else {
                                            openLoginActivity()
                                        }
                                    } else {
                                        openLoginActivity()
                                    }
                                }
                            }, 5000L)
                            //getPresenter()?.getdatakecamatan(limitData, kecamatanoffsetStart)
                        } catch (e: IOException) {
                            Log.d("dinamis", "gagal berpindah halaman")
                        } finally {
                            Log.d("dinamis", "success berpindah halaman")
                        }
                    }
                }
            }

            /*if (Preference.getsaveSyncDataSplashScreen() == null) {
                try {
                    //retrieveDataDinamisFromServer()
                    showDialogSync()
                    Log.d("getData1", "Success mengambil data kecamatan")
                } catch (e: IOException) {
                    Log.d("getData1", "Gagal mengambil data kecamatan")
                } finally {
                    *//*msyncData.appSyncStatus = 1
                    msyncData.appSyncDate = currentDatePlusSevenDays
                    Preference.saveSyncDataSplashScreen(msyncData)*//*
                    Log.d("getData1", "save tgl sync")
                }
            } else {
                *//* jika tgl skrng melebihi tgl awal singkron*//*
                val currentSessionSave = Preference.getsaveSyncDataSplashScreen()
                val dateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
                if (Date().after(currentSessionSave!!.appSyncDate)) {
                    Log.d("haha", "Tanggal sekarang melebihi tanggal tersimpan")
                    *//*Log.d("haha2", dateFormat.format(currentSessionSave.appSyncDate))
                    Log.d("haha3", dateFormat.format(Date()))*//*
                    try {
                        //retrieveDataDinamisFromServer()
                        showDialogSync()
                        Log.d("getData", "Success mengambil data kecamatan")
                    } catch (e: IOException) {
                        Log.d("getData", "Gagal mengambil data kecamatan")
                    } finally {
                        *//*msyncData.appSyncStatus = 1
                        msyncData.appSyncDate = currentDatePlusSevenDays
                        Preference.saveSyncDataSplashScreen(msyncData)*//*
                        Log.d("getData", "save tgl sync")
                    }
                } else {
                    Log.d("haha", "Tanggal sekarang tdk melebihi tanggal tersimpan")
                    Log.d("haha2", dateFormat.format(currentSessionSave.appSyncDate))
                    Log.d("haha3", dateFormat.format(Date()))

                    val currentdataDinamisDate = Preference.getsaveSyncDataDinamisSplashScreen()
                    if (Date().after(currentdataDinamisDate!!.appDinamisSyncDate)) {
                        Log.d("dinamis", "Tanggal sekarang melebihi tanggal tersimpan")
                        Log.d("dinamis", dateFormat.format(currentdataDinamisDate.appDinamisSyncDate))

                        try {
                            retrieveDataReferenceFromServer()
                            //getPresenter()?.getdatakecamatan(limitData, kecamatanoffsetStart)
                        } catch (e: IOException) {
                            Log.d("dataDinamis", "Gagal mengambil data dinamis")
                        } finally {
                            Log.d("dataDinamis", "Success mengambil data dinamis")
                        }
                    } else {
                        try {
                            Handler().postDelayed({
                                //setelah loading maka akan langsung berpindah ke home activity
                                if (!isCancelled) {
                                    if (Utils.getislogin(this@SplashActivity)) {
                                        openMainActivity()
                                    } else {
                                        openLoginActivity()
                                    }
                                    if (Preference.sessionAuth() != null) {
                                        val statusLogin = Preference.sessionAuth()!!.userStatusLogin
                                        if (statusLogin) {
                                            openMainActivity()
                                        } else {
                                            openLoginActivity()
                                        }

                                    } else {
                                        openLoginActivity()
                                    }

                                }
                            }, 8000)
                            //getPresenter()?.getdatakecamatan(limitData, kecamatanoffsetStart)
                        } catch (e: IOException) {
                            Log.d("dinamis", "gagal berpindah halaman")
                        } finally {
                            Log.d("dinamis", "success berpindah halaman")
                        }
                    }
                    *//*try {
                        retrieveDataReferenceFromServer()
                        //getPresenter()?.getdatakecamatan(limitData, kecamatanoffsetStart)
                    } catch (e: IOException) {
                        Log.d("getData", "Gagal mengambil data dinamis")
                    } finally {
                        Log.d("getData", "Success mengambil data dinamis")
                    }*//*
                }
            }*/

        } else {
            Log.d("isConnect", "no internet")
            /* versi 2 */
            resultStringOfChecksForEachCondition(getString(R.string.hint_check_sync_master_data))
            if (synConfig.checkFirstSync) {
                Log.d("synConfig1", "first run sync")
                try {
                    //showDialogSync()
                    resultStringOfChecksForEachCondition(getString(R.string.hint_run_sync_master_data))
                    synConfig.setDataSync()
                    idicatorLoadingSplash.visibility = View.VISIBLE
                    getPresenter()?.getdatakecamatan(limitData, kecamatanoffsetStart)
                    Log.d("synConfig1", "Success pertama sinkron")
                } catch (e: IOException) {
                    Log.d("synConfig1", "Gagal sinkron ${e.localizedMessage}")
                } finally {
                    /*msyncData.appSyncStatus = 1
                    msyncData.appSyncDate = currentDatePlusSevenDays
                    Preference.saveSyncDataSplashScreen(msyncData)*/
                    Log.d("synConfig1", "save tgl sync")
                }
            } else {
                Log.d("synConfig2", "not run the first time sync")
                resultStringOfChecksForEachCondition(getString(R.string.hint_check_sync_master_data))
                if (synConfig.checkFirstSyncWithCurrentDate) {
                    Log.d("synConfig2", "re-sync")
                    try {
                        //showDialogSync()
                        resultStringOfChecksForEachCondition(getString(R.string.hint_run_re_sync_master_data))
                        synConfig.setDataSync()
                        idicatorLoadingSplash.visibility = View.VISIBLE
                        getPresenter()?.getdatakecamatan(limitData, kecamatanoffsetStart)
                        Log.d("synConfig2", "Success sinkron ulang")
                    } catch (e: IOException) {
                        Log.d("synConfig2", "Gagal sinkron ulaang ${e.localizedMessage}")
                    } finally {
                        Log.d("synConfig2", "save tgl sync")
                    }
                } else {
                    Log.d("synConfig2", "not re-sync")
                    try {
                        Handler().postDelayed({
                            //setelah loading maka akan langsung berpindah ke home activity
                            if (!isCancelled) {
                                if (Preference.sessionAuth() != null) {
                                    val statusLogin = Preference.sessionAuth()!!.userStatusLogin
                                    if (statusLogin) {
                                        openMainActivity()
                                    } else {
                                        openLoginActivity()
                                    }

                                } else {
                                    openLoginActivity()
                                }

                            }
                        }, 8000)
                        //getPresenter()?.getdatakecamatan(limitData, kecamatanoffsetStart)
                    } catch (e: IOException) {
                        Log.d("dinamis", "gagal berpindah halaman")
                    } finally {
                        Log.d("dinamis", "success berpindah halaman")
                    }

                }
            }
        }
    }

    private fun retrieveDataDinamisFromServer() {
        getPresenter()?.getMultipleRequestDataLand()
        getPresenter()?.getMultipleRequestDataBangunan()
        getPresenter()?.getMultipleRequestDataSubjekSaranaPelengkap()
        getPresenter()?.getMultipleRequestDataReferenceDinasmis("1", "2")
        //getPresenter()?.requestPagingCity()
    }

    private fun retrieveDataReferenceFromServer() {
        getPresenter()?.getMultipleRequestDataReferenceDinasmis("1", "2")
    }


    @SuppressLint("PrivateResource")
    override fun onPause() {
        super.onPause()
        misshowDialog = false
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)
        SIOJT2.instance?.removeInternetConnectionListener()
    }

    private fun StartAnimations() {
        /*var anim = AnimationUtils.loadAnimation(this, R.anim.up)
        anim.reset()
        val l = findViewById<View>(R.id.lin_lay) as LinearLayout
        l.clearAnimation()
        l.startAnimation(anim)

        anim = AnimationUtils.loadAnimation(this, R.anim.upup)
        anim.reset()
        val iv = findViewById<View>(R.id.logo) as ImageView
        iv.clearAnimation()
        iv.startAnimation(anim)*/
    }


    private fun getPresenter(): SplashPresenter? {
        splashPresenter = SplashPresenter()
        splashPresenter!!.onAttach(this)
        return splashPresenter
    }

    override fun onsuccessgetDataKecamatan(responseDataKecamatan: MutableList<ResponseDataKecamatanWithoutParameter>, totalData: Int) {
        kecamatantotalItemCount = totalData
        kecamatancurrentOffset += limitData
        Log.d("total", totalData.toString())
        Log.d("total_camat", kecamatancurrentOffset.toString())

        realm.executeTransactionAsync({ inrealm ->
            inrealm.copyToRealmOrUpdate(responseDataKecamatan)
        }, {
            Log.d(TAG, "On Success: Data Written Successfully!")
            if (kecamatancurrentOffset <= kecamatantotalItemCount) {
                getPresenter()?.getdatakecamatan(limitData, kecamatancurrentOffset)
            } else {
                //execute other functions
                Log.d(TAG, "hallo, i run function kelurahan")
                getPresenter()?.getdatakelurahan(kllimitData, kloffsetStart)
            }
        }, {
            Log.d(TAG, "On Error: Error in saving Data Kecamatan!")
        })

        /*try {
            for (datapartOfKecamatan in responseDataKecamatan) {
                realm.executeTransaction { inrealm ->
                    inrealm.copyToRealmOrUpdate(datapartOfKecamatan)
                }
            }
        } catch (e: IOException) {
            Toast.makeText(this, "Gagal menambahkan data kecmatan ", Toast.LENGTH_SHORT).show()
        } finally {
            Log.d("success", "success add data kecamatan $kecamatancurrentOffset")
            if (kecamatancurrentOffset <= kecamatantotalItemCount) {
                getPresenter()?.getdatakecamatan(limitData, kecamatancurrentOffset)
            } else {
                //execute other functions
                Log.d(TAG, "hallo, i run function kelurahan")
                getPresenter()?.getdatakelurahan(kllimitData, kloffsetStart)
            }
        }*/

    }


    override fun onsuccessgetDataKelurahan(responseDataKelurahan: MutableList<ResponseDataKelurahanWithoutParameter>, totalData: Int) {
        kltotalItemCount = totalData
        klcurrentOffset += kllimitData
        Log.d("total_kl", totalData.toString())
        Log.d("total_lurah", klcurrentOffset.toString())

        realm.executeTransactionAsync({ inrealm ->
            inrealm.copyToRealmOrUpdate(responseDataKelurahan)
        }, {
            Log.d(TAG, "On Success: Data Written Successfully!")
            if (klcurrentOffset <= kltotalItemCount) {
                getPresenter()?.getdatakelurahan(kllimitData, klcurrentOffset)
            } else {
                //execute other functions
                Log.d(TAG, "hallo, i run another function in onsucces kelurahan")
                try {
                    retrieveDataDinamisFromServer()
                    Log.d("getData", "Success mengambil data statis")
                } catch (e: IOException) {
                    Log.d("getData", "Gagal mengambil data statis")
                } finally {
                    retrieveDataReferenceFromServer()
                }
            }
        }, {
            Log.d(TAG, "On Error: Error in saving Data Kelurahan!")
        })
        /*try {
            for (datapartOfKelurahan in responseDataKelurahan) {
                realm.executeTransaction { inrealm ->
                    inrealm.copyToRealmOrUpdate(datapartOfKelurahan)
                }
            }
        } catch (e: IOException) {
            Toast.makeText(this, "Gagal menambahkan data kelurahan ", Toast.LENGTH_SHORT).show()
        } finally {
            Log.d("success", "success add data kelurahan $klcurrentOffset")
            if (klcurrentOffset <= kltotalItemCount) {
                getPresenter()?.getdatakelurahan(kllimitData, klcurrentOffset)
            } else {
                //execute other functions
                Log.d(TAG, "hallo, i run another function in onsucces kelurahan")
                try {
                    retrieveDataDinamisFromServer()
                    Log.d("getData", "Success mengambil data statis")
                } catch (e: IOException) {
                    Log.d("getData", "Gagal mengambil data statis")
                } finally {
                    retrieveDataReferenceFromServer()
                }
            }
        }*/
    }

    override fun onsuccessReapet(responseDataCity: MutableList<ResponseDataCityWithoutParameter>) {
        Log.d("babab", responseDataCity.size.toString())
        Toast.makeText(this, responseDataCity.size.toString(), Toast.LENGTH_LONG).show()
    }


    override fun onSuccessGetDataTanahWithOutParameter(responseDataListPenguasaanTanah: MutableList<ResponseDataListPenguasaanTanahWithOutParam>,
                                                       responseDataBentukTanah: MutableList<ResponseDataListBentukTanahWithOutParam>,
                                                       responseDataListTipografi: MutableList<ResponseDataListTipografiWithOutParam>,
                                                       responseDataListPosisiTanah: MutableList<ResponseDataListPosisiTanahWithOutParam>,
                                                       responseDataListKepemilikanAtasTanah: MutableList<ResponseDataListKepemilikanAtasTanahWithOutParam>,
                                                       responseDataListPenggunaanTanah: MutableList<ResponseDataListPenggunaanTanahWithOutParam>,
                                                       responseDataListInformasiTambahanTanah: MutableList<ResponseDataListInformasiTambahanTanahWithOutParam>,
                                                       responseDataListPeruntukanTanah: MutableList<ResponseDataListPeruntukanTanahWithOutParam>) {

        Log.d("bTanaha", responseDataBentukTanah.toString())

        /* penguasaan atas tanah */
        try {
            realm.executeTransaction { inrealm ->
                inrealm.delete(ResponseDataListPenguasaanTanahWithOutParam::class.java)
                inrealm.copyToRealmOrUpdate(responseDataListPenguasaanTanah)
            }
        } catch (e: IOException) {
            Toast.makeText(this, "Gagal menambahkan data penguasaajnatasntanah ", Toast.LENGTH_SHORT).show()
        } finally {
            Log.d("pa", realm.where(ResponseDataListPenguasaanTanahWithOutParam::class.java).findAll().size.toString())
        }


/* bentuk tanah */
        realm.executeTransaction { inrealm ->
            inrealm.delete(ResponseDataListBentukTanahWithOutParam::class.java)
            realm.copyToRealm(responseDataBentukTanah)
        }
        //Log.d("size jeni tanah", realm.where(ResponseDataListBentukTanahWithOutParam::class.java).findAll().size.toString())


/* jenis tanah */
//        realm.beginTransaction()
//        realm.delete(ResponseDataListJenisTanahWithOutParam::class.java)
//        realm.commitTransaction()
//        for (dtCt in responseDataListJenisTanah) {
//        }
        Log.d("RealmTransaction", "Success Save Realm Jenis Tanah")
        //Log.d("size bentuk tanah", realm.where(ResponseDataListJenisTanahWithOutParam::class.java).findAll().size.toString())


/* tipografi tanah */
        realm.beginTransaction()
        realm.delete(ResponseDataListTipografiWithOutParam::class.java)
        realm.commitTransaction()
        for (dtCt in responseDataListTipografi) {

            realm.beginTransaction()
            realm.copyToRealm(dtCt)
            realm.commitTransaction()
        }
        Log.d("RealmTransaction", "Success Save Realm Tipografi Tanah")
        //Log.d("size tipografi tanah", realm.where(ResponseDataListTipografiWithOutParam::class.java).findAll().size.toString())

/* posisi tanah */
        realm.beginTransaction()
        realm.delete(ResponseDataListPosisiTanahWithOutParam::class.java)
        realm.commitTransaction()
        for (dtCt in responseDataListPosisiTanah) {

            realm.beginTransaction()
            realm.copyToRealm(dtCt)
            realm.commitTransaction()
        }
        Log.d("RealmTransaction", "Success Save Realm Posisi Tanah")
        //Log.d("size posisi tanah", realm.where(ResponseDataListPosisiTanahWithOutParam::class.java).findAll().size.toString())


/* kepemilikan tanah */
        realm.beginTransaction()
        realm.delete(ResponseDataListKepemilikanAtasTanahWithOutParam::class.java)
        realm.commitTransaction()
        for (dtCt in responseDataListKepemilikanAtasTanah) {

            realm.beginTransaction()
            realm.copyToRealm(dtCt)
            realm.commitTransaction()
        }
        Log.d("RealmTransaction", "Success Save Realm Kepemilikan Tanah")
        //Log.d("size posisi tanah", realm.where(ResponseDataListKepemilikanAtasTanahWithOutParam::class.java).findAll().size.toString())


/* pengunaan tanah */
        realm.beginTransaction()
        realm.delete(ResponseDataListPenggunaanTanahWithOutParam::class.java)
        realm.commitTransaction()
        for (dtCt in responseDataListPenggunaanTanah) {

            realm.beginTransaction()
            realm.copyToRealm(dtCt)
            realm.commitTransaction()
        }
        Log.d("RealmTransaction", "Success Save Realm Penggunaan Tanah")
        //Log.d("size penggunaan tanah", realm.where(ResponseDataListPenggunaanTanahWithOutParam::class.java).findAll().size.toString())


/* info tambahan tanah */
        realm.beginTransaction()
        realm.delete(ResponseDataListInformasiTambahanTanahWithOutParam::class.java)
        realm.commitTransaction()
        for (dtCt in responseDataListInformasiTambahanTanah) {

            realm.beginTransaction()
            realm.copyToRealm(dtCt)
            realm.commitTransaction()
        }
        Log.d("RealmTransaction", "Success Save Realm Info Tanah")
        //Log.d("size Info tanah", realm.where(ResponseDataListInformasiTambahanTanahWithOutParam::class.java).findAll().size.toString())


/* peruntukan tanah */
        realm.beginTransaction()
        realm.delete(ResponseDataListPeruntukanTanahWithOutParam::class.java)
        realm.commitTransaction()
        for (dtCt in responseDataListPeruntukanTanah) {

            realm.beginTransaction()
            realm.copyToRealm(dtCt)
            realm.commitTransaction()
        }
        Log.d("RealmTransaction", "Success Save Realm Peruntukan Tanah")
        //Log.d("size Peruntukan tanah", realm.where(ResponseDataListPeruntukanTanahWithOutParam::class.java).findAll().size.toString())

    }

    override fun onSuccessGetDataBangunan(responseDataListDindingBangunan: MutableList<ResponseDataListDindingBangunanWithOutParam>,
                                          responseDataListKerangkaAtapBangunan: MutableList<ResponseDataListKerangkaAtapBangunanWithOutParam>,
                                          responseDataListPenutupAtapBangunan: MutableList<ResponseDataListPenutupAtapBangunanWithOutParam>,
                                          responseDataListLantaiBangunan: MutableList<ResponseDataListLantaiBangunanWithOutParam>,
                                          responseDataListPintuJendelaBangunan: MutableList<ResponseDataListPintuJendelaBangunanWithOutParam>,

                                          responseDataListPlafonBangunan: MutableList<ResponseDataListPlafonBangunanWithOutParam>,
                                          responseDataListPondasiBangunan: MutableList<ResponseDataListPondasiBangunanWithOutParam>,
                                          responseDataListStrukturBangunan: MutableList<ResponseDataListStrukturBangunanWithOutParam>,
                                          responseDataListTipeBangunan: MutableList<ResponseDataListTipeBangunanWithOutParam>) {

        /* data dinding bangunan*/
        try {
            realm.executeTransaction { inrealm ->
                inrealm.delete(ResponseDataListDindingBangunanWithOutParam::class.java)
                inrealm.copyToRealmOrUpdate(responseDataListDindingBangunan)
            }
        } catch (e: IOException) {
            Toast.makeText(this, "Gagal menambahkan data rumpun", Toast.LENGTH_SHORT).show()
        } finally {
            Log.d("dinding", realm.where(ResponseDataListDindingBangunanWithOutParam::class.java).findAll().size.toString())
        }

        /* kerangka atap bangunan */
        try {
            realm.executeTransaction { inrealm ->
                inrealm.delete(ResponseDataListKerangkaAtapBangunanWithOutParam::class.java)
                inrealm.copyToRealmOrUpdate(responseDataListKerangkaAtapBangunan)
            }
        } catch (e: IOException) {
            Toast.makeText(this, "Gagal menambahkan data kerangka atap bangunan", Toast.LENGTH_SHORT).show()
        } finally {
            Log.d("kerangka atap", realm.where(ResponseDataListKerangkaAtapBangunanWithOutParam::class.java).findAll().size.toString())
        }

        /* penutup atap bangunan */
        try {
            realm.executeTransaction { inrealm ->
                inrealm.delete(ResponseDataListPenutupAtapBangunanWithOutParam::class.java)
                inrealm.copyToRealmOrUpdate(responseDataListPenutupAtapBangunan)
            }
        } catch (e: IOException) {
            Toast.makeText(this, "Gagal menambahkan data penutup atap bangunan", Toast.LENGTH_SHORT).show()
        } finally {
            Log.d("penutup atap", realm.where(ResponseDataListPenutupAtapBangunanWithOutParam::class.java).findAll().size.toString())
        }

        /* lantai atap bangunan */
        try {
            realm.executeTransaction { inrealm ->
                inrealm.delete(ResponseDataListLantaiBangunanWithOutParam::class.java)
                inrealm.copyToRealmOrUpdate(responseDataListLantaiBangunan)
            }
        } catch (e: IOException) {
            Toast.makeText(this, "Gagal menambahkan data lantai bangunan", Toast.LENGTH_SHORT).show()
        } finally {
            Log.d("lantai", realm.where(ResponseDataListLantaiBangunanWithOutParam::class.java).findAll().size.toString())
        }

        /* pintu jendela bangunan */
        try {
            realm.executeTransaction { inrealm ->
                inrealm.delete(ResponseDataListPintuJendelaBangunanWithOutParam::class.java)
                inrealm.copyToRealmOrUpdate(responseDataListPintuJendelaBangunan)
            }
        } catch (e: IOException) {
            Toast.makeText(this, "Gagal menambahkan data pintu jendela bangunan", Toast.LENGTH_SHORT).show()
        } finally {
            Log.d("pj", realm.where(ResponseDataListPintuJendelaBangunanWithOutParam::class.java).findAll().size.toString())
        }


        /* data plafon bangunan*/
        try {
            realm.executeTransaction { inrealm ->
                inrealm.delete(ResponseDataListPlafonBangunanWithOutParam::class.java)
                inrealm.copyToRealmOrUpdate(responseDataListPlafonBangunan)
            }
        } catch (e: IOException) {
            Toast.makeText(this, "Gagal menambahkan data plafon bangunan", Toast.LENGTH_SHORT).show()
        } finally {
            Log.d("plafon", realm.where(ResponseDataListPlafonBangunanWithOutParam::class.java).findAll().size.toString())
        }

        /* pondasi bangunan */
        try {
            realm.executeTransaction { inrealm ->
                inrealm.delete(ResponseDataListPondasiBangunanWithOutParam::class.java)
                inrealm.copyToRealmOrUpdate(responseDataListPondasiBangunan)
            }
        } catch (e: IOException) {
            Toast.makeText(this, "Gagal menambahkan data pondasi bangunan", Toast.LENGTH_SHORT).show()
        } finally {
            Log.d("pondasi", realm.where(ResponseDataListPondasiBangunanWithOutParam::class.java).findAll().size.toString())
        }

        /* struktur bangunan */
        try {
            realm.executeTransaction { inrealm ->
                inrealm.delete(ResponseDataListStrukturBangunanWithOutParam::class.java)
                inrealm.copyToRealmOrUpdate(responseDataListStrukturBangunan)
            }
        } catch (e: IOException) {
            Toast.makeText(this, "Gagal menambahkan data struktur bangunan", Toast.LENGTH_SHORT).show()
        } finally {
            Log.d("stuktur", realm.where(ResponseDataListStrukturBangunanWithOutParam::class.java).findAll().size.toString())
        }


        /* tipe bangunan */
        try {
            realm.executeTransaction { inrealm ->
                inrealm.delete(ResponseDataListTipeBangunanWithOutParam::class.java)
                inrealm.copyToRealmOrUpdate(responseDataListTipeBangunan)
            }
        } catch (e: IOException) {
            Toast.makeText(this, "Gagal menambahkan data tipe bangunan", Toast.LENGTH_SHORT).show()
        } finally {
            Log.d("tipe", realm.where(ResponseDataListTipeBangunanWithOutParam::class.java).findAll().size.toString())
        }


    }


    override fun onSuccessgetMultipleRequestDataSubjekSaranaPelengkap(
            responseDataSumberIdentitas: MutableList<ResponseDataSumberIdentitasWithOutParam>,
            responseDataJenisSumberAir: MutableList<ResponseDataJenisSumberAirWithOutParam>,
            responseDataJenisPagarKeliling: MutableList<ResponseDataJenisPagarKelilingWithOutParam>,
            responseDataProvince: MutableList<ResponseDataProvinceWithoutParameter>,
            responseDataCity: MutableList<ResponseDataCityWithoutParameter>) {


        /* sumber identitas */
        realm.executeTransaction { inrealm ->
            inrealm.delete(ResponseDataSumberIdentitasWithOutParam::class.java)
            inrealm.copyToRealmOrUpdate(responseDataSumberIdentitas)
        }

        /* sumber air */
        realm.executeTransaction { inrealm ->
            inrealm.delete(ResponseDataJenisSumberAirWithOutParam::class.java)
            inrealm.copyToRealmOrUpdate(responseDataJenisSumberAir)
        }

        /* pagar keliling */
        realm.executeTransaction { inrealm ->
            realm.delete(ResponseDataJenisPagarKelilingWithOutParam::class.java)
            inrealm.copyToRealmOrUpdate(responseDataJenisPagarKeliling)
        }

        /* data propinsi */
        try {
            realm.executeTransaction { inrealm ->
//                inrealm.delete(ResponseDataProvinceWithoutParameter::class.java)
                inrealm.copyToRealmOrUpdate(responseDataProvince)
            }
        } catch (e: IOException) {
            Log.d("asddxx","")
            e.printStackTrace()
            Toast.makeText(this, "Gagal menambahkan data provinsi", Toast.LENGTH_SHORT).show()
        } finally {
            Log.d("provinsi", realm.where(ResponseDataProvinceWithoutParameter::class.java).findAll().size.toString())
        }

        /* data kota */

        try {
            realm.executeTransaction { inrealm ->
                inrealm.delete(ResponseDataCityWithoutParameter::class.java)
                inrealm.copyToRealmOrUpdate(responseDataCity)
            }
        } catch (e: IOException) {
            Toast.makeText(this, "Gagal menambahkan data kota", Toast.LENGTH_SHORT).show()
        } finally {
            Log.d("kota", realm.where(ResponseDataCityWithoutParameter::class.java).findAll().size.toString())
        }

    }

    @SuppressLint("SimpleDateFormat")
    override fun onSucceccgetMultupleRequestDataReferenceDinasmis(responseDataMataPencaharianWithOutParam: MutableList<ResponseDataMataPencaharianWithOutParam>,
                                                                  responseDataPekerjaan: MutableList<ResponseDataPekerjaanWithOutParam>,
                                                                  responseDataListKategoriTanaman: MutableList<ResponseDataListKategoriTanamanWithOutParam>,
                                                                  responseDataListKategoriRumpun: MutableList<ResponseDataListKategoriRumpunWithOutParam>,
                                                                  responseDataJenisFasilitas: MutableList<ResponseDataJenisFasilitasWithOutParam>) {


//      msyncDataDinamis.appDinamisSyncStatus = 1
//      msyncDataDinamis.appDinamisSyncDate = currentDatePlusOneDays
//      Preference.saveSyncDataDinamisSplashScreen(msyncDataDinamis)
        resultStringOfChecksForEachCondition(getString(R.string.hint_master_data_sync_successfull))
        /* mata pencaharian */
        realm.executeTransactionAsync({ inrealm ->
            inrealm.delete(ResponseDataMataPencaharianWithOutParam::class.java)
            inrealm.copyToRealmOrUpdate(responseDataMataPencaharianWithOutParam)
        }, {
            Log.d(TAG, "On Success: Data Written Successfully!")
        }, {
            Log.d(TAG, "On Error: Error in saving Data!")
        })

        /* pekerjaan */
        realm.executeTransaction { inrealm ->
            inrealm.delete(ResponseDataPekerjaanWithOutParam::class.java)
            inrealm.copyToRealmOrUpdate(responseDataPekerjaan)
        }

        /* tanaman */
        realm.executeTransaction { inrealm ->
            inrealm.delete(ResponseDataListKategoriTanamanWithOutParam::class.java)
            inrealm.copyToRealmOrUpdate(responseDataListKategoriTanaman)
        }

        /* rumpun */
        try {
            realm.executeTransaction { inrealm ->
                inrealm.delete(ResponseDataListKategoriRumpunWithOutParam::class.java)
                inrealm.copyToRealmOrUpdate(responseDataListKategoriRumpun)
            }
        } catch (e: IOException) {
            Toast.makeText(this, "Gagal menambahkan data rumpun", Toast.LENGTH_SHORT).show()
        } finally {
            Log.d("rumpun", realm.where(ResponseDataListKategoriRumpunWithOutParam::class.java).findAll().size.toString())
        }

        /* fasilitas */
        realm.executeTransaction { inrealm ->
            inrealm.delete(ResponseDataJenisFasilitasWithOutParam::class.java)
            inrealm.copyToRealmOrUpdate(responseDataJenisFasilitas)
        }

        // save data sinkron semua
        if (synConfig.checkFirstSync || Preference.getsaveSyncDataSplashScreen()!!.appSyncStatus == 0) {
            synConfig.saveDataSync()
            /*Handler().postDelayed({
                mCalendar = Calendar.getInstance(Locale.getDefault())
                mCalendar.time = Preference.getsaveSyncDataSplashScreen()!!.appSyncDate
                val tglSync = DateFormat.format(dateTemplate, mCalendar.time)
                showDialogSync2("Sinkronisasi ulang pada tanggal\n$tglSync")
            }, 5000)*/
            Log.d("onsuccessDinamis", "save data sync successfull")
        }

        // save data sinkron dinamis
        if (synConfig.checkSyncOnlyDinamisData || Preference.getsaveSyncDataDinamisSplashScreen()!!.appDinamisSyncStatus == 0) {
            synConfig.saveDinamisDataSync()
            Log.d("onsuccessDinamis", "save dinamis data sync successfull")
        }

        if (Preference.sessionAuth() != null) {
            Handler().postDelayed({
                val statusLogin = Preference.sessionAuth()!!.userStatusLogin
                if (statusLogin) {
                    openMainActivity()
                } else {
                    openLoginActivity()
                }
            }, 4000L)
        } else {
            Handler().postDelayed({
                openLoginActivity()
            }, 4000L)
        }
    }

    override fun openLoginActivity() {
        val intent = LoginActivity.getStartIntent(this@SplashActivity)
        startActivity(intent)
        finish()
    }

    override fun openMainActivity() {
        val intent = MainActivity.getStartIntent(this@SplashActivity)
        startActivity(intent)
        finish()
    }

    override fun onInternetUnavailable() {
        // show temporary UI
        // Toast or Snackbar
        /* comment on 2nd development */
        /*idicatorLoadingSplash.visibility = View.GONE
        llsyncKoneksiInternet.visibility = View.VISIBLE*/
    }

    private fun showDialogSync() {
        /*val dialogSyncSplashScreen = DialogSyncSplashScreen.newInstance("","")
        dialogSyncSplashScreen.show(supportFragmentManager, "dialog")*/
        val mDialogView = LayoutInflater.from(this@SplashActivity).inflate(R.layout.dialog_popup_sync, null)
        val mBuilder = AlertDialog.Builder(this@SplashActivity)
        val mDialog = mBuilder.create()

        mDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialog.setCancelable(false)
        val window = mDialog.window
        if (window != null) {
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            mDialog.window!!.setLayout(width, height)
        }
        mDialog.setView(mDialogView)

        mDialogView.btnContSync.setSafeOnClickListener {
            mDialog.dismiss()
            //onShowLoading()
            synConfig.setDataSync()
            idicatorLoadingSplash.visibility = View.VISIBLE
            getPresenter()?.getdatakecamatan(limitData, kecamatanoffsetStart)
            /*Handler().postDelayed({
                getPresenter()?.getdatakecamatan(limitData, kecamatanoffsetStart)
            }, 2000)*/

        }

        if (mDialog.isShowing) {
            mDialog.cancel()
        } else {
            mDialog.show()
        }

    }

    @SuppressLint("SetTextI18n")
    private fun showDialogSync2(tglSync: String) {
        val mDialogView = LayoutInflater.from(this@SplashActivity).inflate(R.layout.dialog_popup_sync, null)
        val mBuilder = AlertDialog.Builder(this@SplashActivity)
        val mDialog = mBuilder.create()

        mDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialog.setCancelable(false)
        val window = mDialog.window
        if (window != null) {
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            mDialog.window!!.setLayout(width, height)
        }
        mDialog.setView(mDialogView)

        mDialogView.tvTitlePopup.text = "Sinkoronisasi berhasil"
        mDialogView.tvcontentPopup.text = tglSync
        mDialogView.txtbtnContSync.text = "OK"

        mDialogView.btnContSync.setOnClickListener {
            mDialog.dismiss()
            if (Preference.sessionAuth() != null) {
                Handler().postDelayed({
                    val statusLogin = Preference.sessionAuth()!!.userStatusLogin
                    if (statusLogin) {
                        openMainActivity()
                    } else {
                        openLoginActivity()
                    }

                }, 5000)

            } else {
                Handler().postDelayed({
                    openLoginActivity()
                }, 5000)
            }
        }

        mDialog.show()
    }

    private fun showAlreter(message: String) {
        Alerter.create(this@SplashActivity)
                .setTitle(R.string.app_name)
                .setText(message)
                .setTextAppearance(R.style.AlertTextAppearance)
                .setBackgroundColorRes(R.color.colorbuttonAlret)
                .setIcon(R.drawable.ic_info)
                .setDuration(4200)
                .enableVibration(true)
                .enableSwipeToDismiss()
                .addButton("OK", R.style.AlertButton, View.OnClickListener {
                    Alerter.hide()
                })
                .show()


    }

    private fun statusConnectionInternet(isConnected: Boolean) {
        if (isConnected) {
            Log.d("connection", "Good! connected to internet")
        } else {
            Log.d("connection", "No Internet Connection")
            //showAlreter("No Internet Connection")
        }
    }

    override fun onFailed(message: String) {
        limitData = 1000
        kecamatanoffsetStart = 0
        idicatorLoadingSplash.visibility = View.GONE
        tv_splash.text = "Sinkornisasi data gagal, harap ulangi!"
        MaterialAlertDialogBuilder(this@SplashActivity, R.style.CustomAlertDialogMaterialTheme)
                .setTitle("SINKRONISASI DATA GAGAL")
                .setIcon(R.mipmap.ic_launcher)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(resources.getString(R.string.hint_try_again)) { dialog, _ ->
                    // Respond to positive button press
                    dialog.dismiss()
                    checkSyncDataByInternetConnection()
                }
                .show()
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    /*override fun onResume() {
        super.onResume()

        val intentFilter = IntentFilter()

        // Add network connectivity change action.
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        // Set broadcast receiver priority.
        intentFilter.priority = 100
        //register connection receiver
        registerReceiver(receiverNetwork, intentFilter)

        // register connection status listener
        SIOJT2.instance?.setConnectivityListener(this)
    }*/


    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        unregisterReceiver(receiverNetwork)
        Log.d("isConnectedStatus", isConnected.toString())
        this.laststatusIsConnected = isConnected
        statusConnectionInternet(isConnected)
        checkSyncDataByInternetConnection()
    }

    override fun resultStringOfChecksForEachCondition(resultString: String?) {
        tv_splash.text = resultString
    }

    override fun onDestroy() {
        splashPresenter?.onDetach()
        super.onDestroy()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        isCancelled = true
        timer?.cancel()
        finish()
    }

    companion object {
        private const val TAG = "SplashActivity"
        private const val SPLASH_TIME = 2500

        private var limitData = 1000
        private var kecamatantotalItemCount = 0
        private var kecamatanoffsetStart = 0
        private var kecamatancurrentOffset = kecamatanoffsetStart

        private var kllimitData = 5000
        private var kltotalItemCount = 0
        private var kloffsetStart = 0
        private var klcurrentOffset = kloffsetStart


    }
}
