package mki.siojt2.ui.splash.view

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import mki.siojt2.R
import androidx.appcompat.app.AlertDialog
import android.view.*
import android.widget.Toast
import android.view.WindowManager
import kotlinx.android.synthetic.main.dialog_popup_sync.view.*

import mki.siojt2.utils.extension.SafeClickListener

class DialogSyncSplashScreen : androidx.fragment.app.DialogFragment() {

    lateinit var mView: View

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"
        internal const val TAG = "DialogSyncSplashScreen"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): DialogSyncSplashScreen {
            val fragment = DialogSyncSplashScreen()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val dialog = super.onCreateDialog(savedInstanceState)

        //val dialogBuilder = AlertDialog.Builder(ContextThemeWrapper(this, R.style.full_screen_dialog))
        val dialogBuilder = AlertDialog.Builder(activity!!)
        val inflater = LayoutInflater.from(context)
        val dialogView = inflater.inflate(R.layout.dialog_popup_sync, null)
        dialogBuilder.setView(dialogView)

        initSubViews(dialogView)

        // request a window without the title
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        return dialogBuilder.create()
    }


    private fun initSubViews(rootView: View) {

        val btngotoMainMenu = rootView.btnContSync

        btngotoMainMenu.setSafeOnClickListener {
            dismiss()
        }
    }

    fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    private fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
        val safeClickListener = SafeClickListener {
            onSafeClick(it)
        }
        setOnClickListener(safeClickListener)
    }


    override fun onStart() {
        super.onStart()

        //set transparent background
        val window = dialog!!.window

        //window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        if (window != null) {
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            window.setBackgroundDrawableResource(android.R.color.transparent)
            dialog!!.window!!.setLayout(width, height)
        }
        //disable buttons from dialog
        val alertDialog = dialog as AlertDialog
        alertDialog.getButton(Dialog.BUTTON_POSITIVE).isEnabled = false
        alertDialog.getButton(Dialog.BUTTON_NEGATIVE).isEnabled = false
        alertDialog.getButton(Dialog.BUTTON_NEUTRAL).isEnabled = false
    }

    override fun onPause() {
        super.onPause()
        this.dismiss()
    }


    /* override fun onResume() {
         super.onResume()
         val window = dialog.window
         val layoutParams = dialog.window!!.attributes
         layoutParams.dimAmount = 0f
         window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
         window.setGravity(Gravity.CENTER)
         dialog.window!!.attributes = layoutParams
     }*/

}