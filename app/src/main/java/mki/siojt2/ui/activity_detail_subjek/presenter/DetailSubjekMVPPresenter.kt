package mki.siojt2.ui.activity_detail_subjek.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface DetailSubjekMVPPresenter : MVPPresenter {

    fun getdetailSubjek(partyId: Int, accessToken: String)

}