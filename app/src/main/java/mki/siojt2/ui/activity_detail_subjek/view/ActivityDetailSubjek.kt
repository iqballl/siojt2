package mki.siojt2.ui.activity_detail_subjek.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import com.google.gson.Gson
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_detail_subjek.*
import kotlinx.android.synthetic.main.dialog_confirm_delete.view.*
import kotlinx.android.synthetic.main.toolbar_main.*
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.fragment.detail_jenis_objek.tanah.adapter_recycleview.AdapterFotoPropertiTanah
import mki.siojt2.model.ResponseDataDetailPemilik
import mki.siojt2.model.local_save_image.DataImageLand
import mki.siojt2.model.localsave.PartyAdd
import mki.siojt2.model.localsave.PartyAddImage
import mki.siojt2.model.localsave.Session
import mki.siojt2.ui.activity_detail_subjek.presenter.DetailSubjekPresenter
import mki.siojt2.ui.activity_form_subjek.view.ActivityFormSubjek
import mki.siojt2.utils.realm.RealmController
import kotlin.collections.ArrayList


class ActivityDetailSubjek : BaseActivity(), DetailSubjekView {

    private lateinit var presenter: DetailSubjekPresenter
    private lateinit var realm: Realm
    //private var historyData = ""
    val mDataset: MutableList<PartyAdd>? = ArrayList()
    var results: PartyAdd? = null
    private lateinit var session: Session
    private lateinit var mdialogConfirmDelete: AlertDialog
    private lateinit var mDialogView: View

    private var mTempId: Int? = null

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, ActivityDetailSubjek::class.java)
        }
    }

    @SuppressLint("InflateParams")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_subjek)

        val historyData = intent.getStringExtra("data_current_pemilik")
        mTempId = historyData.toInt()
        session = Session(this)
        Log.e("id", historyData)
        //val historyData =intent?.extras?.getString("data_current_pemilik")

        //val turnsType = object : TypeToken<ResponseDataListPemilik>() {}.type
        //val dataconvert = Gson().fromJson(historyData, PartyAdd::class.java)

        //Log.d("bancet1", historyData.toString())
        //Log.d("bancet3", dataconvert.partyProvinceName)

        /* set dialog confirm delete data */
        mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_confirm_delete, null)
        val mBuilder = AlertDialog.Builder(this)
        mdialogConfirmDelete = mBuilder.create()

        mdialogConfirmDelete.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        mdialogConfirmDelete.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mdialogConfirmDelete.setCancelable(false)
        val window = mdialogConfirmDelete.window
        if (window != null) {
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            mdialogConfirmDelete.window!!.setLayout(width, height)
        }
        mdialogConfirmDelete.setView(mDialogView)

        /* set toolbar */
        if (toolbarCustom != null) {
            setSupportActionBar(toolbarCustom)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            //supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        toolbarCustom.setNavigationOnClickListener {
            finish()
        }

        //tvToolbarTitle.text = historyData[0]
        tvToolbarTitle.setTextColor(Color.parseColor("#3DC528"))

        btnshowDaftarObjek.setOnClickListener {
            finish()
        }

        /* retrieve data detail pemilik  */
        getDetailSubjek(historyData)
        //getPresenter()?.getdetailSubjek(dataconvert.partyId!!, Preference.accessToken)

        /* update data */
        cardtitle2.setSafeOnClickListener {
            val param = "['${results?.projectId}','${results?.areaId}',2,${results?.tempId}]"
            val intent = ActivityFormSubjek.getStartIntent(this)
            showToast(results?.tempId.toString())
            intent.putExtra("data_current_project2", param)
            session.setIdAndStatus(results?.tempId.toString(), "2", "1")
            startActivity(intent)
        }

        /* delete data */
        cardtitle3.setSafeOnClickListener {
            mDialogView.btnYesDelete.setOnClickListener {
                realm.executeTransactionAsync({ inRealm ->
                    val results = inRealm.where(PartyAdd::class.java).findAll()
                    val dataParty = results.where().equalTo("TempId", historyData.toInt()).findFirst()
                    dataParty?.deleteFromRealm()
                }, {
                    Log.d("delete", "onSuccess : delete single object")
                    mdialogConfirmDelete.dismiss()
                    finish()
                }, {
                    Log.d("delete", "onFailed : ${it.localizedMessage}")
                    Log.d("delete", "onFailed : delete single object")
                })
            }

            mDialogView.btnNoDelete.setOnClickListener {
                mdialogConfirmDelete.dismiss()
            }
            mdialogConfirmDelete.show()

            /*val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {


                    }

                    DialogInterface.BUTTON_NEGATIVE -> {
                        dialog.dismiss()
                    }
                }
            }

            val builder = android.support.v7.app.AlertDialog.Builder(this)
            builder.setTitle("SIOJT")
                    .setMessage("Yakin akan dihapus?")
                    .setPositiveButton("Ya", dialogClickListener)
                    .setNegativeButton("Tidak", dialogClickListener)
                    .setIcon(ContextCompat.getDrawable(this@ActivityDetailSubjek, R.drawable.ic_logo_splash)!!)
                    .show()*/
        }

    }

    private fun getDetailSubjek(historyData: String) {
        Log.d("data", historyData)
        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        results = realm.where(PartyAdd::class.java).equalTo("TempId", historyData.toInt()).findFirst()
        if (results != null) {
            tampilkanData(results!!)
            Log.d("results", results.toString())
        } else {
            Log.e("data", "kosong")
        }
    }

    @SuppressLint("SetTextI18n")
    private fun tampilkanData(results: PartyAdd) {
        tvToolbarTitle.text = results.partyFullName ?: "-"
//        results.partyOccupationName.toString().isNotEmpty()
        tvNIK.text = if(!results.partyIdentityNumber.isNullOrEmpty()) {results.partyIdentityNumber.toString()} else "-"
        tvName.text = if(!results.partyFullName.isNullOrEmpty()) {results.partyFullName.toString()} else "-"
        tvPhone.text = if(!results.partyPhone.isNullOrEmpty()) {results.partyPhone.toString()} else "-"
        tvEmail.text = if(!results.partyEmail.isNullOrEmpty()) {results.partyEmail.toString()} else "-"
        tvBirthplace.text = if(!results.partyBirthPlaceName.isNullOrEmpty()) {results.partyBirthPlaceName.toString()} else "-"
        tvBirthdate.text = if(!results.partyBirthDate.isNullOrEmpty()) {results.partyBirthDate.toString()} else "-"
        tvJob.text = if(!results.partyOccupationName.isNullOrEmpty()) {results.partyOccupationName.toString()} else "-"
        tvProvince.text = if(!results.partyProvinceName.isNullOrEmpty()) {results.partyProvinceName.toString()} else "-"
        tvCity.text = if(!results.partyRegencyName.isNullOrEmpty()) {results.partyRegencyName.toString()} else "-"
        tvDistrict.text = if(!results.partyDistrictName.isNullOrEmpty()) {results.partyDistrictName.toString()} else "-"
        tvVillage.text = if(!results.partyVillageName.isNullOrEmpty()) {results.partyVillageName.toString()} else "-"
        tvBlock.text = if(!results.partyBlock.isNullOrEmpty()) {results.partyBlock.toString()} else "-"
        tvKomplek.text = if(!results.partyComplexName.isNullOrEmpty()) {results.partyComplexName.toString()} else "-"
        tvNo.text = if(!results.partyNumber.isNullOrEmpty()) {results.partyNumber.toString()} else "-"
        tvStreet.text = if(!results.partyStreetName.isNullOrEmpty()) {results.partyStreetName.toString()} else "-"
        tvRT.text = if(!results.partyRT.isNullOrEmpty()) {results.partyRT.toString()} else "-"
        tvRW.text = if(!results.partyRW.isNullOrEmpty()) {results.partyRW.toString()} else "-"
        tvPostalCode.text = if(!results.partyPostalCode.isNullOrEmpty()) {results.partyPostalCode.toString()} else "-"
        tvNPWP.text = if(!results.partyNPWP.isNullOrEmpty()) {results.partyNPWP.toString()} else "-"
        tvPemilik.text = if(!results.partyOwnershipType.isNullOrEmpty()) {results.partyOwnershipType.toString().replace("[","").replace("]","")} else "-"
        tvDomisili.text = if(results.partyYearStayBegin != null) {results.partyYearStayBegin.toString()} else "-"
        tvOwnership.text = if(!results.partyOwnershipLandData.isNullOrEmpty()) {results.partyOwnershipLandData.toString()} else "-"
        tvNote.text = if(!results.partyNotes.isNullOrEmpty()) {results.partyNotes.toString()} else "-"

        val adapterFotoPropertiTanah = AdapterFotoPropertiTanah(this)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL, false)

        rlfoto.adapter = adapterFotoPropertiTanah
        rlfoto.layoutManager = linearLayoutManager


        val mdatafoto: MutableList<DataImageLand> = ArrayList()
        for(i in results.partyAddImage){
            val x = DataImageLand()
            x.imagepathLand =  i.imagepathLand
            x.imagenameLand = i.imagenameLand
            mdatafoto.add(x)
        }
        adapterFotoPropertiTanah.clear()
        adapterFotoPropertiTanah.addItems(mdatafoto)

        //convertListtoJSON
//        val liveId= results.livelihoodLiveId
//        val liveId2 = liveId.replace("[", "['")
//        val liveId3 = liveId2.replace("]", "']")
//        val liveId4 = liveId3.replace(",", "','")
//
//        val liveName = results.livelihoodLiveName
//        val replace1 = liveName.replace("[", "['")
//        val replace2 = replace1.replace("]", "']")
//        val replace3 = replace2.replace(", ", "','")
//
//        val liveNameOther = results.livelihoodOther
//        val liveNameOther2 = liveNameOther.replace("[", "['")
//        val liveNameOther3 = liveNameOther2.replace("]", "']")
//        val liveNameOther4 = liveNameOther3.replace(", ", "','")
//
//        val liveIncome = results.livelihoodIncome
//        val replac1 = liveIncome.replace("[", "['")
//        val replac2 = replac1.replace("]", "']")
//        val replac3 = replac2.replace(", ", "','")
//
//        val gson = Gson()
//        val lisliveId = gson.fromJson(liveId4, Array<String>::class.java).toList()
//        val listNameAr = gson.fromJson(replace3, Array<String>::class.java).toList()
//        val listNameArOther = gson.fromJson(liveNameOther4, Array<String>::class.java).toList()
//        val listIncomeAr = gson.fromJson(replac3, Array<String>::class.java).toList()
//        Log.d("hasil", listNameAr.size.toString())
//        Log.d("hasil2", listNameArOther.size.toString())
//        Log.d("hasil", listNameAr.toString())
//        Log.d("hasil2", listNameArOther.toString())
//        Log.d("hasil2", listIncomeAr.toString())
//        if (lisliveId.isNotEmpty()) {
//            for (i in lisliveId.indices) {
//                val child = layoutInflater.inflate(R.layout.item_detail_jenis_usaha_vertical, null)
//                val jenisUsaha = child.findViewById(R.id.tvjenisUsaha) as com.pixplicity.fontview.FontTextView
//                val penghasilanUsaha = child.findViewById(R.id.tvPenghasilan) as com.pixplicity.fontview.FontTextView
//
//                if (listNameArOther[i].isEmpty() || listNameArOther[i] == "") {
//                    jenisUsaha.text = listNameAr[i]
//                    penghasilanUsaha.text = "Rp ${listIncomeAr[i]}"
//                } else {
//                    jenisUsaha.text = listNameArOther[i]
//                    penghasilanUsaha.text = "Rp ${listIncomeAr[i]}"
//                }
//
//                item.addView(child)
//            }
//        }


        //val inte = results.livelihoodLiveId.get(0);
        //Log.e("inte",inte.toString())
//        val array = arrayOfNulls<String>(inte.length)
//        inte.toArray(array)
//        val jsonArray = JSONArray(results.livelihoodLiveName)
//        val strArr = arrayOfNulls<String>(jsonArray.length())
//        Log.e("strr",jsonArray.toString())
//
//        for (i in 0 until jsonArray.length()) {
//            strArr[i] = jsonArray.getString(i)
//            Log.e("strr2",strArr[i])
//        }

        //System.out.println(Arrays.toString(strArr))

//        for (i in 0 until results.livelihoodLiveId.length) {
//            Log.e("livelihood",results.livelihoodLiveId[i].toString())
//        }


        //
        //for (i in 0 until jsonArray.length()) {
////            responseDataDetailPemilik.livelihoodLists!!.forEach {
        // val child = layoutInflater.inflate(R.layout.item_detail_jenis_usaha_vertical, null)
        //val jenisUsaha = child.findViewById(R.id.tvjenisUsaha) as com.pixplicity.fontview.FontTextView
//            val penghasilanUsaha = child.findViewById(R.id.tvPenghasilan) as com.pixplicity.fontview.FontTextView
        //jenisUsaha.text = strArr[i]
//            penghasilanUsaha.text = "Rp ${results.livelihoodIncome}"
        //item.addView(child)
//
//            //Log.d("Hasil", it?.livelihoodName.toString())
//        }

    }

    private fun <T> ArrayList(asList: List<List<T>>): Any {
        return asList
    }

    private fun getPresenter(): DetailSubjekPresenter? {
        presenter = DetailSubjekPresenter()
        presenter.onAttach(this)
        return presenter
    }

    @SuppressLint("SetTextI18n", "InflateParams")
    override fun onsuccessgetdetailSubjek(responseDataDetailPemilik: ResponseDataDetailPemilik) {
//        tvPekerjaan.text = responseDataDetailPemilik.partyDetail?.occupationName ?: "-"
//        tvAlamat.text = "${responseDataDetailPemilik.partyDetail?.partyComplexName ?: "-"} " +
//                "${responseDataDetailPemilik.partyDetail?.partyStreetName} " +
//                "${responseDataDetailPemilik.partyDetail?.partyBlock ?: "-"} " +
//                "No.${responseDataDetailPemilik.partyDetail?.partyNumber} " +
//                "${responseDataDetailPemilik.partyDetail?.districtName} " +
//                "${responseDataDetailPemilik.partyDetail?.regencyName}, " +
//                "${responseDataDetailPemilik.partyDetail?.provinceName} " +
//                "${responseDataDetailPemilik.partyDetail?.partyPostalCode}"
//
//        tvisiJenisIdentitas.text = "${responseDataDetailPemilik.partyDetail?.identitySourceName}"
//        tvisinomorIdentitas.text = "${responseDataDetailPemilik.partyDetail?.partyIdentityNumber}"
//        tvNPWP.text = "${responseDataDetailPemilik.partyDetail?.partyNPWP}"
//        tvLamaTinggal.text = "${responseDataDetailPemilik.partyDetail?.partyYearStayBegin
//                ?: "-"} Tahun"
//
//        val item = findViewById<LinearLayout>(R.id.llitemfac2)
//        item.removeAllViews()
//        responseDataDetailPemilik.livelihoodLists!!.forEach {
//            val child = layoutInflater.inflate(R.layout.item_detail_jenis_usaha_vertical, null)
//            val jenisUsaha = child.findViewById(R.id.tvjenisUsaha) as com.pixplicity.fontview.FontTextView
//            val penghasilanUsaha = child.findViewById(R.id.tvPenghasilan) as com.pixplicity.fontview.FontTextView
//            jenisUsaha.text = it?.livelihoodName ?: "-"
//            penghasilanUsaha.text = "Rp ${it?.partyLivelihoodIncome ?: "-"}"
//            item.addView(child)
//
//            //Log.d("Hasil", it?.livelihoodName.toString())
//        }

    }

    override fun onRestart() {
        super.onRestart()
        /* retrieve data detail pemilik  */
        getDetailSubjek(mTempId.toString())
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    override fun onDestroy() {
        super.onDestroy()
        getPresenter()?.onDetach()
    }
}



