package mki.siojt2.ui.activity_detail_subjek.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.ResponseDataDetailPemilik

/**
 * Created by iqbal on 09/09/19
 */

interface DetailSubjekView : MvpView {
    fun onsuccessgetdetailSubjek(responseDataDetailPemilik: ResponseDataDetailPemilik)
}
