package mki.siojt2.ui.activity_detail_subjek.presenter

import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.ui.activity_detail_subjek.view.DetailSubjekView
import mki.siojt2.utils.ErrorHandler
import mki.siojt2.utils.rxJava.RxUtils

class DetailSubjekPresenter : BasePresenter(), DetailSubjekMVPPresenter {

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun getdetailSubjek(partyId: Int, accessToken: String) {
        view().onShowLoading()
        compositeDisposable.add(
                dataManager.getdetailParties(partyId, accessToken)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdetailSubjek(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    private fun view(): DetailSubjekView{
        return getView() as DetailSubjekView
    }
}