package mki.siojt2.ui.main.adapter_reycleview

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.realm.RealmResults
import kotlinx.android.synthetic.main.item_daftar_dppt_vertical.view.*
import kotlinx.android.synthetic.main.item_empty_view.view.*
import mki.siojt2.R
import mki.siojt2.base.BaseViewHolder
import mki.siojt2.model.ResponseDataListProjectLocal
import mki.siojt2.ui.acitivity_map_project.MapsProjectActivity
import mki.siojt2.ui.activity_daftar_kepemilikan_tanah_pertama.view.ActivityDaftarKepemilikanTanahPertama
import mki.siojt2.ui.main.view.MainActivity
import mki.siojt2.utils.MapsProjectIntentSetting
import mki.siojt2.utils.extension.MapBoxUtils
import mki.siojt2.utils.extension.SafeClickListener


class PengajuanDPPTAdapter(context: Context, var myActivity: MainActivity) : RecyclerView.Adapter<BaseViewHolder>() {

    val mDataset: MutableList<ResponseDataListProjectLocal>? = ArrayList()
    var rowLayout = R.layout.item_daftar_dppt_vertical
    var emptyrowLayout = R.layout.item_empty_view

    private val mContext: Context? = context
    private var mCallback: Callback? = null

    fun setCallback(callback: Callback) {
        mCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            VIEW_TYPE_NORMAL -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(rowLayout, parent, false))
            else -> EmptyViewHolder(LayoutInflater.from(parent.context).inflate(emptyrowLayout, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (mDataset != null && mDataset.isNotEmpty()) {
            VIEW_TYPE_NORMAL
        } else {
            VIEW_TYPE_EMPTY
        }
    }

    override fun getItemCount(): Int {
        return if (mDataset!!.isNotEmpty()) {
            mDataset.size
        } else {
            1
        }
    }

    /*inner class RuangViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var r_no: TextView = itemView.findViewById(R.id.rowNo) as TextView
        var r_tvTitle: TextView = itemView.findViewById(R.id.tvTitle) as TextView
        var r_tvContent1: TextView = itemView.findViewById(R.id.tvContent1) as TextView
        var r_tvContent2: TextView = itemView.findViewById(R.id.tvContent2) as TextView

    }*/

    fun clear() {
        this.mDataset!!.clear()
        notifyDataSetChanged()
    }

    fun addItems(data: RealmResults<ResponseDataListProjectLocal>) {
        this.mDataset!!.addAll(data)
        notifyDataSetChanged()
    }

    interface Callback {
        fun onRepoEmptyViewRetryClick()
    }

    inner class ViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {
            itemView.tvvpengajuanDPPT.text = ""
            itemView.tvnominalProyek.text = ""
            itemView.tvbesarProyek.text = ""
            itemView.tvthnProyek.text = ""

            itemView.btnExport.setOnClickListener(null)
        }

        @SuppressLint("SetTextI18n")
        override fun onBind(position: Int) {
            super.onBind(position)

            val dataList = mDataset!![position]

            itemView.tvvpengajuanDPPT.text = dataList.getProjectName()
            itemView.tvnominalProyek.text = String.format("Rp %,.0f", dataList.getProjectNilaiAnggaran()).replace(",".toRegex(), ".")
            itemView.tvbesarProyek.text = dataList.getProjectSumberAnggaran()
            itemView.tvthnProyek.text = Html.fromHtml("${dataList.getProjectLuas()} (m<sup><small>2</small></sup>)")

//            !!important
//            itemView.btnOnclick.setSafeOnClickListener {
//                val param = "['${dataList.getProjectAssignProjectId()}','${dataList.getProjectName()}']"
//                val intent = ActivityDaftarKepemilikanTanahPertama.getStartIntent(mContext!!)
//                intent.putExtra("data_current_project", param)
//                mContext.startActivity(intent)
//            }

            itemView.btnExport.setSafeOnClickListener {
                myActivity.saveExcelFile("${System.currentTimeMillis()}.xlsx", dataList.getProjectAssignProjectId(),dataList.getProjectName())
            }

            itemView.cv.setSafeOnClickListener {
                if(mContext != null && MapBoxUtils().locationStatusCheck(mContext)){
                    mContext.startActivity(Intent(mContext, MapsProjectActivity::class.java)
                            .putExtra("projectId", dataList.getProjectAssignProjectId())
                            .putExtra("setting", MapsProjectIntentSetting(showImageUAV = true, showProjectUAV = true, showObjectUAV = true, canSelectObjectUAV = false, canTouchObjectUAV = true, defaultSelectedObjectUAVId = null, showCompareData = true)))
                }
            }


            inflateData(dataList)
        }

        private fun inflateData(dataList: ResponseDataListProjectLocal) {

        }
    }

    inner class EmptyViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }

        override fun onBind(position: Int) {
            super.onBind(position)

            //itemView.btnloadDataEmpty.visibility = View.VISIBLE
            itemView.btnloadDataEmpty.setOnClickListener {
                if (mCallback != null) {
                    mCallback!!.onRepoEmptyViewRetryClick()
                }
            }

        }
    }

    private fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
        val safeClickListener = SafeClickListener {
            onSafeClick(it)
        }
        setOnClickListener(safeClickListener)
    }

    companion object {
        const val VIEW_TYPE_EMPTY = 0
        const val VIEW_TYPE_NORMAL = 1
    }
}
