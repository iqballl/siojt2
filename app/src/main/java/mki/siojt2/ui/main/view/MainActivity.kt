package mki.siojt2.ui.main.view

import android.Manifest
import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.*
import android.text.Html
import android.text.format.DateFormat
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.itextpdf.text.*
import com.itextpdf.text.Font
import com.itextpdf.text.pdf.PdfPCell
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfWriter
import cn.pedant.SweetAlert.SweetAlertDialog
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_confirm_export.view.*
import kotlinx.android.synthetic.main.dialog_logout.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.model.GroupingDataSaranaPelengkap
import mki.siojt2.model.ResponDataDPPT
import mki.siojt2.model.ResponseDataListProjectLocal
import mki.siojt2.model.data_sync_to_server.DataChildSync
import mki.siojt2.model.data_sync_to_server.DataChildSync2
import mki.siojt2.model.local_save_image.DataImageBuilding
import mki.siojt2.model.local_save_image.DataImageFacilities
import mki.siojt2.model.local_save_image.DataImageLand
import mki.siojt2.model.local_save_image.DataImagePlants
import mki.siojt2.model.localsave.*
import mki.siojt2.persistence.Preference
import mki.siojt2.ui.login.view.LoginActivity
import mki.siojt2.ui.main.adapter_reycleview.PengajuanDPPTAdapter
import mki.siojt2.ui.main.presenter.MainPresenter
import mki.siojt2.utils.extension.MapBoxUtils
import mki.siojt2.utils.realm.RealmController
import org.apache.poi.ss.usermodel.*
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.ss.util.CellUtil
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.json.JSONObject
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

class MainActivity : BaseActivity(), MainView, PengajuanDPPTAdapter.Callback {

    private val PERMISSION_CALLBACK_CONSTANT = 100
    private val REQUEST_PERMISSION_SETTING = 101


    var permissionsRequired = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    lateinit var permissionStatus: SharedPreferences
    private var sentToSettings = false

    private var doubleBackToExitPressedOnce = false

    private lateinit var adapterpengajuanDPPT: PengajuanDPPTAdapter

    private var mainPresenter: MainPresenter? = null

    private var sweetAlretLoading: SweetAlertDialog? = null
    private lateinit var realm: Realm

    private var wb: XSSFWorkbook? = null
    lateinit var ws: XSSFSheet
    lateinit var fileCsv: File
    private var isCsv = false
    private var isJson = false

    private var satuamLuas: String? = Html.fromHtml("(m<sup><small>2</small></sup>)").toString()

    private var oldLastRow = 0
    private var currentLastRow = 13

    var lannumber = ""
    var letak = ""
    var landaffected = ""
    var sisaatas = ""
    var sisabawah = ""
    var kepemilikan = ""
    var statusTanah = ""
    var tandabukti = ""
    var jenisbangunan = ""
    var satuanbangunan = ""
    var luasbangunan = ""
    var jenistanaman = ""
    var k = ""
    var s = ""
    var b = ""
    var r = ""
    var jumlahtanaman = ""
    var satuantanaman = ""
    var fasilitasLain = ""
    var fasilitasValue = ""
    var landInfopiducia = ""
    var f1 = ""
    var f2 = ""
    var st1 = ""
    var st2 = ""
    var rf1 = ""
    var rf2 = ""
    var rc1 = ""
    var rc2 = ""
    var cl1 = ""
    var cl2 = ""
    var dw1 = ""
    var dw2 = ""
    var fl1 = ""
    var fl2 = ""
    var wl1 = ""
    var wl2 = ""
    var menguasai = ""

    private var isPdfile = false
    private val dateTemplate = "dd MMMM yyyy"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        Mapbox.getInstance(this, getString(R.string.mapbox_access_token))
        setContentView(R.layout.activity_main)

        System.setProperty("org.apache.poi.javax.xml.stream.XMLInputFactory", "com.fasterxml.aalto.stax.InputFactoryImpl")
        System.setProperty("org.apache.poi.javax.xml.stream.XMLOutputFactory", "com.fasterxml.aalto.stax.OutputFactoryImpl")
        System.setProperty("org.apache.poi.javax.xml.stream.XMLEventFactory", "com.fasterxml.aalto.stax.EventFactoryImpl")

        //init format excel
        /*Handler().postDelayed({
            CreateFormatExcelTask().execute()
        }, 1000)*/

        //init realm
        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        //get list project
        adapterpengajuanDPPT = PengajuanDPPTAdapter(this, this)
        adapterpengajuanDPPT.setCallback(this)
        getDPPT()

        //permission
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((ActivityCompat.checkSelfPermission(this@MainActivity, permissionsRequired[0]) != PackageManager.PERMISSION_GRANTED
                            || ActivityCompat.checkSelfPermission(this@MainActivity, permissionsRequired[1]) != PackageManager.PERMISSION_GRANTED
                            || ActivityCompat.checkSelfPermission(this@MainActivity, permissionsRequired[2]) != PackageManager.PERMISSION_GRANTED
                            || ActivityCompat.checkSelfPermission(this@MainActivity, permissionsRequired[3]) != PackageManager.PERMISSION_GRANTED)) {
                when {
                    ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, permissionsRequired[0])
                            || ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, permissionsRequired[1])
                            || ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, permissionsRequired[2])
                            || ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, permissionsRequired[3]) -> {
                        //Show Information about why you need the permission
                        val builder = AlertDialog.Builder(this@MainActivity)
                        builder.setTitle("Need Multiple Permissions")
                        builder.setMessage("This app needs Storage, Camera, and Location permissions.")
                        builder.setPositiveButton("Grant") { dialog, which ->
                            dialog.cancel()
                            ActivityCompat.requestPermissions(this@MainActivity, permissionsRequired, PERMISSION_CALLBACK_CONSTANT)
                        }
                        builder.show()
                    }
                    /*permissionStatus.getBoolean(permissionsRequired[0], false) -> {
                        //Previously Permission Request was cancelled with 'Dont Ask Again',
                        // Redirect to Settings after showing Information about why you need the permission
                        val builder = AlertDialog.Builder(this@MainActivity)
                        builder.setTitle("Need Multiple Permissions")
                        builder.setMessage("This app needs Storage, Camera, and Location permissions.")
                        builder.setPositiveButton("Grant") { dialog, which ->
                            dialog.cancel()
                            sentToSettings = true
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri = Uri.fromParts("package", packageName, null)
                            intent.data = uri
                            startActivityForResult(intent, REQUEST_PERMISSION_SETTING)
                            Toast.makeText(baseContext, "Go to Permissions to Grant Camera and Location", Toast.LENGTH_LONG).show()
                        }
                        builder.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }
                        builder.show()
                    }*/
                    else -> //just request the permission
                        ActivityCompat.requestPermissions(this@MainActivity, permissionsRequired, PERMISSION_CALLBACK_CONSTANT)
                }
                //showToast("Permissions Required")
                val editor = permissionStatus.edit()
                editor.putBoolean(permissionsRequired[0], true)
                editor.apply()
            } else {
                //You already have the permission, just go ahead.
                proceedAfterPermission()
            }
        }

        tvnameUser.text = Preference.auth.dataUser?.mobileUserEmployeeName
//        tvcityUser.text = Preference.auth.dataUser?.provinceName

        sweetAlretLoading = SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
        sweetAlretLoading?.titleText = "SYNCHRONOUS DATA"
        sweetAlretLoading?.setCancelable(false)
        sweetAlretLoading?.confirmText = "OK"
        sweetAlretLoading?.setConfirmClickListener { sDialog ->
            sDialog?.let { if (it.isShowing) it.dismiss() }
            adapterpengajuanDPPT.clear()
            getPresenter()?.getlistProject(Preference.auth.dataUser?.mobileUserId!!)
        }

        getPresenter()?.getlistProject(Preference.auth.dataUser?.mobileUserId!!)

        //sample data
        val listDPPT: ArrayList<ResponDataDPPT> = arrayListOf(
                ResponDataDPPT("Jalan layang cikampek", "Rp.155,028,000,000,000", "200,00 Ha", "2017"),
                ResponDataDPPT("Kereta cepat jakarta-bandung tahap 1", "Rp.155,028,000,000,000", "200,00 Ha", "2017"),
                ResponDataDPPT("Jalan layang cikampek", "Rp.155,028,000,000,000", "200,00 Ha", "2017"),
                ResponDataDPPT("Kereta cepat jakarta-bandung tahap 1", "Rp.155,028,000,000,000", "200,00 Ha", "2017")
        )

        rvlistpengajuanDPPT.apply {
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@MainActivity, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
            adapter = adapterpengajuanDPPT
        }

        btnsyncProject.setSafeOnClickListener {
            //getPresenter()?.getsyncProject(Preference.auth.dataUser?.mobileUserId!!, Preference.accessToken)

            val dataSync: MutableList<DataChildSync> = ArrayList()
            val mDatSync = DataChildSync()
            val mDatSync2 = DataChildSync2()

            realm.executeTransactionAsync { inrealm ->
                val resultProject = inrealm.where(ResponseDataListProjectLocal::class.java).findAll()
                val outerObject = JsonObject()
                val outerArray = JsonArray()
                val projectinnerObject = arrayOfNulls<JsonObject>(resultProject.size)
                for (projectCount in 0 until resultProject.size) {
                    /*val resultsPemilik1 = inrealm.where(PartyAdd::class.java).equalTo("ProjectId", resultProject[projectCount]!!.getProjectAssignProjectId()).findAll()
                    *//*val jsonObject = JsonObject()
                    jsonObject.addProperty("ProjectId", resultProject[projectCount]!!.getProjectAssignProjectId().toString())*//*

                    *//*val toConvertToJson = Gson().toJson(dataSync)
                    val jsonArray = JsonArray()
                    jsonArray.add(jsonObject)
                    val jsonStr  = jsonArray.toString()*//*

                    val data: List<PartyAdd> = inrealm.copyFromRealm(resultsPemilik1)
                    mDatSync.projectId = resultProject[projectCount]!!.getProjectAssignProjectId()
                    mDatSync2.partyAdd = data
                    mDatSync.dataChildSync2!!.partyAdd = mDatSync2.partyAdd

                    *//*for(pemilik1Count in resultsPemilik1.indices){
                        val resultProjectLand = inrealm.where(ProjectLand::class.java).equalTo("PartyIdTemp", resultsPemilik1[pemilik1Count]?.tempId).findAll()
                        val dataProjectLand: List<ProjectLand> = inrealm.copyFromRealm(resultProjectLand)
                        mDatSync.dataChildSync2!!.projectLand = dataProjectLand
                    }*//*

                    dataSync.add(mDatSync)
                    val toConvertToJson = Gson().toJson(dataSync)
                    Log.d("iqbal", toConvertToJson.toString())*/

                    val pemilikArray = JsonArray()
                    projectinnerObject[projectCount] = JsonObject()
                    projectinnerObject[projectCount]!!.addProperty("ProjectId", resultProject[projectCount]!!.getProjectAssignProjectId())

                    val resultsPemilik1 = inrealm.where(PartyAdd::class.java).equalTo("ProjectId", resultProject[projectCount]!!.getProjectAssignProjectId()).findAll()
                    val pemilikObject = arrayOfNulls<JsonObject>(resultsPemilik1.size)
                    for (indexPemilik1 in 0 until resultsPemilik1.size) {
                        pemilikObject[indexPemilik1] = JsonObject()
                        pemilikObject[indexPemilik1]!!.addProperty("TempId", resultsPemilik1[indexPemilik1]!!.tempId)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyId", resultsPemilik1[indexPemilik1]!!.partyId)
                        pemilikObject[indexPemilik1]!!.addProperty("ProjectId", resultsPemilik1[indexPemilik1]!!.projectId)
//                        pemilikObject[indexPemilik1]!!.addProperty("PartyIdentitySourceId", resultsPemilik1[indexPemilik1]!!.partyIdentitySourceId)
//                        pemilikObject[indexPemilik1]!!.addProperty("partyIdentitySourceName", resultsPemilik1[indexPemilik1]!!.partyIdentitySourceName)
//                        pemilikObject[indexPemilik1]!!.addProperty("PartyOwnerType", resultsPemilik1[indexPemilik1]!!.partyOwnerType)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyType", resultsPemilik1[indexPemilik1]!!.partyType)
//                        pemilikObject[indexPemilik1]!!.addProperty("PartyIdentitySourceOther", resultsPemilik1[indexPemilik1]!!.partyIdentitySourceOther)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyFullName", resultsPemilik1[indexPemilik1]!!.partyFullName)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyBirthPlaceCode", resultsPemilik1[indexPemilik1]!!.partyBirthPlaceCode)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyBirthPlaceName", resultsPemilik1[indexPemilik1]!!.partyBirthPlaceName)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyBirthPlaceOther", resultsPemilik1[indexPemilik1]!!.partyBirthPlaceOther)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyBirthDate", resultsPemilik1[indexPemilik1]!!.partyBirthDate)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyOccupationId", resultsPemilik1[indexPemilik1]!!.partyOccupationId)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyOccupationName", resultsPemilik1[indexPemilik1]!!.partyOccupationName)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyOccupationOther", resultsPemilik1[indexPemilik1]!!.partyOccupationOther)

                        pemilikObject[indexPemilik1]!!.addProperty("PartyProvinceCode", resultsPemilik1[indexPemilik1]!!.partyProvinceCode)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyProvinceName", resultsPemilik1[indexPemilik1]!!.partyProvinceName)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyRegencyCode", resultsPemilik1[indexPemilik1]!!.partyRegencyCode)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyRegencyName", resultsPemilik1[indexPemilik1]!!.partyRegencyName)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyDistrictCode", resultsPemilik1[indexPemilik1]!!.partyDistrictCode)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyDistrictName", resultsPemilik1[indexPemilik1]!!.partyDistrictName)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyVillageId", resultsPemilik1[indexPemilik1]!!.partyVillageId)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyVillageName", resultsPemilik1[indexPemilik1]!!.partyVillageName)

                        pemilikObject[indexPemilik1]!!.addProperty("PartyRT", resultsPemilik1[indexPemilik1]!!.partyRT)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyRW", resultsPemilik1[indexPemilik1]!!.partyRW)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyNumber", resultsPemilik1[indexPemilik1]!!.partyNumber)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyBlock", resultsPemilik1[indexPemilik1]!!.partyBlock)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyStreetName", resultsPemilik1[indexPemilik1]!!.partyStreetName)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyComplexName", resultsPemilik1[indexPemilik1]!!.partyComplexName)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyPostalCode", resultsPemilik1[indexPemilik1]!!.partyPostalCode)

                        pemilikObject[indexPemilik1]!!.addProperty("PartyNPWP", resultsPemilik1[indexPemilik1]!!.partyNPWP)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyYearStayBegin", resultsPemilik1[indexPemilik1]!!.partyYearStayBegin)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyIdentityNumber", resultsPemilik1[indexPemilik1]!!.partyIdentityNumber)
//                        pemilikObject[indexPemilik1]!!.addProperty("LivelihoodLiveId", resultsPemilik1[indexPemilik1]!!.livelihoodLiveId)
//                        pemilikObject[indexPemilik1]!!.addProperty("LivelihoodLiveName", resultsPemilik1[indexPemilik1]!!.livelihoodLiveName)
//                        pemilikObject[indexPemilik1]!!.addProperty("LivelihoodOther", resultsPemilik1[indexPemilik1]!!.livelihoodOther)
//                        pemilikObject[indexPemilik1]!!.addProperty("LivelihoodIncome", resultsPemilik1[indexPemilik1]!!.livelihoodIncome)

                        pemilikObject[indexPemilik1]!!.addProperty("PartyStatus", resultsPemilik1[indexPemilik1]!!.partyStatus)
                        pemilikObject[indexPemilik1]!!.addProperty("PartyCreateBy", resultsPemilik1[indexPemilik1]!!.partyCreateBy)
                        pemilikObject[indexPemilik1]!!.addProperty("isCreate", resultsPemilik1[indexPemilik1]!!.isCreate)


                        /* add pemilik pertama */
                        pemilikArray.add(pemilikObject[indexPemilik1])
                        projectinnerObject[projectCount]!!.add("PemilikPertama", pemilikArray)

                        val landArray = JsonArray()
                        val resultsProjectLand = inrealm.where(ProjectLand::class.java).equalTo("PartyIdTemp", resultsPemilik1[indexPemilik1]!!.tempId).findAll()
                        val landObject = arrayOfNulls<JsonObject>(resultsProjectLand.size)
                        for (indexLand in 0 until resultsProjectLand.size) {

                            /*data tanah section 1*/
                            landObject[indexLand] = JsonObject()
                            landObject[indexLand]!!.addProperty("LandIdTemp", resultsProjectLand[indexLand]!!.landIdTemp)
                            landObject[indexLand]!!.addProperty("LandId", resultsProjectLand[indexLand]!!.landId)
                            landObject[indexLand]!!.addProperty("PartyIdTemp", resultsProjectLand[indexLand]!!.partyIdTemp)
                            landObject[indexLand]!!.addProperty("PartyId", resultsProjectLand[indexLand]!!.partyId)
                            landObject[indexLand]!!.addProperty("ProjectId", resultsProjectLand[indexLand]!!.projectId)

                            landObject[indexLand]!!.addProperty("LandNumberList", resultsProjectLand[indexLand]!!.landNumberList)
                            landObject[indexLand]!!.addProperty("LandProvinceCode", resultsProjectLand[indexLand]!!.landProvinceCode)
                            landObject[indexLand]!!.addProperty("LandProvinceName", resultsProjectLand[indexLand]!!.landProvinceName)
                            landObject[indexLand]!!.addProperty("LandRegencyCode", resultsProjectLand[indexLand]!!.landRegencyCode)
                            landObject[indexLand]!!.addProperty("LandRegencyName", resultsProjectLand[indexLand]!!.landRegencyName)
                            landObject[indexLand]!!.addProperty("LandDistrictCode", resultsProjectLand[indexLand]!!.landDistrictCode)
                            landObject[indexLand]!!.addProperty("LandDistrictName", resultsProjectLand[indexLand]!!.landDistrictName)
                            landObject[indexLand]!!.addProperty("LandVillageId", resultsProjectLand[indexLand]!!.landVillageId)
                            landObject[indexLand]!!.addProperty("LandVillageName", resultsProjectLand[indexLand]!!.landVillageName)
                            landObject[indexLand]!!.addProperty("LandBlok", resultsProjectLand[indexLand]!!.landBlok)

                            landObject[indexLand]!!.addProperty("LandNumber", resultsProjectLand[indexLand]!!.landNumber)
                            landObject[indexLand]!!.addProperty("LandComplekName", resultsProjectLand[indexLand]!!.landComplekName)
                            landObject[indexLand]!!.addProperty("LandStreet", resultsProjectLand[indexLand]!!.landStreet)
                            landObject[indexLand]!!.addProperty("LandRT", resultsProjectLand[indexLand]!!.landRT)
                            landObject[indexLand]!!.addProperty("LandRW", resultsProjectLand[indexLand]!!.landRW)
                            landObject[indexLand]!!.addProperty("LandPostalCode", resultsProjectLand[indexLand]!!.landPostalCode)
                            landObject[indexLand]!!.addProperty("LandLatitude", resultsProjectLand[indexLand]!!.landLatitude)
                            landObject[indexLand]!!.addProperty("LandLangitude", resultsProjectLand[indexLand]!!.landLangitude)

                            /* data tanah section 2 */
                            landObject[indexLand]!!.addProperty("LandPositionToRoadId", resultsProjectLand[indexLand]!!.landPositionToRoadId)
                            landObject[indexLand]!!.addProperty("LandPositionToRoadName", resultsProjectLand[indexLand]!!.landPositionToRoadName)
                            landObject[indexLand]!!.addProperty("LandWideFrontRoad", resultsProjectLand[indexLand]!!.landWideFrontRoad)
                            landObject[indexLand]!!.addProperty("LandFrontage", resultsProjectLand[indexLand]!!.landWideFrontRoad)
                            landObject[indexLand]!!.addProperty("LandLength", resultsProjectLand[indexLand]!!.landLength)
                            landObject[indexLand]!!.addProperty("LandShapeId", resultsProjectLand[indexLand]!!.landShapeId)
                            landObject[indexLand]!!.addProperty("LandShapeName", resultsProjectLand[indexLand]!!.landShapeName)
                            landObject[indexLand]!!.addProperty("LandAreaCertificate", resultsProjectLand[indexLand]!!.landAreaCertificate)
                            landObject[indexLand]!!.addProperty("LandAreaAffected", resultsProjectLand[indexLand]!!.landAreaAffected)
                            landObject[indexLand]!!.addProperty("LandTypeLandTypeId", resultsProjectLand[indexLand]!!.landTypeLandTypeId)
                            landObject[indexLand]!!.addProperty("LandTypeLandTypeValue", resultsProjectLand[indexLand]!!.landTypeLandTypeValue)
                            landObject[indexLand]!!.addProperty("LandTypographyId", resultsProjectLand[indexLand]!!.landTypographyId)
                            landObject[indexLand]!!.addProperty("LandTypographyName", resultsProjectLand[indexLand]!!.landTypographyName)
                            landObject[indexLand]!!.addProperty("LandHeightToRoad", resultsProjectLand[indexLand]!!.landHeightToRoad)

                            /* data tanah section 3 */
                            landObject[indexLand]!!.addProperty("LandOwnershipId", resultsProjectLand[indexLand]!!.landOwnershipId)
                            landObject[indexLand]!!.addProperty("LandOwnershipName", resultsProjectLand[indexLand]!!.landOwnershipName)
                            landObject[indexLand]!!.addProperty("LandDateOfIssue", resultsProjectLand[indexLand]!!.landDateOfIssue)
                            landObject[indexLand]!!.addProperty("LandRightsExpirationDate", resultsProjectLand[indexLand]!!.landRightsExpirationDate)
                            landObject[indexLand]!!.addProperty("LandNIB", resultsProjectLand[indexLand]!!.landNIB)
                            landObject[indexLand]!!.addProperty("LandCertificateNumber", resultsProjectLand[indexLand]!!.landCertificateNumber)
                            landObject[indexLand]!!.addProperty("LandUtilizationLandUtilizationId", resultsProjectLand[indexLand]!!.landUtilizationLandUtilizationId)
                            landObject[indexLand]!!.addProperty("LandUtilizationLandUtilizationName", resultsProjectLand[indexLand]!!.landUtilizationLandUtilizationName)
                            landObject[indexLand]!!.addProperty("LandZoningSpatialPlanId", resultsProjectLand[indexLand]!!.landZoningSpatialPlanId)
                            landObject[indexLand]!!.addProperty("LandZoningSpatialPlanName", resultsProjectLand[indexLand]!!.landZoningSpatialPlanName)
                            landObject[indexLand]!!.addProperty("LandOrientationWest", resultsProjectLand[indexLand]!!.landOrientationWest)
                            landObject[indexLand]!!.addProperty("LandOrientationEast", resultsProjectLand[indexLand]!!.landOrientationEast)
                            landObject[indexLand]!!.addProperty("LandOrientationNorth", resultsProjectLand[indexLand]!!.landOrientationNorth)
                            landObject[indexLand]!!.addProperty("LandOrientationSouth", resultsProjectLand[indexLand]!!.landOrientationSouth)
                            landObject[indexLand]!!.addProperty("LandOrientationNorthwest", resultsProjectLand[indexLand]!!.landOrientationNorthwest)
                            landObject[indexLand]!!.addProperty("LandOrientationNortheast", resultsProjectLand[indexLand]!!.landOrientationNortheast)
                            landObject[indexLand]!!.addProperty("LandOrientationSoutheast", resultsProjectLand[indexLand]!!.landOrientationSoutheast)
                            landObject[indexLand]!!.addProperty("LandOrientationSouthwest", resultsProjectLand[indexLand]!!.landOrientationSouthwest)

                            /* data tanah section 4 */
                            landObject[indexLand]!!.addProperty("LandEaseToPropertyId", resultsProjectLand[indexLand]!!.landEaseToPropertyId)
                            landObject[indexLand]!!.addProperty("LandEaseToPropertyName", resultsProjectLand[indexLand]!!.landEaseToPropertyName)
                            landObject[indexLand]!!.addProperty("LandEaseToShopingCenterId", resultsProjectLand[indexLand]!!.landEaseToShopingCenterId)
                            landObject[indexLand]!!.addProperty("LandEaseToShopingCenterName", resultsProjectLand[indexLand]!!.landEaseToShopingCenterName)
                            landObject[indexLand]!!.addProperty("LandEaseToEducationFacilitiesId", resultsProjectLand[indexLand]!!.landEaseToEducationFacilitiesId)
                            landObject[indexLand]!!.addProperty("LandEaseToEducationFacilitiesName", resultsProjectLand[indexLand]!!.landEaseToEducationFacilitiesName)
                            landObject[indexLand]!!.addProperty("LandEaseToTouristSitesId", resultsProjectLand[indexLand]!!.landEaseToTouristSitesId)
                            landObject[indexLand]!!.addProperty("LandEaseToTouristSitesName", resultsProjectLand[indexLand]!!.landEaseToTouristSitesName)
                            landObject[indexLand]!!.addProperty("LandEaseOfTransportationId", resultsProjectLand[indexLand]!!.landEaseOfTransportationId)
                            landObject[indexLand]!!.addProperty("LandEaseOfTransportationName", resultsProjectLand[indexLand]!!.landEaseOfTransportationName)
                            landObject[indexLand]!!.addProperty("LandSecurityAgainstCrimeId", resultsProjectLand[indexLand]!!.landSecurityAgainstCrimeId)
                            landObject[indexLand]!!.addProperty("LandSecurityAgainstCrimeName", resultsProjectLand[indexLand]!!.landSecurityAgainstCrimeName)
                            landObject[indexLand]!!.addProperty("LandSafetyAgainstFireHazardsId", resultsProjectLand[indexLand]!!.landSafetyAgainstFireHazardsId)
                            landObject[indexLand]!!.addProperty("LandSafetyAgainstFireHazardsName", resultsProjectLand[indexLand]!!.landSafetyAgainstFireHazardsName)
                            landObject[indexLand]!!.addProperty("LandSecurityAgainstNaturalDisastersId", resultsProjectLand[indexLand]!!.landSecurityAgainstNaturalDisastersId)
                            landObject[indexLand]!!.addProperty("LandSecurityAgainstNaturalDisastersName", resultsProjectLand[indexLand]!!.landSecurityAgainstNaturalDisastersName)
                            landObject[indexLand]!!.addProperty("LandAvailableElectricalGridId", resultsProjectLand[indexLand]!!.landAvailableElectricalGridId)
                            landObject[indexLand]!!.addProperty("LandAvailableElectricalGridName", resultsProjectLand[indexLand]!!.landAvailableElectricalGridName)

                            landObject[indexLand]!!.addProperty("LandAvailableCleanWaterSystemId", resultsProjectLand[indexLand]!!.landAvailableCleanWaterSystemId)
                            landObject[indexLand]!!.addProperty("LandAvailableCleanWaterSystemName", resultsProjectLand[indexLand]!!.landAvailableCleanWaterSystemName)
                            landObject[indexLand]!!.addProperty("LandAvailableTelephoneNetworkId", resultsProjectLand[indexLand]!!.landAvailableTelephoneNetworkId)
                            landObject[indexLand]!!.addProperty("LandAvailableTelephoneNetworkName", resultsProjectLand[indexLand]!!.landAvailableTelephoneNetworkName)
                            landObject[indexLand]!!.addProperty("LandAvailableGasPipelineNetworkId", resultsProjectLand[indexLand]!!.landAvailableGasPipelineNetworkId)
                            landObject[indexLand]!!.addProperty("LandAvailableGasPipelineNetworkName", resultsProjectLand[indexLand]!!.landAvailableGasPipelineNetworkName)
                            landObject[indexLand]!!.addProperty("LandSUTETPresenceId", resultsProjectLand[indexLand]!!.landSUTETPresenceId)
                            landObject[indexLand]!!.addProperty("LandSUTETPresenceName", resultsProjectLand[indexLand]!!.landSUTETPresenceName)
                            landObject[indexLand]!!.addProperty("ProjectLandSUTETDistance", resultsProjectLand[indexLand]!!.projectLandSUTETDistance)
                            landObject[indexLand]!!.addProperty("LandCemeteryPresenceId", resultsProjectLand[indexLand]!!.landCemeteryPresenceId)
                            landObject[indexLand]!!.addProperty("LandCemeteryPresenceName", resultsProjectLand[indexLand]!!.landCemeteryPresenceName)
                            landObject[indexLand]!!.addProperty("LandCemeteryDistance", resultsProjectLand[indexLand]!!.landCemeteryDistance)
                            landObject[indexLand]!!.addProperty("LandSkewerPresenceId", resultsProjectLand[indexLand]!!.landSkewerPresenceId)
                            landObject[indexLand]!!.addProperty("LandSkewerPresenceName", resultsProjectLand[indexLand]!!.landSkewerPresenceName)
                            landObject[indexLand]!!.addProperty("LandAddInfoAddInfoId", resultsProjectLand[indexLand]!!.landAddInfoAddInfoId)
                            landObject[indexLand]!!.addProperty("LandAddInfoAddInfoName", resultsProjectLand[indexLand]!!.landAddInfoAddInfoName)
                            landObject[indexLand]!!.addProperty("PartyCreateBy", resultsProjectLand[indexLand]!!.partyCreateBy)
                            landObject[indexLand]!!.addProperty("isCreate", resultsProjectLand[indexLand]!!.isCreate)

                            /* add projectland */
                            landArray.add(landObject[indexLand])
                            pemilikObject[indexPemilik1]!!.add("ProjectLand", landArray)

                            val imagelandArray = JsonArray()
                            val resultsImageLand = inrealm.copyFromRealm(resultsProjectLand[indexLand]!!.dataImageLands)
                            val encoderesultsImageLand = imageLandEncodeBase64(resultsImageLand)
                            val imagelandpObject = arrayOfNulls<JsonObject>(resultsImageLand.size)

                            for (indexImageLand in 0 until encoderesultsImageLand!!.size) {
                                imagelandpObject[indexImageLand] = JsonObject()
                                imagelandpObject[indexImageLand]!!.addProperty("ImageIdLand", encoderesultsImageLand[indexImageLand].getImageId())
                                imagelandpObject[indexImageLand]!!.addProperty("ImagePathLand", encoderesultsImageLand[indexImageLand].getImagepathLand())
                                imagelandpObject[indexImageLand]!!.addProperty("ImageNameLand", encoderesultsImageLand[indexImageLand].getImagenameLand())

                                /* image land */
                                imagelandArray.add(imagelandpObject[indexImageLand])
                                landObject[indexLand]!!.add("ImageLand", imagelandArray)
                            }

                            val pemilik2Array = JsonArray()
                            val resultsPemilik2 = inrealm.where(Subjek2::class.java).equalTo("LandIdTemp", resultsProjectLand[indexLand]?.landIdTemp).findAll()
                            val pemilik2Object = arrayOfNulls<JsonObject>(resultsPemilik2.size)
                            for (indexPemilik2 in 0 until resultsPemilik2.size) {
                                pemilik2Object[indexPemilik2] = JsonObject()
                                pemilik2Object[indexPemilik2]!!.addProperty("SubjekIdTemp", resultsPemilik2[indexPemilik2]!!.subjekIdTemp)
                                pemilik2Object[indexPemilik2]!!.addProperty("SubjekId", resultsPemilik2[indexPemilik2]!!.subjekId)
                                pemilik2Object[indexPemilik2]!!.addProperty("ProjectId", resultsPemilik2[indexPemilik2]!!.projectId)
//                                pemilik2Object[indexPemilik2]!!.addProperty("LandIdTemp", resultsPemilik2[indexPemilik2]!!.subjek2IdentitySourceId)
                                pemilik2Object[indexPemilik2]!!.addProperty("LandId", resultsPemilik2[indexPemilik2]!!.landId)
                                pemilik2Object[indexPemilik2]!!.addProperty("ProjectPartyType", resultsPemilik2[indexPemilik2]!!.projectPartyType)

//                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2IdentitySourceId", resultsPemilik2[indexPemilik2]!!.subjek2IdentitySourceId)
//                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2IdentitySourceName", resultsPemilik2[indexPemilik2]!!.subjek2IdentitySourceName)
//                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2OwnerType", resultsPemilik2[indexPemilik2]!!.subjek2OwnerType)

//                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2IdentitySourceOther", resultsPemilik2[indexPemilik2]!!.subjek2IdentitySourceOther)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2FullName", resultsPemilik2[indexPemilik2]!!.subjek2FullName)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2BirthPlaceCode", resultsPemilik2[indexPemilik2]!!.subjek2BirthPlaceCode)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2BirthPlaceName", resultsPemilik2[indexPemilik2]!!.subjek2BirthPlaceName)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2BirthPlaceOther", resultsPemilik2[indexPemilik2]!!.subjek2BirthPlaceOther)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2BirthDate", resultsPemilik2[indexPemilik2]!!.subjek2BirthDate)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2OccupationId", resultsPemilik2[indexPemilik2]!!.subjek2OccupationId)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2OccupationName", resultsPemilik2[indexPemilik2]!!.subjek2OccupationName)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2OccupationOther", resultsPemilik2[indexPemilik2]!!.subjek2OccupationOther)

                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2ProvinceCode", resultsPemilik2[indexPemilik2]!!.subjek2ProvinceCode)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2ProvinceName", resultsPemilik2[indexPemilik2]!!.subjek2ProvinceName)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2RegencyCode", resultsPemilik2[indexPemilik2]!!.subjek2RegencyCode)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2RegencyName", resultsPemilik2[indexPemilik2]!!.subjek2RegencyName)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2DistrictCode", resultsPemilik2[indexPemilik2]!!.subjek2DistrictCode)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2DistrictName", resultsPemilik2[indexPemilik2]!!.subjek2DistrictName)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2VillageId", resultsPemilik2[indexPemilik2]!!.subjek2VillageId)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2VillageName", resultsPemilik2[indexPemilik2]!!.subjek2VillageName)

                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2RT", resultsPemilik2[indexPemilik2]!!.subjek2RT)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2RW", resultsPemilik2[indexPemilik2]!!.subjek2RW)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2Number", resultsPemilik2[indexPemilik2]!!.subjek2Number)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2Block", resultsPemilik2[indexPemilik2]!!.subjek2Block)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2StreetName", resultsPemilik2[indexPemilik2]!!.subjek2StreetName)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2ComplexName", resultsPemilik2[indexPemilik2]!!.subjek2ComplexName)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2PostalCode", resultsPemilik2[indexPemilik2]!!.subjek2PostalCode)

                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2NPWP", resultsPemilik2[indexPemilik2]!!.subjek2NPWP)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2PenguasaanTanahId", resultsPemilik2[indexPemilik2]!!.subjek2PenguasaanTanahId)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2PenguasaanTanahName", resultsPemilik2[indexPemilik2]!!.subjek2PenguasaanTanahName)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2YearStayBegin", resultsPemilik2[indexPemilik2]!!.subjek2YearStayBegin)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2IdentityNumber", resultsPemilik2[indexPemilik2]!!.subjek2IdentityNumber)
//                                pemilik2Object[indexPemilik2]!!.addProperty("LivelihoodLiveId", resultsPemilik2[indexPemilik2]!!.livelihoodLiveId)
//                                pemilik2Object[indexPemilik2]!!.addProperty("LivelihoodLiveName", resultsPemilik2[indexPemilik2]!!.livelihoodLiveName)
//                                pemilik2Object[indexPemilik2]!!.addProperty("LivelihoodOther", resultsPemilik2[indexPemilik2]!!.livelihoodOther)
//                                pemilik2Object[indexPemilik2]!!.addProperty("LivelihoodIncome", resultsPemilik2[indexPemilik2]!!.livelihoodIncome)

                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2Status", resultsPemilik2[indexPemilik2]!!.subjek2Status)
                                pemilik2Object[indexPemilik2]!!.addProperty("Subjek2CreateBy", resultsPemilik2[indexPemilik2]!!.subjek2CreateBy)
                                pemilik2Object[indexPemilik2]!!.addProperty("isCreate", resultsPemilik2[indexPemilik2]!!.isCreate)


                                /* pemilik 2 */
                                pemilik2Array.add(pemilik2Object[indexPemilik2])
                                landObject[indexLand]!!.add("PemilikKedua", pemilik2Array)
                            }

                            val ahliwarisArray = JsonArray()
                            val resultsAhliWaris = inrealm.where(AhliWaris::class.java).equalTo("LandIdTemp", resultsProjectLand[indexLand]?.landIdTemp).findAll()
                            val ahliWarisObject = arrayOfNulls<JsonObject>(resultsAhliWaris.size)

                            for (indexAhliWaris in 0 until resultsAhliWaris.size) {
                                ahliWarisObject[indexAhliWaris] = JsonObject()
                                ahliWarisObject[indexAhliWaris]!!.addProperty("AhliWarisIdTemp", resultsAhliWaris[indexAhliWaris]!!.ahliWarisIdTemp)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("AhliwarisId", resultsAhliWaris[indexAhliWaris]!!.ahliwarisId)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("ProjectId", resultsAhliWaris[indexAhliWaris]!!.projectId)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("LandIdTemp", resultsAhliWaris[indexAhliWaris]!!.landIdTemp)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("LandId", resultsAhliWaris[indexAhliWaris]!!.landId)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("AhliWarisPartyType", resultsAhliWaris[indexAhliWaris]!!.ahliWarisProjectPartyType)

//                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyIdentitySourceId", resultsAhliWaris[indexAhliWaris]!!.partyIdentitySourceId)
//                                ahliWarisObject[indexAhliWaris]!!.addProperty("partyIdentitySourceName", resultsAhliWaris[indexAhliWaris]!!.partyIdentitySourceName)
//                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyOwnerType", resultsAhliWaris[indexAhliWaris]!!.partyOwnerType)

//                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyIdentitySourceOther", resultsAhliWaris[indexAhliWaris]!!.partyIdentitySourceOther)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyFullName", resultsAhliWaris[indexAhliWaris]!!.partyFullName)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyBirthPlaceCode", resultsAhliWaris[indexAhliWaris]!!.partyBirthPlaceCode)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyBirthPlaceName", resultsAhliWaris[indexAhliWaris]!!.partyBirthPlaceName)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyBirthPlaceOther", resultsAhliWaris[indexAhliWaris]!!.partyBirthPlaceCode)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyBirthDate", resultsAhliWaris[indexAhliWaris]!!.partyBirthDate)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyOccupationId", resultsAhliWaris[indexAhliWaris]!!.partyOccupationId)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyOccupationName", resultsAhliWaris[indexAhliWaris]!!.partyOccupationName)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyOccupationOther", resultsAhliWaris[indexAhliWaris]!!.partyOccupationOther)

                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyProvinceCode", resultsAhliWaris[indexAhliWaris]!!.partyProvinceCode)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyProvinceName", resultsAhliWaris[indexAhliWaris]!!.partyProvinceName)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyRegencyCode", resultsAhliWaris[indexAhliWaris]!!.partyRegencyCode)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyRegencyName", resultsAhliWaris[indexAhliWaris]!!.partyRegencyName)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyDistrictCode", resultsAhliWaris[indexAhliWaris]!!.partyDistrictCode)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyDistrictName", resultsAhliWaris[indexAhliWaris]!!.partyDistrictName)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyVillageId", resultsAhliWaris[indexAhliWaris]!!.partyVillageId)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyVillageName", resultsAhliWaris[indexAhliWaris]!!.partyVillageName)

                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyRT", resultsAhliWaris[indexAhliWaris]!!.partyRT)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyRW", resultsAhliWaris[indexAhliWaris]!!.partyRW)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyNumber", resultsAhliWaris[indexAhliWaris]!!.partyNumber)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyBlock", resultsAhliWaris[indexAhliWaris]!!.partyBlock)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyStreetName", resultsAhliWaris[indexAhliWaris]!!.partyStreetName)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyComplexName", resultsAhliWaris[indexAhliWaris]!!.partyComplexName)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyPostalCode", resultsAhliWaris[indexAhliWaris]!!.partyPostalCode)

                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyNPWP", resultsAhliWaris[indexAhliWaris]!!.partyNPWP)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyYearStayBegin", resultsAhliWaris[indexAhliWaris]!!.partyYearStayBegin)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyIdentityNumber", resultsAhliWaris[indexAhliWaris]!!.partyIdentityNumber)
//                                ahliWarisObject[indexAhliWaris]!!.addProperty("LivelihoodLiveId", resultsAhliWaris[indexAhliWaris]!!.livelihoodLiveId)
//                                ahliWarisObject[indexAhliWaris]!!.addProperty("LivelihoodLiveName", resultsAhliWaris[indexAhliWaris]!!.livelihoodLiveName)
//                                ahliWarisObject[indexAhliWaris]!!.addProperty("LivelihoodOther", resultsAhliWaris[indexAhliWaris]!!.livelihoodOther)
//                                ahliWarisObject[indexAhliWaris]!!.addProperty("LivelihoodIncome", resultsAhliWaris[indexAhliWaris]!!.livelihoodIncome)

                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyStatus", resultsAhliWaris[indexAhliWaris]!!.partyStatus)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("PartyCreateBy", resultsAhliWaris[indexAhliWaris]!!.partyCreateBy)
                                ahliWarisObject[indexAhliWaris]!!.addProperty("isCreate", resultsAhliWaris[indexAhliWaris]!!.isCreate)


                                /* ahli waris */
                                ahliwarisArray.add(ahliWarisObject[indexAhliWaris])
                                landObject[indexLand]!!.add("AhliWaris", ahliwarisArray)
                            }

                            val bangunanArray = JsonArray()
                            val resultsBangunan = inrealm.where(Bangunan::class.java).equalTo("LandIdTemp", resultsProjectLand[indexLand]?.landIdTemp).findAll()
                            val bangunanObject = arrayOfNulls<JsonObject>(resultsBangunan.size)
                            for (indexBangunan in 0 until resultsBangunan.size) {
                                bangunanObject[indexBangunan] = JsonObject()
                                bangunanObject[indexBangunan]!!.addProperty("BangunanIdTemp", resultsBangunan[indexBangunan]!!.bangunanIdTemp)
                                bangunanObject[indexBangunan]!!.addProperty("BangunanId", resultsBangunan[indexBangunan]!!.bangunanId)
                                bangunanObject[indexBangunan]!!.addProperty("ProjectId", resultsBangunan[indexBangunan]!!.projectId)
                                bangunanObject[indexBangunan]!!.addProperty("LandIdTemp", resultsBangunan[indexBangunan]!!.landIdTemp)
                                bangunanObject[indexBangunan]!!.addProperty("LandId", resultsBangunan[indexBangunan]!!.landId)

                                bangunanObject[indexBangunan]!!.addProperty("ProjectPartyId", resultsBangunan[indexBangunan]!!.projectPartyId)
                                bangunanObject[indexBangunan]!!.addProperty("ProjectPartyName", resultsBangunan[indexBangunan]!!.projectPartyName)
                                bangunanObject[indexBangunan]!!.addProperty("ProjectBuildingTypeId", resultsBangunan[indexBangunan]!!.projectBuildingTypeId)
                                bangunanObject[indexBangunan]!!.addProperty("ProjectBuildingTypeName", resultsBangunan[indexBangunan]!!.projectBuildingTypeName)
                                bangunanObject[indexBangunan]!!.addProperty("ProjectBuildingFloorTotal", resultsBangunan[indexBangunan]!!.projectBuildingFloorTotal)
                                bangunanObject[indexBangunan]!!.addProperty("ProjectBuildingAreaUpperRoom", resultsBangunan[indexBangunan]!!.projectBuildingAreaUpperRoom)
                                bangunanObject[indexBangunan]!!.addProperty("ProjectBuildingAreaLowerRoom", resultsBangunan[indexBangunan]!!.projectBuildingAreaLowerRoom)
                                bangunanObject[indexBangunan]!!.addProperty("FoundationTypeId", resultsBangunan[indexBangunan]!!.foundationTypeId)
                                bangunanObject[indexBangunan]!!.addProperty("FoundationTypeName", resultsBangunan[indexBangunan]!!.foundationTypeName)
                                bangunanObject[indexBangunan]!!.addProperty("PBFoundationTypeAreaTotal", resultsBangunan[indexBangunan]!!.pbFoundationTypeAreaTotal)

                                bangunanObject[indexBangunan]!!.addProperty("StructureTypeId", resultsBangunan[indexBangunan]!!.structureTypeId)
                                bangunanObject[indexBangunan]!!.addProperty("StructureTypeName", resultsBangunan[indexBangunan]!!.structureTypeName)
                                bangunanObject[indexBangunan]!!.addProperty("PBStructureTypeAreaTotal", resultsBangunan[indexBangunan]!!.pbStructureTypeAreaTotal)

                                bangunanObject[indexBangunan]!!.addProperty("RoofFrameTypeId", resultsBangunan[indexBangunan]!!.roofFrameTypeId)
                                bangunanObject[indexBangunan]!!.addProperty("RoofFrameTypeName", resultsBangunan[indexBangunan]!!.roofFrameTypeName)
                                bangunanObject[indexBangunan]!!.addProperty("PBRoofFrameTypeAreaTotal", resultsBangunan[indexBangunan]!!.pbRoofFrameTypeAreaTotal)

                                bangunanObject[indexBangunan]!!.addProperty("RoofCoveringTypeId", resultsBangunan[indexBangunan]!!.roofCoveringTypeId)
                                bangunanObject[indexBangunan]!!.addProperty("RoofCoveringTypeName", resultsBangunan[indexBangunan]!!.roofCoveringTypeName)
                                bangunanObject[indexBangunan]!!.addProperty("PBRoofCoveringTypeAreaTotal", resultsBangunan[indexBangunan]!!.pbRoofCoveringTypeAreaTotal)

                                bangunanObject[indexBangunan]!!.addProperty("CeilingTypeId", resultsBangunan[indexBangunan]!!.ceilingTypeId)
                                bangunanObject[indexBangunan]!!.addProperty("CeilingTypeName", resultsBangunan[indexBangunan]!!.ceilingTypeName)
                                bangunanObject[indexBangunan]!!.addProperty("PBCeilingTypeAreaTotal", resultsBangunan[indexBangunan]!!.pbCeilingTypeAreaTotal)

                                bangunanObject[indexBangunan]!!.addProperty("DoorWindowTypeld", resultsBangunan[indexBangunan]!!.doorWindowTypeld)
                                bangunanObject[indexBangunan]!!.addProperty("DoorWindowTypeName", resultsBangunan[indexBangunan]!!.doorWindowTypeName)
                                bangunanObject[indexBangunan]!!.addProperty("PBDoorWindowTypeAreaTotal", resultsBangunan[indexBangunan]!!.pbDoorWindowTypeAreaTotal)

                                bangunanObject[indexBangunan]!!.addProperty("FloorTypeId", resultsBangunan[indexBangunan]!!.floorTypeId)
                                bangunanObject[indexBangunan]!!.addProperty("FloorTypeName", resultsBangunan[indexBangunan]!!.floorTypeName)
                                bangunanObject[indexBangunan]!!.addProperty("PBFloorTypeAreaTotal", resultsBangunan[indexBangunan]!!.pbFloorTypeAreaTotal)

                                bangunanObject[indexBangunan]!!.addProperty("WallTypeId", resultsBangunan[indexBangunan]!!.wallTypeId)
                                bangunanObject[indexBangunan]!!.addProperty("WallTypeName", resultsBangunan[indexBangunan]!!.wallTypeName)
                                bangunanObject[indexBangunan]!!.addProperty("PBFWallTypeAreaTotal", resultsBangunan[indexBangunan]!!.pbfWallTypeAreaTotal)

                                bangunanObject[indexBangunan]!!.addProperty("CreatedBy", resultsBangunan[indexBangunan]!!.createdBy)
                                bangunanObject[indexBangunan]!!.addProperty("isCreate", resultsBangunan[indexBangunan]!!.isCreate)

                                /* image building */
                                val imagebuildingArray = JsonArray()
                                val resultsImageBuilding = inrealm.copyFromRealm(resultsBangunan[indexBangunan]!!.dataImageBuilding)
                                val encoderesultsImageBuilding = imageBuildingEncodeBase64(resultsImageBuilding)
                                val imagebuildingObject = arrayOfNulls<JsonObject>(resultsImageBuilding.size)

                                for (indexImageBuilding in 0 until encoderesultsImageBuilding!!.size) {
                                    imagebuildingObject[indexImageBuilding] = JsonObject()
                                    imagebuildingObject[indexImageBuilding]!!.addProperty("ImageIdBuilding", encoderesultsImageBuilding[indexImageBuilding].getImageId())
                                    imagebuildingObject[indexImageBuilding]!!.addProperty("ImagePathBuilding", encoderesultsImageBuilding[indexImageBuilding].getImagepathLand())
                                    imagebuildingObject[indexImageBuilding]!!.addProperty("ImageNameBuilding", encoderesultsImageBuilding[indexImageBuilding].getImagenameLand())

                                    /* image building */
                                    imagebuildingArray.add(imagebuildingObject[indexImageBuilding])
                                    bangunanObject[indexBangunan]!!.add("ImageBuilding", imagebuildingArray)
                                }

                                /* bangunan */
                                bangunanArray.add(bangunanObject[indexBangunan])
                                landObject[indexLand]!!.add("Building", bangunanArray)

                            }

                            /* tanaman */
                            val tanamanArray = JsonArray()
                            val resultsTanaman = inrealm.where(Tanaman::class.java).equalTo("LandIdTemp", resultsProjectLand[indexLand]?.landIdTemp).findAll()
                            val tanamanObject = arrayOfNulls<JsonObject>(resultsTanaman.size)

                            for (indexTanaman in 0 until resultsTanaman.size) {
                                tanamanObject[indexTanaman] = JsonObject()
                                tanamanObject[indexTanaman]!!.addProperty("TanamanIdTemp", resultsTanaman[indexTanaman]!!.tanamanIdTemp)
                                tanamanObject[indexTanaman]!!.addProperty("TanamanId", resultsTanaman[indexTanaman]!!.tanamanId)
                                tanamanObject[indexTanaman]!!.addProperty("ProjectId", resultsTanaman[indexTanaman]!!.projectId)
                                tanamanObject[indexTanaman]!!.addProperty("LandIdTemp", resultsTanaman[indexTanaman]!!.landIdTemp)
                                tanamanObject[indexTanaman]!!.addProperty("LandId", resultsTanaman[indexTanaman]!!.landId)

                                tanamanObject[indexTanaman]!!.addProperty("PemilikTanamanId", resultsTanaman[indexTanaman]!!.pemilikTanamanId)
                                tanamanObject[indexTanaman]!!.addProperty("PemilikTanaman", resultsTanaman[indexTanaman]!!.pemilikTanaman)
                                tanamanObject[indexTanaman]!!.addProperty("JenisTanamanId", resultsTanaman[indexTanaman]!!.jenisTanamanId)
                                tanamanObject[indexTanaman]!!.addProperty("JenisTanamanNama", resultsTanaman[indexTanaman]!!.jenisTanamanNama)
                                tanamanObject[indexTanaman]!!.addProperty("NamaTanamanId", resultsTanaman[indexTanaman]!!.namaTanamanId)
                                tanamanObject[indexTanaman]!!.addProperty("NamaTanaman", resultsTanaman[indexTanaman]!!.namaTanaman)
                                tanamanObject[indexTanaman]!!.addProperty("UkuranTanamanId", resultsTanaman[indexTanaman]!!.ukuranTanamanId)
                                tanamanObject[indexTanaman]!!.addProperty("UkuranTanaman", resultsTanaman[indexTanaman]!!.ukuranTanaman)
                                tanamanObject[indexTanaman]!!.addProperty("JumlahTanaman", resultsTanaman[indexTanaman]!!.jumlahTanaman)
                                tanamanObject[indexTanaman]!!.addProperty("isCreate", resultsTanaman[indexTanaman]!!.isCreate)

                                val imageplantArray = JsonArray()
                                val resultsImagePlant = inrealm.copyFromRealm(resultsTanaman[indexTanaman]!!.dataImagePlants)
                                val encoderesultsImagePlant = imagePlantEncodeBase64(resultsImagePlant)
                                val imageplantObject = arrayOfNulls<JsonObject>(resultsImagePlant.size)

                                for (indexImagePlant in 0 until encoderesultsImagePlant!!.size) {
                                    imageplantObject[indexImagePlant] = JsonObject()
                                    imageplantObject[indexImagePlant]!!.addProperty("ImageIdPlant", encoderesultsImagePlant[indexImagePlant].getImageId())
                                    imageplantObject[indexImagePlant]!!.addProperty("ImagePathPlant", encoderesultsImagePlant[indexImagePlant].getImagepathLand())
                                    imageplantObject[indexImagePlant]!!.addProperty("ImageNamePlant", encoderesultsImagePlant[indexImagePlant].getImagenameLand())

                                    /* image building */
                                    imageplantArray.add(imageplantObject[indexImagePlant])
                                    tanamanObject[indexTanaman]!!.add("ImagePlant", imageplantArray)
                                }
                                tanamanArray.add(tanamanObject[indexTanaman])
                                landObject[indexLand]!!.add("Plants", tanamanArray)
                            }

                            val saranpelengkapArray = JsonArray()
                            val resultsSaranPelengkap = inrealm.where(SaranaLain::class.java).equalTo("LandIdTemp", resultsProjectLand[indexLand]?.landIdTemp).findAll()
                            val saranpelengkapObject = arrayOfNulls<JsonObject>(resultsSaranPelengkap.size)

                            for (indexSarana in 0 until resultsSaranPelengkap.size) {
                                saranpelengkapObject[indexSarana] = JsonObject()
                                saranpelengkapObject[indexSarana]!!.addProperty("SaranaIdTemp", resultsSaranPelengkap[indexSarana]!!.saranaIdTemp)
                                saranpelengkapObject[indexSarana]!!.addProperty("SaranaId", resultsSaranPelengkap[indexSarana]!!.saranaId)
                                saranpelengkapObject[indexSarana]!!.addProperty("ProjectId", resultsSaranPelengkap[indexSarana]!!.projectId)
                                saranpelengkapObject[indexSarana]!!.addProperty("LandIdTemp", resultsSaranPelengkap[indexSarana]!!.landIdTemp)
                                saranpelengkapObject[indexSarana]!!.addProperty("LandId", resultsSaranPelengkap[indexSarana]!!.landId)
                                saranpelengkapObject[indexSarana]!!.addProperty("PerlengkapanId", resultsSaranPelengkap[indexSarana]!!.perlengkapanId)
                                saranpelengkapObject[indexSarana]!!.addProperty("PerlengkapanName", resultsSaranPelengkap[indexSarana]!!.perlengkapanName)
                                saranpelengkapObject[indexSarana]!!.addProperty("JenisFasilitasId", resultsSaranPelengkap[indexSarana]!!.jenisFasilitasId)
                                saranpelengkapObject[indexSarana]!!.addProperty("JenisFasilitasName", resultsSaranPelengkap[indexSarana]!!.jenisFasilitasName)
                                saranpelengkapObject[indexSarana]!!.addProperty("Kapasitas", resultsSaranPelengkap[indexSarana]!!.kapasitas)
                                saranpelengkapObject[indexSarana]!!.addProperty("KapasitasValue", resultsSaranPelengkap[indexSarana]!!.kapasitasValue)
                                saranpelengkapObject[indexSarana]!!.addProperty("SatuanKapasitasValue", resultsSaranPelengkap[indexSarana]!!.satuanKapasitasValue)
                                saranpelengkapObject[indexSarana]!!.addProperty("isCreate", resultsSaranPelengkap[indexSarana]!!.isCreate)

                                val imagesaranaLengkapArray = JsonArray()
                                val resultsImageSaranaLengkap = inrealm.copyFromRealm(resultsSaranPelengkap[indexSarana]!!.dataImageOtherFacilities)
                                val encoderesultsImageSaranaLengkap = imageOtherFacilitiesEncodeBase64(resultsImageSaranaLengkap)
                                val imageSaranaLengkapObject = arrayOfNulls<JsonObject>(resultsImageSaranaLengkap.size)

                                for (indexImageSaranaPelengkap in 0 until encoderesultsImageSaranaLengkap!!.size) {
                                    imageSaranaLengkapObject[indexImageSaranaPelengkap] = JsonObject()
                                    imageSaranaLengkapObject[indexImageSaranaPelengkap]!!.addProperty("ImageIdFacilities", encoderesultsImageSaranaLengkap[indexImageSaranaPelengkap].getImageId())
                                    imageSaranaLengkapObject[indexImageSaranaPelengkap]!!.addProperty("ImagePathFacilities", encoderesultsImageSaranaLengkap[indexImageSaranaPelengkap].getImagepathLand())
                                    imageSaranaLengkapObject[indexImageSaranaPelengkap]!!.addProperty("ImageNameFacilities", encoderesultsImageSaranaLengkap[indexImageSaranaPelengkap].getImagenameLand())

                                    /* image building */
                                    imagesaranaLengkapArray.add(imageSaranaLengkapObject[indexImageSaranaPelengkap])
                                    saranpelengkapObject[indexSarana]!!.add("ImageFacilities", imagesaranaLengkapArray)
                                }

                                saranpelengkapArray.add(saranpelengkapObject[indexSarana])
                                landObject[indexLand]!!.add("OtherFacilities", saranpelengkapArray)
                            }

                        }
                    }
                    /*add projectId*/
                    outerArray.add(projectinnerObject[projectCount])
                }

                outerObject.add("results", outerArray)
                runOnUiThread {
                    saveToFileTxt("File_sync", outerObject.toString())
                }

            }
        }

        btnLogOut.setSafeOnClickListener {
            val mDialogView = LayoutInflater.from(this@MainActivity).inflate(R.layout.dialog_logout, null)
            val mBuilder = AlertDialog.Builder(this@MainActivity)
            val mDialog = mBuilder.create()

            mDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
            mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mDialog.setCancelable(false)
            val window = mDialog.window
            if (window != null) {
                val width = ViewGroup.LayoutParams.WRAP_CONTENT
                val height = ViewGroup.LayoutParams.WRAP_CONTENT
                mDialog.window!!.setLayout(width, height)
            }
            mDialog.setView(mDialogView)

            mDialogView.btnNoLogout.setOnClickListener {
                mDialog.dismiss()
            }

            mDialogView.btnyesLogout.setOnClickListener {
                //Utils.saveislogin(false, this@MainActivity)
                val statusLogin = Preference.sessionAuth()
                statusLogin!!.userName = statusLogin.userName
                statusLogin.userPassword = statusLogin.userPassword
                statusLogin.userStatusLogin = false
                Preference.savesessionAuth(statusLogin)
                //Log.d("hasil", Preference.sessionAuth()!!.userName)
                //Log.d("hasil", Preference.sessionAuth()!!.userPassword)
                //Log.d("hasil", Preference.sessionAuth()!!.userStatusLogin.toString())
                mDialog.dismiss()
                val intent = LoginActivity.getStartIntent(this@MainActivity)
                startActivity(intent)
                finish()
            }

            mDialog.show()
        }
    }

    private fun getDPPT() {
        adapterpengajuanDPPT.clear()
        val results = realm.where(ResponseDataListProjectLocal::class.java).findAll()
        Log.d("ayayayaxxxx", "")
        if (results != null) {
            adapterpengajuanDPPT.addItems(results)
            adapterpengajuanDPPT.notifyDataSetChanged()

            Log.d("ayayaya0", "")
            for(i in results){
                if(!i.getUAVDataResponse()?.images.isNullOrEmpty()){
                    for(x in i.getUAVDataResponse()?.images!!){
                        if(!x.url.isNullOrEmpty()){
                            var imgPath: String? = ""
                            val url = x.url
//                            val url = "https://picsum.photos/200/300"
                            CoroutineScope(Dispatchers.IO).launch {
//                            x.imgPath = MapBoxUtils().downloadImage(this, x.url!!)
                                imgPath = try{
                                    MapBoxUtils().downloadImage(this@MainActivity,url!!, System.currentTimeMillis().toString())
                                } catch(e: Exception){
                                    e.printStackTrace()
                                    MapBoxUtils().downloadImage(this@MainActivity,"https://upload.wikimedia.org/wikipedia/commons/5/51/Logo_BPN-KemenATR_%282017%29.png", System.currentTimeMillis().toString())
                                }
                                runOnUiThread {
                                    realm.beginTransaction()
                                    Log.d("ayayaya", imgPath)
                                    x.imgPath = imgPath
                                    realm.commitTransaction()
                                }
                            }
                        }

                    }
                }
            }
        } else {
            Log.e("data", "kosong")
        }
    }

    override fun onsuccesslistProject(dataListProject: MutableList<ResponseDataListProjectLocal>) {
        //adapterpengajuanDPPT.addItems(dataListProject)
        /* penguasaan atas tanah */
        try {
            realm.executeTransaction { inrealm ->
                inrealm.delete(ResponseDataListProjectLocal::class.java)
                inrealm.copyToRealmOrUpdate(dataListProject)
            }
        } catch (e: IOException) {
            Toast.makeText(this, "Gagal menambahkan data  ", Toast.LENGTH_SHORT).show()
        } finally {
            Log.d("pa", realm.where(ResponseDataListProjectLocal::class.java).findAll().size.toString())
        }

        getDPPT()

        MapBoxUtils().saveOfflineMap(this,
                                    "testing")
    }


    override fun onsuccesssyncProject(message: String?) {
        sweetAlretLoading?.contentText = message
        sweetAlretLoading?.show()
    }

    override fun onRepoEmptyViewRetryClick() {
        Toast.makeText(this@MainActivity, "synchronus data empty", Toast.LENGTH_LONG).show()
    }

    private fun getPresenter(): MainPresenter? {
        mainPresenter = MainPresenter()
        mainPresenter?.onAttach(this)
        return mainPresenter

    }

    private fun proceedAfterPermission() {
        Toast.makeText(this@MainActivity, "We got All Permissions", Toast.LENGTH_LONG).show()
    }

    private fun imageLandEncodeBase64(dataimage: MutableList<DataImageLand>): MutableList<DataImageLand>? {
        val dataimageEncode: MutableList<DataImageLand> = ArrayList()

        for (i in 0 until dataimage.size) {
            val mdataImage = DataImageLand()
            mdataImage.imageId = dataimage[i].imageId
            mdataImage.imagepathLand = getEncoded64ImageStringFromBitmap(converttoBitmap(dataimage[i].imagepathLand))
            mdataImage.imagenameLand = dataimage[i].imagenameLand
            dataimageEncode.add(mdataImage)
        }
        return dataimageEncode

    }

    private fun imageBuildingEncodeBase64(dataimage: MutableList<DataImageBuilding>): MutableList<DataImageBuilding>? {
        val dataimageEncode: MutableList<DataImageBuilding> = ArrayList()

        for (i in 0 until dataimage.size) {
            val mdataImage = DataImageBuilding()
            mdataImage.imageId = dataimage[i].imageId
            mdataImage.imagepathLand = getEncoded64ImageStringFromBitmap(converttoBitmap(dataimage[i].imagepathLand))
            mdataImage.imagenameLand = dataimage[i].imagenameLand
            dataimageEncode.add(mdataImage)
        }
        return dataimageEncode

    }

    private fun imagePlantEncodeBase64(dataimage: MutableList<DataImagePlants>): MutableList<DataImagePlants>? {
        val dataimageEncode: MutableList<DataImagePlants> = ArrayList()

        for (i in 0 until dataimage.size) {
            val mdataImage = DataImagePlants()
            mdataImage.imageId = dataimage[i].imageId
            mdataImage.imagepathLand = getEncoded64ImageStringFromBitmap(converttoBitmap(dataimage[i].imagepathLand))
            mdataImage.imagenameLand = dataimage[i].imagenameLand
            dataimageEncode.add(mdataImage)
        }
        return dataimageEncode

    }

    private fun imageOtherFacilitiesEncodeBase64(dataimage: MutableList<DataImageFacilities>): MutableList<DataImageFacilities>? {
        val dataimageEncode: MutableList<DataImageFacilities> = ArrayList()
        for (i in 0 until dataimage.size) {
            val mdataImage = DataImageFacilities()
            mdataImage.imageId = dataimage[i].imageId
            mdataImage.imagepathLand = getEncoded64ImageStringFromBitmap(converttoBitmap(dataimage[i].imagepathLand))
            mdataImage.imagenameLand = dataimage[i].imagenameLand
            dataimageEncode.add(mdataImage)
        }
        return dataimageEncode

    }

    private fun converttoBitmap(imagePath: String): Bitmap {
        val options = BitmapFactory.Options()
        options.inPreferredConfig = Bitmap.Config.ARGB_8888
        return BitmapFactory.decodeFile(imagePath, options)
    }

    private fun getEncoded64ImageStringFromBitmap(bitmap: Bitmap): String? {
        var encodeImage: String?
        var stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream)
        val byteFormat = stream.toByteArray()

        try {
            System.gc()
            encodeImage = Base64.encodeToString(byteFormat, Base64.NO_WRAP)
        } catch (e: Exception) {
            stream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 65, stream)
            encodeImage = Base64.encodeToString(byteFormat, Base64.NO_WRAP)
            Log.d("image", "Out of memory error catched")
        }

        return encodeImage
    }

    @SuppressLint("StaticFieldLeak")
    inner class CreateFormatExcelTask : AsyncTask<Void, Void, Void>() {
        override fun doInBackground(vararg params: Void?): Void? {

            val calendar = Calendar.getInstance(Locale.getDefault())
            //c.get(Calendar.YEAR)
            //c.get(Calendar.MONTH) + 1
            //c.get(Calendar.DAY_OF_MONTH)
            //val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
            //val currentDate = sdf.format(c.time)
            val currentDate = DateFormat.format(dateTemplate, calendar.time)

            wb = XSSFWorkbook()
            ws = wb!!.createSheet("Sheet1")
            ws.setColumnWidth(0, 5 * 256)
            ws.setColumnWidth(1, 5 * 256)
            ws.setColumnWidth(2, 55 * 256)
            ws.setColumnWidth(3, 55 * 256)
            ws.setColumnWidth(4, 10 * 256)
            ws.setColumnWidth(5, 45 * 256)

            /* kategori luas tanah */
            ws.setColumnWidth(6, 10 * 256)
            ws.setColumnWidth(7, 10 * 256)
            ws.setColumnWidth(8, 10 * 256)

            /*status tanah */
            ws.setColumnWidth(9, 17 * 256)

            /* surat tanda bukti hak tanah*/
            ws.setColumnWidth(10, 17 * 256)

            /* ruang atas dan bawah */
            ws.setColumnWidth(11, 17 * 256)
            ws.setColumnWidth(12, 17 * 256)

            /* bangunan */
            ws.setColumnWidth(13, 30 * 256)
            ws.setColumnWidth(14, 13 * 256)
            ws.setColumnWidth(15, 13 * 256)

            /* tanamn*/
            ws.setColumnWidth(16, 17 * 256)
            ws.setColumnWidth(17, 8 * 256)
            ws.setColumnWidth(18, 8 * 256)
            ws.setColumnWidth(19, 8 * 256)

            ws.setColumnWidth(20, 8 * 256)
            ws.setColumnWidth(21, 13 * 256)
            ws.setColumnWidth(22, 13 * 256)


            /* column sarana pelengkap */
            ws.setColumnWidth(23, 30 * 256)
            ws.setColumnWidth(24, 17 * 256)
            /* hak tanah */
            ws.setColumnWidth(25, 30 * 256)
            /* dampak perencanaan */
            ws.setColumnWidth(26, 17 * 256)
            /* keterangan*/
            ws.setColumnWidth(27, 17 * 256)
            ws.addIgnoredErrors(CellRangeAddress(0, 9999, 0, 9999), IgnoredErrorType.NUMBER_STORED_AS_TEXT)

            val defaultFontHeader = wb!!.createFont()
            defaultFontHeader.fontHeight = (16.0 * 20).toShort()
            defaultFontHeader.bold = true

            val defaultFontHeader2 = wb!!.createFont()
            defaultFontHeader2.fontHeight = (14.0 * 20).toShort()
            defaultFontHeader2.bold = true

            val defaultFontNormal = wb!!.createFont()
            defaultFontNormal.fontHeight = (11.0 * 20).toShort()

            /* style */
            val cellStyleHeader = wb!!.createCellStyle()
            cellStyleHeader.wrapText = true
            cellStyleHeader.setFont(defaultFontHeader)
            // justify text alignment
            cellStyleHeader.setAlignment(HorizontalAlignment.CENTER)
            cellStyleHeader.setVerticalAlignment(VerticalAlignment.CENTER)

            val cellStyleHeader2 = wb!!.createCellStyle()
            cellStyleHeader2.wrapText = true
            cellStyleHeader2.setFont(defaultFontHeader2)
            // justify text alignment
            cellStyleHeader2.setAlignment(HorizontalAlignment.CENTER)
            cellStyleHeader2.setVerticalAlignment(VerticalAlignment.CENTER)

            val cellStyleNormal = wb!!.createCellStyle()
            cellStyleNormal.wrapText = true
            cellStyleNormal.setFont(defaultFontNormal)
            // justify text alignment
            cellStyleNormal.setVerticalAlignment(VerticalAlignment.CENTER)
            cellStyleNormal.setAlignment(HorizontalAlignment.CENTER)

            /* set to default */
            oldLastRow = 0
            currentLastRow = 13

            //header
            val rowheader1 = ws.createRow(2)
            val colheader1 = rowheader1.createCell(1)
            colheader1.cellStyle = cellStyleHeader
            colheader1.setCellValue("DAFTAR NORMATIF")
            ws.addMergedRegion(CellRangeAddress(2, 2, 1, 27))

            /*val rowheader2 = ws.createRow(3)
            rowheader2.height = 2 * 256
            val colheader2 = rowheader2.createCell(1)
            colheader2.cellStyle = cellStyleHeader2
            colheader2.setCellValue("PENGADAAN TANAH PEMBANGUNAN JALAN TOL JAKARTA-CIKAMPEK II SISI SELATAN")
            ws.addMergedRegion(CellRangeAddress(3, 3, 1, 27))*/

            val rowheaderNomor = ws.createRow(5)
            val colheaderNomor = rowheaderNomor.createCell(1)
            colheaderNomor.cellStyle = cellStyleHeader2
            colheaderNomor.setCellValue("NOMOR : -")
            ws.addMergedRegion(CellRangeAddress(5, 5, 1, 27))

            val rowheaderDate = ws.createRow(7)
            val colheaderDate = rowheaderDate.createCell(1)
            colheaderDate.cellStyle = cellStyleHeader2
            colheaderDate.setCellValue("TANGGAL : $currentDate")
            ws.addMergedRegion(CellRangeAddress(7, 7, 1, 27))

            //table header 1
            val rowtableHeader1 = ws.createRow(9)
            rowtableHeader1.height = 4 * 256
            val colpihaktableHeader1 = rowtableHeader1.createCell(1)
            colpihaktableHeader1.cellStyle = cellStyleNormal
            colpihaktableHeader1.setCellValue("PIHAK YANG BERHAK")
            ws.addMergedRegion(CellRangeAddress(9, 9, 1, 3))

            val coltanahtableHeader1 = rowtableHeader1.createCell(4)
            coltanahtableHeader1.cellStyle = cellStyleNormal
            coltanahtableHeader1.setCellValue("TANAH")
            ws.addMergedRegion(CellRangeAddress(9, 9, 4, 10))

            val colruangatasbwhtableHeader1 = rowtableHeader1.createCell(11)
            colruangatasbwhtableHeader1.cellStyle = cellStyleNormal
            colruangatasbwhtableHeader1.setCellValue("RUANG ATAS TANAH DAN BAWAH TANAH")
            ws.addMergedRegion(CellRangeAddress(9, 9, 11, 12))

            val colbangunanhtableHeader1 = rowtableHeader1.createCell(13)
            colbangunanhtableHeader1.cellStyle = cellStyleNormal
            colbangunanhtableHeader1.setCellValue("BANGUNAN")
            ws.addMergedRegion(CellRangeAddress(9, 9, 13, 15))

            val coltanamantableHeader1 = rowtableHeader1.createCell(16)
            coltanamantableHeader1.cellStyle = cellStyleNormal
            coltanamantableHeader1.setCellValue("TANAMAN")
            ws.addMergedRegion(CellRangeAddress(9, 9, 16, 22))

            val colsarpeltableHeader1 = rowtableHeader1.createCell(23)
            colsarpeltableHeader1.cellStyle = cellStyleNormal
            colsarpeltableHeader1.setCellValue("BENDA LAIN YANG BERKAITAN DENGAN TANAH")
            ws.addMergedRegion(CellRangeAddress(9, 9, 23, 24))

            val colhaktanahtableHeader1 = rowtableHeader1.createCell(25)
            colhaktanahtableHeader1.cellStyle = cellStyleNormal
            colhaktanahtableHeader1.setCellValue("PEMBEBASAN HAK ATAS TANAH/PIDUCIA")
            ws.addMergedRegion(CellRangeAddress(9, 11, 25, 25))

            val coldampakpembangunantableHeader1 = rowtableHeader1.createCell(26)
            coldampakpembangunantableHeader1.cellStyle = cellStyleNormal
            coldampakpembangunantableHeader1.setCellValue("PERKIRAAN DAMPAK DARI RENCANA PEMBANGUNAN")
            ws.addMergedRegion(CellRangeAddress(9, 11, 26, 26))

            val colketerangantableHeader1 = rowtableHeader1.createCell(27)
            colketerangantableHeader1.cellStyle = cellStyleNormal
            colketerangantableHeader1.setCellValue("KET")
            ws.addMergedRegion(CellRangeAddress(9, 11, 27, 27))

            //table header 2
            val rowtableHeader2 = ws.createRow(10)
            rowtableHeader2.height = (2.1 * 256).toShort()
            val colnomortableHeader2 = rowtableHeader2.createCell(1)
            colnomortableHeader2.cellStyle = cellStyleNormal
            colnomortableHeader2.setCellValue("NO")
            ws.addMergedRegion(CellRangeAddress(10, 11, 1, 1))

            val colpemiliktableHeader2 = rowtableHeader2.createCell(2)
            colpemiliktableHeader2.cellStyle = cellStyleNormal
            colpemiliktableHeader2.setCellValue("PEMILIK")
            ws.addMergedRegion(CellRangeAddress(10, 11, 2, 2))

            val colpemilikkeduatableHeader2 = rowtableHeader2.createCell(3)
            colpemilikkeduatableHeader2.cellStyle = cellStyleNormal
            colpemilikkeduatableHeader2.setCellValue("Menguasai/Menggarap/Menyewa")
            ws.addMergedRegion(CellRangeAddress(10, 11, 3, 3))

            val colnobidangtableHeader2 = rowtableHeader2.createCell(4)
            colnobidangtableHeader2.cellStyle = cellStyleNormal
            colnobidangtableHeader2.setCellValue("NO BIDANG")
            ws.addMergedRegion(CellRangeAddress(10, 11, 4, 4))

            val colletaktanahtableHeader2 = rowtableHeader2.createCell(5)
            colletaktanahtableHeader2.cellStyle = cellStyleNormal
            colletaktanahtableHeader2.setCellValue("LETAK")
            ws.addMergedRegion(CellRangeAddress(10, 11, 5, 5))

            val colluastanahtableHeader2 = rowtableHeader2.createCell(6)
            colluastanahtableHeader2.cellStyle = cellStyleNormal
            colluastanahtableHeader2.setCellValue("LUAS (m2)")
            ws.addMergedRegion(CellRangeAddress(10, 11, 6, 8))

            val colstatustanahtableHeader2 = rowtableHeader2.createCell(9)
            colstatustanahtableHeader2.cellStyle = cellStyleNormal
            colstatustanahtableHeader2.setCellValue("STATUS TANAH")
            ws.addMergedRegion(CellRangeAddress(10, 11, 9, 9))

            val colbuktihaktanahtableHeader2 = rowtableHeader2.createCell(10)
            colbuktihaktanahtableHeader2.cellStyle = cellStyleNormal
            colbuktihaktanahtableHeader2.setCellValue("SURAT TANDA BUKTI/ALAS HAK")
            ws.addMergedRegion(CellRangeAddress(10, 11, 10, 10))

            val colruangatasbwh1tanahtableHeader2 = rowtableHeader2.createCell(11)
            colruangatasbwh1tanahtableHeader2.cellStyle = cellStyleNormal
            colruangatasbwh1tanahtableHeader2.setCellValue("HM/SARUSUN/LAINNYA")
            ws.addMergedRegion(CellRangeAddress(10, 11, 11, 11))

            val colruangatasbwh2tanahtableHeader2 = rowtableHeader2.createCell(12)
            colruangatasbwh2tanahtableHeader2.cellStyle = cellStyleNormal
            colruangatasbwh2tanahtableHeader2.setCellValue("LUAS (m2)")
            ws.addMergedRegion(CellRangeAddress(10, 11, 12, 12))

            val coljenisbangunantableHeader2 = rowtableHeader2.createCell(13)
            coljenisbangunantableHeader2.cellStyle = cellStyleNormal
            coljenisbangunantableHeader2.setCellValue("JENIS")
            ws.addMergedRegion(CellRangeAddress(10, 11, 13, 13))

            val coljumlahbangunantableHeader2 = rowtableHeader2.createCell(14)
            coljumlahbangunantableHeader2.cellStyle = cellStyleNormal
            coljumlahbangunantableHeader2.setCellValue("JUMLAH")
            ws.addMergedRegion(CellRangeAddress(10, 11, 14, 15))

//            val colsatuanbangunantableHeader2 = rowtableHeader2.createCell(15)
//            colsatuanbangunantableHeader2.cellStyle = cellStyleNormal
//            colsatuanbangunantableHeader2.setCellValue("SATUAN")
//            ws.addMergedRegion(CellRangeAddress(10, 11, 15, 15))

            /* tanaman */
            val coljenistanamantableHeader2 = rowtableHeader2.createCell(16)
            coljenistanamantableHeader2.cellStyle = cellStyleNormal
            coljenistanamantableHeader2.setCellValue("JENIS")
            ws.addMergedRegion(CellRangeAddress(10, 11, 16, 16))

            val colkelastanamantableHeader2 = rowtableHeader2.createCell(17)
            colkelastanamantableHeader2.cellStyle = cellStyleNormal
            colkelastanamantableHeader2.setCellValue("KELAS TANAMAN")
            ws.addMergedRegion(CellRangeAddress(10, 10, 17, 20))

            val coljumlahtanamantableHeader2 = rowtableHeader2.createCell(21)
            coljumlahtanamantableHeader2.cellStyle = cellStyleNormal
            coljumlahtanamantableHeader2.setCellValue("JUMLAH")
            ws.addMergedRegion(CellRangeAddress(10, 11, 21, 22))

//            val colsatuantanamantableHeader2 = rowtableHeader2.createCell(22)
//            colsatuantanamantableHeader2.cellStyle = cellStyleNormal
//            colsatuantanamantableHeader2.setCellValue("SATUAN")
//            ws.addMergedRegion(CellRangeAddress(10, 11, 22, 22))

            /* saran pelengkap*/
            val coljenissarpeltableHeader2 = rowtableHeader2.createCell(23)
            coljenissarpeltableHeader2.cellStyle = cellStyleNormal
            coljenissarpeltableHeader2.setCellValue("JENIS")
            ws.addMergedRegion(CellRangeAddress(10, 11, 23, 23))

            val coljumlahsarpeltableHeader2 = rowtableHeader2.createCell(24)
            coljumlahsarpeltableHeader2.cellStyle = cellStyleNormal
            coljumlahsarpeltableHeader2.setCellValue("JUMLAH")
            ws.addMergedRegion(CellRangeAddress(10, 11, 24, 24))


            //table header 3
            val rowtableHeader3 = ws.createRow(11)
            rowtableHeader3.height = (2.1 * 256).toShort()

//            val colluastanah1tableHeader3 = rowtableHeader3.createCell(6)
//            colluastanah1tableHeader3.cellStyle = cellStyleNormal
//            colluastanah1tableHeader3.setCellValue("TERKENA")
//
//            val colluastanah2tableHeader3 = rowtableHeader3.createCell(7)
//            colluastanah2tableHeader3.cellStyle = cellStyleNormal
//            colluastanah2tableHeader3.setCellValue("SISA ATAS")
//
//            val colluastanah3tableHeader3 = rowtableHeader3.createCell(8)
//            colluastanah3tableHeader3.cellStyle = cellStyleNormal
//            colluastanah3tableHeader3.setCellValue("SISA BAWAH")

            /* kategori kelas tanaman */
            val colljenistanaman1tableHeader3 = rowtableHeader3.createCell(17)
            colljenistanaman1tableHeader3.cellStyle = cellStyleNormal
            colljenistanaman1tableHeader3.setCellValue("K")

            val colljenistanaman2tableHeader3 = rowtableHeader3.createCell(18)
            colljenistanaman2tableHeader3.cellStyle = cellStyleNormal
            colljenistanaman2tableHeader3.setCellValue("S")

            val colljenistanaman3tableHeader3 = rowtableHeader3.createCell(19)
            colljenistanaman3tableHeader3.cellStyle = cellStyleNormal
            colljenistanaman3tableHeader3.setCellValue("B")

            val colljenistanaman4tableHeader3 = rowtableHeader3.createCell(20)
            colljenistanaman4tableHeader3.cellStyle = cellStyleNormal
            colljenistanaman4tableHeader3.setCellValue("R")

            //table header 4
            val rowtableHeader4 = ws.createRow(12)
            rowtableHeader4.height = 2 * 256

            val colnomortableHeader4 = rowtableHeader4.createCell(1)
            colnomortableHeader4.cellStyle = cellStyleNormal
            colnomortableHeader4.setCellValue("1")

            val colpemilikpertamatableHeader4 = rowtableHeader4.createCell(2)
            colpemilikpertamatableHeader4.cellStyle = cellStyleNormal
            colpemilikpertamatableHeader4.setCellValue("2")

            val colpemilikpenyewatableHeader4 = rowtableHeader4.createCell(3)
            colpemilikpenyewatableHeader4.cellStyle = cellStyleNormal
            colpemilikpenyewatableHeader4.setCellValue("3")

            val colnobidangtableHeader4 = rowtableHeader4.createCell(4)
            colnobidangtableHeader4.cellStyle = cellStyleNormal
            colnobidangtableHeader4.setCellValue("4")

            val colletaktanahtableHeader4 = rowtableHeader4.createCell(5)
            colletaktanahtableHeader4.cellStyle = cellStyleNormal
            colletaktanahtableHeader4.setCellValue("5")

            val colluastanahtableHeader4 = rowtableHeader4.createCell(6)
            colluastanahtableHeader4.cellStyle = cellStyleNormal
            colluastanahtableHeader4.setCellValue("6")
            ws.addMergedRegion(CellRangeAddress(12, 12, 6, 8))

            val colstatustanahtableHeader4 = rowtableHeader4.createCell(9)
            colstatustanahtableHeader4.cellStyle = cellStyleNormal
            colstatustanahtableHeader4.setCellValue("7")

            val coltanahtableHeader4 = rowtableHeader4.createCell(10)
            coltanahtableHeader4.cellStyle = cellStyleNormal
            coltanahtableHeader4.setCellValue("8")

            val colruangatasbwh1tableHeader4 = rowtableHeader4.createCell(11)
            colruangatasbwh1tableHeader4.cellStyle = cellStyleNormal
            colruangatasbwh1tableHeader4.setCellValue("9")

            val colruangatasbwh2tableHeader4 = rowtableHeader4.createCell(12)
            colruangatasbwh2tableHeader4.cellStyle = cellStyleNormal
            colruangatasbwh2tableHeader4.setCellValue("10")

            val coljenisBangunantableHeader4 = rowtableHeader4.createCell(13)
            coljenisBangunantableHeader4.cellStyle = cellStyleNormal
            coljenisBangunantableHeader4.setCellValue("11")

            val coljumlahBangunantableHeader4 = rowtableHeader4.createCell(14)
            coljumlahBangunantableHeader4.cellStyle = cellStyleNormal
            coljumlahBangunantableHeader4.setCellValue("12")
            ws.addMergedRegion(CellRangeAddress(12, 12, 14, 15))

            val coljenisTanamantableHeader4 = rowtableHeader4.createCell(16)
            coljenisTanamantableHeader4.cellStyle = cellStyleNormal
            coljenisTanamantableHeader4.setCellValue("13")

            val colkelasTanaman1tableHeader4 = rowtableHeader4.createCell(17)
            colkelasTanaman1tableHeader4.cellStyle = cellStyleNormal
            colkelasTanaman1tableHeader4.setCellValue("14")

            val colkelasTanaman2tableHeader4 = rowtableHeader4.createCell(18)
            colkelasTanaman2tableHeader4.cellStyle = cellStyleNormal
            colkelasTanaman2tableHeader4.setCellValue("15")

            val colkelasTanaman3tableHeader4 = rowtableHeader4.createCell(19)
            colkelasTanaman3tableHeader4.cellStyle = cellStyleNormal
            colkelasTanaman3tableHeader4.setCellValue("16")

            val colkelasTanaman4tableHeader4 = rowtableHeader4.createCell(20)
            colkelasTanaman4tableHeader4.cellStyle = cellStyleNormal
            colkelasTanaman4tableHeader4.setCellValue("17")

            val coljumlahanamantableHeader4 = rowtableHeader4.createCell(21)
            coljumlahanamantableHeader4.cellStyle = cellStyleNormal
            coljumlahanamantableHeader4.setCellValue("18")

            ws.addMergedRegion(CellRangeAddress(12, 12, 21, 22))

            val coljenisSarPeltableHeader4 = rowtableHeader4.createCell(23)
            coljenisSarPeltableHeader4.cellStyle = cellStyleNormal
            coljenisSarPeltableHeader4.setCellValue("19")

            val coljumlahSarPeltableHeader4 = rowtableHeader4.createCell(24)
            coljumlahSarPeltableHeader4.cellStyle = cellStyleNormal
            coljumlahSarPeltableHeader4.setCellValue("20")

            val colHakAtasTanahtableHeader4 = rowtableHeader4.createCell(25)
            colHakAtasTanahtableHeader4.cellStyle = cellStyleNormal
            colHakAtasTanahtableHeader4.setCellValue("21")

            val coldampakPembangunantableHeader4 = rowtableHeader4.createCell(26)
            coldampakPembangunantableHeader4.cellStyle = cellStyleNormal
            coldampakPembangunantableHeader4.setCellValue("22")

            val colKeteranganTanahtableHeader4 = rowtableHeader4.createCell(27)
            colKeteranganTanahtableHeader4.cellStyle = cellStyleNormal
            colKeteranganTanahtableHeader4.setCellValue("23")

            /*ws.addMergedRegion(CellRangeAddress(1, 1, 0, 26))
            ws.addMergedRegion(CellRangeAddress(2, 2, 0, 26))
            ws.addMergedRegion(CellRangeAddress(4, 4, 0, 26))
            ws.addMergedRegion(CellRangeAddress(6, 6, 0, 26))

            ws.addMergedRegion(CellRangeAddress(8, 8, 0, 2))
            ws.addMergedRegion(CellRangeAddress(8, 8, 3, 9))
            ws.addMergedRegion(CellRangeAddress(8, 8, 10, 11))
            ws.addMergedRegion(CellRangeAddress(8, 8, 12, 14))
            ws.addMergedRegion(CellRangeAddress(8, 8, 15, 21))
            ws.addMergedRegion(CellRangeAddress(8, 8, 22, 23))
            ws.addMergedRegion(CellRangeAddress(8, 10, 24, 24))
            ws.addMergedRegion(CellRangeAddress(8, 10, 25, 25))
            ws.addMergedRegion(CellRangeAddress(8, 10, 26, 26))

            ws.addMergedRegion(CellRangeAddress(9, 11, 0, 0))
            ws.addMergedRegion(CellRangeAddress(9, 11, 1, 1))
            ws.addMergedRegion(CellRangeAddress(9, 11, 2, 2))
            ws.addMergedRegion(CellRangeAddress(9, 11, 3, 3))
            ws.addMergedRegion(CellRangeAddress(9, 11, 4, 4))
            ws.addMergedRegion(CellRangeAddress(9, 10, 5, 7))
            ws.addMergedRegion(CellRangeAddress(9, 11, 8, 8))
            ws.addMergedRegion(CellRangeAddress(9, 11, 9, 9))
            ws.addMergedRegion(CellRangeAddress(9, 11, 10, 10))
            ws.addMergedRegion(CellRangeAddress(9, 11, 11, 11))
            ws.addMergedRegion(CellRangeAddress(9, 11, 12, 12))
            ws.addMergedRegion(CellRangeAddress(9, 11, 13, 13))
            ws.addMergedRegion(CellRangeAddress(9, 11, 14, 14))
            ws.addMergedRegion(CellRangeAddress(9, 11, 15, 15))
            ws.addMergedRegion(CellRangeAddress(9, 10, 16, 19))
            ws.addMergedRegion(CellRangeAddress(9, 11, 20, 20))
            ws.addMergedRegion(CellRangeAddress(9, 11, 21, 21))
            ws.addMergedRegion(CellRangeAddress(9, 11, 22, 22))
            ws.addMergedRegion(CellRangeAddress(9, 11, 23, 23))

            ws.addMergedRegion(CellRangeAddress(12, 12, 5, 7))
            ws.addMergedRegion(CellRangeAddress(12, 12, 13, 14))

            //judul
            val judul = ws.createRow(1)
            val celljudul = judul.createCell(0)
            val celStylejudul = wb!!.createCellStyle()
            celStylejudul.wrapText = true
            celljudul.cellStyle = celStylejudul
            CellUtil.setAlignment(celljudul, HorizontalAlignment.CENTER)
            celljudul.setCellValue("DAFTAR NOMINATIF")

            //judul
            val judulNomor = ws.createRow(4)
            val cellNomor = judulNomor.createCell(0)
            val celStyleNomor = wb!!.createCellStyle()
            celStyleNomor.wrapText = true
            cellNomor.cellStyle = celStyleNomor
            CellUtil.setAlignment(cellNomor, HorizontalAlignment.CENTER)
            cellNomor.setCellValue("Nomor : ")
            //judul
            val judulTanggal = ws.createRow(6)
            val cellTanggal = judulTanggal.createCell(0)
            val celStyleTanggal = wb!!.createCellStyle()
            celStyleTanggal.wrapText = true
            cellTanggal.cellStyle = celStyleTanggal
            CellUtil.setAlignment(cellTanggal, HorizontalAlignment.CENTER)
            cellTanggal.setCellValue("Tanggal : ")

            //baris pertama
            val row = ws.createRow(8)
            var cell = row.createCell(0)
            val celStyle = wb!!.createCellStyle()
            celStyle.wrapText = true
            celStyle.setVerticalAlignment(VerticalAlignment.CENTER)
            celStyle.setAlignment(HorizontalAlignment.CENTER)
            cell.cellStyle = celStyle

            CellUtil.setAlignment(cell, HorizontalAlignment.CENTER)
            cell.setCellValue("PIHAK YANG BERHAK")
            cell = row.createCell(3)

            CellUtil.setAlignment(cell, HorizontalAlignment.CENTER)
            cell.setCellValue("TANAH")
            cell = row.createCell(10)

            CellUtil.setAlignment(cell, HorizontalAlignment.CENTER)
            cell.setCellValue("RUANG ATAS DAN BAWAH")
            cell = row.createCell(12)

            CellUtil.setAlignment(cell, HorizontalAlignment.CENTER)
            cell.setCellValue("BANGUNAN")
            cell = row.createCell(15)

            CellUtil.setAlignment(cell, HorizontalAlignment.CENTER)
            cell.setCellValue("TANAMAN")
            cell = row.createCell(22)

            CellUtil.setAlignment(cell, HorizontalAlignment.CENTER)
            cell.setCellValue("BENDA LAIN YANG BERKAITAN DENGAN TANAH")
            cell = row.createCell(24)

            CellUtil.setVerticalAlignment(cell, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell, HorizontalAlignment.CENTER)
            cell.setCellValue("Pembebasan Hak Atas Tanah / Piducia")
            cell = row.createCell(25)

            CellUtil.setVerticalAlignment(cell, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell, HorizontalAlignment.CENTER)
            cell.setCellValue("Perkiraan Dampak dari Rencana Pembangunan")
            cell = row.createCell(26)

            CellUtil.setVerticalAlignment(cell, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell, HorizontalAlignment.CENTER)
            cell.setCellValue("KET")


            //baris kedua
            val row2 = ws.createRow(9)
            var cell2 = row2.createCell(0)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("No")
            cell2 = row2.createCell(1)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("Pemilik")
            cell2 = row2.createCell(2)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("Menguasai / Menggarap / Menyewa")
            cell2 = row2.createCell(3)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("No Bidang")
            cell2 = row2.createCell(4)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("Letak")
            cell2 = row2.createCell(5)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("Luas (m2)")
            cell2 = row2.createCell(8)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("Status Tanah")
            cell2 = row2.createCell(9)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("Surat Tanda Bukti / Alas Hak")
            cell2 = row2.createCell(10)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("HM/Sarusun/Lainnya")
            cell2 = row2.createCell(11)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("Luas (m2)")
            cell2 = row2.createCell(12)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("Jenis")
            cell2 = row2.createCell(13)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("Jumlah")
            cell2 = row2.createCell(14)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("Satuan")
            cell2 = row2.createCell(15)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("Jenis")
            cell2 = row2.createCell(16)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("Kelas Tanaman")
            cell2 = row2.createCell(20)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("Jumlah")
            cell2 = row2.createCell(21)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("Satuan")
            cell2 = row2.createCell(22)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("Jenis")
            cell2 = row2.createCell(23)

            CellUtil.setVerticalAlignment(cell2, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell2, HorizontalAlignment.CENTER)
            cell2.setCellValue("Jumlah")


            //baris tiga
            val row3 = ws.createRow(11)
            var cell3 = row3.createCell(5)

            CellUtil.setVerticalAlignment(cell3, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell3, HorizontalAlignment.CENTER)
            cell3.setCellValue("Terkena")
            cell3 = row3.createCell(6)

            CellUtil.setVerticalAlignment(cell3, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell3, HorizontalAlignment.CENTER)
            cell3.setCellValue("Sisa Atas")
            cell3 = row3.createCell(7)

            CellUtil.setVerticalAlignment(cell3, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell3, HorizontalAlignment.CENTER)
            cell3.setCellValue("Sisa Bawah")
            cell3 = row3.createCell(16)

            CellUtil.setVerticalAlignment(cell3, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell3, HorizontalAlignment.CENTER)
            cell3.setCellValue("K")
            cell3 = row3.createCell(17)


            CellUtil.setVerticalAlignment(cell3, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell3, HorizontalAlignment.CENTER)
            cell3.setCellValue("S")
            cell3 = row3.createCell(18)


            CellUtil.setVerticalAlignment(cell3, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell3, HorizontalAlignment.CENTER)
            cell3.setCellValue("B")
            cell3 = row3.createCell(19)


            CellUtil.setVerticalAlignment(cell3, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell3, HorizontalAlignment.CENTER)
            cell3.setCellValue("R")

            //baris empat
            val row4 = ws.createRow(12)
            var cell4 = row4.createCell(0)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("1")
            cell4 = row4.createCell(1)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("2")
            cell4 = row4.createCell(2)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("3")
            cell4 = row4.createCell(3)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("4")
            cell4 = row4.createCell(4)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("5")
            cell4 = row4.createCell(5)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("6")
            cell4 = row4.createCell(8)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("7")
            cell4 = row4.createCell(9)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("8")
            cell4 = row4.createCell(10)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("9")
            cell4 = row4.createCell(11)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("10")
            cell4 = row4.createCell(12)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("11")
            cell4 = row4.createCell(13)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("12")
            cell4 = row4.createCell(15)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("13")
            cell4 = row4.createCell(16)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("14")
            cell4 = row4.createCell(17)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("15")
            cell4 = row4.createCell(18)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("16")
            cell4 = row4.createCell(19)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("17")
            cell4 = row4.createCell(20)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("18")
            cell4 = row4.createCell(21)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("19")
            cell4 = row4.createCell(22)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("20")
            cell4 = row4.createCell(23)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("21")
            cell4 = row4.createCell(24)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("22")
            cell4 = row4.createCell(25)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("23")
            cell4 = row4.createCell(26)

            CellUtil.setVerticalAlignment(cell4, VerticalAlignment.CENTER)
            CellUtil.setAlignment(cell4, HorizontalAlignment.CENTER)
            cell4.setCellValue("24")*/

            return null
        }

    }

    @SuppressLint("SdCardPath")
    fun saveToFileTxt(fileName: String, mData: String) {

        val timeStamp = SimpleDateFormat("ddMMyyyy", Locale.getDefault()).format(System.currentTimeMillis())

        val mfilename: String? = fileName + "_$timeStamp.txt"
        /* custom path ditector, if use v.1*/
        val pathDir = File("/sdcard/SIOJT/")

        /* create file if use v.1*/
        //val outputFile = File(pathDir, "$mfilename")
        /* create bufferdWriter if use  v.1 */
        //val ostream: BufferedWriter?

        var ostream: FileOutputStream? = null
        try {
            // used if using v1
            /*if (!pathDir.exists()) {
                pathDir.mkdirs()
            }

            if (!outputFile.exists()) {
                outputFile.delete()
                outputFile.createNewFile()
            }*/

            /* v.1 */
            /*val fileWriter = FileWriter(outputFile.absoluteFile)
            ostream = BufferedWriter(fileWriter)*/

            ostream = openFileOutput(mfilename, Context.MODE_PRIVATE)
            ostream.write(mData.toByteArray())
        } catch (e: IOException) {
            e.printStackTrace()
            Log.d("Error", "Create File Gagal ${e.message}")
        } finally {
            if (ostream != null) {
                try {
                    ostream.close()
                    sweetAlretLoading = SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                    sweetAlretLoading?.titleText = "Save file Berhasil"
                    sweetAlretLoading?.setCancelable(false)
                    sweetAlretLoading?.contentText = "${filesDir}${"/"}$mfilename"
                    sweetAlretLoading?.confirmText = "OK"
                    sweetAlretLoading?.setConfirmClickListener { sDialog ->
                        sDialog?.let { if (it.isShowing) it.dismiss() }
                    }
                    sweetAlretLoading?.show()
                } catch (e: IOException) {
                    e.printStackTrace()
                    Log.d("Error", "Export File Gagal ${e.message}")
                }

            }

        }
    }

    @SuppressLint("SdCardPath")
    @Suppress("DEPRECATION")
    fun saveExcelFile(fileName: String, projectAssignProjectId: Int?, projectName: String?) {

        val mDialogView = LayoutInflater.from(this@MainActivity).inflate(R.layout.dialog_confirm_export, null)
        val mBuilder = AlertDialog.Builder(this@MainActivity)
        val mDialog = mBuilder.create()

        mDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val window = mDialog.window
        if (window != null) {
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            mDialog.window!!.setLayout(width, height)
        }
        mDialog.setView(mDialogView)

        mDialogView.btnExportCSV.setOnClickListener {
            isCsv = true
            isPdfile = false
            isJson = false
            mDialog.dismiss()
            createExcelFile(fileName, projectAssignProjectId, projectName)
        }

        mDialogView.btnExportExcel.setOnClickListener {
            isCsv = false
            isPdfile = false
            isJson = false
            mDialog.dismiss()
            createExcelFile(fileName, projectAssignProjectId, projectName)
        }

        mDialogView.btnExportPdf.visibility = View.GONE
        mDialogView.btnExportPdf.setOnClickListener {
            isCsv = false
            isPdfile = true
            mDialog.dismiss()
            convertDocToPdfitext(fileName, projectAssignProjectId, projectName)
        }

        mDialog.show()
    }

    private fun createExcelFile(fileName: String, projectAssignProjectId: Int?, projectName: String?) {
        onShowLoading()
        CreateFormatExcelTask().execute()
        Handler().postDelayed({
            val defaultFontNormal = wb!!.createFont()
            defaultFontNormal.fontHeight = (11.0 * 20).toShort()

            /* style */
            val cellStyleNomor = wb!!.createCellStyle()
            cellStyleNomor.wrapText = true
            cellStyleNomor.setFont(defaultFontNormal)
            // justify text alignment
            cellStyleNomor.setVerticalAlignment(VerticalAlignment.TOP)
            cellStyleNomor.setAlignment(HorizontalAlignment.CENTER)

            val cellStyleNormal = wb!!.createCellStyle()
            cellStyleNormal.wrapText = true
            cellStyleNormal.setFont(defaultFontNormal)
            // justify text alignment
            cellStyleNormal.setVerticalAlignment(VerticalAlignment.TOP)
            cellStyleNormal.setAlignment(HorizontalAlignment.LEFT)

            //judul
            /*val judul2 = ws.createRow(2)
            val celljudul2 = judul2.createCell(0)
            val celStylejudul2 = wb!!.createCellStyle()
            celStylejudul2.wrapText = true
            celljudul2.cellStyle = celStylejudul2
            CellUtil.setAlignment(celljudul2, HorizontalAlignment.CENTER)
            celljudul2.setCellValue(projectName)*/

            val defaultFontHeader2 = wb!!.createFont()
            defaultFontHeader2.fontHeight = (14.0 * 20).toShort()
            defaultFontHeader2.bold = true

            val cellStyleHeader2 = wb!!.createCellStyle()
            cellStyleHeader2.wrapText = true
            cellStyleHeader2.setFont(defaultFontHeader2)
            // justify text alignment
            cellStyleHeader2.setAlignment(HorizontalAlignment.CENTER)
            cellStyleHeader2.setVerticalAlignment(VerticalAlignment.CENTER)

            val rowheader2 = ws.createRow(3)
            rowheader2.height = 2 * 256
            val colheader2 = rowheader2.createCell(1)
            colheader2.cellStyle = cellStyleHeader2
            colheader2.setCellValue(projectName.toString().toUpperCase())
            ws.addMergedRegion(CellRangeAddress(3, 3, 1, 27))

            realm.executeTransactionAsync({ inrealm ->
                val results = inrealm.where(PartyAdd::class.java).equalTo("ProjectId", projectAssignProjectId).findAll()

                //old export
                if (results != null) {
                    for (element in 0 until results.size) {
                        createRow(currentLastRow)
                        val rowPemilikPertama = ws.getRow(oldLastRow)
                        //rowNamaTanaman.height = 2 * 256

                        val colNoPemilikPertama = rowPemilikPertama.createCell(1)
                        colNoPemilikPertama.cellStyle = cellStyleNomor
                        colNoPemilikPertama.setCellValue("${element + 1}")

                        val coldataPemilikPertama = rowPemilikPertama.createCell(2)
                        coldataPemilikPertama.cellStyle = cellStyleNormal

                        val nomordIdentitas: String? = if (results[element]?.partyIdentityNumber!!.isNotEmpty()) results[element]?.partyIdentityNumber else "-"

                        val tmptLahirPB: String? = if (results[element]?.partyBirthPlaceName!!.isNotEmpty()) results[element]?.partyBirthPlaceName else "-"
                        val tglLahirPB: String? = if (results[element]?.partyBirthDate!!.isNotEmpty()) results[element]?.partyBirthDate else "-"
                        val pekerjaan: String? = if (results[element]?.partyOccupationName!!.isNotEmpty()) results[element]?.partyOccupationName else "-"
                        val alamatJalan: String? = if (results[element]?.partyStreetName!!.isNotEmpty()) results[element]?.partyStreetName else "-"
                        val alamatNomorRumah: String? = if (results[element]?.partyNumber!!.isNotEmpty()) results[element]?.partyNumber else "-"
                        val alamatRT: String? = if (results[element]?.partyRT!!.isNotEmpty()) results[element]?.partyRT else "-"
                        val alamatRW: String? = if (results[element]?.partyRW!!.isNotEmpty()) results[element]?.partyRW else "-"
                        val alamatBlok: String? = if (results[element]?.partyBlock!!.isNotEmpty()) results[element]?.partyBlock else "-"
                        val alamatKelurahan: String? = if (results[element]?.partyVillageName!!.isNotEmpty()) results[element]?.partyVillageName else "-"
                        val alamatKecamatan: String? = if (results[element]?.partyDistrictName!!.isNotEmpty()) results[element]?.partyDistrictName else "-"
                        val alamatKotaKabupaten: String? = if (results[element]?.partyRegencyName!!.isNotEmpty()) results[element]?.partyRegencyName else "-"
                        val alamatProvinsi: String? = if (results[element]?.partyProvinceName!!.isNotEmpty()) results[element]?.partyProvinceName else "-"
                        val jenisKepemilikan: String? = if (results[element]?.partyOwnershipType!!.isNotEmpty()) results[element]?.partyOwnershipType?.replace("[","")?.replace("]","") else "-"

                        val pemilikPertama = """
                                a.  Nama                    :  ${results[element]?.partyFullName}
                                b.  Tanggal Lahir       :  $tmptLahirPB, $tglLahirPB
                                c.  Pekerjaan             :  $pekerjaan
                                d.  Alamat                  :  Jl.$alamatJalan No.$alamatNomorRumah Blok $alamatBlok RT $alamatRT RW $alamatRW $alamatKelurahan $alamatKecamatan $alamatKotaKabupaten $alamatProvinsi
                                e.  NIK/No.KTP          :  $nomordIdentitas
                                f.  Kepemilikan          :  $jenisKepemilikan
                                
                                

                            """.trimIndent()

                        coldataPemilikPertama.setCellValue(pemilikPertama)

                        //project Land
                        val pjland = inrealm.where(ProjectLand::class.java)
                                .equalTo("ProjectId", results[element]?.projectId)
                                .and()
                                .equalTo("subjectId", results[element]?.tempId)
                                .findAll()

                        if (pjland != null) {
                            lannumber = ""
                            letak = ""
                            landaffected = ""
                            sisaatas = ""
                            sisabawah = ""
                            kepemilikan = ""
                            tandabukti = ""
                            statusTanah = ""
                            jenisbangunan = ""
                            luasbangunan = ""
                            satuanbangunan = ""
                            jenistanaman = ""
                            k = ""
                            s = ""
                            b = ""
                            r = ""
                            jumlahtanaman = ""
                            satuantanaman = ""
                            fasilitasLain = ""
                            fasilitasValue = ""
                            landInfopiducia = ""
                            f1 = ""
                            f2 = ""
                            st1 = ""
                            st2 = ""
                            rf1 = ""
                            rf2 = ""
                            rc1 = ""
                            rc2 = ""
                            cl1 = ""
                            cl2 = ""
                            dw1 = ""
                            dw2 = ""
                            fl1 = ""
                            fl2 = ""
                            wl1 = ""
                            wl2 = ""
                            menguasai = pemilikPertama

                            for (elementTanah in 0 until pjland.size) {
                                val letaktanahRT = if (pjland[elementTanah]?.landRT!!.isNotEmpty()) pjland[elementTanah]?.landRT else "-"
                                val letaktanahRW = if (pjland[elementTanah]?.landRW!!.isNotEmpty()) pjland[elementTanah]?.landRW else "-"
                                val letaktanahKelurahan = if (pjland[elementTanah]?.landVillageName!!.isNotEmpty()) pjland[elementTanah]?.landVillageName else "-"
                                val letaktanahKecamatan = if (pjland[elementTanah]?.landDistrictName!!.isNotEmpty()) pjland[elementTanah]?.landDistrictName else "-"
                                val letaktanahKota = if (pjland[elementTanah]?.landRegencyName!!.isNotEmpty()) pjland[elementTanah]?.landRegencyName else "-"
                                val letaktanahProvinsi = if (pjland[elementTanah]?.landProvinceName!!.isNotEmpty()) pjland[elementTanah]?.landProvinceName else "-"

                                val colnoBidangTanah = rowPemilikPertama.createCell(4)
                                colnoBidangTanah.cellStyle = cellStyleNomor

                                lannumber += """
                                        ${pjland[elementTanah]?.landNumberList}
                                        
                                        
                                        
                                        
                                        
                                        

                                    """.trimIndent()
                                colnoBidangTanah.setCellValue(lannumber)

                                val colletakTanah = rowPemilikPertama.createCell(5)
                                colletakTanah.cellStyle = cellStyleNormal

                                letak += """
                                        a.  RT/RW              :  $letaktanahRT/$letaktanahRW
                                        b.  Kelurahan        :  $letaktanahKelurahan
                                        c.  Kecamatan       :  $letaktanahKecamatan
                                        d.  Kota                  :  $letaktanahKota
                                        e.  Provinsi            :  $letaktanahProvinsi
                                        
                                        

                                    """.trimIndent()
                                colletakTanah.setCellValue(letak)

                                /* luas tanah*/
                                val colluas = rowPemilikPertama.createCell(6)
                                colluas.cellStyle = cellStyleNomor
                                landaffected += """
                                         ${pjland[elementTanah]?.landAreaCertificate}
                                        
                                        
                                        
                                        
                                        
                                        

                                    """.trimIndent()

                                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 6, 8))
                                colluas.setCellValue(landaffected)

                                /* penggunaan tanah */
                                val convertLandUtilizId = pjland[elementTanah]?.landUtilizationLandUtilizationId
                                val convertLandUtilizId2 = convertLandUtilizId!!.replace("[", "['")
                                val convertLandUtilizId3 = convertLandUtilizId2.replace("]", "']")
                                val convertLandUtilizId4 = convertLandUtilizId3.replace(", ", "','")

                                val convertLandUtilizName = pjland[elementTanah]?.landUtilizationLandUtilizationName
                                val convertLandUtilizName2 = convertLandUtilizName!!.replace("[", "['")
                                val convertLandUtilizName3 = convertLandUtilizName2.replace("]", "']")
                                val convertLandUtilizName4 = convertLandUtilizName3.replace(", ", "','")

                                val convertLandUtilizNameOther = pjland[elementTanah]?.landUtilizationLandUtilizationName
                                val convertLandUtilizNameOther2 = convertLandUtilizNameOther!!.replace("[", "['")
                                val convertLandUtilizNameOther3 = convertLandUtilizNameOther2.replace("]", "']")
                                val convertLandUtilizNameOther4 = convertLandUtilizNameOther3.replace(", ", "','")


                                /* kepemilikan tanah */
                                val colkepemilikanTanah = rowPemilikPertama.createCell(10)
                                colkepemilikanTanah.cellStyle = cellStyleNormal

                                kepemilikan += """
                                         ${pjland[elementTanah]?.landAlasHakTanah}
                                        
                                        
                                        
                                        

                                    """.trimIndent()
                                colkepemilikanTanah.setCellValue(kepemilikan)

                                val jenisKepemiikan: String? = if (pjland[elementTanah]?.landOwnershipNameOther!!.isNotEmpty()) {
                                    pjland[elementTanah]?.landOwnershipNameOther
                                } else {
                                    pjland[elementTanah]?.landOwnershipName
                                }

                                val colStatusTanah = rowPemilikPertama.createCell(9)
                                colStatusTanah.cellStyle = cellStyleNormal
                                statusTanah += """
                                        $jenisKepemiikan
                                        
                                        
                                        
                                        
                                        
                                        

                                    """.trimIndent()
                                colStatusTanah.setCellValue(statusTanah)


                                /* informasi tambahan tanah */
                                val convertLandInfoId = pjland[elementTanah]?.landAddInfoAddInfoId
                                val convertLandInfoId2 = convertLandInfoId!!.replace("[", "['")
                                val convertLandInfoId3 = convertLandInfoId2.replace("]", "']")
                                val convertLandInfoId4 = convertLandInfoId3.replace(", ", "','")

                                val convertLandInfoName = pjland[elementTanah]?.landAddInfoAddInfoName
                                val convertLandInfoName2 = convertLandInfoName!!.replace("[", "['")
                                val convertLandInfoName3 = convertLandInfoName2.replace("]", "']")
                                val convertLandInfoName4 = convertLandInfoName3.replace(", ", "','")

                                val dataLandInfoId = Gson().fromJson(convertLandInfoId4, Array<String>::class.java).toList()
                                val dataLandInfoName = Gson().fromJson(convertLandInfoName4, Array<String>::class.java).toList()

                                val colInfoTanah = rowPemilikPertama.createCell(25)
                                colInfoTanah.cellStyle = cellStyleNormal


                                val colDampak = rowPemilikPertama.createCell(26)
                                colDampak.cellStyle = cellStyleNormal
                                colDampak.setCellValue(pjland[elementTanah]?.dampak)

                                var landInfoName = ""
                                var mlandInfoName = ""

                                if (dataLandInfoId.isNotEmpty()) {
                                    for (indexLandInfo in dataLandInfoId.indices) {
                                        val landinfoname = dataLandInfoName[indexLandInfo]
                                        /*landInfoName +=
                                                """
                                                    - $landinfoname
                                                    
                                                    
                                                """.trimIndent()*/

                                        mlandInfoName = """
                                                            - $landinfoname
                                                            
                                                        """.trimIndent()


                                        landInfoName += mlandInfoName
                                    }
                                }

                                landInfopiducia += """
                                        $landInfoName
                                        
                                        
                                        
                                        
                                        
                                        

                                    """.trimIndent()
                                colInfoTanah.setCellValue(landInfopiducia)

                                /* bangunan */
                                val bangunan = inrealm.where(Bangunan::class.java)
                                    .equalTo("ProjectId", pjland[elementTanah]?.projectId)
                                    .and()
                                    .equalTo("LandIdTemp", pjland[elementTanah]?.areaId)
                                        .findAll()
                                if (bangunan != null) {
                                    for (indexBangunan in 0 until bangunan.size) {
                                        val coljenisBangunanTanah = rowPemilikPertama.createCell(13)
                                        coljenisBangunanTanah.cellStyle = cellStyleNormal

                                        /*spesifikasi bangunan*/
                                        //pondasi
                                        val convertPondasiBangunanId = bangunan[indexBangunan]?.foundationTypeId
                                        val convertPondasiBangunanId2 = convertPondasiBangunanId!!.replace("[", "['")
                                        val convertPondasiBangunanId3 = convertPondasiBangunanId2.replace("]", "']")
                                        val convertPondasiBangunanId4 = convertPondasiBangunanId3.replace(", ", "','")

                                        val convertPondasiBangunanName = bangunan[indexBangunan]?.foundationTypeName
                                        val convertPondasiBangunanName2 = convertPondasiBangunanName!!.replace("[", "['")
                                        val convertPondasiBangunanName3 = convertPondasiBangunanName2.replace("]", "']")
                                        val convertPondasiBangunanName4 = convertPondasiBangunanName3.replace(", ", "','")

                                        val convertPondasiBangunanNameOther = bangunan[indexBangunan]?.foundationTypeNameOther
                                        val convertPondasiBangunanNameOther2 = convertPondasiBangunanNameOther!!.replace("[", "['")
                                        val convertPondasiBangunanNameOther3 = convertPondasiBangunanNameOther2.replace("]", "']")
                                        val convertPondasiBangunanNameOther4 = convertPondasiBangunanNameOther3.replace(", ", "','")

                                        val convertStrukturPondasiVolume = bangunan[indexBangunan]?.pbFoundationTypeAreaTotal
                                        val convertStrukturPondasiVolume2 = convertStrukturPondasiVolume!!.replace("[", "['")
                                        val convertStrukturPondasiVolume3 = convertStrukturPondasiVolume2.replace("]", "']")
                                        val convertStrukturPondasiVolume4 = convertStrukturPondasiVolume3.replace(", ", "','")

                                        val dataPondasiId = Gson().fromJson(convertPondasiBangunanId4, Array<String>::class.java).toList()
                                        val dataPondasiName = Gson().fromJson(convertPondasiBangunanName4, Array<String>::class.java).toList()
                                        val dataPondasiNameOther = Gson().fromJson(convertPondasiBangunanNameOther4, Array<String>::class.java).toList()
                                        val dataPondasiVolume = Gson().fromJson(convertStrukturPondasiVolume4, Array<String>::class.java).toList()

                                        /*struktur*/
                                        val convertStrukturBangunanId = bangunan[indexBangunan]?.structureTypeId
                                        val convertStrukturBangunanId2 = convertStrukturBangunanId!!.replace("[", "['")
                                        val convertStrukturBangunanId3 = convertStrukturBangunanId2.replace("]", "']")
                                        val convertStrukturBangunanId4 = convertStrukturBangunanId3.replace(", ", "','")

                                        val convertStrukturBangunanName = bangunan[indexBangunan]?.structureTypeName
                                        val convertStrukturBangunanName2 = convertStrukturBangunanName!!.replace("[", "['")
                                        val convertStrukturBangunanName3 = convertStrukturBangunanName2.replace("]", "']")
                                        val convertStrukturBangunanName4 = convertStrukturBangunanName3.replace(", ", "','")

                                        val convertStrukturBangunanNameOther = bangunan[indexBangunan]?.structureTypeNameOther
                                        val convertStrukturBangunanNameOther2 = convertStrukturBangunanNameOther!!.replace("[", "['")
                                        val convertStrukturBangunanNameOther3 = convertStrukturBangunanNameOther2.replace("]", "']")
                                        val convertStrukturBangunanNameOther4 = convertStrukturBangunanNameOther3.replace(", ", "','")

                                        val convertStrukturBangunanVolume = bangunan[indexBangunan]?.pbStructureTypeAreaTotal
                                        val convertStrukturBangunanVolume2 = convertStrukturBangunanVolume!!.replace("[", "['")
                                        val convertStrukturBangunanVolume3 = convertStrukturBangunanVolume2.replace("]", "']")
                                        val convertStrukturBangunanVolume4 = convertStrukturBangunanVolume3.replace(", ", "','")

                                        val dataStrukturId = Gson().fromJson(convertStrukturBangunanId4, Array<String>::class.java).toList()
                                        val dataStrukturName = Gson().fromJson(convertStrukturBangunanName4, Array<String>::class.java).toList()
                                        val dataStrukturNameOther = Gson().fromJson(convertStrukturBangunanNameOther4, Array<String>::class.java).toList()
                                        val dataStrukturVolume = Gson().fromJson(convertStrukturBangunanVolume4, Array<String>::class.java).toList()


                                        /*kerangka atap */
                                        val convertKeratapBangunanId = bangunan[indexBangunan]?.roofFrameTypeId
                                        val convertKeratapBangunanId2 = convertKeratapBangunanId!!.replace("[", "['")
                                        val convertKeratapBangunanId3 = convertKeratapBangunanId2.replace("]", "']")
                                        val convertKeratapBangunanId4 = convertKeratapBangunanId3.replace(", ", "','")

                                        val convertKeratapBangunanName = bangunan[indexBangunan]?.roofFrameTypeName
                                        val convertKeratapBangunanName2 = convertKeratapBangunanName!!.replace("[", "['")
                                        val convertKeratapBangunanName3 = convertKeratapBangunanName2.replace("]", "']")
                                        val convertKeratapBangunanName4 = convertKeratapBangunanName3.replace(", ", "','")

                                        val convertKeratapBangunanNameOther = bangunan[indexBangunan]?.roofFrameTypeNameOther
                                        val convertKeratapBangunanNameOther2 = convertKeratapBangunanNameOther!!.replace("[", "['")
                                        val convertKeratapBangunanNameOther3 = convertKeratapBangunanNameOther2.replace("]", "']")
                                        val convertKeratapBangunanNameOther4 = convertKeratapBangunanNameOther3.replace(", ", "','")

                                        val convertKeratapBangunanVolume = bangunan[indexBangunan]?.pbRoofFrameTypeAreaTotal
                                        val convertKeratapBangunanVolume2 = convertKeratapBangunanVolume!!.replace("[", "['")
                                        val convertKeratapBangunanVolume3 = convertKeratapBangunanVolume2.replace("]", "']")
                                        val convertKeratapBangunanVolume4 = convertKeratapBangunanVolume3.replace(", ", "','")

                                        val dataKeratapId = Gson().fromJson(convertKeratapBangunanId4, Array<String>::class.java).toList()
                                        val dataKeratapName = Gson().fromJson(convertKeratapBangunanName4, Array<String>::class.java).toList()
                                        val dataKeratapNameOther = Gson().fromJson(convertKeratapBangunanNameOther4, Array<String>::class.java).toList()
                                        val dataKeratapVolume = Gson().fromJson(convertKeratapBangunanVolume4, Array<String>::class.java).toList()


                                        /*penutup atap */
                                        val convertPenutapBangunanId = bangunan[indexBangunan]?.roofCoveringTypeId
                                        val convertPenutapBangunanId2 = convertPenutapBangunanId!!.replace("[", "['")
                                        val convertPenutapBangunanId3 = convertPenutapBangunanId2.replace("]", "']")
                                        val convertPenutapBangunanId4 = convertPenutapBangunanId3.replace(", ", "','")

                                        val convertPenutapBangunanName = bangunan[indexBangunan]?.roofCoveringTypeName
                                        val convertPenutapBangunanNam2 = convertPenutapBangunanName!!.replace("[", "['")
                                        val convertPenutapBangunanNam3 = convertPenutapBangunanNam2.replace("]", "']")
                                        val convertPenutapBangunanNam4 = convertPenutapBangunanNam3.replace(", ", "','")

                                        val convertPenutapBangunanNameOther = bangunan[indexBangunan]?.roofCoveringTypeNameOther
                                        val convertPenutapBangunanNameOther2 = convertPenutapBangunanNameOther!!.replace("[", "['")
                                        val convertPenutapBangunanNameOther3 = convertPenutapBangunanNameOther2.replace("]", "']")
                                        val convertPenutapBangunanNameOther4 = convertPenutapBangunanNameOther3.replace(", ", "','")

                                        val convertPenutapBangunanVolume = bangunan[indexBangunan]?.pbRoofCoveringTypeAreaTotal
                                        val convertPenutapBangunanVolum2 = convertPenutapBangunanVolume!!.replace("[", "['")
                                        val convertPenutapBangunanVolum3 = convertPenutapBangunanVolum2.replace("]", "']")
                                        val convertPenutapBangunanVolum4 = convertPenutapBangunanVolum3.replace(", ", "','")

                                        val dataPenutapId = Gson().fromJson(convertPenutapBangunanId4, Array<String>::class.java).toList()
                                        val dataPenutapName = Gson().fromJson(convertPenutapBangunanNam4, Array<String>::class.java).toList()
                                        val dataPenutapNameOther = Gson().fromJson(convertPenutapBangunanNameOther4, Array<String>::class.java).toList()
                                        val dataPenutapVolume = Gson().fromJson(convertPenutapBangunanVolum4, Array<String>::class.java).toList()

                                        /* plafon */
                                        val convertPlafonBangunanId = bangunan[indexBangunan]?.ceilingTypeId
                                        val convertPlafonBangunanId2 = convertPlafonBangunanId!!.replace("[", "['")
                                        val convertPlafonBangunanId3 = convertPlafonBangunanId2.replace("]", "']")
                                        val convertPlafonBangunanId4 = convertPlafonBangunanId3.replace(", ", "','")

                                        val convertPlafonBangunanName = bangunan[indexBangunan]?.ceilingTypeName
                                        val convertPlafonBangunanName2 = convertPlafonBangunanName!!.replace("[", "['")
                                        val convertPlafonBangunanName3 = convertPlafonBangunanName2.replace("]", "']")
                                        val convertPlafonBangunanName4 = convertPlafonBangunanName3.replace(", ", "','")

                                        val convertPlafonBangunanNameOther = bangunan[indexBangunan]?.ceilingTypeNameOther
                                        val convertPlafonBangunanNameOther3 = convertPlafonBangunanNameOther!!.replace("[", "['")
                                        val convertPlafonBangunanNameOther4 = convertPlafonBangunanNameOther3.replace("]", "']")
                                        val convertPlafonBangunanNameOther5 = convertPlafonBangunanNameOther4.replace(", ", "','")

                                        val convertPlafonBangunanVolume = bangunan[indexBangunan]?.pbCeilingTypeAreaTotal
                                        val convertPlafonBangunanVolume2 = convertPlafonBangunanVolume!!.replace("[", "['")
                                        val convertPlafonBangunanVolume3 = convertPlafonBangunanVolume2.replace("]", "']")
                                        val convertPlafonBangunanVolume4 = convertPlafonBangunanVolume3.replace(", ", "','")

                                        val dataPlafonId = Gson().fromJson(convertPlafonBangunanId4, Array<String>::class.java).toList()
                                        val dataPlafonName = Gson().fromJson(convertPlafonBangunanName4, Array<String>::class.java).toList()
                                        val dataPlafonNameOther = Gson().fromJson(convertPlafonBangunanNameOther5, Array<String>::class.java).toList()
                                        val dataPlafonVolume = Gson().fromJson(convertPlafonBangunanVolume4, Array<String>::class.java).toList()

                                        /* Dinding */
                                        val convertDindingBangunanId = bangunan[indexBangunan]?.wallTypeId
                                        val convertDindingBangunanId2 = convertDindingBangunanId!!.replace("[", "['")
                                        val convertDindingBangunanId3 = convertDindingBangunanId2.replace("]", "']")
                                        val convertDindingBangunanId4 = convertDindingBangunanId3.replace(", ", "','")

                                        val convertDindingBangunanName = bangunan[indexBangunan]?.wallTypeName
                                        val convertDindingBangunanName2 = convertDindingBangunanName!!.replace("[", "['")
                                        val convertDindingBangunanName3 = convertDindingBangunanName2.replace("]", "']")
                                        val convertDindingBangunanName4 = convertDindingBangunanName3.replace(", ", "','")

                                        val convertDindingBangunanNameOther = bangunan[indexBangunan]?.wallTypeNameOther
                                        val convertDindingBangunanNameOther2 = convertDindingBangunanNameOther!!.replace("[", "['")
                                        val convertDindingBangunanNameOther3 = convertDindingBangunanNameOther2.replace("]", "']")
                                        val convertDindingBangunanNameOther4 = convertDindingBangunanNameOther3.replace(", ", "','")

                                        val convertDindingBangunanVolume = bangunan[indexBangunan]?.pbfWallTypeAreaTotal
                                        val convertDindingBangunanVolume2 = convertDindingBangunanVolume!!.replace("[", "['")
                                        val convertDindingBangunanVolume3 = convertDindingBangunanVolume2.replace("]", "']")
                                        val convertDindingBangunanVolume4 = convertDindingBangunanVolume3.replace(", ", "','")

                                        val dataDindingId = Gson().fromJson(convertDindingBangunanId4, Array<String>::class.java).toList()
                                        val dataDindingName = Gson().fromJson(convertDindingBangunanName4, Array<String>::class.java).toList()
                                        val dataDindingNameOther = Gson().fromJson(convertDindingBangunanNameOther4, Array<String>::class.java).toList()
                                        val dataDindingVolume = Gson().fromJson(convertDindingBangunanVolume4, Array<String>::class.java).toList()

                                        /* Pintu jendela */
                                        val convertPinjelBangunanId = bangunan[indexBangunan]?.doorWindowTypeld
                                        val convertPinjelBangunanId2 = convertPinjelBangunanId!!.replace("[", "['")
                                        val convertPinjelBangunanId3 = convertPinjelBangunanId2.replace("]", "']")
                                        val convertPinjelBangunanId4 = convertPinjelBangunanId3.replace(", ", "','")

                                        val convertPinjelBangunanName = bangunan[indexBangunan]?.doorWindowTypeName
                                        val convertPinjelBangunanName2 = convertPinjelBangunanName!!.replace("[", "['")
                                        val convertPinjelBangunanName3 = convertPinjelBangunanName2.replace("]", "']")
                                        val convertPinjelBangunanName4 = convertPinjelBangunanName3.replace(", ", "','")

                                        val convertPinjelBangunanNameOther = bangunan[indexBangunan]?.doorWindowTypeNameOther
                                        val convertPinjelBangunanNameOther2 = convertPinjelBangunanNameOther!!.replace("[", "['")
                                        val convertPinjelBangunanNameOther3 = convertPinjelBangunanNameOther2.replace("]", "']")
                                        val convertPinjelBangunanNameOther4 = convertPinjelBangunanNameOther3.replace(", ", "','")

                                        val convertPinjelBangunanVolume = bangunan[indexBangunan]?.pbDoorWindowTypeAreaTotal
                                        val convertPinjelBangunanVolume2 = convertPinjelBangunanVolume!!.replace("[", "['")
                                        val convertPinjelBangunanVolume3 = convertPinjelBangunanVolume2.replace("]", "']")
                                        val convertPinjelBangunanVolume4 = convertPinjelBangunanVolume3.replace(", ", "','")

                                        val dataPinjelId = Gson().fromJson(convertPinjelBangunanId4, Array<String>::class.java).toList()
                                        val dataPinjelName = Gson().fromJson(convertPinjelBangunanName4, Array<String>::class.java).toList()
                                        val dataPinjelNameOther = Gson().fromJson(convertPinjelBangunanNameOther4, Array<String>::class.java).toList()
                                        val dataPinjelVolume = Gson().fromJson(convertPinjelBangunanVolume4, Array<String>::class.java).toList()


                                        /* Lantai */
                                        val convertLantaiBangunanId = bangunan[indexBangunan]?.floorTypeId
                                        val convertLantaiBangunanId2 = convertLantaiBangunanId!!.replace("[", "['")
                                        val convertLantaiBangunanId3 = convertLantaiBangunanId2.replace("]", "']")
                                        val convertLantaiBangunanId4 = convertLantaiBangunanId3.replace(", ", "','")

                                        val convertLantaiBangunanName = bangunan[indexBangunan]?.floorTypeName
                                        val convertLantaiBangunanName2 = convertLantaiBangunanName!!.replace("[", "['")
                                        val convertLantaiBangunanName3 = convertLantaiBangunanName2.replace("]", "']")
                                        val convertLantaiBangunanName4 = convertLantaiBangunanName3.replace(", ", "','")

                                        val convertLantaiBangunanNameOther = bangunan[indexBangunan]?.floorTypeNameOther
                                        val convertLantaiBangunanNameOther2 = convertLantaiBangunanNameOther!!.replace("[", "['")
                                        val convertLantaiBangunanNameOther3 = convertLantaiBangunanNameOther2.replace("]", "']")
                                        val convertLantaiBangunanNameOther4 = convertLantaiBangunanNameOther3.replace(", ", "','")

                                        val convertLantaiBangunanVolume = bangunan[indexBangunan]?.pbFloorTypeAreaTotal
                                        val convertLantaiBangunanVolume2 = convertLantaiBangunanVolume!!.replace("[", "['")
                                        val convertLantaiBangunanVolume3 = convertLantaiBangunanVolume2.replace("]", "']")
                                        val convertLantaiBangunanVolume4 = convertLantaiBangunanVolume3.replace(", ", "','")

                                        val dataLantaiId = Gson().fromJson(convertLantaiBangunanId4, Array<String>::class.java).toList()
                                        val dataLantaiName = Gson().fromJson(convertLantaiBangunanName4, Array<String>::class.java).toList()
                                        val dataLantaiNameOther = Gson().fromJson(convertLantaiBangunanNameOther4, Array<String>::class.java).toList()
                                        val dataLantaiVolume = Gson().fromJson(convertLantaiBangunanVolume4, Array<String>::class.java).toList()

                                        var pondasiName = ""
                                        var pondasiVolume = ""
                                        var mpondasiVolume = ""
                                        var satuanPondasiBangunan = ""
                                        var mSatuanPondasiBangunan = ""
                                        if (dataPondasiId.isNotEmpty()) {
                                            for (indexPondasi in dataPondasiId.indices) {
                                                f1 = if (dataPondasiNameOther[indexPondasi].isNotEmpty()) {
                                                    """
                                                            - ${dataPondasiNameOther[indexPondasi]}
                                                            
                                                        """.trimIndent()
                                                } else {
                                                    """
                                                            - ${dataPondasiName[indexPondasi]}
                                                            
                                                        """.trimIndent()
                                                }

                                                if (dataPondasiVolume[indexPondasi].isNotEmpty()) {
                                                    mpondasiVolume =
                                                            """
                                                            ${dataPondasiVolume[indexPondasi]}
                                                            
                                                        """.trimIndent()

                                                    mSatuanPondasiBangunan =
                                                            """
                                                            $satuamLuas
                                                            
                                                        """.trimIndent()
                                                }


                                                pondasiName += """
                                                        |$f1
                                                    """.trimMargin()

                                                pondasiVolume += """
                                                        |$mpondasiVolume
                                                    """.trimMargin()

                                                satuanPondasiBangunan += """
                                                        |$mSatuanPondasiBangunan
                                                    """.trimMargin()
                                            }
                                        }//looping data pondasi

                                        var strukturName = ""
                                        var strukturVolume = ""
                                        var mstuktureVolume = ""
                                        var satuanStrukturBangunan = ""
                                        var mSatuanStrukturBangunan = ""
                                        if (dataStrukturId.isNotEmpty()) {
                                            for (indexStruktur in dataStrukturId.indices) {
                                                st1 = if (dataStrukturNameOther[indexStruktur].isNotEmpty()) {
                                                    """
                                                            - ${dataStrukturNameOther[indexStruktur]}
                                                            
                                                        """.trimIndent()
                                                } else {
                                                    """
                                                            - ${dataStrukturName[indexStruktur]}
                                                            
                                                        """.trimIndent()
                                                }
                                                if (dataStrukturVolume[indexStruktur].isNotEmpty()) {
                                                    mstuktureVolume =
                                                            """
                                                            ${dataStrukturVolume[indexStruktur]}
                                                            
                                                        """.trimIndent()

                                                    mSatuanStrukturBangunan =
                                                            """
                                                            $satuamLuas
                                                            
                                                        """.trimIndent()
                                                }


                                                strukturName += """
                                                        |$st1
                                                    """.trimMargin()

                                                strukturVolume += """
                                                        |$mstuktureVolume
                                                    """.trimMargin()
                                                satuanStrukturBangunan += """
                                                        |$mSatuanStrukturBangunan
                                                    """.trimMargin()
                                            }
                                        }//looping data struktur

                                        var kerangkaAtapName = ""
                                        var kerangkaAtapVolume = ""
                                        var mkerangkaAtapVolume = ""
                                        var satuanKertapBangunan = ""
                                        var mSatuanKertapBangunan = ""
                                        if (dataKeratapId.isNotEmpty()) {
                                            for (indexKertap in dataKeratapId.indices) {
                                                rf1 = if (dataKeratapNameOther[indexKertap].isNotEmpty()) {
                                                    """
                                                            - ${dataKeratapNameOther[indexKertap]}
                                                            
                                                        """.trimIndent()
                                                } else {
                                                    """
                                                            - ${dataKeratapName[indexKertap]}
                                                            
                                                        """.trimIndent()
                                                }
                                                if (dataKeratapVolume[indexKertap].isNotEmpty()) {
                                                    mkerangkaAtapVolume =
                                                            """
                                                            ${dataKeratapVolume[indexKertap]}
                                                            
                                                        """.trimIndent()

                                                    mSatuanKertapBangunan =
                                                            """
                                                            $satuamLuas
                                                            
                                                        """.trimIndent()
                                                }


                                                kerangkaAtapName += """
                                                        |$rf1
                                                    """.trimMargin()

                                                kerangkaAtapVolume += """
                                                        |$mkerangkaAtapVolume
                                                    """.trimMargin()

                                                satuanKertapBangunan += """
                                                        |$mSatuanKertapBangunan
                                                    """.trimMargin()
                                            }
                                        }//looping data kerangka atap

                                        var penutupAtapName = ""
                                        var penutupAtapVolume = ""
                                        var mpenutupAtapVolume = ""
                                        var satuanPetapBangunan = ""
                                        var mSatuanPetapBangunan = ""
                                        if (dataPenutapId.isNotEmpty()) {
                                            for (indexPenutupAtap in dataPenutapId.indices) {
                                                rc1 = if (dataPenutapNameOther[indexPenutupAtap].isNotEmpty()) {
                                                    """
                                                            - ${dataPenutapNameOther[indexPenutupAtap]}
                                                            
                                                        """.trimIndent()
                                                } else {
                                                    """
                                                            - ${dataPenutapName[indexPenutupAtap]}
                                                            
                                                        """.trimIndent()
                                                }
                                                if (dataPenutapVolume[indexPenutupAtap].isNotEmpty()) {
                                                    mpenutupAtapVolume =
                                                            """
                                                            ${dataPenutapVolume[indexPenutupAtap]}
                                                            
                                                        """.trimIndent()

                                                    mSatuanPetapBangunan =
                                                            """
                                                            $satuamLuas
                                                            
                                                        """.trimIndent()
                                                }


                                                penutupAtapName += """
                                                        |$rc1
                                                    """.trimMargin()

                                                penutupAtapVolume += """
                                                        |$mpenutupAtapVolume
                                                    """.trimMargin()

                                                satuanPetapBangunan += """
                                                        |$mSatuanPetapBangunan
                                                    """.trimMargin()
                                            }
                                        }//looping data penutup atap

                                        var plafonName = ""
                                        var plafonVolume = ""
                                        var mplafonVolume = ""
                                        var satuanPlafonBangunan = ""
                                        var mSatuanPlafonBangunan = ""
                                        if (dataPlafonId.isNotEmpty()) {
                                            for (indexPlafon in dataPlafonId.indices) {
                                                cl1 = if (dataPlafonNameOther[indexPlafon].isNotEmpty()) {
                                                    """
                                                            - ${dataPlafonNameOther[indexPlafon]}
                                                            
                                                        """.trimIndent()
                                                } else {
                                                    """
                                                            - ${dataPlafonName[indexPlafon]}
                                                            
                                                        """.trimIndent()
                                                }

                                                if (dataPlafonVolume[indexPlafon].isNotEmpty()) {
                                                    mplafonVolume =
                                                            """
                                                            ${dataPlafonVolume[indexPlafon]}
                                                            
                                                        """.trimIndent()

                                                    mSatuanPlafonBangunan =
                                                            """
                                                            $satuamLuas
                                                            
                                                        """.trimIndent()
                                                }


                                                plafonName += """
                                                        |$cl1
                                                    """.trimMargin()

                                                plafonVolume += """
                                                        |$mplafonVolume
                                                    """.trimMargin()

                                                satuanPlafonBangunan += """
                                                        |$mSatuanPlafonBangunan
                                                    """.trimMargin()
                                            }
                                        }//looping data plafon

                                        var dindingName = ""
                                        var dindingVolume = ""
                                        var mdindingVolume = ""
                                        var satuanDindingBangunan = ""
                                        var mSatuanDindingBangunan = ""
                                        if (dataDindingId.isNotEmpty()) {
                                            for (indexDinding in dataDindingId.indices) {
                                                wl1 = if (dataDindingNameOther[indexDinding].isNotEmpty()) {
                                                    """
                                                            - ${dataDindingNameOther[indexDinding]}
                                                            
                                                        """.trimIndent()
                                                } else {
                                                    """
                                                            - ${dataDindingName[indexDinding]}
                                                            
                                                        """.trimIndent()
                                                }
                                                if (dataDindingVolume[indexDinding].isNotEmpty()) {
                                                    mdindingVolume =
                                                            """
                                                            ${dataDindingVolume[indexDinding]}
                                                            
                                                        """.trimIndent()

                                                    mSatuanDindingBangunan =
                                                            """
                                                            $satuamLuas
                                                            
                                                        """.trimIndent()
                                                }


                                                dindingName += """
                                                        |$wl1
                                                    """.trimMargin()

                                                dindingVolume += """
                                                        |$mdindingVolume
                                                    """.trimMargin()

                                                satuanDindingBangunan += """
                                                        |$mSatuanDindingBangunan
                                                    """.trimMargin()
                                            }
                                        }//looping data dinding

                                        var pinjenName = ""
                                        var pinjenVolume = ""
                                        var mpinjenVolume = ""
                                        var satuanPinjelBangunan = ""
                                        var mSatuanPinjelBangunan = ""
                                        if (dataPinjelId.isNotEmpty()) {
                                            for (indexPinJen in dataPinjelId.indices) {
                                                dw1 = if (dataPinjelNameOther[indexPinJen].isNotEmpty()) {
                                                    """
                                                            - ${dataPinjelNameOther[indexPinJen]}
                                                            
                                                        """.trimIndent()
                                                } else {
                                                    """
                                                            - ${dataPinjelName[indexPinJen]}
                                                            
                                                        """.trimIndent()
                                                }

                                                if (dataPinjelVolume[indexPinJen].isNotEmpty()) {
                                                    mpinjenVolume =
                                                            """
                                                            ${dataPinjelVolume[indexPinJen]}
                                                            
                                                        """.trimIndent()

                                                    mSatuanPinjelBangunan =
                                                            """
                                                            $satuamLuas
                                                            
                                                        """.trimIndent()
                                                }


                                                pinjenName += """
                                                        |$dw1
                                                    """.trimMargin()

                                                pinjenVolume += """
                                                        |$mpinjenVolume
                                                    """.trimMargin()

                                                satuanPinjelBangunan += """
                                                        |$mSatuanPinjelBangunan
                                                    """.trimMargin()
                                            }
                                        }//looping data pintu & jendela

                                        var lantaiName = ""
                                        var lantaiVolume = ""
                                        var mlantaiVolume = ""
                                        var satuanlantaiBangunan = ""
                                        var mSatuanLantaiBangunan = ""
                                        if (dataLantaiId.isNotEmpty()) {
                                            for (indexLantai in dataLantaiId.indices) {
                                                fl1 = if (dataLantaiNameOther[indexLantai].isNotEmpty()) {
                                                    """
                                                            - ${dataLantaiNameOther[indexLantai]}
                                                            
                                                        """.trimIndent()
                                                } else {
                                                    """
                                                            - ${dataLantaiName[indexLantai]}
                                                            
                                                        """.trimIndent()
                                                }
                                                if (dataLantaiVolume[indexLantai].isNotEmpty()) {
                                                    mlantaiVolume =
                                                            """
                                                            ${dataLantaiVolume[indexLantai]}
                                                            
                                                        """.trimIndent()

                                                    mSatuanLantaiBangunan =
                                                            """
                                                            $satuamLuas
                                                            
                                                        """.trimIndent()
                                                }


                                                lantaiName += """
                                                        |$fl1
                                                    """.trimMargin()

                                                lantaiVolume += """
                                                        |$mlantaiVolume
                                                    """.trimMargin()

                                                satuanlantaiBangunan += """
                                                        |$mSatuanLantaiBangunan
                                                    """.trimMargin()
                                            }
                                        }//looping data lantai

                                        val namaBangunan = if (bangunan[indexBangunan]?.projectBuildingTypeNameOther!!.toString().isNotEmpty()) {
                                            bangunan[indexBangunan]?.projectBuildingTypeNameOther.toString()
                                        } else {
                                            bangunan[indexBangunan]?.projectBuildingTypeName.toString()
                                        }

                                        val convertNamaBangunanToBold = Html.fromHtml("<b> $namaBangunan </b>").toString()

                                        //Log.d("jir", lantaiName)
                                        jenisbangunan += """
                                                |${if(convertNamaBangunanToBold.isEmpty() || convertNamaBangunanToBold == "-"){""} else {convertNamaBangunanToBold}}
                                                |${if(pondasiName.isEmpty() || pondasiName == "-"){""} else {pondasiName}}
                                                |${if(strukturName.isEmpty() || strukturName == "-"){""} else {strukturName}}
                                                |${if(kerangkaAtapName.isEmpty() || kerangkaAtapName == "-"){""} else {kerangkaAtapName}}
                                                |${if(penutupAtapName.isEmpty() || penutupAtapName == "-"){""} else {penutupAtapName}}
                                                |${if(plafonName.isEmpty() || plafonName == "-"){""} else {plafonName}}
                                                |${if(dindingName.isEmpty() || dindingName == "-"){""} else {dindingName}}
                                                |${if(pinjenName.isEmpty() || pinjenName == "-"){""} else {pinjenName}}
                                                |${if(lantaiName.isEmpty() || lantaiName == "-"){""} else {lantaiName}}
                                                
                                                
                                            """.trimMargin()
                                        coljenisBangunanTanah.setCellValue(jenisbangunan)


                                        /* total lantai dan jumlah area spesfikasi */
                                        val coljumlahBangunanTanah = rowPemilikPertama.createCell(14)
                                        coljumlahBangunanTanah.cellStyle = cellStyleNomor
                                        Log.d("aiaiaia", "$pondasiName | $pondasiVolume")
                                        luasbangunan += """
                                                |${bangunan[indexBangunan]?.projectBuildingFloorTotal}
                                                |${if(pondasiVolume.isEmpty() || pondasiVolume == "0.0"){""} else {pondasiVolume}}
                                                |${if(strukturVolume.isEmpty() || strukturVolume == "0.0"){""} else {strukturVolume}}
                                                |${if(kerangkaAtapVolume.isEmpty() || kerangkaAtapVolume == "0.0"){""} else {kerangkaAtapVolume}}
                                                |${if(penutupAtapVolume.isEmpty() || penutupAtapVolume == "0.0"){""} else {penutupAtapVolume}}
                                                |${if(plafonVolume.isEmpty() || plafonVolume == "0.0"){""} else {plafonVolume}}
                                                |${if(dindingVolume.isEmpty() || dindingVolume == "0.0"){""} else {dindingVolume}}
                                                |${if(pinjenVolume.isEmpty() || pinjenVolume == "0.0"){""} else {pinjenVolume}}
                                                |${if(lantaiVolume.isEmpty() || lantaiVolume == "0.0"){""} else {lantaiVolume}}
                                                
                                                
                                                
                                            """.trimMargin()
                                        coljumlahBangunanTanah.setCellValue(luasbangunan)

                                        ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 14, 15))

//                                        satuanbangunan += """
//                                                |
//                                                |$satuanPondasiBangunan
//                                                |$satuanStrukturBangunan
//                                                |$satuanKertapBangunan
//                                                |$satuanPetapBangunan
//                                                |$satuanPlafonBangunan
//                                                |$satuanDindingBangunan
//                                                |$satuanPinjelBangunan
//                                                |$satuanlantaiBangunan
//
//
//
//                                            """.trimMargin()
//                                        val colsatuanBangunanTanah = rowPemilikPertama.createCell(15)
//                                        colsatuanBangunanTanah.cellStyle = cellStyleNomor
//                                        colsatuanBangunanTanah.setCellValue(satuanbangunan)

                                    }//looping bangunan
                                }

                                /* tanaman */
                                val tanaman = inrealm.where(Tanaman::class.java)
                                    .equalTo("ProjectId", pjland[elementTanah]?.projectId)
                                    .and()
                                    .equalTo("LandIdTemp", pjland[elementTanah]?.areaId)
                                        .findAll()
                                if (tanaman != null) {
                                    for (indexTanaman in 0 until tanaman.size) {
                                        val convertTanamanId = tanaman[indexTanaman]?.jenisTanamanId
                                        val convertTanamanId2 = convertTanamanId!!.replace("[", "['")
                                        val convertTanamanId3 = convertTanamanId2.replace("]", "']")
                                        val convertTanamanId4 = convertTanamanId3.replace(", ", "','")

                                        val convertTanamanNama = tanaman[indexTanaman]?.namaTanaman
                                        val convertTanamanNama2 = convertTanamanNama!!.replace("[", "['")
                                        val convertTanamanNama3 = convertTanamanNama2.replace("]", "']")
                                        val convertTanamanNama4 = convertTanamanNama3.replace(", ", "','")

                                        val convertTanamanNamaOther = tanaman[indexTanaman]?.namaTanamanOther
                                        val convertTanamanNamaOther2 = convertTanamanNamaOther!!.replace("[", "['")
                                        val convertTanamanNamaOther3 = convertTanamanNamaOther2.replace("]", "']")
                                        val convertTanamanNamaOther4 = convertTanamanNamaOther3.replace(", ", "','")

                                        val convertTanamanUkuran = tanaman[indexTanaman]?.ukuranTanaman
                                        val convertTanamanUkuran2 = convertTanamanUkuran!!.replace("[", "['")
                                        val convertTanamanUkuran3 = convertTanamanUkuran2.replace("]", "']")
                                        val convertTanamanUkuran4 = convertTanamanUkuran3.replace(", ", "','")

                                        val convertTanamanJumlah = tanaman[indexTanaman]?.jumlahTanaman
                                        val convertTanamanJumlah2 = convertTanamanJumlah!!.replace("[", "['")
                                        val convertTanamanJumlah3 = convertTanamanJumlah2.replace("]", "']")
                                        val convertTanamanJumlah4 = convertTanamanJumlah3.replace(", ", "','")

                                        val dataJenisTanamanId = Gson().fromJson(convertTanamanId4, Array<String>::class.java).toList()
                                        val dataTanamanName = Gson().fromJson(convertTanamanNama4, Array<String>::class.java).toList()
                                        val dataTanamanNameOther = Gson().fromJson(convertTanamanNamaOther4, Array<String>::class.java).toList()
                                        val dataTanamanUkuran = Gson().fromJson(convertTanamanUkuran4, Array<String>::class.java).toList()
                                        val dataTanamanJumlah = Gson().fromJson(convertTanamanJumlah4, Array<String>::class.java).toList()

                                        val coljenisTanaman = rowPemilikPertama.createCell(16)
                                        coljenisTanaman.cellStyle = cellStyleNormal

                                        val colukuranTanamanKecil = rowPemilikPertama.createCell(17)
                                        colukuranTanamanKecil.cellStyle = cellStyleNomor

                                        val colukuranTanamanSedang = rowPemilikPertama.createCell(18)
                                        colukuranTanamanSedang.cellStyle = cellStyleNomor

                                        val colukuranTanamanBesar = rowPemilikPertama.createCell(19)
                                        colukuranTanamanBesar.cellStyle = cellStyleNomor

                                        val colJumlahTanaman = rowPemilikPertama.createCell(21)
                                        colJumlahTanaman.cellStyle = cellStyleNomor

//                                        val colSatuanTanaman = rowPemilikPertama.createCell(22)
//                                        colSatuanTanaman.cellStyle = cellStyleNomor
//

                                        var tanamanNama = ""
                                        var ukuranTanamanKecil = ""
                                        var ukuranTanamanBesar = ""
                                        var ukuranTanamanSedang = ""
                                        var satuanTanamanName = ""
                                        var jmlhTanaman = 0
                                        var mJmlhTanaman = ""

                                        var mtanamanNama = ""
                                        var mukuranTanamanKecil = ""
                                        var mukuranTanamanBesar = ""
                                        var mukuranTanamanSedang = ""
                                        var msatuanTanamanName = ""
                                        var mjmlhTanaman = ""
                                        var mjmlhTanaman2 = ""

                                        if (dataJenisTanamanId.isNotEmpty()) {
                                            Log.d("hihi", dataJenisTanamanId.size.toString())
                                            for (indexTanamanId in dataJenisTanamanId.indices) {

                                                Log.d("haha", indexTanamanId.toString())

                                                /*f1 = if (dataPondasiNameOther[indexPondasi].isNotEmpty()) {
                                                    """
                                                            - ${dataPondasiNameOther[indexPondasi]}

                                                        """.trimIndent()
                                                } else {
                                                    """
                                                            - ${dataPondasiName[indexPondasi]}

                                                        """.trimIndent()
                                                }

                                                if (dataPondasiVolume[indexPondasi].isNotEmpty()) {
                                                    mpondasiVolume =
                                                            """
                                                            ${dataPondasiVolume[indexPondasi]}

                                                        """.trimIndent()

                                                    mSatuanPondasiBangunan =
                                                            """
                                                            $satuamLuas

                                                        """.trimIndent()
                                                }


                                                pondasiName += """
                                                        |$f1
                                                    """.trimMargin()

                                                pondasiVolume += """
                                                        |$mpondasiVolume
                                                    """.trimMargin()

                                                satuanPondasiBangunan += """
                                                        |$mSatuanPondasiBangunan
                                                    """.trimMargin()*/
                                                jmlhTanaman += dataTanamanJumlah[indexTanamanId].toInt()
                                                if (dataTanamanNameOther[indexTanamanId].isNotEmpty()) {
                                                    mtanamanNama = """
                                                            - ${dataTanamanNameOther[indexTanamanId]}
                                                            
                                                        """.trimIndent()

                                                    mjmlhTanaman = """

                                                        """.trimIndent()

                                                    msatuanTanamanName = """
                                                            pcs
                                                            
                                                        """.trimIndent()
                                                } else {
                                                    mtanamanNama = """
                                                            - ${dataTanamanName[indexTanamanId]}
                                                            
                                                        """.trimIndent()

                                                    mjmlhTanaman = """
                                                            
                                                            
                                                        """.trimIndent()

                                                    msatuanTanamanName = """
                                                            pcs
                                                            
                                                        """.trimIndent()
                                                }


                                                mukuranTanamanKecil = if (dataTanamanUkuran[indexTanamanId] != "Kecil") {
                                                    """
                                                            -
                                                            
                                                        """.trimIndent()

                                                } else {
                                                    """
                                                            ${dataTanamanJumlah[indexTanamanId]}
                                                            
                                                        """.trimIndent()
                                                }

                                                mukuranTanamanSedang = if (dataTanamanUkuran[indexTanamanId] != "Sedang") {
                                                    """
                                                            -
                                                            
                                                        """.trimIndent()

                                                } else {
                                                    """
                                                            ${dataTanamanJumlah[indexTanamanId]}
                                                            
                                                        """.trimIndent()
                                                }

                                                mukuranTanamanBesar = if (dataTanamanUkuran[indexTanamanId] != "Besar") {
                                                    """
                                                            -
                                                            
                                                        """.trimIndent()

                                                } else {
                                                    """
                                                            ${dataTanamanJumlah[indexTanamanId]}
                                                            
                                                        """.trimIndent()
                                                }

                                                tanamanNama += """
                                                        |$mtanamanNama
                                                    """.trimMargin()

                                                satuanTanamanName += """
                                                        |$msatuanTanamanName
                                                    """.trimMargin()

                                                ukuranTanamanKecil += """
                                                        |$mukuranTanamanKecil
                                                    """.trimMargin()

                                                ukuranTanamanSedang += """
                                                        |$mukuranTanamanSedang
                                                    """.trimMargin()

                                                ukuranTanamanBesar += """
                                                        |$mukuranTanamanBesar
                                                    """.trimMargin()

                                                if (indexTanamanId + 1 < dataJenisTanamanId.size) {
                                                    mjmlhTanaman2 += """
                                                        |$mjmlhTanaman
                                                    """.trimMargin()
                                                }

                                                if (indexTanamanId + 1 == dataJenisTanamanId.size) {
                                                    mjmlhTanaman = """
                                                            $jmlhTanaman
                                                        """.trimIndent()


                                                    mjmlhTanaman2 += """
                                                        |$mjmlhTanaman
                                                    """.trimMargin()

                                                    jumlahtanaman += """
                                                        |$mjmlhTanaman2
                                                        

                                                    """.trimMargin()
                                                }


                                                /*mtanamanNama += if (dataTanamanNameOther[indexTanamanId].isNotEmpty()) {
                                                    "- ${dataTanamanNameOther[indexTanamanId]}\n"
                                                } else {
                                                    "- ${dataTanamanName[indexTanamanId]}\n"
                                                }

                                                k += if (dataTanamanUkuran[indexTanamanId] != "Kecil") {
                                                    "\n"
                                                } else {
                                                    "${dataTanamanJumlah[indexTanamanId]}\n"
                                                }

                                                s += if (dataTanamanUkuran[indexTanamanId] != "Sedang") {
                                                    "\n"
                                                } else {
                                                    "${dataTanamanJumlah[indexTanamanId]}\n"
                                                }

                                                b += if (dataTanamanUkuran[indexTanamanId] != "Besar") {
                                                    "\n"
                                                } else {
                                                    "${dataTanamanJumlah[indexTanamanId]}\n"
                                                }

                                                tanamanNama += mtanamanNama


                                                ukuranTanamanKecil += k
                                                ukuranTanamanSedang += s
                                                ukuranTanamanBesar += b*/

                                                /*jumlahtanaman += dataTanamanJumlah[indexTanamanId].toInt()
                                                if (dataTanamanNameOther[indexTanamanId].isNotEmpty()) {
                                                    jenistanaman = "- ${dataTanamanNameOther[indexTanamanId]}\n"
                                                    satuantanaman = "Pcs\n"
                                                    mJmlhTanaman = "\n"
                                                } else {
                                                    jenistanaman = "- ${dataTanamanName[indexTanamanId]}\n"
                                                    satuantanaman = "Pcs\n"
                                                    mJmlhTanaman = "\n"
                                                }

                                                k = if (dataTanamanUkuran[indexTanamanId] != "Kecil") {
                                                    "\n"
                                                } else {
                                                    "${dataTanamanJumlah[indexTanamanId]}\n"
                                                }

                                                s = if (dataTanamanUkuran[indexTanamanId] != "Sedang") {
                                                    "\n"
                                                } else {
                                                    "${dataTanamanJumlah[indexTanamanId]}\n"
                                                }

                                                b = if (dataTanamanUkuran[indexTanamanId] != "Besar") {
                                                    "\n"
                                                } else {
                                                    "${dataTanamanJumlah[indexTanamanId]}\n"
                                                }


                                                tanamanNama += "$jenistanaman\n"
                                                ukuranTanamanKecil += "$k\n"
                                                ukuranTanamanSedang += "$s\n"
                                                ukuranTanamanBesar += "$b\n"
                                                satuanTanamanName += "$satuantanaman\n"
                                                jmlhTanaman += mJmlhTanaman*/

                                            }//looping tanaman

                                        }

                                        //versi 2
                                        //jmlhTanaman += "\n" + jumlahtanaman

                                        /*satuantanaman += """
                                                |$tanamanNama
                                                
                                                
                                            """.trimMargin()*/

                                        jenistanaman += """
                                                |$tanamanNama
                                                
                                                
                                            """.trimMargin()

                                        satuantanaman += """
                                                |$satuanTanamanName
                                                
                                                
                                            """.trimMargin()

                                        k += """
                                                |$ukuranTanamanKecil
                                                
                                                
                                            """.trimMargin()

                                        s += """
                                                |$ukuranTanamanSedang
                                                
                                                
                                            """.trimMargin()

                                        b += """
                                                |$ukuranTanamanBesar
                                                
                                                
                                            """.trimMargin()

                                        coljenisTanaman.setCellValue(jenistanaman)
                                        colukuranTanamanKecil.setCellValue(k)
                                        colukuranTanamanSedang.setCellValue(s)
                                        colukuranTanamanBesar.setCellValue(b)
                                        colJumlahTanaman.setCellValue(jumlahtanaman)
//                                        colSatuanTanaman.setCellValue(satuantanaman)
                                        /*
                                      */

                                        /*mukuranTanamanKecil += """
                                                |$ukuranTanamanKecil

                                            """.trimMargin()

                                        mukuranTanamanBesar += """
                                                |$ukuranTanamanBesar

                                            """.trimMargin()

                                        mukuranTanamanSedang += """
                                                |$ukuranTanamanSedang

                                            """.trimMargin()*/
                                    }//looping tanaman
                                }

                                ws.addMergedRegion(CellRangeAddress(oldLastRow, oldLastRow, 21, 22))

                                /* sarana pelengkap */
                                val otherfacility = inrealm.where(SaranaLain::class.java)
                                        .equalTo("LandIdTemp", pjland[elementTanah]?.areaId)
                                        .and()
                                        .equalTo("ProjectId", pjland[elementTanah]?.projectId)
                                        .findAll()

                                if (otherfacility != null) {
                                    var satuan = ""
                                    //Log.d("cuy", otherfacility.size.toString())

                                    var saranaPelengkapNama = ""
                                    var msaranaPelengkaNama = ""

                                    var jumlahsaranaPelengkap = ""
                                    var mjumlahsaranaPelengkap = ""
                                    val list = ArrayList<GroupingDataSaranaPelengkap>()
                                    for (indexFasilitas in 0 until otherfacility.size) {
                                        val convertJenisFasilitasId = otherfacility[indexFasilitas]?.jenisFasilitasId
                                        val convertJenisFasilitasId2 = convertJenisFasilitasId!!.replace("[", "['")
                                        val convertJenisFasilitasId3 = convertJenisFasilitasId2.replace("]", "']")
                                        val convertJenisFasilitasId4 = convertJenisFasilitasId3.replace(", ", "','")

                                        val convertKapasitasFasilitas = otherfacility[indexFasilitas]?.kapasitas
                                        val convertKapasitasFasilitas2 = convertKapasitasFasilitas!!.replace("[", "['")
                                        val convertKapasitasFasilitas3 = convertKapasitasFasilitas2.replace("]", "']")
                                        val convertKapasitasFasilitas4 = convertKapasitasFasilitas3.replace(", ", "','")

                                        val convertKapasitasValueFasilitas = otherfacility[indexFasilitas]?.kapasitasValue
                                        val convertKapasitasValueFasilitas2 = convertKapasitasValueFasilitas!!.replace("[", "['")
                                        val convertKapasitasValueFasilitas3 = convertKapasitasValueFasilitas2.replace("]", "']")
                                        val convertKapasitasValueFasilitas4 = convertKapasitasValueFasilitas3.replace(", ", "','")

                                        val convertSatuanKapasitasFasilitas = otherfacility[indexFasilitas]?.satuanKapasitasValue
                                        val convertSatuanKapasitasFasilitas2 = convertSatuanKapasitasFasilitas!!.replace("[", "['")
                                        val convertSatuanKapasitasFasilitas3 = convertSatuanKapasitasFasilitas2.replace("]", "']")
                                        val convertSatuanKapasitasFasilitas4 = convertSatuanKapasitasFasilitas3.replace(", ", "','")

                                        val dataJenisFasiltasId = Gson().fromJson(convertJenisFasilitasId4, Array<String>::class.java).toList()
                                        val dataKapasitasFasilitas = Gson().fromJson(convertKapasitasFasilitas4, Array<String>::class.java).toList()
                                        val dataKapasitasValueFasilitas = Gson().fromJson(convertKapasitasValueFasilitas4, Array<String>::class.java).toList()
                                        val dataSatuanKapasitasFasilitas = Gson().fromJson(convertSatuanKapasitasFasilitas4, Array<String>::class.java).toList()

                                        /*fasilitasLain += if (otherfacility[indexFasilitas]?.jenisFasilitasNameOther!!.isNotEmpty()) {
                                            "- ${otherfacility[indexFasilitas]?.jenisFasilitasNameOther}\n"
                                        } else {
                                            "- ${otherfacility[indexFasilitas]!!.jenisFasilitasName}\n"
                                        }*/

                                        /*if (otherfacility[indexFasilitas]?.jenisFasilitasNameOther!!.isNotEmpty()) {
                                            msaranaPelengkaNama = """
                                                            - ${otherfacility[indexFasilitas]?.jenisFasilitasNameOther}
                                                            
                                                        """.trimIndent()
                                        } else {
                                            msaranaPelengkaNama = """
                                                            - ${otherfacility[indexFasilitas]?.jenisFasilitasName}
                                                            
                                                        """.trimIndent()
                                        }


                                        saranaPelengkapNama += """
                                                        |$msaranaPelengkaNama
                                                    """.trimMargin()*/

                                        val saranaPelengkapName = if (otherfacility[indexFasilitas]?.jenisFasilitasNameOther!!.isNotEmpty()) {
                                            otherfacility[indexFasilitas]?.jenisFasilitasNameOther
                                        } else {
                                            otherfacility[indexFasilitas]?.jenisFasilitasName
                                        }

                                        if (dataJenisFasiltasId.isNotEmpty()) {
                                            for (indexFasilitasId in dataJenisFasiltasId.indices) {
                                                list.add(GroupingDataSaranaPelengkap(otherfacility[indexFasilitas]?.saranaIdTemp, dataJenisFasiltasId[indexFasilitasId], saranaPelengkapName))
                                            }
                                        }
                                    }// looping sarana pelengka

                                    Log.d("sarana", list.toString())
                                    for (i in 0 until list.size) {
                                        Log.d("saranaId", list[i].msaranaIdTemp.toString())
                                        Log.d("saranaName", list[i].mjenisFasilitasName.toString())

                                    }

                                    //var studlistGrouped = list.stream().collect(Collectors.groupingBy({ w-> w.stud_location }))
                                    val saranaGrouped = list.groupBy(GroupingDataSaranaPelengkap::mjenisFasilitasName, GroupingDataSaranaPelengkap::msaranaIdTemp)
                                    //Log.d("saranaGroup", saranaGrouped.toString())
                                    //Getting Set of keys
                                    val keySet = saranaGrouped.keys
                                    //Creating an ArrayList of keys
                                    val listOfKeys = ArrayList<String>(keySet)
                                    for (key in listOfKeys) {
                                        //Log.d("split_key", key)
                                        msaranaPelengkaNama = """
                                                            - $key
                                                            
                                                        """.trimIndent()


                                        saranaPelengkapNama += """
                                                        |$msaranaPelengkaNama
                                                    """.trimMargin()
                                    }

                                    //Getting Collection of values
                                    //val listOfValues = saranaGrouped.values
                                    val listOfValues = ArrayList(saranaGrouped.values)
                                    for (index in 0 until listOfValues.size) {
                                        Log.d("split_value", listOfValues[index].size.toString())
                                        jumlahsaranaPelengkap = """
                                                            ${listOfValues[index].size}
                                                            
                                                        """.trimIndent()


                                        mjumlahsaranaPelengkap += """
                                                        |$jumlahsaranaPelengkap
                                                    """.trimMargin()
                                    }

                                    fasilitasLain += """
                                                |$saranaPelengkapNama
                                                
                                                
                                            """.trimMargin()

                                    fasilitasValue += """
                                                |$mjumlahsaranaPelengkap
                                                
                                                
                                            """.trimMargin()


                                    val colnamaFasilitas = rowPemilikPertama.createCell(23)
                                    colnamaFasilitas.cellStyle = cellStyleNormal
                                    colnamaFasilitas.setCellValue(fasilitasLain)

                                    val coljumlahSaranaFasilitas = rowPemilikPertama.createCell(24)
                                    coljumlahSaranaFasilitas.cellStyle = cellStyleNomor
                                    coljumlahSaranaFasilitas.setCellValue(fasilitasValue)

                                }

                                /*pemilik 2 dan ahli waris*/
                                //pihak kedua
                                val dataPihakKedua = inrealm.where(Subjek2::class.java).equalTo("LandIdTemp", pjland[elementTanah]?.landIdTemp).findAll()
                                if (!dataPihakKedua.isNullOrEmpty()) {
                                    for (elementdatapihakkedua in 0 until dataPihakKedua.size) {
                                        val coldataPemilikKedua = rowPemilikPertama.createCell(3)
                                        coldataPemilikKedua.cellStyle = cellStyleNormal

                                        val nomordIdentitasPenggarap: String? = if (dataPihakKedua[elementdatapihakkedua]?.subjek2IdentityNumber!!.isNotEmpty()) results[element]?.partyIdentityNumber else "-"
                                        val tmptLahirPenggarap: String? = if (dataPihakKedua[elementdatapihakkedua]?.subjek2BirthPlaceName!!.isNotEmpty()) dataPihakKedua[elementdatapihakkedua]?.subjek2BirthPlaceName else "-"
                                        val tglLahirPenggarap: String? = if (dataPihakKedua[elementdatapihakkedua]?.subjek2BirthDate!!.isNotEmpty()) dataPihakKedua[elementdatapihakkedua]?.subjek2BirthDate else "-"
                                        val pekerjaanPenggarap: String? = if (dataPihakKedua[elementdatapihakkedua]?.subjek2OccupationName!!.isNotEmpty()) dataPihakKedua[elementdatapihakkedua]?.subjek2OccupationName else "-"
                                        val alamatJalanPenggarap: String? = if (dataPihakKedua[elementdatapihakkedua]?.subjek2StreetName!!.isNotEmpty()) dataPihakKedua[elementdatapihakkedua]?.subjek2StreetName else "-"
                                        val alamatNomorRumahPenggarap: String? = if (dataPihakKedua[elementdatapihakkedua]?.subjek2Number!!.isNotEmpty()) dataPihakKedua[elementdatapihakkedua]?.subjek2Number else "-"
                                        val alamatRTPenggarap: String? = if (dataPihakKedua[elementdatapihakkedua]?.subjek2RT!!.isNotEmpty()) dataPihakKedua[elementdatapihakkedua]?.subjek2RT else "-"
                                        val alamatRWPenggarap: String? = if (dataPihakKedua[elementdatapihakkedua]?.subjek2RW!!.isNotEmpty()) dataPihakKedua[elementdatapihakkedua]?.subjek2RW else "-"
                                        val alamatBlokPenggarap: String? = if (dataPihakKedua[elementdatapihakkedua]?.subjek2Block!!.isNotEmpty()) dataPihakKedua[elementdatapihakkedua]?.subjek2Block else "-"
                                        val alamatKelurahanPenggarap: String? = if (dataPihakKedua[elementdatapihakkedua]?.subjek2VillageName!!.isNotEmpty()) dataPihakKedua[elementdatapihakkedua]?.subjek2VillageName else "-"
                                        val alamatKecamatanPenggarap: String? = if (dataPihakKedua[elementdatapihakkedua]?.subjek2DistrictName!!.isNotEmpty()) dataPihakKedua[elementdatapihakkedua]?.subjek2DistrictName else "-"
                                        val alamatKotaKabupatenPenggarap: String? = if (dataPihakKedua[elementdatapihakkedua]?.subjek2RegencyName!!.isNotEmpty()) dataPihakKedua[elementdatapihakkedua]?.subjek2RegencyName else "-"
                                        val alamatProvinsiPenggarap: String? = if (dataPihakKedua[elementdatapihakkedua]?.subjek2ProvinceName!!.isNotEmpty()) dataPihakKedua[elementdatapihakkedua]?.subjek2ProvinceName?.replace("[","")?.replace("]","") else "-"

                                        menguasai += ("""
                                                a.  Nama                    :  ${dataPihakKedua[elementdatapihakkedua]?.subjek2FullName}
                                                b.  Tanggal Lahir       :  $tmptLahirPenggarap, $tglLahirPenggarap
                                                c.  Pekerjaan             :  $pekerjaanPenggarap
                                                d.  Alamat                  :  Jl.$alamatJalanPenggarap No.$alamatNomorRumahPenggarap Blok $alamatBlokPenggarap RT $alamatRTPenggarap RW $alamatRWPenggarap $alamatKelurahanPenggarap $alamatKecamatanPenggarap $alamatKotaKabupatenPenggarap $alamatProvinsiPenggarap
                                                e.  NIK/No.KTP          :  $nomordIdentitasPenggarap
                                                f.  Kepemilikan          :  $jenisKepemilikan
                                                
                                                

                                            """.trimIndent())
                                        coldataPemilikKedua.setCellValue(menguasai)
                                    }
                                } else {
                                    val coldataPemilikKedua = rowPemilikPertama.createCell(3)
                                    coldataPemilikKedua.cellStyle = cellStyleNormal
                                    coldataPemilikKedua.setCellValue(menguasai)
                                }

                            }// looping tanah

                        }
                    }

                }
                //end of old export

                /*val results = inrealm.where(PartyAdd::class.java).equalTo("ProjectId", projectAssignProjectId).findAll()
                if (results != null) {
                    Log.d("Hasil", results.toString())
                    var rowIdx = 13
                    var rowNo = 1

                    for (i in 0 until results.size) {
                        val row = ws.createRow(rowIdx++)

                        // style cell top center
                        val cellStyle = wb!!.createCellStyle()
                        cellStyle.wrapText = true
                        // justify text alignment
                        cellStyle.setVerticalAlignment(VerticalAlignment.TOP)
                        cellStyle.setAlignment(HorizontalAlignment.CENTER)

                        // style cell top left center
                        val cellStyle2 = wb!!.createCellStyle()
                        cellStyle2.wrapText = true
                        // justify text alignment
                        cellStyle2.setVerticalAlignment(VerticalAlignment.TOP)
                        cellStyle2.setAlignment(HorizontalAlignment.LEFT)

                        *//* kolom no *//*
                            val cell_1 = row.createCell(0)
                            cell_1.setCellValue(rowNo.toString())
                            cell_1.cellStyle = cellStyle

                            *//* kolom pemilik pertama *//*
                            val cellPemilik = row.createCell(1)
                            cellPemilik.setCellValue("a. Nama : ${results[i]?.partyFullName}\nb. Tanggal Lahir : ${results[i]?.partyBirthPlaceName}" +
                                    " ${results[i]?.partyBirthDate}\nc. Pekerjaan : ${results[i]?.partyOccupationName}\nd.Alamat : ${results[i]?.partyStreetName} Komplek ${results[i]?.partyComplexName} Blok ${results[i]!!.partyBlock} " +
                                    "No.${results[i]?.partyNumber} RT. ${results[i]?.partyRT} RW. ${results[i]?.partyRW} ${results[i]?.partyDistrictName} ${results[i]?.partyRegencyName} ${results[i]?.partyProvinceName} ${results[i]?.partyPostalCode}")
                            cellPemilik.cellStyle = cellStyle2
                            //project Land
                            //Log.d("Hasil", results[i]?.tempId.toString())
                            val pjland = inrealm.where(ProjectLand::class.java).equalTo("PartyIdTemp", results[i]?.tempId).findAll()
                            if (pjland != null) {
                                var piducia = ""

                                lannumber = ""
                                letak = ""
                                landaffected = ""
                                sisaatas = ""
                                sisabawah = ""
                                kepemilikan = ""
                                tandabukti = ""
                                jenisbangunan = ""
                                luasbangunan = ""
                                jenistanaman = ""
                                k = ""
                                s = ""
                                b = ""
                                r = ""
                                jumlahtanaman = ""
                                satuantanaman = ""
                                fasilitasLain = ""
                                fasilitasValue = ""
                                landInfopiducia = ""
                                f1 = ""
                                f2 = ""
                                st1 = ""
                                st2 = ""
                                rf1 = ""
                                rf2 = ""
                                rc1 = ""
                                rc2 = ""
                                cl1 = ""
                                cl2 = ""
                                dw1 = ""
                                dw2 = ""
                                fl1 = ""
                                fl2 = ""
                                wl1 = ""
                                wl2 = ""
                                menguasai = ""

                                for (i in 0 until pjland.size) {
                                    lannumber += "${pjland[i]?.landNumberList}\n"
                                    letak += "a. RT/RW : ${pjland[i]?.landRT}/${pjland[i]?.landRW}\nb. Kelurahan : ${pjland[i]?.landDistrictName}\nc. Kecamatan : ${pjland[i]?.landVillageName}\n"
                                    landaffected += "${pjland[i]?.landAreaAffected}\n"
                                    kepemilikan += "${pjland[i]?.landOwnershipName}\n"
                                    tandabukti += "${pjland[i]?.landCertificateNumber}\n"
                                    piducia = "${pjland[i]?.landAddInfoAddInfoName}"
                                    val replac1 = piducia.replace("[", "")
                                    val replac2 = replac1.replace("]", "")
                                    val replac3 = replac2.replace(",", "\n")
                                    landInfopiducia = landInfopiducia + replac3 + "\n"

                                    //ahliwaris
                                    val ahliwaris = inrealm.where(AhliWaris::class.java).equalTo("LandIdTemp", pjland[i]?.landIdTemp).findAll()
                                    if (ahliwaris != null) {
                                        for (i in 0 until ahliwaris.size) {
                                            menguasai = menguasai + "a. Nama : ${ahliwaris[i]?.partyFullName}\nb. Tanggal Lahir : ${ahliwaris[i]?.partyBirthPlaceName}" +
                                                    " ${ahliwaris[i]?.partyBirthDate}\nc. Pekerjaan : ${ahliwaris[i]?.partyOccupationName}\nd.Alamat : ${ahliwaris[i]?.partyStreetName} Komplek ${ahliwaris[i]?.partyComplexName} Blok ${ahliwaris[i]!!.partyBlock} " +
                                                    "No.${ahliwaris[i]?.partyNumber} RT. ${ahliwaris[i]?.partyRT} RW. ${ahliwaris[i]?.partyRW} ${ahliwaris[i]?.partyDistrictName} ${ahliwaris[i]?.partyRegencyName} ${ahliwaris[i]?.partyProvinceName} ${results[i]?.partyPostalCode}\n"
                                        }
                                    }
                                    //pihak kedua
                                    val pihakkedua = inrealm.where(Subjek2::class.java).equalTo("LandIdTemp", pjland[i]?.landIdTemp).findAll()
                                    if (pihakkedua != null) {
                                        for (i in 0 until pihakkedua.size) {
                                            menguasai = menguasai + "a. Nama : ${pihakkedua[i]?.subjek2FullName}\nb. Tanggal Lahir : ${pihakkedua[i]?.subjek2BirthPlaceName}" +
                                                    " ${pihakkedua[i]?.subjek2BirthDate}\nc. Pekerjaan : ${pihakkedua[i]?.subjek2OccupationName}\nd.Alamat : ${pihakkedua[i]?.subjek2StreetName} Komplek ${pihakkedua[i]?.subjek2ComplexName} Blok ${pihakkedua[i]!!.subjek2Block} " +
                                                    "No.${pihakkedua[i]?.subjek2Number} RT. ${pihakkedua[i]?.subjek2RT} RW. ${pihakkedua[i]?.subjek2RW} ${pihakkedua[i]?.subjek2DistrictName} ${pihakkedua[i]?.subjek2RegencyName} ${pihakkedua[i]?.subjek2ProvinceName} ${pihakkedua[i]?.subjek2PostalCode}\n"
                                        }
                                    }

                                    //Bangunan
                                    val bangunan = inrealm.where(Bangunan::class.java).equalTo("LandIdTemp", pjland[i]?.landIdTemp).findAll()
                                    if (bangunan != null) {
                                        for (i in 0 until bangunan.size) {
                                            sisaatas += "${bangunan[i]?.projectBuildingAreaUpperRoom}\n"
                                            sisabawah += "${bangunan[i]?.projectBuildingAreaLowerRoom}\n"
                                            jenisbangunan += "${bangunan[i]?.projectBuildingTypeName}\n"
                                            luasbangunan += "${bangunan[i]?.projectBuildingFloorTotal}\n"
                                            val foundation = "${bangunan[i]?.foundationTypeName}"
                                            val fo = foundation.replace("[", "")
                                            val fo1 = fo.replace("]", "")
                                            val fo2 = fo1.replace(",", "\n")
                                            f1 += fo2

                                            val foundationTotal = "${bangunan[i]?.pbFoundationTypeAreaTotal}"
                                            val fond = foundationTotal.replace("[", "")
                                            val fond1 = fond.replace("]", "")
                                            val fond2 = fond1.replace(",", "\n")
                                            f2 += fond2

                                            val struktur = "${bangunan[i]?.structureTypeName}"
                                            val struk = struktur.replace("[", "")
                                            val struk1 = struk.replace("]", "")
                                            val struk2 = struk1.replace(",", "\n")
                                            st1 += struk2

                                            val strukturTotal = "${bangunan[i]?.pbStructureTypeAreaTotal}"
                                            val struktot = strukturTotal.replace("[", "")
                                            val struktot1 = struktot.replace("]", "")
                                            val struktot2 = struktot1.replace(",", "\n")
                                            st2 += struktot2

                                            val roofFrame = "${bangunan[i]?.roofFrameTypeName}"
                                            val rFrame1 = roofFrame.replace("[", "")
                                            val rFrame2 = rFrame1.replace("]", "")
                                            val rFrame3 = rFrame2.replace(",", "\n")
                                            rf1 += rFrame3

                                            val roofFrameTotal = "${bangunan[i]?.pbRoofFrameTypeAreaTotal}"
                                            val rFramet1 = roofFrameTotal.replace("[", "")
                                            val rFramet2 = rFramet1.replace("]", "")
                                            val rFramet3 = rFramet2.replace(",", "\n")
                                            rf2 += rFramet3

                                            val roofCovering = "${bangunan[i]?.roofCoveringTypeName}"
                                            val roofConver1 = roofCovering.replace("[", "")
                                            val roofConver2 = roofConver1.replace("]", "")
                                            val roofConver3 = roofConver2.replace(",", "\n")
                                            rc1 += roofConver3

                                            val roofCoveringTotal = "${bangunan[i]?.pbRoofCoveringTypeAreaTotal}"
                                            val roofConverT1 = roofCoveringTotal.replace("[", "")
                                            val roofConverT2 = roofConverT1.replace("]", "")
                                            val roofConverT3 = roofConverT2.replace(",", "\n")
                                            rc2 += roofConverT3

                                            val ceiling = "${bangunan[i]?.ceilingTypeName}"
                                            val cei1 = ceiling.replace("[", "")
                                            val cei2 = cei1.replace("]", "")
                                            val cei3 = cei2.replace(",", "\n")
                                            cl1 += cei3

                                            val ceilingTotal = "${bangunan[i]?.pbCeilingTypeAreaTotal}"
                                            val ceiT1 = ceilingTotal.replace("[", "")
                                            val ceiT2 = ceiT1.replace("]", "")
                                            val ceiT3 = ceiT2.replace(",", "\n")
                                            cl2 += ceiT3

                                            val doorWindows = "${bangunan[i]?.doorWindowTypeName}"
                                            val doorname1 = doorWindows.replace("[", "")
                                            val doorname2 = doorname1.replace("]", "")
                                            val doorname3 = doorname2.replace(",", "\n")
                                            dw1 += doorname3

                                            val doorWindowsTotal = "${bangunan[i]?.pbDoorWindowTypeAreaTotal}"
                                            val doorTot1 = doorWindowsTotal.replace("[", "")
                                            val doorTot2 = doorTot1.replace("]", "")
                                            val doorTot3 = doorTot2.replace(",", "\n")
                                            dw2 += doorTot3

                                            val floor = "${bangunan[i]?.floorTypeName}"
                                            val floor1 = floor.replace("[", "")
                                            val floor2 = floor1.replace("]", "")
                                            val floor3 = floor2.replace(",", "\n")
                                            fl1 += floor3

                                            val floorTotal = "${bangunan[i]?.pbFloorTypeAreaTotal}"
                                            val floorT1 = floorTotal.replace("[", "")
                                            val floorT2 = floorT1.replace("]", "")
                                            val floorT3 = floorT2.replace(",", "\n")
                                            fl2 += floorT3

                                            val wall = "${bangunan[i]?.wallTypeName}"
                                            val wall1 = wall.replace("[", "")
                                            val wall2 = wall1.replace("]", "")
                                            val wall3 = wall2.replace(",", "\n")
                                            wl1 += wall3

                                            val wallTotal = "${bangunan[i]?.pbfWallTypeAreaTotal}"
                                            val wallTotal1 = wallTotal.replace("[", "")
                                            val wallTotal2 = wallTotal1.replace("]", "")
                                            val wallTotal3 = wallTotal2.replace(",", "\n")
                                            wl2 += wallTotal3

                                        }
                                    }

                                    //Tanaman
                                    val tanaman = inrealm.where(Tanaman::class.java).equalTo("LandIdTemp", pjland[i]?.landIdTemp).findAll()
                                    if (tanaman != null) {
                                        for (i in 0 until tanaman.size) {
                                            jenistanaman += "${tanaman[i]?.namaTanaman}\n"
                                            val ukuran = "${tanaman[i]?.ukuranTanaman}"
                                            val rumpun = "${tanaman[i]?.jenisTanamanNama}"
                                            val jml = "${tanaman[i]?.jumlahTanaman}"
                                            if (!ukuran.equals("Kecil")) {
                                                k += "\n"
                                            } else {
                                                k += "${tanaman[i]?.jumlahTanaman}\n"
                                            }

                                            if (!ukuran.equals("Sedang")) {
                                                s += "\n"
                                            } else {
                                                s += "${tanaman[i]?.jumlahTanaman}\n"
                                            }

                                            if (!ukuran.equals("Besar")) {
                                                b += "\n"
                                            } else {
                                                b += "${tanaman[i]?.jumlahTanaman}\n"
                                            }

                                            if (!rumpun.equals("Tanaman")) {
                                                r += "${tanaman[i]?.jumlahTanaman}\n"
                                            } else {
                                                r += "\n"
                                            }

                                            if (jml == "") {
                                                jumlahtanaman += "\n"
                                                satuantanaman += "\n"
                                            } else {
                                                jumlahtanaman = jumlahtanaman + jml + "\n"
                                                satuantanaman += "Phn\n"
                                            }

                                        }
                                    }

                                    //OtherFacility
                                    val otherfacility = inrealm.where(SaranaLain::class.java).equalTo("LandIdTemp", pjland[i]?.landIdTemp).findAll()
                                    if (otherfacility != null) {
                                        var satuan = ""
                                        for (i in 0 until otherfacility.size) {
                                            fasilitasLain += "${otherfacility[i]?.jenisFasilitasName}\n"
                                            satuan = "${otherfacility[i]?.kapasitasValue}"
                                            val replac1 = satuan.replace("[", "")
                                            val replac2 = replac1.replace("]", "")
                                            fasilitasValue = fasilitasValue + replac2 + "\n"
                                        }
                                    }
                                }
                            }

                            *//* pemilik 2 & ahli waris *//*
                            val cellPemilik2AhliWaris = row.createCell(2)
                            cellPemilik2AhliWaris.setCellValue(menguasai)
                            cellPemilik2AhliWaris.cellStyle = cellStyle2

                            *//* no tanah *//*
                            val cellNotanah = row.createCell(3)
                            cellNotanah.setCellValue(lannumber)
                            cellNotanah.cellStyle = cellStyle

                            *//* letak tanah *//*
                            val cellLetakTanah = row.createCell(4)
                            cellLetakTanah.setCellValue(letak)
                            cellLetakTanah.cellStyle = cellStyle2

                            //row.createCell(2).setCellValue(menguasai)
                            //row.createCell(3).setCellValue(lannumber)
                            //row.createCell(4).setCellValue(letak)
                            row.createCell(5)//.setCellValue(landaffected)
                            row.createCell(6) //.setCellValue(sisaatas)
                            row.createCell(7).setCellValue(sisabawah)
                            row.createCell(8).setCellValue(kepemilikan)
                            row.createCell(9).setCellValue(tandabukti)

                            row.createCell(12).setCellValue(jenisbangunan + "\n" + f1 + "\n" + st1 + "\n" + rf1 + "\n" + rc1 + "\n" + cl1 + "\n" + dw1 + "\n" + fl1 + "\n" + wl1)
                            row.createCell(13).setCellValue(luasbangunan + "\n" + f2 + "\n" + st2 + "\n" + rf2 + "\n" + rc2 + "\n" + cl2 + "\n" + dw2 + "\n" + fl2 + "\n" + wl2)
                            row.createCell(14).setCellValue("m2\n\nm2\nm2\nm2\nm2\nm2\nm2\nm2\nm2")

                            row.createCell(15).setCellValue(jenistanaman)
                            row.createCell(16).setCellValue(k)
                            row.createCell(17).setCellValue(s)
                            row.createCell(18).setCellValue(b)
                            row.createCell(19).setCellValue(r)
                            row.createCell(20).setCellValue(jumlahtanaman)
                            row.createCell(21).setCellValue(satuantanaman)
                            row.createCell(22).setCellValue(fasilitasLain)
                            row.createCell(23).setCellValue(fasilitasValue)
                            row.createCell(24).setCellValue(landInfopiducia)
                            row.createCell(25).setCellValue(landaffected)

                            rowNo++
                        }
                    }*/
            }, {
                runOnUiThread {
                    onDismissLoading()
                    val mfilename = fileName
                    val pathDir = File(Environment.getExternalStorageDirectory().toString() + File.separator + "SIOJT", "Downloads")
                    val outputFile = File(pathDir, mfilename)

                    if(outputFile.exists()){
                        outputFile.delete()
                    }

                    var ostream: FileOutputStream? = null
                    try {
                        if (!pathDir.exists()) {
                            pathDir.mkdirs()
                        } else {
                            println("Directory is not created")
                        }

                        if (outputFile.exists()) {
                            outputFile.delete()
                            if (!outputFile.createNewFile()) {
                                throw IOException("can able to create file")
                            }
                        }

                        ostream = FileOutputStream(outputFile)
                        wb!!.write(ostream)
                        ostream.flush()
                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                        Log.d("Error", "${e.message}")

                    } catch (e: IOException) {
                        Log.d("Iqbal", "${e.message}")
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Log.d("Error", "Export File Gagal")
                    } finally {
                        if (ostream != null) {
                            ostream.close()
                            try {
                                sweetAlretLoading = SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                                sweetAlretLoading?.titleText = "Export Data Berhasil"
                                sweetAlretLoading?.setCancelable(false)
                                sweetAlretLoading?.contentText = outputFile.toString()
                                sweetAlretLoading?.confirmText = "OK"
                                sweetAlretLoading?.setConfirmClickListener { sDialog ->
                                    sDialog?.let { if (it.isShowing) it.dismiss() }
                                    openFile(fileName)
                                }
                                sweetAlretLoading?.show()
                            } catch (e: IOException) {
                                Log.d("Error", "File gagal dibuka ${e.message}")
                            }
                        }


                    }
                }


            }, {
                Log.d("Error", "Gagal export data")
                Log.d("Error", it.message)
                runOnUiThread {
                    onDismissLoading()
                    sweetAlretLoading = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    sweetAlretLoading?.titleText = "Failed"
                    sweetAlretLoading?.setCancelable(false)
                    sweetAlretLoading?.contentText = "Export Data Gagal"
                    sweetAlretLoading?.confirmText = "OK"
                    sweetAlretLoading?.setConfirmClickListener { sDialog ->
                        sDialog?.let { if (it.isShowing) it.dismiss() }

                    }
                    sweetAlretLoading?.show()
                }
            })

            //Start Content

            //End Of Content
            /*val filename = fileName
            val pathDir = File("/sdcard/SIOJT/")
            pathDir.mkdirs()
            val outputFile = File(pathDir, filename)

            try {
                val out = FileOutputStream(outputFile)
                wb.write(out)
                out.close()

            }catch (e: Exception) {
                e.printStackTrace()
            }finally {
                onDismissLoading()

            }*/

        }, 10000)
    }

    @SuppressLint("SdCardPath")
    private fun openFile(filename: String) {

        val outputExportExcelFile = File(Environment.getExternalStorageDirectory().toString() + File.separator + "SIOJT" + File.separator + "Downloads", filename)
        //val path = Uri.fromFile(outputExportExcelFile)
        val path = FileProvider.getUriForFile(applicationContext, applicationContext.packageName, outputExportExcelFile)
        fileCsv = outputExportExcelFile

        var isfileCsv = false
        var isfilePdf = false
        val intent = Intent(Intent.ACTION_VIEW)
        // Check what kind of file you are trying to open, by comparing the url with extensions.
        // When the if condition is matched, plugin sets the correct intent (mime) type,
        // so Android knew what application to use to open the file
        if (filename.contains(".doc") || filename.contains(".docx")) {
            // Word document
            intent.setDataAndType(path, "application/msword")
        } else if ((filename.contains(".xls") || filename.contains(".xlsx")) && isCsv) {
            isfileCsv = true
        } else if ((filename.contains(".xls") || filename.contains(".xlsx")) && isPdfile) {
            // PDF file
            //intent.setDataAndType(path, "application/pdf")
            isfilePdf = true
        } else if (filename.contains(".ppt") || filename.contains(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(path, "application/vnd.ms-powerpoint")
        } else if (filename.contains(".xls") || filename.contains(".xlsx")) {
            // Excel file
            intent.setDataAndType(path, "application/vnd.ms-excel")
        } else if (filename.contains(".zip") || filename.contains(".rar")) {
            // WAV audio file
            intent.setDataAndType(path, "application/x-wav")
        } else if (filename.contains(".rtf")) {
            // RTF file
            intent.setDataAndType(path, "application/rtf")
        } else if (filename.contains(".wav") || filename.contains(".mp3")) {
            // WAV audio file
            intent.setDataAndType(path, "audio/x-wav")
        } else if (filename.contains(".gif")) {
            // GIF file
            intent.setDataAndType(path, "image/gif")
        } else if (filename.contains(".jpg") || filename.contains(".jpeg") || filename.contains(".png")) {
            // JPG file
            intent.setDataAndType(path, "image/jpeg")
        } else if (filename.contains(".txt")) {
            // Text file
            intent.setDataAndType(path, "text/plain")
        } else if (filename.contains(".3gp") || filename.contains(".mpg") || filename.contains(".mpeg") || filename.contains(".mpe") || filename.contains(".mp4") || filename.contains(".avi")) {
            // Video files
            intent.setDataAndType(path, "video/*")
        } else {
            //if you want you can also define the intent type for any other file

            //additionally use else clause below, to manage other unknown extensions
            //in this case, Android will show all applications installed on the device
            //so you can choose which application to use
            intent.setDataAndType(path, "*/*")
        }

        when {
            isfileCsv -> {
                Toast.makeText(this@MainActivity, "Export to csv", Toast.LENGTH_SHORT).show()
                convertExcelToCSV(fileCsv)
            }
            isfilePdf -> {
                Toast.makeText(this@MainActivity, "Export to pdf", Toast.LENGTH_SHORT).show()
                //convertDocToPdfitext()
            }
            else -> {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                try {
                    startActivity(Intent.createChooser(intent, "Pilih aplikasi"))
                } catch (e: ActivityNotFoundException) {
                    //Toast.makeText(this@Detailsurat_masuk_activity, "No Application available to view PDF", Toast.LENGTH_SHORT).show()
                    Toast.makeText(this@MainActivity, "Tidak ada aplikasi yang tersedia untuk membuka file", Toast.LENGTH_SHORT).show()
                }
            }
        }


    }

    private fun convertExcelToCSV(inputfile: File) {

        val data = StringBuffer()
        try {
            //val csvfile = File(Environment.getExternalStorageDirectory() + "/csvfile.csv")
            /*val reader = CSVReader(FileReader(file.absolutePath))
            var nextLine:Array<String>
            do {
                nextLine = reader.readNext()
                if(nextLine == null)
                    break
                // nextLine[] is an array of values from the line
                println(nextLine[0] + nextLine[1] + "etc...")
            }while (true)
            while ((nextLine = reader.readNext()) != null)
            {
                // nextLine[] is an array of values from the line
                println(nextLine[0] + nextLine[1] + "etc...")
            }*/
            // For storing data into CSV files
            /*

            val sheet = workBook

            // Iterate through each rows from first sheet
           */

            val workBook = XSSFWorkbook(FileInputStream(inputfile))
            for (i in 0 until workBook.numberOfSheets) {
                val mfilename = "${workBook.getSheetAt(i).sheetName}.csv"
                val pathDir = File(Environment.getExternalStorageDirectory().toString() + File.separator + "SIOJT", "Downloads")
                val outputFile = File(pathDir, mfilename)

                // For storing data into CSV files
                val fos = FileOutputStream(outputFile)
                val sheet = workBook.getSheetAt(i)
                var cell: Cell? = null
                var row: Row? = null

                // Iterate through each rows from first sheet
                val rowIterator = sheet.iterator()

                while (rowIterator.hasNext()) {
                    row = rowIterator.next()
                    // For each row, iterate through each columns
                    val cellIterator = row.cellIterator()
                    var columnNumber = 1
                    while (cellIterator.hasNext()) {
                        cell = cellIterator.next()
                        if (columnNumber > 1) {
                            data.append(",")
                        }
                        when (cell.cellTypeEnum) {
                            CellType.BOOLEAN -> {
                                data.append(cell.booleanCellValue)
                            }
                            CellType.NUMERIC -> {
                                data.append(cell.numericCellValue)
                            }
                            CellType.STRING -> {
                                data.append(cell.stringCellValue)
                            }
                            CellType.BLANK -> {
                                data.append(cell.stringCellValue)
                            }
                            else -> {
                                data.append(cell)
                            }
                        }
                        columnNumber++
                    }

                    data.append('\n')
                }
                fos.write(data.toString().toByteArray())
                fos.close()
            }
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
            Toast.makeText(this, "The specified file was not found", Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, "Error convert excel to csv", Toast.LENGTH_SHORT).show()
        }

    }

    //create pdf with aspose
    private fun convertToPdf(inputfile: File) {
        /*val workBook = XSSFWorkbook(FileInputStream(inputfile))
        try {
            val pathDir = Environment.getExternalStorageDirectory().toString() + File.separator + "SIOJT" + File.separator + "Downloads" + File.separator
            val fileName = pathDir + inputfile.name
            //Open an existing Excel file

            val workbook = Workbook(pathDir + inputfile.name)

            //Define PdfSaveOptions
            val saveOptions = PdfSaveOptions()
            saveOptions.compliance = PdfCompliance.PDF_A_1_B
            saveOptions.createdTime = DateTime.getNow()

            //Save the PDF file
            workbook.save(pathDir + "test.pdf", saveOptions)

        } catch (e: FileNotFoundException) {
            e.printStackTrace()
            Toast.makeText(this, "The specified file was not found", Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, "Error convert excel to pdf", Toast.LENGTH_SHORT).show()
        }*/

    }

    private fun convertDocToPdfitext(fileName: String, projectAssignProjectId: Int?, projectName: String?) {
        val document: Document = Document(PageSize.A2.rotate(), 20f, 20f, 35f, 20f)
        try {
            val pathDir = File(Environment.getExternalStorageDirectory().toString() + File.separator + "SIOJT", "Downloads")
            val outputFile = File(pathDir, "siojt.pdf")
            val fos = FileOutputStream(outputFile)

            val pdfWriter = PdfWriter.getInstance(document, fos)
            document.open()

            //PdfFont bold = PdfFontFactory.createFont(FontConstants.TIMES_BOLD);
            val fontBold = Font(Font.FontFamily.HELVETICA, 16f, Font.BOLD)

            val headerTitle1 = Paragraph("DAFTAR NOMINATIF", fontBold)
            headerTitle1.alignment = Element.ALIGN_CENTER
            headerTitle1.spacingBefore = 20f

            val headerTitleNamaProyek = Paragraph(projectName.toString().toUpperCase(), fontBold)
            headerTitleNamaProyek.alignment = Element.ALIGN_CENTER

            val headerTitleTempatProyek = Paragraph("DESA BURANGKENG KECAMATAN SETU", fontBold)
            headerTitleTempatProyek.alignment = Element.ALIGN_CENTER
            headerTitleTempatProyek.spacingAfter = 20f

            val headerTitleNomorProyek = Paragraph("NOMOR : 14 / PENG-10.05 / VII / 2019", fontBold)
            headerTitleNomorProyek.alignment = Element.ALIGN_CENTER
            headerTitleNomorProyek.spacingAfter = 17f

            val headerTitleTglProyek = Paragraph("TANGGAL : 05 Juli 2019", fontBold)
            headerTitleTglProyek.alignment = Element.ALIGN_CENTER
            headerTitleTglProyek.spacingAfter = 17f

            document.add(headerTitle1)
            document.add(headerTitleNamaProyek)
            document.add(headerTitleTempatProyek)
            document.add(headerTitleNomorProyek)
            document.add(headerTitleTglProyek)

            val cellOwner = createSecondTable(projectAssignProjectId)

            document.add(createFirstTable(fileName, projectAssignProjectId, projectName))
            document.add(cellOwner)

            document.close()
            pdfWriter.close()
        } catch (e: DocumentException) {
            Log.d("DocumentException", e.message.toString())
        } catch (e: Exception) {
            Log.d("Exception", e.message.toString())
        }
    }

    private fun createFirstTable(fileName: String, projectAssignProjectId: Int?, projectName: String?): PdfPTable {
        // a table with three columns
        /*val table = PdfPTable(5)

        table.setWidths(intArrayOf(1, 2, 2, 2, 1))
        var cell:PdfPCell
        cell = PdfPCell(Phrase("S/N"))
        cell.setRowspan(2)
        table.addCell(cell)
        cell = PdfPCell(Phrase("Name"))
        cell.setColspan(3)
        table.addCell(cell)
        cell = PdfPCell(Phrase("Age"))
        cell.setRowspan(2)
        table.addCell(cell)
        table.addCell("SURNAME")
        table.addCell("FIRST NAME")
        table.addCell("MIDDLE NAME")
        table.addCell("1")
        table.addCell("James")
        table.addCell("Fish")
        table.addCell("Stone")
        table.addCell("17")*/

        /*cell = PdfPCell(Phrase("Cell with rowspan 2"))
        cell.setRowspan(2)
        table.addCell(cell)
        table.addCell("row 1; cell 1")
        table.addCell("row 1; cell 2")
        table.addCell("row 2; cell 1")
        table.addCell("row 2; cell 2")*/


        val table = PdfPTable(27)
        //setColumnWidth
        //no - pemilik1 - pemilik2 - nobidang - letak - luas tanah (+ 3 chil column)
        table.setTotalWidth(floatArrayOf(20f, 144f, 144f, 30f, 100f,
                30f, 30f, 30f, 30f, 40f,
                40f, 30f, 80f, 80f, 30f,
                80f, 20f, 20f, 20f, 20f,
                30f, 30f, 50f, 50f, 50f,
                50f, 50f))
        table.widthPercentage = 100f

        // the cell object
        // we add a cell with colspan 3
        table.addCell(createCellWithColspan("PIHAK YANG BERHAK", 3, 50f))
        // now we add a cell with colspan 7
        table.addCell(createCellWithColspan("TANAH", 7, 50f))
        // now we add a cell with colspan 2
        table.addCell(createCellWithColspan("RUANG ATAS TANAH DAN BAWAH TANAH", 2, 50f))
        // now we add a cell with colspan 3
        table.addCell(createCellWithColspan("BANGUNAN", 3, 50f))
        // now we add a cell with colspan 7
        table.addCell(createCellWithColspan("TANAMAN", 7, 50f))
        // now we add a cell with colspan 2
        table.addCell(createCellWithColspan("BENDA LAIN YANG BERKAITAN DENGAN TANAH", 2, 50f))

        // now we add a cell with rowspan 2
        table.addCell(createCellWithRowspan("Pembebasan Hak Atas Tanah/Piducia", 3, 50f))

        // now we add a cell with rowspan 2
        table.addCell(createCellWithRowspan("Perkiraan Dampak Dari Rencana Pembangunan", 3, 50f))

        // now we add a cell with rowspan 2
        table.addCell(createCellWithRowspan("KET", 3, 50f))

        //baris dua & 3
        table.addCell(createCellWithRowspan("No.", 2, 25f))
        table.addCell(createCellWithRowspan("Pemilik", 2, 25f))
        table.addCell(createCellWithRowspan("Menguasai/Menggarap/Menyewa", 2, 25f))

        //tanah
        table.addCell(createCellWithRowspan("No Bidang", 2, 25f))
        table.addCell(createCellWithRowspan("Letak", 2, 25f))
        table.addCell(createCellWithRowspan("Luas (m2)", 2, 25f))
//        table.addCell(createCellWithColspan("Luas (m2)", 3, 25f))
        table.addCell(createCellWithRowspan("Status Tanah", 2, 25f))
        table.addCell(createCellWithRowspan("Surat Tanda Bukti/Alas Hak", 2, 25f))

        //ruang atas dan bawah
        table.addCell(createCellWithRowspan("HM/Sarusun/Lainnya", 2, 25f))
        table.addCell(createCellWithRowspan("Luas(m2)", 2, 25f))

        //bangunan
        table.addCell(createCellWithRowspan("Jenis", 2, 25f))
        table.addCell(createCellWithRowspan("Jumlah", 2, 25f))

        //tanaman
        table.addCell(createCellWithRowspan("Jenis", 2, 25f))
        table.addCell(createCellWithColspan("Kelas Tanaman", 4, 25f))
        table.addCell(createCellWithRowspan("Jumlah", 2, 25f))

        //sarana pelengkap
        table.addCell(createCellWithRowspan("Jenis", 2, 25f))
        table.addCell(createCellWithRowspan("Jumlah", 2, 25f))

        table.addCell(createCellNoramal("Terkena", 25f))
        table.addCell(createCellNoramal("Sisa Atas", 25f))
        table.addCell(createCellNoramal("Sisa Bawah", 25f))

        table.addCell(createCellNoramal("K", 25f))
        table.addCell(createCellNoramal("S", 25f))
        table.addCell(createCellNoramal("B", 25f))
        table.addCell(createCellNoramal("R", 25f))

        //number of header table
        table.addCell(createCellNoramal("1", 25f))
        table.addCell(createCellNoramal("2", 25f))
        table.addCell(createCellNoramal("3", 25f))
        table.addCell(createCellNoramal("4", 25f))
        table.addCell(createCellNoramal("5", 25f))
        table.addCell(createCellWithColspan("6", 3, 25f))
        table.addCell(createCellNoramal("7", 25f))
        table.addCell(createCellNoramal("8", 25f))
        table.addCell(createCellNoramal("9", 25f))
        table.addCell(createCellNoramal("10", 25f))
        table.addCell(createCellNoramal("11", 25f))
        table.addCell(createCellWithColspan("12", 2, 25f))
        table.addCell(createCellNoramal("13", 25f))
        table.addCell(createCellNoramal("14", 25f))
        table.addCell(createCellNoramal("15", 25f))
        table.addCell(createCellNoramal("16", 25f))
        table.addCell(createCellNoramal("17", 25f))
        table.addCell(createCellNoramal("18", 25f))
        table.addCell(createCellNoramal("19", 25f))

        table.addCell(createCellNoramal("20", 25f))
        table.addCell(createCellNoramal("21", 25f))

        table.addCell(createCellNoramal("22", 25f))
        table.addCell(createCellNoramal("23", 25f))
        table.addCell(createCellNoramal("24", 25f))


        /*
        table.setLockedWidth(true)
        var cell:PdfPCell
        cell = PdfPCell(Phrase("Table 5"))
        cell.setColspan(3)
        table.addCell(cell)
        cell = PdfPCell(Phrase("Cell with rowspan 2"))
        cell.setRowspan(2)
        table.addCell(cell)*/

        // we add the four remaining cells with addCell()
        /*table.addCell("row 1; cell 1")
        table.addCell("row 1; cell 2")
        table.addCell("row 2; cell 1")
        table.addCell("row 2; cell 2")*/
        return table
    }

    private fun createSecondTable(projectAssignProjectId: Int?): PdfPTable? {
        /*val array = arrayOfNulls<Array<String?>>(3)
        var column = 0
        var row = 0

        for (i in 1..15) {
            if (column == 0) {
                array[row] = arrayOfNulls(5)
            }
            array[row++]!![column] = "cell $i"
            if (row == 3) {
                column++
                row = 0
            }
        }

        for (r in array)
        {
            for (c in r!!)
            {
                table.addCell(c)
            }
        }*/

        val table = PdfPTable(14)
        table.setTotalWidth(floatArrayOf(20f, 144f, 144f, 30f, 100f,
                30f, 30f, 30f, 30f, 40f,
                40f, 30f, 80f, 80f))
        table.widthPercentage = 100f
        //table.addCell(createCellNoramal("1", 25f))
        //table.addCell(createCellNoramal("2", 25f))

        /*val dataOfPemilikPertama = createdataPdf(projectAssignProjectId)
        if(dataOfPemilikPertama.isEmpty()){
            Log.d("kuy", "kosong jing")
        }
        for (element in dataOfPemilikPertama) {
            Log.d("kuy", element)
            table.addCell(createCellNoramal(element, 25f))
        }*/
        realm.executeTransaction {
            val results = it.where(PartyAdd::class.java).equalTo("ProjectId", projectAssignProjectId).findAll()
            if (results != null) {
                for ((index, value) in results.withIndex()) {
                    //1
                    table.addCell(createCellCustom("${index + 1}", Element.ALIGN_CENTER, Element.ALIGN_TOP))

                    val nomordIdentitas: String? = if (value.partyIdentityNumber!!.isNotEmpty()) value.partyIdentityNumber else "-"
                    val tmptLahirPB: String? = if (value.partyBirthPlaceName!!.isNotEmpty()) value.partyBirthPlaceName else "-"
                    val tglLahirPB: String? = if (value.partyBirthDate!!.isNotEmpty()) value.partyBirthDate else "-"
                    val pekerjaan: String? = if (value.partyOccupationName!!.isNotEmpty()) value.partyOccupationName else "-"
                    val alamatJalan: String? = if (value.partyStreetName!!.isNotEmpty()) value.partyStreetName else "-"
                    val alamatNomorRumah: String? = if (value.partyNumber!!.isNotEmpty()) value.partyNumber else "-"
                    val alamatRT: String? = if (value.partyRT!!.isNotEmpty()) value.partyRT else "-"
                    val alamatRW: String? = if (value.partyRW!!.isNotEmpty()) value.partyRW else "-"
                    val alamatBlok: String? = if (value.partyBlock!!.isNotEmpty()) value.partyBlock else "-"
                    val alamatKelurahan: String? = if (value.partyVillageName!!.isNotEmpty()) value.partyVillageName else "-"
                    val alamatKecamatan: String? = if (value.partyDistrictName!!.isNotEmpty()) value?.partyDistrictName else "-"
                    val alamatKotaKabupaten: String? = if (value.partyRegencyName!!.isNotEmpty()) value.partyRegencyName else "-"
                    val alamatProvinsi: String? = if (value.partyProvinceName!!.isNotEmpty()) value.partyProvinceName else "-"
                    val jenisKepemilikan: String? = if (value.partyOwnershipType!!.isNotEmpty()) value.partyOwnershipType else "-"

                    val pemilikPertama =
                            """
                            a.  Nama                    :  ${value.partyFullName}
                            b.  Tanggal Lahir        :  $tmptLahirPB, $tglLahirPB
                            c.  Pekerjaan             :  $pekerjaan
                            d.  Alamat                  :  Jl.$alamatJalan No.$alamatNomorRumah Blok $alamatBlok RT $alamatRT RW $alamatRW $alamatKelurahan $alamatKecamatan $alamatKotaKabupaten $alamatProvinsi
                            e.  NIK/No.KTP          :  $nomordIdentitas
                            f.  Kepemilikan          :  $jenisKepemilikan
                            
                            
                            
                            
                            """.trimIndent()

                    //2
                    table.addCell(createCellWithOutHeight(pemilikPertama))

                    //sample column 3
                    //table.addCell(createCellWithOutHeight(""))

                    //project Land
                    val pjland = it.where(ProjectLand::class.java)
                            .equalTo("PartyIdTemp", value.tempId)
                            .and()
                            .equalTo("ProjectId", value.projectId)
                            .findAll()

                    Log.d("tempId", pjland.toString())

                    if (pjland.isNotEmpty()) {
                        lannumber = ""
                        letak = ""
                        landaffected = ""
                        sisaatas = ""
                        sisabawah = ""
                        kepemilikan = ""
                        tandabukti = ""
                        jenisbangunan = ""
                        luasbangunan = ""
                        jenistanaman = ""
                        k = ""
                        s = ""
                        b = ""
                        r = ""
                        jumlahtanaman = ""
                        satuantanaman = ""
                        fasilitasLain = ""
                        fasilitasValue = ""
                        landInfopiducia = ""
                        f1 = ""
                        f2 = ""
                        st1 = ""
                        st2 = ""
                        rf1 = ""
                        rf2 = ""
                        rc1 = ""
                        rc2 = ""
                        cl1 = ""
                        cl2 = ""
                        dw1 = ""
                        dw2 = ""
                        fl1 = ""
                        fl2 = ""
                        wl1 = ""
                        wl2 = ""
                        menguasai = ""

                        for ((index, valueTanah) in pjland.withIndex()) {
                            val dataPihakKedua = it.where(Subjek2::class.java).equalTo("LandIdTemp", valueTanah.landIdTemp).findAll()
                            if (dataPihakKedua.isNotEmpty()) {
                                for ((indexPemilikKedua, valuePemilikKedua) in dataPihakKedua.withIndex()) {
                                    val nomordIdentitasPenggarap: String? = if (valuePemilikKedua.subjek2IdentityNumber!!.isNotEmpty()) valuePemilikKedua.subjek2IdentityNumber else "-"
                                    val tmptLahirPenggarap: String? = if (valuePemilikKedua.subjek2BirthPlaceName!!.isNotEmpty()) valuePemilikKedua.subjek2BirthPlaceName else "-"
                                    val tglLahirPenggarap: String? = if (valuePemilikKedua.subjek2BirthDate!!.isNotEmpty()) valuePemilikKedua.subjek2BirthDate else "-"
                                    val pekerjaanPenggarap: String? = if (valuePemilikKedua.subjek2OccupationName!!.isNotEmpty()) valuePemilikKedua.subjek2OccupationName else "-"
                                    val alamatJalanPenggarap: String? = if (valuePemilikKedua.subjek2StreetName!!.isNotEmpty()) valuePemilikKedua.subjek2StreetName else "-"
                                    val alamatNomorRumahPenggarap: String? = if (valuePemilikKedua.subjek2Number!!.isNotEmpty()) valuePemilikKedua.subjek2Number else "-"
                                    val alamatRTPenggarap: String? = if (valuePemilikKedua.subjek2RT!!.isNotEmpty()) valuePemilikKedua.subjek2RT else "-"
                                    val alamatRWPenggarap: String? = if (valuePemilikKedua.subjek2RW!!.isNotEmpty()) valuePemilikKedua.subjek2RW else "-"
                                    val alamatBlokPenggarap: String? = if (valuePemilikKedua.subjek2Block!!.isNotEmpty()) valuePemilikKedua.subjek2Block else "-"
                                    val alamatKelurahanPenggarap: String? = if (valuePemilikKedua.subjek2VillageName!!.isNotEmpty()) valuePemilikKedua.subjek2VillageName else "-"
                                    val alamatKecamatanPenggarap: String? = if (valuePemilikKedua.subjek2DistrictName!!.isNotEmpty()) valuePemilikKedua.subjek2DistrictName else "-"
                                    val alamatKotaKabupatenPenggarap: String? = if (valuePemilikKedua.subjek2RegencyName!!.isNotEmpty()) valuePemilikKedua.subjek2RegencyName else "-"
                                    val alamatProvinsiPenggarap: String? = if (valuePemilikKedua.subjek2ProvinceName!!.isNotEmpty()) valuePemilikKedua.subjek2ProvinceName else "-"

                                    val pihakKedua =
                                            """
                                            a.  Nama                    :  ${valuePemilikKedua.subjek2FullName}}
                                            b.  Tanggal Lahir       :  $tmptLahirPenggarap, $tglLahirPenggarap
                                            c.  Pekerjaan             :  $pekerjaanPenggarap
                                            d.  Alamat                  :  Jl.$alamatJalanPenggarap No.$alamatNomorRumahPenggarap Blok $alamatBlokPenggarap RT $alamatRTPenggarap RW $alamatRWPenggarap $alamatKelurahanPenggarap $alamatKecamatanPenggarap $alamatKotaKabupatenPenggarap $alamatProvinsiPenggarap
                                            e.  NIK/No.KTP          :  $nomordIdentitasPenggarap
                                            f.  Kepemilikan          :  $jenisKepemilikan



                                            """.trimIndent()

                                    menguasai += pihakKedua
                                    //3
                                    //table.addCell(createCellWithOutHeight(menguasai))
                                }//looping pihak kedua
                            } else {
                                menguasai = ""
                            }

                            lannumber +=
                                    """
                                    ${valueTanah.landNumberList}
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    """.trimIndent()

                            landaffected +=
                                    """
                                    ${valueTanah.landAreaAffected}
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    """.trimIndent()


                            val letaktanahRT = if (valueTanah.landRT!!.isNotEmpty()) valueTanah.landRT else "-"
                            val letaktanahRW = if (valueTanah.landRW!!.isNotEmpty()) valueTanah.landRW else "-"
                            val letaktanahKelurahan = if (valueTanah.landVillageName!!.isNotEmpty()) valueTanah.landVillageName else "-"
                            val letaktanahKecamatan = if (valueTanah.landDistrictName!!.isNotEmpty()) valueTanah.landDistrictName else "-"
                            val letaktanahKota = if (valueTanah.landRegencyName!!.isNotEmpty()) valueTanah.landRegencyName else "-"
                            val letaktanahProvinsi = if (valueTanah.landProvinceName!!.isNotEmpty()) valueTanah.landProvinceName else "-"
                            letak +=
                                    """
                                    a.  RT/RW              :  $letaktanahRT/$letaktanahRW
                                    b.  Kelurahan         :  $letaktanahKelurahan
                                    c.  Kecamatan        :  $letaktanahKecamatan
                                    d.  Kota                  :  $letaktanahKota
                                    e.  Provinsi             :  $letaktanahProvinsi
                                        
                                        
    
                                    """.trimIndent()

                            val jenisKepemiikan: String? = if (valueTanah.landOwnershipNameOther!!.isNotEmpty()) {
                                valueTanah.landOwnershipNameOther
                            } else {
                                valueTanah.landOwnershipName
                            }
                            kepemilikan +=
                                    """
                                    $jenisKepemiikan ${valueTanah.landCertificateNumber}
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    """.trimIndent()


                            /* bangunan */
                            val bangunan = it.where(Bangunan::class.java)
                                    .equalTo("LandIdTemp", valueTanah.landIdTemp)
                                    .and()
                                    .equalTo("ProjectId", valueTanah.projectId)
                                    .findAll()

                            if (bangunan.isNotEmpty()) {
                                for ((indexBangunan, valueBangunan) in bangunan.withIndex()) {
                                    //spesifikasi bangunan
                                    //pondasi
                                    val convertPondasiBangunanId = valueBangunan.foundationTypeId
                                    val convertPondasiBangunanId2 = convertPondasiBangunanId!!.replace("[", "['")
                                    val convertPondasiBangunanId3 = convertPondasiBangunanId2.replace("]", "']")
                                    val convertPondasiBangunanId4 = convertPondasiBangunanId3.replace(", ", "','")

                                    val convertPondasiBangunanName = valueBangunan.foundationTypeName
                                    val convertPondasiBangunanName2 = convertPondasiBangunanName!!.replace("[", "['")
                                    val convertPondasiBangunanName3 = convertPondasiBangunanName2.replace("]", "']")
                                    val convertPondasiBangunanName4 = convertPondasiBangunanName3.replace(", ", "','")

                                    val convertPondasiBangunanNameOther = valueBangunan.foundationTypeNameOther
                                    val convertPondasiBangunanNameOther2 = convertPondasiBangunanNameOther!!.replace("[", "['")
                                    val convertPondasiBangunanNameOther3 = convertPondasiBangunanNameOther2.replace("]", "']")
                                    val convertPondasiBangunanNameOther4 = convertPondasiBangunanNameOther3.replace(", ", "','")

                                    val convertPondasiVolume = valueBangunan.pbFoundationTypeAreaTotal
                                    val convertPondasiVolume2 = convertPondasiVolume!!.replace("[", "['")
                                    val convertPondasiVolume3 = convertPondasiVolume2.replace("]", "']")
                                    val convertPondasiVolume4 = convertPondasiVolume3.replace(", ", "','")

                                    val dataPondasiId = Gson().fromJson(convertPondasiBangunanId4, Array<String>::class.java).toList()
                                    val dataPondasiName = Gson().fromJson(convertPondasiBangunanName4, Array<String>::class.java).toList()
                                    val dataPondasiNameOther = Gson().fromJson(convertPondasiBangunanNameOther4, Array<String>::class.java).toList()
                                    val dataPondasiVolume = Gson().fromJson(convertPondasiVolume4, Array<String>::class.java).toList()

                                    /*struktur*/
                                    val convertStrukturBangunanId = valueBangunan.structureTypeId
                                    val convertStrukturBangunanId2 = convertStrukturBangunanId!!.replace("[", "['")
                                    val convertStrukturBangunanId3 = convertStrukturBangunanId2.replace("]", "']")
                                    val convertStrukturBangunanId4 = convertStrukturBangunanId3.replace(", ", "','")

                                    val convertStrukturBangunanName = valueBangunan.structureTypeName
                                    val convertStrukturBangunanName2 = convertStrukturBangunanName!!.replace("[", "['")
                                    val convertStrukturBangunanName3 = convertStrukturBangunanName2.replace("]", "']")
                                    val convertStrukturBangunanName4 = convertStrukturBangunanName3.replace(", ", "','")

                                    val convertStrukturBangunanNameOther = valueBangunan.structureTypeNameOther
                                    val convertStrukturBangunanNameOther2 = convertStrukturBangunanNameOther!!.replace("[", "['")
                                    val convertStrukturBangunanNameOther3 = convertStrukturBangunanNameOther2.replace("]", "']")
                                    val convertStrukturBangunanNameOther4 = convertStrukturBangunanNameOther3.replace(", ", "','")

                                    val convertStrukturBangunanVolume = valueBangunan.pbStructureTypeAreaTotal
                                    val convertStrukturBangunanVolume2 = convertStrukturBangunanVolume!!.replace("[", "['")
                                    val convertStrukturBangunanVolume3 = convertStrukturBangunanVolume2.replace("]", "']")
                                    val convertStrukturBangunanVolume4 = convertStrukturBangunanVolume3.replace(", ", "','")

                                    val dataStrukturId = Gson().fromJson(convertStrukturBangunanId4, Array<String>::class.java).toList()
                                    val dataStrukturName = Gson().fromJson(convertStrukturBangunanName4, Array<String>::class.java).toList()
                                    val dataStrukturNameOther = Gson().fromJson(convertStrukturBangunanNameOther4, Array<String>::class.java).toList()
                                    val dataStrukturVolume = Gson().fromJson(convertStrukturBangunanVolume4, Array<String>::class.java).toList()


                                    /*kerangka atap */
                                    val convertKeratapBangunanId = valueBangunan.roofFrameTypeId
                                    val convertKeratapBangunanId2 = convertKeratapBangunanId!!.replace("[", "['")
                                    val convertKeratapBangunanId3 = convertKeratapBangunanId2.replace("]", "']")
                                    val convertKeratapBangunanId4 = convertKeratapBangunanId3.replace(", ", "','")

                                    val convertKeratapBangunanName = valueBangunan.roofFrameTypeName
                                    val convertKeratapBangunanName2 = convertKeratapBangunanName!!.replace("[", "['")
                                    val convertKeratapBangunanName3 = convertKeratapBangunanName2.replace("]", "']")
                                    val convertKeratapBangunanName4 = convertKeratapBangunanName3.replace(", ", "','")

                                    val convertKeratapBangunanNameOther = valueBangunan.roofFrameTypeNameOther
                                    val convertKeratapBangunanNameOther2 = convertKeratapBangunanNameOther!!.replace("[", "['")
                                    val convertKeratapBangunanNameOther3 = convertKeratapBangunanNameOther2.replace("]", "']")
                                    val convertKeratapBangunanNameOther4 = convertKeratapBangunanNameOther3.replace(", ", "','")

                                    val convertKeratapBangunanVolume = valueBangunan.pbRoofFrameTypeAreaTotal
                                    val convertKeratapBangunanVolume2 = convertKeratapBangunanVolume!!.replace("[", "['")
                                    val convertKeratapBangunanVolume3 = convertKeratapBangunanVolume2.replace("]", "']")
                                    val convertKeratapBangunanVolume4 = convertKeratapBangunanVolume3.replace(", ", "','")

                                    val dataKeratapId = Gson().fromJson(convertKeratapBangunanId4, Array<String>::class.java).toList()
                                    val dataKeratapName = Gson().fromJson(convertKeratapBangunanName4, Array<String>::class.java).toList()
                                    val dataKeratapNameOther = Gson().fromJson(convertKeratapBangunanNameOther4, Array<String>::class.java).toList()
                                    val dataKeratapVolume = Gson().fromJson(convertKeratapBangunanVolume4, Array<String>::class.java).toList()


                                    /*penutup atap */
                                    val convertPenutapBangunanId = valueBangunan.roofCoveringTypeId
                                    val convertPenutapBangunanId2 = convertPenutapBangunanId!!.replace("[", "['")
                                    val convertPenutapBangunanId3 = convertPenutapBangunanId2.replace("]", "']")
                                    val convertPenutapBangunanId4 = convertPenutapBangunanId3.replace(", ", "','")

                                    val convertPenutapBangunanName = valueBangunan.roofCoveringTypeName
                                    val convertPenutapBangunanNam2 = convertPenutapBangunanName!!.replace("[", "['")
                                    val convertPenutapBangunanNam3 = convertPenutapBangunanNam2.replace("]", "']")
                                    val convertPenutapBangunanNam4 = convertPenutapBangunanNam3.replace(", ", "','")

                                    val convertPenutapBangunanNameOther = valueBangunan.roofCoveringTypeNameOther
                                    val convertPenutapBangunanNameOther2 = convertPenutapBangunanNameOther!!.replace("[", "['")
                                    val convertPenutapBangunanNameOther3 = convertPenutapBangunanNameOther2.replace("]", "']")
                                    val convertPenutapBangunanNameOther4 = convertPenutapBangunanNameOther3.replace(", ", "','")

                                    val convertPenutapBangunanVolume = valueBangunan.pbRoofCoveringTypeAreaTotal
                                    val convertPenutapBangunanVolum2 = convertPenutapBangunanVolume!!.replace("[", "['")
                                    val convertPenutapBangunanVolum3 = convertPenutapBangunanVolum2.replace("]", "']")
                                    val convertPenutapBangunanVolum4 = convertPenutapBangunanVolum3.replace(", ", "','")

                                    val dataPenutapId = Gson().fromJson(convertPenutapBangunanId4, Array<String>::class.java).toList()
                                    val dataPenutapName = Gson().fromJson(convertPenutapBangunanNam4, Array<String>::class.java).toList()
                                    val dataPenutapNameOther = Gson().fromJson(convertPenutapBangunanNameOther4, Array<String>::class.java).toList()
                                    val dataPenutapVolume = Gson().fromJson(convertPenutapBangunanVolum4, Array<String>::class.java).toList()

                                    /* plafon */
                                    val convertPlafonBangunanId = valueBangunan.ceilingTypeId
                                    val convertPlafonBangunanId2 = convertPlafonBangunanId!!.replace("[", "['")
                                    val convertPlafonBangunanId3 = convertPlafonBangunanId2.replace("]", "']")
                                    val convertPlafonBangunanId4 = convertPlafonBangunanId3.replace(", ", "','")

                                    val convertPlafonBangunanName = valueBangunan.ceilingTypeName
                                    val convertPlafonBangunanName2 = convertPlafonBangunanName!!.replace("[", "['")
                                    val convertPlafonBangunanName3 = convertPlafonBangunanName2.replace("]", "']")
                                    val convertPlafonBangunanName4 = convertPlafonBangunanName3.replace(", ", "','")

                                    val convertPlafonBangunanNameOther = valueBangunan.ceilingTypeNameOther
                                    val convertPlafonBangunanNameOther3 = convertPlafonBangunanNameOther!!.replace("[", "['")
                                    val convertPlafonBangunanNameOther4 = convertPlafonBangunanNameOther3.replace("]", "']")
                                    val convertPlafonBangunanNameOther5 = convertPlafonBangunanNameOther4.replace(", ", "','")

                                    val convertPlafonBangunanVolume = valueBangunan.pbCeilingTypeAreaTotal
                                    val convertPlafonBangunanVolume2 = convertPlafonBangunanVolume!!.replace("[", "['")
                                    val convertPlafonBangunanVolume3 = convertPlafonBangunanVolume2.replace("]", "']")
                                    val convertPlafonBangunanVolume4 = convertPlafonBangunanVolume3.replace(", ", "','")

                                    val dataPlafonId = Gson().fromJson(convertPlafonBangunanId4, Array<String>::class.java).toList()
                                    val dataPlafonName = Gson().fromJson(convertPlafonBangunanName4, Array<String>::class.java).toList()
                                    val dataPlafonNameOther = Gson().fromJson(convertPlafonBangunanNameOther5, Array<String>::class.java).toList()
                                    val dataPlafonVolume = Gson().fromJson(convertPlafonBangunanVolume4, Array<String>::class.java).toList()

                                    /* Dinding */
                                    val convertDindingBangunanId = valueBangunan.wallTypeId
                                    val convertDindingBangunanId2 = convertDindingBangunanId!!.replace("[", "['")
                                    val convertDindingBangunanId3 = convertDindingBangunanId2.replace("]", "']")
                                    val convertDindingBangunanId4 = convertDindingBangunanId3.replace(", ", "','")

                                    val convertDindingBangunanName = valueBangunan.wallTypeName
                                    val convertDindingBangunanName2 = convertDindingBangunanName!!.replace("[", "['")
                                    val convertDindingBangunanName3 = convertDindingBangunanName2.replace("]", "']")
                                    val convertDindingBangunanName4 = convertDindingBangunanName3.replace(", ", "','")

                                    val convertDindingBangunanNameOther = valueBangunan.wallTypeNameOther
                                    val convertDindingBangunanNameOther2 = convertDindingBangunanNameOther!!.replace("[", "['")
                                    val convertDindingBangunanNameOther3 = convertDindingBangunanNameOther2.replace("]", "']")
                                    val convertDindingBangunanNameOther4 = convertDindingBangunanNameOther3.replace(", ", "','")

                                    val convertDindingBangunanVolume = valueBangunan.pbfWallTypeAreaTotal
                                    val convertDindingBangunanVolume2 = convertDindingBangunanVolume!!.replace("[", "['")
                                    val convertDindingBangunanVolume3 = convertDindingBangunanVolume2.replace("]", "']")
                                    val convertDindingBangunanVolume4 = convertDindingBangunanVolume3.replace(", ", "','")

                                    val dataDindingId = Gson().fromJson(convertDindingBangunanId4, Array<String>::class.java).toList()
                                    val dataDindingName = Gson().fromJson(convertDindingBangunanName4, Array<String>::class.java).toList()
                                    val dataDindingNameOther = Gson().fromJson(convertDindingBangunanNameOther4, Array<String>::class.java).toList()
                                    val dataDindingVolume = Gson().fromJson(convertDindingBangunanVolume4, Array<String>::class.java).toList()

                                    /* Pintu jendela */
                                    val convertPinjelBangunanId = valueBangunan.doorWindowTypeld
                                    val convertPinjelBangunanId2 = convertPinjelBangunanId!!.replace("[", "['")
                                    val convertPinjelBangunanId3 = convertPinjelBangunanId2.replace("]", "']")
                                    val convertPinjelBangunanId4 = convertPinjelBangunanId3.replace(", ", "','")

                                    val convertPinjelBangunanName = valueBangunan.doorWindowTypeName
                                    val convertPinjelBangunanName2 = convertPinjelBangunanName!!.replace("[", "['")
                                    val convertPinjelBangunanName3 = convertPinjelBangunanName2.replace("]", "']")
                                    val convertPinjelBangunanName4 = convertPinjelBangunanName3.replace(", ", "','")

                                    val convertPinjelBangunanNameOther = valueBangunan.doorWindowTypeNameOther
                                    val convertPinjelBangunanNameOther2 = convertPinjelBangunanNameOther!!.replace("[", "['")
                                    val convertPinjelBangunanNameOther3 = convertPinjelBangunanNameOther2.replace("]", "']")
                                    val convertPinjelBangunanNameOther4 = convertPinjelBangunanNameOther3.replace(", ", "','")

                                    val convertPinjelBangunanVolume = valueBangunan.pbDoorWindowTypeAreaTotal
                                    val convertPinjelBangunanVolume2 = convertPinjelBangunanVolume!!.replace("[", "['")
                                    val convertPinjelBangunanVolume3 = convertPinjelBangunanVolume2.replace("]", "']")
                                    val convertPinjelBangunanVolume4 = convertPinjelBangunanVolume3.replace(", ", "','")

                                    val dataPinjelId = Gson().fromJson(convertPinjelBangunanId4, Array<String>::class.java).toList()
                                    val dataPinjelName = Gson().fromJson(convertPinjelBangunanName4, Array<String>::class.java).toList()
                                    val dataPinjelNameOther = Gson().fromJson(convertPinjelBangunanNameOther4, Array<String>::class.java).toList()
                                    val dataPinjelVolume = Gson().fromJson(convertPinjelBangunanVolume4, Array<String>::class.java).toList()


                                    /* Lantai */
                                    val convertLantaiBangunanId = valueBangunan.floorTypeId
                                    val convertLantaiBangunanId2 = convertLantaiBangunanId!!.replace("[", "['")
                                    val convertLantaiBangunanId3 = convertLantaiBangunanId2.replace("]", "']")
                                    val convertLantaiBangunanId4 = convertLantaiBangunanId3.replace(", ", "','")

                                    val convertLantaiBangunanName = valueBangunan.floorTypeName
                                    val convertLantaiBangunanName2 = convertLantaiBangunanName!!.replace("[", "['")
                                    val convertLantaiBangunanName3 = convertLantaiBangunanName2.replace("]", "']")
                                    val convertLantaiBangunanName4 = convertLantaiBangunanName3.replace(", ", "','")

                                    val convertLantaiBangunanNameOther = valueBangunan.floorTypeNameOther
                                    val convertLantaiBangunanNameOther2 = convertLantaiBangunanNameOther!!.replace("[", "['")
                                    val convertLantaiBangunanNameOther3 = convertLantaiBangunanNameOther2.replace("]", "']")
                                    val convertLantaiBangunanNameOther4 = convertLantaiBangunanNameOther3.replace(", ", "','")

                                    val convertLantaiBangunanVolume = valueBangunan.pbFloorTypeAreaTotal
                                    val convertLantaiBangunanVolume2 = convertLantaiBangunanVolume!!.replace("[", "['")
                                    val convertLantaiBangunanVolume3 = convertLantaiBangunanVolume2.replace("]", "']")
                                    val convertLantaiBangunanVolume4 = convertLantaiBangunanVolume3.replace(", ", "','")

                                    val dataLantaiId = Gson().fromJson(convertLantaiBangunanId4, Array<String>::class.java).toList()
                                    val dataLantaiName = Gson().fromJson(convertLantaiBangunanName4, Array<String>::class.java).toList()
                                    val dataLantaiNameOther = Gson().fromJson(convertLantaiBangunanNameOther4, Array<String>::class.java).toList()
                                    val dataLantaiVolume = Gson().fromJson(convertLantaiBangunanVolume4, Array<String>::class.java).toList()


                                    var pondasiName = ""
                                    var pondasiVolume = ""
                                    if (dataPondasiId.isNotEmpty()) {
                                        for ((indexPondasi, valuePondasi) in dataPondasiId.withIndex()) {
                                            f1 = if (dataPondasiNameOther[indexPondasi].isNotEmpty()) {
                                                "- ${dataPondasiNameOther[indexPondasi]}"
                                            } else {
                                                "- ${dataPondasiName[indexPondasi]}"
                                            }

                                            pondasiName += "$f1\n"

                                            pondasiVolume += "${dataPondasiVolume[indexPondasi]}\n"
                                        }//looping data pondasi
                                    } else {
                                        f1 = ""
                                        pondasiName = ""
                                        pondasiVolume = ""
                                    }

                                    var strukturName = ""
                                    var strukturVolume = ""
                                    if (dataStrukturId.isNotEmpty()) {
                                        for ((indexStruktur, valueStruktur) in dataStrukturId.withIndex()) {
                                            st1 = if (dataStrukturNameOther[indexStruktur].isNotEmpty()) {
                                                "- ${dataStrukturNameOther[indexStruktur]}"
                                            } else {
                                                "- ${dataStrukturName[indexStruktur]}"
                                            }

                                            strukturName += "$st1\n"
                                            strukturVolume += "${dataStrukturVolume[indexStruktur]}\n"
                                        }
                                    } else {
                                        st1 = ""
                                        strukturName = ""
                                        strukturVolume = ""
                                    }

                                    var kerangkaAtapName = ""
                                    var kerangkaAtapVolume = ""
                                    if (dataKeratapId.isNotEmpty()) {
                                        for ((indexKertap, valueKertapId) in dataKeratapId.withIndex()) {
                                            rf1 = if (dataKeratapNameOther[indexKertap].isNotEmpty()) {
                                                "- ${dataKeratapNameOther[indexKertap]}"
                                            } else {
                                                "- ${dataKeratapName[indexKertap]}"
                                            }

                                            kerangkaAtapName += "$rf1\n"

                                            kerangkaAtapVolume += "${dataKeratapVolume[indexKertap]}\n"
                                        } //looping data kerangka atap
                                    } else {
                                        rf1 = ""
                                        kerangkaAtapName = ""
                                        kerangkaAtapVolume = ""
                                    }


                                    var penutupAtapName = ""
                                    var penutupAtapVolume = ""
                                    if (dataPenutapId.isNotEmpty()) {
                                        for ((indexPenutupAtap, valuePenutupAtapId) in dataPenutapId.withIndex()) {
                                            rc1 = if (dataPenutapNameOther[indexPenutupAtap].isNotEmpty()) {
                                                "- ${dataPenutapNameOther[indexPenutupAtap]}"
                                            } else {
                                                "- ${dataPenutapName[indexPenutupAtap]}"
                                            }

                                            penutupAtapName += "$rc1\n"

                                            penutupAtapVolume += "${dataPenutapVolume[indexPenutupAtap]}\n"
                                        }//looping data penutup atap
                                    } else {
                                        rc1 = ""
                                        penutupAtapName = ""
                                        penutupAtapVolume = ""
                                    }

                                    var plafonName = ""
                                    var plafonVolume = ""
                                    if (dataPlafonId.isNotEmpty()) {
                                        for ((indexPlafon, valuePlafonId) in dataPlafonId.withIndex()) {
                                            cl1 = if (dataPlafonNameOther[indexPlafon].isNotEmpty()) {
                                                "- ${dataPlafonNameOther[indexPlafon]}"
                                            } else {
                                                "- ${dataPlafonName[indexPlafon]}"
                                            }

                                            plafonName += "$cl1\n"
                                            plafonVolume += "${dataPlafonVolume[indexPlafon]}\n"
                                        }//looping data plafon
                                    } else {
                                        cl1 = ""
                                        plafonName = ""
                                        plafonVolume = ""
                                    }

                                    var dindingName = ""
                                    var dindingVolume = ""
                                    if (dataDindingId.isNotEmpty()) {
                                        for ((indexDinding, valueDindingId) in dataDindingId.withIndex()) {
                                            wl1 = if (dataDindingNameOther[indexDinding].isNotEmpty()) {
                                                "- ${dataDindingNameOther[indexDinding]}"
                                            } else {
                                                "- ${dataDindingName[indexDinding]}"
                                            }

                                            dindingName += "$wl1\n"
                                            dindingVolume += "${dataDindingVolume[indexDinding]}\n"
                                        }//looping data dinding
                                    } else {
                                        wl1 = ""
                                        dindingName = ""
                                        dindingVolume = ""
                                    }

                                    var pinjenName = ""
                                    var pinjenVolume = ""
                                    if (dataPinjelId.isNotEmpty()) {
                                        for ((indexPinJen, valuePinjelId) in dataPinjelId.withIndex()) {
                                            dw1 = if (dataPinjelNameOther[indexPinJen].isNotEmpty()) {
                                                "- ${dataPinjelNameOther[indexPinJen]}"
                                            } else {
                                                "- ${dataPinjelName[indexPinJen]}"
                                            }

                                            pinjenName += "$dw1\n"
                                            pinjenVolume += "${dataPinjelVolume[indexPinJen]}\n"
                                        }//looping data pintu & jendela
                                    } else {
                                        dw1 = ""
                                        pinjenName = ""
                                        pinjenVolume = ""
                                    }

                                    var lantaiName = ""
                                    var lantaiVolume = ""
                                    if (dataLantaiId.isNotEmpty()) {
                                        for ((indexLantai, valueLantaiId) in dataLantaiId.withIndex()) {
                                            fl1 = if (dataLantaiNameOther[indexLantai].isNotEmpty()) {
                                                "- ${dataLantaiNameOther[indexLantai]}"
                                            } else {
                                                "- ${dataLantaiName[indexLantai]}"
                                            }

                                            lantaiName += "$fl1\n"

                                            lantaiVolume += "${dataLantaiVolume[indexLantai]}\n"
                                        }//looping data lantai
                                    } else {
                                        fl1 = ""
                                        lantaiName = ""
                                        lantaiVolume = ""
                                    }

                                    jenisbangunan += "${valueBangunan.projectBuildingTypeName}\n" +
                                            "$pondasiName\n" +
                                            "$strukturName\n" +
                                            "$kerangkaAtapName\n" +
                                            "$penutupAtapName\n" +
                                            "$plafonName\n" +
                                            "$dindingName\n" +
                                            "$pinjenName\n" +
                                            "$lantaiName\n"

                                    luasbangunan += "${bangunan[indexBangunan]?.projectBuildingFloorTotal}\n" +
                                            "$pondasiVolume\n" +
                                            "$strukturVolume\n" +
                                            "$kerangkaAtapVolume\n" +
                                            "$penutupAtapVolume\n" +
                                            "$plafonVolume\n" +
                                            "$dindingVolume\n" +
                                            "$pinjenVolume\n" +
                                            "$lantaiVolume\n"

                                }
                            }


                        }//end looping tanah

                        table.addCell(createCellWithOutHeight(menguasai))
                        //set column no bidang tanah
                        table.addCell(createCellCustom(lannumber, Element.ALIGN_CENTER, Element.ALIGN_TOP))
                        //set column letak tanah
                        table.addCell(createCellWithOutHeight(letak))
                        //set column luas tanah terkena dampak
                        table.addCell(createCellCustom(landaffected, Element.ALIGN_CENTER, Element.ALIGN_TOP))
                        //set column luas sisa atas & sisa bawah
                        table.addCell(createCellCustom(sisaatas, Element.ALIGN_CENTER, Element.ALIGN_TOP))
                        table.addCell(createCellCustom(sisabawah, Element.ALIGN_CENTER, Element.ALIGN_TOP))
                        //set column status tanah
                        table.addCell(createCellCustom(tandabukti, Element.ALIGN_CENTER, Element.ALIGN_TOP))
                        //set column surat tanda bukti tanah
                        table.addCell(createCellCustom(tandabukti, Element.ALIGN_CENTER, Element.ALIGN_TOP))
                        //set column ruang atas dan bawah
                        table.addCell(createCellCustom("", Element.ALIGN_CENTER, Element.ALIGN_TOP))
                        table.addCell(createCellCustom("", Element.ALIGN_CENTER, Element.ALIGN_TOP))

                        //jenis bangunan
                        table.addCell(createCellWithOutHeight(jenisbangunan))
                        //luas bangunan
                        table.addCell(createCellWithOutHeight(luasbangunan))


                    } else {
                        Log.d("tanah", "kosong")

                        //set column 3 pihak kedua jika data tanah kosong
                        table.addCell(createCellWithOutHeight("(- pihak kedua)"))
                        //set column 4 no bidang tanah jika data tanah kosong
                        table.addCell(createCellWithOutHeight("(- nobid)"))
                        //set column 5 letak tanah jika data tanah kosong
                        table.addCell(createCellWithOutHeight("(- letak tanah)"))
                        //set column 6 luas dampak tanah jika data tanah kosong
                        table.addCell(createCellWithOutHeight("(- luas dampak tanah)"))
                        //set column 7 & 8 luas sisa atas & sisa bawah jika data tanah kosong
                        table.addCell(createCellCustom("(- luas sisa atas tanah)", Element.ALIGN_CENTER, Element.ALIGN_TOP))
                        table.addCell(createCellCustom("(- luas sisa bwh tanah)", Element.ALIGN_CENTER, Element.ALIGN_TOP))
                        //set column 9 status tanah jika data tanah kosong
                        table.addCell(createCellCustom("(- status tanah)", Element.ALIGN_CENTER, Element.ALIGN_TOP))
                        //set column 10 surat tanda bukti tanah jika data tanah kosong
                        table.addCell(createCellCustom("(- tanda bukti tanah)", Element.ALIGN_CENTER, Element.ALIGN_TOP))
                        //set column 11 & 12 ruang atas dan bawah jika data tanah kosong
                        table.addCell(createCellCustom("-", Element.ALIGN_CENTER, Element.ALIGN_TOP))
                        table.addCell(createCellCustom("-", Element.ALIGN_CENTER, Element.ALIGN_TOP))
                        //set column 13 jenis bangunan jika data tanah kosong
                        table.addCell(createCellCustom(jenisbangunan, Element.ALIGN_CENTER, Element.ALIGN_TOP))
                        //set column 14 luas bangunan jika data tanah kosong
                        table.addCell(createCellCustom(luasbangunan, Element.ALIGN_CENTER, Element.ALIGN_TOP))

                    }


                    /*if (pjland.isNotEmpty()) {
                        Log.d("tanah", "tdk kosong")
                        lannumber = ""
                        letak = ""
                        landaffected = ""
                        sisaatas = ""
                        sisabawah = ""
                        kepemilikan = ""
                        tandabukti = ""
                        jenisbangunan = ""
                        luasbangunan = ""
                        jenistanaman = ""
                        k = ""
                        s = ""
                        b = ""
                        r = ""
                        jumlahtanaman = ""
                        satuantanaman = ""
                        fasilitasLain = ""
                        fasilitasValue = ""
                        landInfopiducia = ""
                        f1 = ""
                        f2 = ""
                        st1 = ""
                        st2 = ""
                        rf1 = ""
                        rf2 = ""
                        rc1 = ""
                        rc2 = ""
                        cl1 = ""
                        cl2 = ""
                        dw1 = ""
                        dw2 = ""
                        fl1 = ""
                        fl2 = ""
                        wl1 = ""
                        wl2 = ""
                        menguasai = ""

                        for ((index, valueTanah) in pjland.withIndex()) {
                            val dataPihakKedua = it.where(Subjek2::class.java).equalTo("LandIdTemp", valueTanah.landIdTemp).findAll()
                            if (dataPihakKedua != null) {
                                var pihakKedua = ""
                                for ((indexPemilikKedua, valuePemilikKedua) in dataPihakKedua.withIndex()) {
                                    val nomordIdentitasPenggarap: String? = if (valuePemilikKedua.subjek2IdentityNumber!!.isNotEmpty()) valuePemilikKedua.subjek2IdentityNumber else "-"
                                    val tmptLahirPenggarap: String? = if (valuePemilikKedua.subjek2BirthPlaceName!!.isNotEmpty()) valuePemilikKedua.subjek2BirthPlaceName else "-"
                                    val tglLahirPenggarap: String? = if (valuePemilikKedua.subjek2BirthDate!!.isNotEmpty()) valuePemilikKedua.subjek2BirthDate else "-"
                                    val pekerjaanPenggarap: String? = if (valuePemilikKedua.subjek2OccupationName!!.isNotEmpty()) valuePemilikKedua.subjek2OccupationName else "-"
                                    val alamatJalanPenggarap: String? = if (valuePemilikKedua.subjek2StreetName!!.isNotEmpty()) valuePemilikKedua.subjek2StreetName else "-"
                                    val alamatNomorRumahPenggarap: String? = if (valuePemilikKedua.subjek2Number!!.isNotEmpty()) valuePemilikKedua.subjek2Number else "-"
                                    val alamatRTPenggarap: String? = if (valuePemilikKedua.subjek2RT!!.isNotEmpty()) valuePemilikKedua.subjek2RT else "-"
                                    val alamatRWPenggarap: String? = if (valuePemilikKedua.subjek2RW!!.isNotEmpty()) valuePemilikKedua.subjek2RW else "-"
                                    val alamatBlokPenggarap: String? = if (valuePemilikKedua.subjek2Block!!.isNotEmpty()) valuePemilikKedua.subjek2Block else "-"
                                    val alamatKelurahanPenggarap: String? = if (valuePemilikKedua.subjek2VillageName!!.isNotEmpty()) valuePemilikKedua.subjek2VillageName else "-"
                                    val alamatKecamatanPenggarap: String? = if (valuePemilikKedua.subjek2DistrictName!!.isNotEmpty()) valuePemilikKedua.subjek2DistrictName else "-"
                                    val alamatKotaKabupatenPenggarap: String? = if (valuePemilikKedua.subjek2RegencyName!!.isNotEmpty()) valuePemilikKedua.subjek2RegencyName else "-"
                                    val alamatProvinsiPenggarap: String? = if (valuePemilikKedua.subjek2ProvinceName!!.isNotEmpty()) valuePemilikKedua.subjek2ProvinceName else "-"

                                    pihakKedua =
                                            """
                                            a.  Nama                    :  ${valuePemilikKedua.subjek2FullName}}
                                            b.  Tanggal Lahir       :  $tmptLahirPenggarap, $tglLahirPenggarap
                                            c.  Pekerjaan             :  $pekerjaanPenggarap
                                            d.  Alamat                  :  Jl.$alamatJalanPenggarap No.$alamatNomorRumahPenggarap Blok $alamatBlokPenggarap RT $alamatRTPenggarap RW $alamatRWPenggarap $alamatKelurahanPenggarap $alamatKecamatanPenggarap $alamatKotaKabupatenPenggarap $alamatProvinsiPenggarap
                                            e.  NIK/No.KTP          :  $nomordIdentitasPenggarap



                                            """.trimIndent()

                                    menguasai += pihakKedua
                                    //3
                                    table.addCell(createCellWithOutHeight(menguasai))
                                }//looping pihak kedua

                            }else{
                                //3
                                table.addCell(createCellWithOutHeight(""))
                            }

                            val letaktanahRT = if (valueTanah.landRT!!.isNotEmpty()) valueTanah.landRT else "-"
                            val letaktanahRW = if (valueTanah.landRW!!.isNotEmpty()) valueTanah.landRW else "-"
                            val letaktanahKelurahan = if (valueTanah.landVillageName!!.isNotEmpty()) valueTanah.landVillageName else "-"
                            val letaktanahKecamatan = if (valueTanah.landDistrictName!!.isNotEmpty()) valueTanah.landDistrictName else "-"
                            val letaktanahKota = if (valueTanah.landRegencyName!!.isNotEmpty()) valueTanah.landRegencyName else "-"
                            val letaktanahProvinsi = if (valueTanah.landProvinceName!!.isNotEmpty()) valueTanah.landProvinceName else "-"

                            val numberofLand = valueTanah.landNumberList
                            //4
                            table.addCell(createCellWithOutHeight(numberofLand))

                            letak +=
                                    """
                                    a.  RT/RW              :  $letaktanahRT/$letaktanahRW
                                    b.  Kelurahan        :  $letaktanahKelurahan
                                    c.  Kecamatan       :  $letaktanahKecamatan
                                    d.  Kota                  :  $letaktanahKota
                                    e.  Provinsi            :  $letaktanahProvinsi
                                    """.trimIndent()
                            //5
                            table.addCell(createCellWithOutHeight(letak))

                        }
                    } else {
                        Log.d("tanah", "kosong")
                        table.addCell(createCellWithOutHeight(""))
                        table.addCell(createCellWithOutHeight(""))
                        table.addCell(createCellWithOutHeight(""))
                    }*/

                }

            } else {
                Log.d("hasil", "data pemilik pertama kosong")
            }
        }

        return table
    }

    private fun createdataPdf(projectAssignProjectId: Int?): ArrayList<String> {
        val dataPemilikPertama: ArrayList<String> = ArrayList()
        realm.executeTransaction {
            val results = it.where(PartyAdd::class.java).equalTo("ProjectId", projectAssignProjectId).findAll()
            if (results != null) {
                //Log.d("hasil", results.toString())
                for (element in 0 until results.size) {
                    //Log.d("kuy", results[element]!!.partyFullName)
                    dataPemilikPertama.add(results[element]!!.partyFullName)
                }
            } else {
                Log.d("hasil", "data pemilik pertama kosong")
            }
        }
        /*realm.executeTransactionAsync({ inrealm ->
            val results = inrealm.where(PartyAdd::class.java).equalTo("ProjectId", projectAssignProjectId).findAll()


        }, {
            Log.d("kuy hasil", dataPemilikPertama.toString())

        }, {
            Log.d("Error", "error realm Transaction second table itextpdf")
            Log.d("Error", it.message.toString())
        })*/

        return dataPemilikPertama
    }


    private fun createCellCustom(content: String, horizontalElement: Int, verticalElement: Int): PdfPCell {
        val font = Font(Font.FontFamily.HELVETICA, 10f)
        val cell = PdfPCell(Phrase(content, font))
        cell.isNoWrap = false
        cell.horizontalAlignment = horizontalElement
        cell.verticalAlignment = verticalElement
        return cell
    }

    private fun createCellNoramal(content: String, heightColumn: Float): PdfPCell {
        val font = Font(Font.FontFamily.HELVETICA, 10f)
        val cell = PdfPCell(Phrase(content, font))
        cell.isNoWrap = false
        cell.fixedHeight = heightColumn
        cell.horizontalAlignment = Element.ALIGN_CENTER
        cell.verticalAlignment = Element.ALIGN_MIDDLE
        return cell
    }

    private fun createCellWithOutHeight(content: String): PdfPCell {
        val font = Font(Font.FontFamily.HELVETICA, 10f)
        val cell = PdfPCell(Phrase(content, font))
        cell.isNoWrap = false
        cell.horizontalAlignment = Element.ALIGN_LEFT
        return cell
    }

    private fun createCellWithRowspan(content: String, total_rowspan: Int, heightColumn: Float): PdfPCell {
        val font = Font(Font.FontFamily.HELVETICA, 10f)
        val cell = PdfPCell(Phrase(content, font))
        cell.isNoWrap = false
        cell.rowspan = total_rowspan
        cell.fixedHeight = heightColumn
        cell.horizontalAlignment = Element.ALIGN_CENTER
        cell.verticalAlignment = Element.ALIGN_MIDDLE
        return cell
    }

    private fun createCellWithColspan(content: String, total_colspan: Int, heightColumn: Float): PdfPCell {
        val font = Font(Font.FontFamily.HELVETICA, 10f)
        val cell = PdfPCell(Phrase(content, font))
        cell.colspan = total_colspan
        cell.isNoWrap = false
        cell.fixedHeight = heightColumn
        cell.horizontalAlignment = Element.ALIGN_CENTER
        cell.verticalAlignment = Element.ALIGN_MIDDLE
        return cell
    }

    private fun createCellWithTextLeft(content: String, heightColumn: Float): PdfPCell {
        val font = Font(Font.FontFamily.HELVETICA, 10f)
        val cell = PdfPCell(Phrase(content, font))
        cell.isNoWrap = false
        cell.fixedHeight = heightColumn
        cell.horizontalAlignment = Element.ALIGN_LEFT
        cell.verticalAlignment = Element.ALIGN_TOP
        return cell
    }

    private fun createRow(numRow: Int) {
        ws.createRow(numRow)
        oldLastRow = numRow
        this.currentLastRow = numRow + 1
    }


    // Function to remove duplicates from an ArrayList
    fun <T> removeDuplicates(list: ArrayList<T>): ArrayList<T> {
        // Create a new ArrayList
        val newList = ArrayList<T>()
        // Traverse through the first list
        for (element in list) {
            // If this element is not present in newList
            // then add it
            if (!newList.contains(element)) {
                newList.add(element)
            }
        }
        // return the new list
        return newList
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            /* REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION -> {
                 val grantResultsLength = grantResults.size
                 if (grantResultsLength > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                     Toast.makeText(this, "Premission granted", Toast.LENGTH_SHORT).show()
                 } else {
                     Toast.makeText(this, "Permission not granted!", Toast.LENGTH_SHORT).show()
                     finish()
                 }
             }*/

            PERMISSION_CALLBACK_CONSTANT -> {
                //check if all permissions are granted
                var allgranted = false
                for (element in grantResults) {
                    if (element == PackageManager.PERMISSION_GRANTED) {
                        allgranted = true
                    } else {
                        allgranted = false
                        break
                    }
                }
                when {
                    allgranted -> proceedAfterPermission()
                    ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, permissionsRequired[0])
                            || ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, permissionsRequired[1])
                            || ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, permissionsRequired[2])
                            || ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, permissionsRequired[3]) -> {
                        //showToast("Permissions Required")
                        val builder = AlertDialog.Builder(this@MainActivity)
                        builder.setTitle("Need Multiple Permissions")
                        builder.setMessage("This app needs Camera and Location permissions.")
                        builder.setPositiveButton("Grant") { dialog, which ->
                            dialog.cancel()
                            ActivityCompat.requestPermissions(this@MainActivity, permissionsRequired, PERMISSION_CALLBACK_CONSTANT)
                        }
                        builder.show()
                    }
                    else -> Log.d("permission", "Unable to get Permission")
                    //Toast.makeText(baseContext, "Unable to get Permission", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_PERMISSION_SETTING -> {
                if (ActivityCompat.checkSelfPermission(this@MainActivity, permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED) {
                    //Got Permission
                    proceedAfterPermission()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (sentToSettings) {
            if (ActivityCompat.checkSelfPermission(this@MainActivity, permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                proceedAfterPermission()
            }
        }
    }

    override fun onDestroy() {
        getPresenter()?.onDetach()
        super.onDestroy()
    }


    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

}