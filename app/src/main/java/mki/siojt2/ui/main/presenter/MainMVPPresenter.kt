package mki.siojt2.ui.main.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface MainMVPPresenter : MVPPresenter {

    fun getlistProject(userId: Int)
    fun getsyncProject(userId: Int, accessToken: String)
}