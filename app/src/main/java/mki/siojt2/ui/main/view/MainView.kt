package mki.siojt2.ui.main.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.ResponseDataListProject
import mki.siojt2.model.ResponseDataListProjectLocal

interface MainView : MvpView {
    fun onsuccesslistProject(dataListProject: MutableList<ResponseDataListProjectLocal>)

    fun onsuccesssyncProject(message: String?)
}