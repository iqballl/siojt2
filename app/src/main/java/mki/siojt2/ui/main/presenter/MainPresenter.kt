package mki.siojt2.ui.main.presenter


import android.annotation.SuppressLint
import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.ui.main.view.MainView
import mki.siojt2.utils.ErrorHandler
import mki.siojt2.utils.rxJava.RxUtils
/*import org.apache.poi.ss.usermodel.HorizontalAlignment
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.ss.util.CellUtil
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception*/

/**
 * Created by iqbal on 09/10/17.
 */

class MainPresenter : BasePresenter(), MainMVPPresenter {

    private var disposables: CompositeDisposable = CompositeDisposable()

    @SuppressLint("CheckResult")
    override fun getlistProject(userId: Int) {
        disposables.add(
                dataManager.getlistProject(userId, dataManager.getaccessToken())
                        .doOnTerminate { mainView().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            mainView().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                mainView().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    mainView().onsuccesslistProject(authResponseDataObject.result!!)
                                } else {
                                    mainView().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            ErrorHandler.handlerErrorPresenter(mainView(), throwable)
                        }
        )
    }

    override fun getsyncProject(userId: Int, accessToken: String) {
        mainView().onShowLoading()
        disposables.add(
                dataManager.getsyncProject(userId, accessToken)
                        .doOnTerminate { mainView().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            mainView().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                mainView().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    mainView().onsuccesssyncProject(authResponseDataObject.message)
                                } else {
                                    mainView().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            mainView().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(mainView(), throwable)
                        }
        )
    }


    private fun mainView(): MainView {
        return getView() as MainView
    }
}
