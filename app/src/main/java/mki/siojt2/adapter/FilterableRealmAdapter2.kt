package mki.siojt2.adapter

import android.annotation.SuppressLint
import android.content.Context
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import io.realm.Case
import io.realm.Realm

import mki.siojt2.R
import mki.siojt2.model.region.ResponseDataCityWithoutParameter

class FilterableRealmAdapter2(context: Context, private val resourceId: Int, realm: Realm) : ArrayAdapter<ResponseDataCityWithoutParameter>(context, resourceId) {

    private val mContext: Context = context
    private val mRealm: Realm = realm

    var mCity
            : List<ResponseDataCityWithoutParameter> = ArrayList()
    var suggestions = ArrayList<ResponseDataCityWithoutParameter>(mCity)
    var tempItems: ArrayList<ResponseDataCityWithoutParameter> = ArrayList()

    @NonNull
    override fun getView(position: Int, @Nullable convertView: View?, @NonNull parent: ViewGroup): View {
        var view = super.getView(position, convertView, parent)
        try {
            if (convertView == null) {
                val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                view = inflater.inflate(resourceId, parent, false)
            }
            val fruit = getItem(position)
            val name = view.findViewById(R.id.text1) as com.pixplicity.fontview.FontTextView
            name.text = mCity[position].getRegencyName()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return view
    }

    override fun getCount(): Int {
        return mCity.size
    }

    override fun getFilter(): Filter {
        return nameFilter
    }


    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    var nameFilter: Filter = object : Filter() {
        override fun convertResultToString(resultValue: Any): CharSequence {
            val str = (resultValue as ResponseDataCityWithoutParameter)
            return str.getRegencyName()!!
        }

        @SuppressLint("DefaultLocale")
        override fun performFiltering(constraint: CharSequence?): FilterResults? {
            return if (constraint != null) {
                suggestions.clear()
                for (people in tempItems) {
                    if (people.getRegencyName()!!.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(people)
                    }
                }
                val filterResults = FilterResults()
                filterResults.values = suggestions
                filterResults.count = suggestions.size
                filterResults
            } else {
                FilterResults()
            }
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            /*val filterList = results!!.values as ArrayList<ResponseDataCityWithoutParameter>
            if (results.count > 0) {
                clear()
                for (people in filterList) {
                    add(people)
                    notifyDataSetChanged()
                }
            }*/

            if (constraint != null) {
                clear()
                mCity = filterCountries(constraint.toString())
                notifyDataSetChanged()
            } else {
                clear()
                notifyDataSetInvalidated()
            }
        }
    }


    private fun filterCountries(query: String): List<ResponseDataCityWithoutParameter> {
        return mRealm
                .where(ResponseDataCityWithoutParameter::class.java)
                .limit(10)
                .contains("regencyName", query, Case.INSENSITIVE)
                .findAll()
    }

    /*override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                return FilterResults()
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (constraint != null) {
                    mResults = mRealmObjectList.where()
                            .contains("regencyName", constraint.toString(), Case.INSENSITIVE)
                            .limit(10)
                            .findAll()
                    notifyDataSetChanged()
                }
            }

        }
    }*/
}