package mki.siojt2.adapter.autocomplate_adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import io.realm.Case

import io.realm.RealmObject
import io.realm.RealmResults
import mki.siojt2.R
import mki.siojt2.model.region.ResponseDataKecamatanWithoutParameter

class FilterableRealmAdapterKecamatan<T : RealmObject>(context: Context, refAutocomplete: AutoCompleteTextView) : ArrayAdapter<T>(context, android.R.layout.simple_list_item_1), Filterable {

    private var mRealmObjectList: RealmResults<T>? = null
    private var mResults: List<T>? = null

    private var mContext = context

    init {
        refAutocomplete.setAdapter(this)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = super.getView(position, convertView, parent)
        try {
            if (convertView == null) {
                val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                view = inflater.inflate(R.layout.item_spinner, parent, false)
            }
            val data: ResponseDataKecamatanWithoutParameter = getItem(position) as ResponseDataKecamatanWithoutParameter
            val name = view.findViewById(R.id.text1) as com.pixplicity.fontview.FontTextView
            name.text = data.getdistrictName().toString()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return view
    }

    override fun getCount(): Int {
        return if (mResults == null) 0 else mResults!!.size
    }

    override fun getItem(position: Int): T? {
        return if (mResults == null) null else mResults!![position]
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                return FilterResults()
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (constraint != null) {
                    if (mRealmObjectList != null) {
                        mResults = mRealmObjectList!!.where()
                                .beginsWith("districtName", constraint.toString(), Case.INSENSITIVE)
                                .or()
                                .contains("districtName", constraint.toString(), Case.INSENSITIVE)
                                .limit(10)
                                .findAll()
                        notifyDataSetChanged()
                    } else {
                        Toast.makeText(mContext, "Harap pilih kota", Toast.LENGTH_SHORT).show()
                    }

                } else {
                    notifyDataSetInvalidated()
                }
            }

        }
    }

    fun setRealmFilter(realmObjectList: RealmResults<T>?) {
        this.mRealmObjectList = realmObjectList
    }
}