package mki.siojt2.base.response

import com.google.gson.annotations.SerializedName

/**
 * Created by bayunvnt on 09/10/17.
 */

    open class BaseResponse {
        @SerializedName("status")
        var status: Int = 0
        @SerializedName("code")
        var code: Int = 0
        @SerializedName("message")
        var message: String? = null
        @SerializedName("generated")
        var generated: String? = null
        @SerializedName("serverTime")
        var serverTime: String? = null
        @SerializedName("totaldata")
        var totaldata: Int = 0
        @SerializedName("totalresult")
        var totalresult: Int = 0
    }
