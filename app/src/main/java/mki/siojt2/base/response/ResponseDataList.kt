package mki.siojt2.base.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by iqbal on 18/04/19.
 */

class ResponseDataList<T> : BaseResponse() {
    @SerializedName("results")
    @Expose
    var result: MutableList<T>? = null
}
