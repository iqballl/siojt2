package mki.siojt2.base

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import mki.siojt2.base.view.BaseActivity

/**
 * Created by bayunvnt on 09/10/17.
 */

open class BaseDialogFragment : androidx.fragment.app.DialogFragment() {

    protected var mContext: Context? = null
    private var mLayoutInflater: LayoutInflater? = null

    private val defaultDialog: Dialog
        get() {
            val dialog = Dialog(mContext!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
            return dialog
        }

    val baseActivity: BaseActivity
        get() = activity as BaseActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        mLayoutInflater = LayoutInflater.from(mContext)
    }

    fun showToast(message: String) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show()
    }
}
