package mki.siojt2.base

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.util.*

/**
 * Created by bayunvnt on 09/10/17.
 */

abstract class BaseAdapter<T>(protected var mContext: Context, private val mResId: Int) : androidx.recyclerview.widget.RecyclerView.Adapter<BaseHolder>() {

    private val mListData = ArrayList<T>()
    private var mInflater: LayoutInflater = LayoutInflater.from(mContext)

    protected var mListener: OnItemClickListener? = null

    val listData: List<T>
        get() = mListData

    fun setListener(baseFragment: BaseFragment) {
        mListener = baseFragment as OnItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseHolder {
        return if (mResId == 0)
            onCreate(null, viewType)
        else
            onCreate(mInflater.inflate(mResId, parent, false), viewType)
    }

    abstract fun onCreate(view: View?, viewType: Int): BaseHolder

    override fun onBindViewHolder(holder: BaseHolder, position: Int) {
        onBind(holder, mListData[position], position)
    }

    protected abstract fun onBind(vh: BaseHolder, t: T, position: Int)

    override fun getItemCount(): Int {
        return mListData.size
    }

    fun getItem(position: Int): T {
        return mListData[position]
    }

    fun pushData(listData: List<T>) {
        mListData.clear()
        mListData.addAll(listData)
        notifyDataSetChanged()
    }

    interface OnItemClickListener {
        fun onItemClickListener(position: Int)
    }
}
