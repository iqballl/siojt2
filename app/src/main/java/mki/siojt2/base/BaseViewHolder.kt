package mki.siojt2.base

import androidx.recyclerview.widget.RecyclerView
import android.view.View

abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var currentPosition: Int = 0
        private set

    protected abstract fun clear()

    open fun onBind(position: Int) {
        clear()
        currentPosition = position
    }
}