package mki.siojt2.base

import android.app.ProgressDialog
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.trello.rxlifecycle.components.support.RxFragment
import mki.siojt2.R
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.base.view.MvpView
import mki.siojt2.utils.CommonUtils
import mki.siojt2.utils.extension.SafeClickListener
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import android.text.InputFilter
import android.widget.Toast


/**
 * Created by iqbal on 18/04/19.
 */

abstract class BaseFragment : RxFragment(), MvpView {

    private var mActivity: BaseActivity? = null
    private var mProgressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUp(view)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity) {
            val activity = context as BaseActivity?
            this.mActivity = activity
            activity!!.onFragmentAttached()
        }
    }

    override fun isNetworkConnected(): Boolean {
        val connectivity = mActivity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = connectivity.allNetworkInfo
        if (info != null)
            for (i in info.indices)
                if (info[i].state == NetworkInfo.State.CONNECTED) {
                    return true
                }
        showSnackBar("Gadeget tidak tehubung dengan internet")
        return false
    }

    override fun onShowLoading() {
        onDismissLoading()
        mProgressDialog = CommonUtils.showLoadingDialog(this.context!!)
    }

    override fun onDismissLoading() {
        if (mProgressDialog != null && mProgressDialog!!.isShowing) {
            mProgressDialog!!.cancel()
        }
    }

    override fun onTokenExpired() {
        if (mActivity != null) {
            mActivity!!.onTokenExpired()
        }
    }

    override fun onFailed(message: String) {
        if (mActivity != null) {
            mActivity!!.onFailed(message)
        }
    }

    override fun hideKeyboard() {
        if (mActivity != null) {
            mActivity!!.hideKeyboard()
        }
    }

    override fun onDetach() {
        mActivity = null
        super.onDetach()
    }

    private fun showSnackBar(message: String) {
        val snackbar = Snackbar.make(mActivity!!.findViewById(android.R.id.content),
                message, Snackbar.LENGTH_LONG)
        snackbar.duration = 100000
        val sbView = snackbar.view
        sbView.setBackgroundColor(ContextCompat.getColor(mActivity!!, R.color.colorPrimary))
        val textView = sbView
                .findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        textView.setTextColor(ContextCompat.getColor(mActivity!!, R.color.colorWhite))
        snackbar.show()
    }

    open fun showToast(msg: Any?){
        Toast.makeText(context, msg.toString(), Toast.LENGTH_SHORT).show()
    }

    open fun setCurrency(editText: EditText) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                editText.removeTextChangedListener(this)
                try {
                    var originalString = editable.toString()
                    val longval: Double?
                    val formatter = NumberFormat.getInstance(Locale.GERMANY) as DecimalFormat
                    val symbols = formatter.decimalFormatSymbols

                    symbols.groupingSeparator = '.'
                    formatter.decimalFormatSymbols = symbols
                    if (originalString.contains(".")) {
                        originalString = originalString.replace("[.]".toRegex(), "")
                    }
                    longval = java.lang.Double.parseDouble(originalString)
                    //                        Locale localeID = new Locale("in", "ID");
                    //                        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(localeID);
                    //                    formatter.applyPattern("#.###.###.###");
                    val formattedString = formatter.format(longval)
                    //setting text after format to EditText
                    editText.setText(formattedString)
                    editText.setSelection(editText.text.length)
                } catch (nfe: NumberFormatException) {
                    nfe.printStackTrace()
                }
                editText.addTextChangedListener(this)

            }
        })

    }

    open fun trimCommaOfString(string: String): String {
        return when {
            string.contains(",") -> {
                string.replace(",", "")
            }
            string.contains(".") -> {
                string.replace(".", "")
            }
            else -> {
                string
            }
        }

    }

    open fun setformatterNPWP(editText: EditText) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {


                editText.removeTextChangedListener(this)
                try {
                    /*var originalString = editable.toString()
                    val longval: Double?
                    val formatter = NumberFormat.getInstance(Locale.GERMANY) as DecimalFormat
                    val symbols = formatter.decimalFormatSymbols

                    symbols.groupingSeparator = '.'
                    formatter.decimalFormatSymbols = symbols
                    if (originalString.contains(".")) {
                        originalString = originalString.replace("[.]".toRegex(), "")
                    }
                    longval = java.lang.Double.parseDouble(originalString)
                    //                        Locale localeID = new Locale("in", "ID");
                    //                        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(localeID);
                    //                    formatter.applyPattern("#.###.###.###");
                    val formattedString = formatter.format(longval)

                    //setting text after format to EditText
                    editText.setText(formattedString)
                    editText.setSelection(editText.text.length)*/

                    var originalString = editable.toString()
                    if (originalString.contains(".")) {
                        originalString = originalString.replace("[.]".toRegex(), "")
                    }
                    if (originalString.contains("-")) {
                        originalString = originalString.replace("[-]".toRegex(), "")
                    }

                    val maman = mutableListOf(
                            "", "", ".",
                            "", "", ".",
                            "", "", ".",
                            "-",
                            "", "", ".",
                            "", "", "", "", "", "", "", ""
                    )

                    var formattedString = ""
                    var reFormattedString = ""
                    for (i in originalString.indices) {
                        if (reFormattedString.length == i) {
                            formattedString += maman[i]
                        }
                        formattedString += originalString[i]
                        reFormattedString += originalString[i]
                    }

                    editText.setText(formattedString)
                    editText.setSelection(editText.text.length)


                } catch (nfe: NumberFormatException) {
                    nfe.printStackTrace()
                }
                editText.addTextChangedListener(this)

            }
        })

    }


    open fun setformatterNIB(editText: EditText) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {


                editText.removeTextChangedListener(this)
                try {
                    /*var originalString = editable.toString()
                    val longval: Double?
                    val formatter = NumberFormat.getInstance(Locale.GERMANY) as DecimalFormat
                    val symbols = formatter.decimalFormatSymbols

                    symbols.groupingSeparator = '.'
                    formatter.decimalFormatSymbols = symbols
                    if (originalString.contains(".")) {
                        originalString = originalString.replace("[.]".toRegex(), "")
                    }
                    longval = java.lang.Double.parseDouble(originalString)
                    //                        Locale localeID = new Locale("in", "ID");
                    //                        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(localeID);
                    //                    formatter.applyPattern("#.###.###.###");
                    val formattedString = formatter.format(longval)

                    //setting text after format to EditText
                    editText.setText(formattedString)
                    editText.setSelection(editText.text.length)*/

                    var originalString = editable.toString()
                    if (originalString.contains(".")) {
                        originalString = originalString.replace("[.]".toRegex(), "")
                    }
                    if (originalString.contains("-")) {
                        originalString = originalString.replace("[-]".toRegex(), "")
                    }

                    val maman = mutableListOf(
                            "", "", ".",
                            "", ".",
                            "", ".",
                            "", ".",
                            "", "", "", "", "", "", "", ""
                    )

                    var formattedString = ""
                    var reFormattedString = ""
                    for (i in originalString.indices) {
                        if (reFormattedString.length == i) {
                            formattedString += maman[i]
                        }
                        formattedString += originalString[i]
                        reFormattedString += originalString[i]
                    }

                    editText.setText(formattedString)
                    editText.setSelection(editText.text.length)


                } catch (nfe: NumberFormatException) {
                    nfe.printStackTrace()
                }
                editText.addTextChangedListener(this)

            }
        })

    }

    open fun setformatterNomorSertifikatTanah(editText: EditText) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {


                editText.removeTextChangedListener(this)
                try {
                    /*var originalString = editable.toString()
                    val longval: Double?
                    val formatter = NumberFormat.getInstance(Locale.GERMANY) as DecimalFormat
                    val symbols = formatter.decimalFormatSymbols

                    symbols.groupingSeparator = '.'
                    formatter.decimalFormatSymbols = symbols
                    if (originalString.contains(".")) {
                        originalString = originalString.replace("[.]".toRegex(), "")
                    }
                    longval = java.lang.Double.parseDouble(originalString)
                    //                        Locale localeID = new Locale("in", "ID");
                    //                        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(localeID);
                    //                    formatter.applyPattern("#.###.###.###");
                    val formattedString = formatter.format(longval)

                    //setting text after format to EditText
                    editText.setText(formattedString)
                    editText.setSelection(editText.text.length)*/

                    var originalString = editable.toString()
                    if (originalString.contains(".")) {
                        originalString = originalString.replace("[.]".toRegex(), "")
                    }
                    if (originalString.contains("-")) {
                        originalString = originalString.replace("[-]".toRegex(), "")
                    }

                    val maman = mutableListOf(
                            "", "", ".",
                            "", ".",
                            "", ".",
                            "", ".", ".",
                            "", "", "", "", "", "", "", ""
                    )

                    var formattedString = ""
                    var reFormattedString = ""
                    for (i in originalString.indices) {
                        if (reFormattedString.length == i) {
                            formattedString += maman[i]
                        }
                        formattedString += originalString[i]
                        reFormattedString += originalString[i]
                    }

                    editText.setText(formattedString)
                    editText.setSelection(editText.text.length)


                } catch (nfe: NumberFormatException) {
                    nfe.printStackTrace()
                }
                editText.addTextChangedListener(this)

            }
        })

    }

    fun setEditTextMaxLength(editText: EditText, length: Int) {
        val FilterArray = arrayOfNulls<InputFilter>(1)
        FilterArray[0] = InputFilter.LengthFilter(length)
        editText.filters = FilterArray
    }

    internal fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
        val safeClickListener = SafeClickListener {
            onSafeClick(it)
        }
        setOnClickListener(safeClickListener)
    }

    protected abstract fun setUp(view: View)

    interface Callback {
        fun onFragmentAttached()
        fun onFragmentDetached(tag: String)
    }


}
