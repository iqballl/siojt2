package mki.siojt2.base.view

/**
 * Created by iqbal on 09/10/17.
 */

interface MvpView {

    fun isNetworkConnected(): Boolean

    fun onShowLoading()

    fun onDismissLoading()

    fun onTokenExpired()

    fun onFailed(message: String)

    fun hideKeyboard()
}
