package mki.siojt2.base.view

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.FragmentManager
import androidx.core.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast

import com.trello.rxlifecycle.components.support.RxAppCompatActivity

import mki.siojt2.R
import mki.siojt2.ui.login.view.LoginActivity
import mki.siojt2.utils.network.ConnectivityReceiver
import android.util.Log
import android.view.WindowManager
import android.widget.EditText
import cn.pedant.SweetAlert.SweetAlertDialog
import mki.siojt2.SIOJT2
import mki.siojt2.base.BaseFragment
import mki.siojt2.model.localsave.Session
import mki.siojt2.utils.dialog.MessageDialog
import mki.siojt2.utils.extension.SafeClickListener
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by iqbal on 18/04/19.
 */

abstract class BaseActivity : RxAppCompatActivity(), MvpView, BaseFragment.Callback{

    private var progressDialog: ProgressDialog? = null

    private var sweetAlretLoading: SweetAlertDialog? = null

    private var mTfLight: Typeface? = null

    private lateinit var session: Session

    val baseFragmentManager: FragmentManager
        get() = supportFragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        session = Session(this)
        initialzeProgressDialoge()

        checkConnection()
        mTfLight = Typeface.createFromAsset(assets, "OpenSans-Light.ttf")
        /*val intentFilter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
        this.registerReceiver(NetworkChangeReceiver(), intentFilter)*/
    }

    open fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun initialzeProgressDialoge() {
        if (sweetAlretLoading == null) {
            sweetAlretLoading = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
            sweetAlretLoading?.progressHelper!!.barColor = Color.parseColor("#A5DC86")
            sweetAlretLoading?.titleText = "Harap tunggu ..."
            sweetAlretLoading?.setCancelable(false)
        }

    }

    override fun onShowLoading() {
        onDismissLoading()
        //progressDialog = CommonUtils.showLoadingDialog(this)
        sweetAlretLoading?.show()
    }

    override fun onDismissLoading() {
        sweetAlretLoading?.let { if (it.isShowing) it.dismiss() }
    }

    override fun onTokenExpired() {
        startActivity(LoginActivity.getStartIntent(this))
        finish()
    }

    override fun onFailed(message: String) {
        onDismissLoading()
        MessageDialog.showMessage(this, message)
    }

    override fun hideKeyboard() {
        /*val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }*/
        val inputManager = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val v = this.currentFocus
        if (v != null) {
            this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            inputManager.hideSoftInputFromWindow(v.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }

    }

    override fun isNetworkConnected(): Boolean {
        val connectivity = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = connectivity.allNetworkInfo
        for (i in info.indices)
            if (info[i].state == NetworkInfo.State.CONNECTED) {
                return true
            }

        showSnackBar("Saat ini gadget anda tidak terkoneksi dengan internet!!")
        return false
    }

    // Method to manually check connection status
    private fun checkConnection() {
        val isConnected = ConnectivityReceiver.isConnected
        showSnack(isConnected)
    }

    open fun showSnackBar(message: String) {
        val snackbar = Snackbar.make(findViewById<View>(android.R.id.content),
                message, Snackbar.LENGTH_LONG)
        val sbView = snackbar.view
        sbView.setBackgroundColor(ContextCompat.getColor(this, R.color.primary_material_light))
        val textView = sbView
                .findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        textView.setTextColor(ContextCompat.getColor(this, R.color.colorWhite))
        snackbar.show()
    }

    // Showing the status in Snackbar
    private fun showSnack(isConnected: Boolean) {
        val message: String = if (isConnected) {
            "Good! Connected to Internet"
            //color = Color.GREEN
        } else {
            "Sorry! Not connisNetworkConnectedected to internet"
            //color = Color.RED
        }
        //var color: Int

        //Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        Log.d("Connection", message)

        /*val snackbar = Snackbar.make(findViewById<View>(android.R.id.content),
                message, Snackbar.LENGTH_LONG)

        val sbView = snackbar.view
        val textView = sbView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
        textView.setTextColor(color)
        snackbar.show()*/
    }

    override fun onStop() {
        super.onStop()
        sweetAlretLoading?.let { if (it.isShowing) it.cancel() }
    }

    override fun onDestroy() {
        super.onDestroy()
        System.gc()
        System.runFinalization()
        onDismissLoading()
        sweetAlretLoading = null
        session.setIdAndStatus("", "", "")
    }

    /*override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_left, R.anim.slide_right)
    }*/

    internal fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
        val safeClickListener = SafeClickListener {
            onSafeClick(it)
        }
        setOnClickListener(safeClickListener)
    }

    internal fun setformatterNPWP(editText: EditText) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {


                editText.removeTextChangedListener(this)
                try {
                    /*var originalString = editable.toString()
                    val longval: Double?
                    val formatter = NumberFormat.getInstance(Locale.GERMANY) as DecimalFormat
                    val symbols = formatter.decimalFormatSymbols

                    symbols.groupingSeparator = '.'
                    formatter.decimalFormatSymbols = symbols
                    if (originalString.contains(".")) {
                        originalString = originalString.replace("[.]".toRegex(), "")
                    }
                    longval = java.lang.Double.parseDouble(originalString)
                    //                        Locale localeID = new Locale("in", "ID");
                    //                        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(localeID);
                    //                    formatter.applyPattern("#.###.###.###");
                    val formattedString = formatter.format(longval)

                    //setting text after format to EditText
                    editText.setText(formattedString)
                    editText.setSelection(editText.text.length)*/

                    var originalString = editable.toString()
                    if (originalString.contains(".")) {
                        originalString = originalString.replace("[.]".toRegex(), "")
                    }
                    if (originalString.contains("-")) {
                        originalString = originalString.replace("[-]".toRegex(), "")
                    }

                    val maman = mutableListOf(
                            "", "", ".",
                            "", "", ".",
                            "", "", ".",
                            "-",
                            "", "", ".",
                            "", "", "", "", "", "", "", ""
                    )

                    var formattedString: String = ""
                    var reFormattedString: String = ""
                    for (i in 0 until originalString.length) {
                        if (reFormattedString.length == i) {
                            formattedString += maman[i]
                        }
                        formattedString += originalString[i]
                        reFormattedString += originalString[i]
                    }

                    editText.setText(formattedString)
                    editText.setSelection(editText.text.length)


                } catch (nfe: NumberFormatException) {
                    nfe.printStackTrace()
                }
                editText.addTextChangedListener(this)

            }
        })

    }


    internal fun formatNPWP(NPWPNumb: String): String {
        var numbers = NPWPNumb.replace("/\\d/g".toRegex(), "")
        numbers = numbers.replace("^[0][1-9]\\d{9}\$|^[1-9]\\d{9}\$/g".toRegex(), "$1 - $2")
        return numbers
    }

}
