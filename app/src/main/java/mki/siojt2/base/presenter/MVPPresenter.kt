package mki.siojt2.base.presenter

import mki.siojt2.base.view.MvpView

interface MVPPresenter{

    fun onAttach(view: MvpView?)

    fun onDetach()

    fun getView(): MvpView?

}