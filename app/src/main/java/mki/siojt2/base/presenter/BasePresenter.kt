package mki.siojt2.base.presenter

import android.content.Context
import mki.siojt2.SIOJT2
import mki.siojt2.base.view.MvpView
import mki.siojt2.network.ApiService
import mki.siojt2.network.DataManager
import javax.inject.Inject

/**
 * Created by bayunvnt on 09/10/17.
 */

abstract class BasePresenter : MVPPresenter{

    @Inject
    lateinit var mContext: Context
    @Inject
    lateinit var mApiService: ApiService
    @Inject
    lateinit var dataManager: DataManager
    private var mvpView: MvpView? = null

    private val isViewAttached: Boolean get() = mvpView != null

    init {
        SIOJT2.component?.inject(this)
    }

    // TODO must be change to generic type
    override fun onAttach(view: MvpView?) {
        this.mvpView = view
    }

    override fun getView(): MvpView? = mvpView

    override fun onDetach() {
        this.mvpView = null
    }

    open fun checkViewAttached() {
        if (!isViewAttached) throw MvpViewNotAttachedException()
    }

    class MvpViewNotAttachedException : RuntimeException("Please call Presenter.onAttach(MvpView) before" +
            " requesting data to the Presenter")


    /*fun attachView(mvpView: MvpView) {
        this.mvpView = mvpView
    }

    fun dettachView() {
        this.mvpView = null
    }


    fun <T : MvpView> getView(): T {
        return mvpView as T
    }*/
}
