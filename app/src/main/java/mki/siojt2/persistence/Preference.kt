package mki.siojt2.persistence

import com.orhanobut.hawk.Hawk
import mki.siojt2.model.SyncDataDinamisSplashScreen
import mki.siojt2.model.SyncDataSplashScreen

import mki.siojt2.model.auth.SessionAuth
import mki.siojt2.model.data_form_tanah.DataKoordinatMapBox
import mki.siojt2.model.data_form_tanah.DataKoordinatMaps
import mki.siojt2.model.login.LoginResponse


/**
 * Created by bayunvnt on 09/10/17.
 */

class Preference {

    object Key {
        val ACCESS_TOKEN = "Key.AccessToken"
        val AUTH = "Key.Auth"
        val KEY_SESSION_AUTH = "Key.SessionAuth"
        val TOKEN = "token"
        val COORDINATE_MAPS = "Key.CoordinateMaps"
        val COORDINATE_MAPBOX = "Key.CoordinateMapBox"
        val KEY_DATA_SPLASH = "Key.SaveDataSyncSplashScreen"
        val KEYDATADINAMIS = "Key.SaveDataDinamisSyncSplashScreen"
    }

    companion object {

        val accessToken: String
            get() = Hawk.get(Key.ACCESS_TOKEN)

        fun saveAccessToken(accessToken: String) {
            Hawk.put(Key.ACCESS_TOKEN, accessToken)
        }

        val auth: LoginResponse
            get() = Hawk.get(Key.AUTH)

        fun saveAuth(auth: LoginResponse) {
            Hawk.put(Key.AUTH, auth)
        }


        /* session auth */
        fun sessionAuth(): SessionAuth? {
            return Hawk.get<Any>(Key.KEY_SESSION_AUTH) as? SessionAuth
        }

        fun savesessionAuth(sessionAuth: SessionAuth) {
            Hawk.put(Key.KEY_SESSION_AUTH, sessionAuth)
        }

        fun saveDataKoordinatMapBox(dataKoordinatMaps: DataKoordinatMapBox) {
            Hawk.put(Key.COORDINATE_MAPBOX, dataKoordinatMaps)
        }

        fun dataKoordinatMapBox(): DataKoordinatMapBox? {
            return Hawk.get<Any>(Key.COORDINATE_MAPBOX) as? DataKoordinatMapBox
        }

        fun deleteDataKoordinatMapBox() {
            Hawk.delete(Key.COORDINATE_MAPBOX)
        }

        fun getsaveSyncDataSplashScreen(): SyncDataSplashScreen? {
            return Hawk.get<Any>(Key.KEY_DATA_SPLASH) as? SyncDataSplashScreen
        }

        fun saveSyncDataSplashScreen(data: SyncDataSplashScreen) {
            Hawk.put(Key.KEY_DATA_SPLASH, data)
        }

        fun deleteSyncDataSplashScreen() {
            Hawk.delete(Key.KEY_DATA_SPLASH)
        }


        /* dinamis data */
        fun getsaveSyncDataDinamisSplashScreen(): SyncDataDinamisSplashScreen? {
            return Hawk.get<Any>(Key.KEYDATADINAMIS) as? SyncDataDinamisSplashScreen
        }

        fun saveSyncDataDinamisSplashScreen(data: SyncDataDinamisSplashScreen) {
            Hawk.put(Key.KEYDATADINAMIS, data)
        }

    }
}
