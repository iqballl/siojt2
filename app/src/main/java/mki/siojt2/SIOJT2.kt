package mki.siojt2

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import com.mapbox.mapboxsdk.Mapbox
import com.orhanobut.hawk.Hawk
import dagger.android.DaggerApplication
import io.realm.Realm
import io.realm.RealmConfiguration
import mki.siojt2.dagger.component.AppComponent
import mki.siojt2.dagger.component.DaggerAppComponent
import mki.siojt2.dagger.modules.AppModule
import mki.siojt2.dagger.modules.NetworkModule
import mki.siojt2.model.localsave.Session
import mki.siojt2.utils.network.ConnectivityReceiver
import mki.siojt2.utils.network.sample.InternetConnectionListener
import mki.siojt2.utils.realm.RealmMigrations

/**
 * Created by iqbal on 09/10/17.
 */

open class SIOJT2 : Application() {
    override fun onCreate() {
        super.onCreate()

        session = Session(this)
        session.setIdAndStatus("", "", "")

        System.setProperty("org.apache.poi.javax.xml.stream.XMLInputFactory", "com.fasterxml.aalto.stax.InputFactoryImpl")
        System.setProperty("org.apache.poi.javax.xml.stream.XMLOutputFactory", "com.fasterxml.aalto.stax.OutputFactoryImpl")
        System.setProperty("org.apache.poi.javax.xml.stream.XMLEventFactory", "com.fasterxml.aalto.stax.EventFactoryImpl")

        // init hawk
        Hawk.init(this).build()

        // Inisialisasi Dagger
        initDagger()

        //inisialisasi Realm Database
        initRealm()

        /*instance = this
        component = AppComponent.Initializer.init(this)
        component!!.inject(this)
        context = applicationContext*/

    }

    override fun onTerminate() {
        super.onTerminate()
        Realm.getDefaultInstance().close()
    }

    private fun initDagger() {
        instance = this
        component = DaggerAppComponent.builder()
                .networkModule(NetworkModule(this))
                .appModule(AppModule())
                .build()
        component!!.inject(this)
    }

    private fun initRealm() {
        Realm.init(this)
        // The RealmConfiguration is created using the builder pattern.
        // The Realm file will be located in Context.getFilesDir() with name
        // "SIOJT.realm"
        val config = RealmConfiguration.Builder()
                .schemaVersion(0)
                .name("SIOJT.realm")
                .migration(RealmMigrations())
                .build()
        Realm.getInstance(config)
    }

    fun setConnectivityListener(listener: ConnectivityReceiver.ConnectivityReceiverListener) {
        ConnectivityReceiver.connectivityReceiverListener = listener
    }

    fun setInternetConnectionListener(listener: InternetConnectionListener) {
        mInternetConnectionListener = listener
    }

    fun removeInternetConnectionListener() {
        mInternetConnectionListener = null
    }

    companion object {
        private lateinit var session: Session
        var component: AppComponent? = null
            private set
        @SuppressLint("StaticFieldLeak")
        var context: Context? = null
            private set

        @SuppressLint("StaticFieldLeak")
        @get:Synchronized
        var instance: SIOJT2? = null

        var mInternetConnectionListener: InternetConnectionListener? = null

    }

}
