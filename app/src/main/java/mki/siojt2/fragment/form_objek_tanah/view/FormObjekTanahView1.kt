package mki.siojt2.fragment.form_objek_tanah.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.region.ResponseDataCity
import mki.siojt2.model.region.ResponseDataKecamatan
import mki.siojt2.model.region.ResponseDataKelurahan
import mki.siojt2.model.region.ResponseDataProvince

/**
 * Created by iqbal on 09/09/19
 */

interface FormObjekTanahView1 : MvpView {
    fun onsuccessgetregionProvince(responseDataProvince: MutableList<ResponseDataProvince>)
    fun onsuccessgetregionCity(responseDataCity: MutableList<ResponseDataCity>)
    fun onsuccessgetregionKecamatan(responseDataKecamatan: MutableList<ResponseDataKecamatan>)
    fun onsuccessgetregionKelurahan(responseDataKelurahan: MutableList<ResponseDataKelurahan>)
}
