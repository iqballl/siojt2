package mki.siojt2.fragment.form_objek_tanah

import android.content.Context
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.gson.Gson
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_form_objek_add_tanah_2.*
import kotlinx.android.synthetic.main.item_add_jenis_tanah.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.fragment.form_objek_tanah.presenter.FormObjekTanahPresenter2
import mki.siojt2.fragment.form_objek_tanah.view.FormObjekTanahView2
import mki.siojt2.fragment.form_subjek.adapter_recycleview.AdapterMataPencaharian
import mki.siojt2.model.data_form_subjek.DataMataPencaharian
import mki.siojt2.model.data_form_tanah.DataFormTanah2
import mki.siojt2.model.data_form_tanah.DataJenisTanah
import mki.siojt2.model.localsave.ProjectLand
import mki.siojt2.model.localsave.Session
import mki.siojt2.model.master_data_form_tanah.*
import mki.siojt2.model.master_data_subjek.ResponseDataMataPencaharianWithOutParam
import mki.siojt2.ui.activity_form_add_objek_tanah.view.ActicityFormAddObjekTanah
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.Utils


class FormObjekTanah2 : BaseFragment(), FormObjekTanahView2, View.OnClickListener {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    private var mListener: OnStepTwoListener? = null

    private var validdataMataPencaharian: Boolean = false

    lateinit var adapterMataPencaharian: AdapterMataPencaharian

    /*val valuebentukTanah = mutableListOf("Segiempat", "Poligon", "Jajar Genjang", "Tidak Beraturan")
    val valuejenisTanah = mutableListOf("Tanah mentah", "Tanah matang", "Tanah persawahan", "Tanah perkebunan",
            "Lain-Nya")

    val valueTopografi = mutableListOf("Datar", "Begelombang")*/

    private var sendIdPosisiTanah: Int = 0
    private var sendIdBentukTanah: Int = 0
    private var sendIdJenisTanah: Int = 0
    private var sendIdTipografiTanah: Int = 0

    private var sendlebarjalanDpnTanah: Float = 0F

    private var dataFormTanah2: MutableList<DataFormTanah2> = ArrayList()

    lateinit var formObjekTanahPresenter2: FormObjekTanahPresenter2

    private var senvalueEdLebarJalan: Float? = 0.00000F
    private var senvalueEdLebarJalanDepan: Float? = 0.00000F
    private var senvalueEdpanjangKebelakang: Float? = 0.00000F

    lateinit var realm: Realm
    lateinit var session: Session


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }

        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        session = Session(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_objek_add_tanah_2, container, false)
    }

    override fun setUp(view: View) {
        val resultsRealm = realm.where(ResponseDataMataPencaharianWithOutParam::class.java).findAll()
        val dataMataPencaharian: MutableList<ResponseDataMataPencaharianWithOutParam> = realm.copyFromRealm(resultsRealm)

        adapterMataPencaharian = AdapterMataPencaharian(dataMataPencaharian, activity!!, this)
        val linearLayoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.VERTICAL, false)
        rvmataPencaharian.layoutManager = linearLayoutManager
        rvmataPencaharian.adapter = adapterMataPencaharian

        titleluastTanah.text = Html.fromHtml("Luas tanah (m<sup><small>2</small></sup>)")
        titleluastTanahTerkenaDampak.text = Html.fromHtml("Luas Tanah yang terkena dampak (m<sup><small>2</small></sup>)")

        if((activity as ActicityFormAddObjekTanah?)?.isSimpleMode!!){
            llposisiTanahTrhdpJln?.visibility = View.GONE
            lllebarJalan1?.visibility = View.GONE
            lllebarJalan2?.visibility = View.GONE
            lllebarJalan3?.visibility = View.GONE
            llbentukTanah?.visibility = View.GONE
            llluasTanahTerdampak?.visibility = View.GONE
            edlainnyajenisTanah?.visibility = View.GONE
            llTopgrafi?.visibility = View.GONE
            lltinggiTanah?.visibility = View.GONE
        } else {
            llposisiTanahTrhdpJln?.visibility = View.VISIBLE
            lllebarJalan1?.visibility = View.VISIBLE
            lllebarJalan2?.visibility = View.VISIBLE
            lllebarJalan3?.visibility = View.VISIBLE
            llbentukTanah?.visibility = View.VISIBLE
            llluasTanahTerdampak?.visibility = View.VISIBLE
            llTopgrafi?.visibility = View.VISIBLE
            lltinggiTanah?.visibility = View.VISIBLE
        }

        getDataForEdit()

        /* data offline posisi tanah */
        val results = realm.where(ResponseDataListPosisiTanahWithOutParam::class.java).findAll()
        val dataPosisiTanah: MutableList<ResponseDataListPosisiTanahWithOutParam> = realm.copyFromRealm(results)

        val adapterposisiTanah = ArrayAdapter(activity!!, R.layout.item_spinner, dataPosisiTanah)
        spinnerPosisiTanah.setAdapter(adapterposisiTanah)
        spinnerPosisiTanah.onItemClickListener = AdapterView.OnItemClickListener { adapterView, _, position, _ ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()

            val dataSpinner = adapterView.getItemAtPosition(position) as ResponseDataListPosisiTanahWithOutParam
            sendIdPosisiTanah = dataSpinner.getpositionToRoadId()!!
            //Toast.makeText(activity!!, sendIdPosisiTanah.toString(), Toast.LENGTH_SHORT).show()
        }

        /* data offline bentuk tanah */
        val resultRealm = realm.where(ResponseDataListBentukTanahWithOutParam::class.java).findAllAsync()
        val dataBentukTanah: MutableList<ResponseDataListBentukTanahWithOutParam> = realm.copyFromRealm(resultRealm)

        val adapterbentukTanah = ArrayAdapter<ResponseDataListBentukTanahWithOutParam>(activity!!, R.layout.item_spinner, dataBentukTanah)
        spinnerBentukTanah.setAdapter(adapterbentukTanah)
        spinnerBentukTanah.onItemClickListener = AdapterView.OnItemClickListener { adapterView, _, position, _ ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            val dataSpinner = adapterView.getItemAtPosition(position) as ResponseDataListBentukTanahWithOutParam
            sendIdBentukTanah = dataSpinner.getshapeId()!!

            if (valueSpinner == "Lain-Nya") {
                llbentukTanahLain.visibility = View.VISIBLE
            } else {
                llbentukTanahLain.visibility = View.GONE
                edbentukLain.text = null
            }
            //Toast.makeText(activity!!, sendIdBentukTanah.toString(), Toast.LENGTH_SHORT).show()
        }

        /* data offline topografi tanah */
        val resultRealmTopografi = realm.where(ResponseDataListTipografiWithOutParam::class.java).findAllAsync()
        val dataTopografiTanah: MutableList<ResponseDataListTipografiWithOutParam> = realm.copyFromRealm(resultRealmTopografi)

        val adapterTopografi = ArrayAdapter<ResponseDataListTipografiWithOutParam>(activity!!, R.layout.item_spinner, dataTopografiTanah)
        spinnerTopografi.setAdapter(adapterTopografi)

        spinnerTopografi.onItemClickListener = AdapterView.OnItemClickListener { adapterView, _, position, _ ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            val dataSpinner = adapterView.getItemAtPosition(position) as ResponseDataListTipografiWithOutParam
            sendIdTipografiTanah = dataSpinner.getTypoGraphyId()!!
            //Toast.makeText(activity!!, sendIdTipografiTanah.toString(), Toast.LENGTH_SHORT).show()
        }

        /*spinnerBentukTanah.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            sendIdBentukTanah = kategoriIdBentukTanah[position]
            //Toast.makeText(activity!!, sendIdBentukTanah.toString(), Toast.LENGTH_SHORT).show()
        }

        spinnerjenisTanah.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            sendIdJenisTanah = kategoriIdJenisTanah[position]
            //Toast.makeText(activity!!, sendIdJenisTanah.toString(), Toast.LENGTH_SHORT).show()
            *//*if (position + 1 == 5) {
                edlainnyajenisTanah.visibility = View.VISIBLE
                Toast.makeText(activity!!, "Explain the other type of land if select 'Lainnya' ", Toast.LENGTH_LONG).show()
            } else {
                edlainnyajenisTanah.visibility = View.GONE
            }*//*
        }

        spinnerTopografi.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            sendIdTipografiTanah = kategoriIdTipografiTanah[position]
            //Toast.makeText(activity!!, sendIdTipografiTanah.toString(), Toast.LENGTH_SHORT).show()
        }*/

    }

    private fun getDataForEdit() {
        session = Session(context)
        if (session.id != "" && session.status == "2") {
            val results = realm.where(ProjectLand::class.java).equalTo("LandIdTemp", session.id.toInt()).findFirst()
            if (results != null) {
                tampilkanData(results)

                //mata pencaharian
                val liveId = results.livelihoodLiveId
                val replace1 = liveId.replace("[", "['")
                val replace2 = replace1.replace("]", "']")
                val replace3 = replace2.replace(",", "','")

                val livename = results.livelihoodLiveName
                val livename1 = livename.replace("[", "['")
                val livename2 = livename1.replace("]", "']")
                val livename4 = livename2.replace(",", "','")

                val livenameother = results.livelihoodOther
                val livenameother2 = livenameother.replace("[", "['")
                val livenameother3 = livenameother2.replace("]", "']")
                val livenameother4 = livenameother3.replace(",", "','")

                val liveIncome = results.livelihoodIncome
                val liveIncome1 = liveIncome.replace("[", "['")
                val liveIncome2 = liveIncome1.replace("]", "']")
                val liveIncome3 = liveIncome2.replace(",", "','")
                val liveIncome4 = liveIncome3.replace(" ", "")

                val convertlistliveId = Gson().fromJson(replace3, Array<String>::class.java).toList()
                val convertlistliveName = Gson().fromJson(livename4, Array<String>::class.java).toList()
                val convertlistliveNameOther = Gson().fromJson(livenameother4, Array<String>::class.java).toList()
                val convertlistliveValue = Gson().fromJson(liveIncome4, Array<String>::class.java).toList()

                for (i in convertlistliveId.indices) {
                    val dataMataPencaharian = DataMataPencaharian()
                    dataMataPencaharian.livelihoodLiveId = convertlistliveId[i]
                    dataMataPencaharian.livelihoodLiveName = convertlistliveName[i]
                    dataMataPencaharian.livelihoodOther = convertlistliveNameOther[i]
                    dataMataPencaharian.livelihoodIncome = convertlistliveValue[i]

                    adapterMataPencaharian.addItems(dataMataPencaharian)
                    adapterMataPencaharian.notifyDataSetChanged()
                }
                Log.d("results", results.toString())

            } else {
                Log.e("data", "kosong")
            }
        } else {
            val mdataJenisTanah = DataJenisTanah()
            mdataJenisTanah.projectLandTypeId = ""
            mdataJenisTanah.projectLandTypeName = ""
            mdataJenisTanah.projectLandTypeNameOther = ""

            val dataMataPencaharian = DataMataPencaharian()
            dataMataPencaharian.livelihoodLiveId = ""
            dataMataPencaharian.livelihoodLiveName = ""
            dataMataPencaharian.livelihoodOther = ""
            dataMataPencaharian.livelihoodIncome = ""

            adapterMataPencaharian.addItems(dataMataPencaharian)
            adapterMataPencaharian.notifyDataSetChanged()
        }
    }

    private fun tampilkanData(results: ProjectLand) {
        sendIdPosisiTanah = results.landPositionToRoadId
        spinnerPosisiTanah.setText(results.landPositionToRoadName)
        edlebarJalan.setText(results.landWideFrontRoad.toString())
        edLebarJalanDepan.setText(results.landFrontage.toString())
        edPanjangKebelakang.setText(results.landLength.toString())
        sendIdBentukTanah = results.landShapeId
        spinnerBentukTanah.setText(results.landShapeName)

        if (results.landShapeNameOther.isNotEmpty()) {
            llbentukTanahLain.visibility = View.VISIBLE
            edbentukLain.setText(results.landShapeNameOther)
        } else {
            llbentukTanahLain.visibility = View.GONE
        }

        edLuasTanah.setText(results.landAreaCertificate)
        edLuasTanahTerkenaDampak.setText(results.landAreaAffected)
        edTanahSisa.setText(results.tanahSisa)
        //jenis tanah


        val jtid1 = results.landTypeLandTypeId
        val jtid2 = jtid1.replace("[", "['")
        val jtid3 = jtid2.replace("]", "']")
        val jtid = jtid3.replace(",", "', '")
        val listjtId = Gson().fromJson(jtid, Array<String>::class.java).toList()

        val jtname1 = results.landTypeLandTypeValue
        val jtname2 = jtname1.replace("[", "['")
        val jtname3 = jtname2.replace("]", "']")
        val jtname = jtname3.replace(", ", "','")
        val listjtName = Gson().fromJson(jtname, Array<String>::class.java).toList()

        val landtypenameOther = results.landTypeLandTypeNameOther
        val landtypenameOther1 = landtypenameOther.replace("[", "['")
        val landtypenameOther2 = landtypenameOther1.replace("]", "']")
        val landtypenameOther3 = landtypenameOther2.replace(", ", "','")
        val landtypenameOtherRef = Gson().fromJson(landtypenameOther3, Array<String>::class.java).toList()


        for (i in listjtId.indices) {
            val mdataJenisTanah = DataJenisTanah()
            mdataJenisTanah.projectLandTypeId = listjtId[i]
            mdataJenisTanah.projectLandTypeName = listjtName[i]
            mdataJenisTanah.projectLandTypeNameOther = landtypenameOtherRef[i]
        }

        sendIdTipografiTanah = results.landTypographyId.toInt()
        spinnerTopografi.setText(results.landTypographyName)
        edTinggiTanah.setText(results.landHeightToRoad)

    }

    private fun getPresenter(): FormObjekTanahPresenter2? {
        formObjekTanahPresenter2 = FormObjekTanahPresenter2()
        formObjekTanahPresenter2.onAttach(this)
        return formObjekTanahPresenter2
    }

    private fun validate(): Boolean {
        val valid: Boolean
        val sweetAlretLoading = SweetAlertDialog(activity!!, SweetAlertDialog.WARNING_TYPE)
        sweetAlretLoading.setCancelable(false)
        sweetAlretLoading.confirmText = "OK"
        sweetAlretLoading.setConfirmClickListener { sDialog ->
            sDialog?.let { if (it.isShowing) it.dismiss() }
        }
        if (edLuasTanah.text.toString().isEmpty()) {
            sweetAlretLoading.titleText = "Lengkapi data luas tanah!"
            sweetAlretLoading.show()
            edLuasTanah.requestFocus()
            valid = false
        } else if (spinnerBentukTanah.text.toString() == "Lain-Nya" && edbentukLain.text.toString().isEmpty()) {
            edjenistanahLain.requestFocus()
            sweetAlretLoading.titleText = "Lengkapi data bentuk tanah!"
            sweetAlretLoading.show()
            valid = false
        }
        else if(!validdataMataPencaharian){
            sweetAlretLoading.titleText = "Lengkapi data mata pencaharian!"
            sweetAlretLoading.show()
            valid = false
        }
        else {
            valid = true
        }

        return valid
    }

    override fun onsuccessgetdaftarposisitanahTerhadapJalan(responseDataListPosisiTanah: MutableList<ResponseDataListPosisiTanah>) {

        /*for (row in responseDataListPosisiTanah) {
            //idPekerjaan.add(row.occupationId.toString())
            kategoriIdPosisiTanah.add(row.positionToRoadId!!)
            kategoriPosisiTanah.add(row.positionToRoadName.toString())
        }

        val adapterposisiTanah = ArrayAdapter<String>(activity!!, R.layout.item_spinner, kategoriPosisiTanah)
        spinnerPosisiTanah.setAdapter(adapterposisiTanah)

        getPresenter()?.getdaftarBentukTanah()*/

    }

    override fun onsuccessgetdaftarBentukTanah(responseDataListBentukTanah: MutableList<ResponseDataListBentukTanah>) {

        /*for (row in responseDataListBentukTanah) {
            //idPekerjaan.add(row.occupationId.toString())
            kategoriIdBentukTanah.add(row.shapeId!!)
            kategoriBentukTanah.add(row.shapeName.toString())
        }

        val adapterbentukTanah = ArrayAdapter<String>(activity!!, R.layout.item_spinner, kategoriBentukTanah)
        spinnerBentukTanah.setAdapter(adapterbentukTanah)

        getPresenter()?.getdaftarJenisTanah()*/

    }

    override fun onsuccessgetdaftarTipografiTanah(responseDataListTipografi: MutableList<ResponseDataListTipografi>) {

        /* for (row in responseDataListTipografi) {
            //idPekerjaan.add(row.occupationId.toString())
            kategoriIdTipografiTanah.add(row.typographyId!!)
            kategoriTipografiTanah.add(row.typographyName.toString())
        }

        val adapterTopografi = ArrayAdapter<String>(activity!!, R.layout.item_spinner, kategoriTipografiTanah)
        spinnerTopografi.setAdapter(adapterTopografi)*/
    }

    override fun onResume() {
        super.onResume()
        btnprevObjekTanah2?.setOnClickListener(this)
        btnnetxtObjekTanah2?.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        btnprevObjekTanah2?.setOnClickListener(null)
        btnnetxtObjekTanah2?.setOnClickListener(null)
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btnnetxtObjekTanah2) {
            hideKeyboard()
            dataFormTanah2.clear()
            Utils.deletedataformtanah2(activity!!)

            val midjenisTanah: ArrayList<String> = ArrayList()
            val mnamajenisTanah: ArrayList<String> = ArrayList()
            val mnamajenisTanahLain: ArrayList<String> = ArrayList()

            val getdataMataPencharian = adapterMataPencaharian.getdataPencaharian()
            val midmataPencharian: ArrayList<String> = ArrayList()
            val mnamamataPencharian: ArrayList<String> = ArrayList()
            val mnamamataPencharianLain: ArrayList<String> = ArrayList()
            val mjumlahMataPencharian: ArrayList<String> = ArrayList()

            loop@ for (i in getdataMataPencharian.indices) {
                when {
                    getdataMataPencharian[i].livelihoodLiveId!!.isEmpty() || getdataMataPencharian[i].livelihoodIncome!!.isEmpty() -> {
                        validdataMataPencaharian = false
                        break@loop
                    }
                    getdataMataPencharian[i].livelihoodLiveName!! == "Lain-Nya" && getdataMataPencharian[i].livelihoodIncome!!.isEmpty() ||
                            getdataMataPencharian[i].livelihoodLiveName!! == "Lain-Nya" && getdataMataPencharian[i].livelihoodOther!!.isEmpty() -> {
                        validdataMataPencaharian = false
                        break@loop
                    }
                    else -> {
                        midmataPencharian.add(getdataMataPencharian[i].livelihoodLiveId!!)
                        mnamamataPencharian.add(getdataMataPencharian[i].livelihoodLiveName!!)
                        mnamamataPencharianLain.add(getdataMataPencharian[i].livelihoodOther!!)
                        mjumlahMataPencharian.add(getdataMataPencharian[i].livelihoodIncome!!)

                        validdataMataPencaharian = true
                    }
                }
            }

            senvalueEdLebarJalan = if (edlebarJalan.text.toString() != "") {
                java.lang.Float.parseFloat(edlebarJalan.text.toString())
            } else {
                0.00000F
            }

            senvalueEdLebarJalanDepan = if (edLebarJalanDepan.text.toString() != "") {
                java.lang.Float.parseFloat(edLebarJalanDepan.text.toString())
            } else {
                0.00000F
            }

            senvalueEdpanjangKebelakang = if (edPanjangKebelakang.text.toString() != "") {
                java.lang.Float.parseFloat(edPanjangKebelakang.text.toString())
            } else {
                0.00000F
            }


            if (validate()) {
                dataFormTanah2.add(DataFormTanah2(
                        sendIdPosisiTanah,
                        spinnerPosisiTanah.text.toString().trim(),
                        senvalueEdLebarJalan,
                        senvalueEdLebarJalanDepan,
                        senvalueEdpanjangKebelakang,
                        sendIdBentukTanah,
                        spinnerBentukTanah.text.toString().trim(),
                        edbentukLain.text.toString(),
                        edLuasTanah.text.toString(),
                        edLuasTanahTerkenaDampak.text.toString(),
                        midjenisTanah,
                        mnamajenisTanah,
                        mnamajenisTanahLain,
                        sendIdTipografiTanah,
                        spinnerTopografi.text.toString(),
                        edTinggiTanah.text.toString(),
                        edTanahSisa.text.toString(),
                        midmataPencharian,
                        mnamamataPencharianLain,
                        mjumlahMataPencharian,
                        mnamamataPencharian
                ))

                val jsonBBM = Gson().toJson(dataFormTanah2)
                Utils.savedataformtanah2(jsonBBM, activity!!)

                //val turnsType = object : TypeToken<MutableList<DataFormTanah2>>() {}.type
                //val dataconvert = Gson().fromJson<MutableList<DataFormTanah2>>(Utils.getdataformtanah2(activity!!), turnsType)

                //Log.d("babab2", dataconvert.toString())


                if (mListener != null) {
                    mListener!!.onNextPressed(this)
                }
            }

            /*val getidmultipleJenisTanah = adapJenisTanah.getStepList()
            val getValuemultipleJenisTanah = adapJenisTanah.getStepList1()

            getidmultipleJenisTanah.forEach {
                if (it.isNotEmpty() || it != "") {
                    sendidjjenistanahMultiple.add(it)
                }
            }

            getValuemultipleJenisTanah.forEach {
                if (it.isNotEmpty() || it != "") {
                    sendValueJenisTanahMultiple.add(it)
                }
            }

            senvalueEdLebarJalan = if (edlebarJalan.text.toString() != "") {
                java.lang.Float.parseFloat(edlebarJalan.text.toString())
            } else {
                0.00000F
            }


            senvalueEdLebarJalanDepan = if (edLebarJalanDepan.text.toString() != "") {
                java.lang.Float.parseFloat(edLebarJalanDepan.text.toString())
            } else {
                0.00000F
            }

            senvalueEdpanjangKebelakang = if (edLebarJalanDepan.text.toString() != "") {
                java.lang.Float.parseFloat(edLebarJalanDepan.text.toString())
            } else {
                0.00000F
            }

            //Log.d("babab2", sendidjjenistanahMultiple.toString())
            if (validate()) {
                dataFormTanah2.add(DataFormTanah2(
                        sendIdPosisiTanah,
                        spinnerPosisiTanah.text.toString().trim(),
                        senvalueEdLebarJalan,
                        senvalueEdLebarJalanDepan,
                        senvalueEdpanjangKebelakang,
                        sendIdBentukTanah,
                        spinnerBentukTanah.text.toString().trim(),
                        edbentukLain.text.toString(),
                        edLuasTanah.text.toString(),
                        edLuasTanahTerkenaDampak.text.toString(),
                        sendidjjenistanahMultiple,
                        sendValueJenisTanahMultiple,
                        sendValueJenisTanahMultiple,
                        sendIdTipografiTanah,
                        spinnerTopografi.text.toString(),
                        edTinggiTanah.text.toString()
                ))

                val jsonBBM = Gson().toJson(dataFormTanah2)
                Utils.savedataformtanah2(jsonBBM, activity!!)

                Handler().postDelayed({

                    //val turnsType = object : TypeToken<MutableList<DataFormTanah2>>() {}.type
                    //val dataconvert = Gson().fromJson<MutableList<DataFormTanah2>>(Utils.getdataformtanah2(activity!!), turnsType)

                    //Log.d("babab2", dataconvert.toString())
                    if (mListener != null) {
                        mListener!!.onNextPressed(this)
                    }

                }, 500)
            }*/
            /*if (mListener != null) {
                mListener!!.onNextPressed(this)
            }*/

        } else if (view.id == R.id.btnprevObjekTanah2) {
            if (mListener != null)
                mListener!!.onBackPressed(this)
        }
    }

    fun addItemMataPencaharian(positon: Int) {
        val dataMataPencaharian = DataMataPencaharian()
        dataMataPencaharian.livelihoodLiveId = ""
        dataMataPencaharian.livelihoodLiveName = ""
        dataMataPencaharian.livelihoodOther = ""
        dataMataPencaharian.livelihoodIncome = ""

        adapterMataPencaharian.addItems(dataMataPencaharian)
        adapterMataPencaharian.notifyDataSetChanged()
//        rvmataPencaharian.scrollToPosition(adapterMataPencaharian.itemCount - 1)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnStepTwoListener) {
            mListener = context
        } else {
            throw RuntimeException(("$context must implement OnFragmentInteractionListener"))
        }
    }

    fun addItem() {
        val mdataJenisTanah = DataJenisTanah()
        mdataJenisTanah.projectLandTypeId = ""
        mdataJenisTanah.projectLandTypeName = ""
        mdataJenisTanah.projectLandTypeNameOther = ""
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
        getPresenter()?.onDetach()
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    interface OnStepTwoListener {
        fun onBackPressed(fragment: androidx.fragment.app.Fragment)
        fun onNextPressed(fragment: androidx.fragment.app.Fragment)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
// TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): FormObjekTanah2 {
            val fragment = FormObjekTanah2()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor