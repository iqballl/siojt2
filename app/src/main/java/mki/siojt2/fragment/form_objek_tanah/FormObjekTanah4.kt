package mki.siojt2.fragment.form_objek_tanah

import android.content.Context
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.gson.Gson
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_form_objek_add_tanah_4.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.fragment.form_objek_tanah.adapter_recycleview.AdapterInformasiTambahan
import mki.siojt2.fragment.form_objek_tanah.presenter.FormObjekTanahPresenter4
import mki.siojt2.fragment.form_objek_tanah.view.FormObjekTanahView4
import mki.siojt2.model.data_form_tanah.DataFormTanah4
import mki.siojt2.model.localsave.ProjectLand
import mki.siojt2.model.localsave.Session
import mki.siojt2.model.master_data_form_tanah.*
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.Utils


class FormObjekTanah4 : BaseFragment(), FormObjekTanahView4, View.OnClickListener {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    private var mListener: OnStepFourListener? = null

    val valueinfromasiTamabahan = mutableListOf("Properti sedang menjadi objek perkara di pengadilan",
            "Properti masih dipersengketakan kepemilikannya", "Properti diletakkan sita oleh pejabat yang berwenang",
            "Properti masih menjadi jaminan di bank", "Pihak yang berhak tidak diketahui keberadaannya")


    lateinit var formObjekTanahPresenter4: FormObjekTanahPresenter4

    private var dataFormTanah4: MutableList<DataFormTanah4> = ArrayList()

    private var sendiIdKemudahanDijangkau: Int? = 0
    private var sendiIdKemudahanPerbelanjaan: Int? = 0
    private var sendiIdKemudahanPendidikan: Int? = 0
    private var sendiIdKemudahanDiWisata: Int? = 0
    private var sendiIdKemudahanTransportasi: Int? = 0

    private var sendiIdKeamananKejahatan: Int? = 0
    private var sendiIdKeamananKebakaran: Int? = 0
    private var sendiIdKeamananBencanaAlam: Int? = 0

    private var sendidFasilitasiListrik: Int? = 0
    private var sendidFasilitasiAirBersih: Int? = 0
    private var sendidFasilitasiTelepon: Int? = 0
    private var sendidFasilitasiGas: Int? = 0

    private var sendidKeberadaaanSutet: Int? = 0
    private var sendidKeberadaaanMakam: Int? = 0
    private var sendidKeberadaaanTusukSate: Int? = 0

    private var sendIdInformasiTambahanTanah: Int? = 0

    private var senvalueSutet: Float? = 0.00000F
    private var senvalueJarakMakam: Float? = 0.00000F

    lateinit var realm: Realm
    lateinit var session : Session


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        session = Session(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_objek_add_tanah_4, container, false)
    }

    override fun setUp(view: View) {
        //getPresenter()?.getinfromasiTambahan()

        GetDataForEdit()

        val valueeaseOfReach = mutableListOf(
                ResponseDataListEaseOfReach(1, "Baik"),
                ResponseDataListEaseOfReach(2, "Menengah"),
                ResponseDataListEaseOfReach(3, "Buruk")
        )

        val valueriskLevel = mutableListOf(
                ResponseDataListRiskLevel(1, "Tinggi"),
                ResponseDataListRiskLevel(2, "Sedang"),
                ResponseDataListRiskLevel(3, "Rendah")
        )

        val valueyesNo = mutableListOf(
                ResponseDataListYesNo(1, "Ya"),
                ResponseDataListYesNo(2, "Tidak")
        )

        val valueexistorNO = mutableListOf(
                ResponseDataListExistOrNo(1, "Ada"),
                ResponseDataListExistOrNo(2, "Tidak Ada")
        )

        val adaptereaseOfReach = ArrayAdapter(activity!!, R.layout.item_spinner, valueeaseOfReach)
        val adapterriskLevel = ArrayAdapter(activity!!, R.layout.item_spinner, valueriskLevel)
        val adapteryesNo = ArrayAdapter(activity!!, R.layout.item_spinner, valueyesNo)
        val adapterexistOrNo = ArrayAdapter(activity!!, R.layout.item_spinner, valueexistorNO)

        spinnerKemudahanDicapai.setAdapter(adaptereaseOfReach)
        spinnerKemudahanDicapai.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()

            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListEaseOfReach
            sendiIdKemudahanDijangkau = selectedProject.easeOfReachId
            //Toast.makeText(activity!!, sendiIdKemudahanDijangkau.toString(), Toast.LENGTH_SHORT).show()

        }

        spinnerKemudahanPerbelanjaan.setAdapter(adaptereaseOfReach)
        spinnerKemudahanPerbelanjaan.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()

            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListEaseOfReach
            sendiIdKemudahanPerbelanjaan = selectedProject.easeOfReachId
            //Toast.makeText(activity!!, sendiIdKemudahanPerbelanjaan.toString(), Toast.LENGTH_SHORT).show()

        }

        spinnerKemudahanPendidikan.setAdapter(adaptereaseOfReach)
        spinnerKemudahanPendidikan.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()

            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListEaseOfReach
            sendiIdKemudahanPendidikan = selectedProject.easeOfReachId
            //Toast.makeText(activity!!, sendiIdKemudahanPendidikan.toString(), Toast.LENGTH_SHORT).show()

        }

        spinnerKemudahanWisata.setAdapter(adaptereaseOfReach)
        spinnerKemudahanWisata.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListEaseOfReach
            sendiIdKemudahanDiWisata = selectedProject.easeOfReachId
            //Toast.makeText(activity!!, sendiIdKemudahanDiWisata.toString(), Toast.LENGTH_SHORT).show()

        }

        spinnerKemudahanTransportasi.setAdapter(adaptereaseOfReach)
        spinnerKemudahanTransportasi.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()

            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListEaseOfReach
            sendiIdKemudahanTransportasi = selectedProject.easeOfReachId
            //Toast.makeText(activity!!, sendiIdKemudahanTransportasi.toString(), Toast.LENGTH_SHORT).show()

        }

        spinnerkeamananKejahatan.setAdapter(adapterriskLevel)
        spinnerkeamananKejahatan.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListRiskLevel
            sendiIdKeamananKejahatan = selectedProject.riskLevelId
            //Toast.makeText(activity!!, sendiIdKeamananKejahatan.toString(), Toast.LENGTH_SHORT).show()

        }

        spinnerkeamananKebakaran.setAdapter(adapterriskLevel)
        spinnerkeamananKebakaran.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListRiskLevel
            sendiIdKeamananKebakaran = selectedProject.riskLevelId
            //Toast.makeText(activity!!, sendiIdKeamananKebakaran.toString(), Toast.LENGTH_SHORT).show()

        }

        spinnerkeamananBencanaAlam.setAdapter(adapterriskLevel)
        spinnerkeamananBencanaAlam.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListRiskLevel
            sendiIdKeamananBencanaAlam = selectedProject.riskLevelId
            //Toast.makeText(activity!!, sendiIdKeamananBencanaAlam.toString(), Toast.LENGTH_SHORT).show()

        }

        /* ketersediaan fasilitas */
        spinnerfasilitasListrik.setAdapter(adapteryesNo)
        spinnerfasilitasListrik.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListYesNo
            sendidFasilitasiListrik = selectedProject.yesNoId
            //Toast.makeText(activity!!, sendidFasilitasiListrik.toString(), Toast.LENGTH_SHORT).show()

        }

        spinnerfasilitasSumberAir.setAdapter(adapteryesNo)
        spinnerfasilitasSumberAir.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListYesNo
            sendidFasilitasiAirBersih = selectedProject.yesNoId
            //Toast.makeText(activity!!, sendidFasilitasiAirBersih.toString(), Toast.LENGTH_SHORT).show()

        }

        spinnerfasilitasJaringanTelepon.setAdapter(adapteryesNo)
        spinnerfasilitasJaringanTelepon.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()

            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListYesNo
            sendidFasilitasiTelepon = selectedProject.yesNoId
            //Toast.makeText(activity!!, sendidFasilitasiTelepon.toString(), Toast.LENGTH_SHORT).show()

        }

        spinnerfasilitasGas.setAdapter(adapteryesNo)
        spinnerfasilitasGas.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()

            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListYesNo
            sendidFasilitasiGas = selectedProject.yesNoId
            //Toast.makeText(activity!!, sendidFasilitasiGas.toString(), Toast.LENGTH_SHORT).show()

        }

        /* keberadaan sutet */
        spinnerKebaradaanSUTET.setAdapter(adapterexistOrNo)
        spinnerKebaradaanSUTET.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListExistOrNo
            sendidKeberadaaanSutet = selectedProject.existOrNoId

            if (position == adapterView.firstVisiblePosition) {
                lljarakMeterSutet.visibility = View.VISIBLE
            } else {
                senvalueSutet = 0.0000F
                edjarakSutet.setText("")
                lljarakMeterSutet.visibility = View.GONE
            }
            //Toast.makeText(activity!!, sendidKeberadaaanSutet.toString(), Toast.LENGTH_SHORT).show()

        }

        /* keberadaan makam */
        spinnerKebaradaanMakam.setAdapter(adapterexistOrNo)
        spinnerKebaradaanMakam.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListExistOrNo
            sendidKeberadaaanMakam = selectedProject.existOrNoId

            if (position == adapterView.firstVisiblePosition) {
                lljarakMakan.visibility = View.VISIBLE
            } else {
                senvalueJarakMakam = 0.0000F
                edjarakMakam.setText("")
                lljarakMakan.visibility = View.GONE
            }
            //Toast.makeText(activity!!, sendidKeberadaaanMakam.toString(), Toast.LENGTH_SHORT).show()

        }

        spinnerTusukSate.setAdapter(adapteryesNo)
        spinnerTusukSate.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListYesNo
            sendidKeberadaaanTusukSate = selectedProject.yesNoId
        }

        /*spinnerinformasiTambahan.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()

            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListInformasiTambahanTanah
            sendIdInformasiTambahanTanah = selectedProject.addInfoId
            Toast.makeText(activity!!, sendIdInformasiTambahanTanah.toString(), Toast.LENGTH_SHORT).show()

        }*/


    }

    private fun GetDataForEdit() {
        session =  Session(context)
        if (!session.id.equals("") && session.status.equals("2")){
            val results = realm.where(ProjectLand::class.java).equalTo("LandIdTemp", session.id.toInt()).findFirst()
            if (results != null) {
                tampilkanData(results)
                Log.d("results", results.toString())
            } else {
                Log.e("data", "kosong")
            }
        }else{

        }
    }

    private fun tampilkanData(results: ProjectLand) {
        sendiIdKemudahanDijangkau = results.landEaseToPropertyId
        spinnerKemudahanDicapai.setText(results.landEaseToPropertyName)
        sendiIdKemudahanPerbelanjaan = results.landEaseToShopingCenterId
        spinnerKemudahanPerbelanjaan.setText(results.landEaseToShopingCenterName)
        sendiIdKemudahanPendidikan = results.landEaseToEducationFacilitiesId
        spinnerKemudahanPendidikan.setText(results.landEaseToEducationFacilitiesName)
        sendiIdKemudahanDiWisata = results.landEaseToTouristSitesId
        spinnerKemudahanWisata.setText(results.landEaseToTouristSitesName)
        sendiIdKemudahanTransportasi = results.landEaseOfTransportationId
        spinnerKemudahanTransportasi.setText(results.landEaseOfTransportationName)
        sendiIdKeamananKejahatan = results.landSecurityAgainstCrimeId
        spinnerkeamananKejahatan.setText(results.landSecurityAgainstCrimeName)
        sendiIdKeamananKebakaran = results.landSafetyAgainstFireHazardsId
        spinnerkeamananKebakaran.setText(results.landSafetyAgainstFireHazardsName)
        sendiIdKeamananBencanaAlam = results.landSecurityAgainstNaturalDisastersId
        spinnerkeamananBencanaAlam.setText(results.landSecurityAgainstNaturalDisastersName)
        sendidFasilitasiListrik = results.landAvailableElectricalGridId
        spinnerfasilitasListrik.setText(results.landAvailableElectricalGridName)
        sendidFasilitasiAirBersih = results.landAvailableCleanWaterSystemId
        spinnerfasilitasSumberAir.setText(results.landAvailableCleanWaterSystemName)
        sendidFasilitasiTelepon = results.landAvailableTelephoneNetworkId
        spinnerfasilitasJaringanTelepon.setText(results.landAvailableTelephoneNetworkName)
        sendidFasilitasiGas = results.landAvailableGasPipelineNetworkId
        spinnerfasilitasGas.setText(results.landAvailableGasPipelineNetworkName)
        sendidKeberadaaanSutet = results.landSUTETPresenceId
        spinnerKebaradaanSUTET.setText(results.landSUTETPresenceName)
        edjarakSutet.setText(results.projectLandSUTETDistance.toString())
        sendidKeberadaaanMakam = results.landCemeteryPresenceId
        spinnerKebaradaanMakam.setText(results.landCemeteryPresenceName)
        edjarakMakam.setText(results.landCemeteryDistance.toString())
        sendidKeberadaaanTusukSate = results.landSkewerPresenceId
        spinnerTusukSate.setText(results.landSkewerPresenceName)
    }

    private fun getPresenter(): FormObjekTanahPresenter4? {
        formObjekTanahPresenter4 = FormObjekTanahPresenter4()
        formObjekTanahPresenter4.onAttach(this)
        return formObjekTanahPresenter4
    }

    private fun validate(): Boolean {

        val valid: Boolean
        if (sendiIdKemudahanDijangkau?.equals(0)!! || sendiIdKemudahanPerbelanjaan?.equals(0)!! ||
                sendiIdKemudahanPendidikan?.equals(0)!! || sendiIdKemudahanDiWisata?.equals(0)!! ||
                sendiIdKemudahanTransportasi?.equals(0)!! || sendiIdKeamananKejahatan?.equals(0)!! ||
                sendiIdKeamananKebakaran?.equals(0)!! || sendiIdKeamananBencanaAlam?.equals(0)!! ||
                sendidFasilitasiListrik?.equals(0)!! || sendidFasilitasiAirBersih?.equals(0)!! ||
                sendidFasilitasiTelepon?.equals(0)!! || sendidFasilitasiGas?.equals(0)!! ||
                sendidKeberadaaanSutet?.equals(0)!! || sendidKeberadaaanMakam?.equals(0)!! ||
                sendidKeberadaaanTusukSate?.equals(0)!! || sendIdInformasiTambahanTanah?.equals(0)!!) {

            val sweetAlretLoading = SweetAlertDialog(activity!!, SweetAlertDialog.WARNING_TYPE)
            sweetAlretLoading.titleText = "LENGKAPI DATA"
            sweetAlretLoading.setCancelable(false)
            sweetAlretLoading.confirmText = "OK"
            sweetAlretLoading.setConfirmClickListener { sDialog ->
                sDialog?.let { if (it.isShowing) it.dismiss() }
            }
            sweetAlretLoading.show()

            valid = false

        } else {
            valid = true
        }

        return valid
    }

    override fun onsuccessgetinfromasiTambahan(responseDataListInformasiTambahanTanah: MutableList<ResponseDataListInformasiTambahanTanah>) {
        /*val adapterInformasiTamabahanTanah = ArrayAdapter<ResponseDataListInformasiTambahanTanah>(activity!!, R.layout.item_spinner, responseDataListInformasiTambahanTanah)
        spinnerinformasiTambahan.setAdapter(adapterInformasiTamabahanTanah)*/

        /*adapterInformasiTambahan = AdapterInformasiTambahan(responseDataListInformasiTambahanTanah, liststepjenisInformasiTambahan, activity!!, this@FormObjekTanah4)
        val linearLayoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.VERTICAL, false)

        rvinformasiTambahan.adapter = adapterInformasiTambahan
        rvinformasiTambahan.layoutManager = linearLayoutManager*/
    }

    override fun onResume() {
        super.onResume()
        btnprevObjekTanah4?.setOnClickListener(this)
        btnnetxtObjekTanah4?.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        btnprevObjekTanah4?.setOnClickListener(null)
        btnnetxtObjekTanah4?.setOnClickListener(null)
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btnnetxtObjekTanah4) {
            hideKeyboard()
            dataFormTanah4.clear()
            Utils.deletedataformtanah4(activity!!)
            senvalueSutet = if (edjarakSutet.text.toString() != "") {
                java.lang.Float.parseFloat(edjarakSutet.text.toString())
            } else {
                0.00000F
            }

            senvalueJarakMakam = if (edjarakMakam.text.toString() != "") {
                java.lang.Float.parseFloat(edjarakMakam.text.toString())
            } else {
                0.00000F
            }

            dataFormTanah4.add(DataFormTanah4(
                    sendiIdKemudahanDijangkau,
                    spinnerKemudahanDicapai.text.toString().trim(),
                    sendiIdKemudahanPerbelanjaan,
                    spinnerKemudahanPerbelanjaan.text.toString().trim(),
                    sendiIdKemudahanPendidikan,
                    spinnerKemudahanPendidikan.text.toString().trim(),
                    sendiIdKemudahanDiWisata,
                    spinnerKemudahanWisata.text.toString().trim(),
                    sendiIdKemudahanTransportasi,
                    spinnerKemudahanTransportasi.text.toString().trim(),
                    sendiIdKeamananKejahatan,
                    spinnerkeamananKejahatan.text.toString().trim(),
                    sendiIdKeamananKebakaran,
                    spinnerkeamananKebakaran.text.toString().trim(),
                    sendiIdKeamananBencanaAlam,
                    spinnerkeamananBencanaAlam.text.toString().trim(),
                    sendidFasilitasiListrik,
                    spinnerfasilitasListrik.text.toString().trim(),
                    sendidFasilitasiAirBersih,
                    spinnerfasilitasSumberAir.text.toString().trim(),
                    sendidFasilitasiTelepon,
                    spinnerfasilitasJaringanTelepon.text.toString().trim(),
                    sendidFasilitasiGas,
                    spinnerfasilitasGas.text.toString().trim(),
                    sendidKeberadaaanSutet,
                    spinnerKebaradaanSUTET.text.toString().trim(),
                    senvalueSutet,
                    sendidKeberadaaanMakam,
                    spinnerKebaradaanMakam.text.toString().trim(),
                    senvalueJarakMakam,
                    sendidKeberadaaanTusukSate,
                    spinnerTusukSate.text.toString().trim()
            ))

            val jsonBBM = Gson().toJson(dataFormTanah4)
            Utils.savedataformtanah4(jsonBBM, activity!!)

            Handler().postDelayed({
                /*val turnsType = object : TypeToken<MutableList<DataFormTanah4>>() {}.type
                val dataconvert = Gson().fromJson<MutableList<DataFormTanah4>>(Utils.getdataformtanah4(activity!!), turnsType)

                Log.d("babab4", dataconvert.toString())*/
                if (mListener != null) {
                    mListener!!.onNextPressed(this)
                }

            }, 1000)


        } else if (view.id == R.id.btnprevObjekTanah4) {
            if (mListener != null)
                mListener!!.onBackPressed(this)
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnStepFourListener) {
            mListener = context
        } else {
            throw RuntimeException(("$context must implement OnFragmentInteractionListener"))
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
        getPresenter()?.onDetach()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        getPresenter()?.onDetach()
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    interface OnStepFourListener {
        fun onBackPressed(fragment: androidx.fragment.app.Fragment)
        fun onNextPressed(fragment: androidx.fragment.app.Fragment)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): FormObjekTanah4 {
            val fragment = FormObjekTanah4()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor