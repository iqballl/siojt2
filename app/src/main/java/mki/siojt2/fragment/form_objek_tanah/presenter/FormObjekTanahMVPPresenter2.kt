package mki.siojt2.fragment.form_objek_tanah.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface FormObjekTanahMVPPresenter2 : MVPPresenter {

    fun getdaftarposisitanahTerhadapJalan()
    fun getdaftarBentukTanah()
    fun getdaftarTipografiTanah()


}