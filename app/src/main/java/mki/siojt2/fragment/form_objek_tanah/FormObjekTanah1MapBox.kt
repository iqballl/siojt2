package mki.siojt2.fragment.form_objek_tanah

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.*
import android.os.Bundle
import android.os.Looper.getMainLooper
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import cn.pedant.SweetAlert.SweetAlertDialog
import androidx.annotation.DrawableRes
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mapbox.android.core.location.*
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import de.hdodenhof.circleimageview.CircleImageView
import io.realm.Realm
import io.realm.Sort
import kotlinx.android.synthetic.main.fragment_form_objek_add_tanah_1_mapbox.*
import kotlinx.android.synthetic.main.fragment_form_objek_add_tanah_1_mapbox.view.*
import mki.siojt2.R
import mki.siojt2.adapter.autocomplate_adapter.*
import mki.siojt2.base.BaseFragment
import mki.siojt2.fragment.form_objek_tanah.presenter.FormObjekTanahPresenter1
import mki.siojt2.fragment.form_objek_tanah.view.FormObjekTanahView1
import mki.siojt2.model.data_form_tanah.DataFormTanah1
import mki.siojt2.model.data_form_tanah.DataImageCanvas
import mki.siojt2.model.data_form_tanah.DataKoordinatMapBox
import mki.siojt2.model.localsave.PartyAdd
import mki.siojt2.model.localsave.ProjectLand
import mki.siojt2.model.localsave.Session
import mki.siojt2.model.region.ResponseDataCity
import mki.siojt2.model.region.ResponseDataKecamatan
import mki.siojt2.model.region.ResponseDataKelurahan
import mki.siojt2.model.region.ResponseDataProvince
import mki.siojt2.model.region.*
import mki.siojt2.persistence.Preference
import mki.siojt2.ui.acitivity_map_project.MapsProjectActivity
import mki.siojt2.ui.acitivity_maps_drawing.MapsActivityDrawingMapBox
import mki.siojt2.ui.activity_form_add_objek_tanah.view.ActicityFormAddObjekTanah
import mki.siojt2.ui.paint_activity.PaintActivity
import mki.siojt2.utils.MapsProjectIntentSetting
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.Utils
import mki.siojt2.utils.extension.MapBoxUtils
import mki.siojt2.utils.extension.OnSingleClickListener
import java.io.ByteArrayInputStream
import java.lang.Exception
import java.lang.ref.WeakReference

class FormObjekTanah1MapBox : BaseFragment(),
        FormObjekTanahView1,
        PermissionsListener,
        View.OnClickListener{


    private val DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L
    private val DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5
    private var mapboxMap: MapboxMap? = null
    private var permissionsManager: PermissionsManager? = null
    private var locationEngine: LocationEngine? = null
    private val callback: FormObjekTanah1MapBoxCallback = FormObjekTanah1MapBoxCallback(this)

    private var fusedLatitude = 0.0
    private var fusedLongitude = 0.0

    // TODO: Rename and change types of parameters
    private var isSimpleMode: Boolean = false
    private var mParam2: String? = null
    private var mParam3: String? = null
    private var mListener: OnStepOneListener? = null

    var selectedSubject: Int? = null

    private var idProvince: Array<String?> = emptyArray()
    private var idCity: Array<String?> = emptyArray()
    private var idKecamatan: Array<String?> = emptyArray()
    private var idKelurahan: Array<String?> = emptyArray()

    private var sendIdProvince: String = ""
    private var sendIdCity: String = ""
    private var sendIdKecamatan: String = ""
    private var sendIdKelurahan: String = ""

    private lateinit var formObjekTanahPresenter1: FormObjekTanahPresenter1

    private var datafromTanah1: MutableList<DataFormTanah1> = ArrayList()

    lateinit var viewx: View
    lateinit var session: Session
    lateinit var realm: Realm

    lateinit var filterableRealmAdapterKota: FilterableRealmAdapterKota<ResponseDataCityWithoutParameter>
    lateinit var filterableRealmSubject: FilterableRealmAdapterSubject<PartyAdd>
    lateinit var filterableRealmAdapterKecamatan: FilterableRealmAdapterKecamatan<ResponseDataKecamatanWithoutParameter>
    lateinit var filterableRealmAdapterKelurahan: FilterableRealmAdapterKelurahan<ResponseDataKelurahanWithoutParameter>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            isSimpleMode = arguments!!.getBoolean(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
            mParam3 = arguments!!.getString(ARG_PARAM3)
        }

        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        session = Session(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        Mapbox.getInstance(requireContext(), getString(R.string.mapbox_access_token))
        // Inflate the layout for this fragment
        viewx = inflater.inflate(R.layout.fragment_form_objek_add_tanah_1_mapbox, container, false)

        return viewx
    }

    override fun setUp(view: View) {
        edBlok.filters = arrayOf<InputFilter>(InputFilter.AllCaps())
        ednomorTanah.filters = arrayOf<InputFilter>(InputFilter.AllCaps())
        ednomorTanah.setText(mParam2)

        swToggle.isChecked = isSimpleMode

        if(isSimpleMode){
            tvMode?.text = "Simple Mode"
            llrtRw?.visibility = View.GONE
        }
        else {
            tvMode?.text = "Detail Mode"
        }

        swToggle.setOnCheckedChangeListener { buttonView, isChecked ->
            (activity as ActicityFormAddObjekTanah?)?.setMode(isChecked)
        }

        /*init filterable*/
        filterableRealmAdapterKota = FilterableRealmAdapterKota(activity!!, autocomplateKotaKabupaten)
        filterableRealmAdapterKecamatan = FilterableRealmAdapterKecamatan(activity!!, autocomplateTVKecamatan)
        filterableRealmAdapterKelurahan = FilterableRealmAdapterKelurahan(activity!!, autocomplateTVKelurahan)

        autocomplateProvince.setOnClickListener {
            autocomplateProvince.showDropDown()
        }

        autocomplateKotaKabupaten.setOnClickListener {
            autocomplateKotaKabupaten.showDropDown()
        }

        autocomplateTVKecamatan.setOnClickListener {
            autocomplateTVKecamatan.showDropDown()
        }

        autocomplateTVKelurahan.setOnClickListener {
            autocomplateTVKelurahan.showDropDown()
        }

        /* Subject */

        val resultsOwner = realm.where(PartyAdd::class.java)
            .equalTo("ProjectId", mParam3?.toInt())
            .equalTo("areaId", mParam2?.toInt())
            .sort("TempId", Sort.DESCENDING)
            .findAll()

        val subjects = arrayListOf<String>()
        for(i in resultsOwner){
            subjects.add(i.partyFullName)
        }

        val adapterSubject = ArrayAdapter(activity!!, R.layout.item_spinner, subjects)
        spinnerSubject.setAdapter(adapterSubject)
        spinnerSubject.onItemClickListener = AdapterView.OnItemClickListener { adapterView, _, position, _ ->
            val dataSpinner = resultsOwner[position]
            selectedSubject = dataSpinner?.tempId
        }

        /* Provinsi */
        val adapertPropinsi = FilterableRealmAdapterPropinsi(activity!!,
                realm.where(ResponseDataProvinceWithoutParameter::class.java).findAll(),
                autocomplateProvince)
        autocomplateProvince.setAdapter(adapertPropinsi)
        autocomplateProvince.showDropDown()
        autocomplateProvince.threshold = 1
        autocomplateProvince.setText("")

        autocomplateProvince.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                //loadProvince(autocomplateProvince.text.toString().trim())
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                /*filterableRealmAdapterKota.setFilterRealm(null)
                sendIdProvince = ""
                sendIdCity = ""
                autocomplateKotaKabupaten.setText("")
                autocomplateKotaKabupaten.setAdapter(filterableRealmAdapterKota)
                filterableRealmAdapterKota.notifyDataSetChanged()
                if (autocomplateProvince.text.toString().trim().isEmpty()) {
                    sendIdProvince = ""
                    llkota.visibility = View.GONE
                } else {
                    llkota.visibility = View.VISIBLE
                }*/
                sendIdCity = ""
                sendIdKelurahan = ""
                sendIdKecamatan = ""
                autocomplateKotaKabupaten.setText("")
                autocomplateTVKecamatan.setText("")
                autocomplateTVKelurahan.setText("")
            }

            override fun afterTextChanged(editable: Editable) {
                //loadProvince(autocomplateProvince.text.toString().trim())
            }
        })

        autocomplateProvince.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            hidekeyboard2()
            val dataSpinner = parent.getItemAtPosition(position) as ResponseDataProvinceWithoutParameter
            sendIdProvince = dataSpinner.getProvinceCode().toString()
            autocomplateKotaKabupaten.setText("")
            sendIdCity = ""

            /**/
            autocomplateKotaKabupaten.setText("")
            val resultRealmKotaByProvince = realm.where(ResponseDataCityWithoutParameter::class.java)
                    .equalTo("regencyProvinceCode", sendIdProvince)
                    .findAll()
            filterableRealmAdapterKota.setFilterRealm(resultRealmKotaByProvince)
            autocomplateKotaKabupaten.setAdapter(filterableRealmAdapterKota)

            /*sendIdProvince = idProvince[position].toString()
            autocomplateKotaKabupaten.setText("")
            sendIdCity = ""*/
            //Toast.makeText(activity!!, sendIdProvince, Toast.LENGTH_SHORT).show()
        }

        /* Kota */
        autocomplateKotaKabupaten.showDropDown()
        autocomplateKotaKabupaten.threshold = 1
        autocomplateKotaKabupaten.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                //loadCity(autocomplateKotaKabupaten.text.toString().trim())
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                /*filterableRealmAdapterKecamatan.setRealmFilter(null)
                sendIdCity = ""
                sendIdKecamatan = ""
                autocomplateTVKecamatan.setText("")
                autocomplateTVKecamatan.setAdapter(filterableRealmAdapterKecamatan)
                filterableRealmAdapterKecamatan.notifyDataSetChanged()
                if (autocomplateKotaKabupaten.text.toString().trim().isEmpty()) {
                    llkecamatan.visibility = View.GONE
                } else if (sendIdProvince.isEmpty() || sendIdProvince == "") {
                    Toast.makeText(activity!!, "Harap pilih propinsi", Toast.LENGTH_SHORT).show()
                } else {
                    llkecamatan.visibility = View.VISIBLE
                }*/
                sendIdKelurahan = ""
                sendIdKecamatan = ""
                autocomplateTVKecamatan.setText("")
                autocomplateTVKelurahan.setText("")
            }

            override fun afterTextChanged(editable: Editable) {
                /*checkData()
                loadCity(autocomplateKotaKabupaten.text.toString().trim())*/
            }
        })

        autocomplateKotaKabupaten.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            hidekeyboard2()
            val dataSpinner = parent.getItemAtPosition(position) as ResponseDataCityWithoutParameter
            sendIdCity = dataSpinner.getRegencyCode().toString()
            sendIdKecamatan = ""

            autocomplateTVKecamatan.setText("")
            val resultRealmKecamatanByKota = realm.where(ResponseDataKecamatanWithoutParameter::class.java)
                    .equalTo("districtRegencyCode", sendIdCity)
                    .findAll()
            filterableRealmAdapterKecamatan.setRealmFilter(resultRealmKecamatanByKota)
            autocomplateTVKecamatan.setAdapter(filterableRealmAdapterKecamatan)
            /*sendIdCity = idCity[position].toString()
            autocomplateTVKecamatan.setText("")
            sendIdKecamatan = ""*/
            //Toast.makeText(activity!!, sendIdCity, Toast.LENGTH_SHORT).show()
        }

        /* Kecematan */
        autocomplateTVKecamatan.threshold = 1
        autocomplateTVKecamatan.showDropDown()
        autocomplateTVKecamatan.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                //loadKecamtan(autocomplateTVKecamatan.text.toString().trim())
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                /*filterableRealmAdapterKelurahan.setRealmFilter(null)
                sendIdKecamatan = ""
                sendIdKelurahan = ""
                autocomplateTVKelurahan.setText("")
                autocomplateTVKelurahan.setAdapter(filterableRealmAdapterKelurahan)
                filterableRealmAdapterKelurahan.notifyDataSetChanged()
                if (autocomplateTVKecamatan.text.toString().trim().isEmpty()) {
                    llkelurahan2.visibility = View.GONE
                } else {
                    llkelurahan2.visibility = View.VISIBLE
                }*/
                sendIdKelurahan = ""
                autocomplateTVKelurahan.setText("")
            }

            override fun afterTextChanged(editable: Editable) {
                /*checkData()
                loadKecamtan(autocomplateTVKecamatan.text.toString().trim())*/
            }
        })

        autocomplateTVKecamatan.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            hidekeyboard2()
            val dataSpinner = parent.getItemAtPosition(position) as ResponseDataKecamatanWithoutParameter
            sendIdKecamatan = dataSpinner.getdistrictCode().toString()
            sendIdKelurahan = ""

            /**/
            autocomplateTVKelurahan.setText("")
            val resultRealmKelurahanByKecamatan = realm.where(ResponseDataKelurahanWithoutParameter::class.java)
                    .equalTo("villageDistrictCode", sendIdKecamatan)
                    .findAll()

            filterableRealmAdapterKelurahan.setRealmFilter(resultRealmKelurahanByKecamatan)
            autocomplateTVKelurahan.setAdapter(filterableRealmAdapterKelurahan)

            /*sendIdKecamatan = idKecamatan[position].toString()
            autocomplateTVKelurahan.setText("")
            sendIdKelurahan = ""
            Toast.makeText(activity!!, sendIdKecamatan, Toast.LENGTH_SHORT).show()*/
        }

        /* Kelurahan */
        autocomplateTVKelurahan.showDropDown()
        autocomplateTVKelurahan.threshold = 1
        autocomplateTVKelurahan.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                //loadKelurahan(autocomplateTVKelurahan.text.toString().trim())
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                // autocomplateTVKotaKabupaten.setText("")
            }

            override fun afterTextChanged(editable: Editable) {
                //loadKelurahan(autocomplateTVKelurahan.text.toString().trim())
            }
        })

        autocomplateTVKelurahan.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            hidekeyboard2()
            val dataSpinner = parent.getItemAtPosition(position) as ResponseDataKelurahanWithoutParameter
            sendIdKelurahan = dataSpinner.getvillageCode().toString()
            //Toast.makeText(activity!!, sendIdKelurahan, Toast.LENGTH_SHORT).show()
        }

        SetupDataForEdit()

    }

    private fun SetupDataForEdit() {
        session = Session(context)
        Preference.saveDataKoordinatMapBox(DataKoordinatMapBox(arrayListOf()))
        if (session.id != "" && session.status == "2") {
            val results = realm.where(ProjectLand::class.java).equalTo("LandIdTemp", session.id.toInt()).findFirst()
            if (results != null) {
                tampilkanData(results)
                Log.d("results", results.toString())
            } else {
                Log.e("data", "kosong")
            }
        }
    }

    private fun tampilkanData(results: ProjectLand) {
        Log.d("hasil", results.landProvinceCode)
        Log.d("hasil", results.landRegencyCode)
        Log.d("hasil", results.landDistrictCode)
        Log.d("hasil", results.landVillageId)

        (activity as ActicityFormAddObjekTanah).selectedLinkedObjectUAVId = results.selectedLinkedObjectUAVId

        resetAdapter(results.landProvinceCode,
                results.landRegencyCode,
                results.landDistrictCode,
                results.landDistrictName,
                results.subjectId)

        ednomorTanah.setText(results.landNumberList)
        sendIdProvince = results.landProvinceCode
        autocomplateProvince.setText(results.landProvinceName)
        sendIdCity = results.landRegencyCode
        autocomplateKotaKabupaten.setText(results.landRegencyName)
        sendIdKecamatan = results.landDistrictCode
        autocomplateTVKecamatan.setText(results.landDistrictName)
        sendIdKelurahan = results.landVillageId
        autocomplateTVKelurahan.setText(results.landVillageName)
        selectedSubject = results.subjectId
        spinnerSubject.setText(results.subjectName)
        edBlok.setText(results.landBlok)
        edNomor.setText(results.landNumber.toString())
        edKomplek.setText(results.landComplekName)
        edJalan.setText(results.landStreet)
        edRt.setText(results.landRT)
        edRW.setText(results.landRW)
        edkodePos.setText(results.landPostalCode)
        fusedLatitude = results.landLatitude.toDouble()
        fusedLongitude = results.landLangitude.toDouble()

        val listPolygonPoint: MutableList<LatLng> = arrayListOf()
        for(i in results.landPolygonPoint){
            listPolygonPoint.add(LatLng(i.lat!!, i.lon!!))
        }

        Preference.saveDataKoordinatMapBox(DataKoordinatMapBox(listPolygonPoint))
    }

    fun resetAdapter(a: String, b: String, c: String, d: String, e: Int) {
        sendIdProvince = a
        sendIdCity = b
        sendIdKecamatan = c
        sendIdKelurahan = d
        selectedSubject = e

        filterableRealmAdapterKota.setFilterRealm(null)
        filterableRealmAdapterKecamatan.setRealmFilter(null)
        filterableRealmAdapterKelurahan.setRealmFilter(null)

        val resultRealmKotaByProvince = realm.where(ResponseDataCityWithoutParameter::class.java)
                .equalTo("regencyProvinceCode", sendIdProvince)
                .findAll()
        filterableRealmAdapterKota.setFilterRealm(resultRealmKotaByProvince)
        autocomplateKotaKabupaten.setAdapter(filterableRealmAdapterKota)

        val resultRealmKecamatanByKota = realm.where(ResponseDataKecamatanWithoutParameter::class.java)
                .equalTo("districtRegencyCode", sendIdCity)
                .findAll()
        filterableRealmAdapterKecamatan.setRealmFilter(resultRealmKecamatanByKota)
        autocomplateTVKecamatan.setAdapter(filterableRealmAdapterKecamatan)

        val resultRealmKelurahanByKecamatan = realm.where(ResponseDataKelurahanWithoutParameter::class.java)
                .equalTo("villageDistrictCode", sendIdKecamatan)
                .findAll()

        filterableRealmAdapterKelurahan.setRealmFilter(resultRealmKelurahanByKecamatan)
        autocomplateTVKelurahan.setAdapter(filterableRealmAdapterKelurahan)
    }

    @SuppressLint("SetTextI18n")
    override fun onResume() {
        super.onResume()
        btnnetxtObjekTanah1?.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        btnnetxtObjekTanah1?.setOnClickListener(null)
        /*try {
            mMapView!!.onPause()
        } catch (e: Exception) {
            e.printStackTrace()
        }*/
    }

    /**
     * Initialize the Maps SDK's LocationComponent
     */
    @SuppressLint("MissingPermission")
    private fun enableLocationComponent(loadedMapStyle: Style) {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(requireContext())) {

            // Get an instance of the component
            val locationComponent = mapboxMap!!.locationComponent

            // Set the LocationComponent activation options
            val locationComponentActivationOptions = LocationComponentActivationOptions.builder(requireContext(), loadedMapStyle)
                    .useDefaultLocationEngine(false)
                    .build()

            // Activate with the LocationComponentActivationOptions object
            locationComponent.activateLocationComponent(locationComponentActivationOptions)


            locationComponent.isLocationComponentEnabled = true

            // Set the component's camera mode
            locationComponent.cameraMode = CameraMode.TRACKING

            // Set the component's render mode
            locationComponent.renderMode = RenderMode.COMPASS
            initLocationEngine()
        } else {
            permissionsManager = PermissionsManager(this)
            permissionsManager?.requestLocationPermissions(requireActivity())
        }
    }

    /**
     * Set up the LocationEngine and the parameters for querying the device's location
     */
    @SuppressLint("MissingPermission")
    private fun initLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(requireContext())
        val request = LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build()
        locationEngine?.requestLocationUpdates(request, callback, getMainLooper())
        locationEngine?.getLastLocation(object : LocationEngineCallback<LocationEngineResult>{
            override fun onSuccess(result: LocationEngineResult?) {
                if(result?.lastLocation?.latitude == null || result?.lastLocation?.longitude == null){
                    initLocationEngine()
                }
                else{
                    val cameraPosition = CameraPosition.Builder()
                            .target(LatLng(
                                    result.lastLocation?.latitude!!,
                                    result.lastLocation?.longitude!!))
                            .zoom(17.0).build()
                    mapboxMap?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                }
            }

            override fun onFailure(exception: Exception) {

            }
        })
        locationEngine?.getLastLocation(callback)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        permissionsManager?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onExplanationNeeded(permissionsToExplain: List<String?>?) {
        Toast.makeText(requireContext(), "Harap aktifkan lokasi",
                Toast.LENGTH_LONG).show()
    }

    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            mapboxMap!!.getStyle { style -> enableLocationComponent(style) }
        } else {
            Toast.makeText(requireContext(), "Permission tidak diberi akses", Toast.LENGTH_LONG).show()
        }
    }

    private class FormObjekTanah1MapBoxCallback internal constructor(fragment: FormObjekTanah1MapBox) : LocationEngineCallback<LocationEngineResult> {
        private val activityWeakReference: WeakReference<FormObjekTanah1MapBox> = WeakReference<FormObjekTanah1MapBox>(fragment)

        /**
         * The LocationEngineCallback interface's method which fires when the device's location has changed.
         *
         * @param result the LocationEngineResult object which has the last known location within it.
         */
        override fun onSuccess(result: LocationEngineResult) {
            val fragment: FormObjekTanah1MapBox? = activityWeakReference.get()
            if (fragment != null) {
                val location = result.lastLocation ?: return

                fragment.fusedLatitude = location.latitude
                fragment.fusedLongitude = location.longitude
                // Create a Toast which displays the new location's coordinates
//                Toast.makeText(fragment.requireContext(), "Lokasi : ${result.lastLocation!!.latitude} | ${result.lastLocation!!.longitude}",
//                        Toast.LENGTH_SHORT).show()

                // Pass the new location to the Maps SDK's LocationComponent
                if (fragment.mapboxMap != null && result.lastLocation != null) {
                    fragment.mapboxMap?.locationComponent?.forceLocationUpdate(result.lastLocation)
                }
            }
        }

        /**
         * The LocationEngineCallback interface's method which fires when the device's location can't be captured
         *
         * @param exception the exception message
         */
        override fun onFailure(exception: java.lang.Exception) {
            val fragment: FormObjekTanah1MapBox? = activityWeakReference.get()
            if (fragment != null) {
                Toast.makeText(fragment.requireContext(), exception.localizedMessage,
                        Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        // Prevent leaks
        if (locationEngine != null) {
            locationEngine!!.removeLocationUpdates(callback)
        }
    }

    fun setFusedLatitude(lat: Double) {
        fusedLatitude = lat
    }

    fun setFusedLongitude(lon: Double) {
        fusedLongitude = lon
    }

    fun getFusedLatitude(): Double {
        return fusedLatitude
    }

    fun getFusedLongitude(): Double {
        return fusedLongitude
    }

    private fun getPresenter(): FormObjekTanahPresenter1? {
        formObjekTanahPresenter1 = FormObjekTanahPresenter1()
        formObjekTanahPresenter1.onAttach(this)
        return formObjekTanahPresenter1
    }

    private fun getMarkerBitmapFromView(@DrawableRes resId: Int): Bitmap {
        //var inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val customMarkerView = (activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.view_custom_marker, null)
        val markerImageView = customMarkerView.findViewById(R.id.profile_image) as CircleImageView
        markerImageView.setImageResource(resId)
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        customMarkerView.layout(0, 0, customMarkerView.measuredWidth, customMarkerView.measuredHeight)
        customMarkerView.buildDrawingCache()
        val returnedBitmap = Bitmap.createBitmap(customMarkerView.measuredWidth, customMarkerView.measuredHeight,
                Bitmap.Config.ARGB_8888)
        val canvas = Canvas(returnedBitmap)
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
        val drawable = customMarkerView.background
        drawable?.draw(canvas)
        customMarkerView.draw(canvas)
        return returnedBitmap
    }

    private fun loadProvince(searchCode: String) {
        getPresenter()?.getregionProvince(searchCode)
    }

    private fun loadCity(searchCode: String) {
        getPresenter()?.getregionCity(sendIdProvince, searchCode, "8")
    }

    private fun loadKecamtan(searchCode: String) {
        getPresenter()?.getregionKecamatan(searchCode, sendIdCity, "8")
    }

    private fun loadKelurahan(searchCode: String) {
        getPresenter()?.getregionKelurahan(searchCode, sendIdKecamatan, "8")
    }

    private fun hidekeyboard2() {
        val inputManager = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val v = activity!!.currentFocus
        if (v != null) {
            activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            inputManager.hideSoftInputFromWindow(v.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)

        }
    }


    private var textWatcherValidasiRegion: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            /*if (autocomplateTVProvince.text.toString().trim().isEmpty()) {
                sendIdProvince = ""
                llkota.visibility = View.GONE
            } else {
                llkota.visibility = View.VISIBLE
            }*/
        }

        override fun afterTextChanged(editable: Editable) {
            checkData()
        }
    }

    private fun checkData() {
        if (sendIdProvince == "" || sendIdProvince.isEmpty()) {
            sendIdProvince = ""
            llkota.visibility = View.GONE
        } else {
            llkota.visibility = View.VISIBLE
        }
    }

    private fun validate(): Boolean {

        val valid: Boolean
        val sweetAlretLoading = SweetAlertDialog(activity!!, SweetAlertDialog.WARNING_TYPE)
        sweetAlretLoading.setCancelable(false)
        sweetAlretLoading.confirmText = "OK"
        sweetAlretLoading.setConfirmClickListener { sDialog ->
            sDialog?.let { if (it.isShowing) it.dismiss() }
        }
        when {
            ednomorTanah.text!!.isEmpty() -> {
                sweetAlretLoading.titleText = "Lengkapi data nomor tanah"
                sweetAlretLoading.show()
                ednomorTanah.requestFocus()
                valid = false
            }
            sendIdProvince.isEmpty() -> {
                sweetAlretLoading.titleText = "Lengkapi data provinsi"
                sweetAlretLoading.show()
                autocomplateProvince.requestFocus()
                valid = false
            }
            sendIdCity.isEmpty() -> {
                sweetAlretLoading.titleText = "Lengkapi data kota/kabupaten"
                sweetAlretLoading.show()
                autocomplateKotaKabupaten.requestFocus()
                valid = false
            }
            sendIdKecamatan.isEmpty() -> {
                sweetAlretLoading.titleText = "Lengkapi data kecamatan"
                sweetAlretLoading.show()
                autocomplateTVKecamatan.requestFocus()
                valid = false
            }
            sendIdKelurahan.isEmpty() -> {
                sweetAlretLoading.titleText = "Lengkapi data kelurahan"
                sweetAlretLoading.show()
                autocomplateTVKelurahan.requestFocus()
                valid = false
            }
            selectedSubject == null -> {
                sweetAlretLoading.titleText = "Lengkapi data nama pemilik"
                sweetAlretLoading.show()
                valid = false
            }
            else -> valid = true
        }

        return valid
    }

    override fun onsuccessgetregionProvince(responseDataProvince: MutableList<ResponseDataProvince>) {
        val listSpinner: ArrayList<String> = ArrayList()
        listSpinner.clear()

        idProvince = arrayOfNulls(responseDataProvince.size)
        for (i in 0 until responseDataProvince.size) {
            idProvince[i] = responseDataProvince[i].provinceCode.toString()
            listSpinner.add(responseDataProvince[i].provinceName.toString())
        }

        val adapterProvinsi = ArrayAdapter<String>(activity!!, R.layout.item_spinner, listSpinner)
        adapterProvinsi.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1)
        autocomplateProvince.threshold = 1 //will start working from first character
        autocomplateProvince.setAdapter(adapterProvinsi)
    }

    override fun onsuccessgetregionCity(responseDataCity: MutableList<ResponseDataCity>) {
        val listSpinner: ArrayList<String> = ArrayList()
        listSpinner.clear()

        idCity = arrayOfNulls(responseDataCity.size)
        for (i in 0 until responseDataCity.size) {
            idCity[i] = responseDataCity[i].regencyCode.toString()
            listSpinner.add(responseDataCity[i].regencyName.toString())
        }

        val adapterCity = ArrayAdapter<String>(activity!!, R.layout.item_spinner, listSpinner)
        adapterCity.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1)
        autocomplateKotaKabupaten.threshold = 1 //will start working from first character
        autocomplateKotaKabupaten.setAdapter(adapterCity)
    }

    override fun onsuccessgetregionKecamatan(responseDataKecamatan: MutableList<ResponseDataKecamatan>) {
        val listSpinner: ArrayList<String> = ArrayList()
        listSpinner.clear()

        idKecamatan = arrayOfNulls(responseDataKecamatan.size)
        for (i in 0 until responseDataKecamatan.size) {
            idKecamatan[i] = responseDataKecamatan[i].districtCode.toString()
            listSpinner.add(responseDataKecamatan[i].districtName.toString())
        }

        val adapterKecamatan = ArrayAdapter<String>(activity!!, R.layout.item_spinner, listSpinner)
        adapterKecamatan.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1)
        autocomplateTVKecamatan.threshold = 1 //will start working from first character
        autocomplateTVKecamatan.setAdapter(adapterKecamatan)
    }

    override fun onsuccessgetregionKelurahan(responseDataKelurahan: MutableList<ResponseDataKelurahan>) {
        val listSpinner: ArrayList<String> = ArrayList()
        listSpinner.clear()

        idKelurahan = arrayOfNulls(responseDataKelurahan.size)
        for (i in 0 until responseDataKelurahan.size) {
            idKelurahan[i] = responseDataKelurahan[i].villageCode.toString()
            listSpinner.add(responseDataKelurahan[i].villageName.toString())
        }

        val adapterKelurahan = ArrayAdapter<String>(activity!!, R.layout.item_spinner, listSpinner)
        adapterKelurahan.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1)
        autocomplateTVKelurahan.threshold = 1 //will start working from first character
        autocomplateTVKelurahan.setAdapter(adapterKelurahan)
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btnnetxtObjekTanah1) {
            datafromTanah1.clear()
            Utils.deletedataformtanah1(activity!!)
            hidekeyboard2()

            if (validate()) {
                val data = DataFormTanah1(
                        ednomorTanah.text.toString(),
                        sendIdProvince,
                        autocomplateProvince.text.toString().trim(),
                        sendIdCity,
                        autocomplateKotaKabupaten.text.toString().trim(),
                        sendIdKecamatan,
                        autocomplateTVKecamatan.text.toString().trim(),
                        sendIdKelurahan,
                        autocomplateTVKelurahan.text.toString().trim(),
                        edBlok.text.toString().trim(),
                        edNomor.text.toString(),
                        edKomplek.text.toString().trim(),
                        edJalan.text.toString().trim(),
                        edRt.text.toString().trim(),
                        edRW.text.toString(),
                        edkodePos.text.toString().trim(),
                        getFusedLatitude().toString(),
                        getFusedLongitude().toString(),
                        subjectId = selectedSubject,
                        subjectName = spinnerSubject.text.toString().trim())
                data.polygonPoint = Preference.dataKoordinatMapBox()?.coordinat

                datafromTanah1.add(data)
                val jsonBBM = Gson().toJson(datafromTanah1)
                Utils.savedataformtanah1(jsonBBM, activity!!)
                if (mListener != null) {
                    mListener!!.onNextPressed(this)
                }

                /*Handler().postDelayed({
                    //val turnsType = object : TypeToken<MutableList<DataFormTanah1>>() {}.type
                    //val dataconvert = Gson().fromJson<MutableList<DataFormTanah1>>(Utils.getdataformtanah1(activity!!), turnsType)

                    //Log.d("babab1", getFusedLatitude().toString())
                    //Log.d("babab2", getFusedLongitude().toString())


                }, 500)*/

            }

            /*if (mListener != null) {
                mListener!!.onNextPressed(this)
            }*/
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        when (context) {
            is OnStepOneListener -> mListener = context
            else -> throw RuntimeException(("$context must implement OnFragmentInteractionListener"))
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
        getPresenter()?.onDetach()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        getPresenter()?.onDetach()
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    interface OnStepOneListener {
        //void onFragmentInteraction(Uri uri);
        fun onNextPressed(fragment: androidx.fragment.app.Fragment)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        private val ARG_PARAM3 = "param3"
        internal const val TAG = "FormObjekTanah1"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: Boolean, param2: String, param3: String): FormObjekTanah1MapBox {
            val fragment = FormObjekTanah1MapBox()
            val args = Bundle()
            args.putBoolean(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            args.putString(ARG_PARAM3, param3)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor