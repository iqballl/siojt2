package mki.siojt2.fragment.form_objek_tanah.presenter

import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.fragment.form_objek_tanah.view.FormObjekTanahView4
import mki.siojt2.utils.rxJava.RxUtils

class FormObjekTanahPresenter4 : BasePresenter(), FormObjekTanahMVPPresenter4 {

    private var disposables: CompositeDisposable = CompositeDisposable()

    override fun getinfromasiTambahan() {
        disposables.add(
                dataManager.getdaftarinfoTambahanTanah()
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetinfromasiTambahan(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }



    private fun view(): FormObjekTanahView4 {
        return getView() as FormObjekTanahView4
    }
}