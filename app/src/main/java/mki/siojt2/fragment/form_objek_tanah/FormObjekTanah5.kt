package mki.siojt2.fragment.form_objek_tanah

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.DatePicker
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_form_objek_add_tanah_5.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.fragment.form_objek_tanah.adapter_recycleview.AdapterAddUserSHM
import mki.siojt2.fragment.form_objek_tanah.adapter_recycleview.AdapterInformasiTambahan
import mki.siojt2.fragment.form_objek_tanah.adapter_recycleview.AdapterPenggunaanTanah
import mki.siojt2.fragment.form_objek_tanah.presenter.FormObjekTanahPresenter3
import mki.siojt2.fragment.form_objek_tanah.view.FormObjekTanahView3
import mki.siojt2.model.data_form_tanah.DataFormTanah3
import mki.siojt2.model.data_form_tanah.DataPenggunaanTanah
import mki.siojt2.model.localsave.ProjectLand
import mki.siojt2.model.localsave.Session
import mki.siojt2.model.master_data_form_tanah.*
import mki.siojt2.ui.activity_form_add_objek_tanah.view.ActicityFormAddObjekTanah
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.Utils
import java.util.*
import kotlin.collections.ArrayList


class FormObjekTanah5 : BaseFragment(), FormObjekTanahView3, View.OnClickListener {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    private var mListener: OnStepFiveListener? = null

    private lateinit var formObjekTanahPresenter3: FormObjekTanahPresenter3

    private var liststepjenisInformasiTambahan: ArrayList<String>? = null
    private var liststepjenisInformasiTambahanName: ArrayList<String>? = null
    private var sendidInformasiTambahanMultiple: ArrayList<String> = ArrayList()
    private var sendNameInformasiTambahanMultiple: ArrayList<String> = ArrayList()

    lateinit var adapterInformasiTambahan: AdapterInformasiTambahan
    lateinit var realm: Realm
    lateinit var session: Session


    private var dialogValidate: SweetAlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }

        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        session = Session(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_objek_add_tanah_5, container, false)
    }

    override fun setUp(view: View) {

        if (liststepjenisInformasiTambahan == null || liststepjenisInformasiTambahan!!.size == 0 || liststepjenisInformasiTambahan!!.isEmpty()) {
            liststepjenisInformasiTambahan = java.util.ArrayList()
            liststepjenisInformasiTambahanName = java.util.ArrayList()
        }

        dialogValidate = SweetAlertDialog(activity!!, SweetAlertDialog.WARNING_TYPE)
        dialogValidate!!.setCancelable(false)
        dialogValidate!!.confirmText = "OK"
        dialogValidate!!.setConfirmClickListener { sDialog ->
            sDialog?.let { if (it.isShowing) it.dismiss() }
        }

        /* Data offline informasi tamabahan tanah */
        val resultRealmInformasiTambahanTanah = realm.where(ResponseDataListInformasiTambahanTanahWithOutParam::class.java).findAllAsync()
        val dataInformasiTambahanTanah: MutableList<ResponseDataListInformasiTambahanTanahWithOutParam> = realm.copyFromRealm(resultRealmInformasiTambahanTanah)

        adapterInformasiTambahan = AdapterInformasiTambahan(dataInformasiTambahanTanah, liststepjenisInformasiTambahan, liststepjenisInformasiTambahanName, activity!!, this@FormObjekTanah5)
        val llManager = androidx.recyclerview.widget.LinearLayoutManager(activity!!, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        rvinformasiTambahan.layoutManager = llManager
        rvinformasiTambahan.adapter = adapterInformasiTambahan

        GetDataForEdit()
    }

    private fun GetDataForEdit() {
        if (session.id != "" && session.status == "2") {
            val results = realm.where(ProjectLand::class.java).equalTo("LandIdTemp", session.id.toInt()).findFirst()
            if (results != null) {
                tampilkanData(results)
                //Log.d("results", results.toString())
            } else {
                //Log.e("data", "kosong")
            }
        } else {
            liststepjenisInformasiTambahan!!.add("")
            liststepjenisInformasiTambahanName!!.add("")
        }
    }

    private fun tampilkanData(results: ProjectLand) {
        //jenis tanah
        val ltid1 = results.landAddInfoAddInfoId
        val ltid2 = ltid1.replace("[", "['")
        val ltid3 = ltid2.replace("]", "']")
        val ltid = ltid3.replace(",", "','")
        val listltId = Gson().fromJson(ltid, Array<String>::class.java).toList()

        val ltname1 = results.landAddInfoAddInfoName
        val ltname2 = ltname1.replace("[", "['")
        val ltname3 = ltname2.replace("]", "']")
        val ltname = ltname3.replace(",", "','")
        val listltName = Gson().fromJson(ltname, Array<String>::class.java).toList()

        liststepjenisInformasiTambahan?.clear()
        liststepjenisInformasiTambahanName?.clear()
        for (i in listltId.indices ){
            liststepjenisInformasiTambahan!!.add(listltId[i])
            liststepjenisInformasiTambahanName!!.add(listltName[i])
        }
    }

    private fun validate(): Boolean {
        var validData = false
        val sweetAlretLoading = SweetAlertDialog(activity!!, SweetAlertDialog.WARNING_TYPE)
        sweetAlretLoading.setCancelable(false)
        sweetAlretLoading.confirmText = "OK"
        sweetAlretLoading.setConfirmClickListener { sDialog ->
            sDialog?.let { if (it.isShowing) it.dismiss() }
        }
        sweetAlretLoading.titleText = "Lengkapi Data"

        if(sendidInformasiTambahanMultiple.isNotEmpty()){
            validData = true
        }
        else {
            sweetAlretLoading.show()
        }

        return validData
    }

    override fun onsuccessgetdaftarKepemilikanTanah(responseDataListKepemilikanAtasTanah: MutableList<ResponseDataListKepemilikanAtasTanah>) {

    }

    override fun onsuccessgetdaftarPenggunaanTanah(responseDataListPenggunaanTanah: MutableList<ResponseDataListPenggunaanTanah>) {
    }

    override fun onsuccessgetdaftarPeruntukanTanah(responseDataListPeruntukanTanah: MutableList<ResponseDataListPeruntukanTanah>) {
    }

    override fun onResume() {
        super.onResume()
        btnprevObjekTanah3?.setOnClickListener(this)
        btnnetxtObjekTanah3?.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        btnprevObjekTanah3?.setOnClickListener(null)
        btnnetxtObjekTanah3?.setOnClickListener(null)
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btnnetxtObjekTanah3) {
            hideKeyboard()

            val getidmultipleinformasiTambahan = adapterInformasiTambahan.getStepList()
            val getNamemultipleinformasiTambahan = adapterInformasiTambahan.getStepList1()

            getidmultipleinformasiTambahan.forEach {
                if (it.isNotEmpty() || it != "") {
                    sendidInformasiTambahanMultiple.add(it)
                }
            }

            getNamemultipleinformasiTambahan.forEach {
                if (it.isNotEmpty() || it != ""){
                    sendNameInformasiTambahanMultiple.add(it)
                }
            }


            val turnsType3 = object : TypeToken<MutableList<DataFormTanah3>>() {}.type
            val dataconvert3 = Gson().fromJson<MutableList<DataFormTanah3>>(Utils.getdataformtanah3(context!!), turnsType3)

            if (validate()) {
                dataconvert3[0].projectLandAddInfoAddInfoId = sendidInformasiTambahanMultiple
                dataconvert3[0].projectLandAddInfoAddInfoName = sendNameInformasiTambahanMultiple

                val jsonBBM = Gson().toJson(dataconvert3)
                Utils.savedataformtanah3(jsonBBM, activity!!)

                if (mListener != null) {
                    mListener!!.onNextPressed(this)
                }
            }
        } else if (view.id == R.id.btnprevObjekTanah3) {
            if (mListener != null)
                mListener!!.onBackPressed(this)
        }
    }

    fun addItem() {
        liststepjenisInformasiTambahan!!.add("")
        liststepjenisInformasiTambahanName!!.add("")
        rvinformasiTambahan.scrollToPosition(adapterInformasiTambahan.itemCount - 1)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnStepFiveListener) {
            mListener = context
        } else {
            throw RuntimeException(("$context must implement OnFragmentInteractionListener"))
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    interface OnStepFiveListener {
        fun onBackPressed(fragment: androidx.fragment.app.Fragment)
        fun onNextPressed(fragment: androidx.fragment.app.Fragment)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): FormObjekTanah5 {
            val fragment = FormObjekTanah5()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor