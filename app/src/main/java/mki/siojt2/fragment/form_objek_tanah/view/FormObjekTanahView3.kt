package mki.siojt2.fragment.form_objek_tanah.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.master_data_form_tanah.ResponseDataListKepemilikanAtasTanah
import mki.siojt2.model.master_data_form_tanah.ResponseDataListPenggunaanTanah
import mki.siojt2.model.master_data_form_tanah.ResponseDataListPeruntukanTanah

/**
 * Created by iqbal on 09/09/19
 */

interface FormObjekTanahView3 : MvpView {
    fun onsuccessgetdaftarKepemilikanTanah(responseDataListKepemilikanAtasTanah: MutableList<ResponseDataListKepemilikanAtasTanah>)
    fun onsuccessgetdaftarPenggunaanTanah(responseDataListPenggunaanTanah: MutableList<ResponseDataListPenggunaanTanah>)
    fun onsuccessgetdaftarPeruntukanTanah(responseDataListPeruntukanTanah: MutableList<ResponseDataListPeruntukanTanah>)
}
