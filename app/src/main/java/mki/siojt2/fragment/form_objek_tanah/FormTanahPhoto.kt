package mki.siojt2.fragment.form_objek_tanah

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import cn.pedant.SweetAlert.SweetAlertDialog
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_form_take_photo.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.fragment.form_objek_tanah.adapter_recycleview.AdapterTakePictureLand
import mki.siojt2.model.data_form_tanah.DataFormTanahImage
import mki.siojt2.model.local_save_image.DataImageLand
import mki.siojt2.model.localsave.ProjectLand
import mki.siojt2.model.localsave.Session
import mki.siojt2.model.master_data_form_tanah.ResponseDataListExistOrNo
import mki.siojt2.model.master_data_form_tanah.ResponseDataListYesNo
import mki.siojt2.ui.activity_image_picker.ImagePickerActivity
import mki.siojt2.utils.Utils
import mki.siojt2.utils.realm.RealmController
import java.io.ByteArrayOutputStream
import java.io.IOException

class FormTanahPhoto : BaseFragment(), View.OnClickListener, AdapterTakePictureLand.OnItemImageClickListener {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    private var mListener: OnStepFiveListener? = null

    lateinit var realm: Realm
    lateinit var session: Session

    private var mpositionImageClick: Int? = null
    lateinit var adapterTakePictureLand: AdapterTakePictureLand

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //set realm & set session for edit or delete flag
        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        session = Session(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_take_photo, container, false)
    }

    override fun setUp(view: View) {
        // Clearing older images from cache directory
        // don't call this line if you want to choose multiple images in the same activity
        // call this once the bitmap(s) usage is over
        ImagePickerActivity.clearCache(activity!!)

        /* set adapter */
        val valueexistorNO = mutableListOf(
            ResponseDataListExistOrNo(1, "Ada"),
            ResponseDataListExistOrNo(2, "Tidak Ada")
        )
        val adapteryesNo = ArrayAdapter(activity!!, R.layout.item_spinner, valueexistorNO)
        spinnerOtherObject.setAdapter(adapteryesNo)
        spinnerOtherObject.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
        }

        adapterTakePictureLand = AdapterTakePictureLand(activity!!, this)
        adapterTakePictureLand.setListnerItemImageClick(this)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity!!, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)

        rvlistPhoto.adapter = adapterTakePictureLand
        rvlistPhoto.layoutManager = linearLayoutManager

        checkisEdit()

    }

    private fun checkisEdit() {
        if (session.id != "" && session.status == "2") {
            val resultsdataLand = realm.where(ProjectLand::class.java).equalTo("LandIdTemp", session.id.toInt()).findFirst()
            val dataimageLand = realm.copyFromRealm(resultsdataLand!!.dataImageLands)

            if (dataimageLand.isNotEmpty()) {
                for (element in 0 until dataimageLand.size) {
                    adapterTakePictureLand.addItems(dataimageLand[element])
                    adapterTakePictureLand.notifyDataSetChanged()
                }
            } else {
                val dataImage = DataImageLand()
                dataImage.imageId = null
                dataImage.imagepathLand = ""
                dataImage.imagenameLand = ""
                adapterTakePictureLand.addItems(dataImage)
                adapterTakePictureLand.notifyDataSetChanged()
            }

            edNotes.setText(resultsdataLand.note)
            spinnerOtherObject.setText(resultsdataLand.hasOtherObject)

        } else {
            val dataImage = DataImageLand()
            dataImage.imageId = null
            dataImage.imagepathLand = ""
            dataImage.imagenameLand = ""
            adapterTakePictureLand.addItems(dataImage)
            adapterTakePictureLand.notifyDataSetChanged()
        }
    }

    private fun loadProfile(url: String) {
        /* Log.d(TAG, "Image cache path: $url")
         Glide.with(activity!!).load(url)
                 .into(ivselect)
         ivselect.setColorFilter(ContextCompat.getColor(activity!!, android.R.color.transparent))*/
    }

    private fun onProfileImageClick() {
        Dexter.withActivity(activity!!)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions()
                        }
                        if (report.isAnyPermissionPermanentlyDenied) {
                            showSettingsDialog()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).check()
    }

    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(activity!!, object : ImagePickerActivity.PickerOptionListener {
            override fun onTakeCameraSelected() {
                launchCameraIntent()
            }

            override fun onChooseGallerySelected() {
                launchGalleryIntent()
            }
        })
    }

    private fun launchCameraIntent() {
        val intent = Intent(activity!!, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE)
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 400)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 400)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    private fun launchGalleryIntent() {
        val intent = Intent(activity!!, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE)
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data!!.getParcelableExtra<Uri>("path")
                try {
                    // You can update this bitmap to your server
                    val bitmap = MediaStore.Images.Media.getBitmap(activity!!.contentResolver, uri)

                    val dataImage = DataImageLand()
                    dataImage.imageId = null
                    dataImage.imagepathLand = uri.path
                    dataImage.imagenameLand = ""
                    adapterTakePictureLand.updateDateAfterClickImage(mpositionImageClick!!, dataImage)
                    adapterTakePictureLand.notifyDataSetChanged()

                    //adapterTakePictureLand.getencodeImage(uri)
                    /*val dataImage = DataImagePhoto()
                    dataImage.imageEncode = getEncoded64ImageStringFromBitmap(bitmap)
                    dataImage.imageName = ""
                    adapterTakePictureLand.addItems(dataImage)
                    adapterTakePictureLand.notifyDataSetChanged()*/

                    // loading profile image from local cache
                    //loadProfile(uri.toString())
                    //Log.d(TAG, "Image cache path: $uri")
                    //Log.d(TAG, "Image cache path: ${uri.path}")
                    //Log.d(TAG, "uri real path: ${getrealpathfromURI(uri)}")
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun getrealpathfromURI(uri: Uri): String? {
        val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
        // Get the cursor
        val cursor = activity!!.contentResolver.query(uri,
                filePathColumn, null, null, null)

        if (cursor != null) {
            val columnIndex = cursor.getColumnIndexOrThrow(filePathColumn[0])
            // Move to first row
            cursor.moveToFirst()
            return cursor.getString(columnIndex)
        }

        return null
    }

    fun getEncoded64ImageStringFromBitmap(bitmap: Bitmap): String? {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream)
        val byteFormat = stream.toByteArray()

        return Base64.encodeToString(byteFormat, Base64.NO_WRAP)

        /*var baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        var b = baos.toByteArray()
        var temp: String? = null
        try
        {
            System.gc()
            temp = Base64.encodeToString(b, Base64.DEFAULT)
        }
        catch (e:Exception) {
            e.printStackTrace()
        }
        catch (e:OutOfMemoryError) {
            baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos)
            b = baos.toByteArray()
            temp = Base64.encodeToString(b, Base64.DEFAULT)
            Log.e("EWN", "Out of memory error catched")
        }
        return temp*/
    }

    fun resizeBase64Image(base64image: String): String {
        val encodeByte = Base64.decode(base64image.toByteArray(), Base64.DEFAULT)
        val options = BitmapFactory.Options()
        options.inPurgeable = true
        var image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size, options)
        if (image.height <= 400 && image.width <= 400) {
            return base64image
        }
        image = Bitmap.createScaledBitmap(image, image.width, image.height, false)
        val baos = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.PNG, 100, baos)
        val b = baos.toByteArray()
        System.gc()
        return Base64.encodeToString(b, Base64.NO_WRAP)
    }

    fun addItem() {
        val dataImage = DataImageLand()
        dataImage.imageId = null
        dataImage.imagepathLand = ""
        dataImage.imagenameLand = ""
        adapterTakePictureLand.addItems(dataImage)
        adapterTakePictureLand.notifyDataSetChanged()
        rvlistPhoto.scrollToPosition(adapterTakePictureLand.itemCount - 1)
    }


    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(getString(R.string.dialog_permission_title))
        builder.setMessage(getString(R.string.dialog_permission_message))
        builder.setPositiveButton(getString(R.string.go_to_settings)) { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(getString(android.R.string.cancel)) { dialog, which -> dialog.cancel() }
        builder.show()
    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", activity!!.packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    override fun onClick(v: View?) {
        if (v!!.id == R.id.btnaddPhoto) {
            hideKeyboard()
            Utils.deletedataformImage(activity!!)
            val dataPhoto: List<DataImageLand> = adapterTakePictureLand.returnDataPhoto()
            val mimagePath: MutableList<String> = ArrayList()
            val mimageTitle: MutableList<String> = ArrayList()

            var isvalidPhoto = false
            for (i in dataPhoto.indices) {
                if (dataPhoto[i].imagepathLand!!.toString().isNotEmpty() && dataPhoto[i].imagenameLand!!.toString().isEmpty() ||
                        dataPhoto[i].imagepathLand!!.toString().isEmpty() && dataPhoto[i].imagenameLand!!.toString().isNotEmpty()) {
                    val sweetAlretLoading = SweetAlertDialog(activity!!, SweetAlertDialog.WARNING_TYPE)
                    sweetAlretLoading.titleText = "LENGKAPI DATA"
                    sweetAlretLoading.setCancelable(false)
                    sweetAlretLoading.confirmText = "OK"
                    sweetAlretLoading.setConfirmClickListener { sDialog ->
                        sDialog?.let { if (it.isShowing) it.dismiss() }
                    }
                    sweetAlretLoading.show()
                    isvalidPhoto = false
                    break
                } else {
                    isvalidPhoto = true
                    //mimagePath.add(dataPhoto[i].imageEncode.toString())
                    //mimageTitle.add(dataPhoto[i].imageName.toString())
                }
            }

            val dataformTanahImage = DataFormTanahImage()
            dataformTanahImage.notes = edNotes.text.toString()
            dataformTanahImage.hasOtherObject = spinnerOtherObject.text.toString()

            if (isvalidPhoto) {
                dataformTanahImage.dataImagePhoto = dataPhoto

                //val turnsType = object : TypeToken<DataFormTanahImage>() {}.type
                //val dataconvert = Gson().fromJson<DataFormTanahImage>(Utils.getdataformTanahImage(activity!!), turnsType)

                //Log.d("babab4", dataconvert.dataImagePhoto.toString())
                if (mListener != null) {
                    mListener!!.onNextPressed(this)
                }
            }

            val jsonBBM = Gson().toJson(dataformTanahImage)
            Utils.savedataformtanahImage(jsonBBM, activity!!)

        }
    }

    override fun OnItemImageClick(position: Int) {
        mpositionImageClick = position
        onProfileImageClick()
    }

    override fun onResume() {
        super.onResume()
        btnaddPhoto?.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        btnaddPhoto?.setOnClickListener(null)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnStepFiveListener) {
            mListener = context
        } else {
            throw RuntimeException(("$context must implement OnFragmentInteractionListener"))
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    interface OnStepFiveListener {
        fun onBackPressed(fragment: androidx.fragment.app.Fragment)
        fun onNextPressed(fragment: androidx.fragment.app.Fragment)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        private val REQUEST_IMAGE = 100
        internal const val TAG = "FormTanahPhoto"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): FormTanahPhoto {
            val fragment = FormTanahPhoto()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}