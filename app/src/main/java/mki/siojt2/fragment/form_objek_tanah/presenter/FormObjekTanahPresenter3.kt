package mki.siojt2.fragment.form_objek_tanah.presenter

import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.fragment.form_objek_tanah.view.FormObjekTanahView3
import mki.siojt2.utils.rxJava.RxUtils

class FormObjekTanahPresenter3 : BasePresenter(), FormObjekTanahMVPPresenter3 {

    private var disposables: CompositeDisposable = CompositeDisposable()

    override fun getdaftarKepemilikanTanah() {
        disposables.add(
                dataManager.getdaftarKepemilikanTanah()
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdaftarKepemilikanTanah(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    override fun getdaftarPenggunaanTanah() {
        disposables.add(
                dataManager.getdaftarPenggunaanTanah()
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdaftarPenggunaanTanah(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    override fun getdaftarPeruntukanTanah() {
        disposables.add(
                dataManager.getdaftarPeruntukanTanah()
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdaftarPeruntukanTanah(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }


    private fun view(): FormObjekTanahView3 {
        return getView() as FormObjekTanahView3
    }
}