package mki.siojt2.fragment.form_objek_tanah.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface FormObjekTanahMVPPresenter3 : MVPPresenter {

    fun getdaftarKepemilikanTanah()
    fun getdaftarPenggunaanTanah()
    fun getdaftarPeruntukanTanah()

}