package mki.siojt2.fragment.form_objek_tanah.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface FormObjekTanahMVPPresenter1 : MVPPresenter {

    fun getregionProvince(searchCode: String)
    fun getregionCity(provinceId: String, searchCode: String, limit: String)
    fun getregionKecamatan(searchCode: String, regencyCode: String, limit: String)
    fun getregionKelurahan(searchCode: String, districtCode: String, limit: String)

}