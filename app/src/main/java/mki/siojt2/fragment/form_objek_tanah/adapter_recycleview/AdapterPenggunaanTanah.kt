package mki.siojt2.fragment.form_objek_tanah.adapter_recycleview

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import mki.siojt2.R
import android.widget.AdapterView
import android.widget.LinearLayout
import cn.pedant.SweetAlert.SweetAlertDialog
import kotlinx.android.synthetic.main.item_add_jenis_tanah.view.*
import mki.siojt2.fragment.form_objek_tanah.FormObjekTanah3
import mki.siojt2.model.data_form_tanah.DataPenggunaanTanah
import kotlin.collections.ArrayList
import mki.siojt2.model.master_data_form_tanah.ResponseDataListPenggunaanTanah
import mki.siojt2.model.master_data_form_tanah.ResponseDataListPenggunaanTanahWithOutParam


class AdapterPenggunaanTanah internal constructor(data: MutableList<ResponseDataListPenggunaanTanahWithOutParam>, context: Context, var mfragment: Fragment) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var mcontext: Context = context
    private var mData: MutableList<ResponseDataListPenggunaanTanahWithOutParam>? = data
    private var mDataSet: MutableList<DataPenggunaanTanah>? = ArrayList()

    private val TYPE_HEADER = 0
    private val TYPE_FOOTER = 1
    private val TYPE_ITEM: Int = 0

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var viewpenggunaantanahLain: LinearLayout = itemView.findViewById(R.id.lljenistanahOther)
        var spinnerjenisjenisTanah: com.weiwangcn.betterspinner.library.BetterSpinner = itemView.findViewById(R.id.spinnerJenisTanah)
        internal var ednamapenggunaaantanahLain: com.google.android.material.textfield.TextInputEditText = itemView.edjenistanahLain

        init {
            spinnerjenisjenisTanah.onItemClickListener = AdapterView.OnItemClickListener { adapterView, _, position, _ ->
                val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListPenggunaanTanahWithOutParam
                val valueSpinner = adapterView.getItemAtPosition(position).toString()

                mDataSet!![adapterPosition].projectLandUtilizationId = selectedProject.getutilizationId().toString()
                mDataSet!![adapterPosition].projectLandUtilizationName = selectedProject.getutilizationName().toString()

                when {
                    mDataSet!![adapterPosition].projectLandUtilizationName == "Lain-Nya" -> {
                        viewpenggunaantanahLain.visibility = View.VISIBLE
                        mDataSet!![adapterPosition].projectLandUtilizationNameOther = ""
                        ednamapenggunaaantanahLain.text = null
                    }
                    else -> {
                        viewpenggunaantanahLain.visibility = View.GONE
                        mDataSet!![adapterPosition].projectLandUtilizationNameOther = ""
                        ednamapenggunaaantanahLain.text = null
                    }
                }
                //Toast.makeText(mcontext, mStepList.toString(), Toast.LENGTH_SHORT).show()
            }

            ednamapenggunaaantanahLain.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    mDataSet!![adapterPosition].projectLandUtilizationNameOther = s.toString()
                }

                override fun afterTextChanged(s: Editable) {}
            })
        }
    }

    inner class ViewHolderFooter(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var plus: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnPlusMataPencaharian2)
        internal var minus: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnMinusMataPencaharian2)

        init {
            minus.setOnClickListener {
                val position = adapterPosition
                try {
                    mDataSet!!.removeAt(position - 1)
                    notifyItemRemoved(position)
                    notifyDataSetChanged()
                    notifyItemChanged(position)
                    Toast.makeText(mcontext, position.toString(), Toast.LENGTH_SHORT).show()
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                }
            }
            plus.setOnClickListener {
                val position = adapterPosition
                //Toast.makeText(mcontext, "${position}", Toast.LENGTH_SHORT).show()
                when {
                    mDataSet?.get(position - 1)!!.projectLandUtilizationId!!.isEmpty() -> {
                        val sweetAlretLoading = SweetAlertDialog(mcontext, SweetAlertDialog.WARNING_TYPE)
                        sweetAlretLoading.titleText = "LENGKAPI DATA"
                        sweetAlretLoading.setCancelable(false)
                        sweetAlretLoading.confirmText = "OK"
                        sweetAlretLoading.setConfirmClickListener { sDialog ->
                            sDialog?.let { if (it.isShowing) it.dismiss() }
                        }
                        sweetAlretLoading.show()
                    }
                    mDataSet?.get(position - 1)!!.projectLandUtilizationName!! == "Lain-Nya" && mDataSet?.get(position - 1)!!.projectLandUtilizationNameOther!!.isEmpty() -> {
                        val sweetAlretLoading = SweetAlertDialog(mcontext, SweetAlertDialog.WARNING_TYPE)
                        sweetAlretLoading.titleText = "LENGKAPI DATA"
                        sweetAlretLoading.setCancelable(false)
                        sweetAlretLoading.confirmText = "OK"
                        sweetAlretLoading.setConfirmClickListener { sDialog ->
                            sDialog?.let { if (it.isShowing) it.dismiss() }
                        }
                        sweetAlretLoading.show()
                    }
                    else -> {
                        try {
                            /* mStepList!!.add(position + 2, "")
                             mStepList2!!.add(position + 2, "")
                             notifyItemInserted(position + 2)
                             notifyItemRangeInserted(position + 2, mStepList!!.size)
                             notifyDataSetChanged()*/
                            (mfragment as FormObjekTanah3).addItem()
                        } catch (e: ArrayIndexOutOfBoundsException) {
                            e.printStackTrace()
                        }
                    }
                }

            }
        }
    }


    @NonNull
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ITEM -> ViewHolder(
                    LayoutInflater.from(viewGroup.context).inflate(R.layout.item_add_jenis_tanah, viewGroup, false))
            else -> ViewHolderFooter(
                    LayoutInflater.from(viewGroup.context).inflate(R.layout.item_mata_pencaharian_footer, viewGroup, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val x = holder.layoutPosition
        if (holder is ViewHolder) {
            if (mDataSet!![x].projectLandUtilizationId!!.isNotEmpty()) {
                holder.spinnerjenisjenisTanah.setText(mDataSet?.get(x)!!.projectLandUtilizationName.toString())
                if (mDataSet?.get(x)!!.projectLandUtilizationNameOther!!.isNotEmpty()) {
                    holder.viewpenggunaantanahLain.visibility = View.VISIBLE
                    holder.ednamapenggunaaantanahLain.setText(mDataSet?.get(x)!!.projectLandUtilizationNameOther.toString())
                } else {
                    holder.viewpenggunaantanahLain.visibility = View.GONE
                }
                //holder.spinnerjenisjenisTanah.setSelection(mStepList!![x].toInt())
            } else {
                holder.viewpenggunaantanahLain.visibility = View.GONE
                holder.spinnerjenisjenisTanah.setText("")
            }

            val adapterspinnerSumberAir2 = ArrayAdapter<ResponseDataListPenggunaanTanahWithOutParam>(mcontext, R.layout.item_spinner, mData!!)
            holder.spinnerjenisjenisTanah.setAdapter(adapterspinnerSumberAir2)

        } else if (holder is ViewHolderFooter) {
            if (x <= 1) {
                holder.minus.visibility = View.GONE
            } else {
                holder.minus.visibility = View.VISIBLE
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == mDataSet!!.size) TYPE_FOOTER else TYPE_ITEM
    }

    override fun getItemCount(): Int {
        return mDataSet!!.size + 1
    }

    fun addItems(data: DataPenggunaanTanah){
        this.mDataSet!!.add(data)
        notifyDataSetChanged()
    }

    fun returnDataPenggunaanTanah(): List<DataPenggunaanTanah>{
        return mDataSet!!
    }

    /*fun getStepList(): java.util.ArrayList<String> {
        return mStepList!!
    }

    fun getStepList1(): java.util.ArrayList<String> {
        return mStepList1!!
    }*/

}