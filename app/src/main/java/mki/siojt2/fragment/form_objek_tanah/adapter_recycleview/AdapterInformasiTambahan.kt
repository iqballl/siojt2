package mki.siojt2.fragment.form_objek_tanah.adapter_recycleview

import android.content.Context
import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import mki.siojt2.R
import android.widget.AdapterView
import cn.pedant.SweetAlert.SweetAlertDialog
import mki.siojt2.fragment.form_objek_tanah.FormObjekTanah5
import mki.siojt2.fragment.form_objek_tanah.FormObjekTanah4
import kotlin.collections.ArrayList
import mki.siojt2.model.master_data_form_tanah.ResponseDataListInformasiTambahanTanah
import mki.siojt2.model.master_data_form_tanah.ResponseDataListInformasiTambahanTanahWithOutParam


class AdapterInformasiTambahan internal constructor(data: MutableList<ResponseDataListInformasiTambahanTanahWithOutParam>, steps: ArrayList<String>?, steps1: ArrayList<String>?, context: Context, var mfragment: androidx.fragment.app.Fragment) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    private var mcontext: Context = context
    private var mStepList: ArrayList<String>? = steps
    private var mStepList1: ArrayList<String>? = steps1
    private var mData: MutableList<ResponseDataListInformasiTambahanTanahWithOutParam>? = data

    private val TYPE_HEADER = 0
    private val TYPE_FOOTER = 1
    private val TYPE_ITEM: Int = 0

    inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        var spinnerjenisjenisTanah: com.weiwangcn.betterspinner.library.BetterSpinner = itemView.findViewById(R.id.spinnerJenisTanah)

        init {
            spinnerjenisjenisTanah.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
                val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListInformasiTambahanTanahWithOutParam
                val valueSpinner = adapterView.getItemAtPosition(position).toString()

                mStepList!![adapterPosition] = selectedProject.getaddInfoId().toString()
                mStepList1!![adapterPosition] = selectedProject.getaddInfoName().toString()
                //Toast.makeText(mcontext, mStepList.toString(), Toast.LENGTH_SHORT).show()
            }
        }
    }

    inner class ViewHolderFooter(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        internal var plus: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnPlusMataPencaharian2)
        internal var minus: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnMinusMataPencaharian2)

        init {
            minus.setOnClickListener {
                val position = adapterPosition
                try {
                    mStepList!!.removeAt(position - 1)
                    mStepList1!!.removeAt(position-1)
                    notifyItemRemoved(position)
                    notifyDataSetChanged()
                    notifyItemChanged(position)
                    Toast.makeText(mcontext, position.toString(), Toast.LENGTH_SHORT).show()
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                }
            }
            plus.setOnClickListener {
                val position = adapterPosition
                //Toast.makeText(mcontext, "${position}", Toast.LENGTH_SHORT).show()
                if (mStepList?.get(position - 1)!!.isEmpty()) {
                    val sweetAlretLoading = SweetAlertDialog(mcontext, SweetAlertDialog.WARNING_TYPE)
                    sweetAlretLoading.titleText = "LENGKAPI DATA"
                    sweetAlretLoading.setCancelable(false)
                    sweetAlretLoading.confirmText = "OK"
                    sweetAlretLoading.setConfirmClickListener { sDialog ->
                        sDialog?.let { if (it.isShowing) it.dismiss() }
                    }
                    sweetAlretLoading.show()
                } else {
                    //Toast.makeText(mcontext, "${position + 1}", Toast.LENGTH_SHORT).show()

                    try {
                        /* mStepList!!.add(position + 2, "")
                         mStepList2!!.add(position + 2, "")
                         notifyItemInserted(position + 2)
                         notifyItemRangeInserted(position + 2, mStepList!!.size)
                         notifyDataSetChanged()*/
                        (mfragment as FormObjekTanah5).addItem()
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        e.printStackTrace()
                    }
                }

            }
        }
    }


    @NonNull
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ITEM -> ViewHolder(
                    LayoutInflater.from(viewGroup.context).inflate(R.layout.item_add_jenis_tanah, viewGroup, false))
            else -> ViewHolderFooter(
                    LayoutInflater.from(viewGroup.context).inflate(R.layout.item_mata_pencaharian_footer, viewGroup, false))
        }
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        val x = holder.layoutPosition
        if (holder is ViewHolder) {

            if (mStepList!![x].isNotEmpty()) {
                holder.spinnerjenisjenisTanah.setText(mStepList1?.get(x).toString())
            } else {
                holder.spinnerjenisjenisTanah.setText("")
            }

            val adapterspinnerSumberAir2 = ArrayAdapter<ResponseDataListInformasiTambahanTanahWithOutParam>(mcontext, R.layout.item_spinner, mData!!)
            holder.spinnerjenisjenisTanah.setAdapter(adapterspinnerSumberAir2)
        } else if (holder is ViewHolderFooter) {
            if (x <= 1) {
                holder.minus.visibility = View.GONE
            } else {
                holder.minus.visibility = View.VISIBLE
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == mStepList!!.size) TYPE_FOOTER else TYPE_ITEM
    }

    override fun getItemCount(): Int {
        return mStepList!!.size + 1
    }

    fun getStepList(): java.util.ArrayList<String> {
        return mStepList!!
    }

    fun getStepList1(): java.util.ArrayList<String>{
        return mStepList1!!
    }
}