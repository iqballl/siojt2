package mki.siojt2.fragment.form_objek_tanah.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.master_data_form_tanah.ResponseDataListBentukTanah
import mki.siojt2.model.master_data_form_tanah.ResponseDataListPosisiTanah
import mki.siojt2.model.master_data_form_tanah.ResponseDataListTipografi

/**
 * Created by iqbal on 09/09/19
 */

interface FormObjekTanahView2 : MvpView {
    fun onsuccessgetdaftarposisitanahTerhadapJalan(responseDataListPosisiTanah: MutableList<ResponseDataListPosisiTanah>)
    fun onsuccessgetdaftarBentukTanah(responseDataListBentukTanah: MutableList<ResponseDataListBentukTanah>)
    fun onsuccessgetdaftarTipografiTanah(responseDataListTipografi: MutableList<ResponseDataListTipografi>)
}
