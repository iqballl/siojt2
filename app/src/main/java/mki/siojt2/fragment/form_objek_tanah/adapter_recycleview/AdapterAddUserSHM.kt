package mki.siojt2.fragment.form_objek_tanah.adapter_recycleview

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import cn.pedant.SweetAlert.SweetAlertDialog
import mki.siojt2.R
import mki.siojt2.fragment.form_objek_tanah.FormObjekTanah3
import kotlin.collections.ArrayList


class AdapterAddUserSHM internal constructor(context: Context, var mfragment: Fragment) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var mcontext: Context = context
    private var mDataset: ArrayList<String>? = ArrayList()

    private val TYPE_HEADER = 0
    private val TYPE_FOOTER = 1
    private val TYPE_ITEM: Int = 0

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var eduserSHM: com.google.android.material.textfield.TextInputEditText = itemView.findViewById(R.id.edadduserSHM)

        init {
            eduserSHM.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    mDataset!![adapterPosition] = s.toString()
                }

                override fun afterTextChanged(s: Editable) {}
            })
        }
    }

    inner class ViewHolderFooter(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var plus: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnPlusMataPencaharian2)
        internal var minus: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnMinusMataPencaharian2)

        init {
            minus.setOnClickListener {
                val position = adapterPosition
                try {
                    mDataset!!.removeAt(position - 1)
                    notifyItemRemoved(position)
                    notifyDataSetChanged()
                    notifyItemChanged(position)
                    Toast.makeText(mcontext, position.toString(), Toast.LENGTH_SHORT).show()
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                }
            }
            plus.setOnClickListener {
                val position = adapterPosition
                //Toast.makeText(mcontext, "${position}", Toast.LENGTH_SHORT).show()
                if (mDataset?.get(position - 1)!!.isEmpty()) {
                    val sweetAlretLoading = SweetAlertDialog(mcontext, SweetAlertDialog.WARNING_TYPE)
                    sweetAlretLoading.titleText = "LENGKAPI DATA"
                    sweetAlretLoading.setCancelable(false)
                    sweetAlretLoading.confirmText = "OK"
                    sweetAlretLoading.setConfirmClickListener { sDialog ->
                        sDialog?.let { if (it.isShowing) it.dismiss() }
                    }
                    sweetAlretLoading.show()
                } else {
                    //Toast.makeText(mcontext, "${position + 1}", Toast.LENGTH_SHORT).show()

                    try {
                        /* mStepList!!.add(position + 2, "")
                         mStepList2!!.add(position + 2, "")
                         notifyItemInserted(position + 2)
                         notifyItemRangeInserted(position + 2, mStepList!!.size)
                         notifyDataSetChanged()*/
                        (mfragment as FormObjekTanah3).addItemUserSHM()
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        e.printStackTrace()
                    }
                }

            }
        }
    }


    @NonNull
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ITEM -> ViewHolder(
                    LayoutInflater.from(viewGroup.context).inflate(R.layout.item_add_user_shm, viewGroup, false))
            else -> ViewHolderFooter(
                    LayoutInflater.from(viewGroup.context).inflate(R.layout.item_mata_pencaharian_footer, viewGroup, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val x = holder.layoutPosition
        if (holder is ViewHolder) {

            if (mDataset!![x].isNotEmpty()) {
                holder.eduserSHM.setText(mDataset?.get(x).toString())
            } else {
                holder.eduserSHM.setText("")
                holder.eduserSHM.setText("")
            }

        } else if (holder is ViewHolderFooter) {
            if (x <= 1) {
                holder.minus.visibility = View.GONE
            } else {
                holder.minus.visibility = View.VISIBLE
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == mDataset!!.size) TYPE_FOOTER else TYPE_ITEM
    }

    override fun getItemCount(): Int {
        return mDataset!!.size + 1
    }

    fun clear() {
        this.mDataset!!.clear()
        notifyDataSetChanged()
    }

    fun addItems(data: String) {
        this.mDataset!!.add(data)
        notifyDataSetChanged()
    }

    fun returnDataUserSHM(): java.util.ArrayList<String> {
        return mDataset!!
    }

}