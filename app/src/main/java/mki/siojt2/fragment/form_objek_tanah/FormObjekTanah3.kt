package mki.siojt2.fragment.form_objek_tanah

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.DatePicker
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.gson.Gson
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_form_objek_add_tanah_3.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.fragment.form_objek_tanah.adapter_recycleview.AdapterAddUserSHM
import mki.siojt2.fragment.form_objek_tanah.adapter_recycleview.AdapterInformasiTambahan
import mki.siojt2.fragment.form_objek_tanah.adapter_recycleview.AdapterPenggunaanTanah
import mki.siojt2.fragment.form_objek_tanah.presenter.FormObjekTanahPresenter3
import mki.siojt2.fragment.form_objek_tanah.view.FormObjekTanahView3
import mki.siojt2.model.data_form_tanah.DataFormTanah3
import mki.siojt2.model.data_form_tanah.DataPenggunaanTanah
import mki.siojt2.model.localsave.ProjectLand
import mki.siojt2.model.localsave.Session
import mki.siojt2.model.master_data_form_tanah.*
import mki.siojt2.ui.activity_form_add_objek_tanah.view.ActicityFormAddObjekTanah
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.Utils
import java.util.*
import kotlin.collections.ArrayList


class FormObjekTanah3 : BaseFragment(), FormObjekTanahView3, View.OnClickListener {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    private var mListener: OnStepThreeListener? = null

    lateinit var mCalendar: Calendar
    private var mYear: Int = 0
    private var mMonth: Int = 0
    private var mDay: Int = 0
    val mHour: Int = 0
    val mMinute: Int = 0

    private val dateTemplate = "dd MMMM yyyy"
    private val dateTemplate2 = "yyyy-MM-dd"

    private var mcalendarGlobal: Calendar = Calendar.getInstance()
    private var mYearGlobal: Int = this.mcalendarGlobal.get(Calendar.YEAR)
    private var mMonthGlobal: Int = this.mcalendarGlobal.get(Calendar.MONTH)
    private var mDayOfMpnthGlobal: Int = this.mcalendarGlobal.get(Calendar.DAY_OF_MONTH)

    private var mdateSetListenerGlobal: DatePickerDialog.OnDateSetListener? = null
    private var mdateSetListenerGlobal2: DatePickerDialog.OnDateSetListener? = null


    /*val valuepenggunaanTanah = mutableListOf("Perumahan", "Industri", "Perkantoran", "Pertokoan", "Lain-Nya")

    val valuekesesuaianPeruntukanTanah = mutableListOf("Sesuai RTRW", "Tidak Sesuai RTRW")

    val valuebentukKepemilikanTanah = mutableListOf("SHM", "HGU", "SHRS", "HP", "HPL", "AJB",
            "Tanah Wakaf", "Girik", "Petok", "Rincik", "Ketitir",
            "Verponding", "Hak Sekunder", "Petok", "Rincik", "Ketitir")*/

    private var kategoriKepemelikanTanah: MutableList<String> = arrayListOf()
    private var kategoriPenggunaanTanah: MutableList<String> = arrayListOf()
    private var kategoriPeruntukanTanah: MutableList<String> = arrayListOf()

    private var kategoriIdKepemelikanTanah: MutableList<Int> = arrayListOf()
    private var kategoriIdPenggunaanTanah: MutableList<Int> = arrayListOf()
    private var kategoriIdPeruntukanTanah: MutableList<Int> = arrayListOf()

    private var sendIdKepemilikanTanah: Int? = 0
    private var sendIdPenggunaanTanah: Int? = 0
    private var sendIdPeuntukanTanah: Int? = 0
    private var sendIdAlasHak: String? = ""
    private var senddateTglDiterbitkan: String? = ""
    private var senddateTglBerakhirDiterbitkan: String? = ""

    private var dataFormTanah3: MutableList<DataFormTanah3> = ArrayList()

    private lateinit var formObjekTanahPresenter3: FormObjekTanahPresenter3

    lateinit var adapterPenggunaanTanah: AdapterPenggunaanTanah
    private var liststepjenispenggunaanTanah: ArrayList<String>? = null
    private var liststepJenisPenggunaanTanahName: ArrayList<String>? = null
    private var sendpenggunaanTanahId: ArrayList<String> = ArrayList()
    private var sendpenggunaanTanahName: ArrayList<String> = ArrayList()
    private var sendpenggunaanTanahNameOther: ArrayList<String> = ArrayList()

    private var sendUserOwnerLandSHM: ArrayList<String> = ArrayList()

    lateinit var adapterAddUserSHM: AdapterAddUserSHM
    lateinit var realm: Realm
    lateinit var session: Session

    lateinit var tanggalEdit: String
    lateinit var tanggalEdit2: String

    private var datepicker: DatePicker? = null
    private var datepicker2: DatePicker? = null

    private var datepicker3: DatePicker? = null
    private var datepicker4: DatePicker? = null

    private var dialogValidate: SweetAlertDialog? = null
    private var validpenggunaanTanah: Boolean = false

    private var valueSpinner: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }

        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        session = Session(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_objek_add_tanah_3, container, false)
    }

    override fun setUp(view: View) {

        dialogValidate = SweetAlertDialog(activity!!, SweetAlertDialog.WARNING_TYPE)
        dialogValidate!!.setCancelable(false)
        dialogValidate!!.confirmText = "OK"
        dialogValidate!!.setConfirmClickListener { sDialog ->
            sDialog?.let { if (it.isShowing) it.dismiss() }
        }

        if((activity as ActicityFormAddObjekTanah?)?.isSimpleMode!!){
            lltglditerbitkanTanah?.visibility = View.GONE
            lltglberakhirditerbitkanTanah?.visibility = View.GONE
            llperuntukanTanah?.visibility = View.GONE
            llbatasTanah?.visibility = View.GONE
        }

        /* Data offline informasi tamabahan tanah */
        val resultRealmInformasiTambahanTanah = realm.where(ResponseDataListInformasiTambahanTanahWithOutParam::class.java).findAllAsync()
        val dataInformasiTambahanTanah: MutableList<ResponseDataListInformasiTambahanTanahWithOutParam> = realm.copyFromRealm(resultRealmInformasiTambahanTanah)

        // Get Current Date
        mCalendar = Calendar.getInstance(Locale.getDefault())
        mYear = mCalendar.get(Calendar.YEAR)
        mMonth = mCalendar.get(Calendar.MONTH)
        mDay = mCalendar.get(Calendar.DATE)

        if (liststepjenispenggunaanTanah == null || liststepjenispenggunaanTanah!!.size == 0 || liststepjenispenggunaanTanah!!.isEmpty()) {
            liststepjenispenggunaanTanah = java.util.ArrayList()
            liststepJenisPenggunaanTanahName = java.util.ArrayList()
        }

        /* data offline bentuk penggunaan tanah */
        val resultRealmPenggunaanTanah = realm.where(ResponseDataListPenggunaanTanahWithOutParam::class.java).findAllAsync()
        val dataPenggunaananah: MutableList<ResponseDataListPenggunaanTanahWithOutParam> = realm.copyFromRealm(resultRealmPenggunaanTanah)

        adapterAddUserSHM = AdapterAddUserSHM(activity!!, this)
        adapterPenggunaanTanah = AdapterPenggunaanTanah(dataPenggunaananah, activity!!, this@FormObjekTanah3)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity!!, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        val linearLayoutManager2 = androidx.recyclerview.widget.LinearLayoutManager(activity!!, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)

        rvjenispenggunaanTanah.layoutManager = linearLayoutManager
        rvjenispenggunaanTanah.adapter = adapterPenggunaanTanah

        rvUserSHM.layoutManager = linearLayoutManager2
        rvUserSHM.adapter = adapterAddUserSHM

        GetDataForEdit()

        setformatterNIB(edNIB)
        //setformatterNomorSertifikatTanah(ednomorSertifikat)

        lltglhakTanah.setSafeOnClickListener {
            selectedDate()
        }

        lltglberakhirditerbitkanTanah.setSafeOnClickListener {
            selectedDate2()
        }

        /*lltglberakhirditerbitkanTanah.setOnClickListener(object : OnSingleClickListener() {
            override fun onSingleClick(v: View) {

            }
        })*/

        mdateSetListenerGlobal = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            mcalendarGlobal.set(Calendar.YEAR, year)
            mcalendarGlobal.set(Calendar.MONTH, monthOfYear)
            mcalendarGlobal.set(Calendar.DATE, dayOfMonth)

            tvtglHakTanah.text = DateFormat.format(dateTemplate, mcalendarGlobal.time)
            senddateTglDiterbitkan = DateFormat.format(dateTemplate2, mcalendarGlobal.time).toString()
            //Toast.makeText(activity, senddateBirthPicker, Toast.LENGTH_SHORT).show()
        }


        mdateSetListenerGlobal2 = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            mcalendarGlobal.set(Calendar.YEAR, year)
            mcalendarGlobal.set(Calendar.MONTH, monthOfYear)
            mcalendarGlobal.set(Calendar.DATE, dayOfMonth)

            tvtglberakhirHakTanah.text = DateFormat.format(dateTemplate, mcalendarGlobal.time)
            senddateTglBerakhirDiterbitkan = DateFormat.format(dateTemplate2, mcalendarGlobal.time).toString()
            //Toast.makeText(activity, senddateBirthPicker, Toast.LENGTH_SHORT).show()
        }

        /* data offline bentuk kepemilikan tanah */
        val resultRealmBentukKepemilikanTanah = realm.where(ResponseDataListKepemilikanAtasTanahWithOutParam::class.java).findAllAsync()
        val dataBentukKepemilikanTanah: MutableList<ResponseDataListKepemilikanAtasTanahWithOutParam> = realm.copyFromRealm(resultRealmBentukKepemilikanTanah)

        val adapterjenisKepemilikanTanah = ArrayAdapter<ResponseDataListKepemilikanAtasTanahWithOutParam>(activity!!, R.layout.item_spinner, dataBentukKepemilikanTanah)
        spinnerbentukKepemilikanTanah.setAdapter(adapterjenisKepemilikanTanah)
        spinnerbentukKepemilikanTanah.onItemClickListener = AdapterView.OnItemClickListener { parent, _, position, _ ->
            valueSpinner = parent.getItemAtPosition(position).toString()

            val dataSpinner = parent.getItemAtPosition(position) as ResponseDataListKepemilikanAtasTanahWithOutParam
            sendIdKepemilikanTanah = dataSpinner.getownerShipId()
            if (valueSpinner == "SHM") {
                lladduserSHM.visibility = View.VISIBLE
                adapterAddUserSHM.addItems("")
                adapterAddUserSHM.notifyDataSetChanged()
                //tvtglberakhirHakTanah.setText("")
            } else {
                lladduserSHM.visibility = View.GONE
                sendUserOwnerLandSHM.clear()
                adapterAddUserSHM.clear()
                //tvtglberakhirHakTanah.setText("")
            }

            if(valueSpinner == "HGB" || valueSpinner == "HGU") {
                lltglberakhirditerbitkanTanah.visibility = View.VISIBLE
            } else {
                lltglberakhirditerbitkanTanah.visibility = View.GONE
            }

            if (valueSpinner == "Lain-Nya" || sendIdAlasHak == "Lainnya") {
                llbentukKepemilikanTanahLain.visibility = View.VISIBLE
                llNIB.visibility = View.GONE
                edNIB.text = null
                llnomorSertifikat.visibility = View.GONE
                ednomorSertifikat.text = null
                lltglditerbitkanTanah.visibility = View.GONE
                tvtglHakTanah.text = null
                senddateTglDiterbitkan = ""
            } else {
                llbentukKepemilikanTanahLain.visibility = View.GONE
                edbentukKepemilikanTanahLain.text = null
                llNIB.visibility = View.VISIBLE
                llnomorSertifikat.visibility = View.VISIBLE
                lltglditerbitkanTanah.visibility = View.VISIBLE
            }
            //Toast.makeText(activity!!, sendIdKepemilikanTanah.toString(), Toast.LENGTH_SHORT).show()
        }

        /* data offline alas hak tanah */

        val arr = arrayListOf("Sertipikat", "Girik", "Letter C", "Akta Jual Beli", "Lainnya")
        val adapterAlasHakTanah = ArrayAdapter(activity!!, R.layout.item_spinner, arr)
        spinnerAlasHakTanah.setAdapter(adapterAlasHakTanah)
        spinnerAlasHakTanah.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val dataSpinner = parent.getItemAtPosition(position)
            sendIdAlasHak = dataSpinner as String
            if(dataSpinner == "Lainnya") {
                llNIB.visibility = View.GONE
                llnomorSertifikat.visibility = View.GONE
                lltglditerbitkanTanah.visibility = View.GONE
                lltglberakhirditerbitkanTanah.visibility = View.GONE
            }
            else if(spinnerbentukKepemilikanTanah.text.toString().toLowerCase() == "shm"){
                lltglberakhirditerbitkanTanah.visibility = View.GONE
            }
            else {
                llNIB.visibility = View.VISIBLE
                llnomorSertifikat.visibility = View.VISIBLE
                lltglditerbitkanTanah.visibility = View.VISIBLE
                lltglberakhirditerbitkanTanah.visibility = View.VISIBLE
            }
        }

        /* data offline peruntukan tanah */
        val resultRealmPeruntukanTanah = realm.where(ResponseDataListPeruntukanTanahWithOutParam::class.java).findAllAsync()
        val dataPeruntukanTanah: MutableList<ResponseDataListPeruntukanTanahWithOutParam> = realm.copyFromRealm(resultRealmPeruntukanTanah)

        val adapterjeniskesesuaianPeruntukanTanah = ArrayAdapter<ResponseDataListPeruntukanTanahWithOutParam>(activity!!, R.layout.item_spinner, dataPeruntukanTanah)
        spinnerkesesuaianPeruntukanTanah.setAdapter(adapterjeniskesesuaianPeruntukanTanah)
        spinnerkesesuaianPeruntukanTanah.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val dataSpinner = parent.getItemAtPosition(position) as ResponseDataListPeruntukanTanahWithOutParam
            sendIdPeuntukanTanah = dataSpinner.getzoningSpatialPlanId()
            //Toast.makeText(activity!!, sendIdPeuntukanTanah.toString(), Toast.LENGTH_SHORT).show()
        }

        /*getPresenter()?.getdaftarKepemilikanTanah()
        getPresenter()?.getdaftarPenggunaanTanah()
        getPresenter()?.getdaftarPeruntukanTanah()
        lltglhakTanah.setOnClickListener {
            showdatePicker()
        }

        lltglberakhirhakTanah.setOnClickListener {
            showdatePicker2()
        }

        spinnerbentukKepemilikanTanah.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val valueSpinner = parent.getItemAtPosition(position).toString()
            sendIdKepemilikanTanah = kategoriIdKepemelikanTanah[position]
            if (valueSpinner == "SHM") {
                lltglberakhirditerbitkanTanah.visibility = View.GONE
            } else {
                lltglberakhirditerbitkanTanah.visibility = View.VISIBLE
            }
            //Toast.makeText(activity!!, sendIdKepemilikanTanah.toString(), Toast.LENGTH_SHORT).show()
        }

        spinnerpengunaanTanahSaatIni.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val valueSpinner = parent.getItemAtPosition(position).toString()
            sendIdPenggunaanTanah = kategoriIdPenggunaanTanah[position]
            //Toast.makeText(activity!!, sendIdPenggunaanTanah.toString(), Toast.LENGTH_SHORT).show()
        }

        spinnerkesesuaianPeruntukanTanah.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val valueSpinner = parent.getItemAtPosition(position).toString()
            sendIdPeuntukanTanah = kategoriIdPeruntukanTanah[position]
            //Toast.makeText(activity!!, sendIdPeuntukanTanah.toString(), Toast.LENGTH_SHORT).show()
        }*/

    }

    private fun GetDataForEdit() {
        if (session.id != "" && session.status == "2") {
            val results = realm.where(ProjectLand::class.java).equalTo("LandIdTemp", session.id.toInt()).findFirst()
            if (results != null) {
                tampilkanData(results)
                //Log.d("results", results.toString())
            } else {
                //Log.e("data", "kosong")
            }
        } else {
            val dataPenggunaanTanah = DataPenggunaanTanah()
            dataPenggunaanTanah.projectLandUtilizationId = ""
            dataPenggunaanTanah.projectLandUtilizationName = ""
            dataPenggunaanTanah.projectLandUtilizationNameOther = ""

            adapterPenggunaanTanah.addItems(dataPenggunaanTanah)
            adapterPenggunaanTanah.notifyDataSetChanged()

//            adapterAddUserSHM.addItems("")
//            adapterAddUserSHM.notifyDataSetChanged()
        }
    }

    private fun tampilkanData(results: ProjectLand) {
        sendIdKepemilikanTanah = results.landOwnershipId
        spinnerbentukKepemilikanTanah.setText(results.landOwnershipName)
        spinnerAlasHakTanah.setText(results.landAlasHakTanah)

        if (results.landOwnershipNameOther.isNotEmpty()) {
            llbentukKepemilikanTanahLain.visibility = View.VISIBLE
            edbentukKepemilikanTanahLain.setText(results.landOwnershipNameOther)
        } else {
            llbentukKepemilikanTanahLain.visibility = View.GONE
        }

        senddateTglDiterbitkan = results.landDateOfIssue
        senddateTglBerakhirDiterbitkan = results.landRightsExpirationDate
        tvtglHakTanah.text = results.landDateOfIssue
        tvtglberakhirHakTanah.text = results.landRightsExpirationDate
        valueSpinner = results.landOwnershipName

        /**/
        if (results.landOwnershipName == "SHM") {
            lladduserSHM.visibility = View.VISIBLE

            val landOwnerShipSHM = results.landOwnershipTypeSHM
            val landOwnerShipSHM1 = landOwnerShipSHM.replace("[", "['")
            val landOwnerShipSHM2 = landOwnerShipSHM1.replace("]", "']")
            val landOwnerShipSHM4 = landOwnerShipSHM2.replace(", ", "','")
            val datalandOwnerShip = Gson().fromJson(landOwnerShipSHM4, Array<String>::class.java).toList()

            Log.d("yyyy2", landOwnerShipSHM)
            if (datalandOwnerShip.isNotEmpty()) {
                for (i in datalandOwnerShip.indices) {
                    adapterAddUserSHM.addItems(datalandOwnerShip[i])
                    adapterAddUserSHM.notifyDataSetChanged()
                }
            }
        }

        if (results.landOwnershipName == "HGB" || results.landOwnershipName == "HGU") {
            lltglberakhirditerbitkanTanah.visibility = View.VISIBLE
        }

        if(results.landOwnershipName == "Lain-Nya" || results.landAlasHakTanah == "Lainnya"){
            llbentukKepemilikanTanahLain.visibility = View.VISIBLE
            llNIB.visibility = View.GONE
            edNIB.text = null
            llnomorSertifikat.visibility = View.GONE
            ednomorSertifikat.text = null
            lltglditerbitkanTanah.visibility = View.GONE
            tvtglHakTanah.text = null
            senddateTglDiterbitkan = ""
        }
        else {
            llbentukKepemilikanTanahLain.visibility = View.GONE
            edbentukKepemilikanTanahLain.text = null
            llNIB.visibility = View.VISIBLE
            llnomorSertifikat.visibility = View.VISIBLE
            lltglditerbitkanTanah.visibility = View.VISIBLE
        }

        tanggalEdit = results.landDateOfIssue
        tanggalEdit2 = results.landRightsExpirationDate

        edNIB.setText(results.landNIB)
        edDampak.setText(results.dampak)
        ednomorSertifikat.setText(results.landCertificateNumber)
        //jenis tanah
        val jtid1 = results.landUtilizationLandUtilizationId
        val jtid2 = jtid1.replace("[", "['")
        val jtid3 = jtid2.replace("]", "']")
        val jtid = jtid3.replace(", ", "','")
        val listjtId = Gson().fromJson(jtid, Array<String>::class.java).toList()

        val jtname1 = results.landUtilizationLandUtilizationName
        val jtname2 = jtname1.replace("[", "['")
        val jtname3 = jtname2.replace("]", "']")
        val jtname = jtname3.replace(", ", "','")
        val listjtName = Gson().fromJson(jtname, Array<String>::class.java).toList()

        val landUtilizationNameOther = results.landUtilizationLandUtilizationNameOther
        val landUtilizationNameOther2 = landUtilizationNameOther.replace("[", "['")
        val landUtilizationNameOther3 = landUtilizationNameOther2.replace("]", "']")
        val landUtilizationNameOther4 = landUtilizationNameOther3.replace(", ", "','")
        val landUtilizationNameOtherRef = Gson().fromJson(landUtilizationNameOther4, Array<String>::class.java).toList()

        liststepjenispenggunaanTanah?.clear()
        liststepJenisPenggunaanTanahName?.clear()
        for (i in listjtId.indices) {
            val dataPenggunaanTanah = DataPenggunaanTanah()
            dataPenggunaanTanah.projectLandUtilizationId = listjtId[i]
            dataPenggunaanTanah.projectLandUtilizationName = listjtName[i]
            dataPenggunaanTanah.projectLandUtilizationNameOther = landUtilizationNameOtherRef[i]

            adapterPenggunaanTanah.addItems(dataPenggunaanTanah)
            adapterPenggunaanTanah.notifyDataSetChanged()
        }
        sendIdPeuntukanTanah = results.landZoningSpatialPlanId
        sendIdAlasHak = results.landAlasHakTanah
        spinnerkesesuaianPeruntukanTanah.setText(results.landZoningSpatialPlanName)
        edorientasiTanahBarat.setText(results.landOrientationWest)
        edorientasiTanahTimur.setText(results.landOrientationEast)
        edorientasiTanahUtara.setText(results.landOrientationNorth)
        edorientasiTanahSelatan.setText(results.landOrientationSouth)
        edorientasiTanahBaratLaut.setText(results.landOrientationNorthwest)
        edorientasiTanahTimurLaut.setText(results.landOrientationNortheast)
        edorientasiTanahTenggara.setText(results.landOrientationSoutheast)
        edorientasiTanahBaratDaya.setText(results.landOrientationSouthwest)


        //jenis tanah
        val ltid1 = results.landAddInfoAddInfoId
        val ltid2 = ltid1.replace("[", "['")
        val ltid3 = ltid2.replace("]", "']")
        val ltid = ltid3.replace(",", "','")
        val listltId = Gson().fromJson(ltid, Array<String>::class.java).toList()

        val ltname1 = results.landAddInfoAddInfoName
        val ltname2 = ltname1.replace("[", "['")
        val ltname3 = ltname2.replace("]", "']")
        val ltname = ltname3.replace(",", "','")
        val listltName = Gson().fromJson(ltname, Array<String>::class.java).toList()
    }

    private fun showdatePicker() {
        // Get Current Date
        /*val c = Calendar.getInstance(Locale.getDefault())
        mYear = c.get(Calendar.YEAR)
        mMonth = c.get(Calendar.MONTH)
        mDay = c.get(Calendar.DATE)


        val datePickerDialog = DatePickerDialog(activity!!, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            c.set(Calendar.YEAR, year)
            c.set(Calendar.MONTH, monthOfYear)
            c.set(Calendar.DATE, dayOfMonth)

            tvtglHakTanah.text = DateFormat.format(dateTemplate, c.time)
            senddateTglDiterbitkan = DateFormat.format(dateTemplate2, c.time).toString()
            //Toast.makeText(activity!!, dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year, Toast.LENGTH_SHORT).show()
            //txtDate.setText(dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
        }, mYear, mMonth, mDay)*/

        val datePickerDialog = DatePickerDialog(activity!!,
                android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                mdateSetListenerGlobal,
                mYearGlobal, mMonthGlobal, mDayOfMpnthGlobal)
        datePickerDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        datePickerDialog.datePicker.maxDate = mcalendarGlobal.timeInMillis
        datePickerDialog.show()


    }

    private fun showdatePicker2() {
        // Get Current Date
        /*val c = Calendar.getInstance(Locale.getDefault())
        mYear = c.get(Calendar.YEAR)
        mMonth = c.get(Calendar.MONTH)
        mDay = c.get(Calendar.DATE)


        val datePickerDialog = DatePickerDialog(activity!!, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            c.set(Calendar.YEAR, year)
            c.set(Calendar.MONTH, monthOfYear)
            c.set(Calendar.DATE, dayOfMonth)

            tvtglberakhirHakTanah.text = DateFormat.format(dateTemplate, c.time)
            senddateTglBerakhirDiterbitkan = DateFormat.format(dateTemplate2, c.time).toString()
            //txtDate.setText(dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
        }, mYear, mMonth, mDay)*/
        val datePickerDialog = DatePickerDialog(activity!!,
                android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                mdateSetListenerGlobal2,
                mYearGlobal, mMonthGlobal, mDayOfMpnthGlobal)
        datePickerDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        datePickerDialog.datePicker.maxDate = mcalendarGlobal.timeInMillis
        datePickerDialog.show()

    }

    private fun selectedDate() {
        if (session.status.isNotEmpty() && tanggalEdit.isNotEmpty()) {
            val datePart = tanggalEdit.split("-")
            val year = datePart[0]
            val month = datePart[1]
            val day = datePart[2]

            Log.d("hasilnya", month.substring(0))
            val mmMonth = if (month.substring(0).contains("0")) {
                val parts = month.substring(1)
                parts.toInt() - 1
            } else {
                month.toInt() - 1
            }

            val mmDay = if (day.substring(0).contains("0")) {
                val parts = day.substring(1)
                parts.toInt()
            } else {
                day.toInt()
            }

            val mmYear = year.toInt()

            val datePickerDialog = DatePickerDialog(activity!!, AlertDialog.THEME_HOLO_DARK, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                Log.d("hasil1", dayOfMonth.toString())
                Log.d("hasil1", monthOfYear.toString())
                Log.d("hasil1", year.toString())
                // bulan dari hasil date picker kurang satu bulan dari bulan yg dipilih sehingga ditambah + 1
                datepicker = DatePicker(activity)
                datepicker!!.init(year, monthOfYear + 1, dayOfMonth, null)

                val calendar2 = GregorianCalendar(year, monthOfYear, dayOfMonth)

                tvtglHakTanah.text = DateFormat.format(dateTemplate, calendar2.time)
                senddateTglDiterbitkan = DateFormat.format(dateTemplate2, calendar2.time).toString()
                //Toast.makeText(activity, senddateBirthPicker, Toast.LENGTH_SHORT).show()

                //txtDate.setText(dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
            }, mmYear, mmMonth, mmDay)

            if (datepicker != null) {
                Log.d("hasil2", "${datepicker!!.month - 1}")
                datePickerDialog.updateDate(datepicker!!.year, datepicker!!.month - 1, datepicker!!.dayOfMonth)
            }

            datePickerDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//            datePickerDialog.datePicker.maxDate = mCalendar.timeInMillis - 1000
            datePickerDialog.show()

        } else {
            val datePickerDialog = DatePickerDialog(activity!!, AlertDialog.THEME_HOLO_DARK, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                Log.d("hasil1", monthOfYear.toString())
                // bulan dari hasil date picker kurang satu bulan dari bulan yg dipilih sehingga ditambah + 1
                datepicker2 = DatePicker(activity)
                datepicker2!!.init(year, monthOfYear + 1, dayOfMonth, null)

                val calendar2 = GregorianCalendar(year, monthOfYear, dayOfMonth)

                tvtglHakTanah.text = DateFormat.format(dateTemplate, calendar2.time)
                senddateTglDiterbitkan = DateFormat.format(dateTemplate2, calendar2.time).toString()
                //Toast.makeText(activity, senddateBirthPicker, Toast.LENGTH_SHORT).show()

                //txtDate.setText(dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
            }, mYear, mMonth, mDay)

            if (datepicker2 != null) {
                Log.d("hasil2", "${datepicker2!!.month - 1}")
                datePickerDialog.updateDate(datepicker2!!.year, datepicker2!!.month - 1, datepicker2!!.dayOfMonth)
            }

            datePickerDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//            datePickerDialog.datePicker.maxDate = mCalendar.timeInMillis - 1000
            datePickerDialog.show()
        }

    }

    private fun selectedDate2() {
        if (session.status.isNotEmpty() && tanggalEdit2.isNotEmpty()) {
            val datePart = tanggalEdit2.split("-")
            val year = datePart[0]
            val month = datePart[1]
            val day = datePart[2]

            val mmMonth = if (month.substring(0).contains("0")) {
                val parts = month.substring(1)
                parts.toInt() - 1
            } else {
                month.toInt() - 1
            }

            val mmDay = if (day.substring(0).contains("0")) {
                val parts = day.substring(1)
                parts.toInt()
            } else {
                day.toInt()
            }

            val mmYear = year.toInt()

            val datePickerDialog = DatePickerDialog(activity!!, AlertDialog.THEME_HOLO_DARK, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                /*Log.d("hasil1", dayOfMonth.toString())
                Log.d("hasil1", monthOfYear.toString())
                Log.d("hasil1", year.toString())*/
                // bulan dari hasil date picker kurang satu bulan dari bulan yg dipilih sehingga ditambah + 1
                datepicker3 = DatePicker(activity)
                datepicker3!!.init(year, monthOfYear + 1, dayOfMonth, null)

                val calendar2 = GregorianCalendar(year, monthOfYear, dayOfMonth)

                tvtglberakhirHakTanah.text = DateFormat.format(dateTemplate, calendar2.time)
                senddateTglBerakhirDiterbitkan = DateFormat.format(dateTemplate2, calendar2.time).toString()
                //Toast.makeText(activity, senddateBirthPicker, Toast.LENGTH_SHORT).show()

                //txtDate.setText(dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
            }, mmYear, mmMonth, mmDay)

            if (datepicker3 != null) {
                Log.d("hasil2", "${datepicker3!!.month - 1}")
                datePickerDialog.updateDate(datepicker3!!.year, datepicker3!!.month - 1, datepicker3!!.dayOfMonth)
            }

            datePickerDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//            datePickerDialog.datePicker.maxDate = mCalendar.timeInMillis - 1000
            datePickerDialog.show()

        } else {
            val datePickerDialog = DatePickerDialog(activity!!, AlertDialog.THEME_HOLO_DARK, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                Log.d("hasil1", monthOfYear.toString())
                // bulan dari hasil date picker kurang satu bulan dari bulan yg dipilih sehingga ditambah + 1
                datepicker4 = DatePicker(activity)
                datepicker4!!.init(year, monthOfYear + 1, dayOfMonth, null)

                val calendar2 = GregorianCalendar(year, monthOfYear, dayOfMonth)

                tvtglberakhirHakTanah.text = DateFormat.format(dateTemplate, calendar2.time)
                senddateTglBerakhirDiterbitkan = DateFormat.format(dateTemplate2, calendar2.time).toString()
                //Toast.makeText(activity, senddateBirthPicker, Toast.LENGTH_SHORT).show()

                //txtDate.setText(dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
            }, mYear, mMonth, mDay)

            if (datepicker4 != null) {
                Log.d("hasil2", "${datepicker4!!.month - 1}")
                datePickerDialog.updateDate(datepicker4!!.year, datepicker4!!.month - 1, datepicker4!!.dayOfMonth)
            }

            datePickerDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//            datePickerDialog.datePicker.maxDate = mCalendar.timeInMillis - 1000
            datePickerDialog.show()
        }

    }

    private fun getPresenter(): FormObjekTanahPresenter3? {
        formObjekTanahPresenter3 = FormObjekTanahPresenter3()
        formObjekTanahPresenter3.onAttach(this)
        return formObjekTanahPresenter3
    }

    private fun validate(): Boolean {
        var validData = false
        val sweetAlretLoading = SweetAlertDialog(activity!!, SweetAlertDialog.WARNING_TYPE)
        sweetAlretLoading.setCancelable(false)
        sweetAlretLoading.confirmText = "OK"
        sweetAlretLoading.setConfirmClickListener { sDialog ->
            sDialog?.let { if (it.isShowing) it.dismiss() }
        }
        val mdataPenggunaanTanah = adapterPenggunaanTanah.returnDataPenggunaanTanah()
        val mdataUserSHM = adapterAddUserSHM.returnDataUserSHM()


        when {
            sendIdKepemilikanTanah?.equals(0)!! -> {
                sweetAlretLoading.titleText = "Lengkapi data kepemilikan tanah"
                sweetAlretLoading.show()
                spinnerbentukKepemilikanTanah.requestFocus()
                validData = false
            }
            (valueSpinner != "Lain-Nya" && sendIdAlasHak != "Lainnya") && edNIB.text.toString().trim().isEmpty() -> {
                sweetAlretLoading.titleText = "Lengkapi data NIB"
                sweetAlretLoading.show()
                edNIB.requestFocus()
                validData = false
            }
            (valueSpinner != "Lain-Nya" && sendIdAlasHak != "Lainnya") && edNIB.text.toString().trim().isNotEmpty() && edNIB.text.toString().trim().length < 17 -> {
                sweetAlretLoading.titleText = "Lengkapi jumlah nomor NIB"
                sweetAlretLoading.show()
                edNIB.requestFocus()
                validData = false
            }
            (valueSpinner != "Lain-Nya" && sendIdAlasHak != "Lainnya") && ednomorSertifikat.text.toString().trim().isEmpty() -> {
                sweetAlretLoading.titleText = "Lengkapi data nomor sertifikat"
                sweetAlretLoading.show()
                ednomorSertifikat.requestFocus()
                validData = false
            }
            /*ednomorSertifikat.text.toString().trim().isNotEmpty() && ednomorSertifikat.text.toString().trim().length < 19 ->{
                sweetAlretLoading.titleText = "Lengkapi jumlah nomor sertifikat"
                sweetAlretLoading.show()
                ednomorSertifikat.requestFocus()
                validData = false
            }*/
            spinnerbentukKepemilikanTanah.text.toString() == "Lain-Nya" && edbentukKepemilikanTanahLain.text.toString().isEmpty() -> {
                sweetAlretLoading.titleText = "Lengkapi bentuk kepemilikan lain"
                sweetAlretLoading.show()
                edbentukKepemilikanTanahLain.requestFocus()
                validData = false
            }
            spinnerbentukKepemilikanTanah.text.toString() == "SHM" && mdataUserSHM.isEmpty() -> {
                sweetAlretLoading.titleText = "Lengkapi data pemilik SHM"
                sweetAlretLoading.show()
                edbentukKepemilikanTanahLain.requestFocus()
                validData = false
            }
            else -> {
                loop@ for (i in mdataPenggunaanTanah.indices) {
                    when {
                        mdataPenggunaanTanah[i].projectLandUtilizationId!!.isEmpty() || mdataPenggunaanTanah[i].projectLandUtilizationName!!.isEmpty() -> {
                            validData = true
                            sweetAlretLoading.titleText = "Lengkapi data penggunaan tanah"
                            sweetAlretLoading.show()
                            break@loop
                        }
                        mdataPenggunaanTanah[i].projectLandUtilizationName!! == "Lain-Nya" && mdataPenggunaanTanah[i].projectLandUtilizationNameOther!!.isEmpty() -> {
                            sweetAlretLoading.titleText = "Lengkapi data penggunaan tanah"
                            sweetAlretLoading.show()
                            validData = true
                            break@loop
                        }
                        else -> {
                            sendpenggunaanTanahId.add(mdataPenggunaanTanah[i].projectLandUtilizationId!!)
                            sendpenggunaanTanahName.add(mdataPenggunaanTanah[i].projectLandUtilizationName!!)
                            sendpenggunaanTanahNameOther.add(mdataPenggunaanTanah[i].projectLandUtilizationNameOther!!)
                            validData = true
                        }
                    }
                }
            }
        }

        return validData
    }

    override fun onsuccessgetdaftarKepemilikanTanah(responseDataListKepemilikanAtasTanah: MutableList<ResponseDataListKepemilikanAtasTanah>) {
        /*for (row in responseDataListKepemilikanAtasTanah) {
            //idPekerjaan.add(row.occupationId.toString())
            kategoriIdKepemelikanTanah.add(row.ownershipId!!)
            kategoriKepemelikanTanah.add(row.ownershipName.toString())
        }
        val adapterjenisKepemilikanTanah = ArrayAdapter<String>(activity!!, R.layout.item_spinner, kategoriKepemelikanTanah)
        spinnerbentukKepemilikanTanah.setAdapter(adapterjenisKepemilikanTanah)*/


    }

    override fun onsuccessgetdaftarPenggunaanTanah(responseDataListPenggunaanTanah: MutableList<ResponseDataListPenggunaanTanah>) {
        /*for (row in responseDataListPenggunaanTanah) {
            //idPekerjaan.add(row.occupationId.toString())
            kategoriIdPenggunaanTanah.add(row.utilizationId!!)
            kategoriPenggunaanTanah.add(row.utilizationName.toString())
        }
        val adapterjenisPenggunaanTanah = ArrayAdapter<String>(activity!!, R.layout.item_spinner, kategoriPenggunaanTanah)
        spinnerpengunaanTanahSaatIni.setAdapter(adapterjenisPenggunaanTanah)*/

        /*adapterPenggunaanTanah = AdapterPenggunaanTanah(responseDataListPenggunaanTanah, liststepjenispenggunaanTanah, activity!!, this@FormObjekTanah3)
        val linearLayoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.VERTICAL, false)

        rvjenispenggunaanTanah.adapter = adapterPenggunaanTanah
        rvjenispenggunaanTanah.layoutManager = linearLayoutManager*/
    }

    override fun onsuccessgetdaftarPeruntukanTanah(responseDataListPeruntukanTanah: MutableList<ResponseDataListPeruntukanTanah>) {
        /*for (row in responseDataListPeruntukanTanah) {
            //idPekerjaan.add(row.occupationId.toString())
            kategoriIdPeruntukanTanah.add(row.zoningSpatialPlanId!!)
            kategoriPeruntukanTanah.add(row.zoningSpatialPlanName.toString())
        }

        val adapterjeniskesesuaianPeruntukanTanah = ArrayAdapter<String>(activity!!, R.layout.item_spinner, kategoriPeruntukanTanah)
        spinnerkesesuaianPeruntukanTanah.setAdapter(adapterjeniskesesuaianPeruntukanTanah)*/
    }

    override fun onResume() {
        super.onResume()
        btnprevObjekTanah3?.setOnClickListener(this)
        btnnetxtObjekTanah3?.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        btnprevObjekTanah3?.setOnClickListener(null)
        btnnetxtObjekTanah3?.setOnClickListener(null)
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btnnetxtObjekTanah3) {
            hideKeyboard()
            dataFormTanah3.clear()
            Utils.deletedataformtanah3(activity!!)

            var validDataUsrSHM = true
            sendUserOwnerLandSHM = adapterAddUserSHM.returnDataUserSHM()

            if (validate() && validDataUsrSHM) {
                dataFormTanah3.add(DataFormTanah3(
                        sendIdKepemilikanTanah,
                        spinnerbentukKepemilikanTanah.text.toString().trim(),
                        edbentukKepemilikanTanahLain.text.toString(),
                        sendUserOwnerLandSHM.toString(),
                        senddateTglDiterbitkan,
                        senddateTglBerakhirDiterbitkan,
                        edNIB.text.toString().trim(),
                        ednomorSertifikat.text.toString().trim(),
                        sendpenggunaanTanahId,
                        sendpenggunaanTanahName,
                        sendpenggunaanTanahNameOther,
                        sendIdPeuntukanTanah,
                        spinnerkesesuaianPeruntukanTanah.text.toString().trim(),
                        edorientasiTanahBarat.text.toString(),
                        edorientasiTanahTimur.text.toString(),
                        edorientasiTanahUtara.text.toString(),
                        edorientasiTanahSelatan.text.toString(),
                        edorientasiTanahBaratLaut.text.toString(),
                        edorientasiTanahTimurLaut.text.toString(),
                        edorientasiTanahTenggara.text.toString(),
                        edorientasiTanahBaratDaya.text.toString(),
                        null,
                        null,
                        sendIdAlasHak,
                        edDampak.text.toString().trim(),
                        spinnerAlasHakTanah.text.toString().trim()
                ))

                val jsonBBM = Gson().toJson(dataFormTanah3)
                Utils.savedataformtanah3(jsonBBM, activity!!)

                if (mListener != null) {
                    mListener!!.onNextPressed(this)
                }
            }


            /*if (validate()) {
                dataFormTanah3.add(DataFormTanah3(
                        sendIdKepemilikanTanah,
                        spinnerbentukKepemilikanTanah.text.toString().trim(),
                        senddateTglDiterbitkan,
                        senddateTglBerakhirDiterbitkan,
                        edNIB.text.toString().trim(),
                        ednomorSertifikat.text.toString().trim(),
                        sendidjjenistanahpenggunaanMultiple,
                        sendNameJenistanahPenggunaanMultiple,
                        sendIdPeuntukanTanah,
                        spinnerkesesuaianPeruntukanTanah.text.toString().trim(),
                        edorientasiTanahBarat.text.toString(),
                        edorientasiTanahTimur.text.toString(),
                        edorientasiTanahUtara.text.toString(),
                        edorientasiTanahSelatan.text.toString(),
                        edorientasiTanahBaratLaut.text.toString(),
                        edorientasiTanahTimurLaut.text.toString(),
                        edorientasiTanahTenggara.text.toString(),
                        edorientasiTanahBaratDaya.text.toString()
                ))

                val jsonBBM = Gson().toJson(dataFormTanah3)
                Utils.savedataformtanah3(jsonBBM, activity!!)

                Handler().postDelayed({

                }, 1000)
            }*/


        } else if (view.id == R.id.btnprevObjekTanah3) {
            if (mListener != null)
                mListener!!.onBackPressed(this)
        }
    }

    fun addItem() {
        val dataPenggunaanTanah = DataPenggunaanTanah()
        dataPenggunaanTanah.projectLandUtilizationId = ""
        dataPenggunaanTanah.projectLandUtilizationName = ""
        dataPenggunaanTanah.projectLandUtilizationNameOther = ""

        adapterPenggunaanTanah.addItems(dataPenggunaanTanah)
        adapterPenggunaanTanah.notifyDataSetChanged()
        rvjenispenggunaanTanah.scrollToPosition(adapterPenggunaanTanah.itemCount - 1)
    }

    fun addItemUserSHM() {
        adapterAddUserSHM.addItems("")
        adapterAddUserSHM.notifyDataSetChanged()
        rvUserSHM.scrollToPosition(adapterAddUserSHM.itemCount - 1)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnStepThreeListener) {
            mListener = context
        } else {
            throw RuntimeException(("$context must implement OnFragmentInteractionListener"))
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    interface OnStepThreeListener {
        fun onBackPressed(fragment: androidx.fragment.app.Fragment)
        fun onNextPressed(fragment: androidx.fragment.app.Fragment)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): FormObjekTanah3 {
            val fragment = FormObjekTanah3()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor