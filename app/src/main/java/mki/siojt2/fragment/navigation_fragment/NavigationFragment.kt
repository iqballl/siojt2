package mki.siojt2.fragment.navigation_fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_navigation_drawer.view.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.model.NavigationData
import mki.siojt2.ui.acitivity_map_project.MapsProjectActivity
import mki.siojt2.ui.acitivity_maps_drawing.MapsActivityDrawingMapBox


class NavigationFragment : BaseFragment(), NavigationView {

    lateinit var viewx: View
    private var adapter: NavigationAdapter? = null

    companion object {
        internal const val TAG = "NavigationFragment"
        fun newInstance(): NavigationFragment {
            val args = Bundle()
            val fragment = NavigationFragment()
            //args.putInt(TAG, index)
            fragment.arguments = args
            return fragment
        }
    }

    private val arrayIcons = intArrayOf(
            R.drawable.ic_map,
            R.drawable.ic_filter_gray
    )


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewx = inflater.inflate(R.layout.fragment_navigation_drawer, container,false)

        fillData()
        setAdapter()
        adapter!!.setSelected(0)
        return viewx
    }

    private fun fillData(): ArrayList<NavigationData> {
        val navigationDataArrayList = ArrayList<NavigationData>()

        val bundle = this.arguments
        val arrayNavigation = bundle?.getStringArray("menus")

        for (i in arrayNavigation!!.indices) {
            val navigationData = NavigationData()
            navigationData.name = arrayNavigation[i]
            navigationData.drawableId = arrayIcons[i]
            navigationDataArrayList.add(navigationData)
        }

        return navigationDataArrayList
    }

    private fun setAdapter() {
        adapter = NavigationAdapter(this)
        viewx.rvNavigation!!.layoutManager = LinearLayoutManager(activity)
        viewx.rvNavigation!!.adapter = adapter

        adapter!!.refreshAdapter(fillData())
    }

    override fun setUp(view: View) {

    }

    override fun onViewClick(position: Int) {
        Log.e(TAG, "View$position")
        replaceFragment(position)
    }

    override fun onIconClick(position: Int) {
        Log.e(TAG, "Icon$position")
        replaceFragment(position)
    }

    private fun replaceFragment(position: Int) {
        try {
            (activity as MapsActivityDrawingMapBox).funreplaceFragment(position)
        }
        catch (e: Exception){
            (activity as MapsProjectActivity).funreplaceFragment(position)
        }
        adapter!!.setSelected(position)
    }

}