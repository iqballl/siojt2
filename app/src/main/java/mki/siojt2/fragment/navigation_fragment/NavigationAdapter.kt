package mki.siojt2.fragment.navigation_fragment

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import mki.siojt2.R
import mki.siojt2.model.NavigationData


class NavigationAdapter(private val listener: NavigationView): RecyclerView.Adapter<NavigationAdapter.NavigationViewHolder>() {


    private val navigationDatas: MutableList<NavigationData>
    init {
        navigationDatas = ArrayList()
    }

    inner class NavigationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener{

        init {
            itemView.setOnClickListener(this)
        }

        var tvNavigationName: TextView = itemView.findViewById(R.id.tvNavigationName) as TextView
        var ivNavigation: ImageView = itemView.findViewById(R.id.ivNavigation) as ImageView

        override fun onClick(view: View?) {
            listener.onViewClick(Integer.parseInt(view?.tag.toString()))
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NavigationViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_navigation, parent, false)
        return NavigationViewHolder(view)
    }

    override fun onBindViewHolder(holder: NavigationViewHolder, position: Int) {
        val navigationData = navigationDatas[position]
        holder.tvNavigationName.text = navigationData.name
        holder.ivNavigation.setImageResource(navigationData.drawableId)
        holder.ivNavigation.tag = position

        holder.ivNavigation.setOnClickListener {
            listener.onIconClick(Integer.parseInt(holder.ivNavigation.tag.toString()))
        }

        holder.itemView.tag = position
        holder.itemView.setBackgroundResource(if (navigationData.isSelected) R.color.ripple_color else android.R.color.transparent)
    }

    override fun getItemCount(): Int {
        return navigationDatas.size
    }

    fun refreshAdapter(data: ArrayList<NavigationData>) {
        navigationDatas.clear()
        navigationDatas.addAll(data)
        notifyDataSetChanged()
    }

    fun setSelected(position: Int) {
        for (i in navigationDatas.indices) {
            navigationDatas[i].isSelected = i == position
        }
        notifyDataSetChanged()
    }
}