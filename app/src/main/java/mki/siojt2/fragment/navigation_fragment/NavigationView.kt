package mki.siojt2.fragment.navigation_fragment

interface NavigationView {

    fun onViewClick(position: Int)
    fun onIconClick(position: Int)
}