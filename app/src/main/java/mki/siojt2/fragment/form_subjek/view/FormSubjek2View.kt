package mki.siojt2.fragment.form_subjek.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.master_data_subjek.ResponseDataSumberIdentitas

interface FormSubjek2View : MvpView {
    fun onsuccessgetdaftarSumberIdentitas(responseDataSumberIdentitas: MutableList<ResponseDataSumberIdentitas>)
}