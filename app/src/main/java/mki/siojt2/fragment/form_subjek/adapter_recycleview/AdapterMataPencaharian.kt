package mki.siojt2.fragment.form_subjek.adapter_recycleview

import android.annotation.SuppressLint
import android.content.Context
import android.opengl.Visibility
import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import mki.siojt2.R
import android.widget.AdapterView
import android.widget.LinearLayout
import com.google.android.material.textfield.TextInputEditText
import cn.pedant.SweetAlert.SweetAlertDialog
import mki.siojt2.fragment.form_objek_tanah.FormObjekTanah2
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList
import mki.siojt2.model.data_form_subjek.DataMataPencaharian
import mki.siojt2.model.master_data_subjek.ResponseDataMataPencaharianWithOutParam


class AdapterMataPencaharian internal constructor(data: MutableList<ResponseDataMataPencaharianWithOutParam>, context: Context, var mfragment: Fragment) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var mcontext: Context = context
    private var mData: MutableList<ResponseDataMataPencaharianWithOutParam>? = data
    private var mDataSet: MutableList<DataMataPencaharian>? = ArrayList()

    /*private var mStepList: ArrayList<String>? = steps
    private var mStepList1: ArrayList<String>? = steps1
    private var mStepList2: ArrayList<String>? = steps2
    private var mStepList3: ArrayList<String>? = steps3*/

    private val TYPE_HEADER = 0
    private val TYPE_FOOTER = 1
    private val TYPE_ITEM: Int = 0

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var spinnerjenismataPencaharian: com.weiwangcn.betterspinner.library.BetterSpinner = itemView.findViewById(R.id.spinnerMataPencaharian)
        /*internal var plus: android.support.v7.widget.AppCompatImageView = itemView.findViewById(R.id.btnPlusMataPencaharian)
        internal var minus: android.support.v7.widget.AppCompatImageView = itemView.findViewById(R.id.btnMinusMataPencaharian)*/
        internal var edjenismataPencaharianLain: TextInputEditText = itemView.findViewById(R.id.ednamaPencaharianLain)
        internal var edpenghasilanpencaharaian: TextInputEditText = itemView.findViewById(R.id.edPenghasilanjenisUsaha)
        internal var txttvtitleMataPencaharian: com.pixplicity.fontview.FontTextView = itemView.findViewById(R.id.tvtitleMataPencaharian)
        internal var viewmataPencharian: LinearLayout = itemView.findViewById(R.id.llmatapencaharianOther)

        init {

            /*
            minus.setOnClickListener {
                val position = adapterPosition
                try {
                    mStepList!!.removeAt(position)
                    mStepList2!!.removeAt(position)
                    notifyItemRemoved(position)
                    notifyDataSetChanged()
                    notifyItemChanged(position)
                    Toast.makeText(mcontext, position.toString(), Toast.LENGTH_SHORT).show()
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                }
            }
            plus.setOnClickListener {
                val position = adapterPosition
                if (mStepList?.get(position)!!.isEmpty() || mStepList2?.get(position)!!.isEmpty()) {
                    val sweetAlretLoading = SweetAlertDialog(mcontext, SweetAlertDialog.WARNING_TYPE)
                    sweetAlretLoading.titleText = "LENGKAPI DATA"
                    sweetAlretLoading.setCancelable(false)
                    sweetAlretLoading.confirmText = "OK"
                    sweetAlretLoading.setConfirmClickListener { sDialog ->
                        sDialog?.let { if (it.isShowing) it.dismiss() }
                    }
                    sweetAlretLoading.show()
                } else {
                    Toast.makeText(mcontext, "${position + 1}", Toast.LENGTH_SHORT).show()
                    try {
                        mStepList!!.add(position + 1, "")
                        mStepList2!!.add(position + 1, "")
                        notifyItemInserted(position + 1)
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        e.printStackTrace()
                    }
                }

            }*/
            setCurrency(edpenghasilanpencaharaian)
            spinnerjenismataPencaharian.onItemClickListener = AdapterView.OnItemClickListener { adapterView, _, position, id ->
                val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataMataPencaharianWithOutParam
                val valueSpinner = adapterView.getItemAtPosition(position).toString()

                mDataSet!![adapterPosition].livelihoodLiveId = selectedProject.getliveliHoodId().toString()
                mDataSet!![adapterPosition].livelihoodLiveName = selectedProject.getliveliHoodName().toString()

                when (mDataSet!![adapterPosition].livelihoodLiveName) {
                    "Tidak ada Mata Pencaharian" -> {
                        edpenghasilanpencaharaian.isEnabled = false
                        mDataSet!![adapterPosition].livelihoodIncome = "0"
                        edpenghasilanpencaharaian.setText("0")
                        viewmataPencharian.visibility = View.GONE
                    }
                    "Lain-Nya" -> {
                        viewmataPencharian.visibility = View.VISIBLE
                    }
                    else -> {
                        viewmataPencharian.visibility = View.GONE
                        edjenismataPencaharianLain.text = null

                        edpenghasilanpencaharaian.isEnabled = true
                        mDataSet!![adapterPosition].livelihoodIncome = ""
                        edpenghasilanpencaharaian.text = null
                    }
                }

                //Toast.makeText(mcontext, adapterPosition.toString(), Toast.LENGTH_SHORT).show()

            }

            edjenismataPencaharianLain.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    mDataSet!![adapterPosition].livelihoodOther = s.toString()
                }

                override fun afterTextChanged(s: Editable) {}
            })

            edpenghasilanpencaharaian.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    mDataSet!![adapterPosition].livelihoodIncome = s.toString()
                }

                override fun afterTextChanged(s: Editable) {}
            })

        }
    }

    inner class ViewHolderFooter(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var plus: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnPlusMataPencaharian2)
        internal var minus: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnMinusMataPencaharian2)

        init {
            minus.setOnClickListener {
                val position = adapterPosition
                try {
                    mDataSet!!.removeAt(position - 1)
                    notifyItemRemoved(position)
                    notifyDataSetChanged()
                    notifyItemChanged(position)
                    //Toast.makeText(mcontext, position.toString(), Toast.LENGTH_SHORT).show()
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                }
            }
            plus.setOnClickListener {
                val position = adapterPosition
                //Toast.makeText(mcontext, "${position}", Toast.LENGTH_SHORT).show()

                when {
                    mDataSet?.get(position - 1)!!.livelihoodLiveId!!.isEmpty() || mDataSet?.get(position - 1)!!.livelihoodIncome!!.isEmpty() -> {
                        val sweetAlretLoading = SweetAlertDialog(mcontext, SweetAlertDialog.WARNING_TYPE)
                        sweetAlretLoading.titleText = "LENGKAPI DATA"
                        sweetAlretLoading.setCancelable(false)
                        sweetAlretLoading.confirmText = "OK"
                        sweetAlretLoading.setConfirmClickListener { sDialog ->
                            sDialog?.let { if (it.isShowing) it.dismiss() }
                        }
                        sweetAlretLoading.show()
                    }

                    mDataSet?.get(position - 1)!!.livelihoodLiveName!! == "Lain-Nya" && mDataSet?.get(position - 1)!!.livelihoodIncome!!.isEmpty() || mDataSet?.get(position - 1)!!.livelihoodLiveName!! == "Lain-Nya" && mDataSet?.get(position - 1)!!.livelihoodOther!!.isEmpty() -> {
                        val sweetAlretLoading = SweetAlertDialog(mcontext, SweetAlertDialog.WARNING_TYPE)
                        sweetAlretLoading.titleText = "LENGKAPI DATA"
                        sweetAlretLoading.setCancelable(false)
                        sweetAlretLoading.confirmText = "OK"
                        sweetAlretLoading.setConfirmClickListener { sDialog ->
                            sDialog?.let { if (it.isShowing) it.dismiss() }
                        }
                        sweetAlretLoading.show()
                    }

                    else -> {
                        try {
                            /* mStepList!!.add(position + 2, "")
                             mStepList2!!.add(position + 2, "")
                             notifyItemInserted(position + 2)
                             notifyItemRangeInserted(position + 2, mStepList!!.size)
                             notifyDataSetChanged()*/
                            (mfragment as FormObjekTanah2).addItemMataPencaharian(adapterPosition + 2)
                        } catch (e: ArrayIndexOutOfBoundsException) {
                            e.printStackTrace()
                        }
                    }
                }

            }
        }
    }


    @NonNull
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        /*val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_mata_pencaharian, viewGroup, false)
        return ViewHolder(v)*/
        /*when (viewType) {
            TYPE_ITEM -> {
                val itemView = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_mata_pencaharian, viewGroup, false)
                return ViewHolder(itemView)
            }
            TYPE_FOOTER -> {
                val itemView = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_mata_pencaharian_footer, viewGroup, false)
                return ViewHolderFooter(itemView)
            }

        }*/
        return when (viewType) {
            //Inflating footer view
            TYPE_ITEM -> ViewHolder(
                    LayoutInflater.from(viewGroup.context).inflate(R.layout.item_mata_pencaharian, viewGroup, false))
            //Inflating recycle view item layout
            else -> ViewHolderFooter(
                    LayoutInflater.from(viewGroup.context).inflate(R.layout.item_mata_pencaharian_footer, viewGroup, false))
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val x = holder.layoutPosition
        if (holder is ViewHolder) {
            /*if (position + 1 == mStepList!!.size) {
                holder.minus.visibility = View.VISIBLE
            } else {
                holder.minus.visibility = View.GONE
            }*/
            holder.txttvtitleMataPencaharian.text = "Jenis mata pencaharian ${x + 1}"

            if (mDataSet!![x].livelihoodLiveId!!.isNotEmpty() || mDataSet!![x].livelihoodIncome!!.isNotEmpty()) {
                holder.edpenghasilanpencaharaian.setText(mDataSet?.get(x)!!.livelihoodIncome.toString())
                holder.spinnerjenismataPencaharian.setText(mDataSet?.get(x)!!.livelihoodLiveName.toString())

                if (mDataSet?.get(x)!!.livelihoodOther.toString().isNotEmpty()) {
                    holder.viewmataPencharian.visibility = View.VISIBLE
                    holder.edjenismataPencaharianLain.setText(mDataSet?.get(x)!!.livelihoodOther.toString())
                } else {
                    holder.viewmataPencharian.visibility = View.GONE
                }
                //val selectedProject = adapterspinnerSumberAir2.getItem(position) as ResponseDataMataPencaharianWithOutParam
                //val spinpos = adapterspinnerSumberAir2.getItem(1).getliveliHoodName()!!.toInt()
                //holder.spinnerjenismataPencaharian.setSelection(selectedProject.getliveliHoodName()!!.toInt())
                //holder.edpenghasilanpencaharaian.setText(mStepList2?.get(x).toString())
                //holder.spinnerjenismataPencaharian.setSelection(mStepList!![x].toInt())

            } else {
                holder.viewmataPencharian.visibility = View.GONE
                holder.spinnerjenismataPencaharian.setText("")
                holder.edpenghasilanpencaharaian.text = null
                holder.edpenghasilanpencaharaian.hint = "....."

                holder.edjenismataPencaharianLain.text = null
                holder.edjenismataPencaharianLain.hint = "....."
            }

            val adapterspinnerSumberAir2 = ArrayAdapter<ResponseDataMataPencaharianWithOutParam>(mcontext, R.layout.item_spinner, mData!!)
            holder.spinnerjenismataPencaharian.setAdapter(adapterspinnerSumberAir2)
        } else if (holder is ViewHolderFooter) {
            if (x <= 1) {
                holder.minus.visibility = View.GONE
            } else {
                holder.minus.visibility = View.VISIBLE
            }
        }
        /*if (x == 0) {
            holder.minus.visibility = View.GONE
        }

        if (x == itemCount) {
            holder.minus.visibility = View.VISIBLE
        }*/

        /*holder.spinnerSumberAir.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                val gender = holder.spinnerSumberAir.text.toString()
                Toast.makeText(mcontext, gender, Toast.LENGTH_LONG).show()

            }
        })*/
        /*if (stepList.get(x).length() > 0)
        {
            holder.step.setText(stepList.get(x))
        }
        else
        {
            holder.step.setText(null)
            holder.step.setHint("Next Step")
            holder.step.requestFocus()
        }*/
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == mDataSet!!.size) TYPE_FOOTER else TYPE_ITEM
    }

    override fun getItemCount(): Int {
        return mDataSet!!.size + 1
    }


    fun clear() {
        /*val size = this.mStepList!!.size
        this.mStepList!!.clear()
        notifyItemRangeRemoved(0, size)
        notifyDataSetChanged()

        if (mStepList!!.isEmpty() || mStepList!!.isNullOrEmpty()) {
            if (mcontext is ActicityFormAddObjekLain) {
                (mcontext as ActicityFormAddObjekLain).removeItemSumberAir()
            }
        }*/
        this.mDataSet!!.clear()
        notifyDataSetChanged()
    }

    fun getdataPencaharian(): List<DataMataPencaharian> {
        return mDataSet!!
    }

    fun addItems(data: DataMataPencaharian) {
        this.mDataSet!!.add(data)
        notifyDataSetChanged()
    }

    /*fun getStepList(): java.util.ArrayList<String> {
        return mStepList!!
    }

    fun getStepList1(): java.util.ArrayList<String> {
        return mStepList1!!
    }

    fun getStepList2(): java.util.ArrayList<String> {
        return mStepList2!!
    }

    fun getStepList3(): java.util.ArrayList<String> {
        return mStepList3!!
    }*/

    private fun setCurrency(editText: TextInputEditText) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                editText.removeTextChangedListener(this)
                try {
                    var originalString = editable.toString()
                    val longval: Double?
                    val formatter = NumberFormat.getInstance(Locale.GERMANY) as DecimalFormat
                    val symbols = formatter.decimalFormatSymbols

                    symbols.groupingSeparator = '.'
                    formatter.decimalFormatSymbols = symbols
                    if (originalString.contains(".")) {
                        originalString = originalString.replace("[.]".toRegex(), "")
                    }
                    longval = java.lang.Double.parseDouble(originalString)

                    val formattedString = formatter.format(longval)
                    //setting text after format to EditText
                    editText.setText(formattedString)
                    editText.setSelection(editText.text!!.length)
                } catch (nfe: NumberFormatException) {
                    nfe.printStackTrace()
                }
                editText.addTextChangedListener(this)

            }
        })

    }

}