package mki.siojt2.fragment.form_subjek.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface Form3MVVPPresenter : MVPPresenter {

    fun getdaftarMataPencharian(searchCode: String, limit: Int)
}