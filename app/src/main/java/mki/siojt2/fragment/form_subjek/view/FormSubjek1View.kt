package mki.siojt2.fragment.form_subjek.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.master_data_subjek.ResponseDataPekerjaan
import mki.siojt2.model.region.ResponseDataCity
import mki.siojt2.model.region.ResponseDataKecamatan
import mki.siojt2.model.region.ResponseDataKelurahan
import mki.siojt2.model.region.ResponseDataProvince

/**
 * Created by iqbal on 09/09/19
 */

interface FormSubjek1View : MvpView {
    fun onsuccessgetregioBirthPlace(responseDataCity: MutableList<ResponseDataCity>)
    fun onsuccessgetregionProvince(responseDataProvince: MutableList<ResponseDataProvince>)
    fun onsuccessgetregionCity(responseDataCity: MutableList<ResponseDataCity>)
    fun onsuccessgetregionKecamatan(responseDataKecamatan: MutableList<ResponseDataKecamatan>)
    fun onsuccessgetregionKelurahan(responseDataKelurahan: MutableList<ResponseDataKelurahan>)

    fun onsuccessgetdaftarPekerjaan(responseDataPekerjaan: MutableList<ResponseDataPekerjaan>)
}
