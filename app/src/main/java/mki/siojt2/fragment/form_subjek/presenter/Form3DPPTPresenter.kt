package mki.siojt2.fragment.form_subjek.presenter

import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.fragment.form_subjek.view.FormSubjek3View
import mki.siojt2.utils.rxJava.RxUtils

class Form3DPPTPresenter : BasePresenter(), Form3MVVPPresenter {

    private var disposables: CompositeDisposable = CompositeDisposable()

    override fun getdaftarMataPencharian(searchCode: String, limit: Int) {
        disposables.add(
                dataManager.getdaftarMataPencaharian(searchCode, limit)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdaftarMataPencharian(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }


    private fun view(): FormSubjek3View {
        return getView() as FormSubjek3View
    }
}