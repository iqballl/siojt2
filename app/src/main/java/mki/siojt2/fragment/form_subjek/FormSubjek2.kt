package mki.siojt2.fragment.form_subjek

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_form_subjek_2.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import com.whiteelephant.monthpicker.MonthPickerDialog
import android.util.Log
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.gson.Gson
import cn.pedant.SweetAlert.SweetAlertDialog
import io.realm.Realm
import mki.siojt2.fragment.form_subjek.presenter.Form2DPPTPresenter
import mki.siojt2.fragment.form_subjek.view.FormSubjek2View
import mki.siojt2.model.data_form_subjek.DataFormSubjek2
import mki.siojt2.model.localsave.AhliWaris
import mki.siojt2.model.localsave.PartyAdd
import mki.siojt2.model.localsave.Session
import mki.siojt2.model.master_data_subjek.ResponseDataSumberIdentitas
import mki.siojt2.model.master_data_subjek.ResponseDataSumberIdentitasWithOutParam
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.Utils
import java.util.*
import mki.siojt2.utils.extension.YearCalculation
import kotlin.collections.ArrayList
import android.text.InputFilter
import android.text.InputType
import droidninja.filepicker.FilePickerBuilder
import mki.siojt2.model.master_data_form_tanah.ResponseDataListEaseOfReach
import droidninja.filepicker.FilePickerConst

import android.app.Activity
import android.app.AlertDialog

import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.provider.Settings
import droidninja.filepicker.FilePickerConst.KEY_SELECTED_MEDIA
import androidx.recyclerview.widget.DefaultItemAnimator

import android.widget.Toast

import droidninja.filepicker.utils.ContentUriUtils

import androidx.recyclerview.widget.StaggeredGridLayoutManager

import androidx.recyclerview.widget.OrientationHelper
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import droidninja.filepicker.FilePickerConst.KEY_SELECTED_DOCS
import droidninja.filepicker.utils.ContentUriUtils.getFilePath
import mki.siojt2.fragment.form_subjek.adapter_recycleview.ImageAdapter
import java.net.URISyntaxException
import io.realm.RealmList
import mki.siojt2.model.localsave.PartyAddImage
import mki.siojt2.ui.activity_image_picker.ImagePickerActivity
import java.io.IOException


class FormSubjek2 : BaseFragment(), FormSubjek2View, View.OnClickListener, AdapterTakePictureSubject.OnItemImageClickListener {

    private var mParam1: String? = null
    private var mParam2: String? = null
    private var mListener: OnStepTwoListener? = null

    private var year: YearCalculation? = null

    private var dataDefaultTakeFotoSubject: PartyAddImage? = null
    lateinit var adapterTakePictureSubject: AdapterTakePictureSubject

    private var filePaths = arrayListOf<String?>()
    private var fileName = arrayListOf<String>()
    private var files = arrayListOf<Uri>()
    private var mpositionImageClick: Int? = null
    private val REQUEST_IMAGE = 100

    private lateinit var form2DPPTPresenter: Form2DPPTPresenter
//    private var sendIdSourceIdentitas: Int = 0

    private var dataFormSubjek2: MutableList<DataFormSubjek2> = ArrayList()

    lateinit var realm: Realm

    private lateinit var session: Session

    var selectedOwnership: MutableList<String> = arrayListOf()
    var landOwnershipData: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }

        realm = RealmController.with(this).realm
        realm.isAutoRefresh
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_form_subjek_2, container, false)
    }

    private fun getDataSubjek2() {
        session = Session(context)

        if (session.id != "" && session.status != "" && session.from == "1") {
            val results =
                realm.where(PartyAdd::class.java).equalTo("TempId", session.id.toInt()).findFirst()
            if (results != null) {
                tampilkanData(results)
                if (results.partyAddImage.isNotEmpty()) {
                    val dataimage = realm.copyFromRealm(results.partyAddImage)
                    for (element in 0 until dataimage.size) {
                        adapterTakePictureSubject.addItems(dataimage[element]!!)
                        adapterTakePictureSubject.notifyDataSetChanged()
                    }
                }
                else {
                    dataDefaultTakeFotoSubject!!.imagepathLand = ""
                    dataDefaultTakeFotoSubject!!.imagenameLand = ""
                    adapterTakePictureSubject.addItems(dataDefaultTakeFotoSubject!!)
                    adapterTakePictureSubject.notifyDataSetChanged()
                }
            } else {
                Log.e("data", "kosong")
            }
        } else if (session.id != "" && session.status != "" && session.from == "3") {
            val results =
                realm.where(AhliWaris::class.java).equalTo("AhliWarisIdTemp", session.id.toInt())
                    .findFirst()
            if (results != null) {
                tampilkanDataAhliWaris(results)
                if (results.partyAddImage.isNotEmpty()) {
                    val dataimage = realm.copyFromRealm(results.partyAddImage)
                    for (element in 0 until dataimage.size) {
                        adapterTakePictureSubject.addItems(dataimage[element]!!)
                        adapterTakePictureSubject.notifyDataSetChanged()
                    }
                }
                else {
                    dataDefaultTakeFotoSubject!!.imagepathLand = ""
                    dataDefaultTakeFotoSubject!!.imagenameLand = ""
                    adapterTakePictureSubject.addItems(dataDefaultTakeFotoSubject!!)
                    adapterTakePictureSubject.notifyDataSetChanged()
                }
            } else {
                Log.e("data", "kosong")
            }
        }
        else {
            dataDefaultTakeFotoSubject!!.imagepathLand = ""
            dataDefaultTakeFotoSubject!!.imagenameLand = ""
            adapterTakePictureSubject.addItems(dataDefaultTakeFotoSubject!!)
            adapterTakePictureSubject.notifyDataSetChanged()
        }
    }

    private fun tampilkanDataAhliWaris(results: AhliWaris) {
        if (results.partyOwnershipType.contains("Data Pemilik Tanah")) {
            cbOwnerField.isChecked = true
            llLandOwnership.visibility = View.VISIBLE
            spinnerLandOwnership.setText(results.partyOwnershipLandData)
            landOwnershipData = results.partyOwnershipLandData

            val dataOwnershipLandData = arrayListOf<String>()
            dataOwnershipLandData.add("Pemilik tanah")
            dataOwnershipLandData.add("Nadzir tanah wakaf")
            dataOwnershipLandData.add("Pemilik tanah adat")
            dataOwnershipLandData.add("Masyarakat hukum adat")
            dataOwnershipLandData.add("Pihak yang menguasai tanah negara dengan itikad baik")
            dataOwnershipLandData.add("Pemegang dasar")
            dataOwnershipLandData.add("Menguasai")
            dataOwnershipLandData.add("Menggarap")
            dataOwnershipLandData.add("Menyewa")

            val adapterLandOwnership =
                ArrayAdapter(activity!!, R.layout.item_spinner, dataOwnershipLandData)
            spinnerLandOwnership.setAdapter(adapterLandOwnership)
            spinnerLandOwnership.onItemClickListener =
                AdapterView.OnItemClickListener { adapterView, _, position, _ ->
                    landOwnershipData = dataOwnershipLandData[position]
                }
        }
        if (results.partyOwnershipType.contains("Data Pemilik Bangunan")) {
            cbOwnerBuilding.isChecked = true
        }
        if (results.partyOwnershipType.contains("Data Pemilik Tanaman")) {
            cbOwnerPlant.isChecked = true
        }
        edNPWP.setText(results.partyNPWP)
        tvtotalTahunDomisil.text = results.partyYearStayBegin.toString()

        for(i in results.filePaths){
            files.add(Uri.parse(i))
            filePaths.add(i)
        }

        fileName.addAll(results.fileName)

        val layoutManager = StaggeredGridLayoutManager(3, OrientationHelper.VERTICAL)
        layoutManager.gapStrategy =
            StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS
        rvFilePicker?.layoutManager = layoutManager
        val imageAdapter =
            ImageAdapter(requireContext(), files, fileName, object : ImageAdapter.ImageAdapterListener {
                override fun onItemClick(uri: Uri?) {
                    try {
                        //make sure to use this getFilePath method from worker thread
                        val path = getFilePath(
                            rvFilePicker.context,
                            uri!!
                        )
                        if (path != null) {
                            Toast.makeText(rvFilePicker?.context, path, Toast.LENGTH_SHORT)
                                .show()
                        }
                    } catch (e: URISyntaxException) {
                        e.printStackTrace()
                    }
                }

                override fun onItemDelete(file: Uri?) {
                    files.remove(file)
                    filePaths.remove(file?.path)
                    fileName.remove(getFileName(file!!))
                    rvFilePicker.adapter?.notifyDataSetChanged()
                }
            })
        rvFilePicker?.adapter = imageAdapter
        rvFilePicker?.itemAnimator = DefaultItemAnimator()
    }

    private fun onProfileImageClick() {
        Dexter.withActivity(activity!!)
            .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        showImagePickerOptions()
                    }
                    if (report.isAnyPermissionPermanentlyDenied) {
                        showSettingsDialog()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                    token.continuePermissionRequest()
                }
            }).check()
    }

    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(getString(R.string.dialog_permission_title))
        builder.setMessage(getString(R.string.dialog_permission_message))
        builder.setPositiveButton(getString(R.string.go_to_settings)) { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(getString(android.R.string.cancel)) { dialog, which -> dialog.cancel() }
        builder.show()
    }

    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", activity!!.packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(activity!!, object : ImagePickerActivity.PickerOptionListener {
            override fun onTakeCameraSelected() {
                launchCameraIntent()
            }

            override fun onChooseGallerySelected() {
                launchGalleryIntent()
            }
        })
    }

    private fun launchCameraIntent() {
        val intent = Intent(activity!!, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE)
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    private fun launchGalleryIntent() {
        val intent = Intent(activity!!, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE)
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    private fun tampilkanData(results: PartyAdd) {
//        spinnerSumberIdentitas.setText(results.partyIdentitySourceName)
//        if (results.partyIdentitySourceOther.isNotEmpty()) {
//            llsumberidentitasLain.visibility = View.VISIBLE
//            edsumberIdentitasLain.setText(results.partyIdentitySourceOther)
//        } else {
//            llsumberidentitasLain.visibility = View.GONE
//        }
//
//        when (spinnerSumberIdentitas.text.toString()) {
//            "Lain-Nya" -> {
//                ednomorIdentitas.filters = arrayOf<InputFilter>()
//                llsumberidentitasLain.visibility = View.VISIBLE
//                ednomorIdentitas.inputType = InputType.TYPE_CLASS_TEXT
//            }
//            "KTP" -> {
//                ednomorIdentitas.filters = arrayOf<InputFilter>()
//                setEditTextMaxLength(ednomorIdentitas, 16)
//                llsumberidentitasLain.visibility = View.GONE
//                ednomorIdentitas.inputType = InputType.TYPE_CLASS_NUMBER
//            }
//            "SIM" -> {
//                ednomorIdentitas.filters = arrayOf<InputFilter>()
//                setEditTextMaxLength(ednomorIdentitas, 12)
//                llsumberidentitasLain.visibility = View.GONE
//                ednomorIdentitas.inputType = InputType.TYPE_CLASS_NUMBER
//            }
//            "Paspor" -> {
//                ednomorIdentitas.filters = arrayOf<InputFilter>()
//                setEditTextMaxLength(ednomorIdentitas, 10)
//                llsumberidentitasLain.visibility = View.GONE
//                ednomorIdentitas.inputType = InputType.TYPE_CLASS_TEXT
//            }
//            else -> {
//                ednomorIdentitas.filters = arrayOf<InputFilter>()
//                llsumberidentitasLain.visibility = View.GONE
//                ednomorIdentitas.inputType = InputType.TYPE_CLASS_NUMBER
//            }
//        }
//
//        sendIdSourceIdentitas = results.partyIdentitySourceId
//        ednomorIdentitas.setText(results.partyIdentityNumber)
        if (results.partyOwnershipType.contains("Data Pemilik Tanah")) {
            cbOwnerField.isChecked = true
            llLandOwnership.visibility = View.VISIBLE
            spinnerLandOwnership.setText(results.partyOwnershipLandData)
            landOwnershipData = results.partyOwnershipLandData

            val dataOwnershipLandData = arrayListOf<String>()
            dataOwnershipLandData.add("Pemilik tanah")
            dataOwnershipLandData.add("Nadzir tanah wakaf")
            dataOwnershipLandData.add("Pemilik tanah adat")
            dataOwnershipLandData.add("Masyarakat hukum adat")
            dataOwnershipLandData.add("Pihak yang menguasai tanah negara dengan itikad baik")
            dataOwnershipLandData.add("Pemegang dasar")
            dataOwnershipLandData.add("Menguasai")
            dataOwnershipLandData.add("Menggarap")
            dataOwnershipLandData.add("Menyewa")

            val adapterLandOwnership =
                ArrayAdapter(activity!!, R.layout.item_spinner, dataOwnershipLandData)
            spinnerLandOwnership.setAdapter(adapterLandOwnership)
            spinnerLandOwnership.onItemClickListener =
                AdapterView.OnItemClickListener { adapterView, _, position, _ ->
                    landOwnershipData = dataOwnershipLandData[position]
                }
        }
        if (results.partyOwnershipType.contains("Data Pemilik Bangunan")) {
            cbOwnerBuilding.isChecked = true
        }
        if (results.partyOwnershipType.contains("Data Pemilik Tanaman")) {
            cbOwnerPlant.isChecked = true
        }
        edNPWP.setText(results.partyNPWP)
        tvtotalTahunDomisil.text = results.partyYearStayBegin.toString()

        for(i in results.filePaths){
            files.add(Uri.parse(i))
            filePaths.add(i)
        }

        fileName.addAll(results.fileName)

        val layoutManager = StaggeredGridLayoutManager(3, OrientationHelper.VERTICAL)
        layoutManager.gapStrategy =
            StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS
        rvFilePicker?.layoutManager = layoutManager
        val imageAdapter =
            ImageAdapter(requireContext(), files, fileName, object : ImageAdapter.ImageAdapterListener {
                override fun onItemClick(uri: Uri?) {
                    try {
                        //make sure to use this getFilePath method from worker thread
                        val path = getFilePath(
                            rvFilePicker.context,
                            uri!!
                        )
                        if (path != null) {
                            Toast.makeText(rvFilePicker?.context, path, Toast.LENGTH_SHORT)
                                .show()
                        }
                    } catch (e: URISyntaxException) {
                        e.printStackTrace()
                    }
                }

                override fun onItemDelete(file: Uri?) {
                    files.remove(file)
                    filePaths.remove(file?.path)
                    fileName.remove(getFileName(file!!))
                    rvFilePicker.adapter?.notifyDataSetChanged()
                }
            })
        rvFilePicker?.adapter = imageAdapter
        rvFilePicker?.itemAnimator = DefaultItemAnimator()
    }

    fun addphotoItem() {
        val dataImage = PartyAddImage()
        dataImage.imagepathLand = ""
        dataImage.imagenameLand = ""
        adapterTakePictureSubject.addItems(dataImage)
        adapterTakePictureSubject.notifyDataSetChanged()
        rvlistPhoto.scrollToPosition(adapterTakePictureSubject.itemCount - 1)
    }

    override fun setUp(view: View) {
        Log.d("FormSubjek2", "visible")

        ImagePickerActivity.clearCache(activity!!)

        dataDefaultTakeFotoSubject = PartyAddImage()

        /* set adapter */
        adapterTakePictureSubject = AdapterTakePictureSubject(activity!!, this)
        adapterTakePictureSubject.setListnerItemImageClick(this)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity!!, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)

        rvlistPhoto.adapter = adapterTakePictureSubject
        rvlistPhoto.layoutManager = linearLayoutManager

        if(mParam2 == "1"){
            txOwner.text = "Data Ahli Waris"
            cbOwnerField.text = "Data Ahli Waris Tanah"
            cbOwnerField.text = "Data Ahli Waris Bangunan"
            cbOwnerField.text = "Data Ahli Waris Tanaman"
            txUpload.text = "Unggah dokumentasi bukti ahli waris"
        }

        btnAddFile?.setOnClickListener {
            FilePickerBuilder.instance
                .enableImagePicker(true)
                .setMaxCount(10) //optional
                .setActivityTheme(R.style.LibAppTheme) //optional
                .pickFile(this)
        }

        cbOwnerBuilding.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                selectedOwnership.add("Data Pemilik Bangunan")
            } else {
                selectedOwnership.remove("Data Pemilik Bangunan")
            }
        }

        cbOwnerField.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                selectedOwnership.add("Data Pemilik Tanah")
                llLandOwnership.visibility = View.VISIBLE
            } else {
                selectedOwnership.remove("Data Pemilik Tanah")
                llLandOwnership.visibility = View.GONE
            }
        }

        cbOwnerPlant.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                selectedOwnership.add("Data Pemilik Tanaman")
            } else {
                selectedOwnership.remove("Data Pemilik Tanaman")
            }
        }

        val dataOwnershipLandData = arrayListOf<String>()
        dataOwnershipLandData.add("Pemilik tanah")
        dataOwnershipLandData.add("Nadzir tanah wakaf")
        dataOwnershipLandData.add("Pemilik tanah adat")
        dataOwnershipLandData.add("Masyarakat hukum adat")
        dataOwnershipLandData.add("Pihak yang menguasai tanah negara dengan itikad baik")
        dataOwnershipLandData.add("Pemegang dasar")
        dataOwnershipLandData.add("Menguasai")
        dataOwnershipLandData.add("Menggarap")
        dataOwnershipLandData.add("Menyewa")

        val adapterLandOwnership =
            ArrayAdapter(activity!!, R.layout.item_spinner, dataOwnershipLandData)
        spinnerLandOwnership.setAdapter(adapterLandOwnership)
        spinnerLandOwnership.onItemClickListener =
            AdapterView.OnItemClickListener { adapterView, _, position, _ ->
                landOwnershipData = dataOwnershipLandData[position]
            }

        val results = realm.where(ResponseDataSumberIdentitasWithOutParam::class.java).findAll()
        val data: MutableList<ResponseDataSumberIdentitasWithOutParam> =
            realm.copyFromRealm(results)

        getDataSubjek2()

//        val adapterSumberIdentitas = ArrayAdapter(activity!!, R.layout.item_spinner, data)
//        spinnerSumberIdentitas.setAdapter(adapterSumberIdentitas)
//        spinnerSumberIdentitas.onItemClickListener = AdapterView.OnItemClickListener { adapterView, _, position, _ ->
//            val valueSpinner = adapterView.getItemAtPosition(position).toString()
//            val dataSpinner = adapterView.getItemAtPosition(position) as ResponseDataSumberIdentitasWithOutParam
//            sendIdSourceIdentitas = dataSpinner.getidentitySourceId()!!
//
//            when (valueSpinner) {
//                "Lain-Nya" -> {
//                    ednomorIdentitas.filters = arrayOf<InputFilter>()
//                    llsumberidentitasLain.visibility = View.VISIBLE
//                    edsumberIdentitasLain.text = null
//                    ednomorIdentitas.text = null
//                    ednomorIdentitas.inputType = InputType.TYPE_CLASS_TEXT
//                }
//                "KTP" -> {
//                    ednomorIdentitas.filters = arrayOf<InputFilter>()
//                    setEditTextMaxLength(ednomorIdentitas, 16)
//                    llsumberidentitasLain.visibility = View.GONE
//                    edsumberIdentitasLain.text = null
//                    ednomorIdentitas.text = null
//                    ednomorIdentitas.inputType = InputType.TYPE_CLASS_NUMBER
//                }
//                "SIM" -> {
//                    ednomorIdentitas.filters = arrayOf<InputFilter>()
//                    setEditTextMaxLength(ednomorIdentitas, 12)
//                    llsumberidentitasLain.visibility = View.GONE
//                    edsumberIdentitasLain.text = null
//                    ednomorIdentitas.text = null
//                    ednomorIdentitas.inputType = InputType.TYPE_CLASS_NUMBER
//                }
//                "Paspor" -> {
//                    ednomorIdentitas.filters = arrayOf<InputFilter>()
//                    setEditTextMaxLength(ednomorIdentitas, 10)
//                    llsumberidentitasLain.visibility = View.GONE
//                    edsumberIdentitasLain.text = null
//                    ednomorIdentitas.text = null
//                    ednomorIdentitas.inputType = InputType.TYPE_CLASS_TEXT
//                }
//                else -> {
//                    ednomorIdentitas.filters = arrayOf<InputFilter>()
//                    llsumberidentitasLain.visibility = View.GONE
//                    edsumberIdentitasLain.text = null
//                    ednomorIdentitas.text = null
//                    ednomorIdentitas.inputType = InputType.TYPE_CLASS_NUMBER
//                }
//            }
//        }

        setformatterNPWP(edNPWP)
        year = YearCalculation()
        tvtitlethnDomisil.setOnClickListener {
            showDialogYearOnly()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            FilePickerConst.REQUEST_CODE_DOC -> if (resultCode == Activity.RESULT_OK && data != null) {
                val selectedData: List<Uri> = data.getParcelableArrayListExtra(KEY_SELECTED_DOCS)
                files.addAll(selectedData)
                for(i in selectedData){
                    filePaths.add(i.path)
                    fileName.add(if(getFileName(i).isNullOrEmpty()){""}else{getFileName(i)!!})
                }

                val layoutManager = StaggeredGridLayoutManager(3, OrientationHelper.VERTICAL)
                layoutManager.gapStrategy =
                    StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS
                rvFilePicker?.layoutManager = layoutManager
                val imageAdapter =
                    ImageAdapter(requireContext(), files, fileName, object : ImageAdapter.ImageAdapterListener {
                        override fun onItemClick(uri: Uri?) {
                            try {
                                //make sure to use this getFilePath method from worker thread
                                val path = getFilePath(
                                    rvFilePicker.context,
                                    uri!!
                                )
                                if (path != null) {
                                    Toast.makeText(rvFilePicker?.context, path, Toast.LENGTH_SHORT)
                                        .show()
                                }
                            } catch (e: URISyntaxException) {
                                e.printStackTrace()
                            }
                        }

                        override fun onItemDelete(file: Uri?) {
                            files.remove(file)
                            filePaths.remove(file?.path)
                            fileName.remove(getFileName(file!!))
                            rvFilePicker.adapter?.notifyDataSetChanged()
                        }
                    })
                rvFilePicker?.adapter = imageAdapter
                rvFilePicker?.itemAnimator = DefaultItemAnimator()
            }
            REQUEST_IMAGE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val uri = data!!.getParcelableExtra<Uri>("path")
                    try {
                        // You can update this bitmap to your server
                        val bitmap = MediaStore.Images.Media.getBitmap(activity!!.contentResolver, uri)
                        val dataImage = PartyAddImage()
                        dataImage.imagepathLand = uri?.path
                        dataImage.imagenameLand = ""
                        adapterTakePictureSubject.updateDateAfterClickImage(mpositionImageClick!!, dataImage)
                        adapterTakePictureSubject.notifyDataSetChanged()

                        //adapterTakePictureLand.getencodeImage(uri)
                        /*val dataImage = DataImagePhoto()
                        dataImage.imageEncode = getEncoded64ImageStringFromBitmap(bitmap)
                        dataImage.imageName = ""
                        adapterTakePictureLand.addItems(dataImage)
                        adapterTakePictureLand.notifyDataSetChanged()*/

                        // loading profile image from local cache
                        //loadProfile(uri.toString())
                        Log.d(TAG, "Image cache path: $uri")
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

    private fun showDialogYearOnly() {
        val today = Calendar.getInstance()
        val builder = MonthPickerDialog.Builder(
            activity!!,
            MonthPickerDialog.OnDateSetListener { selectedMonth, selectedYear ->
                /* Log.d(TAG, "selectedMonth : $selectedMonth selectedYear : $selectedYear")
                 Toast.makeText(activity, "Date set with month$selectedMonth year $selectedYear", Toast.LENGTH_SHORT).show()*/
                year?.setDateOfBirth(selectedYear, selectedMonth, today.get(Calendar.DAY_OF_MONTH))
                tvthnDomisil.text = selectedYear.toString()
                calculateAge()
            },
            today.get(Calendar.YEAR),
            today.get(Calendar.MONTH)
        )

        builder.setActivatedYear(today.get(Calendar.YEAR))
            .setTitle("Tahun pertama tinggal")
            .setMinYear(today.get(Calendar.YEAR) - 100)
            .setMaxYear(today.get(Calendar.YEAR))
            .setOnMonthChangedListener { selectedMonth ->
                Log.d(TAG, "Selected month : $selectedMonth")
            }
            .setOnYearChangedListener { selectedYear ->
                Log.d(TAG, "Selected year : $selectedYear")
            }
            .showYearOnly()
            .build()
            .show()
    }

    @SuppressLint("SetTextI18n")
    private fun calculateAge() {
        year?.calcualteYear()
        tvtotalTahunDomisil.text = "${year?.result}"
    }

    private fun getPresenter(): Form2DPPTPresenter? {
        form2DPPTPresenter = Form2DPPTPresenter()
        form2DPPTPresenter.onAttach(this)
        return form2DPPTPresenter
    }

    private fun validate(): Boolean {
        val valid: Boolean
        val sweetAlretLoading = SweetAlertDialog(activity!!, SweetAlertDialog.WARNING_TYPE)

        sweetAlretLoading.setCancelable(false)
        sweetAlretLoading.confirmText = "OK"
        sweetAlretLoading.setConfirmClickListener { sDialog ->
            sDialog?.let { if (it.isShowing) it.dismiss() }
        }
//        if (ednomorIdentitas.text.toString().trim().isEmpty()
//                || sendIdSourceIdentitas == 0
//                || tvtotalTahunDomisil.text.toString().trim().isEmpty()) {
//
//            sweetAlretLoading.titleText = "LENGKAPI DATA"
//            sweetAlretLoading.show()
//            valid = false
//
//        } else

        if (edNPWP.text.toString().trim().isNotEmpty() && edNPWP.text!!.trim().length < 20) {
            sweetAlretLoading.titleText = "LENGKAPI NOMOR NPWP"
            sweetAlretLoading.show()
            valid = false
        } else if (selectedOwnership.isNullOrEmpty() || (selectedOwnership.contains("Data Pemilik Tanah") && landOwnershipData.isNullOrEmpty())) {
            sweetAlretLoading.titleText = "LENGKAPI DATA"
            sweetAlretLoading.show()
            valid = false
        }
//        else if (spinnerSumberIdentitas.text.toString() == "KTP" && ednomorIdentitas.text!!.trim().length < 16) {
//            sweetAlretLoading.titleText = "LENGKAPI NOMOR KTP"
//            sweetAlretLoading.show()
//            valid = false
//        }
//        else if (spinnerSumberIdentitas.text.toString() == "SIM" && ednomorIdentitas.text!!.trim().length < 12) {
//            sweetAlretLoading.titleText = "LENGKAPI NOMOR SIM"
//            sweetAlretLoading.show()
//            valid = false
//        }
        else {
            valid = true
        }

        return valid
    }

    override fun onsuccessgetdaftarSumberIdentitas(responseDataSumberIdentitas: MutableList<ResponseDataSumberIdentitas>) {
        /*for (row in responseDataSumberIdentitas) {
            kategoriIdIdentitas.add(row.identitySourceId!!)
            kategoriIdentitas.add(row.identitySourceName.toString())
        }

        Log.d("hasil", kategoriIdIdentitas.toString())

        val adaptersumberIdentitas = ArrayAdapter<String>(activity!!,
                R.layout.item_spinner, kategoriIdentitas)
        adaptersumberIdentitas.setDropDownViewResource(android.R.layout.simple_expandable_list_item_2)
        spinnerSumberIdentitas.setAdapter(adaptersumberIdentitas)*/
    }

    override fun onResume() {
        super.onResume()
        /* prev */
        btnprevsubjek2?.setOnClickListener(this)
        /* next */
        btnnextsubjek2?.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        /* prev */
        btnprevsubjek2?.setOnClickListener(null)
        /* next */
        btnnextsubjek2?.setOnClickListener(null)
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btnnextsubjek2) {
            hideKeyboard()
            dataFormSubjek2.clear()
            Utils.deletedataform2(activity!!)

            if (validate()) {
                val realmList: RealmList<String?> = RealmList<String?>()
                realmList.addAll(filePaths)
                val realmListName: RealmList<String?> = RealmList<String?>()
                realmListName.addAll(fileName)

                dataFormSubjek2.add(
                    DataFormSubjek2(
                        "",
                        "",
                        edNPWP.text.toString().trim(),
                        tvtotalTahunDomisil.text.toString().trim(),
                        selectedOwnership.toString(),
                        landOwnershipData,
                        realmList,
                        realmListName,
                        adapterTakePictureSubject.returnDataPhoto()
                    )
                )
                val jsonBBM = Gson().toJson(dataFormSubjek2)
                Utils.savedataform2(jsonBBM, activity!!)
                if (mListener != null)
                    mListener!!.onNextPressed(this)
            }

            /*if (mListener != null)
                mListener!!.onNextPressed(this)*/

        } else if (view.id == R.id.btnprevsubjek2) {
            if (mListener != null)
                mListener!!.onBackPressed(this)
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnStepTwoListener) {
            mListener = context
        } else {
            throw RuntimeException(("$context must implement OnFragmentInteractionListener"))
        }
    }

    override fun onDetach() {
        super.onDetach()
        getPresenter()?.onDetach()
        mListener = null
    }


    fun getFileName(uri: Uri): String? {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor: Cursor? = requireActivity().contentResolver.query(uri, null, null, null, null)
            cursor.use { cursor ->
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result?.substring(cut + 1)
            }
        }
        return result
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    interface OnStepTwoListener {
        fun onBackPressed(fragment: androidx.fragment.app.Fragment)
        fun onNextPressed(fragment: androidx.fragment.app.Fragment)
    }

    companion object {
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"
        internal const val TAG = "FormSubjek2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        fun newInstance(param1: String, param2: String): FormSubjek2 {
            val fragment = FormSubjek2()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

    override fun OnItemImageClick(position: Int) {
        mpositionImageClick = position
        onProfileImageClick()
    }

    override fun onClearImage(position: Int) {

    }


}