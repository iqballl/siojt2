package mki.siojt2.fragment.form_subjek

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mki.siojt2.R
import mki.siojt2.base.BaseFragment

class FormSubjek4 : BaseFragment(){

    companion object {
        internal const val TAG = "FormSubjek4"

        fun newInstance(): FormSubjek4 {
            val args = Bundle()
            val fragment = FormSubjek4()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_form_subjek_4, container, false)

    }

    override fun setUp(view: View) {


    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnStepFourListener {
        fun onBackPressed(fragment: androidx.fragment.app.Fragment)
        fun onNextPressed(fragment: androidx.fragment.app.Fragment)
    }
}