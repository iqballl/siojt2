package mki.siojt2.fragment.form_subjek.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface Form1MVVPPresenter : MVPPresenter {

    fun getregioBirthPlace(searchCode: String, limit: String)
    fun getregionProvince(searchCode: String)
    fun getregionCity(provinceId: String, searchCode: String, limit: String)
    fun getregionKecamatan(searchCode: String, regencyCode: String, limit: String)
    fun getregionKelurahan(searchCode: String, districtCode: String, limit: String)


    fun getdaftarPekerjaan()
}