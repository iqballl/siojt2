package mki.siojt2.fragment.form_subjek

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.text.InputFilter
import android.text.InputType
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import com.whiteelephant.monthpicker.MonthPickerDialog
import android.util.Log
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.gson.Gson
import cn.pedant.SweetAlert.SweetAlertDialog
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_form_subjek_5.*
import mki.siojt2.fragment.form_subjek.presenter.Form5DPPTPresenter
import mki.siojt2.fragment.form_subjek.view.FormSubjek5View
import mki.siojt2.model.master_data_form_tanah.ResponseDataListPenguasaanTanah
import mki.siojt2.model.data_form_subjek.DataFormSubjek2
import mki.siojt2.model.localsave.Session
import mki.siojt2.model.localsave.Subjek2
import mki.siojt2.model.master_data_form_tanah.ResponseDataListPenguasaanTanahWithOutParam
import mki.siojt2.model.master_data_subjek.ResponseDataSumberIdentitas
import mki.siojt2.model.master_data_subjek.ResponseDataSumberIdentitasWithOutParam
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.Utils
import java.util.*
import mki.siojt2.utils.extension.YearCalculation
import kotlin.collections.ArrayList

class FormSubjek5 : BaseFragment(), FormSubjek5View, View.OnClickListener {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    private var mListener: OnStepFiveListener? = null

    private var year: YearCalculation? = null

    //var kategoriIdentitas = arrayListOf("KTP", "SIM", "Paspor")
    private var kategoripemegangatasProperti: ArrayList<String> = ArrayList()
    private var kategoriIdpemegangatasProperti: ArrayList<Int> = ArrayList()
    private var kategoriIdentitas: ArrayList<String> = ArrayList()
    private var kategoriIdIdentitas: ArrayList<Int> = ArrayList()

    lateinit var form5DPPTPresenter: Form5DPPTPresenter
//    private var sendIdSourceIdentitas: Int = 0
    private var sendIdPenguasaanTanah: Int = 0

    private var dataFormSubjek2: MutableList<DataFormSubjek2> = ArrayList()

    lateinit var realm: Realm
    private lateinit var session: Session

    /*var valuepemegangatasProperti = arrayListOf("Pemegang pengelolaan", "Nadzir untuk tanah wakaf",
            "Pemilik tanah bekas milik adat", "Masyarakat hukum adat", "Pihak yang menguasai tanah negara dengan itikad baik",
            "Pemegang dasar penguasaan atas tanah", "Pemilik bangunan, tanaman, atau benda lain yang berkaitan dengan tanah")*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }

        realm = RealmController.with(this).realm
        realm.isAutoRefresh
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_subjek_5, container, false)

    }

    override fun setUp(view: View) {

        setformatterNPWP(edNPWP)

        /* data offline penguasaan atas tanah */
        val resultsRealmPenguasaanAtasTanah =
                realm.where(ResponseDataListPenguasaanTanahWithOutParam::class.java)
                        .notEqualTo("partyTypeId", "1")
                        .findAll()
        val dataRealmPenguasaanAtasTanah: MutableList<ResponseDataListPenguasaanTanahWithOutParam> = realm.copyFromRealm(resultsRealmPenguasaanAtasTanah)

        getDataSubjek2()

        val adapterPenguasaanAtasTanah = ArrayAdapter<ResponseDataListPenguasaanTanahWithOutParam>(activity!!, R.layout.item_spinner, dataRealmPenguasaanAtasTanah)
        spinnerKepemilikanTanah.setAdapter(adapterPenguasaanAtasTanah)
        spinnerKepemilikanTanah.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            val dataSpinner = adapterView.getItemAtPosition(position) as ResponseDataListPenguasaanTanahWithOutParam
            sendIdPenguasaanTanah = dataSpinner.getpartyTypeId()!!.toInt()
            //Toast.makeText(activity!!, sendIdSourceIdentitas.toString(), Toast.LENGTH_SHORT).show()
        }

        /* data offline sumber identitas */
        val results = realm.where(ResponseDataSumberIdentitasWithOutParam::class.java).findAll()
        val data: MutableList<ResponseDataSumberIdentitasWithOutParam> = realm.copyFromRealm(results)

        val adapterSumberIdentitas = ArrayAdapter(activity!!, R.layout.item_spinner, data)
//        spinnerSumberIdentitas.setAdapter(adapterSumberIdentitas)
//        spinnerSumberIdentitas.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
//            val valueSpinner = adapterView.getItemAtPosition(position).toString()
//            val dataSpinner = adapterView.getItemAtPosition(position) as ResponseDataSumberIdentitasWithOutParam
//            sendIdSourceIdentitas = dataSpinner.getidentitySourceId()!!
//
//            when (valueSpinner) {
//                "Lain-Nya" -> {
//                    ednomorIdentitas2.filters = arrayOf<InputFilter>()
//                    llsumberidentitasLain2.visibility = View.VISIBLE
//                    edsumberIdentitasLain2.text = null
//                    ednomorIdentitas2.text = null
//                    ednomorIdentitas2.inputType = InputType.TYPE_CLASS_TEXT
//                }
//                "KTP" -> {
//                    ednomorIdentitas2.filters = arrayOf<InputFilter>()
//                    setEditTextMaxLength(ednomorIdentitas2, 16)
//                    llsumberidentitasLain2.visibility = View.GONE
//                    edsumberIdentitasLain2.text = null
//                    ednomorIdentitas2.text = null
//                    ednomorIdentitas2.inputType = InputType.TYPE_CLASS_NUMBER
//                }
//                "SIM" -> {
//                    ednomorIdentitas2.filters = arrayOf<InputFilter>()
//                    setEditTextMaxLength(ednomorIdentitas2, 12)
//                    llsumberidentitasLain2.visibility = View.GONE
//                    edsumberIdentitasLain2.text = null
//                    ednomorIdentitas2.text = null
//                    ednomorIdentitas2.inputType = InputType.TYPE_CLASS_NUMBER
//                }
//                "Paspor" -> {
//                    ednomorIdentitas2.filters = arrayOf<InputFilter>()
//                    setEditTextMaxLength(ednomorIdentitas2, 10)
//                    llsumberidentitasLain2.visibility = View.GONE
//                    edsumberIdentitasLain2.text = null
//                    ednomorIdentitas2.text = null
//                    ednomorIdentitas2.inputType = InputType.TYPE_CLASS_TEXT
//                }
//                else -> {
//                    ednomorIdentitas2.filters = arrayOf<InputFilter>()
//                    llsumberidentitasLain2.visibility = View.GONE
//                    edsumberIdentitasLain2.text = null
//                    ednomorIdentitas2.text = null
//                    ednomorIdentitas2.inputType = InputType.TYPE_CLASS_NUMBER
//
//                }
//            }
//        }

        year = YearCalculation()
        tvtitlethnDomisil.setOnClickListener {
            showDialogYearOnly()
        }
    }

    private fun getDataSubjek2() {
        session = Session(context)
        if (session.id != "" && session.status != "" && session.from == "2") {
            val results2 = realm.where(Subjek2::class.java).equalTo("SubjekIdTemp", session.id.toInt()).findFirst()
            if (results2 != null) {
                tampilkanData(results2)
                Log.d("results", results2.toString())
            } else {
                Log.e("data", "kosong")
            }
        }
    }

    private fun tampilkanData(results2: Subjek2) {
        spinnerKepemilikanTanah.setText(results2.subjek2PenguasaanTanahName)
        sendIdPenguasaanTanah = results2.subjek2PenguasaanTanahId

//        spinnerSumberIdentitas.setText(results2.subjek2IdentitySourceName)
//        sendIdSourceIdentitas = results2.subjek2IdentitySourceId
        ednomorIdentitas2.setText(results2.subjek2IdentityNumber)

//        if (results2.subjek2IdentitySourceOther.isNotEmpty()) {
//            llsumberidentitasLain2.visibility = View.VISIBLE
//            edsumberIdentitasLain2.setText(results2.subjek2IdentitySourceOther)
//        } else {
//            llsumberidentitasLain2.visibility = View.VISIBLE
//        }

//        when (spinnerSumberIdentitas.text.toString()) {
//            "Lain-Nya" -> {
//                ednomorIdentitas2.filters = arrayOf<InputFilter>()
//                llsumberidentitasLain2.visibility = View.VISIBLE
//                ednomorIdentitas2.inputType = InputType.TYPE_CLASS_TEXT
//            }
//            "KTP" -> {
//                ednomorIdentitas2.filters = arrayOf<InputFilter>()
//                setEditTextMaxLength(ednomorIdentitas2, 16)
//                llsumberidentitasLain2.visibility = View.GONE
//                ednomorIdentitas2.inputType = InputType.TYPE_CLASS_NUMBER
//            }
//            "SIM" -> {
//                ednomorIdentitas2.filters = arrayOf<InputFilter>()
//                setEditTextMaxLength(ednomorIdentitas2, 12)
//                llsumberidentitasLain2.visibility = View.GONE
//                ednomorIdentitas2.inputType = InputType.TYPE_CLASS_NUMBER
//            }
//            "Paspor" -> {
//                ednomorIdentitas2.filters = arrayOf<InputFilter>()
//                setEditTextMaxLength(ednomorIdentitas2, 10)
//                llsumberidentitasLain2.visibility = View.GONE
//                ednomorIdentitas2.inputType = InputType.TYPE_CLASS_TEXT
//            }
//            else -> {
//                ednomorIdentitas2.filters = arrayOf<InputFilter>()
//                llsumberidentitasLain2.visibility = View.GONE
//                ednomorIdentitas2.inputType = InputType.TYPE_CLASS_NUMBER
//
//            }
//        }

        edNPWP.setText(results2.subjek2NPWP)
        tvtotalTahunDomisil.text = results2.subjek2YearStayBegin.toString()
    }

    private fun showDialogYearOnly() {
        val today = Calendar.getInstance()

        val builder = MonthPickerDialog.Builder(activity!!, MonthPickerDialog.OnDateSetListener { selectedMonth, selectedYear ->
            /* Log.d(TAG, "selectedMonth : $selectedMonth selectedYear : $selectedYear")
             Toast.makeText(activity, "Date set with month$selectedMonth year $selectedYear", Toast.LENGTH_SHORT).show()*/
            year?.setDateOfBirth(selectedYear, selectedMonth, today.get(Calendar.DAY_OF_MONTH))
            tvthnDomisil.text = selectedYear.toString()
            calculateAge()
        }, today.get(Calendar.YEAR), today.get(Calendar.MONTH))

        builder.setActivatedYear(today.get(Calendar.YEAR))
                .setTitle("Lama tahun tinggal")
                .setMinYear(today.get(Calendar.YEAR) - 100)
                .setMaxYear(today.get(Calendar.YEAR))
                .setOnMonthChangedListener { selectedMonth ->
                    Log.d(TAG, "Selected month : $selectedMonth")
                    // Toast.makeText(MainActivity.this, " Selected month : " + selectedMonth, Toast.LENGTH_SHORT).show();
                }
                .setOnYearChangedListener { selectedYear ->
                    Log.d(TAG, "Selected year : $selectedYear")
                    // Toast.makeText(MainActivity.this, " Selected year : " + selectedYear, Toast.LENGTH_SHORT).show();
                }
                .showYearOnly()
                .build()
                .show()
    }

    @SuppressLint("SetTextI18n")
    private fun calculateAge() {
        year?.calcualteYear()
        //Toast.makeText(activity, "click the resulted button" + year?.result, Toast.LENGTH_SHORT).show()
        tvtotalTahunDomisil.text = "${year?.result}"
    }

    private fun getPresenter(): Form5DPPTPresenter? {
        form5DPPTPresenter = Form5DPPTPresenter()
        form5DPPTPresenter.onAttach(this)
        return form5DPPTPresenter
    }

    private fun validate(): Boolean {

        val sweetAlretLoading = SweetAlertDialog(activity!!, SweetAlertDialog.WARNING_TYPE)
        sweetAlretLoading.setCancelable(false)
        sweetAlretLoading.confirmText = "OK"
        sweetAlretLoading.setConfirmClickListener { sDialog ->
            sDialog?.let { if (it.isShowing) it.dismiss() }
        }
        val valid: Boolean
        if (ednomorIdentitas2.text.toString().trim().isEmpty() || sendIdPenguasaanTanah == 0 ||
                tvtotalTahunDomisil.text.toString().trim().isEmpty()) {
            sweetAlretLoading.titleText = "LENGKAPI DATA"
            sweetAlretLoading.show()
            valid = false

        }
//        else if (spinnerSumberIdentitas.text.toString() == "Lain-Nya" && edsumberIdentitasLain2.text.toString().isEmpty()) {
//            sweetAlretLoading.titleText = "LENGKAPI DATA"
//            sweetAlretLoading.show()
//            valid = false
//        }
        else if (ednomorIdentitas2.text!!.trim().length < 16) {
            sweetAlretLoading.titleText = "LENGKAPI NOMOR KTP"
            sweetAlretLoading.show()
            valid = false
        }
//        else if (spinnerSumberIdentitas.text.toString() == "SIM" && ednomorIdentitas2.text!!.trim().length < 12) {
//            sweetAlretLoading.titleText = "LENGKAPI NOMOR SIM"
//            sweetAlretLoading.show()
//            valid = false
//        }
        else {
            valid = true
        }

        /*else if (edNPWP.text.toString().trim().isNotEmpty() && edNPWP.text!!.trim().length < 20) {
            sweetAlretLoading.titleText = "LENGKAPI NOMOR NPWP"
            sweetAlretLoading.show()

            valid = false
        }*/

        return valid
    }


    override fun onsuccessgetdaftarPenguasaanTanah(responseDataListPenguasaanTanah: MutableList<ResponseDataListPenguasaanTanah>) {
        /*for (row in responseDataListPenguasaanTanah) {
            if (row.partyTypeId != 1) {
                kategoriIdpemegangatasProperti.add(row.partyTypeId!!)
                kategoripemegangatasProperti.add(row.partyTypeName.toString())

            }

        }

*//*        for (i in 0..responseDataListPenguasaanTanah.size) {
            if (responseDataListPenguasaanTanah[i].partyTypeId != 1) {
                kategoripemegangatasProperti[i] = (responseDataListPenguasaanTanah[i].partyTypeName.toString())
            }
            Log.d("bancet", kategoripemegangatasProperti.toString())
        }*//*


        val adapterKepemilikanTanah = ArrayAdapter<String>(activity!!,
                R.layout.item_spinner, kategoripemegangatasProperti)
        adapterKepemilikanTanah.setDropDownViewResource(android.R.layout.simple_expandable_list_item_2)
        spinnerKepemilikanTanah.setAdapter(adapterKepemilikanTanah)*/
    }

    override fun onsuccessgetdaftarSumberIdentitas2(responseDataSumberIdentitas: MutableList<ResponseDataSumberIdentitas>) {
        /*for (row in responseDataSumberIdentitas) {
            kategoriIdIdentitas.add(row.identitySourceId!!)
            kategoriIdentitas.add(row.identitySourceName.toString())
        }

        //Log.d("hasil", kategoriIdIdentitas.toString())

        val adaptersumberIdentitas = ArrayAdapter<String>(activity!!,
                R.layout.item_spinner, kategoriIdentitas)
        adaptersumberIdentitas.setDropDownViewResource(android.R.layout.simple_expandable_list_item_2)
        spinnerSumberIdentitas.setAdapter(adaptersumberIdentitas)*/
    }

    override fun onResume() {
        super.onResume()
        /* prev */
        btnprevsubjek5.setOnClickListener(this)
        /* next */
        btnnextsubjek5.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        /* prev */
        btnprevsubjek5?.setOnClickListener(null)
        /* next */
        btnnextsubjek5?.setOnClickListener(null)
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btnnextsubjek5) {
            hideKeyboard()
            dataFormSubjek2.clear()
            Utils.deletedataform2(activity!!)
            if (validate()) {
//                dataFormSubjek2.add(DataFormSubjek2(
//                        sendIdPenguasaanTanah.toString(),
//                        spinnerKepemilikanTanah.text.toString().trim(),
////                        sendIdSourceIdentitas.toString(),
////                        spinnerSumberIdentitas.text.toString(),
////                        edsumberIdentitasLain2.text.toString(),
////                        ednomorIdentitas2.text.toString().trim(),
//                        edNPWP.text.toString().trim(),
//                        tvtotalTahunDomisil.text.toString().trim()
//                ))

                val jsonBBM = Gson().toJson(dataFormSubjek2)
                Utils.savedataform2(jsonBBM, activity!!)
                Handler().postDelayed({
                    //Toast.makeText(activity!!, "Clicked", Toast.LENGTH_SHORT).show()
                    if (mListener != null)
                        mListener!!.onNextPressed(this)

                }, 1000)

            }
        } else if (view.id == R.id.btnprevsubjek5) {
            if (mListener != null)
                mListener!!.onBackPressed(this)
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnStepFiveListener) {
            mListener = context
        } else {
            throw RuntimeException(("$context must implement OnFragmentInteractionListener"))
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
        getPresenter()?.onDetach()
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    interface OnStepFiveListener {
        fun onBackPressed(fragment: androidx.fragment.app.Fragment)
        fun onNextPressed(fragment: androidx.fragment.app.Fragment)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"
        internal const val TAG = "FormSubjek5"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): FormSubjek5 {
            val fragment = FormSubjek5()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }


}// Required empty public constructor