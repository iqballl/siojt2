package mki.siojt2.fragment.form_subjek

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import mki.siojt2.R
import androidx.appcompat.app.AlertDialog
import android.view.*
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_form_subjek_4.view.*
import mki.siojt2.utils.extension.OnSingleClickListener
import android.view.ViewGroup
import android.view.WindowManager
import mki.siojt2.utils.Utils

class DialogSuccessAddSubjek : androidx.fragment.app.DialogFragment() {

    lateinit var mView: View
    private var currentttitleDialog: String? = null

    companion object {
        private const val ARG_PARAM1 = "title_dialog"
        fun newInstance(param1: String): DialogSuccessAddSubjek {
            val fragment = DialogSuccessAddSubjek()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            val mArgs = arguments
            currentttitleDialog = mArgs?.getString(ARG_PARAM1)
        }
    }

    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val dialog = super.onCreateDialog(savedInstanceState)

        //val dialogBuilder = AlertDialog.Builder(ContextThemeWrapper(this, R.style.full_screen_dialog))
        val dialogBuilder = AlertDialog.Builder(activity!!)
        val inflater = LayoutInflater.from(context)
        val dialogView = inflater.inflate(R.layout.fragment_form_subjek_4, null)
        dialogBuilder.setView(dialogView)

        initSubViews(dialogView)

        // request a window without the title
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        return dialogBuilder.create()
    }


    private fun initSubViews(rootView: View) {
        val btngotoMainMenu = rootView.btnsuccessaddData
        val tvtitledialogTransaksi = rootView.tvsuccessaddData

        currentttitleDialog?.let {
            tvtitledialogTransaksi.text = it
        }

        btngotoMainMenu.setOnClickListener(object : OnSingleClickListener() {
            override fun onSingleClick(v: View) {
                dismiss()
                Utils.deletedataform1(activity!!)
                Utils.deletedataform3(activity!!)
                Utils.deletedataform3(activity!!)
                activity!!.finish()
            }

        })

    }

    fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }


    override fun onStart() {
        super.onStart()

        //set transparent background
        /*
        //window!!.setBackgroundDrawableResource(android.R.color.transparent)
        window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        */

        val window = dialog!!.window
        if (window != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog!!.window!!.setLayout(width, height)
        }
        //disable buttons from dialog
        val alertDialog = dialog as AlertDialog
        alertDialog.getButton(Dialog.BUTTON_POSITIVE).isEnabled = false
        alertDialog.getButton(Dialog.BUTTON_NEGATIVE).isEnabled = false
        alertDialog.getButton(Dialog.BUTTON_NEUTRAL).isEnabled = false
    }


    /* override fun onResume() {
         super.onResume()
         val window = dialog.window
         val layoutParams = dialog.window!!.attributes
         layoutParams.dimAmount = 0f
         window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
         window.setGravity(Gravity.CENTER)
         dialog.window!!.attributes = layoutParams
     }*/

}