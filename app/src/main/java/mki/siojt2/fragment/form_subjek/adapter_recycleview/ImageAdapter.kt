package mki.siojt2.fragment.form_subjek.adapter_recycleview

import android.content.Context
import android.database.Cursor

import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView

import android.net.Uri
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView

import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import mki.siojt2.R

import java.io.File
import java.util.ArrayList
import android.provider.OpenableColumns
import android.widget.ImageView


class ImageAdapter(
    context: Context,
    paths: ArrayList<Uri>,
    var fileName: ArrayList<String>,
    imageAdapterListener: ImageAdapterListener
) :
    RecyclerView.Adapter<ImageAdapter.FileViewHolder>() {
    private val imageAdapterListener: ImageAdapterListener

    interface ImageAdapterListener {
        fun onItemClick(uri: Uri?)
        fun onItemDelete(file: Uri?)
    }

    private val paths: ArrayList<Uri>
    private val context: Context
    private var imageSize = 0
    private fun setColumnNumber(context: Context, columnNum: Int) {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val metrics = DisplayMetrics()
        wm.defaultDisplay.getMetrics(metrics)
        val widthPixels = metrics.widthPixels
        imageSize = widthPixels / columnNum
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FileViewHolder {
        val itemView: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_filepicker, parent, false)
        return FileViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: FileViewHolder, position: Int) {
        val path: Uri = paths[position]
        Glide.with(context)
            .load(path)
            .apply(
                RequestOptions.centerCropTransform()
                    .dontAnimate()
                    .override(imageSize, imageSize)
                    .placeholder(droidninja.filepicker.R.drawable.image_placeholder)
            )
            .thumbnail(0.5f)
            .into(holder.imageView)
        holder.tvFilename.text = fileName[position]
        holder.itemView.setOnClickListener {
            imageAdapterListener.onItemClick(path)
        }
        holder.btnClose.setOnClickListener {
            imageAdapterListener.onItemDelete(path)
        }
    }

    override fun getItemCount(): Int {
        return paths.size
    }

    class FileViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: AppCompatImageView = itemView.findViewById(R.id.iv_photo)
        var tvFilename: TextView = itemView.findViewById(R.id.tvFilename)
        var btnClose: ImageView = itemView.findViewById(R.id.btnClose)
    }

    init {
        this.context = context
        this.paths = paths
        this.imageAdapterListener = imageAdapterListener
        setColumnNumber(context, 3)
    }
}