package mki.siojt2.fragment.form_subjek.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface Form5MVVPPresenter : MVPPresenter {

    fun getdaftarPenguasaanTanah()
    fun getdaftarSumberIdentitas()
}