package mki.siojt2.fragment.form_subjek.presenter

import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.fragment.form_subjek.view.FormSubjek1View
import mki.siojt2.utils.rxJava.RxUtils

class Form1DPPTPresenter : BasePresenter(), Form1MVVPPresenter {

    private var disposables: CompositeDisposable = CompositeDisposable()

    override fun getdaftarPekerjaan() {
        disposables.add(
                dataManager.getdaftarPekerjaan()
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdaftarPekerjaan(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    override fun getregioBirthPlace(searchCode: String, limit: String) {
        disposables.add(
                dataManager.getdaftarBirthPlace(searchCode, limit)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetregioBirthPlace(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }


    override fun getregionProvince(searchCode: String) {
        disposables.add(
                dataManager.getdaftarProvinsi(searchCode)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetregionProvince(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    override fun getregionCity(provinceId: String, searchCode: String, limit: String) {
        disposables.add(
                dataManager.getdaftarKotaKabupaten(provinceId, searchCode, limit)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetregionCity(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    override fun getregionKecamatan(searchCode: String, regencyCode: String, limit: String) {
        disposables.add(
                dataManager.getdaftarKecamatan(searchCode, regencyCode, limit)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetregionKecamatan(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    override fun getregionKelurahan(searchCode: String, districtCode: String, limit: String) {
        disposables.add(
                dataManager.getdaftarKelurahan(searchCode, districtCode, limit)
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetregionKelurahan(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    private fun view(): FormSubjek1View {
        return getView() as FormSubjek1View
    }
}