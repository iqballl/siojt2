package mki.siojt2.fragment.form_subjek.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.master_data_form_tanah.ResponseDataListPenguasaanTanah
import mki.siojt2.model.master_data_subjek.ResponseDataSumberIdentitas

interface FormSubjek5View : MvpView {
    fun onsuccessgetdaftarPenguasaanTanah(responseDataListPenguasaanTanah: MutableList<ResponseDataListPenguasaanTanah>)
    fun onsuccessgetdaftarSumberIdentitas2(responseDataSumberIdentitas: MutableList<ResponseDataSumberIdentitas>)
}