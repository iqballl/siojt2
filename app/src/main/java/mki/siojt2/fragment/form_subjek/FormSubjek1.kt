package mki.siojt2.fragment.form_subjek

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_form_subjek_1.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.fragment.form_subjek.view.FormSubjek1View
import java.util.*
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Log
import mki.siojt2.fragment.form_subjek.presenter.Form1DPPTPresenter
import kotlin.collections.ArrayList
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.gson.Gson
import io.realm.*
import kotlinx.android.synthetic.main.fragment_form_objek_add_tanah_1.*
import kotlinx.android.synthetic.main.fragment_form_subjek_1.autocomplateTVKecamatan
import kotlinx.android.synthetic.main.fragment_form_subjek_1.autocomplateTVKelurahan
import kotlinx.android.synthetic.main.fragment_form_subjek_1.edBlok
import kotlinx.android.synthetic.main.fragment_form_subjek_1.edJalan
import kotlinx.android.synthetic.main.fragment_form_subjek_1.edKomplek
import kotlinx.android.synthetic.main.fragment_form_subjek_1.edNomor
import kotlinx.android.synthetic.main.fragment_form_subjek_1.edRW
import kotlinx.android.synthetic.main.fragment_form_subjek_1.edRt
import kotlinx.android.synthetic.main.fragment_form_subjek_1.edkodePos
import kotlinx.android.synthetic.main.fragment_form_subjek_1.llkota
import kotlinx.android.synthetic.main.item_spinner.view.*
import mki.siojt2.adapter.autocomplate_adapter.FilterableRealmAdapterPropinsi
import mki.siojt2.adapter.autocomplate_adapter.FilterableRealmAdapterKecamatan
import mki.siojt2.adapter.autocomplate_adapter.FilterableRealmAdapterKelurahan
import mki.siojt2.adapter.autocomplate_adapter.FilterableRealmAdapterKota
import mki.siojt2.adapter.autocomplate_adapter.FilterableRealmAdapterKotaKelahiran
import mki.siojt2.model.data_form_subjek.DataFormSubjek1
import mki.siojt2.model.localsave.AhliWaris
import mki.siojt2.model.localsave.PartyAdd
import mki.siojt2.model.localsave.Session
import mki.siojt2.model.localsave.Subjek2
import mki.siojt2.model.master_data_subjek.ResponseDataPekerjaan
import mki.siojt2.model.master_data_subjek.ResponseDataPekerjaanWithOutParam
import mki.siojt2.model.region.*
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.Utils


class FormSubjek1 : BaseFragment(), FormSubjek1View, View.OnClickListener {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    private var mListener: OnStepOneListener? = null

    lateinit var mCalendar: Calendar
    private var mYear: Int = 0
    private var mMonth: Int = 0
    private var mDay: Int = 0
    val mHour: Int = 0
    val mMinute: Int = 0

    var check: Int = 0

    private lateinit var form1DPPTPresenter: Form1DPPTPresenter

    private val dateTemplate = "dd MMMM yyyy"
    private val dateTemplate2 = "yyyy-MM-dd"

    //private val valuespinnerpekerjaan = mutableListOf("Karyawan Swasta", "Pegawai Negeri", "Wiraswata", "Ibu Rumah Tangga")
    private val valuespinnerpekerjaan: MutableList<String> = ArrayList()

    private var idPekerjaan: ArrayList<String?> = ArrayList()
    private var idBirthplace: Array<String?> = emptyArray()
    private var idProvince: Array<String?> = emptyArray()
    private var idCity: Array<String?> = emptyArray()
    private var idKecamatan: Array<String?> = emptyArray()
    private var idKelurahan: Array<String?> = emptyArray()

    private var sendIdPekerjaan: String = ""
    private var sendidBirthPlace: String = ""
    private var sendIdProvince: String = ""
    private var sendIdCity: String = ""
    private var sendIdKecamatan: String = ""
    private var sendIdKelurahan: String = ""

//    private var idJenisKepemilikan: String = ""

    private var senddateBirthPicker: String = ""

    private var dataFormSubjek1: MutableList<DataFormSubjek1> = ArrayList()

    private var mdateSetListener: DatePickerDialog.OnDateSetListener? = null

    lateinit var realm: Realm
    private lateinit var session: Session
    lateinit var adapterKota: FilterableRealmAdapterKota<ResponseDataCityWithoutParameter>
    lateinit var adapterKecamatan: FilterableRealmAdapterKecamatan<ResponseDataKecamatanWithoutParameter>
    lateinit var adapterKelurahan: FilterableRealmAdapterKelurahan<ResponseDataKelurahanWithoutParameter>

    lateinit var tanggalEdit: String

    private var datepicker: DatePicker? = null
    private var datepicker2: DatePicker? = null

    var adapterPekerjaan: ArrayAdapter<ResponseDataPekerjaanWithOutParam>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }

        session = Session(context)
        realm = RealmController.with(this).realm
        realm.isAutoRefresh

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_subjek_1, container, false)
    }

    override fun setUp(view: View) {
        Log.d("FormSubjek1", "visible")
//        if (mParam1 == "1") {
//            llkepemilikan.visibility = View.GONE
//        }

        /* data offline jenis pekerjaan */
        val results = realm.where(ResponseDataPekerjaanWithOutParam::class.java).findAll()
        val data: MutableList<ResponseDataPekerjaanWithOutParam> = realm.copyFromRealm(results)

        adapterPekerjaan = ArrayAdapter(activity!!, R.layout.item_spinner, data)
        spinnerPekerjaan.setAdapter(adapterPekerjaan)
        spinnerPekerjaan.onItemClickListener = AdapterView.OnItemClickListener { adapterView, _, position, _ ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()

            val dataSpinner = adapterView.getItemAtPosition(position) as ResponseDataPekerjaanWithOutParam
            sendIdPekerjaan = dataSpinner.getliveliHoodId().toString()
            //Toast.makeText(activity!!, sendIdPekerjaan, Toast.LENGTH_SHORT).show()

            /*val valuespinner = parent.getItemAtPosition(position).toString()
            sendIdPekerjaan = idPekerjaan[position]!!.toString()
            Toast.makeText(activity!!, sendIdPekerjaan, Toast.LENGTH_SHORT).show()*/
        }

        // Get Current Date
        mCalendar = Calendar.getInstance(Locale.getDefault())
        mYear = mCalendar.get(Calendar.YEAR)
        mMonth = mCalendar.get(Calendar.MONTH)
        mDay = mCalendar.get(Calendar.DATE)

        mdateSetListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            mYear = year
            mMonth = monthOfYear
            mDay = dayOfMonth

            val calendar2 = Calendar.getInstance(Locale.getDefault())
            calendar2.set(Calendar.YEAR, mYear)
            calendar2.set(Calendar.MONTH, mMonth)
            calendar2.set(Calendar.DATE, mDay)

            tvtisitglLahir.text = DateFormat.format(dateTemplate, calendar2.time)
            senddateBirthPicker = DateFormat.format(dateTemplate2, calendar2.time).toString()
            Toast.makeText(activity, senddateBirthPicker, Toast.LENGTH_SHORT).show()
        }

        lltitletglLahir.setSafeOnClickListener {
            selectedDate()
        }

        edBlok.filters = arrayOf<InputFilter>(InputFilter.AllCaps())

        /*autocomplateTVProvince.addTextChangedListener(textWatcherValidasiRegion)
        autocomplateTVKotaKabupaten.addTextChangedListener(textWatcherValidasiRegion)*/

        autocomplateTVProvince.setOnClickListener {
            autocomplateTVProvince.showDropDown()
        }

        autocomplateTVKotaKabupaten.setOnClickListener {
            autocomplateTVKotaKabupaten.showDropDown()
        }

        autocomplateTVKecamatan.setOnClickListener {
            autocomplateTVKecamatan.showDropDown()
        }

        autocomplateTVKelurahan.setOnClickListener {
            autocomplateTVKelurahan.showDropDown()
        }

        autocomplateTVBirthPlace.setOnClickListener {
            autocomplateTVKelurahan.showDropDown()
        }

        /* birth place*/
        /*autocomplateTVBirthPlace.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                loadbirthPlace(autocomplateTVBirthPlace.text.toString().trim())
            }

        })*/

        /*val autoCompleteAdapter = RealmAutoCompleteAdapter(realm, realm.where(ResponseDataCityWithoutParameter::class.java).findAll())
        testKota.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                autoCompleteAdapter.updateInput(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        rvKota.let {
            it.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
            it.itemAnimator = DefaultItemAnimator()
            it.adapter = autoCompleteAdapter
        }*/

        val adapertKotaKelahiran = FilterableRealmAdapterKotaKelahiran(activity!!,
                realm.where(ResponseDataCityWithoutParameter::class.java).findAll(),
                autocomplateTVBirthPlace)
        autocomplateTVBirthPlace.setAdapter(adapertKotaKelahiran)
        autocomplateTVBirthPlace.showDropDown()
        autocomplateTVBirthPlace.threshold = 1
        autocomplateTVBirthPlace.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            hidekeyboard2()
            val dataSpinner = parent.getItemAtPosition(position) as ResponseDataCityWithoutParameter

            sendidBirthPlace = dataSpinner.getRegencyCode().toString()
            //Toast.makeText(activity!!, sendidBirthPlace, Toast.LENGTH_SHORT).show()
            /*sendidBirthPlace = idBirthplace[position].toString()
            */
        }

        /* Provinsi */
        val adapertPropinsi = FilterableRealmAdapterPropinsi(activity!!,
                realm.where(ResponseDataProvinceWithoutParameter::class.java).findAll(),
                autocomplateTVProvince)

        autocomplateTVProvince.setAdapter(adapertPropinsi)
        autocomplateTVProvince.showDropDown()
        autocomplateTVProvince.threshold = 1
        autocomplateTVProvince.setText("")

        adapterKota = FilterableRealmAdapterKota(activity!!, autocomplateTVKotaKabupaten)
        adapterKecamatan = FilterableRealmAdapterKecamatan(activity!!, autocomplateTVKecamatan)
        adapterKelurahan = FilterableRealmAdapterKelurahan(activity!!, autocomplateTVKelurahan)

        autocomplateTVProvince.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                //loadProvince(autocomplateTVProvince.text.toString().trim())
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                sendIdCity = ""
                sendIdKelurahan = ""
                sendIdKecamatan = ""
                autocomplateTVKotaKabupaten.setText("")
                autocomplateTVKecamatan.setText("")
                autocomplateTVKelurahan.setText("")
            }

            override fun afterTextChanged(editable: Editable) {
                //loadProvince(autocomplateTVProvince.text.toString().trim())
            }
        })

        autocomplateTVProvince.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            hidekeyboard2()
            val dataSpinner = parent.getItemAtPosition(position) as ResponseDataProvinceWithoutParameter
            sendIdProvince = dataSpinner.getProvinceCode().toString()

            autocomplateTVKotaKabupaten.setText("")
            sendIdCity = ""
            val resultRealmKotaByProvince = realm.where(ResponseDataCityWithoutParameter::class.java)
                    .equalTo("regencyProvinceCode", sendIdProvince)
                    .findAll()
            adapterKota.setFilterRealm(resultRealmKotaByProvince)
            autocomplateTVKotaKabupaten.setAdapter(adapterKota)
            //Toast.makeText(activity!!, sendIdProvince, Toast.LENGTH_SHORT).show()

            /* online */
            /*sendIdProvince = idProvince[position].toString()
            //autocomplateTVKotaKabupaten.setText("")
            sendIdCity = ""
            Toast.makeText(activity!!, sendIdProvince, Toast.LENGTH_SHORT).show()*/
        }

        /* Kota */
        autocomplateTVKotaKabupaten.showDropDown()
        autocomplateTVKotaKabupaten.threshold = 1
        autocomplateTVKotaKabupaten.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                //loadCity(autocomplateTVKotaKabupaten.text.toString().trim())
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                /*adapterKecamatan.setRealmFilter(null)
                sendIdCity = ""
                sendIdKecamatan = ""
                autocomplateTVKecamatan.setText("")
                autocomplateTVKecamatan.setAdapter(adapterKecamatan)
                adapterKecamatan.notifyDataSetChanged()*/
                /*autocomplateTVKecamatan.setText("")
                if (autocomplateTVKotaKabupaten.text.toString().trim().isEmpty()) {
                    sendIdCity = ""
                    llkecamatan.visibility = View.GONE
                } else {
                    llkecamatan.visibility = View.VISIBLE
                }*/

                /*else if (sendIdProvince.isEmpty() || sendIdProvince == "") {
                    Toast.makeText(activity!!, "Harap pilih propinsi", Toast.LENGTH_SHORT).show()
                }*/
                sendIdKelurahan = ""
                sendIdKecamatan = ""
                autocomplateTVKecamatan.setText("")
                autocomplateTVKelurahan.setText("")

            }

            override fun afterTextChanged(editable: Editable) {
                //checkData()
                //loadCity(autocomplateTVKotaKabupaten.text.toString().trim())
            }
        })

        autocomplateTVKotaKabupaten.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            hidekeyboard2()
            val dataSpinner = parent.getItemAtPosition(position) as ResponseDataCityWithoutParameter
            sendIdCity = dataSpinner.getRegencyCode().toString()

            /* ------- */
            autocomplateTVKecamatan.setText("")
            sendIdKecamatan = ""
            val resultRealmKecamatanByKota = realm.where(ResponseDataKecamatanWithoutParameter::class.java)
                    .equalTo("districtRegencyCode", sendIdCity)
                    .findAll()
            adapterKecamatan.setRealmFilter(resultRealmKecamatanByKota)
            /*autocomplateTVKecamatan.setAdapter(adapterKecamatan)
            //Toast.makeText(activity!!, sendIdCity, Toast.LENGTH_SHORT).show()
            sendIdCity = idCity[position].toString()
            autocomplateTVKecamatan.setText("")
            sendIdKecamatan = ""*/
        }

        /* Kecematan */
        autocomplateTVKecamatan.showDropDown()
        autocomplateTVKecamatan.threshold = 1
        autocomplateTVKecamatan.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                //loadKecamtan(autocomplateTVKecamatan.text.toString().trim())
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                /*adapterKelurahan.setRealmFilter(null)
                sendIdKelurahan = ""
                sendIdKecamatan = ""
                autocomplateTVKelurahan.setText("")
                autocomplateTVKelurahan.setAdapter(adapterKelurahan)
                adapterKelurahan.notifyDataSetChanged()*/
                /*autocomplateTVKelurahan.setText("")
                if (autocomplateTVKecamatan.text.toString().trim().isEmpty()) {
                    sendIdKecamatan = ""
                    llkelurahan2.visibility = View.GONE
                } else {
                    llkelurahan2.visibility = View.VISIBLE
                }*/
                sendIdKelurahan = ""
                autocomplateTVKelurahan.setText("")
            }

            override fun afterTextChanged(editable: Editable) {
                //checkData()
                //loadKecamtan(autocomplateTVKecamatan.text.toString().trim())
            }
        })

        autocomplateTVKecamatan.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            hidekeyboard2()
            val dataSpinner = parent.getItemAtPosition(position) as ResponseDataKecamatanWithoutParameter
            sendIdKecamatan = dataSpinner.getdistrictCode().toString()
            sendIdKelurahan = ""

            /* -- */
            autocomplateTVKelurahan.setText("")
            val resultRealmKelurahanByKecamatan = realm.where(ResponseDataKelurahanWithoutParameter::class.java)
                    .equalTo("villageDistrictCode", sendIdKecamatan)
                    .findAll()

            adapterKelurahan.setRealmFilter(resultRealmKelurahanByKecamatan)
            autocomplateTVKelurahan.setAdapter(adapterKelurahan)
            //Toast.makeText(activity!!, sendIdKecamatan, Toast.LENGTH_SHORT).show()
            /*sendIdKecamatan = idKecamatan[position].toString()
            autocomplateTVKelurahan.setText("")
            sendIdKelurahan = ""
            Toast.makeText(activity!!, sendIdKecamatan, Toast.LENGTH_SHORT).show()*/
        }

        /* Kelurahan */
        autocomplateTVKecamatan.showDropDown()
        autocomplateTVKecamatan.threshold = 1
        autocomplateTVKelurahan.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                //loadKelurahan(autocomplateTVKelurahan.text.toString().trim())
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                // autocomplateTVKotaKabupaten.setText("")
            }

            override fun afterTextChanged(editable: Editable) {
                //loadKelurahan(autocomplateTVKelurahan.text.toString().trim())
            }
        })

        autocomplateTVKelurahan.showDropDown()
        autocomplateTVKelurahan.threshold = 1
        autocomplateTVKelurahan.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            hidekeyboard2()
            val dataSpinner = parent.getItemAtPosition(position) as ResponseDataKelurahanWithoutParameter
            sendIdKelurahan = dataSpinner.getvillageCode().toString()
            //Toast.makeText(activity!!, sendIdKelurahan, Toast.LENGTH_SHORT).show()
            /*sendIdKelurahan = idKelurahan[position].toString()*/
        }

//        radio_group?.setOnCheckedChangeListener { group, checkedId ->
//            idJenisKepemilikan = if (R.id.rbPerorangan == checkedId) "0" else "1"
//        }

        getDataSubjek()
    }

    private fun getDataSubjek() {
        if (session.id != "" && session.status != "" && session.from == "1") {
            val results = realm.where(PartyAdd::class.java).equalTo("TempId", session.id.toInt()).findFirst()
            if (results != null) {
                tampilkanData(results)
                Log.d("results", results.toString())
            } else {
                Log.e("data", "kosong")
            }
        } else if (session.id != "" && session.status != "" && session.from == "2") {
            //subjek2
            val results2 = realm.where(Subjek2::class.java).equalTo("SubjekIdTemp", session.id.toInt()).findFirst()
            if (results2 != null) {
                tampilkanDataSubjek2(results2)
                Log.d("results", results2.toString())
            } else {
                Log.e("data", "kosong")
            }
        } else if (session.id != "" && session.status != "" && session.from == "3") {
            //ahli waris
            val results = realm.where(AhliWaris::class.java).equalTo("AhliWarisIdTemp", session.id.toInt()).findFirst()
            if (results != null) {
                tampilkanDataAhliWaris(results)
                Log.d("results", results.toString())
            } else {
                Log.e("data", "kosong")
            }
        }
        else{
            edTemporaryFieldNumber.setText(mParam1)
        }
    }

    private fun tampilkanDataAhliWaris(results: AhliWaris) {
        resetAdapter(results.partyProvinceCode,
                results.partyRegencyCode,
                results.partyDistrictCode,
                results.partyVillageId)

//        if (results.partyOwnerType == 0) radio_group.check(R.id.rbPerorangan) else radio_group.check(R.id.rbInstansi)
        edTemporaryFieldNumber.setText(mParam1)
        edNIK.setText(results.partyIdentityNumber)
        edPhone.setText(results.partyPhone)
        edEmail.setText(results.partyEmail)
        ednamaLengkap.setText(results.partyFullName.toString())
        autocomplateTVBirthPlace.setText(results.partyBirthPlaceName.toString())
        sendidBirthPlace = results.partyBirthPlaceCode
        tvtisitglLahir.text = results.partyBirthDate.toString()
        senddateBirthPicker = results.partyBirthDate
        spinnerPekerjaan.setText(results.partyOccupationName.toString())
        sendIdPekerjaan = results.partyOccupationId
        autocomplateTVProvince.setText(results.partyProvinceName.toString())
        sendIdProvince = results.partyProvinceCode
        autocomplateTVKotaKabupaten.setText(results.partyRegencyName.toString())
        sendIdCity = results.partyRegencyCode
        autocomplateTVKecamatan.setText(results.partyDistrictName)
        sendIdKecamatan = results.partyDistrictCode
        autocomplateTVKelurahan.setText(results.partyVillageName)
        sendIdKelurahan = results.partyVillageId
        edBlok.setText(results.partyBlock.toString())
        edNomor.setText(results.partyNumber.toString())
        edKomplek.setText(results.partyComplexName.toString())
        edJalan.setText(results.partyStreetName.toString())
        edRt.setText(results.partyRT.toString())
        edRW.setText(results.partyRW.toString())
        edkodePos.setText(results.partyPostalCode.toString())

        tanggalEdit = results.partyBirthDate
        /* data offline jenis pekerjaan */
        val dataPekerjaan = realm.where(ResponseDataPekerjaanWithOutParam::class.java).findAll()
        val data: MutableList<ResponseDataPekerjaanWithOutParam> = realm.copyFromRealm(dataPekerjaan)

        adapterPekerjaan = ArrayAdapter(activity!!, R.layout.item_spinner, data)
        spinnerPekerjaan.setAdapter(adapterPekerjaan)
        spinnerPekerjaan.onItemClickListener = AdapterView.OnItemClickListener { adapterView, _, position, _ ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()

            val dataSpinner = adapterView.getItemAtPosition(position) as ResponseDataPekerjaanWithOutParam
            sendIdPekerjaan = dataSpinner.getliveliHoodId().toString()
            //Toast.makeText(activity!!, sendIdPekerjaan, Toast.LENGTH_SHORT).show()

            /*val valuespinner = parent.getItemAtPosition(position).toString()
            sendIdPekerjaan = idPekerjaan[position]!!.toString()
            Toast.makeText(activity!!, sendIdPekerjaan, Toast.LENGTH_SHORT).show()*/
        }
    }

    private fun tampilkanDataSubjek2(results: Subjek2) {
        resetAdapter(results.subjek2ProvinceCode,
                results.subjek2RegencyCode,
                results.subjek2DistrictCode,
                results.subjek2VillageId)

//        if (results.subjek2OwnerType == 0) radio_group.check(R.id.rbPerorangan) else radio_group.check(R.id.rbInstansi)
        ednamaLengkap.setText(results.subjek2FullName.toString())
        autocomplateTVBirthPlace.setText(results.subjek2BirthPlaceName.toString())
        sendidBirthPlace = results.subjek2BirthPlaceCode
        tvtisitglLahir.text = results.subjek2BirthDate.toString()
        senddateBirthPicker = results.subjek2BirthDate
        spinnerPekerjaan.setText(results.subjek2OccupationName.toString())
        sendIdPekerjaan = results.subjek2OccupationId
        autocomplateTVProvince.setText(results.subjek2ProvinceName.toString())
        sendIdProvince = results.subjek2ProvinceCode
        autocomplateTVKotaKabupaten.setText(results.subjek2RegencyName.toString())
        sendIdCity = results.subjek2RegencyCode
        autocomplateTVKecamatan.setText(results.subjek2DistrictName)
        sendIdKecamatan = results.subjek2DistrictCode
        autocomplateTVKelurahan.setText(results.subjek2VillageName)
        sendIdKelurahan = results.subjek2VillageId
        edBlok.setText(results.subjek2Block.toString())
        edNomor.setText(results.subjek2Number.toString())
        edKomplek.setText(results.subjek2ComplexName.toString())
        edJalan.setText(results.subjek2StreetName.toString())
        edRt.setText(results.subjek2RT.toString())
        edRW.setText(results.subjek2RW.toString())
        edkodePos.setText(results.subjek2PostalCode.toString())

        tanggalEdit = results.subjek2BirthDate
        /* data offline jenis pekerjaan */
        val dataPekerjaan = realm.where(ResponseDataPekerjaanWithOutParam::class.java).findAll()
        val data: MutableList<ResponseDataPekerjaanWithOutParam> = realm.copyFromRealm(dataPekerjaan)

        adapterPekerjaan = ArrayAdapter(activity!!, R.layout.item_spinner, data)
        spinnerPekerjaan.setAdapter(adapterPekerjaan)
        spinnerPekerjaan.onItemClickListener = AdapterView.OnItemClickListener { adapterView, _, position, _ ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()

            val dataSpinner = adapterView.getItemAtPosition(position) as ResponseDataPekerjaanWithOutParam
            sendIdPekerjaan = dataSpinner.getliveliHoodId().toString()
            //Toast.makeText(activity!!, sendIdPekerjaan, Toast.LENGTH_SHORT).show()

            /*val valuespinner = parent.getItemAtPosition(position).toString()
            sendIdPekerjaan = idPekerjaan[position]!!.toString()
            Toast.makeText(activity!!, sendIdPekerjaan, Toast.LENGTH_SHORT).show()*/
        }
    }

    private fun tampilkanData(results: PartyAdd) {
        resetAdapter(results.partyProvinceCode,
                results.partyRegencyCode,
                results.partyDistrictCode,
                results.partyVillageId)

//        if (results.partyOwnerType == 0) radio_group.check(R.id.rbPerorangan) else radio_group.check(R.id.rbInstansi)
//        idJenisKepemilikan = results.partyOwnerType.toString()
        edTemporaryFieldNumber.setText(mParam1)
        edNIK.setText(results.partyIdentityNumber)
        edPhone.setText(results.partyPhone)
        edEmail.setText(results.partyEmail)
        ednamaLengkap.setText(results.partyFullName.toString())
        autocomplateTVBirthPlace.setText(results.partyBirthPlaceName.toString())
        sendidBirthPlace = results.partyBirthPlaceCode
        tvtisitglLahir.text = results.partyBirthDate.toString()
        senddateBirthPicker = results.partyBirthDate
        spinnerPekerjaan.setText(results.partyOccupationName.toString())
        sendIdPekerjaan = results.partyOccupationId
        autocomplateTVProvince.setText(results.partyProvinceName.toString())
        sendIdProvince = results.partyProvinceCode
        autocomplateTVKotaKabupaten.setText(results.partyRegencyName.toString())
        sendIdCity = results.partyRegencyCode
        autocomplateTVKecamatan.setText(results.partyDistrictName)
        sendIdKecamatan = results.partyDistrictCode
        autocomplateTVKelurahan.setText(results.partyVillageName)
        sendIdKelurahan = results.partyVillageId
        edBlok.setText(results.partyBlock.toString())
        edNomor.setText(results.partyNumber.toString())
        edKomplek.setText(results.partyComplexName.toString())
        edJalan.setText(results.partyStreetName.toString())
        edRt.setText(results.partyRT.toString())
        edRW.setText(results.partyRW.toString())
        edkodePos.setText(results.partyPostalCode.toString())

        tanggalEdit = results.partyBirthDate
        /* data offline jenis pekerjaan */
        val dataPekerjaan = realm.where(ResponseDataPekerjaanWithOutParam::class.java).findAll()
        val data: MutableList<ResponseDataPekerjaanWithOutParam> = realm.copyFromRealm(dataPekerjaan)

        adapterPekerjaan = ArrayAdapter(activity!!, R.layout.item_spinner, data)
        spinnerPekerjaan.setAdapter(adapterPekerjaan)
        spinnerPekerjaan.onItemClickListener = AdapterView.OnItemClickListener { adapterView, _, position, _ ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()

            val dataSpinner = adapterView.getItemAtPosition(position) as ResponseDataPekerjaanWithOutParam
            sendIdPekerjaan = dataSpinner.getliveliHoodId().toString()
            //Toast.makeText(activity!!, sendIdPekerjaan, Toast.LENGTH_SHORT).show()

            /*val valuespinner = parent.getItemAtPosition(position).toString()
            sendIdPekerjaan = idPekerjaan[position]!!.toString()
            Toast.makeText(activity!!, sendIdPekerjaan, Toast.LENGTH_SHORT).show()*/
        }
    }

    fun resetAdapter(a: String, b: String, c: String, d: String) {
        sendIdProvince = a
        sendIdCity = b
        sendIdKecamatan = c
        sendIdKelurahan = d

        adapterKota.setFilterRealm(null)
        adapterKecamatan.setRealmFilter(null)
        adapterKelurahan.setRealmFilter(null)

        Log.d("iqbak", c)

        val resultRealmKotaByProvince = realm.where(ResponseDataCityWithoutParameter::class.java)
                .equalTo("regencyProvinceCode", sendIdProvince)
                .findAll()
        adapterKota.setFilterRealm(resultRealmKotaByProvince)
        autocomplateTVKotaKabupaten.setAdapter(adapterKota)

        val resultRealmKecamatanByKota = realm.where(ResponseDataKecamatanWithoutParameter::class.java)
                .equalTo("districtRegencyCode", sendIdCity)
                .findAll()
        adapterKecamatan.setRealmFilter(resultRealmKecamatanByKota)
        autocomplateTVKecamatan.setAdapter(adapterKecamatan)

        val resultRealmKelurahanByKecamatan = realm.where(ResponseDataKelurahanWithoutParameter::class.java)
                .equalTo("villageDistrictCode", sendIdKecamatan)
                .findAll()

        adapterKelurahan.setRealmFilter(resultRealmKelurahanByKecamatan)
        autocomplateTVKelurahan.setAdapter(adapterKelurahan)
    }

    private var textWatcherValidasiRegion: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            /*if (autocomplateTVProvince.text.toString().trim().isEmpty()) {
                sendIdProvince = ""
                llkota.visibility = View.GONE
            } else {
                llkota.visibility = View.VISIBLE
            }*/
        }

        override fun afterTextChanged(editable: Editable) {
            checkData()
        }
    }

    private fun checkData() {
        if (sendIdProvince == "" || sendIdProvince.isEmpty()) {
            sendIdProvince = ""
            llkota.visibility = View.GONE
        } else {
            llkota.visibility = View.VISIBLE
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun showdatePicker() {
        /* val datePickerDialog = DatePickerDialog(activity!!, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
             c.set(Calendar.YEAR, year)
             c.set(Calendar.MONTH, monthOfYear)
             c.set(Calendar.DATE, dayOfMonth)

             tvtisitglLahir.text = DateFormat.format(dateTemplate, c.time)
             senddateBirthPicker = DateFormat.format(dateTemplate2, c.time).toString()
             //txtDate.setText(dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
         }, mYear, mMonth, mDay)*/

        if (session.from.isNotEmpty() && tanggalEdit.isNotEmpty()) {
            val datePart = tanggalEdit.split("-")
            val year = datePart[0]
            val month = datePart[1]
            val day = datePart[2]
            val outputDateConvert = "$day/$month/$year"

            mMonth = if (month.contains("0")) {
                val parts = month.substring(1)
                parts.toInt()
            } else {
                month.toInt()
            }

            mDay = if (day.contains("0")) {
                val parts = day.substring(1)
                parts.toInt()
            } else {
                day.toInt()
            }

            mYear = year.toInt()

            Log.d("hasil", mMonth.toString())
            Log.d("hasil", mDay.toString())
            val datePickerDialog = DatePickerDialog(activity!!,
                    android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                    mdateSetListener,
                    mYear, mMonth, mDay)

            datePickerDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            datePickerDialog.datePicker.maxDate = mCalendar.timeInMillis - 1000
            datePickerDialog.show()
        } else {
            Log.d("hasil", mMonth.toString())
            Log.d("hasil2", mDay.toString())
            val datePickerDialog2 = DatePickerDialog(activity!!,
                    AlertDialog.THEME_HOLO_DARK,
                    mdateSetListener,
                    mYear, mMonth, mDay)
            datePickerDialog2.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            datePickerDialog2.datePicker.minDate = mCalendar.timeInMillis - 1000
            datePickerDialog2.show()
        }

    }

    private fun selectedDate() {
        if (session.from.isNotEmpty() && tanggalEdit.isNotEmpty()) {
            val datePart = tanggalEdit.split("-")
            val year = datePart[0]
            val month = datePart[1]
            val day = datePart[2]

            val mmDay = if (month.substring(0, 1).contains("0")) {
                val parts = day.substring(1)
                parts.toInt()
            } else {
                day.toInt()
            }

            val mmMonth = if (month.substring(0, 1).contains("0")) {
                val parts = month.substring(1)
                parts.toInt() - 1
            } else {
                month.toInt() - 1
            }

            val mmYear = year.toInt()

            val datePickerDialog = DatePickerDialog(activity!!, AlertDialog.THEME_HOLO_DARK, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                /*Log.d("hasil1", dayOfMonth.toString())
                Log.d("hasil1", monthOfYear.toString())
                Log.d("hasil1", year.toString())*/
                // bulan dari hasil date picker kurang satu bulan dari bulan yg dipilih sehingga ditambah + 1
                datepicker = DatePicker(activity)
                datepicker!!.init(year, monthOfYear + 1, dayOfMonth, null)
                val calendar2 = GregorianCalendar(year, monthOfYear, dayOfMonth)

                tvtisitglLahir.text = DateFormat.format(dateTemplate, calendar2.time)
                senddateBirthPicker = DateFormat.format(dateTemplate2, calendar2.time).toString()
                //Toast.makeText(activity, senddateBirthPicker, Toast.LENGTH_SHORT).show()

                //txtDate.setText(dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
            }, mmYear, mmMonth, mmDay)

            if (datepicker != null) {
                //Log.d("hasil2", "${datepicker!!.month - 1}")
                datePickerDialog.updateDate(datepicker!!.year, datepicker!!.month - 1, datepicker!!.dayOfMonth)
            }

            datePickerDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            datePickerDialog.datePicker.maxDate = mCalendar.timeInMillis - 1000
            datePickerDialog.show()

        } else {
            val datePickerDialog = DatePickerDialog(activity!!, AlertDialog.THEME_HOLO_DARK, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // bulan dari hasil date picker kurang satu bulan dari bulan yg dipilih sehingga ditambah + 1
                datepicker2 = DatePicker(activity)
                datepicker2!!.init(year, monthOfYear + 1, dayOfMonth, null)

                val calendar2 = GregorianCalendar(year, monthOfYear, dayOfMonth)

                tvtisitglLahir.text = DateFormat.format(dateTemplate, calendar2.time)
                senddateBirthPicker = DateFormat.format(dateTemplate2, calendar2.time).toString()
                //Toast.makeText(activity, senddateBirthPicker, Toast.LENGTH_SHORT).show()

                //txtDate.setText(dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
            }, mYear, mMonth, mDay)

            if (datepicker2 != null) {
                //Log.d("hasil2", "${datepicker2!!.month - 1}")
                datePickerDialog.updateDate(datepicker2!!.year, datepicker2!!.month - 1, datepicker2!!.dayOfMonth)
            }

            datePickerDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            datePickerDialog.datePicker.maxDate = mCalendar.timeInMillis - 1000
            datePickerDialog.show()
        }

    }

    override fun onResume() {
        super.onResume()
        nextBT?.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        nextBT?.setOnClickListener(null)
    }


    override fun onClick(view: View) {
        if (view.id == R.id.nextBT) {
            hidekeyboard2()
            dataFormSubjek1.clear()
            Utils.deletedataform1(activity!!)
            if (validate()) {
                val data = DataFormSubjek1(
                    ednamaLengkap.text.toString().trim(),
                    sendidBirthPlace,
                    autocomplateTVBirthPlace.text.toString().trim(),
                    "",
                    senddateBirthPicker,
                    sendIdPekerjaan,
                    spinnerPekerjaan.text.toString().trim(),
                    "",
                    sendIdProvince,
                    autocomplateTVProvince.text.toString(),
                    sendIdCity,
                    autocomplateTVKotaKabupaten.text.toString(),
                    sendIdKecamatan,
                    autocomplateTVKecamatan.text.toString(),
                    sendIdKelurahan,
                    autocomplateTVKelurahan.text.toString(),
                    edBlok.text.toString().trim(),
                    edNomor.text.toString().trim(),
                    edKomplek.text.toString().trim(),
                    edJalan.text.toString().trim(),
                    edRt.text.toString().trim(),
                    edRW.text.toString().trim(),
                    edkodePos.text.toString().trim(),
                    edTemporaryFieldNumber.text.toString().trim(),
                    edNIK.text.toString().trim(),
                    edPhone.text.toString().trim(),
                    edEmail.text.toString().trim())

                dataFormSubjek1.add(data)

                val jsonBBM = Gson().toJson(dataFormSubjek1)
                Utils.savedataform1(jsonBBM, activity!!)
                if (mListener != null) {
                    mListener!!.onNextPressed(this)
                }
            }

            /*if (mListener != null) {
                mListener!!.onNextPressed(this)
            }*/
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnStepOneListener) {
            mListener = context
        } else {
            throw RuntimeException(("$context must implement OnFragmentInteractionListener"))
        }
    }

    override fun onDetach() {
        super.onDetach()
        getPresenter()?.onDetach()
        mListener = null

    }

    private fun validate(): Boolean {
        val valid: Boolean

        val sweetAlretLoading = SweetAlertDialog(activity!!, SweetAlertDialog.WARNING_TYPE)
        sweetAlretLoading.setCancelable(false)
        sweetAlretLoading.confirmText = "OK"
        sweetAlretLoading.setConfirmClickListener { sDialog ->
            sDialog?.let { if (it.isShowing) it.dismiss() }
        }
        when {
            edNIK.text.toString().isEmpty() || edNIK.text.toString().length < 16 -> {
                sweetAlretLoading.titleText = "Lengkapi data KTP"
                sweetAlretLoading.show()
                ednamaLengkap.requestFocus()
                valid = false
            }
            ednamaLengkap.text.toString().trim().isEmpty() -> {
                sweetAlretLoading.titleText = "Lengkapi data nama lengkap"
                sweetAlretLoading.show()
                ednamaLengkap.requestFocus()
                valid = false
            }
            edPhone.text.toString().isNullOrEmpty() -> {
                sweetAlretLoading.titleText = "Lengkapi data nomor telephone (WA)"
                sweetAlretLoading.show()
                ednamaLengkap.requestFocus()
                valid = false
            }
            sendIdProvince.isEmpty() -> {
                sweetAlretLoading.titleText = "Lengkapi data provinsi"
                sweetAlretLoading.show()
                autocomplateTVProvince.requestFocus()
                valid = false
            }
            sendIdCity.isEmpty() -> {
                sweetAlretLoading.titleText = "Lengkapi data kota/kabupaten"
                sweetAlretLoading.show()
                autocomplateKotaKabupaten.requestFocus()
                valid = false
            }
            sendIdKecamatan.isEmpty() -> {
                sweetAlretLoading.titleText = "Lengkapi data kecamatan"
                sweetAlretLoading.show()
                autocomplateTVKecamatan.requestFocus()
                valid = false
            }
            sendIdKelurahan.isEmpty() -> {
                sweetAlretLoading.titleText = "Lengkapi data kelurahan"
                sweetAlretLoading.show()
                autocomplateTVKelurahan.requestFocus()
                valid = false
            }
            else -> valid = true
        }

        return valid
    }

    private fun getPresenter(): Form1DPPTPresenter? {
        form1DPPTPresenter = Form1DPPTPresenter()
        form1DPPTPresenter.onAttach(this)
        return form1DPPTPresenter
    }

    private fun hidekeyboard2() {
        val inputManager = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        val v = activity!!.currentFocus

        if (v != null) {
            activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            inputManager.hideSoftInputFromWindow(v.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)

        }
    }

    private fun loadbirthPlace(searchCode: String) {

        //getPresenter()?.getregioBirthPlace(searchCode, "8")
        // Build the query looking at all kota
        // Add query conditions:

        //add to Spinner
        val listSpinner: ArrayList<String> = ArrayList()
        listSpinner.clear()

        realm.executeTransaction { inrealm ->
            val data = inrealm.where(ResponseDataCityWithoutParameter::class.java)
                    .beginsWith("regencyName", searchCode)
                    .limit(10)
                    .findAll()

            idBirthplace = arrayOfNulls(data.size)
            for (i in 0 until data.size) {
                idBirthplace[i] = data[i]?.getRegencyId().toString()
                listSpinner.add(data[i]?.getRegencyName().toString())
                Log.d("idBirthPlace", idBirthplace[i])
            }

        }

        val adapterkotaKelahiran = ArrayAdapter<String>(activity!!, R.layout.item_spinner, listSpinner)
        adapterkotaKelahiran.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1)
        autocomplateTVBirthPlace.threshold = 1 //will start working from first character
        autocomplateTVBirthPlace.setAdapter(adapterkotaKelahiran)

        //GetDataFromRealmDatabase
        //val results = realm.where(ResponseDataCityWithoutParameter::class.java).findAll()
        val results = realm.where(ResponseDataCityWithoutParameter::class.java)
                .contains("regencyName", searchCode, Case.INSENSITIVE)
                .findAll()
//        Log.d("results",results.toString())
//
//        //add to Spinner
//        val listSpinner: ArrayList<String> = ArrayList()
//        listSpinner.clear()
//
//        idBirthplace = arrayOfNulls(results.size)
//        for (i in 0 until results.size) {
//            idBirthplace[i] = results[i]?.getRegencyId().toString()
//            listSpinner.add(results[i]?.getRegencyName().toString())
//            Log.d("idBirthPlace",idBirthplace[i])
//        }
//
//        val adapterKelurahan = ArrayAdapter<String>(activity!!, R.layout.item_spinner, listSpinner)
//        adapterKelurahan.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1)
//        autocomplateTVBirthPlace.threshold = 1 //will start working from first character
//        autocomplateTVBirthPlace.setAdapter(adapterKelurahan)
    }

    private fun loadProvince(searchCode: String) {
        getPresenter()?.getregionProvince(searchCode)
    }

    private fun loadCity(searchCode: String) {
        getPresenter()?.getregionCity(sendIdProvince, searchCode, "8")
    }

    private fun loadKecamtan(searchCode: String) {
        getPresenter()?.getregionKecamatan(searchCode, sendIdCity, "8")
    }

    private fun loadKelurahan(searchCode: String) {
        getPresenter()?.getregionKelurahan(searchCode, sendIdKecamatan, "8")
    }

    override fun onsuccessgetregionProvince(responseDataProvince: MutableList<ResponseDataProvince>) {
        val listSpinner: ArrayList<String> = ArrayList()
        listSpinner.clear()

        /*idProvince = arrayOfNulls(responseDataProvince.size)
        responseDataProvince.forEach {
            listSpinner.add(it.provinceName.toString())
            idProvince[it] = it.provinceCode
        }*/

        idProvince = arrayOfNulls(responseDataProvince.size)
        for (i in 0 until responseDataProvince.size) {
            idProvince[i] = responseDataProvince[i].provinceCode.toString()
            listSpinner.add(responseDataProvince[i].provinceName.toString())
        }

        val adapterProvinsi = ArrayAdapter<String>(activity!!, R.layout.item_spinner, listSpinner)
        adapterProvinsi.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1)
        autocomplateTVProvince.threshold = 1 //will start working from first character
        autocomplateTVProvince.setAdapter(adapterProvinsi)

    }

    override fun onsuccessgetregionCity(responseDataCity: MutableList<ResponseDataCity>) {
        val listSpinner: ArrayList<String> = ArrayList()
        listSpinner.clear()

        idCity = arrayOfNulls(responseDataCity.size)
        for (i in 0 until responseDataCity.size) {
            idCity[i] = responseDataCity[i].regencyCode.toString()
            listSpinner.add(responseDataCity[i].regencyName.toString())
        }

        val adapterCity = ArrayAdapter<String>(activity!!, R.layout.item_spinner, listSpinner)
        adapterCity.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1)
        autocomplateTVKotaKabupaten.threshold = 1 //will start working from first character
        autocomplateTVKotaKabupaten.setAdapter(adapterCity)
    }

    override fun onsuccessgetregionKecamatan(responseDataKecamatan: MutableList<ResponseDataKecamatan>) {
        val listSpinner: ArrayList<String> = ArrayList()
        listSpinner.clear()

        idKecamatan = arrayOfNulls(responseDataKecamatan.size)
        for (i in 0 until responseDataKecamatan.size) {
            idKecamatan[i] = responseDataKecamatan[i].districtCode.toString()
            listSpinner.add(responseDataKecamatan[i].districtName.toString())
        }

        val adapterKecamatan = ArrayAdapter<String>(activity!!, R.layout.item_spinner, listSpinner)
        adapterKecamatan.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1)
        autocomplateTVKecamatan.threshold = 1 //will start working from first character
        autocomplateTVKecamatan.setAdapter(adapterKecamatan)
    }

    override fun onsuccessgetregionKelurahan(responseDataKelurahan: MutableList<ResponseDataKelurahan>) {
        val listSpinner: ArrayList<String> = ArrayList()
        listSpinner.clear()

        idKelurahan = arrayOfNulls(responseDataKelurahan.size)
        for (i in 0 until responseDataKelurahan.size) {
            idKelurahan[i] = responseDataKelurahan[i].villageCode.toString()
            listSpinner.add(responseDataKelurahan[i].villageName.toString())
        }

        val adapterKelurahan = ArrayAdapter<String>(activity!!, R.layout.item_spinner, listSpinner)
        adapterKelurahan.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1)
        autocomplateTVKelurahan.threshold = 1 //will start working from first character
        autocomplateTVKelurahan.setAdapter(adapterKelurahan)
    }

    override fun onsuccessgetregioBirthPlace(responseDataCity: MutableList<ResponseDataCity>) {
        val listSpinner: ArrayList<String> = ArrayList()
        listSpinner.clear()

        idBirthplace = arrayOfNulls(responseDataCity.size)
        for (i in 0 until responseDataCity.size) {
            idBirthplace[i] = responseDataCity[i].regencyCode.toString()
            listSpinner.add(responseDataCity[i].regencyName.toString())
        }

        val adapterKelurahan = ArrayAdapter<String>(activity!!, R.layout.item_spinner, listSpinner)
        adapterKelurahan.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1)
        autocomplateTVBirthPlace.threshold = 1 //will start working from first character
        autocomplateTVBirthPlace.setAdapter(adapterKelurahan)
    }

    override fun onsuccessgetdaftarPekerjaan(responseDataPekerjaan: MutableList<ResponseDataPekerjaan>) {
        // untuk online hapus comment & panggil fungsinya
        /*for (row in responseDataPekerjaan) {
            idPekerjaan.add(row.occupationId.toString())
            valuespinnerpekerjaan.add(row.occupationName.toString())
        }
        val adapterPekerjaan = ArrayAdapter<String>(activity!!, R.layout.item_spinner, valuespinnerpekerjaan)
        spinnerPekerjaan.setAdapter(adapterPekerjaan)

        Log.d("babab", idPekerjaan.toString())*/
    }


    class RealmAutoCompleteAdapter(realm: Realm, items: RealmResults<ResponseDataCityWithoutParameter>) : androidx.recyclerview.widget.RecyclerView.Adapter<KotaViewHolder>() {

        private var mRealm: Realm = realm
        private val realmChangeListener: RealmChangeListener<RealmResults<ResponseDataCityWithoutParameter>>
        private var mitems: RealmResults<ResponseDataCityWithoutParameter>?
        private var searchTerm: String? = ""

        init {
            this.realmChangeListener = object : RealmChangeListener<RealmResults<ResponseDataCityWithoutParameter>> {
                override fun onChange(t: RealmResults<ResponseDataCityWithoutParameter>) {
                    notifyDataSetChanged()
                }

            }

            this.mitems = items
            mitems!!.addChangeListener(realmChangeListener)
        }

        override fun onCreateViewHolder(parent: ViewGroup, p1: Int): KotaViewHolder {
            return KotaViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_spinner, parent, false))
        }

        fun updateInput(input: String) {
            this.searchTerm = input
            if (mitems != null && mitems!!.isValid) {
                mitems!!.removeAllChangeListeners()
            }
            var query = mRealm.where(ResponseDataCityWithoutParameter::class.java)
            if (searchTerm != null && "" != searchTerm);
            run { query = query.beginsWith("regencyName", searchTerm, Case.INSENSITIVE) }
            mitems = query.findAll()
            mitems!!.addChangeListener(realmChangeListener)
            notifyDataSetChanged()
        }

        override fun getItemCount(): Int {
            if (mitems == null || mitems!!.isValid) {
                return 0
            }
            return mitems!!.size
        }

        override fun onBindViewHolder(p0: KotaViewHolder, p1: Int) {
            val mData = mitems!![p1]
            p0.bind(mData)
        }

    }


    class KotaViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {

        val txtName: com.pixplicity.fontview.FontTextView = view.text1 as com.pixplicity.fontview.FontTextView

        fun bind(data: ResponseDataCityWithoutParameter?) {
            txtName.text = data!!.getRegencyName()
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    interface OnStepOneListener {
        //void onFragmentInteraction(Uri uri);
        fun onNextPressed(fragment: androidx.fragment.app.Fragment)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"
        internal const val TAG = "FormSubjek1"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): FormSubjek1 {
            val fragment = FormSubjek1()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor