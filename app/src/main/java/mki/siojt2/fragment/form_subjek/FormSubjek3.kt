package mki.siojt2.fragment.form_subjek

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import cn.pedant.SweetAlert.SweetAlertDialog
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_form_subjek_3.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.fragment.form_subjek.adapter_recycleview.AdapterMataPencaharian
import mki.siojt2.fragment.form_subjek.presenter.Form3DPPTPresenter
import mki.siojt2.fragment.form_subjek.view.FormSubjek3View
import mki.siojt2.model.data_form_subjek.DataFormMataPencaharian
import mki.siojt2.model.data_form_subjek.DataFormSubjek3
import mki.siojt2.model.data_form_subjek.DataMataPencaharian
import mki.siojt2.model.localsave.*
import mki.siojt2.model.master_data_subjek.ResponseDataMataPencaharian
import mki.siojt2.model.master_data_subjek.ResponseDataMataPencaharianWithOutParam
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.Utils

class FormSubjek3 : BaseFragment(), FormSubjek3View, View.OnClickListener {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    private var mListener: OnStepThreeListener? = null

    private var validdataMataPencaharian: Boolean = false
    private lateinit var form3DPPTPresenter: Form3DPPTPresenter

    private var idjenisUsaha: ArrayList<Int> = ArrayList()
    private var sendIdJensiUsaha: Int? = null

    private var dataFormSubjek3: MutableList<DataFormSubjek3> = ArrayList()

    lateinit var adapterMataPencaharian: AdapterMataPencaharian

    lateinit var realm: Realm
    private lateinit var session: Session
    private var dialogValidate: SweetAlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }

        realm = RealmController.with(this).realm
        realm.isAutoRefresh
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_form_subjek_3, container, false)
    }

    override fun setUp(view: View) {
        Log.d("FormSubjek3", "visible")

        dialogValidate = SweetAlertDialog(activity!!, SweetAlertDialog.WARNING_TYPE)
        dialogValidate!!.titleText = "Gagal menambahkan"
        dialogValidate!!.contentText = "Data tidak boleh kosong!"
        dialogValidate!!.setCancelable(false)
        dialogValidate!!.confirmText = "OK"
        dialogValidate!!.setConfirmClickListener { sDialog ->
            sDialog?.let { if (it.isShowing) it.dismiss() }
        }

        val resultsRealm = realm.where(ResponseDataMataPencaharianWithOutParam::class.java).findAll()
        val dataMataPencaharian: MutableList<ResponseDataMataPencaharianWithOutParam> = realm.copyFromRealm(resultsRealm)

        adapterMataPencaharian = AdapterMataPencaharian(dataMataPencaharian, activity!!, this@FormSubjek3)
//        val linearLayoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.VERTICAL, false)
//        rvmataPencaharian.layoutManager = linearLayoutManager
//        rvmataPencaharian.adapter = adapterMataPencaharian

        getDataSubjek3()

//        spinnerJA_SA.onItemClickListener = AdapterView.OnItemClickListener { adapterView, _, position, _ ->
//            sendIdJensiUsaha = if (position == adapterView.firstVisiblePosition) {
//                null
//            } else {
//                idjenisUsaha[position]
//            }
//        }
    }

    private fun getDataSubjek3() {
        session = Session(context)
        if (session.id != "" && session.status != "" && session.from == "1") {
            val results = realm.where(PartyAdd::class.java).equalTo("TempId", session.id.toInt()).findFirst()
            if (results != null) {
                edNotes.setText(results.partyNotes)
            } else {
                Log.e("data", "kosong")
            }
        }
//        else if (session.id != "" && session.status != "" && session.from == "2") {
//            val results2 = realm.where(Subjek2::class.java).equalTo("SubjekIdTemp", session.id.toInt()).findFirst()
//            if (results2 != null) {
//                val liveId = results2.livelihoodLiveId
//                val replace1 = liveId.replace("[", "['")
//                val replace2 = replace1.replace("]", "']")
//                val replace3 = replace2.replace(",", "','")
//
//                val livename = results2.livelihoodLiveName
//                val livename1 = livename.replace("[", "['")
//                val livename2 = livename1.replace("]", "']")
//                val livename4 = livename2.replace(",", "','")
//
//                val livenameother = results2.livelihoodOther
//                val livenameother2 = livenameother.replace("[", "['")
//                val livenameother3 = livenameother2.replace("]", "']")
//                val livenameother4 = livenameother3.replace(",", "','")
//
//                val liveIncome = results2.livelihoodIncome
//                val liveIncome1 = liveIncome.replace("[", "['")
//                val liveIncome2 = liveIncome1.replace("]", "']")
//                val liveIncome3 = liveIncome2.replace(",", "','")
//                val liveIncome4 = liveIncome3.replace(" ", "")
//
//                val convertlistliveId = Gson().fromJson(replace3, Array<String>::class.java).toList()
//                val convertlistliveName = Gson().fromJson(livename4, Array<String>::class.java).toList()
//                val convertlistliveNameOther = Gson().fromJson(livenameother4, Array<String>::class.java).toList()
//                val convertlistliveValue = Gson().fromJson(liveIncome4, Array<String>::class.java).toList()
//
//
//                for (i in convertlistliveId.indices) {
//                    val dataMataPencaharian = DataMataPencaharian()
//                    dataMataPencaharian.livelihoodLiveId = convertlistliveId[i]
//                    dataMataPencaharian.livelihoodLiveName = convertlistliveName[i]
//                    dataMataPencaharian.livelihoodOther = convertlistliveNameOther[i]
//                    dataMataPencaharian.livelihoodIncome = convertlistliveValue[i]
//
//                    adapterMataPencaharian.addItems(dataMataPencaharian)
//                    adapterMataPencaharian.notifyDataSetChanged()
//                }
//
//            } else {
//                Log.e("data", "kosong")
//            }
//        } else if (session.id != "" && session.status != "" && session.from == "3") {
//            val results3 = realm.where(AhliWaris::class.java).equalTo("AhliWarisIdTemp", session.id.toInt()).findFirst()
//            if (results3 != null) {
//
//                val liveId = results3.livelihoodLiveId
//                val replace1 = liveId.replace("[", "['")
//                val replace2 = replace1.replace("]", "']")
//                val replace3 = replace2.replace(",", "','")
//
//                val livename = results3.livelihoodLiveName
//                val livename1 = livename.replace("[", "['")
//                val livename2 = livename1.replace("]", "']")
//                val livename4 = livename2.replace(",", "','")
//
//                val livenameother = results3.livelihoodOther
//                val livenameother2 = livenameother.replace("[", "['")
//                val livenameother3 = livenameother2.replace("]", "']")
//                val livenameother4 = livenameother3.replace(",", "','")
//
//                val liveIncome = results3.livelihoodIncome
//                val liveIncome1 = liveIncome.replace("[", "['")
//                val liveIncome2 = liveIncome1.replace("]", "']")
//                val liveIncome3 = liveIncome2.replace(",", "','")
//                val liveIncome4 = liveIncome3.replace(" ", "")
//
//                val convertlistliveId = Gson().fromJson(replace3, Array<String>::class.java).toList()
//                val convertlistliveName = Gson().fromJson(livename4, Array<String>::class.java).toList()
//                val convertlistliveNameOther = Gson().fromJson(livenameother4, Array<String>::class.java).toList()
//                val convertlistliveValue = Gson().fromJson(liveIncome4, Array<String>::class.java).toList()
//
//
//                for (i in convertlistliveId.indices) {
//                    val dataMataPencaharian = DataMataPencaharian()
//                    dataMataPencaharian.livelihoodLiveId = convertlistliveId[i]
//                    dataMataPencaharian.livelihoodLiveName = convertlistliveName[i]
//                    dataMataPencaharian.livelihoodOther = convertlistliveNameOther[i]
//                    dataMataPencaharian.livelihoodIncome = convertlistliveValue[i]
//
//                    adapterMataPencaharian.addItems(dataMataPencaharian)
//                    adapterMataPencaharian.notifyDataSetChanged()
//                }
//
//            } else {
//                Log.e("data", "kosong")
//            }
//        } else {
//            val dataMataPencaharian = DataMataPencaharian()
//            dataMataPencaharian.livelihoodLiveId = ""
//            dataMataPencaharian.livelihoodLiveName = ""
//            dataMataPencaharian.livelihoodOther = ""
//            dataMataPencaharian.livelihoodIncome = ""
//
//            adapterMataPencaharian.addItems(dataMataPencaharian)
//            adapterMataPencaharian.notifyDataSetChanged()
//        }
    }

    private fun getPreseneter(): Form3DPPTPresenter? {
        form3DPPTPresenter = Form3DPPTPresenter()
        form3DPPTPresenter.onAttach(this)
        return form3DPPTPresenter
    }

    override fun onsuccessgetdaftarMataPencharian(resdataMataPencaharian: MutableList<ResponseDataMataPencaharian>) {

    }

    override fun onResume() {
        super.onResume()
        btnprevsubjek3?.setOnClickListener(this)
        btnnextsubjek3?.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        btnprevsubjek3?.setOnClickListener(null)
        btnnextsubjek3?.setOnClickListener(null)
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btnnextsubjek3) {
            hideKeyboard()
            dataFormSubjek3.clear()
            Utils.deletedataform3(activity!!)

            val getdataMataPencharian = adapterMataPencaharian.getdataPencaharian()
            val midmataPencharian: ArrayList<String> = ArrayList()
            val mnamamataPencharian: ArrayList<String> = ArrayList()
            val mnamamataPencharianLain: ArrayList<String> = ArrayList()
            val mjumlahMataPencharian: ArrayList<String> = ArrayList()

            loop@ for (i in getdataMataPencharian.indices) {
                when {
                    getdataMataPencharian[i].livelihoodLiveId!!.isEmpty() || getdataMataPencharian[i].livelihoodIncome!!.isEmpty() -> {
                        validdataMataPencaharian = false
                        dialogValidate!!.show()
                        break@loop
                    }
                    getdataMataPencharian[i].livelihoodLiveName!! == "Lain-Nya" && getdataMataPencharian[i].livelihoodIncome!!.isEmpty() ||
                            getdataMataPencharian[i].livelihoodLiveName!! == "Lain-Nya" && getdataMataPencharian[i].livelihoodOther!!.isEmpty() -> {
                        dialogValidate!!.show()
                        validdataMataPencaharian = false
                        break@loop
                    }
                    else -> {
                        midmataPencharian.add(getdataMataPencharian[i].livelihoodLiveId!!)
                        mnamamataPencharian.add(getdataMataPencharian[i].livelihoodLiveName!!)
                        mnamamataPencharianLain.add(getdataMataPencharian[i].livelihoodOther!!)
                        mjumlahMataPencharian.add(getdataMataPencharian[i].livelihoodIncome!!)

                        validdataMataPencaharian = true
                    }
                }
            }

//            if (validdataMataPencaharian) {
                dataFormSubjek3.add(DataFormSubjek3(
//                        midmataPencharian,
//                        mnamamataPencharianLain,
//                        mjumlahMataPencharian,
//                        mnamamataPencharian
                edNotes.text.toString()
                )
                )

                val jsonBBM = Gson().toJson(dataFormSubjek3)
                Utils.savedataform3(jsonBBM, activity!!)

                if (mListener != null) {
                    mListener!!.onNextPressed(this)
                }
//            }
        } else if (view.id == R.id.btnprevsubjek3) {
            if (mListener != null)
                mListener!!.onBackPressed(this)
        }
    }

    fun addItem(positon: Int) {
        val dataMataPencaharian = DataMataPencaharian()
        dataMataPencaharian.livelihoodLiveId = ""
        dataMataPencaharian.livelihoodLiveName = ""
        dataMataPencaharian.livelihoodOther = ""
        dataMataPencaharian.livelihoodIncome = ""

        adapterMataPencaharian.addItems(dataMataPencaharian)
        adapterMataPencaharian.notifyDataSetChanged()
//        rvmataPencaharian.scrollToPosition(adapterMataPencaharian.itemCount - 1)
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnStepThreeListener) {
            mListener = context
        } else {
            throw RuntimeException(("$context must implement OnFragmentInteractionListener"))
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    interface OnStepThreeListener {
        fun onBackPressed(fragment: Fragment)
        fun onNextPressed(fragment: Fragment)
    }

    companion object {
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        internal const val TAG = "FormSubjek3"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        fun newInstance(param1: String, param2: String): FormSubjek3 {
            val fragment = FormSubjek3()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}