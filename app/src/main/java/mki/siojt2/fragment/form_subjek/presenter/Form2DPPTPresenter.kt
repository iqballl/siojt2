package mki.siojt2.fragment.form_subjek.presenter

import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.fragment.form_subjek.view.FormSubjek2View
import mki.siojt2.utils.rxJava.RxUtils

class Form2DPPTPresenter : BasePresenter(), Form2MVVPPresenter {

    private var disposables: CompositeDisposable = CompositeDisposable()
    override fun getdaftarSumberIdentitas() {
        disposables.add(
                dataManager.getdaftarSumberIdentitas()
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdaftarSumberIdentitas(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    private fun view(): FormSubjek2View {
        return getView() as FormSubjek2View
    }
}