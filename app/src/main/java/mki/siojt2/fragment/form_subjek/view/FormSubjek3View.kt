package mki.siojt2.fragment.form_subjek.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.master_data_subjek.ResponseDataMataPencaharian

interface FormSubjek3View: MvpView {
    fun onsuccessgetdaftarMataPencharian(resdataMataPencaharian: MutableList<ResponseDataMataPencaharian>)
}