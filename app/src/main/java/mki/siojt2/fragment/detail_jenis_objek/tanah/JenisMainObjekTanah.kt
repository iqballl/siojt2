package mki.siojt2.fragment.detail_jenis_objek.tanah

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_main_detail_objek_tanah.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment

class JenisMainObjekTanah : BaseFragment(), DialogMenuObjekTanah.OnChangeTanahFragmentListener {

    // TODO: Rename and change types of parameters
    private var mParam1: Int? = null
    private var mParam2: Int? = null

    var codeKategoriObjekTanah: Int? = 1
    var nameKategoriObjekTanah: String? = "Lokasi Tanah"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getInt(ARG_PARAM1)
            mParam2 = arguments!!.getInt(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_detail_objek_tanah, container, false)
    }

    override fun setUp(view: View) {

        tvstatuscurrentmenuObjekTanah.text = nameKategoriObjekTanah

//        setFragment(JenisDetailObjekTanah1.newInstance(mParam1!!, mParam2!!), JenisDetailObjekTanah1.TAG)
        setFragment(JenisDetailObjekTanah1MapBox.newInstance(mParam1!!, mParam2!!), JenisDetailObjekTanah1MapBox.TAG)

        cardmainMenuObjekTanah.setOnClickListener {
            try {
                val dialogPopup = DialogMenuObjekTanah.newInstance(codeKategoriObjekTanah!!, nameKategoriObjekTanah!!, mParam1!!, mParam2!!)
                //val transaction = childFragmentManager.beginTransaction()

                dialogPopup.setTargetFragment(this@JenisMainObjekTanah, 0)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    fragmentManager?.beginTransaction()?.add(dialogPopup, "dialog_menu_tanah")?.commit()
                else
                    activity!!.supportFragmentManager.beginTransaction().add(dialogPopup, "dialog_menu_tanah").commit()
                //dialogPopup.show(fragmentManager, "dialog_menu_tanah")

            } catch (e: Exception) {
                Log.e("ClickMenuObject", "exception", e)
            }
        }

    }

    private fun setFragment(fragment: Fragment?, fragmentTag: String) {
        if (fragment != null) {
            refreshBackStack()
            val transact = childFragmentManager.beginTransaction()
            transact.setCustomAnimations(R.anim.fragment_enter_from_right, R.anim.fragment_exit_to_left, R.anim.fragment_enter_from_left, R.anim.fragment_exit_to_right)
            transact.replace(R.id.flmainObjekTanah, fragment, fragmentTag).commit()
        }

    }

    private fun refreshBackStack() {
        childFragmentManager.popBackStack(null, androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    override fun onSendFragmentTanah(fragment: Fragment, fragmentTag: String, codestatusCurrent: Int, namestatusCurrent: String) {
        codeKategoriObjekTanah = codestatusCurrent
        nameKategoriObjekTanah = namestatusCurrent
        tvstatuscurrentmenuObjekTanah.text = nameKategoriObjekTanah
        setFragment(fragment, fragmentTag)
    }


    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"
        internal const val TAG = "JenisMainObjekTanah"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: Int, param2: Int): JenisMainObjekTanah {
            val fragment = JenisMainObjekTanah()
            val args = Bundle()
            args.putInt(ARG_PARAM1, param1)
            args.putInt(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor