package mki.siojt2.fragment.detail_jenis_objek.bangunan

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_detail_objek_bangunan_1.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.fragment.detail_jenis_objek.bangunan.reycleview_adapter.AdapterFotoPropertiBangunan
import mki.siojt2.model.data_detail_objek_bangunan.ResponseDataDetailBangunan
import mki.siojt2.model.local_save_image.DataImageBuilding
import mki.siojt2.model.localsave.Bangunan
import mki.siojt2.ui.activity_detail_objek_bangunan.presenter.DetailBangunanPresenter
import mki.siojt2.ui.activity_detail_objek_bangunan.view.DetailBangunanView
import mki.siojt2.utils.realm.RealmController

@Suppress("DEPRECATION")
class JenisDetailObjekBangunan1 : BaseFragment(), DetailBangunanView {

    // TODO: Rename and change types of parameters
    private var mParam1: Int? = null
    private var mParam2: String? = ""

    lateinit var detailBangunanPresenter: DetailBangunanPresenter
    lateinit var realm:Realm

    lateinit var adapterFotoPropertiBangunan: AdapterFotoPropertiBangunan
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getInt(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_objek_bangunan_1, container, false)
    }

    override fun setUp(view: View) {
        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        adapterFotoPropertiBangunan = AdapterFotoPropertiBangunan(activity!!)
        val linearLayoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.HORIZONTAL, false)

        rlfotoPropertiBangunan.adapter = adapterFotoPropertiBangunan
        rlfotoPropertiBangunan.layoutManager = linearLayoutManager

        val results = realm.where(Bangunan::class.java).equalTo("BangunanIdTemp",mParam1).findFirst()
        //Log.d("result",results.toString())
        if (results != null) {
            if(results.projectBuildingTypeNameOther.isNotEmpty()){
                tvisiJenisBangunan.text = results.projectBuildingTypeNameOther
            }else{
                tvisiJenisBangunan.text = results.projectBuildingTypeName
            }
            tvisijmlLantai.text = results.projectBuildingFloorTotal.toString()

            tvisiluasLantai1.text = Html.fromHtml("${results.projectBuildingAreaUpperRoom} (m<sup><small>2</small></sup>)")
            tvisiluasLantai2.text = Html.fromHtml("${results.projectBuildingAreaLowerRoom} (m<sup><small>2</small></sup>)")

            val mdatafotoTanah: MutableList<DataImageBuilding> = ArrayList()
            mdatafotoTanah.addAll(results.dataImageBuilding)
            adapterFotoPropertiBangunan.clear()
            adapterFotoPropertiBangunan.addItems(mdatafotoTanah)
        }else{
            Log.e("data","kosong")
        }
        //getPresenter()?.getDetalBangunan(mParam1!!, Preference.accessToken)
    }

//    private fun getPresenter(): DetailBangunanPresenter?{
//        detailBangunanPresenter = DetailBangunanPresenter()
//        detailBangunanPresenter.onAttach(this)
//        return detailBangunanPresenter
//    }

    override fun onsuccessgetDetalBangunan(responseDataDetailBangunan: ResponseDataDetailBangunan) {
        //tvisiJenisBangunan.text = responseDataDetailBangunan.buildingDetail?.buildingTypeName ?: "-"
        tvisijmlLantai.text = responseDataDetailBangunan.buildingDetail?.projectBuildingFloorTotal.toString()

        tvisiluasLantai1.text = Html.fromHtml("${responseDataDetailBangunan.buildingDetail?.projectBuildingFloorAreaUpperRoom.toString()} (m<sup><small>2</small></sup>)")
        tvisiluasLantai2.text = Html.fromHtml("${responseDataDetailBangunan.buildingDetail?.projectBuildingFloorAreaLowerRoom.toString()} (m<sup><small>2</small></sup>)")

    }


    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2  = "param2"
        internal const val TAG = "JenisDetailObjekBangunan1"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: Int, param2: String): JenisDetailObjekBangunan1 {
            val fragment = JenisDetailObjekBangunan1()
            val args = Bundle()
            args.putInt(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor