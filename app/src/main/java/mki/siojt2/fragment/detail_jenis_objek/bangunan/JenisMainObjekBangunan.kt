package mki.siojt2.fragment.detail_jenis_objek.bangunan

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_main_detail_objek_bangunan.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment

class JenisMainObjekBangunan : BaseFragment(), DialogMenuObjekBangunan.OnChangeBangunanFragmentListener {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    var codeKategoriObjekBangunan: Int? = 1
    var nameKategoriObjekBangunan: String? = "Dasar"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_detail_objek_bangunan, container, false)
    }

    override fun setUp(view: View) {
        tvstatuscurrentmenuObjekBangunan.text = nameKategoriObjekBangunan

        //setFragment(JenisDetailObjekBangunan1.newInstance("", ""), JenisDetailObjekBangunan1.TAG)
        cardmainMenuObjekBangunan.setOnClickListener {
            /*try {
                val dialogPopup = DialogMenuObjekBangunan.newInstance(codeKategoriObjekBangunan!!, nameKategoriObjekBangunan!!)

                dialogPopup.setTargetFragment(this@JenisMainObjekBangunan, 0)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    fragmentManager?.beginTransaction()?.add(dialogPopup, "dialog_menu_bangunan")?.commit()
                else
                    childFragmentManager.beginTransaction().add(dialogPopup, "dialog_menu_bangunan").commit()
                //dialogPopup.show(fragmentManager, "dialog_menu_tanah")

            } catch (e: Exception) {
                Log.e("ClickMenuObject", "exception", e)
            }*/
        }
    }

    private fun setFragment(fragment: Fragment?, fragmentTag: String) {
        if (fragment != null) {
            refreshBackStack()
            val transact = childFragmentManager.beginTransaction()
            transact.setCustomAnimations(R.anim.fragment_enter_from_right, R.anim.fragment_exit_to_left, R.anim.fragment_enter_from_left, R.anim.fragment_exit_to_right)
            transact.replace(R.id.flmainObjekBangunan, fragment, fragmentTag).commit()
        }

    }

    private fun refreshBackStack() {
        childFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    override fun onSendFragmentBangunan(fragment: Fragment, fragmentTag: String, codestatusCurrent: Int, namestatusCurrent: String) {
        codeKategoriObjekBangunan = codestatusCurrent
        nameKategoriObjekBangunan = namestatusCurrent
        tvstatuscurrentmenuObjekBangunan.text = nameKategoriObjekBangunan
        setFragment(fragment, fragmentTag)

    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"
        internal const val TAG = "JenisMainObjekBangunan"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): JenisMainObjekBangunan {
            val fragment = JenisMainObjekBangunan()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor