package mki.siojt2.fragment.detail_jenis_objek.bangunan

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle

import mki.siojt2.R
import androidx.appcompat.app.AlertDialog
import android.view.*
import android.widget.Toast
import android.content.Context
import android.graphics.Paint
import android.graphics.Paint.STRIKE_THRU_TEXT_FLAG
import kotlinx.android.synthetic.main.item_ubah_menu_objek_bangunan.view.*
import mki.siojt2.constant.Constant


class DialogMenuObjekBangunan : androidx.fragment.app.DialogFragment() {

    lateinit var mView: View
    private var mListenerBangunan: OnChangeBangunanFragmentListener? = null

    private var statusCurrentMenuBangunan: Int? = null
    private var statusCurrentMenuNameBangunan: String? = null

    private var currentprojectbuildingId: Int? = null
    private var currentprojectpartyid: String? = ""

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_PARAM1 = "statusCurrentMenuObjekBangunan"
        private const val ARG_PARAM2 = "statusCurrentMenuNameObjekBangunan"

        private const val ARG_PARAM3 = "projectBuildingid"
        private const val ARG_PARAM4 = "projectpartyid"
        internal const val TAG = "DialogMenuObjekBangunan"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: Int, param2: String, projectbuildingId: Int, projectpartyid: String): DialogMenuObjekBangunan {
            val fragment = DialogMenuObjekBangunan()
            val args = Bundle()
            args.putInt(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            args.putInt(ARG_PARAM3, projectbuildingId)
            args.putString(ARG_PARAM4, projectpartyid)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            val mArgs = arguments
            statusCurrentMenuBangunan = mArgs?.getInt(ARG_PARAM1)
            statusCurrentMenuNameBangunan = mArgs?.getString(ARG_PARAM2)
            currentprojectbuildingId = mArgs?.getInt(ARG_PARAM3)
            currentprojectpartyid = mArgs?.getString(ARG_PARAM4)
        }
    }


    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val dialog = super.onCreateDialog(savedInstanceState)

        val dialogBuilder = AlertDialog.Builder(activity!!)
        val inflater = LayoutInflater.from(context)
        val dialogView = inflater.inflate(R.layout.item_ubah_menu_objek_bangunan, null)
        dialogBuilder.setView(dialogView)

        initSubViews(dialogView)

        // request a window without the title
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        return dialogBuilder.create()
    }


    private fun initSubViews(rootView: View) {

        val btncloseDialog = rootView.ivcloseDialogMenuObjekBangunan

        val btndasarBangunan = rootView.cardasarBangunan
        val btnspekBangunan = rootView.cardspekBangunan
        val btnfasilitasBangunan = rootView.cardfasilitasBangunan
        val btnsarprasBangunan = rootView.cardsarprasBangunan

        val txdasarBangunan = rootView.tvdasarBangunan
        val txtspekBangunan = rootView.tvspekBangunan
        val txtfasilitasBangunan = rootView.tvfasilitasTanah
        val txtsarprasBangunan = rootView.tvsarprasTanah

        when (statusCurrentMenuBangunan) {
            1 -> {
                btndasarBangunan.isEnabled = false
                btnspekBangunan.isEnabled = true
                btnfasilitasBangunan.isEnabled = true
                btnsarprasBangunan.isEnabled = true
                txdasarBangunan.paintFlags = txdasarBangunan.paintFlags or STRIKE_THRU_TEXT_FLAG
                txtspekBangunan.paintFlags = txtspekBangunan.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtfasilitasBangunan.paintFlags = txtfasilitasBangunan.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtsarprasBangunan.paintFlags = txtsarprasBangunan.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
            }
            2 -> {
                btndasarBangunan.isEnabled = true
                btnspekBangunan.isEnabled = false
                btnfasilitasBangunan.isEnabled = true
                btnsarprasBangunan.isEnabled = true
                txdasarBangunan.paintFlags = txdasarBangunan.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtspekBangunan.paintFlags = txtspekBangunan.paintFlags or STRIKE_THRU_TEXT_FLAG
                txtfasilitasBangunan.paintFlags = txtfasilitasBangunan.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtsarprasBangunan.paintFlags = txtsarprasBangunan.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
            }
            3 -> {
                btndasarBangunan.isEnabled = true
                btnspekBangunan.isEnabled = true
                btnfasilitasBangunan.isEnabled = false
                btnsarprasBangunan.isEnabled = true
                txdasarBangunan.paintFlags = txdasarBangunan.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtspekBangunan.paintFlags = txtspekBangunan.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtfasilitasBangunan.paintFlags = txtfasilitasBangunan.paintFlags or STRIKE_THRU_TEXT_FLAG
                txtsarprasBangunan.paintFlags = txtsarprasBangunan.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
            }
            4 -> {
                btndasarBangunan.isEnabled = true
                btnspekBangunan.isEnabled = true
                btnfasilitasBangunan.isEnabled = true
                btnsarprasBangunan.isEnabled = false
                txdasarBangunan.paintFlags = txdasarBangunan.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtspekBangunan.paintFlags = txtspekBangunan.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtfasilitasBangunan.paintFlags = txtfasilitasBangunan.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtsarprasBangunan.paintFlags = txtsarprasBangunan.paintFlags or STRIKE_THRU_TEXT_FLAG
            }
        }

        btncloseDialog.setOnClickListener {
            dismiss()
        }

        btndasarBangunan.setOnClickListener {
            if (mListenerBangunan != null) {
                statusCurrentMenuBangunan = 1
                statusCurrentMenuNameBangunan = Constant.MENUBANGUNAN1
                mListenerBangunan!!.onSendFragmentBangunan(
                        JenisDetailObjekBangunan1.newInstance(currentprojectbuildingId!!, currentprojectpartyid.toString()),
                        JenisDetailObjekBangunan1.TAG,
                        statusCurrentMenuBangunan!!,
                        statusCurrentMenuNameBangunan!!
                )

            }
            dialog!!.cancel()
        }

        btnspekBangunan.setOnClickListener {
            if (mListenerBangunan != null) {
                statusCurrentMenuBangunan = 2
                statusCurrentMenuNameBangunan = Constant.MENUBANGUNAN2
                mListenerBangunan!!.onSendFragmentBangunan(
                        JenisDetailObjekBangunan2.newInstance(currentprojectbuildingId!!, currentprojectpartyid.toString()),
                        JenisDetailObjekBangunan2.TAG,
                        statusCurrentMenuBangunan!!,
                        statusCurrentMenuNameBangunan!!
                )

            }
            dialog!!.cancel()
        }

        btnfasilitasBangunan.setOnClickListener {
            if (mListenerBangunan != null) {
                statusCurrentMenuBangunan = 3
                statusCurrentMenuNameBangunan = Constant.MENUBANGUNAN3
                mListenerBangunan!!.onSendFragmentBangunan(
                        JenisDetailObjekBangunan3.newInstance("", ""),
                        JenisDetailObjekBangunan3.TAG,
                        statusCurrentMenuBangunan!!,
                        statusCurrentMenuNameBangunan!!
                )

            }
            dialog!!.cancel()
        }

        btnsarprasBangunan.setOnClickListener {
            if (mListenerBangunan != null) {
                statusCurrentMenuBangunan = 4
                statusCurrentMenuNameBangunan = Constant.MENUBANGUNAN4
                mListenerBangunan!!.onSendFragmentBangunan(
                        JenisDetailObjekBangunan4.newInstance("", ""),
                        JenisDetailObjekBangunan4.TAG,
                        statusCurrentMenuBangunan!!,
                        statusCurrentMenuNameBangunan!!
                )

            }
            dialog!!.cancel()
        }


    }

    fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    override fun onStart() {
        super.onStart()

        //set transparent background
        val window = dialog!!.window
        if (window != null) {
            val width = WindowManager.LayoutParams.MATCH_PARENT
            val height = WindowManager.LayoutParams.MATCH_PARENT
            dialog!!.window!!.setLayout(width, height)
        }
        window!!.setBackgroundDrawableResource(android.R.color.transparent)

        //disable buttons from dialog
        val alertDialog = dialog as AlertDialog
        alertDialog.getButton(Dialog.BUTTON_POSITIVE).isEnabled = false
        alertDialog.getButton(Dialog.BUTTON_NEGATIVE).isEnabled = false
        alertDialog.getButton(Dialog.BUTTON_NEUTRAL).isEnabled = false
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        //this.mListenerBangunan = targetFragment as OnChangeBangunanFragmentListener
        try {
            mListenerBangunan = activity as OnChangeBangunanFragmentListener?
        } catch (e: ClassCastException) {
            //Log.e(TAG, "onAttach: ClassCastException: " + e.message)
            throw ClassCastException("Calling Fragment must implement OnChangeTanahFragmentListener : ${e.message}")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListenerBangunan = null

    }

    interface OnChangeBangunanFragmentListener {
        //void onFragmentInteraction(Uri uri);
        fun onSendFragmentBangunan(fragment: androidx.fragment.app.Fragment, fragmentTag: String, codestatusCurrent: Int, namestatusCurrent: String)
    }


}