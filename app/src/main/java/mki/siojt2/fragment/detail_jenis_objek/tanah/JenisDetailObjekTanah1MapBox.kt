package mki.siojt2.fragment.detail_jenis_objek.tanah

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.*
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_detail_objek_tanah_1_mapbox.view.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.fragment.detail_jenis_objek.tanah.adapter_recycleview.AdapterFotoPropertiTanah
import mki.siojt2.model.data_detail_tanah.ResponseDataDetailTanah2
import mki.siojt2.model.local_save_image.DataImageLand
import mki.siojt2.model.localsave.ProjectLand
import mki.siojt2.ui.activity_detail_tanah.presenter.DetailTanahPresenter
import mki.siojt2.ui.activity_detail_tanah.view.DetailTanahView
import mki.siojt2.utils.realm.RealmController

class JenisDetailObjekTanah1MapBox : BaseFragment(),
        DetailTanahView {

    lateinit var detailTanahPresenter: DetailTanahPresenter
    lateinit var viewJenisDetailObjekTanah1: View
    lateinit var mMapView: MapView
    private val MY_PERMISSIONS_REQUEST_LOCATION = 99

    // TODO: Rename and change types of parameters
    private var mParam1: Int? = null
    private var mParam2: Int? = null

    lateinit var realm: Realm
    private var mMapBoxMap: MapboxMap? = null
    private var mLatLang: Feature? = null
    private val SOURCE_ID = "SOURCE_ID"
    private val ICON_ID = "ICON_ID"
    private val LAYER_ID = "LAYER_ID"

    /* Map */

    lateinit var adapterFotoPropertiTanah: AdapterFotoPropertiTanah

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getInt(ARG_PARAM1)
            mParam2 = arguments!!.getInt(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        Mapbox.getInstance(context!!, getString(R.string.mapbox_access_token))
        viewJenisDetailObjekTanah1 = inflater.inflate(R.layout.fragment_detail_objek_tanah_1_mapbox, container, false)
        mMapView = viewJenisDetailObjekTanah1.mapviewDetail as MapView
        mMapView.onCreate(savedInstanceState)
        mMapView.onResume()
        mMapView.getMapAsync {
            mMapBoxMap = it

            if ((ContextCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {

                val latLng: LatLng?
                var position: Feature? = null
                if (mLatLang != null){
                    position = mLatLang as Feature
                    val point = mLatLang?.geometry() as Point
                    latLng = LatLng(point.latitude(),point.longitude())
                }
                else{
                    position = Feature.fromGeometry(
                                    Point.fromLngLat(106.908182, -6.161941))
                    latLng = LatLng(106.908182, -6.161941)
                }

                mMapBoxMap?.setStyle(
                        Style.Builder().fromUri(Style.LIGHT) // Add the SymbolLayer icon image to the map style
                        .withImage(ICON_ID, BitmapFactory.decodeResource(
                                this.resources, R.drawable.mapbox_marker_icon_default)) // Adding a GeoJson source for the SymbolLayer icons.
                        .withSource(GeoJsonSource(SOURCE_ID,
                                FeatureCollection.fromFeature(position!!))) // Adding the actual SymbolLayer to the map style. An offset is added that the bottom of the red
                        // marker icon gets fixed to the coordinate, rather than the middle of the icon being fixed to
                        // the coordinate point. This is offset is not always needed and is dependent on the image
                        // that you use for the SymbolLayer icon.
                        .withLayer(SymbolLayer(LAYER_ID, SOURCE_ID)
                                .withProperties(
                                        iconImage(ICON_ID),
                                        iconAllowOverlap(true),
                                        iconIgnorePlacement(true)
                                )
                        ), Style.OnStyleLoaded { })


                // For zooming automatically to the location of the marker
                val cameraPosition = CameraPosition.Builder().target(latLng).zoom(15.0).build()
                mMapBoxMap!!.animateCamera(CameraUpdateFactory.newLatLng(latLng!!))
                mMapBoxMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            }
        }
        return viewJenisDetailObjekTanah1

    }

    @SuppressLint("SetTextI18n")
    override fun setUp(view: View) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission()
        }

        adapterFotoPropertiTanah = AdapterFotoPropertiTanah(activity!!)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity!!, androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL, false)

        viewJenisDetailObjekTanah1.rlfotoPropertiTanah.adapter = adapterFotoPropertiTanah
        viewJenisDetailObjekTanah1.rlfotoPropertiTanah.layoutManager = linearLayoutManager

        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        val results = realm.where(ProjectLand::class.java).equalTo("LandIdTemp", mParam1).findFirst()
        Log.d("result", results.toString())
        Log.d("asddxx2",mParam1.toString())
        if (results != null) {
            viewJenisDetailObjekTanah1.tvisiAlamat.text = "${results.landComplekName ?: ""} " +
                    "${results.landStreet} " +
                    "${results.landBlok ?: ""} " +
                    "No.${results.landNumber ?: ""} " +
                    "RT ${results.landRT} " +
                    "RW ${results.landRW} " +
                    "${results.landVillageName} " +
                    "${results.landDistrictName} " +
                    "${results.landRegencyName} " +
                    "${results.landProvinceName} " +
                    results.landPostalCode

            //jenis tanah
            val jtname1 = results.landUtilizationLandUtilizationName
            val jtname2 = jtname1.replace("[", "['")
            val jtname3 = jtname2.replace("]", "']")
            val jtname = jtname3.replace(", ", "','")
            val listjtName = Gson().fromJson(jtname, Array<String>::class.java).toList()

            val landUtilizationNameOther = results.landUtilizationLandUtilizationNameOther
            val landUtilizationNameOther2 = landUtilizationNameOther.replace("[", "['")
            val landUtilizationNameOther3 = landUtilizationNameOther2.replace("]", "']")
            val landUtilizationNameOther4 = landUtilizationNameOther3.replace(", ", "','")
            val landUtilizationNameOtherRef = Gson().fromJson(landUtilizationNameOther4, Array<String>::class.java).toList()

            if(listjtName.isNotEmpty()){
                for(i in listjtName.indices){
                    if(landUtilizationNameOtherRef[i].isNotEmpty()){
                        viewJenisDetailObjekTanah1.tvUtilizationLand.text = "${viewJenisDetailObjekTanah1.tvUtilizationLand.text} -${listjtName[i]} (${landUtilizationNameOtherRef[i]})\n"
                    }
                    else {
                        viewJenisDetailObjekTanah1.tvUtilizationLand.text = "${viewJenisDetailObjekTanah1.tvUtilizationLand.text} -${listjtName[i]}\n"
                    }
                }
            }

            mLatLang = Feature.fromGeometry(
                    Point.fromLngLat(results.landLangitude.toDouble(), results.landLatitude.toDouble()))

            val mdatafotoTanah: MutableList<DataImageLand> = ArrayList()
            mdatafotoTanah.addAll(results.dataImageLands)
            adapterFotoPropertiTanah.clear()
            adapterFotoPropertiTanah.addItems(mdatafotoTanah)

            val splitJob = results.livelihoodLiveName.replace("[","").replace("]","").split(",")
            val splitJobIncome = results.livelihoodIncome.replace("[","").replace("]","").split(",")
            if(splitJob.isNotEmpty()){
                for((i, value) in splitJob.withIndex()) {
                    viewJenisDetailObjekTanah1.tvJob.text = "${viewJenisDetailObjekTanah1.tvJob.text}- $value (Rp ${splitJobIncome[i] ?: ""}) \n"
                }
            }
            else {
                viewJenisDetailObjekTanah1.tvJob.text = "${results.livelihoodLiveName.replace("[","").replace("]","")} - ${results.livelihoodIncome.replace("[","").replace("]","")}"
            }

        } else {
            Log.e("data", "kosong")
        }
        //getPresenter()?.getdatadetaiLTanah(mParam1!!, Preference.accessToken)

    }

    private fun getPresenter(): DetailTanahPresenter? {
        detailTanahPresenter = DetailTanahPresenter()
        detailTanahPresenter.onAttach(this)
        return detailTanahPresenter
    }

    @SuppressLint("SetTextI18n")
    override fun onsuccesgetdatadetaiLTanah(responseDataDetailTanah: ResponseDataDetailTanah2) {
        viewJenisDetailObjekTanah1.tvisiAlamat.text = "${responseDataDetailTanah.landDetail?.projectLandComplexName ?: ""} " +
                "${responseDataDetailTanah.landDetail?.projectLandStreetName} " +
                "${responseDataDetailTanah.landDetail?.projectLandBlock ?: ""} " +
                "No.${responseDataDetailTanah.landDetail?.projectLandNumber ?: ""} " +
                "${responseDataDetailTanah.landDetail?.projectLandVillageName} " +
                "${responseDataDetailTanah.landDetail?.districtName} " +
                "${responseDataDetailTanah.landDetail?.regencyName} " +
                "${responseDataDetailTanah.landDetail?.provinceName} " +
                "${responseDataDetailTanah.landDetail?.projectLandPostalCode}"
    }


    override fun onStop() {
        super.onStop()

    }

    override fun onLowMemory() {
        super.onLowMemory()
        try {
            mMapView.onLowMemory()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDetach() {
        super.onDetach()
        getPresenter()?.onDetach()
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            mMapView.onDestroy()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        getPresenter()?.onDetach()
    }

    private fun checkLocationPermission(): Boolean {
        return if ((ContextCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS_REQUEST_LOCATION)
            } else {
                ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS_REQUEST_LOCATION)
            }
            false
        } else {
            true
        }
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"
        internal const val TAG = "JenisDetailObjekTanah1"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: Int, param2: Int): JenisDetailObjekTanah1MapBox {
            val fragment = JenisDetailObjekTanah1MapBox()
            val args = Bundle()
            args.putInt(ARG_PARAM1, param1)
            args.putInt(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor