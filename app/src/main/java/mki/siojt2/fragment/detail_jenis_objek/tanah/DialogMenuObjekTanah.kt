package mki.siojt2.fragment.detail_jenis_objek.tanah

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import mki.siojt2.R
import androidx.appcompat.app.AlertDialog
import android.view.*
import android.widget.Toast
import android.content.Context
import android.graphics.Paint
import android.graphics.Paint.STRIKE_THRU_TEXT_FLAG
import kotlinx.android.synthetic.main.item_ubah_menu_objek_tanah.view.*
import mki.siojt2.constant.Constant

class DialogMenuObjekTanah : androidx.fragment.app.DialogFragment() {

    lateinit var mView: View
    private var mListenerTanah: OnChangeTanahFragmentListener? = null

    private var statusCurrentMenuTanah: Int? = null
    private var statusCurrentMenuNameTanah: String? = null

    private var currentprojectlandid: Int? = null
    private var currentprojectpartyid: Int? = null

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_PARAM1 = "statusCurrentMenuObjekTanah"
        private const val ARG_PARAM2 = "statusCurrentMenuNameObjekTanah"

        private const val ARG_PARAM3 = "projectlandid"
        private const val ARG_PARAM4 = "projectpartyid"
        internal const val TAG = "DialogMenuObjekTanah"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: Int, param2: String, projectlandid: Int, projectpartyid: Int): DialogMenuObjekTanah {
            val fragment = DialogMenuObjekTanah()
            val args = Bundle()
            args.putInt(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            args.putInt(ARG_PARAM3, projectlandid)
            args.putInt(ARG_PARAM4, projectpartyid)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            val mArgs = arguments
            statusCurrentMenuTanah = mArgs?.getInt(ARG_PARAM1)
            statusCurrentMenuNameTanah = mArgs?.getString(ARG_PARAM2)
            currentprojectlandid = mArgs?.getInt(ARG_PARAM3)
            currentprojectpartyid = mArgs?.getInt(ARG_PARAM4)
        }
    }


    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val dialog = super.onCreateDialog(savedInstanceState)
        val dialogBuilder = AlertDialog.Builder(activity!!)
        val inflater = LayoutInflater.from(context)
        val dialogView = inflater.inflate(R.layout.item_ubah_menu_objek_tanah, null)
        dialogBuilder.setView(dialogView)

        initSubViews(dialogView)

        // request a window without the title
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        return dialogBuilder.create()
    }


    private fun initSubViews(rootView: View) {

        val btncloseDialog = rootView.ivcloseDialogMenuObjekTanah

        val btnLokTanah = rootView.cardLokasiTanah

        val btnBidangTanah = rootView.cardbidangTanah
        val btnLegalTanah = rootView.cardlegalTanah

        val txtLoktanah = rootView.tvLokasiTanah
        val txtBidangTanah = rootView.tvbidangTanah
        val txtLegalTanah = rootView.tvlegalTanah

        btncloseDialog.setOnClickListener {
            dismiss()
        }

        when (statusCurrentMenuTanah) {
            1 -> {
                btnLokTanah.isEnabled = false
                btnBidangTanah.isEnabled = true
                btnLegalTanah.isEnabled = true
                txtLoktanah.paintFlags = txtLoktanah.paintFlags or STRIKE_THRU_TEXT_FLAG
                txtBidangTanah.paintFlags = txtBidangTanah.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtLegalTanah.paintFlags = txtLegalTanah.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()

            }
            2 -> {
                btnLokTanah.isEnabled = true
                btnBidangTanah.isEnabled = false
                btnLegalTanah.isEnabled = true
                txtLoktanah.paintFlags = txtLoktanah.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtBidangTanah.paintFlags = txtBidangTanah.paintFlags or STRIKE_THRU_TEXT_FLAG
                txtLegalTanah.paintFlags = txtLegalTanah.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
            }
            3 -> {
                btnLokTanah.isEnabled = true
                btnBidangTanah.isEnabled = true
                btnLegalTanah.isEnabled = false
                txtLoktanah.paintFlags = txtLoktanah.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtBidangTanah.paintFlags = txtBidangTanah.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtLegalTanah.paintFlags = txtLoktanah.paintFlags or STRIKE_THRU_TEXT_FLAG
            }

        }


        btnLokTanah.setOnClickListener {
            if (mListenerTanah != null) {
                statusCurrentMenuTanah = 1
                statusCurrentMenuNameTanah = Constant.MENUTANAH1
                mListenerTanah!!.onSendFragmentTanah(
//                        JenisDetailObjekTanah1.newInstance(currentprojectlandid!!, currentprojectpartyid!!),
//                        JenisDetailObjekTanah1.TAG,
                        JenisDetailObjekTanah1MapBox.newInstance(currentprojectlandid!!, currentprojectpartyid!!),
                        JenisDetailObjekTanah1MapBox.TAG,
                        statusCurrentMenuTanah!!,
                        statusCurrentMenuNameTanah!!
                )

            }
            dialog!!.dismiss()
        }

        btnBidangTanah.setOnClickListener {
            if (mListenerTanah != null) {
                statusCurrentMenuTanah = 2
                statusCurrentMenuNameTanah = Constant.MENUTANAH2
                mListenerTanah!!.onSendFragmentTanah(
                        JenisDetailObjekTanah2.newInstance(currentprojectlandid!!, currentprojectpartyid!!),
                        JenisDetailObjekTanah2.TAG,
                        statusCurrentMenuTanah!!,
                        statusCurrentMenuNameTanah!!
                )
            }
            dialog!!.dismiss()
        }

        btnLegalTanah.setOnClickListener {
            if (mListenerTanah != null) {
                statusCurrentMenuTanah = 3
                statusCurrentMenuNameTanah = Constant.MENUTANAH3
                mListenerTanah!!.onSendFragmentTanah(
                        JenisDetailObjekTanah3.newInstance(currentprojectlandid!!, currentprojectpartyid!!),
                        JenisDetailObjekTanah3.TAG,
                        statusCurrentMenuTanah!!,
                        statusCurrentMenuNameTanah!!
                )
            }
            dialog!!.dismiss()
        }


    }

    fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    override fun onStart() {
        super.onStart()

        //set transparent background
        val window = dialog!!.window
        if (window != null) {
            val width = WindowManager.LayoutParams.MATCH_PARENT
            val height = WindowManager.LayoutParams.MATCH_PARENT
            dialog!!.window!!.setLayout(width, height)
        }
        window!!.setBackgroundDrawableResource(android.R.color.transparent)

        //disable buttons from dialog
        val alertDialog = dialog as AlertDialog
        alertDialog.getButton(Dialog.BUTTON_POSITIVE).isEnabled = false
        alertDialog.getButton(Dialog.BUTTON_NEGATIVE).isEnabled = false
        alertDialog.getButton(Dialog.BUTTON_NEUTRAL).isEnabled = false
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mListenerTanah = targetFragment as OnChangeTanahFragmentListener
        /* try {
             this.mListenerTanah = context as OnChangeTanahFragmentListener?
         } catch (e: ClassCastException) {
             //Log.e(TAG, "onAttach: ClassCastException: " + e.message)
             throw ClassCastException("Calling Fragment must implement OnChangeTanahFragmentListener : ${e.message}")
         }*/
    }

    override fun onDetach() {
        super.onDetach()
        mListenerTanah = null

    }

    interface OnChangeTanahFragmentListener {
        //void onFragmentInteraction(Uri uri);
        fun onSendFragmentTanah(fragment: androidx.fragment.app.Fragment, fragmentTag: String, codestatusCurrent: Int, namestatusCurrent: String)
    }


}