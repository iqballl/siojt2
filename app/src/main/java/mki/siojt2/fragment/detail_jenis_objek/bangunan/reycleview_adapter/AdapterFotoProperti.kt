package mki.siojt2.fragment.detail_jenis_objek.bangunan.reycleview_adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_foto_properti_horizontal.view.*
import mki.siojt2.R
import mki.siojt2.base.BaseViewHolder
import mki.siojt2.model.ResponDataFotoProperti


class AdapterFotoProperti(context: Context) : RecyclerView.Adapter<BaseViewHolder>() {

    private val mContext: Context? = context
    val mDataset: MutableList<ResponDataFotoProperti>? = ArrayList()
    var rowLayout = R.layout.item_foto_properti_horizontal
    var emptyrowLayout = R.layout.item_empty_view
    var pos = 0


    private var mCallback: Callback? = null

    fun setCallback(callback: Callback) {
        mCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            VIEW_TYPE_NORMAL -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(rowLayout, parent, false))
            VIEW_TYPE_EMPTY -> EmptyViewHolder(
                    LayoutInflater.from(parent.context).inflate(R.layout.item_empty_view, parent, false))
            else -> EmptyViewHolder(LayoutInflater.from(parent.context).inflate(emptyrowLayout, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (mDataset != null && mDataset.isNotEmpty()) {
            VIEW_TYPE_NORMAL
        } else {
            VIEW_TYPE_EMPTY
        }
    }

    override fun getItemCount(): Int {
        return if (mDataset!!.isNotEmpty()) {
            mDataset.size
        } else {
            1
        }
    }

    /*inner class RuangViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var r_no: TextView = itemView.findViewById(R.id.rowNo) as TextView
        var r_tvTitle: TextView = itemView.findViewById(R.id.tvTitle) as TextView
        var r_tvContent1: TextView = itemView.findViewById(R.id.tvContent1) as TextView
        var r_tvContent2: TextView = itemView.findViewById(R.id.tvContent2) as TextView

    }*/

    fun clear() {
        this.mDataset!!.clear()
    }

    fun addItems(data: List<ResponDataFotoProperti>) {
        this.mDataset!!.addAll(data)
        notifyDataSetChanged()
    }

    interface Callback {
        fun onRepoEmptyViewRetryClick()
    }

    inner class ViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }

        override fun onBind(position: Int) {
            super.onBind(position)

            val dataList = mDataset!![position]
            inflateData(dataList)

            Picasso.get().load(dataList.imageUrl.toString())
                    .error(R.drawable.ic_close)
                    .into(itemView.imgProperti)


            /*Picasso.with(mContext)
                    .load(dataList.imageUrl.toString())
                    .error(R.drawable.ic_close)
                    .into(itemView.imgProperti)*/

        }

        private fun inflateData(dataList: ResponDataFotoProperti) {

        }
    }

    inner class EmptyViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }


    }

    companion object {
        const val VIEW_TYPE_EMPTY = 0
        const val VIEW_TYPE_NORMAL = 1
    }
}
