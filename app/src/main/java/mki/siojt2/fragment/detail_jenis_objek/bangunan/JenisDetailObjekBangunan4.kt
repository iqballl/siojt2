package mki.siojt2.fragment.detail_jenis_objek.bangunan

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_detail_objek_bangunan_4.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.fragment.detail_jenis_objek.bangunan.reycleview_adapter.AdapterFotoProperti
import mki.siojt2.model.ResponDataFotoProperti

class JenisDetailObjekBangunan4 : BaseFragment(), AdapterFotoProperti.Callback {


    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    private val android_version_names = arrayOf("Donut", "Eclair", "Froyo", "Gingerbread", "Honeycomb", "Ice Cream Sandwich", "Jelly Bean", "KitKat", "Lollipop", "Marshmallow")
    private val android_image_urls = arrayOf("https://i.annihil.us/u/prod/marvel/i/mg/9/30/538cd33e15ab7/standard_xlarge.jpg",
            "https://i.annihil.us/u/prod/marvel/i/mg/1/c0/537ba2bfd6bab/standard_xlarge.jpg",
            "https://i.annihil.us/u/prod/marvel/i/mg/6/a0/55b6a25e654e6/standard_xlarge.jpg",
            "https://i.annihil.us/u/prod/marvel/i/mg/5/c0/537ba730e05e0/standard_xlarge.jpg",
            "https://i.annihil.us/u/prod/marvel/i/mg/c/10/537ba5ff07aa4/standard_xlarge.jpg",
            "https://i.annihil.us/u/prod/marvel/i/mg/6/90/54ad7297b0a59/standard_xlarge.jpg",
            "https://i.annihil.us/u/prod/marvel/i/mg/9/30/538cd33e15ab7/standard_xlarge.jpg",
            "https://i.annihil.us/u/prod/marvel/i/mg/6/a0/55b6a25e654e6/standard_xlarge.jpg",
            "https://i.annihil.us/u/prod/marvel/i/mg/5/c0/537ba730e05e0/standard_xlarge.jpg",
            "https://i.annihil.us/u/prod/marvel/i/mg/c/10/537ba5ff07aa4/standard_xlarge.jpg")

    lateinit var adapterforotProperti: AdapterFotoProperti

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_objek_bangunan_4, container, false)
    }

    override fun setUp(view: View) {
        val listHeroes: ArrayList<ResponDataFotoProperti> = arrayListOf(
                ResponDataFotoProperti("Spider-Man", "https://farm2.staticflickr.com/1793/42937679651_3094ebb2b9_c.jpg"),
                ResponDataFotoProperti("Black Panther", "https://farm2.staticflickr.com/1731/27940806257_8067196b41_c.jpg"),
                ResponDataFotoProperti("Iron Man", "https://images.unsplash.com/photo-1528621764154-430964093299?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=60"),
                ResponDataFotoProperti("Dead Pool", "https://images.unsplash.com/photo-1470688090067-6d429c0b2600?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=60"),
                ResponDataFotoProperti("Captain Marvel", "https://images.unsplash.com/photo-1462953491269-9aff00919695?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=60"),
                ResponDataFotoProperti("Ant Man", "https://images.unsplash.com/photo-1497752531616-c3afd9760a11?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=60")
        )


        adapterforotProperti = AdapterFotoProperti(activity!!)
        adapterforotProperti.addItems(listHeroes)
        adapterforotProperti.setCallback(this)

        rlfotoPropertiBangunan.apply {
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity, androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL, false)
            //addItemDecoration(OverlapDecoration())
            setHasFixedSize(true)
            adapter = adapterforotProperti

        }

    }

    private fun prepareData(): MutableList<ResponDataFotoProperti> {
        val androidVersion1 = mutableListOf<ResponDataFotoProperti>()
        for (i in android_version_names.indices) {
            val androidVersion2 = ResponDataFotoProperti()
            androidVersion2.imageName = android_version_names[i]
            androidVersion2.imageUrl = android_image_urls[i]
            androidVersion1.add(androidVersion2)
        }
        return androidVersion1
    }

    override fun onRepoEmptyViewRetryClick() {

    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"
        internal const val TAG = "JenisDetailObjekBangunan4"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): JenisDetailObjekBangunan4 {
            val fragment = JenisDetailObjekBangunan4()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor