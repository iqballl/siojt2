package mki.siojt2.fragment.detail_jenis_objek.mesin

import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_detail_objek_mesin.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.fragment.detail_jenis_objek.mesin.recycleview.AdapterDaftarMesin
import mki.siojt2.model.ResponDataMesin

class JenisObjekMesinPerlatan : BaseFragment(), AdapterDaftarMesin.Callback {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    private lateinit var adapterpengajuanDPPT: AdapterDaftarMesin

    private val repositories = mutableListOf<ResponDataMesin>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_objek_mesin, container, false)
    }

    override fun setUp(view: View) {

        getBrands().forEach {
            if (it.mesinChecked){
                repositories.add(it)
            }
        }
       /* for (item: ResponDataMesin in getBrands()) {
            // ...
            if (item.mesinChecked){
                repositories.add(item)
            }
        }*/
        Log.d("dwi", repositories.toString())

        adapterpengajuanDPPT = AdapterDaftarMesin(activity!!)
        adapterpengajuanDPPT.setCallback(this)
        adapterpengajuanDPPT.addItems(repositories)
        rvjenisMesin.apply {
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
            setHasFixedSize(true)
            itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()
            adapter = adapterpengajuanDPPT
        }

    }

    private fun getBrands(): List<ResponDataMesin> {
        val modelList = ArrayList<ResponDataMesin>()
        modelList.add(ResponDataMesin("Adidas", "Mesin Produksi", true))
        modelList.add(ResponDataMesin("Nike", "Mesin Laboratorium", false))
        modelList.add(ResponDataMesin("Reebok", "Mesin Utilitas", true))
        modelList.add(ResponDataMesin("Boss", "Mesin Workshop/Bengkel", false))
        modelList.add(ResponDataMesin("Reebok", "Kendaraan", true))
        modelList.add(ResponDataMesin("Boss", "Alat Berat", true))
        modelList.add(ResponDataMesin("Reebok", "Kapal", false))
        modelList.add(ResponDataMesin("Boss", "Pesawat terbang", false))
        return modelList
    }

    override fun onRepoEmptyViewRetryClick() {

    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2  = "param2"
        internal const val TAG = "JenisObjekMesinPerlatan"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): JenisObjekMesinPerlatan {
            val fragment = JenisObjekMesinPerlatan()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor