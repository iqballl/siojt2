package mki.siojt2.fragment.detail_jenis_objek.tanah

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.google.gson.Gson
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_detail_objek_tanah_2.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.model.data_detail_tanah.ResponseDataDetailTanah2
import mki.siojt2.model.localsave.ProjectLand
import mki.siojt2.ui.activity_detail_tanah.presenter.DetailTanahPresenter
import mki.siojt2.ui.activity_detail_tanah.view.DetailTanahView
import mki.siojt2.utils.realm.RealmController

class JenisDetailObjekTanah2 : BaseFragment(), DetailTanahView {

    lateinit var detailTanahPresenter: DetailTanahPresenter

    // TODO: Rename and change types of parameters
    private var mParam1: Int? = null
    private var mParam2: Int? = null

    lateinit var realm: Realm
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getInt(ARG_PARAM1)
            mParam2 = arguments!!.getInt(ARG_PARAM2)
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_objek_tanah_2, container, false)
    }

    @SuppressLint("InflateParams")
    override fun setUp(view: View) {
        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        val results = realm.where(ProjectLand::class.java).equalTo("LandIdTemp", mParam1).findFirst()
        Log.d("result", results.toString())
        if (results != null) {
            tvMudahDijangkau.text = if(results.landEaseToPropertyName.isNullOrEmpty()){"-"} else results.landEaseToPropertyName
            tvMudahBelanja.text = if(results.landEaseToShopingCenterName.isNullOrEmpty()){"-"} else results.landEaseToShopingCenterName
            tvMudahPendidikan.text = if(results.landEaseToEducationFacilitiesName.isNullOrEmpty()){"-"} else results.landEaseToEducationFacilitiesName
            tvMudahWisata.text = if(results.landEaseToTouristSitesName.isNullOrEmpty()){"-"} else results.landEaseToTouristSitesName
            tvMudahTransportasi.text = if(results.landEaseOfTransportationName.isNullOrEmpty()){"-"} else results.landEaseOfTransportationName
            tvAman.text = if(results.landSecurityAgainstCrimeName.isNullOrEmpty()){"-"} else results.landSecurityAgainstCrimeName
            tvAmanBencana.text = if(results.landSecurityAgainstNaturalDisastersName.isNullOrEmpty()){"-"} else results.landSecurityAgainstNaturalDisastersName
            tvAmanKebakaran.text = if(results.landSafetyAgainstFireHazardsName.isNullOrEmpty()){"-"} else results.landSafetyAgainstFireHazardsName
            tvMudahListrik.text = if(results.landAvailableElectricalGridName.isNullOrEmpty()){"-"} else results.landAvailableElectricalGridName
            tvMudahAir.text = if(results.landAvailableCleanWaterSystemName.isNullOrEmpty()){"-"} else results.landAvailableCleanWaterSystemName
            tvMudahTelepon.text = if(results.landAvailableTelephoneNetworkName.isNullOrEmpty()){"-"} else results.landAvailableTelephoneNetworkName
            tvMudahGas.text = if(results.landAvailableGasPipelineNetworkName.isNullOrEmpty()){"-"} else results.landAvailableGasPipelineNetworkName
            tvSutet.text = if(results.landSUTETPresenceName.isNullOrEmpty()){"-"} else results.landSUTETPresenceName
            tvMakan.text = if(results.landCemeteryPresenceName.isNullOrEmpty()){"-"} else results.landCemeteryPresenceName
            tvTusukSate.text = if(results.landSkewerPresenceName.isNullOrEmpty()){"-"} else results.landSkewerPresenceName
//            not used
            tvisiPosisiTanah.text = results.landPositionToRoadName
            tvisilebarJln.text = results.landWideFrontRoad.toString()
            tvisibarJlnDpn.text = results.landFrontage.toString()
            tvisiPanjang.text = results.landLength.toString()
            if (results.landShapeNameOther.isNotEmpty()) {
                tvisibntkTanah.text = results.landShapeNameOther
            } else {
                tvisibntkTanah.text = results.landShapeName
            }
            tvisiTopografi.text = results.landTypographyName //typographyname

            tvisiluasTanah.text = Html.fromHtml("${results.landAreaCertificate} (m<sup><small>2</small></sup>)")
            tvisiluasTanahDampak.text = Html.fromHtml("${results.landAreaAffected} (m<sup><small>2</small></sup>)")
            tvisiluasTanahSisa.text = Html.fromHtml("${results.tanahSisa} (m<sup><small>2</small></sup>)")

            tvisiTnggiTanah.text = results.landHeightToRoad

            val liveTypeId = results.landTypeLandTypeId
            val liveTypeId1 = liveTypeId.replace("[", "['")
            val liveTypeId2 = liveTypeId1.replace("]", "']")
            val liveTypeIdRef = liveTypeId2.replace(", ", "','")

            val liveIncome = results.landTypeLandTypeValue
            val replac1 = liveIncome.replace("[", "['")
            val replac2 = replac1.replace("]", "']")
            val replac3 = replac2.replace(", ", "','")

            val landTYpeNameOther = results.landTypeLandTypeNameOther
            val landTYpeNameOther2 = landTYpeNameOther.replace("[", "['")
            val landTYpeNameOther3 = landTYpeNameOther2.replace("]", "']")
            val landTYpeNameOtherRef = landTYpeNameOther3.replace(", ", "','")

            val landTypeId = Gson().fromJson(liveTypeIdRef, Array<String>::class.java).toList()
            val landTypeName = Gson().fromJson(replac3, Array<String>::class.java).toList()
            val landTypeNameOther = Gson().fromJson(landTYpeNameOtherRef, Array<String>::class.java).toList()
            val item = activity!!.findViewById<LinearLayout>(R.id.lljenisTanah)
            item.removeAllViews()
            for (i in landTypeId.indices) {
                val child = layoutInflater.inflate(R.layout.item_detail_jenis_tanah_vertical, null)
                val jenisTanah = child.findViewById(R.id.tvjenisTanah) as com.pixplicity.fontview.FontTextView

                if (landTypeNameOther[i].isNotEmpty()) {
                    jenisTanah.text = landTypeNameOther[i]
                } else {
                    jenisTanah.text = landTypeName[i]
                }

                item.addView(child)
            }
        } else {
            Log.e("data", "kosong")
        }
    }

    private fun getPresenter(): DetailTanahPresenter? {
        detailTanahPresenter = DetailTanahPresenter()
        detailTanahPresenter.onAttach(this)
        return detailTanahPresenter
    }

    override fun onsuccesgetdatadetaiLTanah(responseDataDetailTanah: ResponseDataDetailTanah2) {
        tvisiPosisiTanah.text = responseDataDetailTanah.landDetail?.positionToRoadName
                ?: "-"
        tvisilebarJln.text = responseDataDetailTanah.landDetail?.projectLandWideFrontRoad.toString()
        tvisibarJlnDpn.text = responseDataDetailTanah.landDetail?.projectLandFrontage.toString()
        tvisiPanjang.text = responseDataDetailTanah.landDetail?.projectLandLength.toString()
        tvisibntkTanah.text = responseDataDetailTanah.landDetail?.shapeName ?: "-"
        tvisiTopografi.text = responseDataDetailTanah.landDetail?.typographyName ?: "-"

        tvisiluasTanah.text = Html.fromHtml("${responseDataDetailTanah.landDetail?.projectLandAreaCertificate} (m<sup><small>2</small></sup>)")
        tvisiluasTanahDampak.text = Html.fromHtml("${responseDataDetailTanah.landDetail?.projectLandAreaAffected} (m<sup><small>2</small></sup>)")

        tvisiTnggiTanah.text = responseDataDetailTanah.landDetail?.projectLandHeightToRoad

        val item = activity!!.findViewById<LinearLayout>(R.id.lljenisTanah)
        item.removeAllViews()
        responseDataDetailTanah.projectLandTypes!!.forEach {
            val child = layoutInflater.inflate(R.layout.item_detail_jenis_tanah_vertical, null)
            val jenisTanah = child.findViewById(R.id.tvjenisTanah) as com.pixplicity.fontview.FontTextView
            jenisTanah.text = it?.landTypeName ?: "-"
            item.addView(child)

        }
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"
        internal const val TAG = "JenisDetailObjekTanah2"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: Int, param2: Int): JenisDetailObjekTanah2 {
            val fragment = JenisDetailObjekTanah2()
            val args = Bundle()
            args.putInt(ARG_PARAM1, param1)
            args.putInt(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor