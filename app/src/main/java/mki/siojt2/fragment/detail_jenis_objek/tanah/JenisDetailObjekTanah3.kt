package mki.siojt2.fragment.detail_jenis_objek.tanah

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.google.gson.Gson
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_detail_objek_tanah_3.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.model.data_detail_tanah.ResponseDataDetailTanah2
import mki.siojt2.model.localsave.ProjectLand
import mki.siojt2.ui.activity_detail_tanah.presenter.DetailTanahPresenter
import mki.siojt2.ui.activity_detail_tanah.view.DetailTanahView
import mki.siojt2.utils.realm.RealmController
import java.text.SimpleDateFormat
import java.util.*

class JenisDetailObjekTanah3 : BaseFragment(), DetailTanahView {

    lateinit var detailTanahPresenter: DetailTanahPresenter

    // TODO: Rename and change types of parameters
    private var mParam1: Int? = null
    private var mParam2: Int? = null

    lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getInt(ARG_PARAM1)
            mParam2 = arguments!!.getInt(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_objek_tanah_3, container, false)
    }

    @SuppressLint("InflateParams")
    override fun setUp(view: View) {
        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        val results = realm.where(ProjectLand::class.java).equalTo("LandIdTemp", mParam1).findFirst()
        //Log.d("result", results.toString())
        if (results != null) {
            //tvisijenisKepemilikanTanah.text = results.landOwnershipName
            if (results.landOwnershipNameOther.isNotEmpty()) {
                tvisijenisKepemilikanTanah.text = results.landOwnershipNameOther?.toString() ?: "-"
            } else {
                tvisijenisKepemilikanTanah.text = results.landOwnershipName?.toString() ?: "-"
            }

            val landOwnershipTypeSHM = results.landOwnershipTypeSHM
            val landOwnershipTypeSHM1 = landOwnershipTypeSHM.replace("[", "['")
            val landOwnershipTypeSHM2 = landOwnershipTypeSHM1.replace("]", "']")
            val landOwnershipTypeSHM3 = landOwnershipTypeSHM2.replace(", ", "','")

            if(results.landOwnershipName?.toString() == "SHM"){
                val datalandOwnershipTypeSHM = Gson().fromJson(landOwnershipTypeSHM3, Array<String>::class.java).toList()
                if(datalandOwnershipTypeSHM.isNotEmpty()){
                    val item = activity!!.findViewById<LinearLayout>(R.id.lluserOwnerShipTypeSHM)
                    item.removeAllViews()
                    for (element in datalandOwnershipTypeSHM.indices) {
                        val child = layoutInflater.inflate(R.layout.item_detail_jenis_tanah_vertical, null)
                        val jenisTanah = child.findViewById(R.id.tvjenisTanah) as com.pixplicity.fontview.FontTextView
                        jenisTanah.text = datalandOwnershipTypeSHM[element]
                        item.addView(child)
                    }
                }
            }


            tvisitglditerbitkanTanah.text = if(results.landDateOfIssue.isNullOrEmpty()) {"-"} else {results.landDateOfIssue} //SimpleDateFormat("dd MMMM yyyy").format(results.landDateOfIssue)z
            tvisitglberakhirditerbitkanTanah.text = if(results.landRightsExpirationDate.isNullOrEmpty()) {"-"} else {results.landRightsExpirationDate} //SimpleDateFormat("dd MMMM yyyy").format(results.landRightsExpirationDate)
            tvisikesesuaianTanah.text = results.landZoningSpatialPlanName

            val landUtilatization = results.landUtilizationLandUtilizationId
            val landUtilatization1 = landUtilatization.replace("[", "['")
            val landUtilatization2 = landUtilatization1.replace("]", "']")
            val landUtilatization3 = landUtilatization2.replace(", ", "','")

            val liveIncome = results.landUtilizationLandUtilizationName
            val replac1 = liveIncome.replace("[", "['")
            val replac2 = replac1.replace("]", "']")
            val replac3 = replac2.replace(", ", "','")

            val landUtilatizationNameOther = results.landUtilizationLandUtilizationNameOther
            val landUtilatizationNameOther1 = landUtilatizationNameOther.replace("[", "['")
            val landUtilatizationNameOther2 = landUtilatizationNameOther1.replace("]", "']")
            val landUtilatizationNameOther3 = landUtilatizationNameOther2.replace(", ", "','")

            val landUtilatizationId = Gson().fromJson(landUtilatization3, Array<String>::class.java).toList()
            val landUtilatizationName = Gson().fromJson(replac3, Array<String>::class.java).toList()
            val landUtilatizationNameOtherRep = Gson().fromJson(landUtilatizationNameOther3, Array<String>::class.java).toList()

            val item = activity!!.findViewById<LinearLayout>(R.id.llpenggunaanTanah)
            item.removeAllViews()
            for (element in landUtilatizationId.indices) {
                val child = layoutInflater.inflate(R.layout.item_detail_jenis_tanah_vertical, null)
                val jenisTanah = child.findViewById(R.id.tvjenisTanah) as com.pixplicity.fontview.FontTextView

                if(landUtilatizationNameOtherRep[element].isNotEmpty()){
                    jenisTanah.text = landUtilatizationNameOtherRep[element]
                }else{
                    jenisTanah.text = landUtilatizationName[element]
                }
                item.addView(child)
            }

            tvisiBatasTanahBarat.text = Html.fromHtml("<b>BARAT : </b> ${results.landOrientationWest}")
            tvisiBatasTanahTimur.text = Html.fromHtml("<b>TIMUR : </b> ${results.landOrientationEast}")
            tvisiBatasTanahUtara.text = Html.fromHtml("<b>UTARA : </b> ${results.landOrientationNorth}")
            tvisiBatasSelatan.text = Html.fromHtml("<b>SELATAN : </b> ${results.landOrientationSouth}")
            tvisiBatasBaratLaut.text = Html.fromHtml("<b>BARAT LAUT : </b> ${results.landOrientationNorthwest}")
            tvisiBatasTimurLaut.text = Html.fromHtml("<b>TIMUR LAUT : </b> ${results.landOrientationNortheast}")
            tvisiBatasTanahTenggara.text = Html.fromHtml("<b>TENGGARA : </b> ${results.landOrientationSoutheast}")
            tvisiBatasTanahBaratDaya.text = Html.fromHtml("<b>BARAT DAYA : </b> ${results.landOrientationSouthwest}")

            tvProofAlasHak.text = if(results.landAlasHakTanah.isNullOrEmpty()) {"-"} else {results.landAlasHakTanah}
            tvNIB.text = if(results.landNIB.isNullOrEmpty()) {"-"} else {results.landNIB}
            tvCertificateNumber.text = if(results.landCertificateNumber.isNullOrEmpty()) {"-"} else {results.landCertificateNumber}
            tvFiducia.text = if(results.landAddInfoAddInfoName.replace("[","").replace("]","").isNullOrEmpty()) {"-"} else {results.landAddInfoAddInfoName.replace("[","").replace("]","")}
            tvBuild.text = if(results.dampak.isNullOrEmpty()) {"-"} else {results.dampak}
            tvNotes.text = if(results.note.isNullOrEmpty()) {"-"} else {results.note}
            tvOther.text = if(results.hasOtherObject.isNullOrEmpty()) {"-"} else {results.hasOtherObject}

        } else {
            Log.e("data", "kosong")
        }
        //getPresenter()?.getdatadetaiLTanah(mParam1!!, Preference.accessToken)
    }

    private fun getPresenter(): DetailTanahPresenter? {
        detailTanahPresenter = DetailTanahPresenter()
        detailTanahPresenter.onAttach(this)
        return detailTanahPresenter
    }

    @SuppressLint("InflateParams", "SimpleDateFormat")
    override fun onsuccesgetdatadetaiLTanah(responseDataDetailTanah: ResponseDataDetailTanah2) {

        var json: String? = responseDataDetailTanah.landDetail?.projectLandDateOfIssue
        json = json?.replace("/Date(", "")?.replace(")/", "")
        val time = java.lang.Long.parseLong(json!!)
        val date = Date(time)

        var json2: String? = responseDataDetailTanah.landDetail?.projectLandRightsExpirationDate
        json2 = json2?.replace("/Date(", "")?.replace(")/", "")
        val time2 = java.lang.Long.parseLong(json2!!)
        val date2 = Date(time2)

        tvisijenisKepemilikanTanah.text = responseDataDetailTanah.landDetail?.ownershipName ?: "-"
        tvisitglditerbitkanTanah.text = SimpleDateFormat("dd MMMM yyyy").format(date)
        tvisitglberakhirditerbitkanTanah.text = SimpleDateFormat("dd MMMM yyyy").format(date2)

        tvisikesesuaianTanah.text = responseDataDetailTanah.landDetail?.zoningSpatialPlanName

        val item = activity!!.findViewById<LinearLayout>(R.id.llpenggunaanTanah)
        item.removeAllViews()
        responseDataDetailTanah.projectLandUtilizations!!.forEach {
            val child = layoutInflater.inflate(R.layout.item_detail_jenis_tanah_vertical, null)
            val jenisTanah = child.findViewById(R.id.tvjenisTanah) as com.pixplicity.fontview.FontTextView
            jenisTanah.text = it?.utilizationName ?: "-"
            item.addView(child)

        }
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"
        internal const val TAG = "JenisDetailObjekTanah3"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: Int, param2: Int): JenisDetailObjekTanah3 {
            val fragment = JenisDetailObjekTanah3()
            val args = Bundle()
            args.putInt(ARG_PARAM1, param1)
            args.putInt(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor