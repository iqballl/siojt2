package mki.siojt2.fragment.detail_jenis_objek.tanah

import android.Manifest
import android.annotation.SuppressLint
import android.content.IntentSender
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_detail_objek_tanah_1.*
import kotlinx.android.synthetic.main.fragment_detail_objek_tanah_1.view.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.fragment.detail_jenis_objek.tanah.adapter_recycleview.AdapterFotoPropertiTanah
import mki.siojt2.model.data_detail_tanah.ResponseDataDetailTanah2
import mki.siojt2.model.local_save_image.DataImageLand
import mki.siojt2.model.localsave.ProjectLand
import mki.siojt2.ui.activity_detail_tanah.presenter.DetailTanahPresenter
import mki.siojt2.ui.activity_detail_tanah.view.DetailTanahView
import mki.siojt2.utils.realm.RealmController

class JenisDetailObjekTanah1 : BaseFragment(),
        DetailTanahView, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    lateinit var detailTanahPresenter: DetailTanahPresenter
    lateinit var viewJenisDetailObjekTanah1: View
    lateinit var mMapView: MapView

    // TODO: Rename and change types of parameters
    private var mParam1: Int? = null
    private var mParam2: Int? = null

    lateinit var realm: Realm

    /* Map */
    private var mLatLang: LatLng? = null
    private var mGoogleMap: GoogleMap? = null
    private var mGoogleApiClient: GoogleApiClient? = null

    private val MY_PERMISSIONS_REQUEST_LOCATION = 99
    private val GPS_SETTINGS = 0x7
    private var locationRequest: LocationRequest? = null
    private var result: PendingResult<LocationSettingsResult>? = null

    lateinit var adapterFotoPropertiTanah: AdapterFotoPropertiTanah

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getInt(ARG_PARAM1)
            mParam2 = arguments!!.getInt(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        viewJenisDetailObjekTanah1 = inflater.inflate(R.layout.fragment_detail_objek_tanah_1, container, false)
        mMapView = viewJenisDetailObjekTanah1.mapviewDetail as MapView
        mMapView.onCreate(savedInstanceState)
        mMapView.onResume()
        mMapView.getMapAsync(this)
        return viewJenisDetailObjekTanah1

    }

    @SuppressLint("SetTextI18n")
    override fun setUp(view: View) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission()
        }

        adapterFotoPropertiTanah = AdapterFotoPropertiTanah(activity!!)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity!!, androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL, false)

        rlfotoPropertiTanah.adapter = adapterFotoPropertiTanah
        rlfotoPropertiTanah.layoutManager = linearLayoutManager

        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        val results = realm.where(ProjectLand::class.java).equalTo("LandIdTemp", mParam1).findFirst()
        Log.d("result", results.toString())
        if (results != null) {
            tvisiAlamat.text = "${results.landComplekName ?: ""} " +
                    "${results.landStreet} " +
                    "${results.landBlok ?: ""} " +
                    "No.${results.landNumber ?: ""} " +
                    "${results.landVillageName} " +
                    "${results.landDistrictName} " +
                    "${results.landRegencyName} " +
                    "${results.landProvinceName} " +
                    results.landPostalCode

            mLatLang = LatLng(results.landLatitude.toDouble(),
                    results.landLangitude.toDouble())

            val mdatafotoTanah: MutableList<DataImageLand> = ArrayList()
            mdatafotoTanah.addAll(results.dataImageLands)
            adapterFotoPropertiTanah.clear()
            adapterFotoPropertiTanah.addItems(mdatafotoTanah)

        } else {
            Log.e("data", "kosong")
        }
        //getPresenter()?.getdatadetaiLTanah(mParam1!!, Preference.accessToken)

    }

    private fun getPresenter(): DetailTanahPresenter? {
        detailTanahPresenter = DetailTanahPresenter()
        detailTanahPresenter.onAttach(this)
        return detailTanahPresenter
    }

    @SuppressLint("SetTextI18n")
    override fun onsuccesgetdatadetaiLTanah(responseDataDetailTanah: ResponseDataDetailTanah2) {
        tvisiAlamat.text = "${responseDataDetailTanah.landDetail?.projectLandComplexName ?: ""} " +
                "${responseDataDetailTanah.landDetail?.projectLandStreetName} " +
                "${responseDataDetailTanah.landDetail?.projectLandBlock ?: ""} " +
                "No.${responseDataDetailTanah.landDetail?.projectLandNumber ?: ""} " +
                "${responseDataDetailTanah.landDetail?.projectLandVillageName} " +
                "${responseDataDetailTanah.landDetail?.districtName} " +
                "${responseDataDetailTanah.landDetail?.regencyName} " +
                "${responseDataDetailTanah.landDetail?.provinceName} " +
                "${responseDataDetailTanah.landDetail?.projectLandPostalCode}"
    }


    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient!!.isConnected) {
            mGoogleApiClient!!.disconnect()
        }
    }

    //onMapReadyCallback Listener
    override fun onMapReady(googleMap: GoogleMap?) {
        mGoogleMap = googleMap

        if ((ContextCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            if (googleMap != null) {
                //mGoogleMap?.isMyLocationEnabled = true

                val position: LatLng = if (mLatLang != null) {
                    mLatLang as LatLng
                    //val sydney = LatLng(-6.161941, 106.908182)
                } else {
                    LatLng(-6.161941, 106.908182)
                }

                mGoogleMap!!.addMarker(MarkerOptions().position(position))

                // For zooming automatically to the location of the marker
                val cameraPosition = CameraPosition.Builder().target(position).zoom(15f).build()
                mGoogleMap!!.animateCamera(CameraUpdateFactory.newLatLng(position))
                mGoogleMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

                buildGoogleApiClient()
            }

        } else {
            buildGoogleApiClient()
        }
    }

    //GoogleApiClient.ConnectionCallbacks Listener
    override fun onConnected(p0: Bundle?) {
        locationRequest = LocationRequest()
        locationRequest?.interval = 1000
        locationRequest?.fastestInterval = 1000
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest!!)
        builder.setAlwaysShow(true)
        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())
        result?.setResultCallback { result ->
            val status = result.status
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                    status.startResolutionForResult(activity, GPS_SETTINGS)
                } catch (e: IntentSender.SendIntentException) {
                }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }
        }

    }

    override fun onConnectionSuspended(p0: Int) {
        Log.d(TAG, "onConnectionSuspended : Connection suspended")
        if (mGoogleApiClient == null) {
            mGoogleApiClient!!.connect()
        }
    }

    // GoogleApiClient.OnConnectionFailedListener Listener
    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.d(TAG, "Connection failed. Error: " + connectionResult.errorCode)
    }

    @Synchronized
    private fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(activity!!)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        mGoogleApiClient!!.connect()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        try {
            mMapView.onLowMemory()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDetach() {
        super.onDetach()
        getPresenter()?.onDetach()
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            mMapView.onDestroy()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (mGoogleApiClient != null) {
            mGoogleApiClient!!.disconnect()
        }
        getPresenter()?.onDetach()
    }

    private fun checkLocationPermission(): Boolean {
        return if ((ContextCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS_REQUEST_LOCATION)
            } else {
                ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS_REQUEST_LOCATION)
            }
            false
        } else {
            true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                if ((grantResults.isNotEmpty() || grantResults[0] == PackageManager.PERMISSION_GRANTED || grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        buildGoogleApiClient()
                    }
                    //mGoogleMap?.isMyLocationEnabled = true
                    onMapReady(mGoogleMap)
                } else {
                    Toast.makeText(this.context, "Permission denied...", Toast.LENGTH_LONG).show()
                }
            }
        }

    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"
        internal const val TAG = "JenisDetailObjekTanah1"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: Int, param2: Int): JenisDetailObjekTanah1 {
            val fragment = JenisDetailObjekTanah1()
            val args = Bundle()
            args.putInt(ARG_PARAM1, param1)
            args.putInt(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor