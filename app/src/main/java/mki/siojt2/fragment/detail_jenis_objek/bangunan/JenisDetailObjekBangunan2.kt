package mki.siojt2.fragment.detail_jenis_objek.bangunan

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.google.gson.Gson
import io.realm.Realm
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.model.data_detail_objek_bangunan.ResponseDataDetailBangunan
import mki.siojt2.model.localsave.Bangunan
import mki.siojt2.ui.activity_detail_objek_bangunan.presenter.DetailBangunanPresenter
import mki.siojt2.ui.activity_detail_objek_bangunan.view.DetailBangunanView
import mki.siojt2.utils.realm.RealmController
import java.text.DecimalFormat

class JenisDetailObjekBangunan2 : BaseFragment(), DetailBangunanView {

    // TODO: Rename and change types of parameters
    private var mParam1: Int? = null
    private var mParam2: String? = null

    lateinit var detailBangunanPresenter: DetailBangunanPresenter

    lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getInt(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_objek_bangunan_2, container, false)
    }

    private fun getPresenter(): DetailBangunanPresenter? {
        detailBangunanPresenter = DetailBangunanPresenter()
        detailBangunanPresenter.onAttach(this)
        return detailBangunanPresenter
    }

    override fun setUp(view: View) {
        //getPresenter()?.getDetalBangunan(mParam1!!, Preference.accessToken)
        //llitemPondasi
        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        val results = realm.where(Bangunan::class.java).equalTo("BangunanIdTemp", mParam1).findFirst()
        Log.d("result", results.toString())
        if (results != null) {
            showResults(results)
        } else {
            Log.e("data", "kosong")
        }
    }

    @SuppressLint("SetTextI18n", "InflateParams")
    private fun showResults(results: Bangunan) {
        //pondasi

        val foundationId = results.foundationTypeId
        val foundationId1 = foundationId.replace("[", "['")
        val foundationId2 = foundationId1.replace("]", "']")
        val foundationId3 = foundationId2.replace(", ", "','")
        val foundationRepId = Gson().fromJson(foundationId3, Array<String>::class.java).toList()

        val foundation = results.foundationTypeName
        val foundrep1 = foundation.replace("[", "['")
        val foundrep2 = foundrep1.replace("]", "']")
        val foundrep3 = foundrep2.replace(", ", "','")
        val foundationname = Gson().fromJson(foundrep3, Array<String>::class.java).toList()

        val foundationOther = results.foundationTypeNameOther
        val foundationOther1 = foundationOther.replace("[", "['")
        val foundationOther2 = foundationOther1.replace("]", "']")
        val foundationOther3 = foundationOther2.replace(", ", "','")
        val foundationnameOther = Gson().fromJson(foundationOther3, Array<String>::class.java).toList()

        val fnameation = results.pbFoundationTypeAreaTotal
        val fnamerep1 = fnameation.replace("[", "['")
        val fnamerep2 = fnamerep1.replace("]", "']")
        val fnamerep3 = fnamerep2.replace(", ", "','")
        val fname = Gson().fromJson(fnamerep3, Array<String>::class.java).toList()


        val itemPondasi = activity!!.findViewById<LinearLayout>(R.id.llitemPondasi)
        itemPondasi.removeAllViews()
        if (foundationRepId.isNotEmpty()) {
            for (i in foundationRepId.indices) {
                val child = layoutInflater.inflate(R.layout.item_detail_multiple_vertical, null)
                val txttvisiMultiple1 = child.findViewById(R.id.tvtitleMultiple1) as com.pixplicity.fontview.FontTextView
                val txgttvisiMultiple1 = child.findViewById(R.id.tvisiMultiple1) as com.pixplicity.fontview.FontTextView

                val txttvisiMultiple2 = child.findViewById(R.id.tvtitleMultiple2) as com.pixplicity.fontview.FontTextView
                val txgttvisiMultiple2 = child.findViewById(R.id.tvisiMultiple2) as com.pixplicity.fontview.FontTextView

                if (foundationnameOther[i].isNotEmpty()) {
                    txttvisiMultiple1.text = "Jenis pondasi bangunan"
                    txgttvisiMultiple1.text = foundationnameOther[i]

                    //val a = convertToFloat(it?.pBFoundationTypeAreaTotal!!)
                    txttvisiMultiple2.text = "Total area pondasi"
                    /*val thesum = (fname[i].toFloat() * 10)
                    val form = DecimalFormat("0.00")*/
                    txgttvisiMultiple2.text = fname[i] + " % "
                    itemPondasi.addView(child)
                } else {
                    txttvisiMultiple1.text = "Jenis pondasi bangunan"
                    txgttvisiMultiple1.text = foundationname[i]

                    //val a = convertToFloat(it?.pBFoundationTypeAreaTotal!!)
                    txttvisiMultiple2.text = "Total area pondasi"
                    /*val thesum = (fname[i].toFloat() * 10)
                    val form = DecimalFormat("0.00")*/
                    txgttvisiMultiple2.text = fname[i] + " % "
                    itemPondasi.addView(child)
                }

            }

        }

        //struktur
        val strukturId = results.structureTypeId
        val strukturId1 = strukturId.replace("[", "['")
        val strukturId2 = strukturId1.replace("]", "']")
        val strukturId3 = strukturId2.replace(", ", "','")
        val stukturRepId = Gson().fromJson(strukturId3, Array<String>::class.java).toList()

        val strukturName = results.structureTypeName
        val repstrid = strukturName.replace("[", "['")
        val repstrid1 = repstrid.replace("]", "']")
        val repstrid2 = repstrid1.replace(", ", "','")
        val repstr = Gson().fromJson(repstrid2, Array<String>::class.java).toList()

        val strukturNameOther = results.structureTypeNameOther
        val strukturNameOthe1 = strukturNameOther.replace("[", "['")
        val strukturNameOthe2 = strukturNameOthe1.replace("]", "']")
        val strukturNameOthe3 = strukturNameOthe2.replace(", ", "','")
        val strukturNameRepOther = Gson().fromJson(strukturNameOthe3, Array<String>::class.java).toList()

        val stotal1 = results.pbStructureTypeAreaTotal
        val stotal2 = stotal1.replace("[", "['")
        val stotal3 = stotal2.replace("]", "']")
        val stotal4 = stotal3.replace(", ", "','")
        val stotal = Gson().fromJson(stotal4, Array<String>::class.java).toList()


        val itemStruktur = activity!!.findViewById<LinearLayout>(R.id.llitemStuktur)
        itemStruktur.removeAllViews()
        if (stukturRepId.isNotEmpty()) {
            for (i in stukturRepId.indices) {
                val child = layoutInflater.inflate(R.layout.item_detail_multiple_vertical, null)
                val txttvisiMultiple1 = child.findViewById(R.id.tvtitleMultiple1) as com.pixplicity.fontview.FontTextView
                val txgttvisiMultiple1 = child.findViewById(R.id.tvisiMultiple1) as com.pixplicity.fontview.FontTextView

                val txttvisiMultiple2 = child.findViewById(R.id.tvtitleMultiple2) as com.pixplicity.fontview.FontTextView
                val txgttvisiMultiple2 = child.findViewById(R.id.tvisiMultiple2) as com.pixplicity.fontview.FontTextView

                if (strukturNameRepOther[i].isNotEmpty()) {
                    txttvisiMultiple1.text = "Jenis stuktur bangunan"
                    txgttvisiMultiple1.text = strukturNameRepOther[i]

                    txttvisiMultiple2.text = "Total area struktur"
                    /*val thesum = (stotal[i].toFloat() * 10)
                    val form = DecimalFormat("0.00")*/
                    txgttvisiMultiple2.text = stotal[i] + " % "
                    itemStruktur.addView(child)
                } else {
                    txttvisiMultiple1.text = "Jenis stuktur bangunan"
                    txgttvisiMultiple1.text = repstr[i]

                    txttvisiMultiple2.text = "Total area struktur"
                    /*val thesum = (stotal[i].toFloat() * 10)
                    val form = DecimalFormat("0.00")*/
                    txgttvisiMultiple2.text = stotal[i] + " % "
                    itemStruktur.addView(child)

                }


            }
        }

        //kerangkaatap
        val atapId = results.roofFrameTypeId
        val atapId1 = atapId.replace("[", "['")
        val atapId2 = atapId1.replace("]", "']")
        val atapId3 = atapId2.replace(", ", "','")
        val atapRepId = Gson().fromJson(atapId3, Array<String>::class.java).toList()

        val atapid1 = results.roofFrameTypeName
        val atapid2 = atapid1.replace("[", "['")
        val atapid3 = atapid2.replace("]", "']")
        val atapid4 = atapid3.replace(", ", "','")
        val atapid = Gson().fromJson(atapid4, Array<String>::class.java).toList()

        val atapnameOther = results.roofFrameTypeNameOther
        val atapnameOther1 = atapnameOther.replace("[", "['")
        val atapnameOther2 = atapnameOther1.replace("]", "']")
        val atapnameOther3 = atapnameOther2.replace(", ", "','")
        val atapnameRepOther = Gson().fromJson(atapnameOther3, Array<String>::class.java).toList()

        val tatap1 = results.pbRoofFrameTypeAreaTotal
        val tatap2 = tatap1.replace("[", "['")
        val tatap3 = tatap2.replace("]", "']")
        val tatap4 = tatap3.replace(", ", "','")
        val tatap = Gson().fromJson(tatap4, Array<String>::class.java).toList()

        val itemKerangkaAtap = activity!!.findViewById<LinearLayout>(R.id.llitemKerangkaAtap)
        itemKerangkaAtap.removeAllViews()
        if (atapRepId.isNotEmpty()) {
            for (i in atapRepId.indices) {
                val child = layoutInflater.inflate(R.layout.item_detail_multiple_vertical, null)
                val txttvisiMultiple1 = child.findViewById(R.id.tvtitleMultiple1) as com.pixplicity.fontview.FontTextView
                val txgttvisiMultiple1 = child.findViewById(R.id.tvisiMultiple1) as com.pixplicity.fontview.FontTextView

                val txttvisiMultiple2 = child.findViewById(R.id.tvtitleMultiple2) as com.pixplicity.fontview.FontTextView
                val txgttvisiMultiple2 = child.findViewById(R.id.tvisiMultiple2) as com.pixplicity.fontview.FontTextView
                if (atapnameRepOther[i].isNotEmpty()) {
                    txttvisiMultiple1.text = "Jenis kerangka atap bangunan"
                    txgttvisiMultiple1.text = atapnameRepOther[i]

                    txttvisiMultiple2.text = "Total area kerangka atap"
                    /*val thesum = (tatap[i].toFloat() * 10)
                    val form = DecimalFormat("0.00")*/
                    txgttvisiMultiple2.text = tatap[i] + " % "
                    itemKerangkaAtap.addView(child)
                } else {
                    txttvisiMultiple1.text = "Jenis kerangka atap bangunan"
                    txgttvisiMultiple1.text = atapid[i]

                    txttvisiMultiple2.text = "Total area kerangka atap"
                    /*val thesum = (tatap[i].toFloat() * 10)
                    val form = DecimalFormat("0.00")*/
                    txgttvisiMultiple2.text = tatap[i] + " % "
                    itemKerangkaAtap.addView(child)
                }

            }
        }

        //penutupatap
        val penutupId = results.roofCoveringTypeId
        val penutupId2 = penutupId.replace("[", "['")
        val penutupId3 = penutupId2.replace("]", "']")
        val penutupId4 = penutupId3.replace(", ", "','")
        val penutupIdRep = Gson().fromJson(penutupId4, Array<String>::class.java).toList()

        val penutun1 = results.roofCoveringTypeName
        val penutun2 = penutun1.replace("[", "['")
        val penutun3 = penutun2.replace("]", "']")
        val penutun4 = penutun3.replace(", ", "','")
        val penutun = Gson().fromJson(penutun4, Array<String>::class.java).toList()

        val penutupNameOther = results.roofCoveringTypeNameOther
        val penutupNameOther1 = penutupNameOther.replace("[", "['")
        val penutupNameOther2 = penutupNameOther1.replace("]", "']")
        val penutupNameOther3 = penutupNameOther2.replace(", ", "','")
        val penutupNameOtherRep = Gson().fromJson(penutupNameOther3, Array<String>::class.java).toList()

        val tpenutupn1 = results.pbRoofCoveringTypeAreaTotal
        val tpenutupn2 = tpenutupn1.replace("[", "['")
        val tpenutupn3 = tpenutupn2.replace("]", "']")
        val tpenutupn4 = tpenutupn3.replace(", ", "','")
        val tpenutupn = Gson().fromJson(tpenutupn4, Array<String>::class.java).toList()

        val itemPenutupAtap = activity!!.findViewById<LinearLayout>(R.id.llitemPenutupAtap)
        itemPenutupAtap.removeAllViews()
        if (penutupIdRep.isNotEmpty()) {
            for (i in penutupIdRep.indices) {
                val child = layoutInflater.inflate(R.layout.item_detail_multiple_vertical, null)
                val txttvisiMultiple1 = child.findViewById(R.id.tvtitleMultiple1) as com.pixplicity.fontview.FontTextView
                val txgttvisiMultiple1 = child.findViewById(R.id.tvisiMultiple1) as com.pixplicity.fontview.FontTextView

                val txttvisiMultiple2 = child.findViewById(R.id.tvtitleMultiple2) as com.pixplicity.fontview.FontTextView
                val txgttvisiMultiple2 = child.findViewById(R.id.tvisiMultiple2) as com.pixplicity.fontview.FontTextView
                if (penutupNameOtherRep[i].isNotEmpty()) {
                    txttvisiMultiple1.text = "Jenis penutup atap bangunan"
                    txgttvisiMultiple1.text = penutupNameOtherRep[i]

                    txttvisiMultiple2.text = "Total area penutup atap"
                    /*val thesum = (tpenutupn[i].toFloat() * 10)
                    val form = DecimalFormat("0.00")*/
                    txgttvisiMultiple2.text = tpenutupn[i] + " % "
                    itemPenutupAtap.addView(child)
                } else {
                    txttvisiMultiple1.text = "Jenis penutup atap bangunan"
                    txgttvisiMultiple1.text = penutun[i]

                    txttvisiMultiple2.text = "Total area penutup atap"
                    /*val thesum = (tpenutupn[i].toFloat() * 10)
                    val form = DecimalFormat("0.00")*/
                    txgttvisiMultiple2.text = tpenutupn[i] + " % "
                    itemPenutupAtap.addView(child)
                }
            }
        }

        //plafon
        val plafonId = results.ceilingTypeId
        val plafonId2 = plafonId.replace("[", "['")
        val plafonId3 = plafonId2.replace("]", "']")
        val plafonId4 = plafonId3.replace(", ", "','")
        val plafonIdRep = Gson().fromJson(plafonId4, Array<String>::class.java).toList()

        val nplatform1 = results.ceilingTypeName
        val nplatform2 = nplatform1.replace("[", "['")
        val nplatform3 = nplatform2.replace("]", "']")
        val nplatform4 = nplatform3.replace(", ", "','")
        val nplatform = Gson().fromJson(nplatform4, Array<String>::class.java).toList()

        val plafonNameOther = results.ceilingTypeNameOther
        val plafonNameOther2 = plafonNameOther.replace("[", "['")
        val plafonNameOther3 = plafonNameOther2.replace("]", "']")
        val plafonNameOther4 = plafonNameOther3.replace(", ", "','")
        val plafonNameOtherRep = Gson().fromJson(plafonNameOther4, Array<String>::class.java).toList()

        val tplatform1 = results.pbCeilingTypeAreaTotal
        val tplatform2 = tplatform1.replace("[", "['")
        val tplatform3 = tplatform2.replace("]", "']")
        val tplatform4 = tplatform3.replace(", ", "','")
        val tplatform = Gson().fromJson(tplatform4, Array<String>::class.java).toList()

        val itemPlafon = activity!!.findViewById<LinearLayout>(R.id.llitemPlafon)
        itemPlafon.removeAllViews()
        if (plafonIdRep.isNotEmpty()) {
            for (i in plafonIdRep.indices) {
                val child = layoutInflater.inflate(R.layout.item_detail_multiple_vertical, null)
                val txttvisiMultiple1 = child.findViewById(R.id.tvtitleMultiple1) as com.pixplicity.fontview.FontTextView
                val txgttvisiMultiple1 = child.findViewById(R.id.tvisiMultiple1) as com.pixplicity.fontview.FontTextView

                val txttvisiMultiple2 = child.findViewById(R.id.tvtitleMultiple2) as com.pixplicity.fontview.FontTextView
                val txgttvisiMultiple2 = child.findViewById(R.id.tvisiMultiple2) as com.pixplicity.fontview.FontTextView
                if (plafonNameOtherRep[i].isNotEmpty()) {
                    txttvisiMultiple1.text = "Jenis plafon bangunan"
                    txgttvisiMultiple1.text = plafonNameOtherRep[i]

                    txttvisiMultiple2.text = "Total area plafon"
                    /*val thesum = (tplatform[i].toFloat() * 10)
                    val form = DecimalFormat("0.00")*/
                    txgttvisiMultiple2.text = tplatform[i] + " % "
                    itemPlafon.addView(child)
                } else {
                    txttvisiMultiple1.text = "Jenis plafon bangunan"
                    txgttvisiMultiple1.text = nplatform[i]

                    txttvisiMultiple2.text = "Total area plafon"
                    /*val thesum = (tplatform[i].toFloat() * 10)
                    val form = DecimalFormat("0.00")*/
                    txgttvisiMultiple2.text = tplatform[i] + " % "
                    itemPlafon.addView(child)
                }

            }
        }

        //dinding
        val dindingId = results.wallTypeId
        val dindingId1 = dindingId.replace("[", "['")
        val dindingId2 = dindingId1.replace("]", "']")
        val dindingId3 = dindingId2.replace(", ", "','")
        val dindingIdRep = Gson().fromJson(dindingId3, Array<String>::class.java).toList()

        val ndinding1 = results.wallTypeName
        val ndinding2 = ndinding1.replace("[", "['")
        val ndinding3 = ndinding2.replace("]", "']")
        val ndinding4 = ndinding3.replace(", ", "','")
        val ndinding = Gson().fromJson(ndinding4, Array<String>::class.java).toList()

        val dindingnameOther = results.wallTypeNameOther
        val dindingnameOther2 = dindingnameOther.replace("[", "['")
        val dindingnameOther3 = dindingnameOther2.replace("]", "']")
        val dindingnameOther4 = dindingnameOther3.replace(", ", "','")
        val dindingnameOtherRef = Gson().fromJson(dindingnameOther4, Array<String>::class.java).toList()

        val tdinding1 = results.pbfWallTypeAreaTotal
        val tdinding2 = tdinding1.replace("[", "['")
        val tdinding3 = tdinding2.replace("]", "']")
        val tdinding4 = tdinding3.replace(", ", "','")
        val tdinding = Gson().fromJson(tdinding4, Array<String>::class.java).toList()

        val itemDinding = activity!!.findViewById<LinearLayout>(R.id.llitemDinding)
        itemDinding.removeAllViews()
        if (dindingIdRep.isNotEmpty()) {
            for (i in dindingIdRep.indices) {
                val child = layoutInflater.inflate(R.layout.item_detail_multiple_vertical, null)
                val txttvisiMultiple1 = child.findViewById(R.id.tvtitleMultiple1) as com.pixplicity.fontview.FontTextView
                val txgttvisiMultiple1 = child.findViewById(R.id.tvisiMultiple1) as com.pixplicity.fontview.FontTextView

                val txttvisiMultiple2 = child.findViewById(R.id.tvtitleMultiple2) as com.pixplicity.fontview.FontTextView
                val txgttvisiMultiple2 = child.findViewById(R.id.tvisiMultiple2) as com.pixplicity.fontview.FontTextView
                if (dindingnameOtherRef[i].isNotEmpty()) {
                    txttvisiMultiple1.text = "Jenis dinding bangunan"
                    txgttvisiMultiple1.text = dindingnameOtherRef[i]

                    txttvisiMultiple2.text = "Total area dinding"
                    /*val thesum = (tdinding[i].toFloat() * 10)
                    val form = DecimalFormat("0.00")*/
                    txgttvisiMultiple2.text = tdinding[i] + " % "
                    itemDinding.addView(child)
                } else {
                    txttvisiMultiple1.text = "Jenis dinding bangunan"
                    txgttvisiMultiple1.text = ndinding[i]

                    txttvisiMultiple2.text = "Total area dinding"
                    /*val thesum = (tdinding[i].toFloat() * 10)
                    val form = DecimalFormat("0.00")*/
                    txgttvisiMultiple2.text = tdinding[i] + " % "
                    itemDinding.addView(child)
                }

            }
        }

        //pintu dan jendela
        val doorwindownameOId = results.doorWindowTypeld
        val doorwindownameOId1 = doorwindownameOId.replace("[", "['")
        val doorwindownameOId2 = doorwindownameOId1.replace("]", "']")
        val doorwindownameOId3 = doorwindownameOId2.replace(", ", "','")
        val doorwindownameOIdRef = Gson().fromJson(doorwindownameOId3, Array<String>::class.java).toList()

        val ndoor1 = results.doorWindowTypeName
        val ndoor2 = ndoor1.replace("[", "['")
        val ndoor3 = ndoor2.replace("]", "']")
        val ndoor4 = ndoor3.replace(", ", "','")
        val ndoor = Gson().fromJson(ndoor4, Array<String>::class.java).toList()

        val doorwindownameOther = results.doorWindowTypeNameOther
        val doorwindownameOther1 = doorwindownameOther.replace("[", "['")
        val doorwindownameOther2 = doorwindownameOther1.replace("]", "']")
        val doorwindownameOther3 = doorwindownameOther2.replace(", ", "','")
        val doorwindownameOtherRef = Gson().fromJson(doorwindownameOther3, Array<String>::class.java).toList()

        val tdoor1 = results.pbDoorWindowTypeAreaTotal
        val tdoor2 = tdoor1.replace("[", "['")
        val tdoor3 = tdoor2.replace("]", "']")
        val tdoor4 = tdoor3.replace(", ", "','")
        val tdoor = Gson().fromJson(tdoor4, Array<String>::class.java).toList()

        val itempintuJendela = activity!!.findViewById<LinearLayout>(R.id.llitempintuJendela)
        itempintuJendela.removeAllViews()
        if (doorwindownameOIdRef.isNotEmpty()) {
            for (i in doorwindownameOIdRef.indices) {
                val child = layoutInflater.inflate(R.layout.item_detail_multiple_vertical, null)
                val txttvisiMultiple1 = child.findViewById(R.id.tvtitleMultiple1) as com.pixplicity.fontview.FontTextView
                val txgttvisiMultiple1 = child.findViewById(R.id.tvisiMultiple1) as com.pixplicity.fontview.FontTextView

                val txttvisiMultiple2 = child.findViewById(R.id.tvtitleMultiple2) as com.pixplicity.fontview.FontTextView
                val txgttvisiMultiple2 = child.findViewById(R.id.tvisiMultiple2) as com.pixplicity.fontview.FontTextView

                if (doorwindownameOtherRef[i].isNotEmpty()) {
                    txttvisiMultiple1.text = "Jenis pintu dan jendela bangunan"
                    txgttvisiMultiple1.text = doorwindownameOtherRef[i]

                    txttvisiMultiple2.text = "Total area pintu dan jendela"
                    /*val thesum = (tdoor[i].toFloat() * 10)
                    val form = DecimalFormat("0.00")*/
                    txgttvisiMultiple2.text = tdoor[i] + " % "
                    itempintuJendela.addView(child)
                } else {
                    txttvisiMultiple1.text = "Jenis pintu dan jendela bangunan"
                    txgttvisiMultiple1.text = ndoor[i]

                    txttvisiMultiple2.text = "Total area pintu dan jendela"
                    /*val thesum = (tdoor[i].toFloat() * 10)
                    val form = DecimalFormat("0.00")*/
                    txgttvisiMultiple2.text = tdoor[i] + " % "
                    itempintuJendela.addView(child)
                }


            }
        }

        //lantai
        val lantainameId = results.floorTypeId
        val lantainameId1 = lantainameId.replace("[", "['")
        val lantainameId2 = lantainameId1.replace("]", "']")
        val lantainameId3 = lantainameId2.replace(", ", "','")
        val lantainameIdRef = Gson().fromJson(lantainameId3, Array<String>::class.java).toList()

        val nlantai1 = results.floorTypeName
        val nlantai2 = nlantai1.replace("[", "['")
        val nlantai3 = nlantai2.replace("]", "']")
        val nlantai4 = nlantai3.replace(", ", "','")
        val nlantai = Gson().fromJson(nlantai4, Array<String>::class.java).toList()

        val lantainameOther = results.floorTypeNameOther
        val lantainameOther1 = lantainameOther.replace("[", "['")
        val lantainameOther2 = lantainameOther1.replace("]", "']")
        val lantainameOther3 = lantainameOther2.replace(", ", "','")
        val lantainameOtherRef = Gson().fromJson(lantainameOther3, Array<String>::class.java).toList()

        val tlantai1 = results.pbFloorTypeAreaTotal
        val tlantai2 = tlantai1.replace("[", "['")
        val tlantai3 = tlantai2.replace("]", "']")
        val tlantai4 = tlantai3.replace(", ", "','")
        val tlantai = Gson().fromJson(tlantai4, Array<String>::class.java).toList()

        val itemLantai = activity!!.findViewById<LinearLayout>(R.id.llitemLantai)
        itemLantai.removeAllViews()
        if (lantainameIdRef.isNotEmpty()) {
            for (i in lantainameIdRef.indices) {
                val child = layoutInflater.inflate(R.layout.item_detail_multiple_vertical, null)
                val txttvisiMultiple1 = child.findViewById(R.id.tvtitleMultiple1) as com.pixplicity.fontview.FontTextView
                val txgttvisiMultiple1 = child.findViewById(R.id.tvisiMultiple1) as com.pixplicity.fontview.FontTextView

                val txttvisiMultiple2 = child.findViewById(R.id.tvtitleMultiple2) as com.pixplicity.fontview.FontTextView
                val txgttvisiMultiple2 = child.findViewById(R.id.tvisiMultiple2) as com.pixplicity.fontview.FontTextView
                if (lantainameOtherRef[i].isNotEmpty()) {
                    txttvisiMultiple1.text = "Jenis lantai bangunan"
                    txgttvisiMultiple1.text = lantainameOtherRef[i]

                    txttvisiMultiple2.text = "Total area lantai"
                    /*val thesum = (tlantai[i].toFloat() * 10)
                    val form = DecimalFormat("0.00")*/
                    txgttvisiMultiple2.text = tlantai[i] + " % "
                    itemLantai.addView(child)
                } else {
                    txttvisiMultiple1.text = "Jenis lantai bangunan"
                    txgttvisiMultiple1.text = nlantai[i]

                    txttvisiMultiple2.text = "Total area lantai"
                    /*val thesum = (tlantai[i].toFloat() * 10)
                    val form = DecimalFormat("0.00")*/
                    txgttvisiMultiple2.text = tlantai[i] + " % "
                    itemLantai.addView(child)
                }


            }
        }

    }

    @SuppressLint("SetTextI18n", "InflateParams")
    override fun onsuccessgetDetalBangunan(responseDataDetailBangunan: ResponseDataDetailBangunan) {
        val itemPondasi = activity!!.findViewById<LinearLayout>(R.id.llitemPondasi)
        itemPondasi.removeAllViews()
        responseDataDetailBangunan.projectBuildingFoundationTypes!!.forEach {
            val child = layoutInflater.inflate(R.layout.item_detail_multiple_vertical, null)
            val txttvisiMultiple1 = child.findViewById(R.id.tvtitleMultiple1) as com.pixplicity.fontview.FontTextView
            val txgttvisiMultiple1 = child.findViewById(R.id.tvisiMultiple1) as com.pixplicity.fontview.FontTextView

            val txttvisiMultiple2 = child.findViewById(R.id.tvtitleMultiple2) as com.pixplicity.fontview.FontTextView
            val txgttvisiMultiple2 = child.findViewById(R.id.tvisiMultiple2) as com.pixplicity.fontview.FontTextView
            txttvisiMultiple1.text = "Jenis pondasi bangunan"
            txgttvisiMultiple1.text = it?.foundationTypeName ?: "-"


            //val a = convertToFloat(it?.pBFoundationTypeAreaTotal!!)
            txttvisiMultiple2.text = "Total area pondasi"
            val thesum = (it?.pBFoundationTypeAreaTotal!! * 10)
            val form = DecimalFormat("0.00")
            txgttvisiMultiple2.text = form.format(thesum) + " % "
            itemPondasi.addView(child)

        }

        val itemStruktur = activity!!.findViewById<LinearLayout>(R.id.llitemStuktur)
        itemStruktur.removeAllViews()
        responseDataDetailBangunan.projectBuildingStructureTypes!!.forEach {
            val child = layoutInflater.inflate(R.layout.item_detail_multiple_vertical, null)
            val txttvisiMultiple1 = child.findViewById(R.id.tvtitleMultiple1) as com.pixplicity.fontview.FontTextView
            val txgttvisiMultiple1 = child.findViewById(R.id.tvisiMultiple1) as com.pixplicity.fontview.FontTextView

            val txttvisiMultiple2 = child.findViewById(R.id.tvtitleMultiple2) as com.pixplicity.fontview.FontTextView
            val txgttvisiMultiple2 = child.findViewById(R.id.tvisiMultiple2) as com.pixplicity.fontview.FontTextView
            txttvisiMultiple1.text = "Jenis stuktur bangunan"
            txgttvisiMultiple1.text = it?.structureTypeName ?: "-"

            txttvisiMultiple2.text = "Total area stuktur"
            val thesum = (it?.pBStructureTypeAreaTotal!! * 10)
            val form = DecimalFormat("0.00")
            txgttvisiMultiple2.text = form.format(thesum) + " % "
            itemStruktur.addView(child)

        }

        val itemKerangkaAtap = activity!!.findViewById<LinearLayout>(R.id.llitemKerangkaAtap)
        itemKerangkaAtap.removeAllViews()
        responseDataDetailBangunan.projectBuildingRoofFrameTypes!!.forEach {
            val child = layoutInflater.inflate(R.layout.item_detail_multiple_vertical, null)
            val txttvisiMultiple1 = child.findViewById(R.id.tvtitleMultiple1) as com.pixplicity.fontview.FontTextView
            val txgttvisiMultiple1 = child.findViewById(R.id.tvisiMultiple1) as com.pixplicity.fontview.FontTextView

            val txttvisiMultiple2 = child.findViewById(R.id.tvtitleMultiple2) as com.pixplicity.fontview.FontTextView
            val txgttvisiMultiple2 = child.findViewById(R.id.tvisiMultiple2) as com.pixplicity.fontview.FontTextView
            txttvisiMultiple1.text = "Jenis kerangka atap bangunan"
            txgttvisiMultiple1.text = it?.roofFrameTypeName ?: "-"

            txttvisiMultiple2.text = "Total area kerangka atap"
            val thesum = (it?.pBRoofFrameTypeAreaTotal!! * 10)
            val form = DecimalFormat("0.00")
            txgttvisiMultiple2.text = form.format(thesum) + " % "
            itemKerangkaAtap.addView(child)

        }

        val itemPenutupAtap = activity!!.findViewById<LinearLayout>(R.id.llitemPenutupAtap)
        itemPenutupAtap.removeAllViews()
        responseDataDetailBangunan.projectBuildingRoofCoveringTypes!!.forEach {
            val child = layoutInflater.inflate(R.layout.item_detail_multiple_vertical, null)
            val txttvisiMultiple1 = child.findViewById(R.id.tvtitleMultiple1) as com.pixplicity.fontview.FontTextView
            val txgttvisiMultiple1 = child.findViewById(R.id.tvisiMultiple1) as com.pixplicity.fontview.FontTextView

            val txttvisiMultiple2 = child.findViewById(R.id.tvtitleMultiple2) as com.pixplicity.fontview.FontTextView
            val txgttvisiMultiple2 = child.findViewById(R.id.tvisiMultiple2) as com.pixplicity.fontview.FontTextView
            txttvisiMultiple1.text = "Jenis penutup atap bangunan"
            txgttvisiMultiple1.text = it?.roofCoveringTypeName ?: "-"

            txttvisiMultiple2.text = "Total area penutup atap"
            val thesum = (it?.pBRoofCoveringTypeAreaTotal!! * 10)
            val form = DecimalFormat("0.00")
            txgttvisiMultiple2.text = form.format(thesum) + " % "
            itemPenutupAtap.addView(child)

        }


        val itemPlafon = activity!!.findViewById<LinearLayout>(R.id.llitemPlafon)
        itemPlafon.removeAllViews()
        responseDataDetailBangunan.projectBuildingCeilingTypes!!.forEach {
            val child = layoutInflater.inflate(R.layout.item_detail_multiple_vertical, null)
            val txttvisiMultiple1 = child.findViewById(R.id.tvtitleMultiple1) as com.pixplicity.fontview.FontTextView
            val txgttvisiMultiple1 = child.findViewById(R.id.tvisiMultiple1) as com.pixplicity.fontview.FontTextView

            val txttvisiMultiple2 = child.findViewById(R.id.tvtitleMultiple2) as com.pixplicity.fontview.FontTextView
            val txgttvisiMultiple2 = child.findViewById(R.id.tvisiMultiple2) as com.pixplicity.fontview.FontTextView
            txttvisiMultiple1.text = "Jenis plafon bangunan"
            txgttvisiMultiple1.text = it?.ceilingTypeName ?: "-"

            txttvisiMultiple2.text = "Total area plafon"
            val thesum = (it?.pBCeilingTypeAreaTotal!! * 10)
            val form = DecimalFormat("0.00")
            txgttvisiMultiple2.text = form.format(thesum) + " % "
            itemPlafon.addView(child)

        }

        val itemDinding = activity!!.findViewById<LinearLayout>(R.id.llitemDinding)
        itemDinding.removeAllViews()
        responseDataDetailBangunan.projectBuildingWallTypes!!.forEach {
            val child = layoutInflater.inflate(R.layout.item_detail_multiple_vertical, null)
            val txttvisiMultiple1 = child.findViewById(R.id.tvtitleMultiple1) as com.pixplicity.fontview.FontTextView
            val txgttvisiMultiple1 = child.findViewById(R.id.tvisiMultiple1) as com.pixplicity.fontview.FontTextView

            val txttvisiMultiple2 = child.findViewById(R.id.tvtitleMultiple2) as com.pixplicity.fontview.FontTextView
            val txgttvisiMultiple2 = child.findViewById(R.id.tvisiMultiple2) as com.pixplicity.fontview.FontTextView
            txttvisiMultiple1.text = "Jenis dinding bangunan"
            txgttvisiMultiple1.text = it?.wallTypeName ?: "-"

            txttvisiMultiple2.text = "Total area dinding"
            val thesum = (it?.pBWallTypeAreaTotal!! * 10)
            val form = DecimalFormat("0.00")
            txgttvisiMultiple2.text = form.format(thesum) + " % "
            itemDinding.addView(child)

        }

        val itempintuJendela = activity!!.findViewById<LinearLayout>(R.id.llitempintuJendela)
        itempintuJendela.removeAllViews()
        responseDataDetailBangunan.projectBuildingDoorWindowTypes!!.forEach {
            val child = layoutInflater.inflate(R.layout.item_detail_multiple_vertical, null)
            val txttvisiMultiple1 = child.findViewById(R.id.tvtitleMultiple1) as com.pixplicity.fontview.FontTextView
            val txgttvisiMultiple1 = child.findViewById(R.id.tvisiMultiple1) as com.pixplicity.fontview.FontTextView

            val txttvisiMultiple2 = child.findViewById(R.id.tvtitleMultiple2) as com.pixplicity.fontview.FontTextView
            val txgttvisiMultiple2 = child.findViewById(R.id.tvisiMultiple2) as com.pixplicity.fontview.FontTextView
            txttvisiMultiple1.text = "Jenis pintu dan jendela bangunan"
            txgttvisiMultiple1.text = it?.doorWindowTypeName ?: "-"

            txttvisiMultiple2.text = "Total area pintu dan jendela"
            val thesum = (it?.pBDoorWindowTypeAreaTotal!! * 10)
            val form = DecimalFormat("0.00")
            txgttvisiMultiple2.text = form.format(thesum) + " % "
            itempintuJendela.addView(child)

        }

        val itemLantai = activity!!.findViewById<LinearLayout>(R.id.llitemLantai)
        itemLantai.removeAllViews()
        responseDataDetailBangunan.projectBuildingFloorTypes!!.forEach {
            val child = layoutInflater.inflate(R.layout.item_detail_multiple_vertical, null)
            val txttvisiMultiple1 = child.findViewById(R.id.tvtitleMultiple1) as com.pixplicity.fontview.FontTextView
            val txgttvisiMultiple1 = child.findViewById(R.id.tvisiMultiple1) as com.pixplicity.fontview.FontTextView

            val txttvisiMultiple2 = child.findViewById(R.id.tvtitleMultiple2) as com.pixplicity.fontview.FontTextView
            val txgttvisiMultiple2 = child.findViewById(R.id.tvisiMultiple2) as com.pixplicity.fontview.FontTextView
            txttvisiMultiple1.text = "Jenis lantai bangunan"
            txgttvisiMultiple1.text = it?.floorTypeName ?: "-"

            txttvisiMultiple2.text = "Total area lantai"
            val thesum = (it?.pBFloorTypeAreaTotal!! * 10)
            val form = DecimalFormat("0.00")
            txgttvisiMultiple2.text = form.format(thesum) + " % "
            itemLantai.addView(child)

        }


    }

    //without decimal digits
    private fun toPercentage(n: Float): String {
        return String.format("%.0f", n * 100) + " % "
    }

    //accept a param to determine the numbers of decimal digits
    private fun toPercentage(n: Float, digits: Int): String {
        return String.format("%." + digits + "f", n * 100) + " % "
    }

    private fun convertToFloat(doubleValue: Double): Float? {
        return doubleValue.toFloat()
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"
        internal const val TAG = "JenisDetailObjekBangunan2"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: Int, param2: String): JenisDetailObjekBangunan2 {
            val fragment = JenisDetailObjekBangunan2()
            val args = Bundle()
            args.putInt(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor