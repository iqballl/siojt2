package mki.siojt2.fragment.form_objek_bangunan.presenter

import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.fragment.form_objek_bangunan.view.FormObjekBangunan2View
import mki.siojt2.utils.rxJava.RxUtils

class FormObjek2BangunanPresenter : BasePresenter(), FormObjek2BangunanMVPPresenter {

    private var disposables: CompositeDisposable = CompositeDisposable()

    override fun getdaftarjenisPondasiBangunan() {
        disposables.add(
                dataManager.getdaftarPondasiBangunan()
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdaftarjenisPondasiBangunan(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    override fun getdaftarjenisStruckturBangunan() {
        disposables.add(
                dataManager.getdaftarStrukturBangunan()
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdaftarjenisStruckturBangunan(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    override fun getdaftarjenisKerangkaAtapBangunan() {
        disposables.add(
                dataManager.getdaftarKerangkaAtapBangunan()
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdaftarjenisKerangkaAtapBangunan(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    override fun getdaftarjenisPenutupAtapBangunan() {
        disposables.add(
                dataManager.getdaftarPenutupAtapBangunan()
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdaftarjenisPenutupAtapBangunan(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    override fun getdaftarjenisPlafonBangunan() {
        disposables.add(
                dataManager.getdaftarPlafonBangunan()
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdaftarjenisPlafonBangunan(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    override fun getdaftarDindingBangunan() {
        disposables.add(
                dataManager.getdaftarDindingBangunan()
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdaftarDindingBangunan(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    override fun getdaftarPintuJendelaBangunan() {
        disposables.add(
                dataManager.getdaftarPintuJendelaBangunan()
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdaftarPintuJendelaBangunan(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    override fun getdaftarlantaiBangunan() {
        disposables.add(
                dataManager.getdaftarLantaiBangunan()
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdaftarlantaiBangunan(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            //ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }


    private fun view(): FormObjekBangunan2View {
        return getView() as FormObjekBangunan2View
    }
}