package mki.siojt2.fragment.form_objek_bangunan.adapter_recycleview

import android.annotation.SuppressLint
import android.content.Context
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import mki.siojt2.R
import android.widget.AdapterView
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import cn.pedant.SweetAlert.SweetAlertDialog
import kotlinx.android.synthetic.main.item_building_spesfication.view.*
import mki.siojt2.fragment.form_objek_bangunan.FormObjekBangunan2
import mki.siojt2.model.data_form_objek_bangunan.DataKerangkaAtap
import mki.siojt2.model.data_form_objek_bangunan.DataPenutupAtap
import mki.siojt2.model.master_data_bangunan.ResponseDataListPenutupAtapBangunan
import mki.siojt2.model.master_data_bangunan.ResponseDataListPenutupAtapBangunanWithOutParam
import mki.siojt2.ui.activity_form_add_objek_lain.view.ActicityFormAddObjekLain


class AdapterPenutupAtap internal constructor(data: MutableList<ResponseDataListPenutupAtapBangunanWithOutParam>, context: Context, val mFragment: Fragment) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var mcontext: Context = context
    private var mData: MutableList<ResponseDataListPenutupAtapBangunanWithOutParam>? = data
    private var mDataSet: MutableList<DataPenutupAtap>? = ArrayList()

    private val TYPE_HEADER = 0
    private val TYPE_FOOTER = 1
    private val TYPE_ITEM: Int = 0

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var viewPondasiLain: LinearLayout = itemView.findViewById(R.id.llspekBangunanLain)
        internal var spinnerjenisSpesifikasi: com.weiwangcn.betterspinner.library.BetterSpinner = itemView.findViewById(R.id.spinnerJenisSpesifikasi)
        internal var ednamajenisPondasiLain: com.google.android.material.textfield.TextInputEditText = itemView.edspekBangunanLain
        internal var edvolumSpesifikasi: com.aldoapps.autoformatedittext.AutoFormatEditText = itemView.findViewById(R.id.edvolumspesifikasi)
        internal var txttitleSpekBangunan: com.pixplicity.fontview.FontTextView = itemView.findViewById(R.id.tvtitleSpekBangunan)

        init {

            spinnerjenisSpesifikasi.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
                val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListPenutupAtapBangunanWithOutParam
                val valueSpinner = adapterView.getItemAtPosition(position).toString()
                //val idsumberAir = selectedProject.cleanWaterSourceId
                //sendvalueSumberAir = selectedProject.cleanWaterSourceName
                mDataSet!![adapterPosition].roofCoveringTypeId = selectedProject.getroofCoveringTypeId().toString()
                mDataSet!![adapterPosition].roofCoveringTypeName = selectedProject.getroofCoveringTypeName().toString()

                when {
                    mDataSet!![adapterPosition].roofCoveringTypeName == "Lain-Nya" -> {
                        viewPondasiLain.visibility = View.VISIBLE
                    }
                    else -> {
                        viewPondasiLain.visibility = View.GONE
                        ednamajenisPondasiLain.text = null

                        mDataSet!![adapterPosition].pbRoofCoveringTypeAreaTotal = 0F
                        edvolumSpesifikasi.text = null
                    }
                }
                //mStepList!![adapterPosition] = .toString()
                //Toast.makeText(mcontext, mStepList.toString(), Toast.LENGTH_SHORT).show()
            }

            var convertfloatValuePondasi: Float
            edvolumSpesifikasi.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    convertfloatValuePondasi = if (s.toString().isNotEmpty() || s.toString() != "") {
                        java.lang.Float.parseFloat(s.toString())
                    } else {
                        0f
                    }
                    mDataSet!![adapterPosition].pbRoofCoveringTypeAreaTotal = convertfloatValuePondasi
                }

                override fun afterTextChanged(s: Editable) {}
            })

            ednamajenisPondasiLain.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    mDataSet!![adapterPosition].roofCoveringTypeNameOther = s.toString()
                }

                override fun afterTextChanged(s: Editable) {}
            })
        }
    }

    inner class ViewHolderFooter(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var plus: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnPlusMataPencaharian2)
        internal var minus: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnMinusMataPencaharian2)

        init {
            minus.setOnClickListener {
                val position = adapterPosition
                try {
                    mDataSet!!.removeAt(position - 1)
                    notifyItemRemoved(position)
                    notifyDataSetChanged()
                    notifyItemChanged(position)
                    //Toast.makeText(mcontext, position.toString(), Toast.LENGTH_SHORT).show()
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                }
            }
            plus.setOnClickListener {
                val position = adapterPosition
                //Toast.makeText(mcontext, "${position}", Toast.LENGTH_SHORT).show()

                when {
                    mDataSet?.get(position - 1)!!.roofCoveringTypeId!!.isEmpty() || mDataSet?.get(position - 1)!!.pbRoofCoveringTypeAreaTotal!! == 0F -> {
                        val sweetAlretLoading = SweetAlertDialog(mcontext, SweetAlertDialog.WARNING_TYPE)
                        sweetAlretLoading.titleText = "LENGKAPI DATA"
                        sweetAlretLoading.setCancelable(false)
                        sweetAlretLoading.confirmText = "OK"
                        sweetAlretLoading.setConfirmClickListener { sDialog ->
                            sDialog?.let { if (it.isShowing) it.dismiss() }
                        }
                        sweetAlretLoading.show()
                    }

                    mDataSet?.get(position - 1)!!.roofCoveringTypeName!! == "Lain-Nya" && mDataSet?.get(position - 1)!!.pbRoofCoveringTypeAreaTotal!! == 0F || mDataSet?.get(position - 1)!!.roofCoveringTypeName!! == "Lain-Nya" && mDataSet?.get(position - 1)!!.roofCoveringTypeNameOther!!.isEmpty() -> {
                        val sweetAlretLoading = SweetAlertDialog(mcontext, SweetAlertDialog.WARNING_TYPE)
                        sweetAlretLoading.titleText = "LENGKAPI DATA"
                        sweetAlretLoading.setCancelable(false)
                        sweetAlretLoading.confirmText = "OK"
                        sweetAlretLoading.setConfirmClickListener { sDialog ->
                            sDialog?.let { if (it.isShowing) it.dismiss() }
                        }
                        sweetAlretLoading.show()
                    }

                    else -> {
                        try {
                            /* mStepList!!.add(position + 2, "")
                             mStepList2!!.add(position + 2, "")
                             notifyItemInserted(position + 2)
                             notifyItemRangeInserted(position + 2, mStepList!!.size)
                             notifyDataSetChanged()*/
                            (mFragment as FormObjekBangunan2).additempenutupAtap()
                        } catch (e: ArrayIndexOutOfBoundsException) {
                            e.printStackTrace()
                        }
                    }
                }

            }
        }
    }


    @NonNull
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        /*val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_building_spesfication, viewGroup, false)
        return ViewHolder(v)*/
        return when (viewType) {
            //Inflating footer view
            TYPE_ITEM -> ViewHolder(
                    LayoutInflater.from(viewGroup.context).inflate(R.layout.item_building_spesfication, viewGroup, false))
            //Inflating recycle view item layout
            else -> ViewHolderFooter(
                    LayoutInflater.from(viewGroup.context).inflate(R.layout.item_mata_pencaharian_footer, viewGroup, false))
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val x = holder.layoutPosition

        if (holder is ViewHolder) {
            holder.txttitleSpekBangunan.text = "Jenis penutup atap ${x + 1}"

            if (mDataSet!![x].roofCoveringTypeId!!.isNotEmpty() || mDataSet!![x].pbRoofCoveringTypeAreaTotal!! != 0F) {
                holder.edvolumSpesifikasi.setText(mDataSet?.get(x)!!.pbRoofCoveringTypeAreaTotal.toString())
                holder.spinnerjenisSpesifikasi.setText(mDataSet?.get(x)!!.roofCoveringTypeName.toString())

                if (mDataSet?.get(x)!!.roofCoveringTypeNameOther.toString().isNotEmpty()) {
                    holder.viewPondasiLain.visibility = View.VISIBLE
                    holder.ednamajenisPondasiLain.setText(mDataSet?.get(x)!!.roofCoveringTypeNameOther.toString())
                } else {
                    holder.viewPondasiLain.visibility = View.GONE
                }
                //val selectedProject = adapterspinnerSumberAir2.getItem(position) as ResponseDataMataPencaharianWithOutParam
                //val spinpos = adapterspinnerSumberAir2.getItem(1).getliveliHoodName()!!.toInt()
                //holder.spinnerjenismataPencaharian.setSelection(selectedProject.getliveliHoodName()!!.toInt())
                //holder.edpenghasilanpencaharaian.setText(mStepList2?.get(x).toString())
                //holder.spinnerjenismataPencaharian.setSelection(mStepList!![x].toInt())

            } else {
                holder.viewPondasiLain.visibility = View.GONE
                holder.spinnerjenisSpesifikasi.setText("")
                holder.edvolumSpesifikasi.text = null
                holder.edvolumSpesifikasi.hint = "%"

                holder.ednamajenisPondasiLain.text = null
                holder.ednamajenisPondasiLain.hint = "....."
            }

            val adapterspinnerSumberAir2 = ArrayAdapter<ResponseDataListPenutupAtapBangunanWithOutParam>(mcontext, R.layout.item_spinner, mData!!)
            holder.spinnerjenisSpesifikasi.setAdapter(adapterspinnerSumberAir2)
        } else if (holder is ViewHolderFooter) {
            if (x <= 1) {
                holder.minus.visibility = View.GONE
            } else {
                holder.minus.visibility = View.VISIBLE
            }
        }

        /*if (x == 0) {
            holder.minus.visibility = View.GONE
        }

        if (mStepList!![x].isNotEmpty() || mStepList2!![x] != 0f) {
            holder.edvolumSpesifikasi.setText(mStepList2?.get(x).toString())
            holder.spinnerjenisSpesifikasi.setText(mStepList3?.get(x).toString())
            //holder.spinnerjenisSpesifikasi.setSelection(mStepList!![x].toInt())
        } else {
            holder.spinnerjenisSpesifikasi.setText("")
            holder.edvolumSpesifikasi.text = null
            holder.edvolumSpesifikasi.hint = "%"
        }

        val adapterspinnerSumberAir2 = ArrayAdapter<ResponseDataListPenutupAtapBangunanWithOutParam>(mcontext, R.layout.item_spinner, mData!!)
        holder.spinnerjenisSpesifikasi.setAdapter(adapterspinnerSumberAir2)*/

        /*holder.spinnerSumberAir.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                val gender = holder.spinnerSumberAir.text.toString()
                Toast.makeText(mcontext, gender, Toast.LENGTH_LONG).show()

            }
        })*/
        /*if (stepList.get(x).length() > 0)
        {
            holder.step.setText(stepList.get(x))
        }
        else
        {
            holder.step.setText(null)
            holder.step.setHint("Next Step")
            holder.step.requestFocus()
        }*/
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == mDataSet!!.size) TYPE_FOOTER else TYPE_ITEM
    }

    override fun getItemCount(): Int {
        return mDataSet!!.size + 1
    }

    fun returnDataPenutupAtap(): List<DataPenutupAtap>{
        return mDataSet!!
    }


    fun clear() {
        /*val size = this.mStepList!!.size
        this.mStepList!!.clear()
        notifyItemRangeRemoved(0, size)
        notifyDataSetChanged()

        if (mStepList!!.isEmpty() || mStepList!!.isNullOrEmpty()) {
            if (mcontext is ActicityFormAddObjekLain) {
                (mcontext as ActicityFormAddObjekLain).removeItemSumberAir()
            }
        }*/
    }

    fun addItems(data: DataPenutupAtap){
        this.mDataSet!!.add(data)
        notifyDataSetChanged()
    }


    /*fun getStepList(): java.util.ArrayList<String> {
        return mStepList!!
    }

    fun getStepList2(): java.util.ArrayList<Float> {
        return mStepList2!!
    }

    fun getStepList3(): java.util.ArrayList<String>{
        return mStepList3!!
    }*/
}