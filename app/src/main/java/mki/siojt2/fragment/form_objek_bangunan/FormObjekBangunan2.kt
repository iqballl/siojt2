package mki.siojt2.fragment.form_objek_bangunan

import android.content.Context
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import android.os.Handler
import android.util.Log
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_form_objek_add_bangunan_2_1.*
import mki.siojt2.fragment.form_objek_bangunan.adapter_recycleview.*
import mki.siojt2.fragment.form_objek_bangunan.presenter.FormObjek2BangunanPresenter
import mki.siojt2.fragment.form_objek_bangunan.view.FormObjekBangunan2View
import mki.siojt2.model.data_form_objek_bangunan.*
import mki.siojt2.model.localsave.Bangunan
import mki.siojt2.model.localsave.Session
import mki.siojt2.model.master_data_bangunan.*
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.Utils


class FormObjekBangunan2 : BaseFragment(), FormObjekBangunan2View, View.OnClickListener {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    private var mListener: OnStepTwoListener? = null

    lateinit var formObjek2BangunanPresenter: FormObjek2BangunanPresenter
    //private var dataFormObjekBangunan2: DataFormObjekBangunan2 = ArrayList()

    private var sendIdPondasiBangunan: Int? = 0
    private var sendIdStrukturBangunan: Int? = 0
    private var sendIdKerangkaAtapBangunan: Int? = 0
    private var sendIdPenutupAtapBangunan: Int? = 0
    private var sendIdPlafonBangunan: Int? = 0
    private var sendIdDindingBangunan: Int? = 0
    private var sendIdPintuJendelaBangunan: Int? = 0
    private var sendIdLantaiBangunan: Int? = 0


    /* pondasi */
    lateinit var adapterPondasi: AdapterPondasi
    private var liststepPondasi: ArrayList<String>? = null
    private var liststepPondasi2: ArrayList<Float>? = null
    private var liststepPondasi3: ArrayList<String>? = null

    private var mliststepPondasiIdFilter: ArrayList<String>? = ArrayList()
    private var mliststepPondasiValueFilter: ArrayList<Float>? = ArrayList()
    private var mListstepPondasiNameFilter: ArrayList<String>? = ArrayList()

    /* stuktur*/
    lateinit var adapterStuktur: AdapterStuktur
    private var liststepStuktur: ArrayList<String>? = null
    private var liststepStuktur2: ArrayList<Float>? = null
    private var liststepStuktur3: ArrayList<String>? = null

    private var mliststepStukturIdFilter: ArrayList<String>? = ArrayList()
    private var mliststepStukturValueFilter: ArrayList<Float>? = ArrayList()
    private var mliststepStrukturNameFilter: ArrayList<String>? = ArrayList()

    /* kerangka atap */
    lateinit var adapterKerangkaAtap: AdapterKerangkaAtap
    private var liststepKerangkaAtap: ArrayList<String>? = null
    private var liststepKerangkaAtap2: ArrayList<Float>? = null
    private var liststepKerangkaAtap3: ArrayList<String>? = null

    private var mliststepKerangkaAtapIdFilter: ArrayList<String>? = ArrayList()
    private var mliststepKerangkaAtapValueFilter: ArrayList<Float>? = ArrayList()
    private var mliststepKerangkaAtapNameFilter: ArrayList<String>? = ArrayList()

    /* penutup atap */
    lateinit var adapterpenutupAtap: AdapterPenutupAtap
    private var liststeppenutupAtap: ArrayList<String>? = null
    private var liststeppenutupAtap2: ArrayList<Float>? = null
    private var liststeppenutupAtap3: ArrayList<String>? = null

    private var mliststeppenutupAtapIdFilter: ArrayList<String>? = ArrayList()
    private var mliststeppenutupAtapValueFilter: ArrayList<Float>? = ArrayList()
    private var mliststeppenutupAtapNameFilter: ArrayList<String>? = ArrayList()

    /* plafon atap */
    lateinit var adapterPlafon: AdapterPlafon
    private var liststepPlafon: ArrayList<String>? = null
    private var liststepPlafon2: ArrayList<Float>? = null
    private var liststepPlafon3: ArrayList<String>? = null

    private var mliststepPlafonIdFilter: ArrayList<String>? = ArrayList()
    private var mliststepPlafonValue: ArrayList<Float>? = ArrayList()
    private var mliststepPlafonName: ArrayList<String>? = ArrayList()

    /* dinding */
    lateinit var adapterDinding: AdapterDinding
    private var liststepDinding: ArrayList<String>? = null
    private var liststepDinding2: ArrayList<Float>? = null
    private var liststepDinding3: ArrayList<String>? = null

    private var mliststepDindingIdFilter: ArrayList<String>? = ArrayList()
    private var mliststepDindingValue: ArrayList<Float>? = ArrayList()
    private var mliststepDindingName: ArrayList<String>? = ArrayList()

    /* pintu jendela */
    lateinit var adapterPintuJendela: AdapterPintuJendela
    private var liststepPintuJendela: ArrayList<String>? = null
    private var liststepPintuJendela2: ArrayList<Float>? = null
    private var liststepPintuJendela3: ArrayList<String>? = null

    private var mliststepPintuJendelaIdFilter: ArrayList<String>? = ArrayList()
    private var mliststepPintuJendelaValue: ArrayList<Float>? = ArrayList()
    private var mliststepPintuJendelaName: ArrayList<String>? = ArrayList()

    /* Lantai */
    lateinit var adapterLantai: AdapterLantai
    private var liststepLantai: ArrayList<String>? = null
    private var liststepLantai2: ArrayList<Float>? = null
    private var liststepLantai3: ArrayList<String>? = null

    private var mliststepLantaiIdFilter: ArrayList<String>? = ArrayList()
    private var mliststepLantaiValue: ArrayList<Float>? = ArrayList()
    private var mliststepLantaiName: ArrayList<String>? = ArrayList()


    /*val valuejenisPondasi = mutableListOf("Pondasi dalam bor pile", "Pondasi dalam mini pile", "Pondasi dalam tiang pancang",
            "Pondasi dangkal dengan pondasi tapak beton bertulang", "Pondasi dangkal dengan batu kali menerus",
            "Pondasi dangkal dengan batu kali setempat", "Lain-Nya")

    val valuekerangkapenutupAtap = mutableListOf("Rangka baja ringan kelas I", "Rangka baja ringan kelas II",
            "Rangka baja ringan kelas III", "Rangka kayu kelas I", "Rangka kayu kelas II", "Rangka kayu kelas III",
            "Kontruksi baja", "Struktur beton bertulang", "Lain-Nya")

    val valuejenispenutupAtap = mutableListOf("Genteng keramik", "Genteng kodok", "Genteng metal", "Sirap",
            "Seng", "Asbes", "Spandex/Galvalum", "Fiberglass", "Dak beton", "Lain-Nya")

    val valuejenispintuJendela = mutableListOf("Kusen pintu kayu kelas I", "Kusen pintu kayu kelas II", "Kusen pintu kayu kelas III",
            "Kusen pintu besi/baja", "Pintu dan jendela kaca rayban", "Pintu dan jendela kaca bening", "Pintu dan jendela kaca es",
            "Pintu dan jendela kaca gravir biasa", "Pintu dan jendela kaca rayban", "Pintu plywood", "Jendela kaca bening", "Jendela kaca es",
            "Rangka pintu dan jendela alumunium", "Lain-Nya")

    val valuejenisLantai = mutableListOf("Marmer lokal", "Marmer impor", "Granit lokal", "Granit impor",
            "Keramik lokal", "Keramik impor", "Ubin kayu impor (Parquet / Parket)", "Ubin parket jati",
            "Batu tempel hitam", "Batu palimanan", "Ubin teraso", "Ubin PC", "Papan", "Pelat lantai beton, tebal = 9 cm",
            "Pelat lantai beton, tebal = 10 cm", "Pelat lantai beton, tebal = 12 cm", "Pelat lantai beton, tebal = 15 cm",
            "Pelat lantai beton, tebal = 18 cm", "Rabat beton (semen ekspos)", "Slab beton, tebal =  10 cm",
            "Slab beton, tebal =  15 cm", "Slab beton, tebal =  20 cm", "Slab beton, tebal =  25 cm", "Slab beton, tebal =  30 cm",
            "Rangka pintu dan jendela alumunium", "Lain-Nya")

    val valuejenisStruktur = mutableListOf("Kontruksi baja", "Stuktur beton bertulang", "Lain-Nya")*/

    private var validPondasi: Boolean = true
    private var validStuktur: Boolean = true
    private var validKerangkaAtap: Boolean = true
    private var validPenutupAtap: Boolean = true
    private var validPlafon: Boolean = true
    private var validDinding: Boolean = true
    private var validPintuJendela: Boolean = true
    private var validLantai: Boolean = true

    private var dialogValidate: SweetAlertDialog? = null

    lateinit var realm: Realm
    lateinit var session: Session

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }

        realm = RealmController.with(this).realm
        realm.isAutoRefresh

        session = Session(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_objek_add_bangunan_2_1, container, false)
    }

    override fun setUp(view: View) {
        /*getPresenter()?.getdaftarjenisPondasiBangunan()
        getPresenter()?.getdaftarjenisStruckturBangunan()
        getPresenter()?.getdaftarjenisKerangkaAtapBangunan()
        getPresenter()?.getdaftarjenisPenutupAtapBangunan()
        getPresenter()?.getdaftarjenisPlafonBangunan()
        getPresenter()?.getdaftarDindingBangunan()
        getPresenter()?.getdaftarPintuJendelaBangunan()
        getPresenter()?.getdaftarlantaiBangunan()*/

        dialogValidate = SweetAlertDialog(activity!!, SweetAlertDialog.WARNING_TYPE)
        dialogValidate!!.setCancelable(false)
        dialogValidate!!.confirmText = "OK"
        dialogValidate!!.setConfirmClickListener { sDialog ->
            sDialog?.let { if (it.isShowing) it.dismiss() }
        }

        /* SET FIRST ITEM PER ADAPTER*/
        if (liststepStuktur == null || liststepStuktur!!.size == 0 || liststepStuktur!!.isEmpty()) {
            liststepStuktur = java.util.ArrayList()
            liststepStuktur2 = java.util.ArrayList()
            liststepStuktur3 = java.util.ArrayList()
        }

        if (liststepKerangkaAtap == null || liststepKerangkaAtap!!.size == 0 || liststepKerangkaAtap!!.isEmpty()) {
            liststepKerangkaAtap = java.util.ArrayList()
            liststepKerangkaAtap2 = java.util.ArrayList()
            liststepKerangkaAtap3 = java.util.ArrayList()
        }

        if (liststeppenutupAtap == null || liststeppenutupAtap!!.size == 0 || liststeppenutupAtap!!.isEmpty()) {
            liststeppenutupAtap = java.util.ArrayList()
            liststeppenutupAtap2 = java.util.ArrayList()
            liststeppenutupAtap3 = java.util.ArrayList()
        }

        if (liststepPlafon == null || liststepPlafon!!.size == 0 || liststepPlafon!!.isEmpty()) {
            liststepPlafon = java.util.ArrayList()
            liststepPlafon2 = java.util.ArrayList()
            liststepPlafon3 = java.util.ArrayList()
        }

        if (liststepDinding == null || liststepDinding!!.size == 0 || liststepDinding!!.isEmpty()) {
            liststepDinding = java.util.ArrayList()
            liststepDinding2 = java.util.ArrayList()
            liststepDinding3 = java.util.ArrayList()
        }

        if (liststepPintuJendela == null || liststepPintuJendela!!.size == 0 || liststepPintuJendela!!.isEmpty()) {
            liststepPintuJendela = java.util.ArrayList()
            liststepPintuJendela2 = java.util.ArrayList()
            liststepPintuJendela3 = java.util.ArrayList()
        }

        if (liststepLantai == null || liststepLantai!!.size == 0 || liststepLantai!!.isEmpty()) {
            liststepLantai = java.util.ArrayList()
            liststepLantai2 = java.util.ArrayList()
            liststepLantai3 = java.util.ArrayList()
        }

        rvjenisPondasi.isNestedScrollingEnabled = false
        rvjenisStruktur.isNestedScrollingEnabled = false
        rvjenisKerangkaAtap.isNestedScrollingEnabled = false
        rvjenispenutupAtap.isNestedScrollingEnabled = false
        rvjenisPlafon.isNestedScrollingEnabled = false

        val linearLayoutManagerPondasi = androidx.recyclerview.widget.LinearLayoutManager(activity!!)
        val linearLayoutManagerStruktur = androidx.recyclerview.widget.LinearLayoutManager(activity!!)
        val linearLayoutManagerKerangkaAtap = androidx.recyclerview.widget.LinearLayoutManager(activity!!)
        val linearLayoutManagerPenutupAtap = androidx.recyclerview.widget.LinearLayoutManager(activity!!)
        val linearLayoutManagerPlafon = androidx.recyclerview.widget.LinearLayoutManager(activity!!)
        val linearLayoutManagerDinding = androidx.recyclerview.widget.LinearLayoutManager(activity!!)
        val linearLayoutManagerPintuJendela = androidx.recyclerview.widget.LinearLayoutManager(activity!!)
        val linearLayoutManagerLantai = androidx.recyclerview.widget.LinearLayoutManager(activity!!)

        val resultRealmPondasi = realm.where(ResponseDataListPondasiBangunanWithOutParam::class.java).findAllAsync()
        val dataPondasi: MutableList<ResponseDataListPondasiBangunanWithOutParam> = realm.copyFromRealm(resultRealmPondasi)

        val resultRealmStruktur = realm.where(ResponseDataListStrukturBangunanWithOutParam::class.java).findAllAsync()
        val dataStuktur: MutableList<ResponseDataListStrukturBangunanWithOutParam> = realm.copyFromRealm(resultRealmStruktur)

        val resultRealmKerangkaAtap = realm.where(ResponseDataListKerangkaAtapBangunanWithOutParam::class.java).findAllAsync()
        val dataKerangkaAtap: MutableList<ResponseDataListKerangkaAtapBangunanWithOutParam> = realm.copyFromRealm(resultRealmKerangkaAtap)

        val resultRealmPenutupAtap = realm.where(ResponseDataListPenutupAtapBangunanWithOutParam::class.java).findAllAsync()
        val dataPenutupAtap: MutableList<ResponseDataListPenutupAtapBangunanWithOutParam> = realm.copyFromRealm(resultRealmPenutupAtap)

        val resultRealmPlafonAtap = realm.where(ResponseDataListPlafonBangunanWithOutParam::class.java).findAllAsync()
        val dataPlafonAtap: MutableList<ResponseDataListPlafonBangunanWithOutParam> = realm.copyFromRealm(resultRealmPlafonAtap)

        val resultRealmDinding = realm.where(ResponseDataListDindingBangunanWithOutParam::class.java).findAllAsync()
        val dataDinding: MutableList<ResponseDataListDindingBangunanWithOutParam> = realm.copyFromRealm(resultRealmDinding)

        val resultPintuJendela = realm.where(ResponseDataListPintuJendelaBangunanWithOutParam::class.java).findAllAsync()
        val dataPintuJendela: MutableList<ResponseDataListPintuJendelaBangunanWithOutParam> = realm.copyFromRealm(resultPintuJendela)

        val resultLantai = realm.where(ResponseDataListLantaiBangunanWithOutParam::class.java).findAllAsync()
        val dataLantai: MutableList<ResponseDataListLantaiBangunanWithOutParam> = realm.copyFromRealm(resultLantai)


        /* pondasi */
        adapterPondasi = AdapterPondasi(dataPondasi, activity!!, this)
        rvjenisPondasi.layoutManager = linearLayoutManagerPondasi
        rvjenisPondasi.adapter = adapterPondasi

        /* struktur */
        adapterStuktur = AdapterStuktur(dataStuktur, activity!!, this)
        rvjenisStruktur.layoutManager = linearLayoutManagerStruktur
        rvjenisStruktur.adapter = adapterStuktur

        /* kerangka atap */
        adapterKerangkaAtap = AdapterKerangkaAtap(dataKerangkaAtap, activity!!, this)
        rvjenisKerangkaAtap.layoutManager = linearLayoutManagerKerangkaAtap
        rvjenisKerangkaAtap.adapter = adapterKerangkaAtap

        /* penutup atap */
        adapterpenutupAtap = AdapterPenutupAtap(dataPenutupAtap, activity!!, this)
        rvjenispenutupAtap.adapter = adapterpenutupAtap
        rvjenispenutupAtap.layoutManager = linearLayoutManagerPenutupAtap

        /* plafon */
        adapterPlafon = AdapterPlafon(dataPlafonAtap, activity!!, this)
        rvjenisPlafon.adapter = adapterPlafon
        rvjenisPlafon.layoutManager = linearLayoutManagerPlafon

        /* dinding */
        adapterDinding = AdapterDinding(dataDinding, activity!!, this)
        rvjenisDinding.adapter = adapterDinding
        rvjenisDinding.layoutManager = linearLayoutManagerDinding

        /* pintu jendela */
        adapterPintuJendela = AdapterPintuJendela(dataPintuJendela, activity!!, this)
        rvjenisPintuJendela.adapter = adapterPintuJendela
        rvjenisPintuJendela.layoutManager = linearLayoutManagerPintuJendela

        /* lantai */
        adapterLantai = AdapterLantai(dataLantai, activity!!, this)
        rvjrnisLantai.adapter = adapterLantai
        rvjrnisLantai.layoutManager = linearLayoutManagerLantai

        getDataSubjek3()

        // Initializing an ArrayAdapter
        /* val spinnerArrayAdapter = object : ArrayAdapter<String>(
                 activity!!, R.layout.item_spinner, valuejenisPondasi) {
             override fun getDropDownView(position: Int, convertView: View?,
                                          parent: ViewGroup): View {
                 val view = super.getDropDownView(position, convertView, parent)
                 val tv = view as com.pixplicity.fontview.FontTextView
                 if (position % 2 == 1) {
                     // Set the item background color
                     tv.setBackgroundColor(parseColor("#F4F0FF"))
                 } else {
                     // Set the alternate item background color
                     tv.setBackgroundColor(parseColor("#c4c4c4"))
                 }
                 return view
             }
         }*/

        /* spinnerjenisPondasi.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
             val valueSpinner = adapterView.getItemAtPosition(position).toString()
             *//*if (position == parent.lastVisiblePosition) {
                edlainnyakerangkaAtap.visibility = View.VISIBLE
                Toast.makeText(activity!!, "Explain the other type of land if select 'Lainnya' ", Toast.LENGTH_LONG).show()
            } else {
                edlainnyakerangkaAtap.visibility = View.GONE
            }*//*
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListPondasiBangunan
            sendIdPondasiBangunan = selectedProject.foundationTypeId
            Toast.makeText(activity!!, sendIdPondasiBangunan.toString(), Toast.LENGTH_SHORT).show()
        }*/

        /*spinnerjenisStruktur.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            *//*if (position == parent.lastVisiblePosition) {
                edlainnyakerangkaAtap.visibility = View.VISIBLE
                Toast.makeText(activity!!, "Explain the other type of land if select 'Lainnya' ", Toast.LENGTH_LONG).show()
            } else {
                edlainnyakerangkaAtap.visibility = View.GONE
            }*//*
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListStrukturBangunan
            sendIdStrukturBangunan = selectedProject.structureTypeId
            Toast.makeText(activity!!, sendIdStrukturBangunan.toString(), Toast.LENGTH_SHORT).show()
        }

        spinnerkerangkaAtap.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            *//*if (position == parent.lastVisiblePosition) {
                edlainnyakerangkaAtap.visibility = View.VISIBLE
                Toast.makeText(activity!!, "Explain the other type of land if select 'Lainnya' ", Toast.LENGTH_LONG).show()
            } else {
                edlainnyakerangkaAtap.visibility = View.GONE
            }*//*
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListKerangkaAtapBangunan
            sendIdKerangkaAtapBangunan = selectedProject.roofFrameTypeId
            Toast.makeText(activity!!, sendIdKerangkaAtapBangunan.toString(), Toast.LENGTH_SHORT).show()
        }


        spinnerpenutupAtap.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            *//*if (position == parent.lastVisiblePosition) {
                edlainnyaPenutupAtap.visibility = View.VISIBLE
                Toast.makeText(activity!!, "Explain the other type of land if select 'Lainnya' ", Toast.LENGTH_LONG).show()
            } else {
                edlainnyaPenutupAtap.visibility = View.GONE
            }*//*
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListPenutupAtapBangunan
            sendIdPenutupAtapBangunan = selectedProject.roofCoveringTypeId
            Toast.makeText(activity!!, sendIdPenutupAtapBangunan.toString(), Toast.LENGTH_SHORT).show()
        }

        spinnerplafon.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            *//*if (position == parent.lastVisiblePosition) {
                edlainnyaPenutupAtap.visibility = View.VISIBLE
                Toast.makeText(activity!!, "Explain the other type of land if select 'Lainnya' ", Toast.LENGTH_LONG).show()
            } else {
                edlainnyaPenutupAtap.visibility = View.GONE
            }*//*
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListPlafonBangunan
            sendIdPlafonBangunan = selectedProject.ceilingTypeId
            Toast.makeText(activity!!, sendIdPlafonBangunan.toString(), Toast.LENGTH_SHORT).show()
        }

        spinnerDinding.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            *//*if (position == parent.lastVisiblePosition) {
                edlainnyaPenutupAtap.visibility = View.VISIBLE
                Toast.makeText(activity!!, "Explain the other type of land if select 'Lainnya' ", Toast.LENGTH_LONG).show()
            } else {
                edlainnyaPenutupAtap.visibility = View.GONE
            }*//*
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListDindingBangunan
            sendIdDindingBangunan = selectedProject.wallTypeId
            Toast.makeText(activity!!, sendIdDindingBangunan.toString(), Toast.LENGTH_SHORT).show()
        }

        spinnerpintuJendela.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            *//*if (position == parent.lastVisiblePosition) {
                edlainnyapintuJendela.visibility = View.VISIBLE
                Toast.makeText(activity!!, "Explain the other type of land if select 'Lainnya' ", Toast.LENGTH_LONG).show()
            } else {
                edlainnyapintuJendela.visibility = View.GONE
            }*//*
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListPintuJendelaBangunan
            sendIdPintuJendelaBangunan = selectedProject.doorWindowTypeId
            Toast.makeText(activity!!, sendIdPintuJendelaBangunan.toString(), Toast.LENGTH_SHORT).show()
        }

        spinnerjenisLantai.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position).toString()
            *//*if (position == parent.lastVisiblePosition) {
                edlainnyajenisLantai.visibility = View.VISIBLE
                Toast.makeText(activity!!, "Explain the other type of land if select 'Lainnya' ", Toast.LENGTH_LONG).show()
            } else {
                edlainnyajenisLantai.visibility = View.GONE
            }*//*
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListLantaiBangunan
            sendIdLantaiBangunan = selectedProject.floorTypeId
            Toast.makeText(activity!!, sendIdLantaiBangunan.toString(), Toast.LENGTH_SHORT).show()
        }*/
    }


    private fun getDataSubjek3() {
        if (session.id != "" && session.status == "2") {
            val results = realm.where(Bangunan::class.java).equalTo("BangunanIdTemp", session.id.toInt()).findFirst()
            if (results != null) {
                val pondasiId1 = results.foundationTypeId
                val pondasiId2 = pondasiId1.replace("[", "['")
                val pondasiId3 = pondasiId2.replace("]", "']")
                val pondasiId4 = pondasiId3.replace(", ", "','")

                val pondasiName1 = results.foundationTypeName
                val pondasiName2 = pondasiName1.replace("[", "['")
                val pondasiName3 = pondasiName2.replace("]", "']")
                val pondasiName4 = pondasiName3.replace(", ", "','")

                val pondasiNameOther1 = results.foundationTypeNameOther
                val pondasiNameOther2 = pondasiNameOther1.replace("[", "['")
                val pondasiNameOther3 = pondasiNameOther2.replace("]", "']")
                val pondasiNameOther4 = pondasiNameOther3.replace(", ", "','")

                val pondasiTotal = results.pbFoundationTypeAreaTotal
                val pondasiTota2 = pondasiTotal.replace("[", "['")
                val pondasiTota3 = pondasiTota2.replace("]", "']")
                val pondasiTota4 = pondasiTota3.replace(", ", "','")

                val datapondasiId = Gson().fromJson(pondasiId4, Array<String>::class.java).toList()
                val datapondasiName = Gson().fromJson(pondasiName4, Array<String>::class.java).toList()
                val datapondasiNameOther = Gson().fromJson(pondasiNameOther4, Array<String>::class.java).toList()
                val datapondasiPresentase = Gson().fromJson(pondasiTota4, Array<String>::class.java).toList()

                for(i in datapondasiId.indices){
                    val mdataPondasi = DataPondasi()
                    mdataPondasi.foundationTypeId = datapondasiId[i] //id
                    mdataPondasi.foundationTypeName = datapondasiName[i] //name
                    mdataPondasi.foundationTypeNameOther = datapondasiNameOther[i] //name_other
                    mdataPondasi.pbFoundationTypeAreaTotal = datapondasiPresentase[i].toFloat() //total_pondasi

                    adapterPondasi.addItems(mdataPondasi)
                    adapterPondasi.notifyDataSetChanged()
                }

                val srukturId = results.structureTypeId
                val srukturId2 = srukturId.replace("[", "['")
                val srukturId3 = srukturId2.replace("]", "']")
                val srukturId4 = srukturId3.replace(", ", "','")

                val srukturName = results.structureTypeName
                val srukturName2 = srukturName.replace("[", "['")
                val srukturName3 = srukturName2.replace("]", "']")
                val srukturName4 = srukturName3.replace(", ", "','")

                val srukturNameOther = results.structureTypeNameOther
                val srukturNameOther2 = srukturNameOther.replace("[", "['")
                val srukturNameOther3 = srukturNameOther2.replace("]", "']")
                val srukturNameOther4 = srukturNameOther3.replace(", ", "','")

                val srukturTotal = results.pbStructureTypeAreaTotal
                val srukturTotal2 = srukturTotal.replace("[", "['")
                val srukturTotal3 = srukturTotal2.replace("]", "']")
                val srukturTotal4 = srukturTotal3.replace(", ", "','")

                val datasturkturId = Gson().fromJson(srukturId4, Array<String>::class.java).toList()
                val datastrukturName = Gson().fromJson(srukturName4, Array<String>::class.java).toList()
                val datastrukturNameOther = Gson().fromJson(srukturNameOther4, Array<String>::class.java).toList()
                val liststrtotal = Gson().fromJson(srukturTotal4, Array<String>::class.java).toList()

                for(i in datasturkturId.indices){
                    val mdataStruktur = DataStruktur()
                    mdataStruktur.structureTypeId = datasturkturId[i] //id
                    mdataStruktur.structureTypeName = datastrukturName[i] //name
                    mdataStruktur.structureTypeNameOther = datastrukturNameOther[i] //name_other
                    mdataStruktur.pbStructureTypeAreaTotal = liststrtotal[i].toFloat() //total_sturktur

                    adapterStuktur.addItems(mdataStruktur)
                    adapterStuktur.notifyDataSetChanged()
                }

                val kerangkaAtapId = results.roofFrameTypeId
                val kerangkaAtapId2 = kerangkaAtapId.replace("[", "['")
                val kerangkaAtapId3 = kerangkaAtapId2.replace("]", "']")
                val kerangkaAtapId4 = kerangkaAtapId3.replace(", ", "','")

                val kerangkaAtapName = results.roofFrameTypeName
                val kerangkaAtapName2 = kerangkaAtapName.replace("[", "['")
                val kerangkaAtapName3 = kerangkaAtapName2.replace("]", "']")
                val kerangkaAtapName4 = kerangkaAtapName3.replace(", ", "','")

                val kerangkaAtapNameOther = results.roofFrameTypeNameOther
                val kerangkaAtapNameOther2 = kerangkaAtapNameOther.replace("[", "['")
                val kerangkaAtapNameOther3 = kerangkaAtapNameOther2.replace("]", "']")
                val kerangkaAtapNameOther4 = kerangkaAtapNameOther3.replace(", ", "','")

                val kerangkaAtapTotal = results.pbRoofFrameTypeAreaTotal
                val kerangkaAtapTotal2 = kerangkaAtapTotal.replace("[", "['")
                val kerangkaAtapTotal3 = kerangkaAtapTotal2.replace("]", "']")
                val kerangkaAtapTotal4 = kerangkaAtapTotal3.replace(", ", "','")

                val datakerangkaatapId = Gson().fromJson(kerangkaAtapId4, Array<String>::class.java).toList()
                val datakerangkaatapName = Gson().fromJson(kerangkaAtapName4, Array<String>::class.java).toList()
                val datakerangkaatapNameOther = Gson().fromJson(kerangkaAtapNameOther4, Array<String>::class.java).toList()
                val datakerangkaatapPersentase= Gson().fromJson(kerangkaAtapTotal4, Array<String>::class.java).toList()

                for(i in datakerangkaatapId.indices){
                    val mdatakerangkaAtap = DataKerangkaAtap()
                    mdatakerangkaAtap.roofFrameTypeId = datakerangkaatapId[i] //id
                    mdatakerangkaAtap.roofFrameTypeName = datakerangkaatapName[i] //name
                    mdatakerangkaAtap.roofFrameTypeNameOther = datakerangkaatapNameOther[i] //name_other
                    mdatakerangkaAtap.pbRoofFrameTypeAreaTotal = datakerangkaatapPersentase[i].toFloat() //total_kerangka_atap

                    adapterKerangkaAtap.addItems(mdatakerangkaAtap)
                    adapterKerangkaAtap.notifyDataSetChanged()
                }

                val penutupAtapId = results.roofCoveringTypeId
                val penutupAtapId2 = penutupAtapId.replace("[", "['")
                val penutupAtapId3 = penutupAtapId2.replace("]", "']")
                val penutupAtapId4 = penutupAtapId3.replace(", ", "','")

                val penutupAtapName = results.roofCoveringTypeName
                val penutupAtapNam2 = penutupAtapName.replace("[", "['")
                val penutupAtapNam3 = penutupAtapNam2.replace("]", "']")
                val penutupAtapNam4 = penutupAtapNam3.replace(", ", "','")

                val penutupAtapNameOther = results.roofCoveringTypeNameOther
                val penutupAtapNameOther2 = penutupAtapNameOther.replace("[", "['")
                val penutupAtapNameOther3 = penutupAtapNameOther2.replace("]", "']")
                val penutupAtapNameOther4 = penutupAtapNameOther3.replace(", ", "','")

                val penutupAtapTotal = results.pbRoofCoveringTypeAreaTotal
                val penutupAtapTotal2 = penutupAtapTotal.replace("[", "['")
                val penutupAtapTotal3 = penutupAtapTotal2.replace("]", "']")
                val penutupAtapTotal4 = penutupAtapTotal3.replace(", ", "','")

                val datapenutupAtapId = Gson().fromJson(penutupAtapId4, Array<String>::class.java).toList()
                val datapenutupaAtapName = Gson().fromJson(penutupAtapNam4, Array<String>::class.java).toList()
                val datapenutupAtapNameOther = Gson().fromJson(penutupAtapNameOther4, Array<String>::class.java).toList()
                val datapenutupAtapPersentase= Gson().fromJson(penutupAtapTotal4, Array<String>::class.java).toList()

                for(i in datapenutupAtapId.indices){
                    val mdatapenutupAtap = DataPenutupAtap()
                    mdatapenutupAtap.roofCoveringTypeId = datapenutupAtapId[i] //id
                    mdatapenutupAtap.roofCoveringTypeName = datapenutupaAtapName[i] //name
                    mdatapenutupAtap.roofCoveringTypeNameOther = datapenutupAtapNameOther[i] //name_other
                    mdatapenutupAtap.pbRoofCoveringTypeAreaTotal = datapenutupAtapPersentase[i].toFloat() //total_penutup_atap

                    adapterpenutupAtap.addItems(mdatapenutupAtap)
                    adapterpenutupAtap.notifyDataSetChanged()
                }

                val plafonId = results.ceilingTypeId
                val plafonId2 = plafonId.replace("[", "['")
                val plafonId3 = plafonId2.replace("]", "']")
                val plafonId4 = plafonId3.replace(", ", "','")

                val plafonName = results.ceilingTypeName
                val plafonName2 = plafonName.replace("[", "['")
                val plafonName3 = plafonName2.replace("]", "']")
                val plafonName4 = plafonName3.replace(", ", "','")

                val plafonNameOther = results.ceilingTypeNameOther
                val plafonNameOther2 = plafonNameOther.replace("[", "['")
                val plafonNameOther3 = plafonNameOther2.replace("]", "']")
                val plafonNameOther4 = plafonNameOther3.replace(", ", "','")

                val plafonTotal = results.pbCeilingTypeAreaTotal
                val plafonTotal2 = plafonTotal.replace("[", "['")
                val plafonTotal3 = plafonTotal2.replace("]", "']")
                val plafonTotal4 = plafonTotal3.replace(", ", "','")

                val dataplafonId = Gson().fromJson(plafonId4, Array<String>::class.java).toList()
                val dataplafonName = Gson().fromJson(plafonName4, Array<String>::class.java).toList()
                val dataplafonNameOther = Gson().fromJson(plafonNameOther4, Array<String>::class.java).toList()
                val dataplafonPersentase = Gson().fromJson(plafonTotal4, Array<String>::class.java).toList()

                for(i in dataplafonId.indices){
                    val mdataPlafon = DataPlafon()
                    mdataPlafon.ceilingTypeId = dataplafonId[i] //id
                    mdataPlafon.ceilingTypeName = dataplafonName[i] //name
                    mdataPlafon.ceilingTypeNameOther = dataplafonNameOther[i] //name_other
                    mdataPlafon.pbCeilingTypeAreaTotal = dataplafonPersentase[i].toFloat() //total_plafon

                    adapterPlafon.addItems(mdataPlafon)
                    adapterPlafon.notifyDataSetChanged()
                }

                val dindingId = results.wallTypeId
                val dindingId2 = dindingId.replace("[", "['")
                val dindingId3 = dindingId2.replace("]", "']")
                val dindingId4 = dindingId3.replace(", ", "','")

                val dindingName = results.wallTypeName
                val dindingName2 = dindingName.replace("[", "['")
                val dindingName3 = dindingName2.replace("]", "']")
                val dindingName4 = dindingName3.replace(", ", "','")

                val dindingNameOther = results.wallTypeNameOther
                val dindingNameOther2 = dindingNameOther.replace("[", "['")
                val dindingNameOther3 = dindingNameOther2.replace("]", "']")
                val dindingNameOther4 = dindingNameOther3.replace(", ", "','")

                val dindingTotal = results.pbfWallTypeAreaTotal
                val dindingTota2 = dindingTotal.replace("[", "['")
                val dindingTota3 = dindingTota2.replace("]", "']")
                val dindingTota4 = dindingTota3.replace(", ", "','")

                val datadindingId = Gson().fromJson(dindingId4, Array<String>::class.java).toList()
                val datadindingName = Gson().fromJson(dindingName4, Array<String>::class.java).toList()
                val datadindingNameOther = Gson().fromJson(dindingNameOther4, Array<String>::class.java).toList()
                val datadindingPersentase = Gson().fromJson(dindingTota4, Array<String>::class.java).toList()

                for(i in datadindingId.indices){
                    val mdataDinding = DataDinding()
                    mdataDinding.wallTypeId = datadindingId[i] //id
                    mdataDinding.wallTypeTypeName = datadindingName[i] //name
                    mdataDinding.wallTypeNameOther = datadindingNameOther[i] //name_other
                    mdataDinding.pbFWallTypeAreaTotal = datadindingPersentase[i].toFloat() //total_dinding

                    adapterDinding.addItems(mdataDinding)
                    adapterDinding.notifyDataSetChanged()
                }

                val pintuJendelaId = results.doorWindowTypeld
                val pintuJendelaId2 = pintuJendelaId.replace("[", "['")
                val pintuJendelaId3 = pintuJendelaId2.replace("]", "']")
                val pintuJendelaId4 = pintuJendelaId3.replace(", ", "','")

                val pintuJendelaName = results.doorWindowTypeName
                val pintuJendelaName2 = pintuJendelaName.replace("[", "['")
                val pintuJendelaName3 = pintuJendelaName2.replace("]", "']")
                val pintuJendelaName4 = pintuJendelaName3.replace(", ", "','")

                val pintuJendelaNameOther = results.doorWindowTypeNameOther
                val pintuJendelaNameOthe2 = pintuJendelaNameOther.replace("[", "['")
                val pintuJendelaNameOthe3 = pintuJendelaNameOthe2.replace("]", "']")
                val pintuJendelaNameOthe4 = pintuJendelaNameOthe3.replace(", ", "','")

                val pintuJendelaTotal = results.pbDoorWindowTypeAreaTotal
                val pintuJendelaTotal2 = pintuJendelaTotal.replace("[", "['")
                val pintuJendelaTotal3 = pintuJendelaTotal2.replace("]", "']")
                val pintuJendelaTotal4 = pintuJendelaTotal3.replace(", ", "','")

                val dataPintuJendelaId = Gson().fromJson(pintuJendelaId4, Array<String>::class.java).toList()
                val dataPintuJendelaName = Gson().fromJson(pintuJendelaName4, Array<String>::class.java).toList()
                val dataPintuJendelaNameOther = Gson().fromJson(pintuJendelaNameOthe4, Array<String>::class.java).toList()
                val dataPintuJendelaPersentase = Gson().fromJson(pintuJendelaTotal4, Array<String>::class.java).toList()

                for(i in dataPintuJendelaId.indices){
                    val mdataPintuJendela = DataPintuJendela()
                    mdataPintuJendela.doorWindowTypeld = dataPintuJendelaId[i] //id
                    mdataPintuJendela.doorWindowTypeName = dataPintuJendelaName[i] //name
                    mdataPintuJendela.doorWindowTypeNameOther = dataPintuJendelaNameOther[i] //name_other
                    mdataPintuJendela.pbDoorWindowTypeAreaTotal = dataPintuJendelaPersentase[i].toFloat() //total_dinding

                    adapterPintuJendela.addItems(mdataPintuJendela)
                    adapterPintuJendela.notifyDataSetChanged()
                }

                val lantaiId = results.floorTypeId
                val lantaiId2 = lantaiId.replace("[", "['")
                val lantaiId3 = lantaiId2.replace("]", "']")
                val lantaiId4 = lantaiId3.replace(", ", "','")

                val lantaiName = results.floorTypeName
                val lantaiName2 = lantaiName.replace("[", "['")
                val lantaiName3 = lantaiName2.replace("]", "']")
                val lantaiName4 = lantaiName3.replace(", ", "','")

                val lantaiNameOther = results.floorTypeNameOther
                val lantaiNameOther2 = lantaiNameOther.replace("[", "['")
                val lantaiNameOther3 = lantaiNameOther2.replace("]", "']")
                val lantaiNameOther4 = lantaiNameOther3.replace(", ", "','")

                val lantaiTotal = results.pbFloorTypeAreaTotal
                val lantaiTotal2 = lantaiTotal.replace("[", "['")
                val lantaiTotal3 = lantaiTotal2.replace("]", "']")
                val lantaiTotal4 = lantaiTotal3.replace(", ", "','")

                val dataLantaiId = Gson().fromJson(lantaiId4, Array<String>::class.java).toList()
                val dataLantaiName = Gson().fromJson(lantaiName4, Array<String>::class.java).toList()
                val dataLantaiNameOther = Gson().fromJson(lantaiNameOther4, Array<String>::class.java).toList()
                val dataLantaiPersentase = Gson().fromJson(lantaiTotal4, Array<String>::class.java).toList()

                for(i in dataLantaiId.indices){
                    val mdataLantai = DataLantai()
                    mdataLantai.floorTypeId = dataLantaiId[i] //id
                    mdataLantai.floorTypeName = dataLantaiName[i] //name
                    mdataLantai.floorTypeNameOther = dataLantaiNameOther[i] //name_other
                    mdataLantai.pbFloorTypeAreaTotal = dataLantaiPersentase[i].toFloat() //total_dinding

                    adapterLantai.addItems(mdataLantai)
                    adapterLantai.notifyDataSetChanged()
                }
            } else {
                Log.e("data", "kosong")
            }
        } else {
            /* pondasi */
            val dataMataPondasi = DataPondasi()
            dataMataPondasi.foundationTypeId = ""
            dataMataPondasi.foundationTypeName = ""
            dataMataPondasi.foundationTypeNameOther = ""
            dataMataPondasi.pbFoundationTypeAreaTotal = 0F

            adapterPondasi.addItems(dataMataPondasi)
            adapterPondasi.notifyDataSetChanged()

            /* struktur */
            val dataStruktur = DataStruktur()
            dataStruktur.structureTypeId = ""
            dataStruktur.structureTypeName = ""
            dataStruktur.structureTypeNameOther = ""
            dataStruktur.pbStructureTypeAreaTotal = 0F

            adapterStuktur.addItems(dataStruktur)
            adapterStuktur.notifyDataSetChanged()

            /* kerangka atap */
            val datakerangkaAtap = DataKerangkaAtap()
            datakerangkaAtap.roofFrameTypeId = ""
            datakerangkaAtap.roofFrameTypeName = ""
            datakerangkaAtap.roofFrameTypeNameOther = ""
            datakerangkaAtap.pbRoofFrameTypeAreaTotal = 0F

            adapterKerangkaAtap.addItems(datakerangkaAtap)
            adapterKerangkaAtap.notifyDataSetChanged()

            /* penutup atap */
            val dataPenutupAtap = DataPenutupAtap()
            dataPenutupAtap.roofCoveringTypeId = ""
            dataPenutupAtap.roofCoveringTypeName = ""
            dataPenutupAtap.roofCoveringTypeNameOther = ""
            dataPenutupAtap.pbRoofCoveringTypeAreaTotal = 0F

            adapterpenutupAtap.addItems(dataPenutupAtap)
            adapterpenutupAtap.notifyDataSetChanged()

            /* plafon */
            val dataPlafon = DataPlafon()
            dataPlafon.ceilingTypeId = ""
            dataPlafon.ceilingTypeName = ""
            dataPlafon.ceilingTypeNameOther = ""
            dataPlafon.pbCeilingTypeAreaTotal = 0F

            adapterPlafon.addItems(dataPlafon)
            adapterPlafon.notifyDataSetChanged()

            /* dinding */
            val dataDinding = DataDinding()
            dataDinding.wallTypeId = ""
            dataDinding.wallTypeTypeName = ""
            dataDinding.wallTypeNameOther = ""
            dataDinding.pbFWallTypeAreaTotal = 0F

            adapterDinding.addItems(dataDinding)
            adapterDinding.notifyDataSetChanged()

            /* pintu & jendela */
            val dataPintuJendela = DataPintuJendela()
            dataPintuJendela.doorWindowTypeld = ""
            dataPintuJendela.doorWindowTypeName = ""
            dataPintuJendela.doorWindowTypeNameOther = ""
            dataPintuJendela.pbDoorWindowTypeAreaTotal = 0F

            adapterPintuJendela.addItems(dataPintuJendela)
            adapterPintuJendela.notifyDataSetChanged()

            /* lantai */
            val dataLantai = DataLantai()
            dataLantai.floorTypeId = ""
            dataLantai.floorTypeName = ""
            dataLantai.floorTypeNameOther = ""
            dataLantai.pbFloorTypeAreaTotal = 0F

            adapterLantai.addItems(dataLantai)
            adapterLantai.notifyDataSetChanged()
        }
    }

    private fun getPresenter(): FormObjek2BangunanPresenter? {
        formObjek2BangunanPresenter = FormObjek2BangunanPresenter()
        formObjek2BangunanPresenter.onAttach(this)
        return formObjek2BangunanPresenter
    }

    private fun validate(): Boolean {
        val valid: Boolean
        /*if (sendIdPondasiBangunan?.equals(0)!! || sendIdStrukturBangunan?.equals(0)!! ||
                sendIdKerangkaAtapBangunan?.equals(0)!! || sendIdPenutupAtapBangunan?.equals(0)!! ||
                sendIdPlafonBangunan?.equals(0)!! || sendIdDindingBangunan?.equals(0)!! ||
                sendIdPintuJendelaBangunan?.equals(0)!! || sendIdLantaiBangunan?.equals(0)!!)*/
        if (mliststepPondasiIdFilter?.isEmpty()!! || mliststepPondasiValueFilter?.isEmpty()!! ||
                mliststepStukturIdFilter?.isEmpty()!! || mliststepPondasiValueFilter?.isEmpty()!! ||
                mliststepKerangkaAtapIdFilter?.isEmpty()!! || mliststepKerangkaAtapValueFilter?.isEmpty()!! ||
                mliststeppenutupAtapIdFilter?.isEmpty()!! || mliststeppenutupAtapValueFilter?.isEmpty()!! ||
                mliststepPondasiIdFilter?.isEmpty()!! || mliststepPondasiValueFilter?.isEmpty()!! ||
                mliststepDindingIdFilter?.isEmpty()!! || mliststepDindingValue?.isEmpty()!! ||
                mliststepPintuJendelaIdFilter?.isEmpty()!! || mliststepPintuJendelaValue?.isEmpty()!! ||
                mliststepLantaiIdFilter?.isEmpty()!! || mliststepLantaiValue?.isEmpty()!!) {

            val sweetAlretLoading = SweetAlertDialog(activity!!, SweetAlertDialog.WARNING_TYPE)
            sweetAlretLoading.titleText = "LENGKAPI DATA"
            sweetAlretLoading.setCancelable(false)
            sweetAlretLoading.confirmText = "OK"
            sweetAlretLoading.setConfirmClickListener { sDialog ->
                sDialog?.let { if (it.isShowing) it.dismiss() }
            }
            sweetAlretLoading.show()

            valid = false

        } else {
            valid = true
        }

        return valid
    }

    override fun onsuccessgetdaftarjenisPondasiBangunan(responseDataListPondasiBangunan: MutableList<ResponseDataListPondasiBangunan>) {
        /*val adapterpondasiBangunan = ArrayAdapter<ResponseDataListPondasiBangunan>(activity!!, R.layout.item_spinner, responseDataListPondasiBangunan)
        //spinnerjenisPondasi.setAdapter(adapterpondasiBangunan)

        val linearLayoutManager = LinearLayoutManager(activity!!)
        adapterPondasi = AdapterPondasi(responseDataListPondasiBangunan, liststepPondasi, liststepPondasi2,liststepPondasi3, activity!!)
        rvjenisPondasi.adapter = adapterPondasi
        rvjenisPondasi.layoutManager = linearLayoutManager*/

    }

    override fun onsuccessgetdaftarjenisStruckturBangunan(responseDataListStrukturBangunan: MutableList<ResponseDataListStrukturBangunan>) {
        /*val adapterstrukturBangunan = ArrayAdapter<ResponseDataListStrukturBangunan>(activity!!, R.layout.item_spinner, responseDataListStrukturBangunan)
        //spinnerjenisStruktur.setAdapter(adapterstrukturBangunan)

        val linearLayoutManager = LinearLayoutManager(activity!!)
        adapterStuktur = AdapterStuktur(responseDataListStrukturBangunan, liststepStuktur, liststepStuktur2, liststepStuktur3, activity!!)
        rvjenisStruktur.adapter = adapterStuktur
        rvjenisStruktur.layoutManager = linearLayoutManager*/

    }

    override fun onsuccessgetdaftarjenisKerangkaAtapBangunan(responseDataListKerangkaAtapBangunan: MutableList<ResponseDataListKerangkaAtapBangunan>) {
        /*val adapterkerangkaAtapBangunan = ArrayAdapter<ResponseDataListKerangkaAtapBangunan>(activity!!, R.layout.item_spinner, responseDataListKerangkaAtapBangunan)
        //spinnerkerangkaAtap.setAdapter(adapterkerangkaAtapBangunan)

        val linearLayoutManager = LinearLayoutManager(activity!!)
        adapterKerangkaAtap = AdapterKerangkaAtap(responseDataListKerangkaAtapBangunan, liststepKerangkaAtap, liststepKerangkaAtap2, liststepKerangkaAtap3, activity!!)

        rvjenisKerangkaAtap.adapter = adapterKerangkaAtap
        rvjenisKerangkaAtap.layoutManager = linearLayoutManager*/
    }

    override fun onsuccessgetdaftarjenisPenutupAtapBangunan(responseDataListPenutupAtapBangunan: MutableList<ResponseDataListPenutupAtapBangunan>) {
        /*val adapterpenutupAtapBangunan = ArrayAdapter<ResponseDataListPenutupAtapBangunan>(activity!!, R.layout.item_spinner, responseDataListPenutupAtapBangunan)
        //spinnerpenutupAtap.setAdapter(adapterpenutupAtapBangunan)


        val linearLayoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.VERTICAL, false)
        adapterpenutupAtap = AdapterPenutupAtap(responseDataListPenutupAtapBangunan, liststeppenutupAtap, liststeppenutupAtap2, liststeppenutupAtap3, activity!!)
        rvjenispenutupAtap.adapter = adapterpenutupAtap
        rvjenispenutupAtap.layoutManager = linearLayoutManager*/
    }

    override fun onsuccessgetdaftarjenisPlafonBangunan(responseDataListPlafonBangunan: MutableList<ResponseDataListPlafonBangunan>) {
        /*val adapterpondasiBangunan = ArrayAdapter<ResponseDataListPlafonBangunan>(activity!!, R.layout.item_spinner, responseDataListPlafonBangunan)
        //spinnerplafon.setAdapter(adapterpondasiBangunan)

        val linearLayoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.VERTICAL, false)
        adapterPlafon = AdapterPlafon(responseDataListPlafonBangunan, liststepPlafon, liststepPlafon2, liststepPlafon3, activity!!)
        rvjenisPlafon.adapter = adapterPlafon
        rvjenisPlafon.layoutManager = linearLayoutManager*/
    }

    override fun onsuccessgetdaftarDindingBangunan(responseDataListDindingBangunan: MutableList<ResponseDataListDindingBangunan>) {
        /*val adapterdindingBangunan = ArrayAdapter<ResponseDataListDindingBangunan>(activity!!, R.layout.item_spinner, responseDataListDindingBangunan)
        //spinnerDinding.setAdapter(adapterdindingBangunan)

        val linearLayoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.VERTICAL, false)
        adapterDinding = AdapterDinding(responseDataListDindingBangunan, liststepDinding, liststepDinding2, liststepDinding3, activity!!)
        rvjenisDinding.adapter = adapterDinding
        rvjenisDinding.layoutManager = linearLayoutManager*/
    }

    override fun onsuccessgetdaftarPintuJendelaBangunan(responseDataListPintuJendelaBangunan: MutableList<ResponseDataListPintuJendelaBangunan>) {
        /*val adapterpintujendelaBangunan = ArrayAdapter<ResponseDataListPintuJendelaBangunan>(activity!!, R.layout.item_spinner, responseDataListPintuJendelaBangunan)
        //spinnerpintuJendela.setAdapter(adapterpintujendelaBangunan)

        val linearLayoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.VERTICAL, false)
        adapterPintuJendela = AdapterPintuJendela(responseDataListPintuJendelaBangunan, liststepPintuJendela, liststepPintuJendela2, liststepPintuJendela3, activity!!)
        rvjenisPintuJendela.adapter = adapterPintuJendela
        rvjenisPintuJendela.layoutManager = linearLayoutManager*/
    }

    override fun onsuccessgetdaftarlantaiBangunan(responseDataListLantaiBangunan: MutableList<ResponseDataListLantaiBangunan>) {
        /*val adapterlantaiBangunan = ArrayAdapter<ResponseDataListLantaiBangunan>(activity!!, R.layout.item_spinner, responseDataListLantaiBangunan)
        //spinnerjenisLantai.setAdapter(adapterlantaiBangunan)

        val linearLayoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.VERTICAL, false)
        adapterLantai = AdapterLantai(responseDataListLantaiBangunan, liststepLantai, liststepLantai2, liststepLantai3,activity!!)
        rvjrnisLantai.adapter = adapterLantai
        rvjrnisLantai.layoutManager = linearLayoutManager*/
    }

    override fun onResume() {
        super.onResume()
        //btnprevaddBanugnan2?.setOnClickListener(this)
        btnnextaddBanugnan2?.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        //btnprevaddBanugnan2?.setOnClickListener(null)
        btnnextaddBanugnan2?.setOnClickListener(null)
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btnnextaddBanugnan2) {
            hideKeyboard()
            //dataFormObjekBangunan2?.clear()
            Utils.deletedatabangunan2(activity!!)
            /* store data */
            val dataPondasi = adapterPondasi.returnDataPondasi()
            val dataStuktur = adapterStuktur.returnDataStruktur()
            val dataKerangkaAtap = adapterKerangkaAtap.returnDataKerangkaAtap()
            val dataPenutupAtap = adapterpenutupAtap.returnDataPenutupAtap()
            val dataPlafon = adapterPlafon.returnDataPlafon()
            val dataDinding = adapterDinding.returnDataPDinding()
            val dataPintuJendela = adapterPintuJendela.returnDataPintuJendela()
            val dataLantai = adapterLantai.returnDataLantai()

            val midPondasi: ArrayList<String> = ArrayList()
            val mnamaPondasi: ArrayList<String> = ArrayList()
            val mnamaPondasiLain: ArrayList<String> = ArrayList()
            val mpersentasePondasi: ArrayList<Float> = ArrayList()

            val midStuktur: ArrayList<String> = ArrayList()
            val mnamaStuktur: ArrayList<String> = ArrayList()
            val mnamaStukturLain: ArrayList<String> = ArrayList()
            val mpersentaseStuktur: ArrayList<Float> = ArrayList()

            val midkerangkaAtap: ArrayList<String> = ArrayList()
            val mnamakerangkaAtap: ArrayList<String> = ArrayList()
            val mnamakerangkaAtapLain: ArrayList<String> = ArrayList()
            val mpersentasekerangkaAtap: ArrayList<Float> = ArrayList()

            val midpenutupAtap: ArrayList<String> = ArrayList()
            val mnamapenutupAtap: ArrayList<String> = ArrayList()
            val mnamapenutupAtapLain: ArrayList<String> = ArrayList()
            val mpersentasepenutupAtap: ArrayList<Float> = ArrayList()

            val midPlafon: ArrayList<String> = ArrayList()
            val mnamaPlafon: ArrayList<String> = ArrayList()
            val mnamaPlafonLain: ArrayList<String> = ArrayList()
            val mpersentasePlafon: ArrayList<Float> = ArrayList()

            val midDinding: ArrayList<String> = ArrayList()
            val mnamaDinding: ArrayList<String> = ArrayList()
            val mnamaDindingLain: ArrayList<String> = ArrayList()
            val mpersentaseDinding: ArrayList<Float> = ArrayList()

            val midpintuJendela: ArrayList<String> = ArrayList()
            val mnamapintuJendela: ArrayList<String> = ArrayList()
            val mnamapintuJendelaLain: ArrayList<String> = ArrayList()
            val mpersentasepintuJendela: ArrayList<Float> = ArrayList()

            val midLantai: ArrayList<String> = ArrayList()
            val mnamaLantai: ArrayList<String> = ArrayList()
            val mnamaLantaiLain: ArrayList<String> = ArrayList()
            val mpersentaseLantai: ArrayList<Float> = ArrayList()

            loop@ for (i in dataPondasi.indices) {
                midPondasi.add(dataPondasi[i].foundationTypeId!!)
                mnamaPondasi.add(dataPondasi[i].foundationTypeName!!)
                mnamaPondasiLain.add(dataPondasi[i].foundationTypeNameOther!!)
                mpersentasePondasi.add(dataPondasi[i].pbFoundationTypeAreaTotal!!)
                validPondasi = true
            }

            loop@ for (i in dataStuktur.indices) {
                midStuktur.add(dataStuktur[i].structureTypeId!!)
                mnamaStuktur.add(dataStuktur[i].structureTypeName!!)
                mnamaStukturLain.add(dataStuktur[i].structureTypeNameOther!!)
                mpersentaseStuktur.add(dataStuktur[i].pbStructureTypeAreaTotal!!)
                validStuktur = true
            }

            loop@ for (i in dataKerangkaAtap.indices) {
                midkerangkaAtap.add(dataKerangkaAtap[i].roofFrameTypeId!!)
                mnamakerangkaAtap.add(dataKerangkaAtap[i].roofFrameTypeName!!)
                mnamakerangkaAtapLain.add(dataKerangkaAtap[i].roofFrameTypeNameOther!!)
                mpersentasekerangkaAtap.add(dataKerangkaAtap[i].pbRoofFrameTypeAreaTotal!!)
                validKerangkaAtap = true
            }

            loop@ for (i in dataPenutupAtap.indices) {
                midpenutupAtap.add(dataPenutupAtap[i].roofCoveringTypeId!!)
                mnamapenutupAtap.add(dataPenutupAtap[i].roofCoveringTypeName!!)
                mnamapenutupAtapLain.add(dataPenutupAtap[i].roofCoveringTypeNameOther!!)
                mpersentasepenutupAtap.add(dataPenutupAtap[i].pbRoofCoveringTypeAreaTotal!!)
                validPenutupAtap = true
            }

            loop@ for (i in dataPlafon.indices) {
                midPlafon.add(dataPlafon[i].ceilingTypeId!!)
                mnamaPlafon.add(dataPlafon[i].ceilingTypeName!!)
                mnamaPlafonLain.add(dataPlafon[i].ceilingTypeNameOther!!)
                mpersentasePlafon.add(dataPlafon[i].pbCeilingTypeAreaTotal!!)
                validPlafon = true
            }

            loop@ for (i in dataDinding.indices) {
                midDinding.add(dataDinding[i].wallTypeId!!)
                mnamaDinding.add(dataDinding[i].wallTypeTypeName!!)
                mnamaDindingLain.add(dataDinding[i].wallTypeNameOther!!)
                mpersentaseDinding.add(dataDinding[i].pbFWallTypeAreaTotal!!)
                validDinding = true
            }

            loop@ for (i in dataPintuJendela.indices) {
                midpintuJendela.add(dataPintuJendela[i].doorWindowTypeld!!)
                mnamapintuJendela.add(dataPintuJendela[i].doorWindowTypeName!!)
                mnamapintuJendelaLain.add(dataPintuJendela[i].doorWindowTypeNameOther!!)
                mpersentasepintuJendela.add(dataPintuJendela[i].pbDoorWindowTypeAreaTotal!!)
                validPintuJendela = true
            }

            loop@ for (i in dataLantai.indices) {
                midLantai.add(dataLantai[i].floorTypeId!!)
                mnamaLantai.add(dataLantai[i].floorTypeName!!)
                mnamaLantaiLain.add(dataLantai[i].floorTypeNameOther!!)
                mpersentaseLantai.add(dataLantai[i].pbFloorTypeAreaTotal!!)
                validLantai = true
            }

            if (validPondasi && validStuktur && validKerangkaAtap && validPenutupAtap && validPlafon
                    && validDinding && validPintuJendela && validLantai) {
                val dataFormObjekBangunan2 = DataFormObjekBangunan2()
                dataFormObjekBangunan2.foundationTypeId = midPondasi
                dataFormObjekBangunan2.foundationTypeName = mnamaPondasi
                dataFormObjekBangunan2.foundationTypeNameOther = mnamaPondasiLain
                dataFormObjekBangunan2.pbFoundationTypeAreaTotal = mpersentasePondasi

                dataFormObjekBangunan2.structureTypeId = midStuktur
                dataFormObjekBangunan2.structureTypeName = mnamaStuktur
                dataFormObjekBangunan2.structureTypeNameOther = mnamaStukturLain
                dataFormObjekBangunan2.pbStructureTypeAreaTotal = mpersentaseStuktur

                dataFormObjekBangunan2.roofFrameTypeId = midkerangkaAtap
                dataFormObjekBangunan2.roofFrameTypeName = mnamakerangkaAtap
                dataFormObjekBangunan2.roofFrameTypeNameOther = mnamakerangkaAtapLain
                dataFormObjekBangunan2.pbRoofFrameTypeAreaTotal = mpersentasekerangkaAtap

                dataFormObjekBangunan2.roofCoveringTypeId = midpenutupAtap
                dataFormObjekBangunan2.roofCoveringTypeName = mnamapenutupAtap
                dataFormObjekBangunan2.roofCoveringTypeNameOther = mnamapenutupAtapLain
                dataFormObjekBangunan2.pbRoofCoveringTypeAreaTotal = mpersentasepenutupAtap

                dataFormObjekBangunan2.ceilingTypeId = midPlafon
                dataFormObjekBangunan2.ceilingTypeName = mnamaPlafon
                dataFormObjekBangunan2.ceilingTypeNameOther = mnamaPlafonLain
                dataFormObjekBangunan2.pbCeilingTypeAreaTotal = mpersentasePlafon

                dataFormObjekBangunan2.doorWindowTypeld = midpintuJendela
                dataFormObjekBangunan2.doorWindowTypeName = mnamapintuJendela
                dataFormObjekBangunan2.doorWindowTypeNameOther = mnamapintuJendelaLain
                dataFormObjekBangunan2.pbDoorWindowTypeAreaTotal = mpersentasepintuJendela

                dataFormObjekBangunan2.floorTypeId = midLantai
                dataFormObjekBangunan2.floorTypeName = mnamaLantai
                dataFormObjekBangunan2.floorTypeNameOther = mnamaLantaiLain
                dataFormObjekBangunan2.pbFloorTypeAreaTotal = mpersentaseLantai

                dataFormObjekBangunan2.wallTypeId = midDinding
                dataFormObjekBangunan2.wallTypeName = mnamaDinding
                dataFormObjekBangunan2.wallTypeNameOther = mnamaDindingLain
                dataFormObjekBangunan2.pbFWallTypeAreaTotal = mpersentaseDinding

                val jsonData = Gson().toJson(dataFormObjekBangunan2)
                Utils.savedatabangunan2(jsonData, activity!!)

//                val turnsType = object : TypeToken<DataFormObjekBangunan2>() {}.type
//                val dataconvert = Gson().fromJson<DataFormObjekBangunan2>(Utils.getdatabangunan2(activity!!), turnsType)
//
//                Log.d("babab2", dataconvert.toString())
                if (mListener != null) {
                    mListener!!.onNextPressed(this)
                }

            }


            //Data pondasi bangunan
            /*val getidmultiplePondasi = adapterPondasi.getStepList()
            val getidvaluePondasi = adapterPondasi.getStepList2()
            val getNamePondasi = adapterPondasi.getStepList3()
            mliststepPondasiIdFilter!!.clear()
            mliststepPondasiValueFilter!!.clear()
            mListstepPondasiNameFilter!!.clear()
            for (row in getidmultiplePondasi) {
                if (row.isNotEmpty()) {
                    mliststepPondasiIdFilter!!.add(row)
                }
            }

            for (row in getidvaluePondasi) {
                if (row != 0f) {
                    mliststepPondasiValueFilter!!.add(row)
                }
            }

            for (row in getNamePondasi) {
                if (row.isNotEmpty()) {
                    mListstepPondasiNameFilter!!.add(row)
                }
            }
            //Data stuktur bangunan
            val getidmultipleStuktur = adapterStuktur.getStepList()
            val getidvalueStruktur = adapterStuktur.getStepList2()
            val getNameStruktur = adapterStuktur.getStepList3()
            mliststepStukturIdFilter!!.clear()
            mliststepStukturValueFilter!!.clear()
            mliststepStrukturNameFilter!!.clear()
            for (row in getidmultipleStuktur) {
                if (row.isNotEmpty()) {
                    mliststepStukturIdFilter!!.add(row)
                }
            }

            for (row in getidvalueStruktur) {
                if (row != 0f) {
                    mliststepStukturValueFilter!!.add(row)
                }
            }

            for (row in getNameStruktur) {
                if (row.isNotEmpty()) {
                    mliststepStrukturNameFilter!!.add(row)
                }
            }

            //Data  kerangka atap
            val getidmultipleKerangkaAtap = adapterKerangkaAtap.getStepList()
            val getidvalueKerangkaAtap = adapterKerangkaAtap.getStepList2()
            val getNameKerangkaAtap = adapterKerangkaAtap.getStepList3()
            mliststepKerangkaAtapIdFilter!!.clear()
            mliststepKerangkaAtapValueFilter!!.clear()
            mliststepKerangkaAtapNameFilter!!.clear()
            for (row in getidmultipleKerangkaAtap) {
                if (row.isNotEmpty()) {
                    mliststepKerangkaAtapIdFilter!!.add(row)
                }
            }

            for (row in getidvalueKerangkaAtap) {
                if (row != 0f) {
                    mliststepKerangkaAtapValueFilter!!.add(row)
                }
            }

            for (row in getNameKerangkaAtap) {
                if (row.isNotEmpty()) {
                    mliststepKerangkaAtapNameFilter!!.add(row)
                }
            }

            //Data penutup atap bangunan
            val getidmultiplePenutupAtap = adapterpenutupAtap.getStepList()
            val getidvaluePenutupAtap = adapterpenutupAtap.getStepList2()
            val getNamePenutupAtap = adapterpenutupAtap.getStepList3()

            mliststeppenutupAtapIdFilter!!.clear()
            mliststeppenutupAtapValueFilter!!.clear()
            mliststeppenutupAtapNameFilter!!.clear()
            for (row in getidmultiplePenutupAtap) {
                if (row.isNotEmpty()) {
                    mliststeppenutupAtapIdFilter!!.add(row)
                }
            }

            for (row in getidvaluePenutupAtap) {
                if (row != 0f) {
                    mliststeppenutupAtapValueFilter!!.add(row)
                }
            }

            for (row in getNamePenutupAtap) {
                if (row.isNotEmpty()) {
                    mliststeppenutupAtapNameFilter!!.add(row)
                }
            }

            //Data plafon
            val getidmultiplePlafon = adapterPlafon.getStepList()
            val getidvaluePlafon = adapterPlafon.getStepList2()
            val getNamePlafon = adapterPlafon.getStepList3()
            mliststepPlafonIdFilter!!.clear()
            mliststepPlafonValue!!.clear()
            mliststepPlafonName!!.clear()
            for (row in getidmultiplePlafon) {
                if (row.isNotEmpty()) {
                    mliststepPlafonIdFilter!!.add(row)
                }
            }

            for (row in getidvaluePlafon) {
                if (row != 0f) {
                    mliststepPlafonValue!!.add(row)
                }
            }

            for (row in getNamePlafon) {
                if (row.isNotEmpty()) {
                    mliststepPlafonName!!.add(row)
                }
            }

            //Data dinding
            val getidmultipleDinding = adapterDinding.getStepList()
            val getidvalueDinding = adapterDinding.getStepList2()
            val getNameDinding = adapterDinding.getStepList3()
            mliststepDindingIdFilter!!.clear()
            mliststepDindingValue!!.clear()
            mliststepDindingName!!.clear()
            for (row in getidmultipleDinding) {
                if (row.isNotEmpty()) {
                    mliststepDindingIdFilter!!.add(row)
                }
            }

            for (row in getidvalueDinding) {
                if (row != 0f) {
                    mliststepDindingValue!!.add(row)
                }
            }

            for (row in getNameDinding) {
                if (row.isNotEmpty()) {
                    mliststepDindingName!!.add(row)
                }
            }
            //Data pintu jendela
            val getidmultiplePintuJendela = adapterPintuJendela.getStepList()
            val getidvaluePintuJendela = adapterPintuJendela.getStepList2()
            val getNameValuPintuJendela = adapterPintuJendela.getStepList3()
            mliststepPintuJendelaIdFilter!!.clear()
            mliststepPintuJendelaValue!!.clear()
            mliststepPintuJendelaName!!.clear()
            for (row in getidmultiplePintuJendela) {
                if (row.isNotEmpty()) {
                    mliststepPintuJendelaIdFilter!!.add(row)
                }
            }

            for (row in getidvaluePintuJendela) {
                if (row != 0f) {
                    mliststepPintuJendelaValue!!.add(row)
                }
            }

            for (row in getNameValuPintuJendela) {
                if (row.isNotEmpty()) {
                    mliststepPintuJendelaName!!.add(row)
                }
            }
            //Data lantai
            val getidmultiplePiLantai = adapterLantai.getStepList()
            val getidvalueLantai = adapterLantai.getStepList2()
            val getNameValueLantai = adapterLantai.getStepList3()
            mliststepLantaiIdFilter!!.clear()
            mliststepLantaiValue!!.clear()
            mliststepLantaiName!!.clear()
            for (row in getidmultiplePiLantai) {
                if (row.isNotEmpty()) {
                    mliststepLantaiIdFilter!!.add(row)
                }
            }

            for (row in getidvalueLantai) {
                if (row != 0f) {
                    mliststepLantaiValue!!.add(row)
                }
            }

            for (row in getNameValueLantai) {
                if (row.isNotEmpty()) {
                    mliststepLantaiName!!.add(row)
                }
            }


            if (validate()) {
                dataFormObjekBangunan2?.clear()
                Utils.deletedatabangunan2(activity!!)
                dataFormObjekBangunan2?.add(DataFormObjekBangunan2(
                        mliststepPondasiIdFilter,
                        mListstepPondasiNameFilter,
                        mliststepPondasiValueFilter,
                        mliststepStukturIdFilter,
                        mliststepStrukturNameFilter,
                        mliststepStukturValueFilter,
                        mliststepKerangkaAtapIdFilter,
                        mliststepKerangkaAtapNameFilter,
                        mliststepKerangkaAtapValueFilter,
                        mliststeppenutupAtapIdFilter,
                        mliststeppenutupAtapNameFilter,
                        mliststeppenutupAtapValueFilter,
                        mliststepPlafonIdFilter,
                        mliststepPlafonName,
                        mliststepPlafonValue,
                        mliststepPintuJendelaIdFilter,
                        mliststepPintuJendelaName,
                        mliststepPintuJendelaValue,
                        mliststepLantaiIdFilter,
                        mliststepLantaiName,
                        mliststepLantaiValue,
                        mliststepDindingIdFilter,
                        mliststepDindingName,
                        mliststepDindingValue

                ))

                val jsonData = Gson().toJson(dataFormObjekBangunan2)
                Utils.savedatabangunan2(jsonData, activity!!)

                Handler().postDelayed({
//                    val turnsType = object : TypeToken<MutableList<DataFormObjekBangunan2>>() {}.type
//                    val dataconvert = Gson().fromJson<MutableList<DataFormObjekBangunan2>>(Utils.getdatabangunan2(activity!!), turnsType)
//
//                    Log.d("babab2", dataconvert.toString())
                    if (mListener != null) {
                        mListener!!.onNextPressed(this)
                    }
                }, 1000)
            }
*/
//            if (mListener != null) {
//                mListener!!.onNextPressed(this)
//            }

            /*if (validate()) {
                dataFormObjekBangunan2?.clear()
                Utils.deletedatabangunan2(activity!!)
                dataFormObjekBangunan2?.add(DataFormObjekBangunan2(
                        mliststepPondasiIdFilter,
                        mliststepPondasiValueFilter

                ))
                *//*dataFormObjekBangunan2?.add(DataFormObjekBangunan2(
                        sendIdPondasiBangunan,
                        0f,
                        sendIdStrukturBangunan,
                        java.lang.Float.parseFloat(edvolumStruktur.text.toString()),
                        sendIdKerangkaAtapBangunan,
                        java.lang.Float.parseFloat(edvolumkerangkaAtap.text.toString()),
                        sendIdPenutupAtapBangunan,
                        java.lang.Float.parseFloat(edvolumpenutupAtap.text.toString()),
                        sendIdPlafonBangunan,
                        java.lang.Float.parseFloat(edvolumPlafon.text.toString()),
                        sendIdPintuJendelaBangunan,
                        java.lang.Float.parseFloat(edvolumPintuJendela.text.toString()),
                        sendIdLantaiBangunan,
                        java.lang.Float.parseFloat(edvolumLantai.text.toString()),
                        sendIdDindingBangunan,
                        java.lang.Float.parseFloat(edvolumDinding.text.toString())

                ))*//*

                val jsonBBM = Gson().toJson(dataFormObjekBangunan2)
                Utils.savedatabangunan2(jsonBBM, activity!!)

                Handler().postDelayed({
                    val turnsType = object : TypeToken<MutableList<DataFormObjekBangunan2>>() {}.type
                    val dataconvert = Gson().fromJson<MutableList<DataFormObjekBangunan2>>(Utils.getdatabangunan2(activity!!), turnsType)

                    Log.d("babab2", dataconvert.toString())
                    *//*if (mListener != null) {
                        mListener!!.onNextPressed(this)
                    }*//*

                }, 500)
            }*/

        } else if (view.id == R.id.btnprevaddBanugnan2) {
            if (mListener != null)
                mListener!!.onBackPressed(this)
        }
    }

    fun additemPondasi() {
        val dataMataPondasi = DataPondasi()
        dataMataPondasi.foundationTypeId = ""
        dataMataPondasi.foundationTypeName = ""
        dataMataPondasi.foundationTypeNameOther = ""
        dataMataPondasi.pbFoundationTypeAreaTotal = 0F

        adapterPondasi.addItems(dataMataPondasi)
        adapterPondasi.notifyDataSetChanged()
        rvjenisPondasi.scrollToPosition(adapterPondasi.itemCount - 1)
    }

    fun additemStruktur() {
        val dataStruktur = DataStruktur()
        dataStruktur.structureTypeId = ""
        dataStruktur.structureTypeName = ""
        dataStruktur.structureTypeNameOther = ""
        dataStruktur.pbStructureTypeAreaTotal = 0F

        adapterStuktur.addItems(dataStruktur)
        adapterStuktur.notifyDataSetChanged()
        rvjenisStruktur.scrollToPosition(adapterStuktur.itemCount - 1)
    }

    fun additemKerangkaAtap() {
        val datakerangkaAtap = DataKerangkaAtap()
        datakerangkaAtap.roofFrameTypeId = ""
        datakerangkaAtap.roofFrameTypeName = ""
        datakerangkaAtap.roofFrameTypeNameOther = ""
        datakerangkaAtap.pbRoofFrameTypeAreaTotal = 0F

        adapterKerangkaAtap.addItems(datakerangkaAtap)
        adapterKerangkaAtap.notifyDataSetChanged()
        rvjenisKerangkaAtap.scrollToPosition(adapterKerangkaAtap.itemCount - 1)
    }

    fun additempenutupAtap() {
        val dataPenutupAtap = DataPenutupAtap()
        dataPenutupAtap.roofCoveringTypeId = ""
        dataPenutupAtap.roofCoveringTypeName = ""
        dataPenutupAtap.roofCoveringTypeNameOther = ""
        dataPenutupAtap.pbRoofCoveringTypeAreaTotal = 0F

        adapterpenutupAtap.addItems(dataPenutupAtap)
        adapterpenutupAtap.notifyDataSetChanged()
        rvjenispenutupAtap.scrollToPosition(adapterpenutupAtap.itemCount - 1)
    }

    fun additemPlafon() {
        val dataPlafon = DataPlafon()
        dataPlafon.ceilingTypeId = ""
        dataPlafon.ceilingTypeName = ""
        dataPlafon.ceilingTypeNameOther = ""
        dataPlafon.pbCeilingTypeAreaTotal = 0F

        adapterPlafon.addItems(dataPlafon)
        adapterPlafon.notifyDataSetChanged()
        rvjenisPlafon.scrollToPosition(adapterPlafon.itemCount - 1)
    }

    fun additemDinding() {
        val dataDinding = DataDinding()
        dataDinding.wallTypeId = ""
        dataDinding.wallTypeTypeName = ""
        dataDinding.wallTypeNameOther = ""
        dataDinding.pbFWallTypeAreaTotal = 0F

        adapterDinding.addItems(dataDinding)
        adapterDinding.notifyDataSetChanged()
        rvjenisDinding.scrollToPosition(adapterDinding.itemCount - 1)
    }

    fun additemPintuJendela() {
        val dataPintuJendela = DataPintuJendela()
        dataPintuJendela.doorWindowTypeld = ""
        dataPintuJendela.doorWindowTypeName = ""
        dataPintuJendela.doorWindowTypeNameOther = ""
        dataPintuJendela.pbDoorWindowTypeAreaTotal = 0F

        adapterPintuJendela.addItems(dataPintuJendela)
        adapterPintuJendela.notifyDataSetChanged()
        rvjenisPintuJendela.scrollToPosition(adapterPintuJendela.itemCount - 1)
    }

    fun additemLantai() {
        val dataLantai = DataLantai()
        dataLantai.floorTypeId = ""
        dataLantai.floorTypeName = ""
        dataLantai.floorTypeNameOther = ""
        dataLantai.pbFloorTypeAreaTotal = 0F

        adapterLantai.addItems(dataLantai)
        adapterLantai.notifyDataSetChanged()
        rvjrnisLantai.scrollToPosition(adapterLantai.itemCount - 1)
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnStepTwoListener) {
            mListener = context
        } else {
            throw RuntimeException(("$context must implement OnFragmentInteractionListener"))
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    interface OnStepTwoListener {
        fun onBackPressed(fragment: androidx.fragment.app.Fragment)
        fun onNextPressed(fragment: androidx.fragment.app.Fragment)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): FormObjekBangunan2 {
            val fragment = FormObjekBangunan2()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor