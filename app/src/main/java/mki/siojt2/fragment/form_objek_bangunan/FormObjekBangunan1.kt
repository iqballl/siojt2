package mki.siojt2.fragment.form_objek_bangunan

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.text.format.DateFormat
import androidx.fragment.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_form_objek_add_bangunan_1.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.fragment.form_objek_bangunan.presenter.FormObjek1BangunanPresenter
import mki.siojt2.fragment.form_objek_bangunan.view.FormObjekBangunan1View
import mki.siojt2.model.master_data_bangunan.ResponseDataListTipeBangunan
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton
import com.google.gson.Gson
import cn.pedant.SweetAlert.SweetAlertDialog
import com.whiteelephant.monthpicker.MonthPickerDialog
import io.realm.Realm
import mki.siojt2.fragment.form_subjek.FormSubjek2
import mki.siojt2.model.ResponseDataListPemilik
import mki.siojt2.model.data_form_objek_bangunan.DataFormObjekBangunan1
import mki.siojt2.model.localsave.Bangunan
import mki.siojt2.model.localsave.Session
import mki.siojt2.model.localsave.TempPemilikObjek
import mki.siojt2.model.master_data_bangunan.ResponseDataListTipeBangunanWithOutParam
import mki.siojt2.ui.acitivity_map_project.MapsProjectActivity
import mki.siojt2.ui.activity_form_add_bangunan.view.ActicityFormAddObjekBangunan
import mki.siojt2.utils.MapsProjectIntentSetting
import mki.siojt2.utils.realm.RealmController
import mki.siojt2.utils.Utils
import java.util.*
import kotlin.collections.ArrayList

class FormObjekBangunan1 : BaseFragment(), FormObjekBangunan1View, View.OnClickListener {

    // TODO: Rename and change types of parameters
    private var mParam1: Int? = null
    private var mParam2: Int? = null
    private var mListener: OnStepOneListener? = null

    lateinit var formObjek1BangunanPresenter: FormObjek1BangunanPresenter
    private var dataFormObjekBangunan1: MutableList<DataFormObjekBangunan1>? = ArrayList()

    private var dataPemilikObjek: MutableList<TempPemilikObjek>? = ArrayList()
    var documents: List<TempPemilikObjek> = ArrayList()

    private var sendidJenisBangunan: Int? = 0
    private var sendjumlahLantai: Int? = 0
    private var partyTypeId: String? = ""

    val valuejenisBangunan = mutableListOf("Rumah Tinggal", "Ruko", "Rukan", "Gudang/Pabrik", "Hotel",
            "Perkebunan", "Lain-Nya")

    lateinit var realm: Realm
    lateinit var session: Session

    lateinit var tanggalEdit: String
    private var datepicker: DatePicker? = null
    private var datepicker2: DatePicker? = null

//    lateinit var mCalendar: Calendar
//    private var mYear: Int = 0
//    private var mMonth: Int = 0
//    private var mDay: Int = 0
//    private val dateTemplate = "dd MMMM yyyy"
//    private val dateTemplate2 = "yyyy-MM-dd"
//    private var senddateBirthPicker: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getInt(ARG_PARAM1)
            mParam2 = arguments!!.getInt(ARG_PARAM2)
        }

        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        session = Session(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_objek_add_bangunan_1, container, false)
    }

    override fun setUp(view: View) {
        Log.d("bancet", mParam1.toString())

        // Get Current Date
//        mCalendar = Calendar.getInstance(Locale.getDefault())
//        mYear = mCalendar.get(Calendar.YEAR)
//        mMonth = mCalendar.get(Calendar.MONTH)
//        mDay = mCalendar.get(Calendar.DATE)

        edTemporaryFieldNumber.setText(mParam1.toString())

        /* data offline tipe bangunan */
        val realmResultTipeBangunan = realm.where(ResponseDataListTipeBangunanWithOutParam::class.java).findAllAsync()
        val dataTipeBangunan: List<ResponseDataListTipeBangunanWithOutParam> = realm.copyFromRealm(realmResultTipeBangunan)
        /* data offline pemilik */
        val realmResults = realm.where(TempPemilikObjek::class.java).findAll()
        val documents: MutableList<TempPemilikObjek> = realm.copyFromRealm(realmResults)

        setupForEdit()

        val adapterJenisBangunan = ArrayAdapter(activity!!, R.layout.item_spinner, dataTipeBangunan)
        spinnerjenisBangunan.setAdapter(adapterJenisBangunan)

        val adapterPemilikBangunan = ArrayAdapter(activity!!, R.layout.item_spinner, documents)
        spinnerpemilikBangunan.setAdapter(adapterPemilikBangunan)

        spinnerpemilikBangunan.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val valueSpinner = adapterView.getItemAtPosition(position)
            val selectedProject = adapterView.getItemAtPosition(position) as TempPemilikObjek
            partyTypeId = selectedProject.pemilikId
            //Toast.makeText(activity!!, partyTypeId.toString(), Toast.LENGTH_SHORT).show()
        }


        spinnerjenisBangunan.onItemClickListener = AdapterView.OnItemClickListener { adapterView, _, position, _ ->
            val valuespinner = adapterView.getItemAtPosition(position).toString()
            val selectedProject = adapterView.getItemAtPosition(position) as ResponseDataListTipeBangunanWithOutParam
            sendidJenisBangunan = selectedProject.getbuildingTypeId()

            if (valuespinner == "Lain-Nya") {
                lljenisBangunanLain.visibility = View.VISIBLE
            } else {
                lljenisBangunanLain.visibility = View.GONE
                edjenisBangunanLain.text = null
            }
            //Toast.makeText(activity!!, sendidJenisBangunan.toString(), Toast.LENGTH_SHORT).show()
        }

        elegantNumberButton.setOnClickListener(ElegantNumberButton.OnClickListener {
            val number = elegantNumberButton.number
            sendjumlahLantai = number.toInt()
            elegantNumberButton.number = number
        })

        elegantNumberButton.setOnValueChangeListener { _, oldValue, newValue ->
            sendjumlahLantai = newValue
            Log.d("ganteng", String.format("oldValue: %d   newValue: %d", oldValue, newValue))
        }

        tv_pick_thn_dibangun_bangunan.setSafeOnClickListener {
            selectedDate()
        }

        btnLinkUAV.setOnClickListener {
            requireActivity().startActivityForResult(Intent(requireActivity(), MapsProjectActivity::class.java)
                    .putExtra("projectId", (activity as ActicityFormAddObjekBangunan).ProjectId)
                    .putExtra("setting", MapsProjectIntentSetting(showImageUAV = true, showProjectUAV = false, showObjectUAV = true, canTouchObjectUAV = true, canSelectObjectUAV = true, defaultSelectedObjectUAVId = (activity as ActicityFormAddObjekBangunan).selectedLinkedObjectUAVId, objectType = 2, showCompareData = false))
                    ,333)
        }

    }

    private fun showDialogYearOnly() {
        val today = Calendar.getInstance()
        val builder = MonthPickerDialog.Builder(activity!!, MonthPickerDialog.OnDateSetListener { selectedMonth, selectedYear ->
            tv_thn_dibangun_bangunan.text = selectedYear.toString()
        }, today.get(Calendar.YEAR), today.get(Calendar.MONTH))

        builder.setActivatedYear(today.get(Calendar.YEAR))
                .setTitle("Tahun bangunan")
                .setMinYear(today.get(Calendar.YEAR) - 100)
                .setMaxYear(today.get(Calendar.YEAR))
                .setOnMonthChangedListener { selectedMonth ->
                    Log.d(FormSubjek2.TAG, "Selected month : $selectedMonth")
                    // Toast.makeText(MainActivity.this, " Selected month : " + selectedMonth, Toast.LENGTH_SHORT).show();
                }
                .setOnYearChangedListener { selectedYear ->
                    Log.d(FormSubjek2.TAG, "Selected year : $selectedYear")
                    tv_thn_dibangun_bangunan.text = selectedYear.toString()
                    // Toast.makeText(MainActivity.this, " Selected year : " + selectedYear, Toast.LENGTH_SHORT).show();
                }
                .showYearOnly()
                .build()
                .show()
    }

    private fun selectedDate() {
        showDialogYearOnly()
//        if ((session.id != "" && session.status == "2") && tanggalEdit.isNotEmpty()) {
//            val datePart = tanggalEdit.split("-")
//            val year = datePart[0]
//            val month = datePart[1]
//            val day = datePart[2]
//
//            val mmDay = if (month.substring(0, 1).contains("0")) {
//                val parts = day.substring(1)
//                parts.toInt()
//            } else {
//                day.toInt()
//            }
//
//            val mmMonth = if (month.substring(0, 1).contains("0")) {
//                val parts = month.substring(1)
//                parts.toInt() - 1
//            } else {
//                month.toInt() - 1
//            }
//
//            val mmYear = year.toInt()
//
//            val datePickerDialog = DatePickerDialog(activity!!, AlertDialog.THEME_HOLO_DARK, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
//                // bulan dari hasil date picker kurang satu bulan dari bulan yg dipilih sehingga ditambah + 1
//                datepicker = DatePicker(activity)
//                datepicker!!.init(year, monthOfYear + 1, dayOfMonth, null)
//                val calendar2 = GregorianCalendar(year, monthOfYear, dayOfMonth)
//
//                tv_thn_dibangun_bangunan.text = DateFormat.format(dateTemplate, calendar2.time)
//                senddateBirthPicker = DateFormat.format(dateTemplate2, calendar2.time).toString()
//            }, mmYear, mmMonth, mmDay)
//
//            if (datepicker != null) {
//                //Log.d("hasil2", "${datepicker!!.month - 1}")
//                datePickerDialog.updateDate(datepicker!!.year, datepicker!!.month - 1, datepicker!!.dayOfMonth)
//            }
//
//            datePickerDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//            datePickerDialog.datePicker.maxDate = mCalendar.timeInMillis - 1000
//            datePickerDialog.show()
//
//        } else {
//            val datePickerDialog = DatePickerDialog(activity!!, AlertDialog.THEME_HOLO_DARK, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
//                // bulan dari hasil date picker kurang satu bulan dari bulan yg dipilih sehingga ditambah + 1
//                datepicker2 = DatePicker(activity)
//                datepicker2!!.init(year, monthOfYear + 1, dayOfMonth, null)
//
//                val calendar2 = GregorianCalendar(year, monthOfYear, dayOfMonth)
//
//                tv_thn_dibangun_bangunan.text = DateFormat.format(dateTemplate, calendar2.time)
//                senddateBirthPicker = DateFormat.format(dateTemplate2, calendar2.time).toString()
//                //Toast.makeText(activity, senddateBirthPicker, Toast.LENGTH_SHORT).show()
//
//                //txtDate.setText(dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
//            }, mYear, mMonth, mDay)
//
//            if (datepicker2 != null) {
//                //Log.d("bab4", "${datepicker!!.month - 1}")
//                datePickerDialog.updateDate(datepicker2!!.year, datepicker2!!.month - 1, datepicker2!!.dayOfMonth)
//            }
//
//            datePickerDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//            datePickerDialog.datePicker.maxDate = mCalendar.timeInMillis - 1000
//            datePickerDialog.show()
//        }
    }

    private fun setupForEdit() {
        if (session.id != "" && session.status == "2") {
            val results = realm.where(Bangunan::class.java).equalTo("BangunanIdTemp", session.id.toInt()).findFirst()
            if (results != null) {
                tampilkanData(results)
                //Log.d("results", results.toString())
            } else {
                Log.e("data", "kosong")
            }
        } else if (session.id != "" && session.status == "3") {
            val results = realm.where(Bangunan::class.java).equalTo("BangunanIdTemp", session.id.toInt()).findFirst()
            if (results != null) {
                tampilkanData(results)
                //Log.d("results", results.toString())
            } else {
                Log.e("data", "kosong")
            }
        }

    }

    private fun tampilkanData(results: Bangunan) {
        (activity as ActicityFormAddObjekBangunan).selectedLinkedObjectUAVId = results.selectedLinkedObjectUAVId
        spinnerpemilikBangunan.setText(results.projectPartyName)
        partyTypeId = results.projectPartyId
        tanggalEdit = results.projectBuildingYearOfBuilt
        tv_thn_dibangun_bangunan.text = results.projectBuildingYearOfBuilt.toString()
        spinnerjenisBangunan.setText(results.projectBuildingTypeName)
        sendidJenisBangunan = results.projectBuildingTypeId
        if (results.projectBuildingTypeNameOther.isNotEmpty()) {
            lljenisBangunanLain.visibility = View.VISIBLE
            edjenisBangunanLain.setText(results.projectBuildingTypeNameOther)
        } else {
            lljenisBangunanLain.visibility = View.GONE
        }

        edJumlahLantai.setText(results.projectBuildingFloorTotal.toString())
        edluasLantai1.setText(results.projectBuildingAreaUpperRoom)
        edluasLantai2.setText(results.projectBuildingAreaLowerRoom)
    }

    private fun getPresenter(): FormObjek1BangunanPresenter? {
        formObjek1BangunanPresenter = FormObjek1BangunanPresenter()
        formObjek1BangunanPresenter.onAttach(this)
        return formObjek1BangunanPresenter
    }

    private fun validate(): Boolean {
        val sweetAlretLoading = SweetAlertDialog(activity!!, SweetAlertDialog.WARNING_TYPE)
        sweetAlretLoading.setCancelable(false)
        sweetAlretLoading.confirmText = "OK"
        sweetAlretLoading.setConfirmClickListener { sDialog ->
            sDialog?.let { if (it.isShowing) it.dismiss() }
        }

        val valid: Boolean
        if (partyTypeId?.equals(0)!! || sendidJenisBangunan?.equals(0)!!) {
            sweetAlretLoading.titleText = "LENGKAPI DATA"
            sweetAlretLoading.show()

            valid = false

        } else if (spinnerjenisBangunan.text.toString() == "Lain-Nya" && edjenisBangunanLain.text.toString().isEmpty()) {
            sweetAlretLoading.titleText = "LENGKAPI DATA"
            sweetAlretLoading.show()
            valid = false

        } else {
            valid = true
        }

        return valid
    }

    override fun onsuccessgetlistdataPemilik(responseDataListPemilik: MutableList<ResponseDataListPemilik>) {
        //getPresenter()?.getdaftarJenisBangunan()
        val adapterpemilikBangunan = ArrayAdapter<ResponseDataListPemilik>(activity!!,
                R.layout.item_spinner, responseDataListPemilik)
        adapterpemilikBangunan.setNotifyOnChange(true)
        spinnerpemilikBangunan.setAdapter(adapterpemilikBangunan)
        adapterpemilikBangunan.setNotifyOnChange(true)
    }

    override fun onsuccessgetdaftarJenisBangunan(responseDataListTipeBangunan: MutableList<ResponseDataListTipeBangunan>) {
        /*val adapterJenisBangunan = ArrayAdapter<ResponseDataListTipeBangunan>(activity!!, R.layout.item_spinner, responseDataListTipeBangunan)
        spinnerjenisBangunan.setAdapter(adapterJenisBangunan)*/

    }

    override fun onResume() {
        super.onResume()
        btnnextaddBanugnan1?.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        btnnextaddBanugnan1?.setOnClickListener(null)
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btnnextaddBanugnan1) {
            if (validate()) {
                hideKeyboard()
                Utils.deletedatabangunan1(activity!!)
                dataFormObjekBangunan1?.clear()
                dataFormObjekBangunan1?.add(DataFormObjekBangunan1(
                        partyTypeId!!,
                        spinnerpemilikBangunan.text.toString(),
                        sendidJenisBangunan,
                        spinnerjenisBangunan.text.toString(),
                        tv_thn_dibangun_bangunan.text.toString(),
                        edjenisBangunanLain.text.toString(),
                        if(edJumlahLantai.text.isNullOrEmpty()){
                            null
                        }
                        else{
                            Integer.parseInt(edJumlahLantai.text.toString())
                        },
                        edluasLantai1.text.toString().trim(),
                        edluasLantai2.text.toString().trim()
                ))

                val jsonBBM = Gson().toJson(dataFormObjekBangunan1)
                Utils.savedatabangunan1(jsonBBM, activity!!)

                /*val turnsType = object : TypeToken<MutableList<DataFormObjekBangunan1>>() {}.type
                val dataconvert = Gson().fromJson<MutableList<DataFormObjekBangunan1>>(Utils.getdatabangunan1(activity!!), turnsType)

                Log.d("babab2", dataconvert.toString())*/

                if (mListener != null) {
                    mListener!!.onNextPressed(this)
                }

            }

        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnStepOneListener) {
            mListener = context
        } else {
            throw RuntimeException(("$context must implement OnFragmentInteractionListener"))
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    interface OnStepOneListener {
        //void onFragmentInteraction(Uri uri);
        fun onNextPressed(fragment: androidx.fragment.app.Fragment)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: Int, param2: Int): FormObjekBangunan1 {
            val fragment = FormObjekBangunan1()
            val args = Bundle()
            args.putInt(ARG_PARAM1, param1)
            args.putInt(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor