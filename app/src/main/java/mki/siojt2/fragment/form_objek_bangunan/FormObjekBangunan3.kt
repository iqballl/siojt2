package mki.siojt2.fragment.form_objek_bangunan

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_form_objek_add_bangunan_3.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment

class FormObjekBangunan3 : BaseFragment(), View.OnClickListener {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    private var mListener: OnStepThreeListener? = null

    private var mYear: Int = 0
    private var mMonth: Int = 0
    private var mDay: Int = 0
    val mHour: Int = 0
    val mMinute: Int = 0

    private val dateTemplate = "dd MMMM yyyy"

    val valuesaluranTeganganListrik = mutableListOf("Terpasang", "Tidak terpasang")

    val valuesaluranAirBersih = mutableListOf("PDAM", "Water Treatment Plant (WTP)", "Boreholes",
            "Dug well", "Water from the region", "Tidak ada", "Lain-Nya")

    val valuesaluranTelepon = mutableListOf("Ada", "Tidak ada")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_objek_add_bangunan_3, container, false)
    }

    override fun setUp(view: View) {
        val adaptersaluranListrik = ArrayAdapter<String>(activity!!, android.R.layout.simple_dropdown_item_1line, valuesaluranTeganganListrik)
        spinnerSaluranListrik.setAdapter(adaptersaluranListrik)

        val adaptersaliranAirBersih = ArrayAdapter<String>(activity!!, android.R.layout.simple_dropdown_item_1line, valuesaluranAirBersih)
        spinnersumberAir.setAdapter(adaptersaliranAirBersih)

        val adaptersaluranTelepon = ArrayAdapter<String>(activity!!, android.R.layout.simple_dropdown_item_1line, valuesaluranTelepon)
        spinnersaluranTelepon.setAdapter(adaptersaluranTelepon)

        spinnerSaluranListrik.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val valueSpinner = parent.getItemAtPosition(position).toString()

            if (position + 1 == 5){
                lldayaListrik.visibility = View.VISIBLE
            }else{
                lldayaListrik.visibility = View.GONE
            }
        }

        spinnersumberAir.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val valueSpinner = parent.getItemAtPosition(position).toString()

        }

        spinnersaluranTelepon.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val valueSpinner = parent.getItemAtPosition(position).toString()
            if (position + 1 == 5){
                llsalurantelepon.visibility = View.VISIBLE
            }else{
                llsalurantelepon.visibility = View.GONE
            }
        }
    }


    override fun onResume() {
        super.onResume()
        btnprevaddBanugnan3?.setOnClickListener(this)
        btnnextaddBanugnan3?.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        btnprevaddBanugnan3?.setOnClickListener(null)
        btnnextaddBanugnan3?.setOnClickListener(null)
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btnnextaddBanugnan3) {
            if (mListener != null)
                mListener!!.onNextPressed(this)
        } else if (view.id == R.id.btnprevaddBanugnan3) {
            if (mListener != null)
                mListener!!.onBackPressed(this)
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnStepThreeListener) {
            mListener = context
        } else {
            throw RuntimeException(("$context must implement OnFragmentInteractionListener"))
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    interface OnStepThreeListener {
        fun onBackPressed(fragment: Fragment)
        fun onNextPressed(fragment: Fragment)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): FormObjekBangunan3 {
            val fragment = FormObjekBangunan3()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor