package mki.siojt2.fragment.form_objek_bangunan.presenter

import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.fragment.form_objek_bangunan.view.FormObjekBangunan1View
import mki.siojt2.utils.ErrorHandler
import mki.siojt2.utils.rxJava.RxUtils

class FormObjek1BangunanPresenter : BasePresenter(), FormObjek1BangunanMVPPresenter {

    private var disposables: CompositeDisposable = CompositeDisposable()

    override fun getlistdataPemilik(projectId: Int, landId: String) {
        view().onShowLoading()
        disposables.add(
                dataManager.getAllParties(projectId, landId)
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ responseData ->
                            view().onDismissLoading()
                            if (responseData.status == Constant.STATUS_ERROR) {
                                view().onFailed(responseData.message!!)
                            } else {
                                if (responseData.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetlistdataPemilik(responseData.result!!)
                                } else {
                                    view().onFailed(responseData.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    override fun getdaftarJenisBangunan() {
        disposables.add(
                dataManager.getdaftarTipeBangunan()
                        .doOnTerminate { view().onDismissLoading() }
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ authResponseDataObject ->
                            view().onDismissLoading()
                            if (authResponseDataObject.status == Constant.STATUS_ERROR) {
                                view().onFailed(authResponseDataObject.message!!)
                            } else {
                                if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetdaftarJenisBangunan(authResponseDataObject.result!!)
                                } else {
                                    view().onFailed(authResponseDataObject.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }


    private fun view(): FormObjekBangunan1View {
        return getView() as FormObjekBangunan1View
    }
}