package mki.siojt2.fragment.form_objek_bangunan.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface FormObjek1BangunanMVPPresenter : MVPPresenter {
    fun getdaftarJenisBangunan()
    fun getlistdataPemilik(projectId: Int, landId: String)
}