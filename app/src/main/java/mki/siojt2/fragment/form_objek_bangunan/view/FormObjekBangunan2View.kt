package mki.siojt2.fragment.form_objek_bangunan.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.master_data_bangunan.*

interface FormObjekBangunan2View : MvpView {

    fun onsuccessgetdaftarjenisPondasiBangunan(responseDataListPondasiBangunan: MutableList<ResponseDataListPondasiBangunan>)
    fun onsuccessgetdaftarjenisStruckturBangunan(responseDataListStrukturBangunan: MutableList<ResponseDataListStrukturBangunan>)
    fun onsuccessgetdaftarjenisKerangkaAtapBangunan(responseDataListKerangkaAtapBangunan: MutableList<ResponseDataListKerangkaAtapBangunan>)
    fun onsuccessgetdaftarjenisPenutupAtapBangunan(responseDataListPenutupAtapBangunan: MutableList<ResponseDataListPenutupAtapBangunan>)

    fun onsuccessgetdaftarjenisPlafonBangunan(responseDataListPlafonBangunan: MutableList<ResponseDataListPlafonBangunan>)

    fun onsuccessgetdaftarDindingBangunan(responseDataListDindingBangunan: MutableList<ResponseDataListDindingBangunan>)

    fun onsuccessgetdaftarPintuJendelaBangunan(responseDataListPintuJendelaBangunan: MutableList<ResponseDataListPintuJendelaBangunan>)

    fun onsuccessgetdaftarlantaiBangunan(responseDataListLantaiBangunan: MutableList<ResponseDataListLantaiBangunan>)

}