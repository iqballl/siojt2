package mki.siojt2.fragment.form_objek_bangunan

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_form_objek_add_bangunan_4.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment

class FormObjekBangunan4 : BaseFragment(), View.OnClickListener {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    private var mListener: OnStepFourListener? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_objek_add_bangunan_4, container, false)
    }

    override fun setUp(view: View) {

    }

    override fun onResume() {
        super.onResume()
        btnprevaddBanugnan4?.setOnClickListener(this)
        btnnextaddBanugnan4?.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        btnprevaddBanugnan4?.setOnClickListener(null)
        btnnextaddBanugnan4?.setOnClickListener(null)
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btnnextaddBanugnan4) {
            if (mListener != null)
                mListener!!.onNextPressed(this)
        } else if (view.id == R.id.btnprevaddBanugnan4) {
            if (mListener != null)
                mListener!!.onBackPressed(this)
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnStepFourListener) {
            mListener = context
        } else {
            throw RuntimeException(("$context must implement OnFragmentInteractionListener"))
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    interface OnStepFourListener {
        fun onBackPressed(fragment: androidx.fragment.app.Fragment)
        fun onNextPressed(fragment: androidx.fragment.app.Fragment)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): FormObjekBangunan4 {
            val fragment = FormObjekBangunan4()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor