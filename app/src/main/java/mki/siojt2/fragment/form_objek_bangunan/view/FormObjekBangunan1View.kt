package mki.siojt2.fragment.form_objek_bangunan.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.ResponseDataListPemilik
import mki.siojt2.model.master_data_bangunan.ResponseDataListTipeBangunan

interface FormObjekBangunan1View : MvpView {
    fun onsuccessgetdaftarJenisBangunan(responseDataListTipeBangunan: MutableList<ResponseDataListTipeBangunan>)
    fun onsuccessgetlistdataPemilik(responseDataListPemilik: MutableList<ResponseDataListPemilik>)
}