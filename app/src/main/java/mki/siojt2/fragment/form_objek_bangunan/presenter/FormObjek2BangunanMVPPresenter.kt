package mki.siojt2.fragment.form_objek_bangunan.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface FormObjek2BangunanMVPPresenter : MVPPresenter {

    fun getdaftarjenisPondasiBangunan()
    fun getdaftarjenisStruckturBangunan()
    fun getdaftarjenisKerangkaAtapBangunan()
    fun getdaftarjenisPenutupAtapBangunan()
    fun getdaftarjenisPlafonBangunan()
    fun getdaftarDindingBangunan()
    fun getdaftarPintuJendelaBangunan()
    fun getdaftarlantaiBangunan()
}