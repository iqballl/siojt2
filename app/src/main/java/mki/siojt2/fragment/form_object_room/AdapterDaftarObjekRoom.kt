package mki.siojt2.fragment.form_object_room

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.gson.Gson
import io.realm.RealmResults
import kotlinx.android.synthetic.main.item_daftar_objek_room.view.*
import mki.siojt2.R
import mki.siojt2.base.BaseViewHolder
import mki.siojt2.model.localsave.*
import mki.siojt2.ui.activity_detail_room.ActivityDetailRoom
import mki.siojt2.ui.activity_form_add_objek_room.view.ActicityFormAddObjekRoom
import mki.siojt2.utils.extension.SafeClickListener


class AdapterDaftarObjekRoom(context: Context, val activity: Activity) : RecyclerView.Adapter<BaseViewHolder>() {

    private val mContext: Context? = context
    val mDataset: MutableList<Room>? = ArrayList()
    var rowLayout = R.layout.item_daftar_objek_room
    var emptyrowLayout = R.layout.item_empty_view
    var pos = 0

    private var mCallback: Callback? = null
    private lateinit var session: Session

    fun setCallback(callback: Callback) {
        mCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            VIEW_TYPE_NORMAL -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(rowLayout, parent, false))
            VIEW_TYPE_EMPTY -> EmptyViewHolder(
                    LayoutInflater.from(parent.context).inflate(R.layout.item_empty_view, parent, false))
            else -> EmptyViewHolder(LayoutInflater.from(parent.context).inflate(emptyrowLayout, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (mDataset != null && mDataset.isNotEmpty()) {
            VIEW_TYPE_NORMAL
        } else {
            VIEW_TYPE_EMPTY
        }
    }

    override fun getItemCount(): Int {
        return if (mDataset!!.isNotEmpty()) {
            mDataset.size
        } else {
            1
        }
    }

    /*inner class RuangViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var r_no: TextView = itemView.findViewById(R.id.rowNo) as TextView
        var r_tvTitle: TextView = itemView.findViewById(R.id.tvTitle) as TextView
        var r_tvContent1: TextView = itemView.findViewById(R.id.tvContent1) as TextView
        var r_tvContent2: TextView = itemView.findViewById(R.id.tvContent2) as TextView

    }*/

    fun clear() {
        this.mDataset!!.clear()
    }

    fun addItems(data: RealmResults<Room>) {
        this.mDataset!!.addAll(data)
        notifyDataSetChanged()
    }

    interface Callback {
        fun onRepoEmptyViewRetryClick()
        fun onDismissBottomsheet()
    }

    inner class ViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }

        @SuppressLint("SetTextI18n")
        override fun onBind(position: Int) {
            super.onBind(position)

            val dataList = mDataset!![position]

            inflateData(dataList)

            session = Session(mContext)

            val pos = pos + position + 1
            if(!dataList.ownerDataName.isNullOrEmpty()){
                itemView.tvnamaSubjek.text = dataList.ownerDataName
            }

            itemView.tvLuas1.text = "Total Luas Lantai Atas : ${Html.fromHtml("${dataList.upperRoom} (m<sup><small>2</small></sup>)")}"
            itemView.tvLuas2.text = "Total Luas Lantai Bawah : ${Html.fromHtml("${dataList.lowerRoom} (m<sup><small>2</small></sup>)")}"

            itemView.btnSelngkapnya.setSafeOnClickListener {
                mCallback?.onDismissBottomsheet()
                val param = "['${dataList.roomId}','${dataList.ProjectId}']"
                val intent = ActivityDetailRoom.getStartIntent(mContext!!)
                intent.putExtra("data_current_room", param)
                mContext.startActivity(intent)
            }

            itemView.btnEdit.setSafeOnClickListener {
                mCallback?.onDismissBottomsheet()
                val param = "['${dataList.LandIdTemp}','${dataList.ProjectId}', '']"
                val intent = ActicityFormAddObjekRoom.getStartIntent(mContext!!)
                val mSession = mki.siojt2.model.Session(dataList.roomId, dataList.LandIdTemp,"2")
                intent.putExtra("data_current_tanah", param)
                intent.putExtra("data_current_session", Gson().toJson(mSession))
                session.setIdAndStatus(dataList.roomId.toString(), "2", "0")
                mContext.startActivity(intent)
            }

            itemView.btnDelete.setSafeOnClickListener {
                mCallback?.onDismissBottomsheet()
                val param = "['${dataList.LandIdTemp}','${dataList.ProjectId}', '']"
                val intent = ActicityFormAddObjekRoom.getStartIntent(mContext!!)
                val mSession = mki.siojt2.model.Session(dataList.roomId, dataList.LandIdTemp,"3")
                intent.putExtra("data_current_tanah", param)
                intent.putExtra("data_current_session", Gson().toJson(mSession))
                session.setIdAndStatus(dataList.roomId.toString(), "3", "")
                mContext.startActivity(intent)
            }
        }

        private fun inflateData(dataList: Room) {
            //nama?.let { itemView.tvMatapelajaran.text = it }
//            itemView.tvContent1.text = dataList.namaJenis
//            itemView.tvContent2.text= dataList.keterangan
        }
    }

    inner class EmptyViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }

    }

    private fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
        val safeClickListener = SafeClickListener {
            onSafeClick(it)
        }
        setOnClickListener(safeClickListener)
    }

    companion object {
        const val VIEW_TYPE_EMPTY = 0
        const val VIEW_TYPE_NORMAL = 1
    }
}
