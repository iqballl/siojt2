package mki.siojt2.fragment.form_object_room.presenter

import mki.siojt2.base.presenter.MVPPresenter

interface FormObjek1RoomMVPPresenter : MVPPresenter {
    fun getlistdataPemilik(projectId: Int, landId: String)
}