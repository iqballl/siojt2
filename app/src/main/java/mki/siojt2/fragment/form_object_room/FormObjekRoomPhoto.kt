package mki.siojt2.fragment.form_object_room

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_form_take_photo.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.model.local_save_image.DataImageRoom
import mki.siojt2.model.localsave.Room
import mki.siojt2.model.localsave.Session
import mki.siojt2.ui.activity_form_add_objek_room.view.ActicityFormAddObjekRoom
import mki.siojt2.ui.activity_image_picker.ImagePickerActivity
import mki.siojt2.utils.Utils
import mki.siojt2.utils.realm.RealmController
import java.io.ByteArrayOutputStream
import java.io.IOException

class FormObjekRoomPhoto : BaseFragment(), View.OnClickListener, AdapterTakePictureRoom.OnItemImageClickListener {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    private var mListener: OnStepTwoListener? = null

    private var mpositionImageClick: Int? = null
    lateinit var adapterTakePictureBuilding: AdapterTakePictureRoom

    private var dataDefaultTakeFotoBuilding: DataImageRoom? = null

    lateinit var realm: Realm
    lateinit var session: Session

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //set realm & set session for edit or delete flag
        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        session = Session(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_take_photo, container, false)
    }

    override fun setUp(view: View) {
        // Clearing older images from cache directory
        // don't call this line if you want to choose multiple images in the same activity
        // call this once the bitmap(s) usage is over
        ImagePickerActivity.clearCache(activity!!)

        dataDefaultTakeFotoBuilding = DataImageRoom()

        /* set adapter */
        adapterTakePictureBuilding = AdapterTakePictureRoom(activity!!, this)
        adapterTakePictureBuilding.setListnerItemImageClick(this)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity!!, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)

        rvlistPhoto.adapter = adapterTakePictureBuilding
        rvlistPhoto.layoutManager = linearLayoutManager
        llOtherObject.visibility = View.GONE

        checkisEdit()

    }

    private fun checkisEdit() {
        if (session.id != "" && session.status == "2") {
            val resultsdataBangunan = realm.where(Room::class.java).equalTo("roomId", session.id.toInt()).findFirst()
            val dataimageBangunan = realm.copyFromRealm(resultsdataBangunan!!.dataImageRoom)

            edNotes.setText(resultsdataBangunan.notes.toString())

            if (dataimageBangunan.isNotEmpty()) {
                for (element in 0 until dataimageBangunan.size) {
                    adapterTakePictureBuilding.addItems(dataimageBangunan[element])
                    adapterTakePictureBuilding.notifyDataSetChanged()
                    //Log.d("img_bangunan", dataimageBangunan[element].imagenameLand.toString())
                }
            } else {
                dataDefaultTakeFotoBuilding!!.imagepathLand = ""
                dataDefaultTakeFotoBuilding!!.imagenameLand = ""
                adapterTakePictureBuilding.addItems(dataDefaultTakeFotoBuilding!!)
                adapterTakePictureBuilding.notifyDataSetChanged()
            }


        } else {
            dataDefaultTakeFotoBuilding!!.imagepathLand = ""
            dataDefaultTakeFotoBuilding!!.imagenameLand = ""
            adapterTakePictureBuilding.addItems(dataDefaultTakeFotoBuilding!!)
            adapterTakePictureBuilding.notifyDataSetChanged()
        }
    }

    private fun loadProfile(url: String) {
        /* Log.d(TAG, "Image cache path: $url")
         Glide.with(activity!!).load(url)
                 .into(ivselect)
         ivselect.setColorFilter(ContextCompat.getColor(activity!!, android.R.color.transparent))*/
    }

    private fun onProfileImageClick() {
        Dexter.withActivity(activity!!)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions()
                        }
                        if (report.isAnyPermissionPermanentlyDenied) {
                            showSettingsDialog()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).check()
    }

    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(activity!!, object : ImagePickerActivity.PickerOptionListener {
            override fun onTakeCameraSelected() {
                launchCameraIntent()
            }

            override fun onChooseGallerySelected() {
                launchGalleryIntent()
            }
        })
    }

    private fun launchCameraIntent() {
        val intent = Intent(activity!!, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE)
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    private fun launchGalleryIntent() {
        val intent = Intent(activity!!, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE)
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data!!.getParcelableExtra<Uri>("path")
                try {
                    // You can update this bitmap to your server
                    val bitmap = MediaStore.Images.Media.getBitmap(activity!!.contentResolver, uri)
                    val dataImage = DataImageRoom()
                    dataImage.imagepathLand = uri?.path
                    dataImage.imagenameLand = ""
                    adapterTakePictureBuilding.updateDateAfterClickImage(mpositionImageClick!!, dataImage)
                    adapterTakePictureBuilding.notifyDataSetChanged()

                    //adapterTakePictureLand.getencodeImage(uri)
                    /*val dataImage = DataImagePhoto()
                    dataImage.imageEncode = getEncoded64ImageStringFromBitmap(bitmap)
                    dataImage.imageName = ""
                    adapterTakePictureLand.addItems(dataImage)
                    adapterTakePictureLand.notifyDataSetChanged()*/

                    // loading profile image from local cache
                    //loadProfile(uri.toString())
                    Log.d(TAG, "Image cache path: $uri")
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun getEncoded64ImageStringFromBitmap(bitmap: Bitmap): String {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val byteFormat = stream.toByteArray()

        return Base64.encodeToString(byteFormat, Base64.DEFAULT)
    }

    override fun OnItemImageClick(position: Int) {
        mpositionImageClick = position
        onProfileImageClick()
        //Toast.makeText(activity!!, position.toString(), Toast.LENGTH_SHORT).show()
    }

    override fun onClearImage(position: Int) {

    }


    fun addphotoItem() {
        val dataImage = DataImageRoom()
        dataImage.imagepathLand = ""
        dataImage.imagenameLand = ""
        adapterTakePictureBuilding.addItems(dataImage)
        adapterTakePictureBuilding.notifyDataSetChanged()
        rvlistPhoto.scrollToPosition(adapterTakePictureBuilding.itemCount - 1)
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(getString(R.string.dialog_permission_title))
        builder.setMessage(getString(R.string.dialog_permission_message))
        builder.setPositiveButton(getString(R.string.go_to_settings)) { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(getString(android.R.string.cancel)) { dialog, which -> dialog.cancel() }
        builder.show()
    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", activity!!.packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }


    override fun onClick(v: View?) {
        if (v!!.id == R.id.btnaddPhoto) {
            hideKeyboard()
            Utils.deletedataformImage(activity!!)
            Utils.deletedataformImage(activity!!)
            val dataPhoto = adapterTakePictureBuilding.returnDataPhoto()
            val mimagePath: MutableList<String> = ArrayList()
            val mimageTitle: MutableList<String> = ArrayList()

            var isvalidPhoto = false
            for (i in dataPhoto.indices) {
                if (dataPhoto[i]?.imagepathLand!!.toString().isNotEmpty() && dataPhoto[i]?.imagenameLand!!.toString().isEmpty() ||
                        dataPhoto[i]?.imagepathLand!!.toString().isEmpty() && dataPhoto[i]?.imagenameLand!!.toString().isNotEmpty()) {
                    val sweetAlretLoading = SweetAlertDialog(activity!!, SweetAlertDialog.WARNING_TYPE)
                    sweetAlretLoading.titleText = "LENGKAPI DATA"
                    sweetAlretLoading.setCancelable(false)
                    sweetAlretLoading.confirmText = "OK"
                    sweetAlretLoading.setConfirmClickListener { sDialog ->
                        sDialog?.let { if (it.isShowing) it.dismiss() }
                    }
                    sweetAlretLoading.show()
                    isvalidPhoto = false
                    break
                } else {
                    isvalidPhoto = true
                    //mimagePath.add(dataPhoto[i].imageEncode.toString())
                    //mimageTitle.add(dataPhoto[i].imageName.toString())
                }
            }

            if (isvalidPhoto) {
                (activity as ActicityFormAddObjekRoom?)?.saveData?.notes = edNotes.text.toString().trim()
                (activity as ActicityFormAddObjekRoom?)?.saveData?.dataImageRoom = dataPhoto
                if (mListener != null) {
                    mListener!!.onNextPressed(this)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        btnaddPhoto?.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        btnaddPhoto?.setOnClickListener(null)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnStepTwoListener) {
            mListener = context
        } else {
            throw RuntimeException(("$context must implement OnFragmentInteractionListener"))
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    interface OnStepTwoListener {
        fun onBackPressed(fragment: androidx.fragment.app.Fragment)
        fun onNextPressed(fragment: androidx.fragment.app.Fragment)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        private val REQUEST_IMAGE = 100
        internal const val TAG = "FormTanahPhoto"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): FormObjekRoomPhoto {
            val fragment = FormObjekRoomPhoto()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}