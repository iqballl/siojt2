package mki.siojt2.fragment.form_object_room.view

import mki.siojt2.base.view.MvpView
import mki.siojt2.model.ResponseDataListPemilik
import mki.siojt2.model.master_data_bangunan.ResponseDataListTipeBangunan

interface FormObjekRoom1View : MvpView {
    fun onsuccessgetlistdataPemilik(responseDataListPemilik: MutableList<ResponseDataListPemilik>)
}