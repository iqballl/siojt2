package mki.siojt2.fragment.form_object_room

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.DatePicker
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.gson.Gson
import com.whiteelephant.monthpicker.MonthPickerDialog
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_form_objek_add_room_1.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.fragment.form_object_room.presenter.FormObjek1RoomPresenter
import mki.siojt2.fragment.form_object_room.view.FormObjekRoom1View
import mki.siojt2.fragment.form_subjek.FormSubjek2
import mki.siojt2.model.ResponseDataListPemilik
import mki.siojt2.model.localsave.PartyAdd
import mki.siojt2.model.localsave.Room
import mki.siojt2.model.localsave.Session
import mki.siojt2.model.localsave.TempPemilikObjek
import mki.siojt2.ui.activity_form_add_objek_room.view.ActicityFormAddObjekRoom
import mki.siojt2.utils.Utils
import mki.siojt2.utils.realm.RealmController
import java.util.*

class FormObjekRoom1 : BaseFragment(), FormObjekRoom1View, View.OnClickListener {

    // TODO: Rename and change types of parameters
    private var mParam1: Int? = null
    private var mParam2: Int? = null
    private var mListener: OnStepOneListener? = null

    lateinit var formObjek1RoomPresenter: FormObjek1RoomPresenter

    private var dataPemilikObjek: MutableList<TempPemilikObjek>? = ArrayList()
    var documents: List<TempPemilikObjek> = ArrayList()

    private var partyTypeId: String? = ""

    val valuejenisBangunan = mutableListOf("Rumah Tinggal", "Ruko", "Rukan", "Gudang/Pabrik", "Hotel",
            "Perkebunan", "Lain-Nya")

    lateinit var realm: Realm
    lateinit var session: Session

    var tanggalEdit: String? = null
    private var datepicker: DatePicker? = null
    private var datepicker2: DatePicker? = null

    private var ownerData: TempPemilikObjek? = null

//    lateinit var mCalendar: Calendar
//    private var mYear: Int = 0
//    private var mMonth: Int = 0
//    private var mDay: Int = 0
//    private val dateTemplate = "dd MMMM yyyy"
//    private val dateTemplate2 = "yyyy-MM-dd"
//    private var senddateBirthPicker: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getInt(ARG_PARAM1)
            mParam2 = arguments!!.getInt(ARG_PARAM2)
        }

        realm = RealmController.with(this).realm
        realm.isAutoRefresh
        session = Session(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_objek_add_room_1, container, false)
    }

    override fun setUp(view: View) {
        edTemporaryFieldNumber.setText(mParam1.toString())

        /* data offline pemilik */
        val realmResults = realm.where(TempPemilikObjek::class.java).findAll()
        val documents: MutableList<TempPemilikObjek> = realm.copyFromRealm(realmResults)

        setupForEdit()

        val adapterPemilikBangunan = ArrayAdapter(activity!!, R.layout.item_spinner, documents)
        spinnerpemilikBangunan.setAdapter(adapterPemilikBangunan)

        spinnerpemilikBangunan.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            ownerData = adapterView.getItemAtPosition(position) as TempPemilikObjek
            val selectedProject = adapterView.getItemAtPosition(position) as TempPemilikObjek
            partyTypeId = selectedProject.pemilikId
            //Toast.makeText(activity!!, partyTypeId.toString(), Toast.LENGTH_SHORT).show()
        }

        tv_pick_thn_dibangun_bangunan.setSafeOnClickListener {
            selectedDate()
        }
    }

    private fun showDialogYearOnly() {
        val today = Calendar.getInstance()
        val builder = MonthPickerDialog.Builder(activity!!, MonthPickerDialog.OnDateSetListener { selectedMonth, selectedYear ->
            tv_thn_dibangun_bangunan.text = selectedYear.toString()
        }, today.get(Calendar.YEAR), today.get(Calendar.MONTH))

        builder.setActivatedYear(today.get(Calendar.YEAR))
                .setTitle("Tahun bangunan")
                .setMinYear(today.get(Calendar.YEAR) - 100)
                .setMaxYear(today.get(Calendar.YEAR))
                .setOnMonthChangedListener { selectedMonth ->
                    Log.d(FormSubjek2.TAG, "Selected month : $selectedMonth")
                    // Toast.makeText(MainActivity.this, " Selected month : " + selectedMonth, Toast.LENGTH_SHORT).show();
                }
                .setOnYearChangedListener { selectedYear ->
                    Log.d(FormSubjek2.TAG, "Selected year : $selectedYear")
                    tanggalEdit = selectedYear.toString()
                    tv_thn_dibangun_bangunan.text = selectedYear.toString()
                    // Toast.makeText(MainActivity.this, " Selected year : " + selectedYear, Toast.LENGTH_SHORT).show();
                }
                .showYearOnly()
                .build()
                .show()
    }

    private fun selectedDate() {
        showDialogYearOnly()
//        if ((session.id != "" && session.status == "2") && tanggalEdit.isNotEmpty()) {
//            val datePart = tanggalEdit.split("-")
//            val year = datePart[0]
//            val month = datePart[1]
//            val day = datePart[2]
//
//            val mmDay = if (month.substring(0, 1).contains("0")) {
//                val parts = day.substring(1)
//                parts.toInt()
//            } else {
//                day.toInt()
//            }
//
//            val mmMonth = if (month.substring(0, 1).contains("0")) {
//                val parts = month.substring(1)
//                parts.toInt() - 1
//            } else {
//                month.toInt() - 1
//            }
//
//            val mmYear = year.toInt()
//
//            val datePickerDialog = DatePickerDialog(activity!!, AlertDialog.THEME_HOLO_DARK, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
//                // bulan dari hasil date picker kurang satu bulan dari bulan yg dipilih sehingga ditambah + 1
//                datepicker = DatePicker(activity)
//                datepicker!!.init(year, monthOfYear + 1, dayOfMonth, null)
//                val calendar2 = GregorianCalendar(year, monthOfYear, dayOfMonth)
//
//                tv_thn_dibangun_bangunan.text = DateFormat.format(dateTemplate, calendar2.time)
//                senddateBirthPicker = DateFormat.format(dateTemplate2, calendar2.time).toString()
//            }, mmYear, mmMonth, mmDay)
//
//            if (datepicker != null) {
//                //Log.d("hasil2", "${datepicker!!.month - 1}")
//                datePickerDialog.updateDate(datepicker!!.year, datepicker!!.month - 1, datepicker!!.dayOfMonth)
//            }
//
//            datePickerDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//            datePickerDialog.datePicker.maxDate = mCalendar.timeInMillis - 1000
//            datePickerDialog.show()
//
//        } else {
//            val datePickerDialog = DatePickerDialog(activity!!, AlertDialog.THEME_HOLO_DARK, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
//                // bulan dari hasil date picker kurang satu bulan dari bulan yg dipilih sehingga ditambah + 1
//                datepicker2 = DatePicker(activity)
//                datepicker2!!.init(year, monthOfYear + 1, dayOfMonth, null)
//
//                val calendar2 = GregorianCalendar(year, monthOfYear, dayOfMonth)
//
//                tv_thn_dibangun_bangunan.text = DateFormat.format(dateTemplate, calendar2.time)
//                senddateBirthPicker = DateFormat.format(dateTemplate2, calendar2.time).toString()
//                //Toast.makeText(activity, senddateBirthPicker, Toast.LENGTH_SHORT).show()
//
//                //txtDate.setText(dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
//            }, mYear, mMonth, mDay)
//
//            if (datepicker2 != null) {
//                //Log.d("bab4", "${datepicker!!.month - 1}")
//                datePickerDialog.updateDate(datepicker2!!.year, datepicker2!!.month - 1, datepicker2!!.dayOfMonth)
//            }
//
//            datePickerDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//            datePickerDialog.datePicker.maxDate = mCalendar.timeInMillis - 1000
//            datePickerDialog.show()
//        }
    }

    private fun setupForEdit() {
        if (session.id != "" && session.status == "2") {
            val results = realm.where(Room::class.java).equalTo("roomId", session.id.toInt()).findFirst()
            if (results != null) {
                tampilkanData(results)
                //Log.d("results", results.toString())
            } else {
                Log.e("data", "kosong")
            }
        } else if (session.id != "" && session.status == "3") {
            val results = realm.where(Room::class.java).equalTo("roomId", session.id.toInt()).findFirst()
            if (results != null) {
                tampilkanData(results)
                //Log.d("results", results.toString())
            } else {
                Log.e("data", "kosong")
            }
        }

    }

    private fun tampilkanData(results: Room) {
        spinnerpemilikBangunan.setText(results.ownerDataName)
        tanggalEdit = results.year
        tv_thn_dibangun_bangunan.text = results.year.toString()
        edluasLantai1.setText(results.upperRoom.toString())
        edluasLantai2.setText(results.lowerRoom.toString())
    }

    private fun getPresenter(): FormObjek1RoomPresenter? {
        formObjek1RoomPresenter = FormObjek1RoomPresenter()
        formObjek1RoomPresenter.onAttach(this)
        return formObjek1RoomPresenter
    }

    private fun validate(): Boolean {
        val sweetAlretLoading = SweetAlertDialog(activity!!, SweetAlertDialog.WARNING_TYPE)
        sweetAlretLoading.setCancelable(false)
        sweetAlretLoading.confirmText = "OK"
        sweetAlretLoading.setConfirmClickListener { sDialog ->
            sDialog?.let { if (it.isShowing) it.dismiss() }
        }

        val valid: Boolean
        if (partyTypeId?.equals(0)!!) {
            sweetAlretLoading.titleText = "LENGKAPI DATA"
            sweetAlretLoading.show()

            valid = false

        }
//        else if (edluasLantai1.text.isNullOrEmpty() || edluasLantai2.text.isNullOrEmpty()) {
//            sweetAlretLoading.titleText = "LENGKAPI DATA"
//            sweetAlretLoading.show()
//            valid = false
//
//        }
        else {
            valid = true
        }

        return valid
    }

    override fun onsuccessgetlistdataPemilik(responseDataListPemilik: MutableList<ResponseDataListPemilik>) {
        //getPresenter()?.getdaftarJenisBangunan()
        val adapterpemilikBangunan = ArrayAdapter(activity!!,
                R.layout.item_spinner, responseDataListPemilik)
        adapterpemilikBangunan.setNotifyOnChange(true)
        spinnerpemilikBangunan.setAdapter(adapterpemilikBangunan)
        adapterpemilikBangunan.setNotifyOnChange(true)
    }

    override fun onResume() {
        super.onResume()
        btnnextaddBanugnan1?.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        btnnextaddBanugnan1?.setOnClickListener(null)
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btnnextaddBanugnan1) {
            if (validate()) {
                hideKeyboard()
                (activity as ActicityFormAddObjekRoom?)?.saveData?.ownerDataId = ownerData?.pemilikId
                (activity as ActicityFormAddObjekRoom?)?.saveData?.ownerDataName = ownerData?.pemilikName
                (activity as ActicityFormAddObjekRoom?)?.saveData?.year = tanggalEdit
                (activity as ActicityFormAddObjekRoom?)?.saveData?.upperRoom = if(edluasLantai1.text.isNullOrEmpty()){0}else{edluasLantai1.text.toString().toInt()}
                (activity as ActicityFormAddObjekRoom?)?.saveData?.lowerRoom = if(edluasLantai2.text.isNullOrEmpty()){0}else{edluasLantai2.text.toString().toInt()}

                if (mListener != null) {
                    mListener!!.onNextPressed(this)
                }

            }

        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnStepOneListener) {
            mListener = context
        } else {
            throw RuntimeException(("$context must implement OnFragmentInteractionListener"))
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    interface OnStepOneListener {
        //void onFragmentInteraction(Uri uri);
        fun onNextPressed(fragment: androidx.fragment.app.Fragment)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FormSubjek1.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: Int, param2: Int): FormObjekRoom1 {
            val fragment = FormObjekRoom1()
            val args = Bundle()
            args.putInt(ARG_PARAM1, param1)
            args.putInt(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor