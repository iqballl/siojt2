package mki.siojt2.fragment.form_object_room.presenter

import io.reactivex.disposables.CompositeDisposable
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.constant.Constant
import mki.siojt2.fragment.form_objek_bangunan.view.FormObjekBangunan1View
import mki.siojt2.utils.ErrorHandler
import mki.siojt2.utils.rxJava.RxUtils

class FormObjek1RoomPresenter : BasePresenter(), FormObjek1RoomMVPPresenter {

    private var disposables: CompositeDisposable = CompositeDisposable()

    override fun getlistdataPemilik(projectId: Int, landId: String) {
        view().onShowLoading()
        disposables.add(
                dataManager.getAllParties(projectId, landId)
                        .compose(RxUtils.applyScheduler())
                        .subscribe({ responseData ->
                            view().onDismissLoading()
                            if (responseData.status == Constant.STATUS_ERROR) {
                                view().onFailed(responseData.message!!)
                            } else {
                                if (responseData.status == Constant.STATUS_SUCCESS) {
                                    view().onsuccessgetlistdataPemilik(responseData.result!!)
                                } else {
                                    view().onFailed(responseData.message!!)
                                }
                            }
                        }) { throwable ->
                            view().onDismissLoading()
                            ErrorHandler.handlerErrorPresenter(view(), throwable)
                        }
        )
    }

    private fun view(): FormObjekBangunan1View {
        return getView() as FormObjekBangunan1View
    }
}