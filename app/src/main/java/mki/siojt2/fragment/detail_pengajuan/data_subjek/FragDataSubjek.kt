package mki.siojt2.fragment.detail_pengajuan.data_subjek

import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.fragment_datasubjek.*
import mki.siojt2.R
import mki.siojt2.base.BaseFragment
import mki.siojt2.fragment.detail_pengajuan.data_subjek.recycleview.AdapterDaftarSubjek
import mki.siojt2.model.ResponDataSubjekDPPT
import mki.siojt2.ui.activity_form_subjek.view.ActivityFormSubjek
import mki.siojt2.utils.extension.OnSingleClickListener

class FragDataSubjek : BaseFragment(), AdapterDaftarSubjek.Callback {

    private lateinit var adapterlistSubjek: AdapterDaftarSubjek

    private var datalistSubjek: MutableList<ResponDataSubjekDPPT>? = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_datasubjek, container, false)

    }

    override fun setUp(view: View) {

        datalistSubjek?.add(ResponDataSubjekDPPT("1", "Komp. Sukabirus IV BLOK C2 No.54",
                "Sdr. Yayan Sudrajat", "Kel. Sindangparit Kec. Mandalajati Kab. Bandung"))
        datalistSubjek?.add(ResponDataSubjekDPPT("2", "Komp. Mawar BLOK A",
                "Sdr. Tatang Sudrajat", "Kel. Majalaya Bandung"))


        adapterlistSubjek = AdapterDaftarSubjek(datalistSubjek, activity!!)
        adapterlistSubjek.setCallback(this)
        //adapterJenisInventaris!!.clear()
        rvdaftarSubjek.let {
            it.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
            it.setHasFixedSize(true)
            it.itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()
            it.adapter = adapterlistSubjek
        }

        /*btnaddSubjekDPPT.setOnClickListener {
             intent = ActivityFormSubjek.getStartIntent(this.activity!!)
            startActivity(intent)
        }*/

        /*btnaddSubjekDPPT.setDoubleClickListener(View.OnClickListener { v ->
            // do stuff
             intent = ActivityFormSubjek.getStartIntent(this.activity!!)
            startActivity(intent)
        })*/


        btnaddSubjekDPPT.setOnClickListener(object : OnSingleClickListener() {
            override fun onSingleClick(v: View) {
                val intent = ActivityFormSubjek.getStartIntent(activity!!)
                startActivity(intent)
            }
        })
    }

    override fun onRepoEmptyViewRetryClick() {

    }

    /*fun View.setDoubleClickListener(listener: View.OnClickListener, waitMillis: Long = 1000) {
        var lastClickTime = 0L
        setOnClickListener { view ->
            if (System.currentTimeMillis() > lastClickTime + waitMillis) {
                listener.onClick(view)
                lastClickTime = System.currentTimeMillis()
            }
        }
    }*/

    companion object {
        internal const val TAG = "FragDataSubjek"

        fun newInstance(): FragDataSubjek {
            val args = Bundle()
            val fragment = FragDataSubjek()
            fragment.arguments = args
            return fragment
        }
    }
}