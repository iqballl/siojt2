package mki.siojt2.fragment.detail_pengajuan.data_subjek.recycleview

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_daftar_subjek_vertical.view.*
import mki.siojt2.R
import mki.siojt2.base.BaseViewHolder
import mki.siojt2.model.ResponDataSubjekDPPT
import mki.siojt2.ui.activity_detail_subjek.view.ActivityDetailSubjek
import mki.siojt2.utils.extension.OnSingleClickListener


class AdapterDaftarSubjek(mDatasetxes: MutableList<ResponDataSubjekDPPT>?, context: Context) : androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder>() {

    private val mContext: Context? = context
    val mDataset: MutableList<ResponDataSubjekDPPT>? = mDatasetxes
    var rowLayout = R.layout.item_daftar_subjek_vertical
    var emptyrowLayout = R.layout.item_empty_view
    var pos = 0


    private var mCallback: Callback? = null

    fun setCallback(callback: Callback) {
        mCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            VIEW_TYPE_NORMAL -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(rowLayout, parent, false))
            VIEW_TYPE_EMPTY -> EmptyViewHolder(
                    LayoutInflater.from(parent.context).inflate(R.layout.item_empty_view, parent, false))
            else -> EmptyViewHolder(LayoutInflater.from(parent.context).inflate(emptyrowLayout, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (mDataset != null && mDataset.isNotEmpty()) {
            VIEW_TYPE_NORMAL
        } else {
            VIEW_TYPE_EMPTY
        }
    }

    override fun getItemCount(): Int {
        return if (mDataset!!.isNotEmpty()) {
            mDataset.size
        } else {
            1
        }
    }

    /*inner class RuangViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var r_no: TextView = itemView.findViewById(R.id.rowNo) as TextView
        var r_tvTitle: TextView = itemView.findViewById(R.id.tvTitle) as TextView
        var r_tvContent1: TextView = itemView.findViewById(R.id.tvContent1) as TextView
        var r_tvContent2: TextView = itemView.findViewById(R.id.tvContent2) as TextView

    }*/

    fun clear() {
        this.mDataset!!.clear()
    }

    fun addItems(data: List<ResponDataSubjekDPPT>) {
        this.mDataset!!.addAll(data)
        notifyDataSetChanged()
    }

    interface Callback {
        fun onRepoEmptyViewRetryClick()
    }

    inner class ViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }

        override fun onBind(position: Int) {
            super.onBind(position)

            val dataList = mDataset!![position]

            inflateData(dataList)

            val pos = pos + position + 1
            itemView.tvnamaSubjek.text = dataList.namaJenis
            itemView.tvalamat1Subjek.text = dataList.kodeJenis
            itemView.tvalamat2Subjek.text = dataList.keterangan


//            !!important
//            itemView.btnDaftarObjek.setOnClickListener(object : OnSingleClickListener() {
//                override fun onSingleClick(v: View) {
//                    val intent = ActivityDaftarTanah.getStartIntent(mContext!!)
//                    mContext.startActivity(intent)
//                }
//            })


            itemView.btnSelngkapnya.setOnClickListener(object : OnSingleClickListener(){
                override fun onSingleClick(v: View) {
                    val intent = ActivityDetailSubjek.getStartIntent(mContext!!)
                    mContext.startActivity(intent)
                }
            })


        }

        private fun inflateData(dataList: ResponDataSubjekDPPT) {
            //nama?.let { itemView.tvMatapelajaran.text = it }
//            itemView.tvContent1.text = dataList.namaJenis
//            itemView.tvContent2.text= dataList.keterangan
        }
    }

    inner class EmptyViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun clear() {

        }


    }

    companion object {
        const val VIEW_TYPE_EMPTY = 0
        const val VIEW_TYPE_NORMAL = 1
    }
}
