package mki.siojt2.fragment.detail_pengajuan.summary

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mki.siojt2.R
import mki.siojt2.base.BaseFragment

class FragSummary : BaseFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_summary, container, false)
    }

    override fun setUp(view: View) {

    }

    companion object {
        internal const val TAG = "FragSummary"

        fun newInstance(): FragSummary {
            val args = Bundle()
            val fragment = FragSummary()
            fragment.arguments = args
            return fragment
        }
    }
}