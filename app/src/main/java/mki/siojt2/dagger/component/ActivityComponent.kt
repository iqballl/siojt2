package mki.siojt2.dagger.component

import dagger.Component
import mki.siojt2.dagger.ActivityContext
import mki.siojt2.dagger.modules.ActivityModule
import mki.siojt2.dagger.modules.FragmentModule
import mki.siojt2.dagger.modules.PresenterModule

/**
 * Created by bayunvnt on 09/10/17.
 */
@ActivityContext
@Component(modules = [FragmentModule::class, PresenterModule::class, ActivityModule::class], dependencies = [AppComponent::class])
interface ActivityComponent
