package mki.siojt2.dagger.component

import android.os.Handler
import com.google.gson.Gson
import dagger.Component
import mki.siojt2.SIOJT2
import mki.siojt2.base.presenter.BasePresenter
import mki.siojt2.dagger.modules.AppModule
import mki.siojt2.dagger.modules.NetworkModule
import mki.siojt2.network.DataManager
import javax.inject.Singleton

/**
 * Created by iqbal on 09/10/17.
 */
@Singleton
@Component(modules = [AppModule::class, NetworkModule::class])
interface AppComponent {

    fun inject(siojt2: SIOJT2)
    fun inject(basePresenter: BasePresenter)

    fun gson(): Gson
    fun datamanager(): DataManager
    fun mainHandler(): Handler

    /*object Initializer {

        fun init(context: Context): AppComponent {
            return DaggerAppComponent.builder()
                    .networkModule(NetworkModule(context))
                    .appModule(AppModule(context))
                    .build()
        }
    }*/
}
