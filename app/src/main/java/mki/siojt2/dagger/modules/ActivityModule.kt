package mki.siojt2.dagger.modules

import androidx.fragment.app.FragmentManager
import dagger.Module
import dagger.Provides
import mki.siojt2.base.view.BaseActivity
import mki.siojt2.dagger.ActivityContext

/**
 * Created by bayunvnt on 09/10/17.
 */
@Module
class ActivityModule(private val baseActivity: BaseActivity) {

    @Provides
    @ActivityContext
    internal fun provideActivity(): BaseActivity {
        return baseActivity
    }

    @Provides
    @ActivityContext
    internal fun provideFragmentManager(): FragmentManager {
        return baseActivity.baseFragmentManager
    }
}
