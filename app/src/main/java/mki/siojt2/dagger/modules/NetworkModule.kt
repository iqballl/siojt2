package mki.siojt2.dagger.modules

import android.content.Context
import android.util.Log

import com.google.gson.Gson
import com.google.gson.GsonBuilder

import java.lang.reflect.Modifier
import java.util.concurrent.TimeUnit

import dagger.Module
import dagger.Provides
import mki.siojt2.BuildConfig
import mki.siojt2.constant.Constant
import mki.siojt2.network.ApiService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.security.cert.CertificateException
import javax.inject.Singleton
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import android.net.NetworkInfo
import androidx.core.content.ContextCompat.getSystemService
import android.net.ConnectivityManager
import mki.siojt2.SIOJT2
import mki.siojt2.utils.network.sample.NetworkConnectionInterceptor
import okhttp3.Cache
import okhttp3.Interceptor
import java.io.File


/**
 * Created by iqbal on 26/05/19.
 */
@Module
open class NetworkModule(private val application: SIOJT2) {

    @Provides
    @Singleton
    fun provideContext(): Context = application.applicationContext

    @Provides
    @Singleton
    fun provideCache(): Cache {
        val cacheSize: Long = 10 * 1024 * 1024
        val cacheDir = File(application.cacheDir, "cache")
        val cache = Cache(cacheDir, cacheSize)
        return cache
    }

    @Provides
    internal fun provideGson(): Gson {
        return Gson()
    }

    fun isInternetAvailable(): Boolean {
        val connectivityManager = application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        val activeNetworkInfo = connectivityManager!!.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    @Provides
    @Singleton
    fun provideHttpLogging(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor { message -> Log.d("LoggingInterceptor", message) }
                .setLevel(if (BuildConfig.DEBUG)
                    HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE)
    }

    private fun getUnsafeOkHttpClient(): OkHttpClient {

        val interceptor = HttpLoggingInterceptor { message -> Log.d("LogIntercep", message) }
        interceptor.level = if (BuildConfig.DEBUG)
            HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        try {
            // Create a trust manager that does not validate certificate chains
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                @Throws(CertificateException::class)
                override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                }

                override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                    return arrayOf()
                }
            })

            // Install the all-trusting trust manager
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())
            // Create an ssl socket factory with our all-trusting manager
            val sslSocketFactory = sslContext.socketFactory

            val builder = OkHttpClient.Builder()
            builder.connectTimeout(1, TimeUnit.MINUTES)
            builder.readTimeout(30, TimeUnit.SECONDS)
            builder.writeTimeout(15, TimeUnit.SECONDS)
            builder.addInterceptor(interceptor)
            builder.sslSocketFactory(sslSocketFactory)
            builder.hostnameVerifier { hostname, session -> true }

            return builder.build()
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }

    private val REWRITE_RESPONSE_INTERCEPTOR = Interceptor { chain ->
        val originalResponse = chain.proceed(chain.request())
        val cacheControl = originalResponse.header("Cache-Control")
        if (cacheControl == null || cacheControl.contains("no-store") || cacheControl.contains("no-cache") ||
                cacheControl.contains("must-revalidate") || cacheControl.contains("max-age=0")) {
            originalResponse.newBuilder()
                    .header("Cache-Control", "public, max-age=" + 5000)
                    .build()
        } else {
            originalResponse
        }
    }


    @Provides
    @Singleton
    internal fun provideOkHttpClient(cache: Cache, httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        val interceptor = HttpLoggingInterceptor { message -> Log.d("LogIntercep", message) }
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .followSslRedirects(true)
                .addInterceptor(object : NetworkConnectionInterceptor() {
                    override val isInternetAvailable: Boolean
                        get() = isInternetAvailable()

                    override fun onInternetUnavailable() {
                        if (SIOJT2.mInternetConnectionListener != null) {
                            SIOJT2.mInternetConnectionListener!!.onInternetUnavailable()
                        }

                    }

                })
                .addNetworkInterceptor { chain ->
                    val request = chain.request()

                    val newRequest = request.newBuilder()
                            //                            .addHeader("Accept", "application/json")
                            //                            .addHeader("Authorization","Basic NTc2YWNjNDkzNTU0MTA5MDc5ODI2NWFkOk94MXJaeWxRaFd2OGp1UmFmS0JnaUc5YldSbGw4TUo5")
                            //                            .addHeader("Cache-Control", "no-cache")
                            //                            .addHeader("Cache-Control", "no-store")
                            .build()
                    chain.proceed(newRequest)
                }
                .cache(cache)
                .build()
    }

    @Provides
    @Singleton
    internal fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(Constant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
                        .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                        .serializeNulls()
                        .create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    internal fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }
}
