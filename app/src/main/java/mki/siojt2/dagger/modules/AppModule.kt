package mki.siojt2.dagger.modules

import android.content.Context
import android.os.Handler
import android.os.Looper

import dagger.Module
import dagger.Provides
import mki.siojt2.constant.Constant
import mki.siojt2.dagger.PreferenceInfo
import mki.siojt2.dagger.preferences.AppPreferenceHelper
import mki.siojt2.dagger.preferences.PreferenceHelper
import javax.inject.Singleton

/**
 * Created by bayunvnt on 09/10/17.
 */
@Module
class AppModule {

    @Provides
    internal fun provideHandler(): Handler {
        return Handler(Looper.getMainLooper())
    }

    @Provides
    @PreferenceInfo
    internal fun provideprefFileName(): String = Constant.PREF_NAME

    @Provides
    @Singleton
    internal fun providePrefHelper(appPreferenceHelper: AppPreferenceHelper): PreferenceHelper = appPreferenceHelper
}
