package mki.siojt2.dagger.preferences

interface PreferenceHelper {
    fun getAccessToken(): String?

    fun setAccessToken(accessToken: String?)

}