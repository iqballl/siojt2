package mki.siojt2.dagger

import javax.inject.Qualifier

@Qualifier
@Retention annotation class PreferenceInfo