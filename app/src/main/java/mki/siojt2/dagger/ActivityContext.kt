package mki.siojt2.dagger

import javax.inject.Scope

/**
 * Created by bayunvnt on 09/10/17.
 */
@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class ActivityContext
