package mki.siojt2.listener

/**
 * Created by bayunvnt on 09/10/17.
 */

interface OnLoadMoreListener {

    fun onLoadMore()
}
