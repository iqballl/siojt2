package mki.siojt2.utils.network

import android.content.Context
import android.net.ConnectivityManager

object NetworkUtil {

    private var TYPE_WIFI = 1
    private var TYPE_MOBILE = 2
    private var TYPE_NOT_CONNECTED = 0
    private fun getConnectivityStatus(context: Context):Int {
        val cm = context
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        if (null != activeNetwork)
        {
            if (activeNetwork.type == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI
            if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE
        }
        return TYPE_NOT_CONNECTED
    }
    fun getConnectivityStatusString(context:Context): String? {
        val conn = NetworkUtil.getConnectivityStatus(context)
        var status: String? = null
        when (conn) {
            NetworkUtil.TYPE_WIFI -> status = "Connected to Internet"
            NetworkUtil.TYPE_MOBILE -> status = "Connected to Internet"
            NetworkUtil.TYPE_NOT_CONNECTED -> status = "Not connected to Internet"
        }
        return status
    }
}