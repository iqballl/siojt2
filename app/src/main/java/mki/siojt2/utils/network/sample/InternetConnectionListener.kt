package mki.siojt2.utils.network.sample

interface InternetConnectionListener {

    fun onInternetUnavailable()

}