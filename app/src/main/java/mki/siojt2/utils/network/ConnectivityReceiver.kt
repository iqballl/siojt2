package mki.siojt2.utils.network

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.util.Log
import mki.siojt2.SIOJT2
import java.net.HttpURLConnection
import java.net.URL


class ConnectivityReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        // versi 1
        /*val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        val isConnected = (activeNetwork != null && activeNetwork.isConnected)
        if (connectivityReceiverListener != null) {
            connectivityReceiverListener!!.onNetworkConnectionChanged(isConnected)
        }*/

        val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        if (activeNetwork != null) {
            if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                CheckConnectionTask().execute()
                Log.d("tWifi", statusIsConnected.toString())
            } else if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                CheckConnectionTask().execute()
                Log.d("tMobile", statusIsConnected.toString())
            }
        } else {
            /*CheckConnectionTask().execute()
            Log.d("tNull", statusIsConnected.toString())*/
            Log.d("tNull", statusIsConnected.toString())
            if (connectivityReceiverListener != null) {
                connectivityReceiverListener!!.onNetworkConnectionChanged(false)
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    inner class CheckConnectionTask : AsyncTask<Void, Void, Boolean>() {
        override fun doInBackground(vararg params: Void): Boolean {
            return try {
                val urlc = (URL("http://clients3.google.com/generate_204")
                        .openConnection()) as HttpURLConnection
                urlc.setRequestProperty("User-Agent", "Android")
                urlc.setRequestProperty("Connection", "close")
                urlc.connectTimeout = 1500
                urlc.connect()
                urlc.responseCode == 204 && urlc.contentLength == 0
            } catch (e: Exception) {
                Log.e("exception", e.toString())
                false
            }
        }

        override fun onPostExecute(result: Boolean) {
            super.onPostExecute(result)
            if (connectivityReceiverListener != null) {
                connectivityReceiverListener!!.onNetworkConnectionChanged(result)
            }
            Log.d("execute", result.toString())
        }
    }

    interface ConnectivityReceiverListener {
        fun onNetworkConnectionChanged(isConnected: Boolean)
    }

    companion object {
        internal var connectivityReceiverListener: ConnectivityReceiverListener? = null
        private var statusIsConnected: Boolean = false
        val isConnected: Boolean
            get() {
                val cm = SIOJT2.instance?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetwork = cm.activeNetworkInfo
                return (activeNetwork != null && activeNetwork.isConnected)
            }
    }

}