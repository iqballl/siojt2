package mki.siojt2.utils.network

import android.content.Context
import mki.siojt2.utils.Utils
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class ConnectivityInterceptor(context: Context): Interceptor {
    private val mContext:Context = context
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!Utils.isOnline(mContext))
        {
            throw NoConnectivityException()
        }
        val builder = chain.request().newBuilder()
        return chain.proceed(builder.build())
    }
}