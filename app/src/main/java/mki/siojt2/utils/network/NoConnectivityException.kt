package mki.siojt2.utils.network

import java.io.IOException

class NoConnectivityException: IOException() {
    override val message:String
        get() {
            return "No connectivity exception"
        }
}