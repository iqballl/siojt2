package mki.siojt2.utils.network.sample

import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

abstract class NetworkConnectionInterceptor : Interceptor {
    //For type boolean, the default value is false.
    abstract val isInternetAvailable: Boolean // is false

    abstract fun onInternetUnavailable()
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        /* if no internet connection, run the conditions below */
        if (!isInternetAvailable) // output (!isInternetAvailable) == true
        {
            /*onInternetUnavailable()
            request = request.newBuilder().header("Cache-Control",
                    "public, only-if-cached, max-stale=" + 60 * 60 * 24).build()
            val response = chain.proceed(request)
            if (response.cacheResponse() == null)
            {
                onCacheUnavailable()
            }
            println("statusNetwork: $isInternetAvailable")
            println("network: " + response.networkResponse())
            println("cache: " + response.cacheResponse())
            return response*/
            onInternetUnavailable()
        }

        return chain.proceed(request)
    }
}