package mki.siojt2.utils.network


import mki.siojt2.base.response.BaseResponse

/**
 * Created by bayunvnt on 09/10/17.
 */

class BaseAppException(val response: BaseResponse, val responseCode: Int) : Exception()
