package mki.siojt2.utils

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.preference.PreferenceManager
import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import mki.siojt2.model.DataExpiredDialogSync
import mki.siojt2.persistence.Preference
import java.util.*


/**
 * Created by iqbal on 09/10/17.
 */

object Utils {

    // All Shared Preferences Keys
    private const val IS_LOGIN = "IsLoggedIn"
    private const val dataform1 = "dataform1"
    private const val dataform2 = "dataform2"
    private const val dataform3 = "dataform3"

    private const val dataTanah1 = "dataTanah1"
    private const val dataTanah2 = "dataTanah2"
    private const val dataTanah3 = "dataTanah3"
    private const val dataTanah4 = "dataTanah4"

    private const val dataBangunan1 = "dataBangunan1"
    private const val dataBangunan2 = "dataBangunan2"

    fun getisshowDialogSync(context: Context): Boolean {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getBoolean("IsShowDialogSync", false)
    }

    fun setisshowDialogSync(log: Boolean, context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putBoolean("IsShowDialogSync", log)
        editor.apply()
    }

    fun clearisshowDialogSync(context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.remove("IsShowDialogSync")
        editor.apply()
    }

    fun getdataform1(context: Context): String? {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString(dataform1, "")
    }

    fun savedataform1(log: String, context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putString(dataform1, log)
        editor.apply()
    }

    fun deletedataform1(context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.remove(dataform1)
        editor.apply()
    }

    fun getdataform2(context: Context): String? {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString(dataform2, "")
    }

    fun savedataform2(log: String, context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putString(dataform2, log)
        editor.apply()
    }

    fun deletedataform2(context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.remove(dataform2)
        editor.apply()
    }

    fun getdataform3(context: Context): String? {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString(dataform3, "")
    }

    fun savedataform3(log: String, context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putString(dataform3, log)
        editor.apply()
    }

    fun deletedataform3(context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.remove(dataform3)
        editor.apply()
    }

    /**
     * data form tanah
     */

    fun getdataformtanah1(context: Context): String? {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString(dataTanah1, "")
    }

    fun savedataformtanah1(log: String, context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putString(dataTanah1, log)
        editor.apply()
    }

    fun deletedataformtanah1(context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.remove(dataTanah1)
        editor.apply()
    }

    fun getdataformtanah2(context: Context): String? {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString(dataTanah2, "")
    }

    fun savedataformtanah2(log: String, context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putString(dataTanah2, log)
        editor.apply()
    }

    fun deletedataformtanah2(context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.remove(dataTanah2)
        editor.apply()
    }

    fun getdataformtanah3(context: Context): String? {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString(dataTanah3, "")
    }

    fun savedataformtanah3(log: String, context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putString(dataTanah3, log)
        editor.apply()
    }

    fun deletedataformtanah3(context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.remove(dataTanah3)
        editor.apply()
    }

    fun getdataformtanah4(context: Context): String? {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString(dataTanah4, "")
    }

    fun savedataformtanah4(log: String, context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putString(dataTanah4, log)
        editor.apply()
    }

    fun deletedataformtanah4(context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.remove(dataTanah4)
        editor.apply()
    }

    fun getdataformTanahImage(context: Context): String? {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString("image_land", "")
    }

    fun savedataformtanahImage(log: String, context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putString("image_land", log)
        editor.apply()
    }

    fun deletedataformImage(context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.remove("image_land")
        editor.apply()
    }

    fun getdataImageCanvas(context: Context): String? {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString("imageCanvas", "")
    }

    fun savedataImageCanvas(log: String, context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putString("imageCanvas", log)
        editor.apply()
    }

    fun deletedataImageCanvas(context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.remove("imageCanvas")
        editor.apply()
    }

    /**
     * Daata objek
     */

    fun getdatabangunan1(context: Context): String? {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString(dataBangunan1, "")
    }

    fun savedatabangunan1(log: String, context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putString(dataBangunan1, log)
        editor.apply()
    }

    fun deletedatabangunan1(context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.remove(dataBangunan1)
        editor.apply()
    }

    fun getdatabangunan2(context: Context): String? {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString(dataBangunan2, "")
    }

    fun savedatabangunan2(log: String, context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putString(dataBangunan2, log)
        editor.apply()
    }

    fun deletedatabangunan2(context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.remove(dataBangunan2)
        editor.apply()
    }

    /**
     * Check users status login
     */
    private fun getPreferences(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun saveislogin(loggedIn: Boolean, context: Context) {
        val editor = getPreferences(context).edit()
        editor.putBoolean(IS_LOGIN, loggedIn)
        editor.apply()
    }

    fun getislogin(context: Context): Boolean {
        return getPreferences(context).getBoolean(IS_LOGIN, false)
    }

    /**
     * Check if network is connected
     */
    fun isNetworkConnected(ctx: Context): Boolean {
        val cm = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val ni = cm.activeNetworkInfo
        return ni != null
    }

    fun isOnline(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = connectivityManager.activeNetworkInfo
        return (netInfo != null && netInfo.isConnected)
    }

    /**
     * Check remainder expired dialog sync
     */

    fun getemainderDialogSync(context: Context): String? {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString("dataExpired", "")
    }

    fun saveremainderDialogSync(log: String, context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putString("dataExpired", log)
        editor.apply()
    }

    fun getToken(context: Context): String? {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString(Preference.Key.TOKEN, null)
    }

    fun saveToken(activity: Context, value: String) {
        Log.d("SPMANAGER", "saving" + value + " for key " + Preference.Key.TOKEN)
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity)
        val editor = sharedPreferences.edit()
        editor.putString(Preference.Key.TOKEN, value)
        if (editor.commit()) {
            Log.d("SPMANAGER", "commited" + value)
        } else {
            Log.d("SPMANAGER", "not commited")
        }
    }
}
