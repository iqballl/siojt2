package mki.siojt2.utils

import android.graphics.Path
import java.io.Serializable


class MapsProjectIntentSetting(
        var showImageUAV: Boolean = false,
        var showProjectUAV: Boolean = false,
        var showObjectUAV: Boolean = false,
        var canTouchObjectUAV: Boolean = false,
        var canSelectObjectUAV: Boolean = false,
        var defaultSelectedObjectUAVId: Int? = 0,
        var objectType: Int? = 0, //1 tanah 2 bangunan
        var showCompareData: Boolean = false
) : Serializable