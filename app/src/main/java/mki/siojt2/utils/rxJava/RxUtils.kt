package mki.siojt2.utils.rxJava


import android.os.Looper
import android.util.Log
import io.reactivex.ObservableTransformer
import io.reactivex.SingleTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import mki.siojt2.utils.Logger

/**
 * Created by iqbal on 09/10/17.
 */

object RxUtils {

    /*public static Observable.Transformer applySchedulers = new Observable.Transformer() {
        @Override
        public Object call(Object o){
            return null;
        }
    };*/
    fun <T> applyScheduler(): ObservableTransformer<T, T> {
        return ObservableTransformer { tObservable ->
            tObservable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
        }
    }

    fun <T> ioToMainSingleScheduler(): SingleTransformer<T, T> = SingleTransformer { upstream ->
        upstream.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun <T> applyApiCall(): ObservableTransformer<T, T> {
        return ObservableTransformer { tObservable ->
            tObservable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    /*.onErrorResumeNext { throwable ->

                        Logger.log(throwable)
                        if (throwable is HttpException) {
                            try {
                                val res = throwable.response().errorBody().string()

                                val gson = SIOJT2.component!!.gson()
                                val baseResponse = gson.fromJson(res, BaseResponse::class.java)
                                val e = BaseAppException(baseResponse, throwable.response().code())
                                return@onErrorResumeNext tObservable.subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                            } catch (e: IOException) {
                                return@onErrorResumeNext tObservable.subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                            }

                        } else {
                            return@onErrorResumeNext tObservable.subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())

                        }
                    }*/
        }
    }

    fun checkMainThread() {
        val isMainThread = Looper.myLooper() == Looper.getMainLooper()
        Logger.log(Log.DEBUG, "RX___ Is main thread :$isMainThread")
    }
}
