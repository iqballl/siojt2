package mki.siojt2.utils.custom_view

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import com.mapbox.mapboxsdk.maps.MapView


class CustomMapBox(context: Context, attrs: AttributeSet) : MapView(context, attrs) {

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        when (ev.action) {
            MotionEvent.ACTION_UP -> {
                println("unlocked")
                this.parent.requestDisallowInterceptTouchEvent(false)
            }

            MotionEvent.ACTION_DOWN -> {
                println("locked")
                this.parent.requestDisallowInterceptTouchEvent(true)
            }
        }
        return super.dispatchTouchEvent(ev)
    }
}