package mki.siojt2.utils.custom_view

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import io.realm.Case
import io.realm.Realm
import mki.siojt2.R
import mki.siojt2.model.region.ResponseDataCityWithoutParameter


class CountryAutoCompleteAdapter(context: Context) : BaseAdapter(), Filterable {

    var mCity: List<ResponseDataCityWithoutParameter> = ArrayList()
    val mContext: Context = context

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val view: TextView = convertView as TextView?
                ?: LayoutInflater.from(mContext).inflate(R.layout.item_spinner, parent, false) as TextView
        view.text = mCity[position].getRegencyName()
        return view
    }

    override fun getItem(position: Int): ResponseDataCityWithoutParameter {
        return mCity[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return mCity.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {

            override fun convertResultToString(resultValue: Any): String {
                return (resultValue as ResponseDataCityWithoutParameter).getRegencyName().toString()
            }

            @SuppressLint("DefaultLocale")
            override fun performFiltering(constraint: CharSequence?): FilterResults? {
                val queryString = constraint?.toString()?.toLowerCase()
                val filterResults = FilterResults()
                filterResults.values = if (queryString==null || queryString.isEmpty())
                    mCity = filterCountries(queryString.toString())
                else
                    mCity.filter {
                        it.getRegencyName()?.toLowerCase()!!.contains(queryString)
                    }
                return filterResults
            }

            @SuppressLint("DefaultLocale")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
               /* mCity = results?.values as ArrayList<ResponseDataCityWithoutParameter>
                notifyDataSetChanged()*/
                if (constraint != null) {
                    val queryString = constraint.toString().toLowerCase()
                    val query = constraint.toString().toLowerCase()
                    results!!.values = if (queryString.isEmpty())
                        mCity = filterCountries(queryString)
                    else
                        mCity.filter {
                            it.getRegencyName()?.toLowerCase()!!.contains(queryString)
                        }
                    notifyDataSetChanged()
                } else {
                    notifyDataSetInvalidated()
                }
            }
        }
    }

    private fun filterCountries(query: String): List<ResponseDataCityWithoutParameter> {
        val mRealm = Realm.getDefaultInstance()
        return mRealm
                .where(ResponseDataCityWithoutParameter::class.java)
                .limit(5)
                .contains("regencyName", query, Case.INSENSITIVE)
                .findAll()
    }

    private class ListRowHolder(row: View?) {
        val label: TextView = row?.findViewById(R.id.text1) as com.pixplicity.fontview.FontTextView

    }

}