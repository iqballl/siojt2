package mki.siojt2.utils.dialog

import android.content.Context
import android.graphics.Color
import androidx.core.content.ContextCompat

import com.afollestad.materialdialogs.MaterialDialog
import mki.siojt2.R

/**
 * Created by bayunvnt on 09/10/17.
 */

object MessageDialog {
    fun showMessage(context: Context, message: Int) {
        showMessage(context, context.getString(message))
    }

    fun showMessage(context: Context, message: String) {
        MaterialDialog.Builder(context)
                .title(R.string.app_name)
                .titleColor(Color.parseColor("#B24F0E"))
                .content(message)
                .cancelable(false)
                .backgroundColor(Color.parseColor("#E9E3E3"))
                .icon(ContextCompat.getDrawable(context, R.drawable.ic_logo_splash)!!)
                .maxIconSize(40)
                .positiveText("OK")
                .positiveColor(Color.parseColor("#B24F0E"))
                .onPositive { dialog, _ -> dialog.dismiss() }
                .show()
    }
}
