package mki.siojt2.utils.realm;

import android.app.Activity;
import android.app.Application;
import androidx.fragment.app.Fragment;

import io.realm.Realm;

/**
 * Created by Endra on 2019-10-14.
 */
public class RealmController {
    private static RealmController instance;
    private final Realm realm;

    private RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {
        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {
        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {
        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }
}
