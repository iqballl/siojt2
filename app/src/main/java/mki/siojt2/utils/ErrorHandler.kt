package mki.siojt2.utils

import com.google.gson.JsonSyntaxException
import mki.siojt2.constant.Constant
import mki.siojt2.base.view.MvpView
import mki.siojt2.utils.network.BaseAppException
import retrofit2.HttpException
import java.io.IOException
import java.net.HttpURLConnection.HTTP_BAD_REQUEST
import java.net.HttpURLConnection.HTTP_INTERNAL_ERROR
import java.net.HttpURLConnection.HTTP_FORBIDDEN
import java.net.HttpURLConnection.HTTP_UNAUTHORIZED
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.net.ssl.HttpsURLConnection


/**
 * Created by iqbal.
 */

object ErrorHandler {
    fun handlerErrorPresenter(mvpView: MvpView, throwable: Throwable) {
        val API_STATUS_CODE_LOCAL_ERROR = 0
        val message = handleError(throwable)

        when (throwable) {
            is HttpException -> when (throwable.code()) {
                HttpsURLConnection.HTTP_UNAUTHORIZED -> mvpView.onFailed("Unauthorised user")
                HttpsURLConnection.HTTP_FORBIDDEN -> mvpView.onFailed("Forbidden")
                HttpsURLConnection.HTTP_INTERNAL_ERROR -> mvpView.onFailed("Internal server error")
                HttpsURLConnection.HTTP_BAD_REQUEST -> mvpView.onFailed("Bad request")
                API_STATUS_CODE_LOCAL_ERROR -> mvpView.onFailed("No internet connection")
                else -> mvpView.onFailed(throwable.message())
            }
            is BaseAppException -> {
                val cetunException = throwable as BaseAppException?
                val baseResponse = cetunException!!.response
                mvpView.onFailed(baseResponse.message.toString())
            }
            is SocketTimeoutException -> {
                mvpView.onFailed("Koneksi internet lambat. Harap ulangi!")
            }
            is UnknownHostException ->{
                mvpView.onFailed(throwable.message.toString())
            }
            is IOException -> mvpView.onFailed("No internet connection!")
            is JsonSyntaxException -> mvpView.onFailed("Something went wrong API is not responding properly!")
            else -> mvpView.onFailed(Constant.NETWORK_ERROR)
        }
    }

    private fun handleError(throwable: Throwable?): String? {
        Logger.log(throwable)
        var messageThrowable: String? = null

        if (throwable == null) {
            messageThrowable = Constant.NETWORK_ERROR
            return messageThrowable
        } else if (throwable is BaseAppException) {
            val cetunException = throwable as BaseAppException?
            val baseResponse = cetunException!!.response
            messageThrowable = baseResponse.message
            //return messageThrowable
        } else if (throwable is HttpException) {
            val httpException = throwable as HttpException?
            if (httpException!!.code() < 200 || httpException.code() >= 300) {
                //forceLogout()
                messageThrowable = httpException.message()
                //return messageThrowable
            }
        }
        return messageThrowable
    }

    private fun forceLogout() {

    }
}
