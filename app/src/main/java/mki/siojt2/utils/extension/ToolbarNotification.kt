package mki.siojt2.utils.extension

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.notification_badge_icon_layout.view.*
import mki.siojt2.R


/**
 * Created by iqbal on 1/23/2018.
 */

object ToolbarNotification {

    fun convertLayoutToImage(mContext: Context, count: Int, drawableId: Int): Drawable {
        val inflater = LayoutInflater.from(mContext)
        val view = inflater.inflate(R.layout.notification_badge_icon_layout, null)
        (view.findViewById(R.id.icon_badge) as ImageView).setImageResource(drawableId)

        if (count == 0) {
            val counterTextPanel = view.counterValuePanel
            counterTextPanel.visibility = View.GONE
        } else {
            val textView = view.findViewById(R.id.count) as TextView
            textView.text = "" + count
        }

        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
        view.layout(0, 0, view.measuredWidth, view.measuredHeight)

        view.isDrawingCacheEnabled = true
        view.drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
        val bitmap = Bitmap.createBitmap(view.drawingCache)
        view.isDrawingCacheEnabled = false

        return BitmapDrawable(mContext.resources, bitmap)
    }
}
