package mki.siojt2.utils.extension

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import com.google.gson.Gson
import mki.siojt2.model.DataExpiredDialogSync
import mki.siojt2.model.SyncDataDinamisSplashScreen
import mki.siojt2.model.SyncDataSplashScreen
import mki.siojt2.persistence.Preference
import mki.siojt2.utils.Utils
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class SyncConfig(context: Context) {

    private var mContext = context

    /* temporary storage of data synchronization */
    private var dataSyncrounous = Preference.getsaveSyncDataSplashScreen()
    private var msyncData = SyncDataSplashScreen()

    /* temporary storage of dynamic data synchronization */
    private var datadinamisSyncrounous = Preference.getsaveSyncDataDinamisSplashScreen()
    private var msyncDataDinamis = SyncDataDinamisSplashScreen()

    @SuppressLint("SimpleDateFormat")
    private val dateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
    private lateinit var mCalendar: Calendar
    private lateinit var mCalendar2: Calendar

    /**/
    val checkFirstSync: Boolean
        get() {
            if (dataSyncrounous?.appSyncDate == null) return true
            return false
        }

    val checkSyncOnlyDinamisData: Boolean
        get() {
            if (datadinamisSyncrounous?.appDinamisSyncDate == null) return true
            return false
        }

    val checkFirstSyncWithCurrentDate: Boolean
        get() {
            if (dataSyncrounous!!.appSyncDate == null) {
                return true
            } else {
                val string = "January 01, 2020 00:05:00"
                val format = SimpleDateFormat("MMMM d, yyyy HH:mm:ss", Locale.ENGLISH)
                val mdate = format.parse(string)
                //check that the current date exceeds the first synchronized date
                Log.d("SyncConfig", dateFormat.format(mdate))
                Log.d("SyncConfig", dateFormat.format(dataSyncrounous!!.appSyncDate))
                Log.d("SyncConfig", dateFormat.format(Date()))
                if (Date().after(dataSyncrounous!!.appSyncDate)) return true
                return false
            }

        }

    val checkDinamisSyncWithCurrentDate: Boolean
        get() {
            //check that the current date exceeds the first synchronized date
            if (datadinamisSyncrounous!!.appDinamisSyncDate == null) {
                return true
            } else {
                Log.d("SyncConfig", dateFormat.format(datadinamisSyncrounous!!.appDinamisSyncDate))
                Log.d("SyncConfig", dateFormat.format(Date()))
                if (Date().after(datadinamisSyncrounous?.appDinamisSyncDate)) return true
                return false
            }

        }

    /**/
    fun setDataSync() {
        try {
            msyncData.appSyncStatus = 0
            Preference.saveSyncDataSplashScreen(msyncData)
        } catch (e: Exception) {
            Log.d("SyncConfig", "failed to set data sync")
        } finally {
            Log.d("SyncConfig", "set data sync successfull")
        }

    }

    fun setDinamisDataSync() {
        try {
            msyncDataDinamis.appDinamisSyncStatus = 0
            Preference.saveSyncDataDinamisSplashScreen(msyncDataDinamis)
        } catch (e: Exception) {
            Log.d("SyncConfig", "failed to set dinamis data sync")
        } finally {
            Log.d("SyncConfig", "set dinamis data sync successfull")
        }

    }

    /**/
    fun saveDataSync() {
        val currentDate = Date()
        // convert date to calendar
        mCalendar = Calendar.getInstance(Locale.getDefault())
        mCalendar.time = currentDate
        mCalendar.add(Calendar.MONTH, 0)
        mCalendar.set(Calendar.HOUR_OF_DAY, 0)
        mCalendar.set(Calendar.MINUTE, 0)
        mCalendar.set(Calendar.SECOND, 0)

        val mCalendar2 = Calendar.getInstance(Locale.getDefault())
        mCalendar2.set(Calendar.HOUR_OF_DAY, 0)
        mCalendar2.set(Calendar.MINUTE, 0)
        mCalendar2.set(Calendar.SECOND, 0)

        try {
            msyncData.appSyncStatus = 1
            msyncData.appSyncDate = mCalendar.time
            Preference.saveSyncDataSplashScreen(msyncData)

            val dataExpiredDialogSync = DataExpiredDialogSync()
            dataExpiredDialogSync.dtaExpiredDialogSyncStatus = 0
            dataExpiredDialogSync.dataExpiredDialogSyncDate = mCalendar2.time
            val dataExpireDate = Gson().toJson(dataExpiredDialogSync)
            Utils.saveremainderDialogSync(dataExpireDate, mContext)
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("SyncConfig", "failed to save data sync")
        } finally {
            Log.d("SyncConfig", "save data sync successfull")
        }

    }


    fun saveDinamisDataSync() {
        val currentDate = Date()
        // convert date to calendar
        mCalendar2 = Calendar.getInstance(Locale.getDefault())
        mCalendar2.time = currentDate
        mCalendar2.add(Calendar.DAY_OF_MONTH, 1)
        mCalendar2.set(Calendar.HOUR_OF_DAY, 0)
        mCalendar2.set(Calendar.MINUTE, 0)
        mCalendar2.set(Calendar.SECOND, 0)

        try {
            msyncDataDinamis.appDinamisSyncStatus = 1
            msyncDataDinamis.appDinamisSyncDate = mCalendar2.time
            Preference.saveSyncDataDinamisSplashScreen(msyncDataDinamis)
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("SyncConfig", "failed to save data sync")
        } finally {

            Log.d("SyncConfig", "save data sync successfull")
        }

    }

}