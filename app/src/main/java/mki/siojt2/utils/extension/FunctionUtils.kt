package mki.siojt2.utils.extension

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.database.MergeCursor
import android.os.Build
import android.provider.MediaStore
import androidx.core.app.ActivityCompat
import android.view.View
import java.text.SimpleDateFormat
import java.util.*

object FunctionUtils {


    private val KEY_ALBUM = "album_name"
    private val KEY_PATH = "path"
    private val KEY_TIMESTAMP = "timestamp"
    private val KEY_TIME = "date"
    private val KEY_COUNT = "date"


    fun View.setDoubleClickListener(listener: View.OnClickListener, waitMillis : Long = 1000) {
        var lastClickTime = 0L
        setOnClickListener { view ->
            if (System.currentTimeMillis() > lastClickTime + waitMillis) {
                listener.onClick(view)
                lastClickTime = System.currentTimeMillis()
            }
        }
    }


    fun hasPermissions(context: Context?, permissions: Array<String>): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false
                }
            }
        }
        return true
    }


    fun mappingInbox(album: String, path: String, timestamp: String, time: String, count: String): HashMap<String, String> {
        val map = HashMap<String, String>()
        map[KEY_ALBUM] = album
        map[KEY_PATH] = path
        map[KEY_TIMESTAMP] = timestamp
        map[KEY_TIME] = time
        map[KEY_COUNT] = count
        return map
    }


    fun getCount(c: Context, album_name: String): String {
        val uriExternal = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val uriInternal = MediaStore.Images.Media.INTERNAL_CONTENT_URI

        val projection = arrayOf(MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.MediaColumns.DATE_MODIFIED)
        val cursorExternal = c.contentResolver.query(uriExternal, projection, "bucket_display_name = \"$album_name\"", null, null)
        val cursorInternal = c.contentResolver.query(uriInternal, projection, "bucket_display_name = \"$album_name\"", null, null)
        val cursor = MergeCursor(arrayOf(cursorExternal, cursorInternal))


        return cursor.count.toString() + " Photos"
    }

    @SuppressLint("SimpleDateFormat")
    fun converToTime(timestamp: String): String {
        val datetime = java.lang.Long.parseLong(timestamp)
        val date = Date(datetime)
        val formatter = SimpleDateFormat("dd/MM HH:mm")
        return formatter.format(date)
    }

    fun convertDpToPixel(dp: Float, context: Context): Float {
        val resources = context.resources
        val metrics = resources.displayMetrics
        return dp * (metrics.densityDpi / 160f)
    }

}