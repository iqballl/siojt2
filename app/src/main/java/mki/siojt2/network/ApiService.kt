package mki.siojt2.network

import io.reactivex.Observable
import io.reactivex.Single
import mki.siojt2.base.response.BaseResponse
import mki.siojt2.constant.ApiUri
import mki.siojt2.model.*
import mki.siojt2.model.accesstoken.ResponseAccessToken
import mki.siojt2.model.data_detail_objek_bangunan.ResponseDataDetailBangunan
import mki.siojt2.model.data_detail_tanah.ResponseDataDetailTanah2
import mki.siojt2.model.login.LoginResponse
import mki.siojt2.model.master_data_bangunan.*
import mki.siojt2.model.master_data_form_tanah.*
import mki.siojt2.model.master_data_sarana_lain.*
import mki.siojt2.model.master_data_subjek.*
import mki.siojt2.model.master_data_tanaman.ResponseDataListKategoriRumpunWithOutParam
import mki.siojt2.model.master_data_tanaman.ResponseDataListKategoriTanaman
import mki.siojt2.model.master_data_tanaman.ResponseDataListKategoriTanamanWithOutParam
import mki.siojt2.model.region.*
import mki.siojt2.base.response.ResponseDataList
import mki.siojt2.base.response.ResponseDataObject
import mki.siojt2.model.response_data_objek.ResponseDataListDetailFasilitasLain
import mki.siojt2.model.response_data_objek.ResponseDataListFasilitasLain
import mki.siojt2.model.response_data_objek.ResponseDataListGedung
import mki.siojt2.model.response_data_objek.ResponseDataListTanaman
import retrofit2.http.*


/**
 * Created by iqbal.
 */

interface ApiService {
    @FormUrlEncoded
    @POST(ApiUri.GET_ACCESSTOKEN)
    fun getAccessToken(@Field("app_code") app_code: String): Observable<ResponseDataObject<ResponseAccessToken>>

    @GET(ApiUri.GET_AUTH)
    fun getAuth(
            @Query("username") username: String,
            @Query("password") password: String)
            : Single<ResponseDataObject<LoginResponse>>

    @GET(ApiUri.GET_LIST_PROJECT)
    fun getlistProject(
            @Query("userid") userId: Int,
            @Query("Accesstoken") accessToken: String)
            : Observable<ResponseDataList<ResponseDataListProjectLocal>>

    @GET(ApiUri.GET_SYNC_PROJECT)
    fun getsyncProject(@Query("userid") userId: Int,
                       @Query("Accesstoken") accessToken: String): Observable<BaseResponse>

    @FormUrlEncoded
    @POST(ApiUri.ADD_PARTY)
    fun postaddParties(
            @Field("UserId") userId: Int,
            @Field("ProjectId") projectId: Int,
            @Field("LandId") landId: String,
            @Field("PartyOwnerType") partyOwnerType: String,
            @Field("ProjectPartyType") projectPartyType: String,
            @Field("PartyIdentitySourceId") partyIdentitySourceId: String,
            @Field("PartyIdentitySourceOther") partyIdentitySourceOther: String,
            @Field("PartyFullName") partyFullName: String,
            @Field("PartyBirthPlaceCode") partyBirthPlaceCode: String,
            @Field("PartyBirthPlaceOther") partyBirthPlaceOther: String,
            @Field("PartyBirthDate") partyBirthDate: String,
            @Field("PartyOccupationId") partyOccupationId: String,
            @Field("PartyOccupationOther") partyOccupationOther: String,
            @Field("PartyProvinceCode") partyProvinceCode: String,
            @Field("PartyRegencyCode") partyRegencyCode: String,
            @Field("PartyDistrictCode") partyDistrictCode: String,
            @Field("PartyVillageName") partyVillageName: String,
            @Field("PartyRT") partyRT: String,
            @Field("PartyRW") partyRW: String,
            @Field("PartyNumber") partyNumber: String,
            @Field("PartyBlock") partyBlock: String,
            @Field("PartyStreetName") partyStreetName: String,
            @Field("PartyComplexName") partyComplexName: String,
            @Field("PartyPostalCode") partyPostalCode: String,
            @Field("PartyNPWP") partyNPWP: String,
            @Field("PartyYearStayBegin") partyYearStayBegin: String,
            @Field("PartyIdentityNumber") partyIdentityNumber: String,
            @Field("PartyLivelihoodLivelihoodId[]") partyLivelihoodLivelihoodId: ArrayList<String>?,
            @Field("PartyLivelihoodLivelihoodOther[]") partyLivelihoodLivelihoodOther: ArrayList<String>?,
            @Field("PartyLivelihoodIncome[]") partyLivelihoodIncome: ArrayList<String>?)
            : Observable<ResponseDataObject<BaseResponse>>

    @GET(ApiUri.GET_LIST_PARTIES1)
    fun getlistParties(
            @Query("ProjectId") projectId: Int,
            @Query("AccessToken") accessToken: String)
            : Observable<ResponseDataList<ResponseDataListPemilik>>

    @GET(ApiUri.GET_LIST_PARTIES2)
    fun getlistParties2(
            @Query("ProjectId") projectId: Int,
            @Query("IsFirstParty") isFirstParty: Int,
            @Query("LandId") landId: Int,
            @Query("AccessToken") accessToken: String)
            : Observable<ResponseDataList<ResponseDataListPemilik>>

    @GET(ApiUri.GET_LIST_PARTIES2)
    fun getAllParties(
            @Query("ProjectId") projectId: Int,
            @Query("LandId") landId: String): Observable<ResponseDataList<ResponseDataListPemilik>>


    @GET(ApiUri.GET_DETAIL_PARTIES)
    fun getdetailParties(
            @Query("PartyId") partyId: Int,
            @Query("AccessToken") accessToken: String)
            : Observable<ResponseDataObject<ResponseDataDetailPemilik>>

    /* wilayah */
    @GET(ApiUri.GET_DAFTAR_KOTAKABUPATEN)
    fun getdaftarBirthPlace(
            @Query("SearchCode") searchCode: String,
            @Query("Limit") limit: String): Observable<ResponseDataList<ResponseDataCity>>

    @GET(ApiUri.GET_DAFTAR_PROVINSI)
    fun getdaftarProvinsi(
            @Query("SearchCode") searchCode: String): Observable<ResponseDataList<ResponseDataProvince>>

    @GET(ApiUri.GET_DAFTAR_KOTAKABUPATEN)
    fun getdaftarKotaKabupaten(
            @Query("ProvinceCode") provinceCode: String,
            @Query("SearchCode") searchCode: String,
            @Query("Limit") limit: String): Observable<ResponseDataList<ResponseDataCity>>

    @GET(ApiUri.GET_DAFTAR_KECAMATAN)
    fun getdaftarKecamatan(
            @Query("SearchCode") searchCode: String,
            @Query("RegencyCode") regencyCode: String,
            @Query("Limit") limit: String): Observable<ResponseDataList<ResponseDataKecamatan>>

    @GET(ApiUri.GET_DAFTAR_KELURAHAN)
    fun getdaftarKelurahan(
            @Query("SearchCode") searchCode: String,
            @Query("DistrictCode") districtCode: String,
            @Query("Limit") limit: String): Observable<ResponseDataList<ResponseDataKelurahan>>

    /* tanah */
    @FormUrlEncoded
    @POST(ApiUri.LAND_ADD)
    fun postaddLand(
            @Field("UserId") userId: Int,
            @Field("ProjectId") projectId: Int,
            @Field("PartyId") partyId: Int,
            @Field("ProjectLandNumberList") projectLandNumberList: String,
            @Field("ProjectLandProvinceCode") projectLandProvinceCode: String,
            @Field("ProjectLandRegencyCode") projectLandRegencyCode: String,
            @Field("ProjectLandDistrictCode") projectLandDistrictCode: String,
            @Field("ProjectLandVillageName") projectLandVillageName: String,
            @Field("ProjectLandRT") projectLandRT: String,
            @Field("ProjectLandRW") projectLandRW: String,
            @Field("ProjectLandNumber") projectLandNumber: Int,
            @Field("ProjectLandBlock") projectLandBlock: String,
            @Field("ProjectLandStreetName") projectLandStreetName: String,
            @Field("ProjectLandComplexName") projectLandComplexName: String,
            @Field("ProjectLandPostalCode") projectLandPostalCode: String,
            @Field("ProjectLandPositionToRoadId") projectLandPositionToRoadId: Int,
            @Field("ProjectLandWideFrontRoad") projectLandWideFrontRoad: Float,
            @Field("ProjectLandFrontage") projectLandFrontage: Float,
            @Field("ProjectLandLength") projectLandLength: Float,
            @Field("ProjectLandShapeId") projectLandShapeId: Int,

            @Field("ProjectLandTopographyId") projectLandTopographyId: Int,
            @Field("ProjectLandHeightToRoad") projectLandHeightToRoad: String,
            @Field("ProjectLandZoningSpatialPlanId") projectLandZoningSpatialPlanId: Int,
            @Field("ProjectLandAvailableElectricalGrid") projectLandAvailableElectricalGrid: Int,
            @Field("ProjectLandAvailableCleanWaterSystem") projectLandAvailableCleanWaterSystem: Int,
            @Field("ProjectLandAvailableTelephoneNetwork") projectLandAvailableTelephoneNetwork: Int,
            @Field("ProjectLandAvailableGasPipelineNetwork") projectLandAvailableGasPipelineNetwork: Int,
            @Field("ProjectLandSUTETPresence") projectLandSUTETPresence: Int,
            @Field("ProjectLandSUTETDistance") projectLandSUTETDistance: Float,
            @Field("ProjectLandSkewerPresence") projectLandSkewerPresence: Int,
            @Field("ProjectLandCemeteryPresence") projectLandCemeteryPresence: Int,
            @Field("ProjectLandCemeteryDistance") projectLandCemeteryDistance: Float,

            @Field("ProjectLandOrientationWest") projectLandOrientationWest: String,
            @Field("ProjectLandOrientationEast") projectLandOrientationEast: String,
            @Field("ProjectLandOrientationNorth") projectLandOrientationNorth: String,
            @Field("ProjectLandOrientationSouth") projectLandOrientationSouth: String,
            @Field("ProjectLandOrientationNorthwest") projectLandOrientationNorthwest: String,
            @Field("ProjectLandOrientationSoutheast") projectLandOrientationSoutheast: String,
            @Field("ProjectLandOrientationNortheast") projectLandOrientationNortheast: String,
            @Field("ProjectLandOrientationSouthwest") projectLandOrientationSouthwest: String,
            @Field("ProjectLandEaseToProperty") projectLandEaseToProperty: Int,
            @Field("ProjectLandEaseToShopingCenter") projectLandEaseToShopingCenter: Int,
            @Field("ProjectLandEaseToEducationFacilities") projectLandEaseToEducationFacilities: Int,
            @Field("ProjectLandEaseToTouristSites") projectLandEaseToTouristSites: Int,
            @Field("ProjectLandEaseOfTransportation") projectLandEaseOfTransportation: Int,
            @Field("ProjectLandSecurityAgainstCrime") projectLandSecurityAgainstCrime: Int,
            @Field("ProjectLandSafetyAgainstFireHazards") projectLandSafetyAgainstFireHazards: Int,
            @Field("ProjectLandSecurityAgainstNaturalDisasters") projectLandSecurityAgainstNaturalDisasters: Int,

            @Field("ProjectLandLatitude") projectLandLatitude: String,
            @Field("ProjectLandLangitude") projectLandLangitude: String,
            @Field("ProjectLandOwnershipId") projectLandOwnershipId: Int,
            @Field("ProjectLandNIB") projectLandNIB: String,
            @Field("ProjectLandCertificateNumber") projectLandCertificateNumber: String,
            @Field("ProjectLandAreaCertificate") projectLandAreaCertificate: String,
            @Field("ProjectLandAreaAffected") projectLandAreaAffected: String,
            @Field("ProjectLandDateOfIssue") projectLandDateOfIssue: String,
            @Field("ProjectLandRightsExpirationDate") projectLandRightsExpirationDate: String,
            @Field("ProjectLandAddInfoAddInfoId[]") projectLandAddInfoAddInfoId: ArrayList<String>?,
            @Field("ProjectLandTypeLandTypeId[]") projectLandTypeLandTypeId: ArrayList<String>?,
            @Field("ProjectLandUtilizationLandUtilizationId[]") projectLandUtilizationLandUtilizationId: ArrayList<String>?,
            @Field("AccessToken") accessToken: String): Observable<BaseResponse>


    @GET(ApiUri.GET_LAND_LIST)
    fun getdaftarTanah(
            @Query("ProjectId") projectId: Int,
            @Query("PartyId") partyId: Int,
            @Query("Accesstoken") accessToken: String): Observable<ResponseDataList<ResponseDataListTanah>>

    @GET(ApiUri.GET_LAND_DETAIL)
    fun getdetailTanah(
            @Query("ProjectLandId") projectLandId: Int,
            @Query("Accesstoken") accessToken: String): Observable<ResponseDataObject<ResponseDataDetailTanah2>>

    /* master data subjek/pemilik */
    @GET(ApiUri.GET_DAFTAR_MATA_PENCAHARIAN)
    fun getdaftarMataPencaharian(
            @Query("SearchCode") searchCode: String,
            @Query("limit") districtCode: Int): Observable<ResponseDataList<ResponseDataMataPencaharian>>

    @GET(ApiUri.GET_DAFTAR_SUMBER_IDENTITAS)
    fun getdaftarSumberIdentitas(): Observable<ResponseDataList<ResponseDataSumberIdentitas>>

    @GET(ApiUri.GET_DAFTAR_PEKJERJAAN)
    fun getdaftarPekerjaan(): Observable<ResponseDataList<ResponseDataPekerjaan>>

    @GET(ApiUri.GET_DAFTAR_PENGUASAAN_TANAH)
    fun getdaftarPenguasaanTanah(): Observable<ResponseDataList<ResponseDataListPenguasaanTanah>>


    /* master data tanah */
    /* tanah */
    @GET(ApiUri.GET_LAND_INFO_ADD)
    fun getdaftarinfoTambahanTanah(): Observable<ResponseDataList<ResponseDataListInformasiTambahanTanah>>

    @GET(ApiUri.GET_LAND_SHAPE_LIST)
    fun getdaftarbentukTanah(): Observable<ResponseDataList<ResponseDataListBentukTanah>>

    @GET(ApiUri.GET_LAND_TOPOGRAFI)
    fun getdaftarTopgrafiTanah(): Observable<ResponseDataList<ResponseDataListTipografi>>

    @GET(ApiUri.GET_LAND_UTILIZATION)
    fun getdaftarPenggunaanTanah(): Observable<ResponseDataList<ResponseDataListPenggunaanTanah>>

    @GET(ApiUri.GET_LAND_ZONING_SPATIAL_PLAN)
    fun getdaftarPeruntukanTanah(): Observable<ResponseDataList<ResponseDataListPeruntukanTanah>>

    @GET(ApiUri.GET_LAND_TO_ROAD)
    fun getdaftarposisiTanah(): Observable<ResponseDataList<ResponseDataListPosisiTanah>>

    @GET(ApiUri.GET_LAND_OWNERSHIPS)
    fun getdaftarKepemilikanTanah(): Observable<ResponseDataList<ResponseDataListKepemilikanAtasTanah>>

    /* bangunan */
    @GET(ApiUri.GET_TYPE_BANGUNAN)
    fun getdaftarTipeBangunan(): Observable<ResponseDataList<ResponseDataListTipeBangunan>>

    @GET(ApiUri.GET_PONDASI_BUILDING)
    fun getdaftarPondasiBangunan(): Observable<ResponseDataList<ResponseDataListPondasiBangunan>>

    @GET(ApiUri.GET_STRUCTURE_BUILDING)
    fun getdaftarStrukturBangunan(): Observable<ResponseDataList<ResponseDataListStrukturBangunan>>

    @GET(ApiUri.GET_ROOF_FRAME_BUILDING)
    fun getdaftarKerangkaAtapBangunan(): Observable<ResponseDataList<ResponseDataListKerangkaAtapBangunan>>

    @GET(ApiUri.GET_ROOF_COVERING_BUILDING)
    fun getdaftarPenutupAtapBangunan(): Observable<ResponseDataList<ResponseDataListPenutupAtapBangunan>>

    @GET(ApiUri.GET_ROOF_PLAFON_BUILDING)
    fun getdaftarPlafonBangunan(): Observable<ResponseDataList<ResponseDataListPlafonBangunan>>

    @GET(ApiUri.GET_ROOF_WALL_BUILDING)
    fun getdaftarDindingBangunan(): Observable<ResponseDataList<ResponseDataListDindingBangunan>>

    @GET(ApiUri.GET_ROOF_DOOR_WINDOW_BUILDING)
    fun getdaftarPintuJendelaBangunan(): Observable<ResponseDataList<ResponseDataListPintuJendelaBangunan>>

    @GET(ApiUri.GET_ROOF_FLOOR_BUILDING)
    fun getdaftarLantaiBangunan(): Observable<ResponseDataList<ResponseDataListLantaiBangunan>>

    /* tanaman */
    @GET(ApiUri.GET_TYPE_PLANTS)
    fun getkategoriTanaman(
            @Query("PlantType") plantType: Int): Observable<ResponseDataList<ResponseDataListKategoriTanaman>>

    /* sarana pelengkap */
    @GET(ApiUri.GET_FASILITY_TYPE)
    fun getdaftarjenisFasilitas(): Observable<ResponseDataList<ResponseDataJenisFasilitas>>

    @GET(ApiUri.GET_FASILITY_TYPE_CLEAN_WATER)
    fun getdaftarsumberAir(): Observable<ResponseDataList<ResponseDataJenisSumberAir>>

    @GET(ApiUri.GET_FASILITY_TYPE_CIRCUM)
    fun getdaftarpagarKeliling(): Observable<ResponseDataList<ResponseDataJenisPagarKeliling>>


    /* objek tanah */
    @GET(ApiUri.GET_DAFTAR_OBJEK_GEDUNG)
    fun getdaftarobjekGedung(
            @Query("ProjectLandId") projectLandId: Int,
            @Query("AccessToken") accessToken: String): Observable<ResponseDataList<ResponseDataListGedung>>

    @GET(ApiUri.GET_BUILDING_DETAIL)
    fun getdetailobjekBangunan(
            @Query("ProjectBuildingId") projectBuildingId: Int,
            @Query("AccessToken") accessToken: String): Observable<ResponseDataObject<ResponseDataDetailBangunan>>


    /*
    *       @Query("ProjectBuildingBuiltYear") projectBuildingBuiltYear: Int,
            @Query("ProjectBuildingRenovYear") projectBuildingRenovyear: Int,
            @Query("ProjectBuildingOnPermiteDate") projectBuildingOnPermiteDate: String,
            @Query("ProjectBuildingOnPermiteArea") projectBuildingOnPermiteArea: Float,
    * */

    @FormUrlEncoded
    @POST(ApiUri.ADD_BUILDING)
    fun postBuilding(
            @Field("UserId") userId: Int,
            @Field("ProjectLandId") projectLandId: Int,
            @Field("ProjectPartyId") projectPartyId: Int,
            @Field("ProjectBuildingTypeId") projectBuildingTypeId: Int,
            @Field("ProjectBuildingFloorTotal") projectBuildingFloorTotal: Int,
            @Field("ProjectBuildingFloorAreaUpperRoom") projectBuildingAreaUpperRoom: String,
            @Field("ProjectBuildingFloorAreaLowerRoom") projectBuildingAreaLowerRoom: String,
            @Field("CeilingTypeId[]") ceilingTypeId: ArrayList<String>,
            @Field("PBCeilingTypeAreaTotal[]") pbCeilingTypeAreaTotal: ArrayList<Float>,
            @Field("DoorWindowTypeId[]") doorWindowTypeld: ArrayList<String>,
            @Field("PBDoorWindowTypeAreaTotal[]") pbDoorWindowTypeAreaTotal: ArrayList<Float>,
            @Field("FloorTypeId[]") floorTypeId: ArrayList<String>,
            @Field("PBFloorTypeAreaTotal[]") pbFloorTypeAreaTotal: ArrayList<Float>,
            @Field("FoundationTypeId[]") foundationTypeId: ArrayList<String>,
            @Field("PBFoundationTypeAreaTotal[]") pbFoundationTypeAreaTotal: ArrayList<Float>,
            @Field("RoofCoveringTypeId[]") roofCoveringTypeId: ArrayList<String>,
            @Field("PBRoofCoveringTypeAreaTotal[]") pbRoofCoveringTypeAreaTotal: ArrayList<Float>,
            @Field("RoofFrameTypeId[]") roofFrameTypeId: ArrayList<String>,
            @Field("PBRoofFrameTypeAreaTotal[]") pbRoofFrameTypeAreaTotal: ArrayList<Float>,
            @Field("StructureTypeId[]") structureTypeId: ArrayList<String>,
            @Field("PBStructureTypeAreaTotal[]") pbStructureTypeAreaTotal: ArrayList<Float>,
            @Field("WallTypeId[]") wallTypeId: ArrayList<String>,
            @Field("PBWallTypeAreaTotal[]") pbWallTypeAreaTotal: ArrayList<Float>,
            @Field("AccessToken") accessToken: String

    ): Observable<ResponseDataObject<BaseResponse>>


    @GET(ApiUri.GET_DAFTAR_OBJEK_TANAMAN)
    fun getdaftarobjekTanaman(
            @Query("ProjectLandId") projectLandId: Int,
            @Query("AccessToken") accessToken: String): Observable<ResponseDataList<ResponseDataListTanaman>>


    @POST(ApiUri.ADD_PLANT)
    fun postPlant(
            @Query("UserId") userId: Int,
            @Query("ProjectLandId") projectLandId: Int,
            @Query("ProjectPartyId") projectPartyId: Int,
            @Query("PlantId") plantId: Int,
            @Query("PPlantPlantType") pPlantPlantType: Int,
            @Query("PPlantPlantSizeId") pPlantPlantSizeId: Int,
            @Query("PPlantNumber") pPlantNumber: Int,
            @Query("AccessToken") accessToken: String): Observable<BaseResponse>


    @FormUrlEncoded
    @POST(ApiUri.ADD_OTHER_FACILITY)
    fun postOtherFacility(
            @Field("UserId") userId: Int,
            @Field("ProjectLandId") projectLandId: Int,
            @Field("ProjectPartyId") projectPartyId: Int,
            @Field("FacilityTypeId[]") facilityTypeId: ArrayList<Int>,
            @Field("POtherFacilityName[]") pOtherFacilityName: ArrayList<String>,
            @Field("POtherFacilityValue[]") pOtherFacilityValue: ArrayList<String>,
            @Field("POtherFacilityDescription[]") pOtherFacilityDescription: ArrayList<String>,
            @Field("AccessToken") accessToken: String): Observable<ResponseDataObject<BaseResponse>>

    @GET(ApiUri.GET_DAFTAR_OBJEK_LAIN)
    fun getdaftarobjekLain(
            @Query("ProjectLandId") projectLandId: Int,
            @Query("AccessToken") accessToken: String): Observable<ResponseDataList<ResponseDataListFasilitasLain>>

    @GET(ApiUri.GET_BUILDING_OTHER_FACILITY)
    fun getdetailobjekLain(
            @Query("ProjectLandId") projectLandId: Int,
            @Query("ProjectPartyId") projectPartyId: Int,
            @Query("FacilityTypeId") facilityTypeId: Int,
            @Query("AccessToken") accessToken: String): Observable<ResponseDataObject<ResponseDataListDetailFasilitasLain>>


    /* Wilayah without parameter*/
    @GET(ApiUri.GET_DAFTAR_PROVINSI)
    fun getlistOfProvinceWithOutParameter(): Observable<ResponseDataList<ResponseDataProvinceWithoutParameter>>

    @GET(ApiUri.GET_DAFTAR_KOTAKABUPATEN)
    fun getlistOfCityWithOutParameter(): Observable<ResponseDataList<ResponseDataCityWithoutParameter>>

    @GET(ApiUri.GET_DAFTAR_KECAMATAN)
    fun getlistOfKecamatanWithOutParameter(
            @Query("limit") limit: Int,
            @Query("offset") offset: Int
    ): Observable<ResponseDataList<ResponseDataKecamatanWithoutParameter>>

    @GET(ApiUri.GET_DAFTAR_KELURAHAN)
    fun getlistOfKelurahanWithOutParameter(
            @Query("limit") limit: Int,
            @Query("offset") offset: Int
    ): Observable<ResponseDataList<ResponseDataKelurahanWithoutParameter>>

    /* Data tanah without parameter*/
    @GET(ApiUri.GET_DAFTAR_PENGUASAAN_TANAH)
    fun getdaftarPenguasaanTanahWithOutParam(): Observable<ResponseDataList<ResponseDataListPenguasaanTanahWithOutParam>>

    @GET(ApiUri.GET_LAND_SHAPE_LIST)
    fun getdaftarbentukTanahWithOutParam(): Observable<ResponseDataList<ResponseDataListBentukTanahWithOutParam>>

    @GET(ApiUri.GET_LAND_TOPOGRAFI)
    fun getdaftarTopgrafiTanahWithOutParam(): Observable<ResponseDataList<ResponseDataListTipografiWithOutParam>>

    @GET(ApiUri.GET_LAND_TO_ROAD)
    fun getdaftarposisiTanahWithOutParam(): Observable<ResponseDataList<ResponseDataListPosisiTanahWithOutParam>>

    @GET(ApiUri.GET_LAND_OWNERSHIPS)
    fun getdaftarKepemilikanTanahWithOutParam(): Observable<ResponseDataList<ResponseDataListKepemilikanAtasTanahWithOutParam>>

    @GET(ApiUri.GET_LAND_UTILIZATION)
    fun getdaftarPenggunaanTanahWithOutParam(): Observable<ResponseDataList<ResponseDataListPenggunaanTanahWithOutParam>>

    @GET(ApiUri.GET_LAND_INFO_ADD)
    fun getdaftarinfoTambahanTanahWithOutParam(): Observable<ResponseDataList<ResponseDataListInformasiTambahanTanahWithOutParam>>

    @GET(ApiUri.GET_LAND_ZONING_SPATIAL_PLAN)
    fun getdaftarPeruntukanTanahWithOutParam(): Observable<ResponseDataList<ResponseDataListPeruntukanTanahWithOutParam>>


    /* Data objek bangunan without parameter*/
    @GET(ApiUri.GET_ROOF_WALL_BUILDING)
    fun getdaftarDindingBangunanWithOutParam(): Observable<ResponseDataList<ResponseDataListDindingBangunanWithOutParam>>

    @GET(ApiUri.GET_ROOF_FRAME_BUILDING)
    fun getdaftarKerangkaAtapBangunanWithOutParam(): Observable<ResponseDataList<ResponseDataListKerangkaAtapBangunanWithOutParam>>

    @GET(ApiUri.GET_ROOF_COVERING_BUILDING)
    fun getdaftarPenutupAtapBangunanWithOutParam(): Observable<ResponseDataList<ResponseDataListPenutupAtapBangunanWithOutParam>>

    @GET(ApiUri.GET_ROOF_FLOOR_BUILDING)
    fun getdaftarLantaiBangunanWithOutParam(): Observable<ResponseDataList<ResponseDataListLantaiBangunanWithOutParam>>

    @GET(ApiUri.GET_ROOF_DOOR_WINDOW_BUILDING)
    fun getdaftarPintuJendelaBangunanWithOutParam(): Observable<ResponseDataList<ResponseDataListPintuJendelaBangunanWithOutParam>>

    @GET(ApiUri.GET_ROOF_PLAFON_BUILDING)
    fun getdaftarPlafonBangunanWithOutParam(): Observable<ResponseDataList<ResponseDataListPlafonBangunanWithOutParam>>

    @GET(ApiUri.GET_PONDASI_BUILDING)
    fun getdaftarPondasiBangunanWithOutParam(): Observable<ResponseDataList<ResponseDataListPondasiBangunanWithOutParam>>

    @GET(ApiUri.GET_STRUCTURE_BUILDING)
    fun getdaftarStrukturBangunanWithOutParam(): Observable<ResponseDataList<ResponseDataListStrukturBangunanWithOutParam>>

    @GET(ApiUri.GET_TYPE_BANGUNAN)
    fun getdaftarTipeBangunanWithOutParam(): Observable<ResponseDataList<ResponseDataListTipeBangunanWithOutParam>>


    /* Data objek tanaman without parameter */
    @GET(ApiUri.GET_TYPE_PLANTS)
    fun getkategoriTanamanWithRealmObject(
            @Query("PlantType") plantType: String): Observable<ResponseDataList<ResponseDataListKategoriTanamanWithOutParam>>

    @GET(ApiUri.GET_TYPE_PLANTS)
    fun getkategoriRumpunWithRealmObject(
            @Query("PlantType") plantType: String): Observable<ResponseDataList<ResponseDataListKategoriRumpunWithOutParam>>


    /* Data objek sarana pelengkap without parameter */
    @GET(ApiUri.GET_FASILITY_TYPE)
    fun getdaftarjenisFasilitasWithOutParam(): Observable<ResponseDataList<ResponseDataJenisFasilitasWithOutParam>>

    @GET(ApiUri.GET_FASILITY_TYPE_CIRCUM)
    fun getdaftarpagarKelilingWithOutParam(): Observable<ResponseDataList<ResponseDataJenisPagarKelilingWithOutParam>>

    @GET(ApiUri.GET_FASILITY_TYPE_CLEAN_WATER)
    fun getdaftarsumberAirWithOutParam(): Observable<ResponseDataList<ResponseDataJenisSumberAirWithOutParam>>

    /* Data subjek without parameter */
    @GET(ApiUri.GET_DAFTAR_MATA_PENCAHARIAN)
    fun getdaftarMataPencaharianWithOutParam(): Observable<ResponseDataList<ResponseDataMataPencaharianWithOutParam>>

    @GET(ApiUri.GET_DAFTAR_PEKJERJAAN)
    fun getdaftarPekerjaanWithOutParam(): Observable<ResponseDataList<ResponseDataPekerjaanWithOutParam>>

    @GET(ApiUri.GET_DAFTAR_SUMBER_IDENTITAS)
    fun getdaftarSumberIdentitasWithOutParam(): Observable<ResponseDataList<ResponseDataSumberIdentitasWithOutParam>>

}
