package mki.siojt2.network

import io.reactivex.Observable
import io.reactivex.Single
import mki.siojt2.base.response.BaseResponse
import mki.siojt2.constant.Constant
import mki.siojt2.model.*
import mki.siojt2.model.accesstoken.ResponseAccessToken
import mki.siojt2.model.data_detail_objek_bangunan.ResponseDataDetailBangunan
import mki.siojt2.model.data_detail_tanah.ResponseDataDetailTanah2
import mki.siojt2.model.login.LoginResponse
import mki.siojt2.model.master_data_bangunan.*
import mki.siojt2.model.master_data_form_tanah.*
import mki.siojt2.model.master_data_sarana_lain.*
import mki.siojt2.model.master_data_subjek.*
import mki.siojt2.model.master_data_tanaman.ResponseDataListKategoriRumpunWithOutParam
import mki.siojt2.model.master_data_tanaman.ResponseDataListKategoriTanaman
import mki.siojt2.model.master_data_tanaman.ResponseDataListKategoriTanamanWithOutParam
import mki.siojt2.model.region.*
import mki.siojt2.base.response.ResponseDataList
import mki.siojt2.base.response.ResponseDataObject
import mki.siojt2.dagger.preferences.PreferenceHelper
import mki.siojt2.model.auth.SessionAuth
import mki.siojt2.model.response_data_objek.ResponseDataListDetailFasilitasLain
import mki.siojt2.model.response_data_objek.ResponseDataListFasilitasLain
import mki.siojt2.model.response_data_objek.ResponseDataListGedung
import mki.siojt2.model.response_data_objek.ResponseDataListTanaman
import mki.siojt2.persistence.Preference
import javax.inject.Inject

/**
 * Created by iqbal on 26/05/19.
 */

class DataManager @Inject
constructor(private val apiService: ApiService, preferencesHelper: PreferenceHelper) {

    val accessToken: Observable<ResponseDataObject<ResponseAccessToken>>
        get() = apiService.getAccessToken(Constant.APP_CODE)

    fun getAuth(username: String, password: String): Single<ResponseDataObject<LoginResponse>> {
        return apiService.getAuth(username, password)
    }

    fun getlistProject(userId: Int, accessToken: String): Observable<ResponseDataList<ResponseDataListProjectLocal>> {
        return apiService.getlistProject(userId, accessToken)
    }

    fun getsyncProject(userId: Int, accessToken: String): Observable<BaseResponse> {
        return apiService.getsyncProject(userId, accessToken)
    }

    fun postaddParties(
            userId: Int, projectId: Int, landId: String, partyOwnerType: String,
            projectPartyType: String, partyIdentitySourceId: String,
            partyIdentitySourceOther: String, partyFullName: String,
            partyBirthPlaceCode: String, partyBirthPlaceOther: String,
            partyBirthDate: String, artyOccupationId: String,
            partyOccupationOther: String, partyProvinceCode: String,
            partyRegencyCode: String, partyDistrictCode: String,
            partyVillageName: String, partyRT: String,
            partyRW: String, partyNumber: String, partyBlock: String,
            partyStreetName: String, partyComplexName: String,
            partyPostalCode: String, partyNPWP: String,
            partyYearStayBegin: String, partyIdentityNumber: String,
            partyLivelihoodLivelihoodId: ArrayList<String>?, partyLivelihoodLivelihoodOther: ArrayList<String>?, partyLivelihoodIncome: ArrayList<String>?): Observable<ResponseDataObject<BaseResponse>> {

        return apiService.postaddParties(userId, projectId, landId, partyOwnerType, projectPartyType, partyIdentitySourceId, partyIdentitySourceOther,
                partyFullName, partyBirthPlaceCode, partyBirthPlaceOther, partyBirthDate, artyOccupationId, partyOccupationOther,
                partyProvinceCode, partyRegencyCode, partyDistrictCode, partyVillageName, partyRT, partyRW, partyNumber, partyBlock,
                partyStreetName, partyComplexName, partyPostalCode, partyNPWP, partyYearStayBegin, partyIdentityNumber, partyLivelihoodLivelihoodId,
                partyLivelihoodLivelihoodOther, partyLivelihoodIncome)
    }

    fun getAllParties(projectId: Int, landId: String): Observable<ResponseDataList<ResponseDataListPemilik>> {
        return apiService.getAllParties(projectId, landId)
    }


    fun getlistParties(projectId: Int, accessToken: String): Observable<ResponseDataList<ResponseDataListPemilik>> {
        return apiService.getlistParties(projectId, accessToken)
    }

    fun getlistParties2(projectId: Int, isFirstParty: Int, landId: Int, accessToken: String): Observable<ResponseDataList<ResponseDataListPemilik>> {
        return apiService.getlistParties2(projectId, isFirstParty, landId, accessToken)
    }


    fun getdetailParties(partyId: Int, accessToken: String): Observable<ResponseDataObject<ResponseDataDetailPemilik>> {
        return apiService.getdetailParties(partyId, accessToken)
    }

    /* wilayah */
    fun getdaftarBirthPlace(searchCode: String, limit: String): Observable<ResponseDataList<ResponseDataCity>> {
        return apiService.getdaftarBirthPlace(searchCode, limit)
    }

    fun getdaftarProvinsi(searchCode: String): Observable<ResponseDataList<ResponseDataProvince>> {
        return apiService.getdaftarProvinsi(searchCode)
    }

    fun getdaftarKotaKabupaten(provinceCode: String, searchCode: String, limit: String): Observable<ResponseDataList<ResponseDataCity>> {
        return apiService.getdaftarKotaKabupaten(provinceCode, searchCode, limit)
    }

    fun getdaftarKecamatan(searchCode: String, regencyCode: String, limit: String): Observable<ResponseDataList<ResponseDataKecamatan>> {
        return apiService.getdaftarKecamatan(searchCode, regencyCode, limit)
    }

    fun getdaftarKelurahan(searchCode: String, districtCode: String, limit: String): Observable<ResponseDataList<ResponseDataKelurahan>> {
        return apiService.getdaftarKelurahan(searchCode, districtCode, limit)
    }

    /*tanah*/
    fun postaddLand(userId: Int, projectId: Int, partyId: Int, projectLandNumberList: String,
                    projectLandProvinceCode: String, projectLandRegencyCode: String,
                    projectLandDistrictCode: String, projectLandVillageName: String,
                    projectLandRT: String, projectLandRW: String,
                    projectLandNumber: Int, projectLandBlock: String,
                    projectLandStreetName: String, projectLandComplexName: String,
                    projectLandPostalCode: String, projectLandPositionToRoadId: Int,
                    projectLandWideFrontRoad: Float, projectLandFrontage: Float,
                    projectLandLength: Float, projectLandShapeId: Int,

                    projectLandTopographyId: Int, projectLandHeightToRoad: String,
                    projectLandZoningSpatialPlanId: Int, projectLandAvailableElectricalGrid: Int,
                    projectLandAvailableCleanWaterSystem: Int, projectLandAvailableTelephoneNetwork: Int,
                    projectLandAvailableGasPipelineNetwork: Int, projectLandSUTETPresence: Int,
                    projectLandSUTETDistance: Float, projectLandSkewerPresence: Int,
                    projectLandCemeteryPresence: Int, projectLandCemeteryDistance: Float,

                    projectLandOrientationWest: String, projectLandOrientationEast: String,
                    projectLandOrientationNorth: String, projectLandOrientationSouth: String,
                    projectLandOrientationNorthwest: String, projectLandOrientationSoutheast: String,
                    projectLandOrientationNortheast: String, projectLandOrientationSouthwest: String,
                    projectLandEaseToProperty: Int, projectLandEaseToShopingCenter: Int,
                    projectLandEaseToEducationFacilities: Int, projectLandEaseToTouristSites: Int,
                    projectLandEaseOfTransportation: Int, projectLandSecurityAgainstCrime: Int,
                    projectLandSafetyAgainstFireHazards: Int, projectLandSecurityAgainstNaturalDisasters: Int,

                    projectLandLatitude: String, projectLandLangitude: String,
                    projectLandOwnershipId: Int, projectLandNIB: String,
                    projectLandCertificateNumber: String, projectLandAreaCertificate: String,
                    projectLandAreaAffected: String, projectLandDateOfIssue: String,
                    projectLandRightsExpirationDate: String, projectLandAddInfoAddInfoId: ArrayList<String>?,
                    projectLandTypeLandTypeId: ArrayList<String>?, projectLandUtilizationLandUtilizationId: ArrayList<String>?,
                    accessToken: String): Observable<BaseResponse> {
        return apiService.postaddLand(userId, projectId, partyId, projectLandNumberList,
                projectLandProvinceCode, projectLandRegencyCode,
                projectLandDistrictCode, projectLandVillageName,
                projectLandRT, projectLandRW,
                projectLandNumber, projectLandBlock,
                projectLandStreetName, projectLandComplexName,
                projectLandPostalCode, projectLandPositionToRoadId,
                projectLandWideFrontRoad, projectLandFrontage,
                projectLandLength, projectLandShapeId,

                projectLandTopographyId, projectLandHeightToRoad,
                projectLandZoningSpatialPlanId, projectLandAvailableElectricalGrid,
                projectLandAvailableCleanWaterSystem, projectLandAvailableTelephoneNetwork,
                projectLandAvailableGasPipelineNetwork, projectLandSUTETPresence,
                projectLandSUTETDistance, projectLandSkewerPresence,
                projectLandCemeteryPresence, projectLandCemeteryDistance,

                projectLandOrientationWest, projectLandOrientationEast,
                projectLandOrientationNorth, projectLandOrientationSouth,
                projectLandOrientationNorthwest, projectLandOrientationSoutheast,
                projectLandOrientationNortheast, projectLandOrientationSouthwest,
                projectLandEaseToProperty, projectLandEaseToShopingCenter,
                projectLandEaseToEducationFacilities, projectLandEaseToTouristSites,
                projectLandEaseOfTransportation, projectLandSecurityAgainstCrime,
                projectLandSafetyAgainstFireHazards, projectLandSecurityAgainstNaturalDisasters,

                projectLandLatitude, projectLandLangitude,
                projectLandOwnershipId, projectLandNIB,
                projectLandCertificateNumber, projectLandAreaCertificate,
                projectLandAreaAffected, projectLandDateOfIssue,
                projectLandRightsExpirationDate, projectLandAddInfoAddInfoId,
                projectLandTypeLandTypeId, projectLandUtilizationLandUtilizationId,
                accessToken
        )
    }

    fun getdaftarTanah(projectId: Int, partyId: Int, accessToken: String): Observable<ResponseDataList<ResponseDataListTanah>> {
        return apiService.getdaftarTanah(projectId, partyId, accessToken)
    }

    fun getdetailTanah(projectLandId: Int, accessToken: String): Observable<ResponseDataObject<ResponseDataDetailTanah2>> {
        return apiService.getdetailTanah(projectLandId, accessToken)
    }

    /* MASTER DATA */
    /* subjek/pemilik*/
    fun getdaftarMataPencaharian(searchCode: String, limit: Int): Observable<ResponseDataList<ResponseDataMataPencaharian>> {
        return apiService.getdaftarMataPencaharian(searchCode, limit)
    }

    fun getdaftarSumberIdentitas(): Observable<ResponseDataList<ResponseDataSumberIdentitas>> {
        return apiService.getdaftarSumberIdentitas()
    }

    fun getdaftarPekerjaan(): Observable<ResponseDataList<ResponseDataPekerjaan>> {
        return apiService.getdaftarPekerjaan()
    }

    fun getdaftarPenguasaanTanah(): Observable<ResponseDataList<ResponseDataListPenguasaanTanah>> {
        return apiService.getdaftarPenguasaanTanah()
    }


    /* tanah*/
    fun getdaftarKepemilikanTanah(): Observable<ResponseDataList<ResponseDataListKepemilikanAtasTanah>> {
        return apiService.getdaftarKepemilikanTanah()
    }

    fun getdaftarPenggunaanTanah(): Observable<ResponseDataList<ResponseDataListPenggunaanTanah>> {
        return apiService.getdaftarPenggunaanTanah()
    }

    fun getdaftarPeruntukanTanah(): Observable<ResponseDataList<ResponseDataListPeruntukanTanah>> {
        return apiService.getdaftarPeruntukanTanah()
    }

    fun getdaftarTopgrafiTanah(): Observable<ResponseDataList<ResponseDataListTipografi>> {
        return apiService.getdaftarTopgrafiTanah()
    }

    fun getdaftarinfoTambahanTanah(): Observable<ResponseDataList<ResponseDataListInformasiTambahanTanah>> {
        return apiService.getdaftarinfoTambahanTanah()
    }

    fun getdaftarbentukTanah(): Observable<ResponseDataList<ResponseDataListBentukTanah>> {
        return apiService.getdaftarbentukTanah()
    }

    fun getdaftarposisiTanah(): Observable<ResponseDataList<ResponseDataListPosisiTanah>> {
        return apiService.getdaftarposisiTanah()
    }


    /* bangunan */
    fun getdaftarTipeBangunan(): Observable<ResponseDataList<ResponseDataListTipeBangunan>> {
        return apiService.getdaftarTipeBangunan()
    }

    fun getdaftarPondasiBangunan(): Observable<ResponseDataList<ResponseDataListPondasiBangunan>> {
        return apiService.getdaftarPondasiBangunan()
    }

    fun getdaftarStrukturBangunan(): Observable<ResponseDataList<ResponseDataListStrukturBangunan>> {
        return apiService.getdaftarStrukturBangunan()
    }

    fun getdaftarKerangkaAtapBangunan(): Observable<ResponseDataList<ResponseDataListKerangkaAtapBangunan>> {
        return apiService.getdaftarKerangkaAtapBangunan()
    }

    fun getdaftarPenutupAtapBangunan(): Observable<ResponseDataList<ResponseDataListPenutupAtapBangunan>> {
        return apiService.getdaftarPenutupAtapBangunan()
    }

    fun getdaftarPlafonBangunan(): Observable<ResponseDataList<ResponseDataListPlafonBangunan>> {
        return apiService.getdaftarPlafonBangunan()
    }

    fun getdaftarDindingBangunan(): Observable<ResponseDataList<ResponseDataListDindingBangunan>> {
        return apiService.getdaftarDindingBangunan()
    }

    fun getdaftarPintuJendelaBangunan(): Observable<ResponseDataList<ResponseDataListPintuJendelaBangunan>> {
        return apiService.getdaftarPintuJendelaBangunan()
    }

    fun getdaftarLantaiBangunan(): Observable<ResponseDataList<ResponseDataListLantaiBangunan>> {
        return apiService.getdaftarLantaiBangunan()
    }

    /* tanaman */
    fun getkategoriTanaman(plantType: Int): Observable<ResponseDataList<ResponseDataListKategoriTanaman>> {
        return apiService.getkategoriTanaman(plantType)
    }

    /* sarana pelngkap */
    fun getdaftarjenisFasilitas(): Observable<ResponseDataList<ResponseDataJenisFasilitas>> {
        return apiService.getdaftarjenisFasilitas()
    }

    fun getdaftarsumberAir(): Observable<ResponseDataList<ResponseDataJenisSumberAir>> {
        return apiService.getdaftarsumberAir()
    }

    fun getdaftarpagarKeliling(): Observable<ResponseDataList<ResponseDataJenisPagarKeliling>> {
        return apiService.getdaftarpagarKeliling()
    }


    /* objek tanah */
    fun getdaftarobjekGedung(projectLandId: Int, accessToken: String): Observable<ResponseDataList<ResponseDataListGedung>> {
        return apiService.getdaftarobjekGedung(projectLandId, accessToken)
    }

    fun getdetailobjekBangunan(projectBuildingId: Int, accessToken: String): Observable<ResponseDataObject<ResponseDataDetailBangunan>> {
        return apiService.getdetailobjekBangunan(projectBuildingId, accessToken)
    }

    fun postBuilding(userId: Int, projectLandId: Int, projectPartyId: Int,
                     projectBuildingTypeId: Int, projectBuildingFloorTotal: Int,
                     projectBuildingAreaUpperRoom: String, projectBuildingAreaLowerRoom: String,
                     ceilingTypeId: ArrayList<String>, pbCeilingTypeAreaTotal: ArrayList<Float>,
                     doorWindowTypeld: ArrayList<String>, pbDoorWindowTypeAreaTotal: ArrayList<Float>,
                     floorTypeId: ArrayList<String>, pbFloorTypeAreaTotal: ArrayList<Float>,
                     foundationTypeId: ArrayList<String>, pbFoundationTypeAreaTotal: ArrayList<Float>,
                     roofCoveringTypeId: ArrayList<String>, pbRoofCoveringTypeAreaTotal: ArrayList<Float>,
                     roofFrameTypeId: ArrayList<String>, pbRoofFrameTypeAreaTotal: ArrayList<Float>,
                     structureTypeId: ArrayList<String>, pbStructureTypeAreaTotal: ArrayList<Float>,
                     wallTypeId: ArrayList<String>, pbWallTypeAreaTotal: ArrayList<Float>,
                     accessToken: String): Observable<ResponseDataObject<BaseResponse>> {
        return apiService.postBuilding(userId, projectLandId, projectPartyId, projectBuildingTypeId, projectBuildingFloorTotal,
                projectBuildingAreaUpperRoom, projectBuildingAreaLowerRoom,
                ceilingTypeId, pbCeilingTypeAreaTotal,
                doorWindowTypeld, pbDoorWindowTypeAreaTotal,
                floorTypeId, pbFloorTypeAreaTotal,
                foundationTypeId, pbFoundationTypeAreaTotal,
                roofCoveringTypeId, pbRoofCoveringTypeAreaTotal,
                roofFrameTypeId, pbRoofFrameTypeAreaTotal,
                structureTypeId, pbStructureTypeAreaTotal,
                wallTypeId, pbWallTypeAreaTotal,
                accessToken

        )
    }

    fun postPlant(userId: Int, projectLandId: Int, projectPartyId: Int, plantId: Int, pPlantPlantType: Int,
                  pPlantPlantSizedId: Int, pPlantNumber: Int, accessToken: String): Observable<BaseResponse> {
        return apiService.postPlant(userId, projectLandId, projectPartyId, plantId, pPlantPlantType,
                pPlantPlantSizedId, pPlantNumber, accessToken)
    }

    fun getdaftarobjekTanaman(projectLandId: Int, accessToken: String): Observable<ResponseDataList<ResponseDataListTanaman>> {
        return apiService.getdaftarobjekTanaman(projectLandId, accessToken)
    }

    fun postOtherFacility(userId: Int, projectLandId: Int, projectPartyId: Int, facilityTypeId: ArrayList<Int>, pOtherFacilityName: ArrayList<String>,
                          pOtherFacilityValue: ArrayList<String>, pOtherFacilityDescription: ArrayList<String>, accessToken: String): Observable<ResponseDataObject<BaseResponse>> {
        return apiService.postOtherFacility(userId, projectLandId, projectPartyId, facilityTypeId, pOtherFacilityName,
                pOtherFacilityValue, pOtherFacilityDescription, accessToken)
    }

    fun getdaftarobjekLain(projectLandId: Int, accessToken: String): Observable<ResponseDataList<ResponseDataListFasilitasLain>> {
        return apiService.getdaftarobjekLain(projectLandId, accessToken)
    }

    fun getdetailobjekLain(projectLandId: Int, projectPartyId: Int, facilityTypeId: Int, accessToken: String): Observable<ResponseDataObject<ResponseDataListDetailFasilitasLain>> {
        return apiService.getdetailobjekLain(projectLandId, projectPartyId, facilityTypeId, accessToken)
    }


    /*wilayah without parameter*/
    fun getlistOfProvinceWithOutParameter(): Observable<ResponseDataList<ResponseDataProvinceWithoutParameter>> {
        return apiService.getlistOfProvinceWithOutParameter()
    }

    fun getlistOfCityWithOutParameter(): Observable<ResponseDataList<ResponseDataCityWithoutParameter>> {
        return apiService.getlistOfCityWithOutParameter()
    }

    fun getlistOfKecamatanWithOutParameter(limit: Int, offset: Int): Observable<ResponseDataList<ResponseDataKecamatanWithoutParameter>> {
        return apiService.getlistOfKecamatanWithOutParameter(limit, offset)
    }

    fun getlistOfKelurahanWithOutParameter(limit: Int, offset: Int): Observable<ResponseDataList<ResponseDataKelurahanWithoutParameter>> {
        return apiService.getlistOfKelurahanWithOutParameter(limit, offset)
    }

    /* Data tanah with out parameter */
    fun getdaftarPenguasaanTanahWithOutParam(): Observable<ResponseDataList<ResponseDataListPenguasaanTanahWithOutParam>> {
        return apiService.getdaftarPenguasaanTanahWithOutParam()
    }

    fun getdaftarbentukTanahWithOutParam(): Observable<ResponseDataList<ResponseDataListBentukTanahWithOutParam>> {
        return apiService.getdaftarbentukTanahWithOutParam()
    }

    fun getdaftarTopgrafiTanahWithOutParam(): Observable<ResponseDataList<ResponseDataListTipografiWithOutParam>> {
        return apiService.getdaftarTopgrafiTanahWithOutParam()
    }

    fun getdaftarposisiTanahWithOutParam(): Observable<ResponseDataList<ResponseDataListPosisiTanahWithOutParam>> {
        return apiService.getdaftarposisiTanahWithOutParam()
    }

    fun getdaftarKepemilikanTanahWithOutParam(): Observable<ResponseDataList<ResponseDataListKepemilikanAtasTanahWithOutParam>> {
        return apiService.getdaftarKepemilikanTanahWithOutParam()
    }

    fun getdaftarPenggunaanTanahWithOutParam(): Observable<ResponseDataList<ResponseDataListPenggunaanTanahWithOutParam>> {
        return apiService.getdaftarPenggunaanTanahWithOutParam()
    }

    fun getdaftarinfoTambahanTanahWithOutParam(): Observable<ResponseDataList<ResponseDataListInformasiTambahanTanahWithOutParam>> {
        return apiService.getdaftarinfoTambahanTanahWithOutParam()
    }

    fun getdaftarPeruntukanTanahWithOutParam(): Observable<ResponseDataList<ResponseDataListPeruntukanTanahWithOutParam>> {
        return apiService.getdaftarPeruntukanTanahWithOutParam()
    }

    /* Data bangunan without parameter */
    fun getdaftarDindingBangunanWithOutParam(): Observable<ResponseDataList<ResponseDataListDindingBangunanWithOutParam>> {
        return apiService.getdaftarDindingBangunanWithOutParam()
    }

    fun getdaftarKerangkaAtapBangunanWithOutParam(): Observable<ResponseDataList<ResponseDataListKerangkaAtapBangunanWithOutParam>> {
        return apiService.getdaftarKerangkaAtapBangunanWithOutParam()
    }

    fun getdaftarPenutupAtapBangunanWithOutParam(): Observable<ResponseDataList<ResponseDataListPenutupAtapBangunanWithOutParam>> {
        return apiService.getdaftarPenutupAtapBangunanWithOutParam()
    }

    fun getdaftarLantaiBangunanWithOutParam(): Observable<ResponseDataList<ResponseDataListLantaiBangunanWithOutParam>> {
        return apiService.getdaftarLantaiBangunanWithOutParam()
    }

    fun getdaftarPintuJendelaBangunanWithOutParam(): Observable<ResponseDataList<ResponseDataListPintuJendelaBangunanWithOutParam>> {
        return apiService.getdaftarPintuJendelaBangunanWithOutParam()
    }

    fun getdaftarPlafonBangunanWithOutParam(): Observable<ResponseDataList<ResponseDataListPlafonBangunanWithOutParam>> {
        return apiService.getdaftarPlafonBangunanWithOutParam()
    }

    fun getdaftarPondasiBangunanWithOutParam(): Observable<ResponseDataList<ResponseDataListPondasiBangunanWithOutParam>> {
        return apiService.getdaftarPondasiBangunanWithOutParam()
    }

    fun getdaftarStrukturBangunanWithOutParam(): Observable<ResponseDataList<ResponseDataListStrukturBangunanWithOutParam>> {
        return apiService.getdaftarStrukturBangunanWithOutParam()
    }

    fun getdaftarTipeBangunanWithOutParam(): Observable<ResponseDataList<ResponseDataListTipeBangunanWithOutParam>> {
        return apiService.getdaftarTipeBangunanWithOutParam()
    }

    /* Data tanaman without parameter */
    fun getkategoriTanamanWithRealmObject(plantId: String): Observable<ResponseDataList<ResponseDataListKategoriTanamanWithOutParam>> {
        return apiService.getkategoriTanamanWithRealmObject(plantId)
    }

    fun getkategoriRumpunWithRealmObject(plantId: String): Observable<ResponseDataList<ResponseDataListKategoriRumpunWithOutParam>> {
        return apiService.getkategoriRumpunWithRealmObject(plantId)
    }


    /* Data sarana pelengkap without parameter */
    fun getdaftarjenisFasilitasWithOutParam(): Observable<ResponseDataList<ResponseDataJenisFasilitasWithOutParam>> {
        return apiService.getdaftarjenisFasilitasWithOutParam()
    }

    fun getdaftarpagarKelilingWithOutParam(): Observable<ResponseDataList<ResponseDataJenisPagarKelilingWithOutParam>> {
        return apiService.getdaftarpagarKelilingWithOutParam()
    }

    fun getdaftarsumberAirWithOutParam(): Observable<ResponseDataList<ResponseDataJenisSumberAirWithOutParam>> {
        return apiService.getdaftarsumberAirWithOutParam()
    }


    /* Data subjek without parameter */
    fun getdaftarMataPencaharianWithOutParam(): Observable<ResponseDataList<ResponseDataMataPencaharianWithOutParam>> {
        return apiService.getdaftarMataPencaharianWithOutParam()
    }

    fun getdaftarPekerjaanWithOutParam(): Observable<ResponseDataList<ResponseDataPekerjaanWithOutParam>> {
        return apiService.getdaftarPekerjaanWithOutParam()
    }

    fun getdaftarSumberIdentitasWithOutParam(): Observable<ResponseDataList<ResponseDataSumberIdentitasWithOutParam>> {
        return apiService.getdaftarSumberIdentitasWithOutParam()
    }

    /*fun getAuction(token: String, user_id: String, offset: String): Observable<ResponseDataObject<AuctionData>> {
        return apiService.getAuction(token, user_id, offset)
    }

    fun getStepAuction(token: String, user_id: String, url: String): Observable<ResponseDataObject<StepData>> {
        return apiService.getStepAuction(token, user_id, url)
    }*/

    /* 14 feburari 2019 */

    fun updateUserInfo(accessToken: String, loginResponse: LoginResponse, sessionLogin: SessionAuth) {
        Preference.saveAccessToken(accessToken)
        Preference.saveAuth(loginResponse)
        Preference.savesessionAuth(sessionLogin)
    }

    fun getaccessToken(): String{
        return Preference.accessToken
    }



}
